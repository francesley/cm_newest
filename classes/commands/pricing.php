<?



class command_pricing extends CM_Command {

	

	private $allSelected;

	private $smallSelected;

	private $medSelected;

	private $largeSelected;

	private $frameSelected;

	private $tradSelected;

	private $chineseSelected;

	private $opener;



    function doExecute( CM_Request $request ) {

        

		$contentGetter = new pricing_DBController($request -> getProperty("subtype"));

		$pageDetails = $contentGetter -> find();

			

		if (!$pageDetails) {

			$request->addFeedback( "No pageDetails, pricing.php line22");

			return 0;

		}

		

		$subMenu = new pricingMenu();

		

		$img = parent::GetImage($pageDetails -> imgName, $pageDetails -> alt, true);

		$vectors = <<<EOQ
<div id='testi'>
{$pageDetails -> quote}{$pageDetails -> testimonial}
</div>
EOQ;

		$img = "<div id='pic'>\n$img\n$vectors\n" . $subMenu -> m_menu . "\n";

		

		

		

		require_once("../classes/get_xml.php");		

		$selText = "class='tabSelected'";

		$xpath = false; 

		$right_include = $this -> INCLUDE_DIR . "pricing.php";

		$class = "GetMarqueePriceList";

		

		switch ($request -> getProperty("subtype")) {

			case "event_marquees":

				$class = "GetMarqueeSizePriceList";

				$xpath = "row[@size='large']";

				$this -> largeSelected = $selText;

				$opener = "<p>All sizes and styles of marquees accomodating 300+ guests.</p>";

				break;

			case "smaller_marquees":

				$class = "GetMarqueeSizePriceList";

				$xpath = "row[@size='small']";

				$this -> smallSelected = $selText;

				$opener = "<p>All sizes and styles of marquees accomodating less than 100 guests.</p>";

				break;

			case "medium_marquees":

				$xpath = "marquee[@size='medium']";

				$this -> medSelected = $selText;

				$opener = "<p>All sizes and styles of marquees accomodating between 75 and 600 guests</p>";

				break;

			case "traditional_marquees":

				$xpath = "marquee[@type='traditional']";

				$this -> tradSelected = $selText;

				$opener = "<p>Prices for <em>traditional</em> style marquees.</p>";

				break;

			case "frame_marquees":

				$xpath = "marquee[@type='frame']";

				$this -> frameSelected = $selText;

				$opener = "<p>Prices for clear span <em>frame</em> marquees.</p>";

				break;

			case "equipment":

				$class = "GetEquipmentPriceList";

				$opener = "";

				$right_include = $this -> INCLUDE_DIR . "pricing_equipment.php";

				break;

			case "furniture":

				$class = "GetFurniturePriceList";

				$opener = "";

				$right_include = $this -> INCLUDE_DIR . "pricing_furniture.php";

				break;

			case "sample":

				$class = "GetSamples";

				$opener = "<p style='margin-bottom:1.5em'>The following example quotes give likely costs for three typical marquee events. We hope they provide useful guidelines.</p><p>For individual advice, please <a href='/contact_us.htm'>contact us</a>. Or request your own free <a href='/pricing/quotation.htm'>personalised quote</a>.</p>";

				$right_include = $this -> INCLUDE_DIR . "pricing_samples.php";

				break;

			case "chinese":

				$xpath = "marquee[@type='chinese hat']";

				$opener = "<p>Prices for <em>chinese hat</em> or <em>pagoda</em> style marquees.</p>";

				$this -> chineseSelected = $selText;

				break;

			default:

				$class = "GetAllMarqueesPriceList";

				$this -> allSelected = $selText;

				$opener = "";

				$xpath = "";

				break;

				

		}

		

		if ($xpath || $request -> getProperty("subtype") == "marquees") {

			

			$all = $request -> getProperty("subtype") == "marquees"?true:false;

			$priceListGetter = new Get_html_xml('xml/marquee_prices.xml');

			$pageDetails -> footerExtra .= "<script type=\"text/javascript\" src=\"/scripts/jquery-1.1.4.pack.js\" ></script>\n<script language=\"javascript\" src=\"/scripts/facets.js\" type=\"text/javascript\"></script>";

			

			$facets = <<<EOF
<div id='facets'>
<p id='switch'>Prices for the marquee you need</p>
<div id='pick'>
<p><a href='javascript:void(0)' id='picker'>Filter by size or type</a><a href="javascript:void(0)" id="close"><img src="/images/down_arrow.png" width="12" height="6" alt="Show more" /></a></p>
<div id='facetlists'>
<ul class='first'>
<li><a href='marquees.htm'  {$this -> allSelected} >All marquee prices</a></li>
</ul>
<ul><li class="select">Select by size</li>

<li><a href="smaller_marquees.htm" {$this -> smallSelected} >Small<br/>(30 to 110)</a></li>
<li><a href="medium_marquees.htm" {$this -> medSelected} >Medium<br/>(75 to 600)</a></li>
<li><a href="event_marquees.htm" {$this -> largeSelected} >Large<br/>(230 to 880)</a></li>
</ul>
<ul  class='rightfacet'>
<li class="select">Select by type</li>
<li><a href="frame_marquees.htm" {$this -> frameSelected} >Frame<br/>marquees</a></li>
<li><a href="traditional_marquees.htm" {$this -> tradSelected} >Traditional<br/>marquees</a></li>
<li><a href="chinese.htm" {$this -> chineseSelected} >Chinese hat<br/>marquees</a></li>
</ul>
</div></div></div>
EOF;



$help = <<<EOF
<div id='help'>
<p id='helptitle'><a href="javascript:void(0)" style="float:left;">Help with marquee pricing</a><a href="javascript:void(0)" id="closehelp"><img src="/images/down_arrow.png" width="12" height="6" alt="Show more" /></a><div class="unnec"></div></p>
<div  id="helpcontents"><ul>
<li>Marquee prices are determined by size</li>
<li><strong>How big</strong> a marquee you will need depends on whether guests are <strong>seated or not</strong>, and <strong><em>extras</em></strong>. Our price lists include seated and buffet prices; and prices with and without simple flooring, lighting and lining.</li>
<li>Determine the size you will need from the <strong>Capacity</strong> columns in the tables below. Then look at the <strong>Total hire price</strong> columns on the right. This will be the rough cost</li>
<li>Alternatively, the <strong>quickest way to get an idea of costs</strong> is to use our <a href="../marquee_essentials.html">online marquee quote</a>. It is accurate, immediate and you can print or mail all quotes.</li>
<li>For other ways to work out the cost, see <a href="../marquee_essentials.html">How much does it cost to hire a marquee</a>.</li>
<li> Or look at some <a href="sample.htm">sample marquee prices</a> for examples of all-in prices</li>
<li>If you already know what you would like, <a href="/pricing/quotation.htm">request a no-obligation quotation</a> online.</li>
<li>For more help, please <a href="../contact_us.htm">contact us</a>.</li>
</ul></div></div>
EOF;





			$content =  $all?"<div id='pricing'>$facets<h1>Marquee pricing</h1>$help":"<div id='pricing'>$facets<h1>Marquee pricing &#8212; {$pageDetails -> page_title}</h1>$help";

			

			if ($priceListGetter -> xml) {

				$content .= "\n$opener \n<div id='pricetables'>\n<div id='mob'>" .$priceListGetter -> $class($xpath) . "</div>";

			} else {

				//TODO something

				$request -> addFeedback("cant get xml, pricing.php line 161", $request -> getProperty("cat") . ", " .  $request -> getProperty("subtype"));

				return 2;

			}

		} else {

		

			

		

			if ($request -> getProperty("subtype") != "sample") { 

				$content =  "<div id='pricing'><h1 style='margin-bottom:1.5em'>{$pageDetails -> page_title}</h1>";

			} else {

				$content =  "<div id='pricing'><h1>{$pageDetails -> page_title}</h1>";

			}

			$xml_file = "xml/" . $request -> getProperty("subtype") . "_prices.xml";

			$priceListGetter = new Get_html_xml($xml_file);

			

			if ($priceListGetter -> xml) {

				$content .= $opener . $priceListGetter -> $class();

			} else {

				//TODO something

				$request -> addFeedback("cant get xml, pricing.php line 180", $request -> getProperty("cat") . ", " .  $request -> getProperty("subtype"));

				return 2;

			}

		}

		

		$content .="</div>";

		

		

		require_once("shortBody.php");

		echo $img;

		require_once($right_include);

		echo $content;

        

		require_once( "end.php");		

		return true;

    }

}





?>

