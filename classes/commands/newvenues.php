<?

class command_newvenues extends CM_Command {

    function doExecute( CM_Request $request ) {       
		
		$contentGetter = new specials_DBController("newvenues");
		$pageDetails = $contentGetter -> find();
		
		if (!$pageDetails) {
			$request->addFeedback( "No pageDetails, venues.php line13");
			return 0;
		}		
		
		$subMenu = new venuesMenu(true);
		require_once("shortBody.php");
		
		echo "<img src=\"/images/top/venue_thin.jpg\" width=\"918\" height=\"70\" id=\"smallpic\" alt=\"Marquee wedding venue in Surrey\"/><h1>" . $pageDetails -> page_title . "</h1>";
		echo $subMenu -> m_menu;
		require_once("maps/map.php");
		
		//$request->addFeedback( "Welcome to County Marquees from venues" );
		require_once( "end.php");
		return true;
    }
	
}


?>
