<?php



class command_default extends CM_Command {

    function doExecute( CM_Request $request ) {

		

		

		$contentGetter = new noSubType_DBController("home");

		$pageDetails = $contentGetter -> find();

		

		//no db result

		//Util::ShowTrue($pageDetails, "pageDetails");

		if (!$pageDetails) {

			return false;

		}

		

		//crazyegg

		/*$pageDetails  -> footerExtra .= <<<EOQ

			        <script type="text/javascript">

				  setTimeout(function(){var a=document.createElement("script");

				  var b=document.getElementsByTagName("script")[0];

				  a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0004/0247.js?"+Math.floor(new Date().getTime()/3600000);

				  a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

				  </script>



		

EOQ;*/



		$vectors = <<<EOQ
<div id='testi'>
{$pageDetails -> quote}{$pageDetails -> testimonial}
</div>
EOQ;

		$img = <<<EOQ
<div id='pic'>
<div id="imgcycle">
<a href="/photos/funky.htm"><img src='/images/top/winter_wedding.jpg' width='918' height='323'  alt="Winter wonderland wedding" /></a>
<a href="/photos/funky.htm" ><img src='/images/home/aerial.jpg' width='918' height='323'  alt="Drone view of large clear marquee at night"  loading="lazy" /></a>
<a href="/photos/waiting.htm" ><img src='/images/home/hanging.jpg' width='918' height='323'  alt="Luxury marquee with hanging foliage baskets" loading="lazy" /></a>
<a href="/photos/wedding.htm" ><img src='/images/home/clear_marquee.jpg' width='918' height='323'  alt="Transparent marquee" loading="lazy" /></a>
<a href="/photos/luxury.htm" ><img src='/images/home/fairy_light_canopy.jpg' width='918' height='323'  alt="Frame marquee with fairy lights strung across the roof"  loading="lazy" /></a>
<a href="/photos/classy.htm"><img src='/images/home/traditional_tent.jpg' width='918' height='323'  alt="Traditional style tent"  loading="lazy" /></a>
</div>
$vectors
</div>
EOQ;


$content = <<<EOF
<div id="homeTop">
$img
<div class="unnec"></div> 
</div>
<div id="text" style="height:1380px;">
<h1>Marquee hire for every occasion</h1>
<p id="topP"><strong>Our promise</strong>: beautiful marquees and efficient friendly service.</p>
<div class="homebox">
<a href="wedding-marquees.htm"><img src='/images/home/wedding_marquees.jpg' width='188' height='100' alt="Wedding marquee hire" loading="lazy" /></a>
<h3><a href="/wedding-marquees.htm">Wedding marquees</a></h3>
<p>With experience of 1,000s of marquee weddings, we know you need perfection.</p>
<a class="bottom" href="/wedding-marquees.htm">Find out more</a>
</div>
<div class="homebox">
<a href="events/party_marquees.htm"><img src='/images/home/party_marquees2.jpg' width='188' height='100' alt="Party marquee"  loading="lazy" /></a>
<h3><a href="events/party_marquees.htm">Party marquees</a></h3>
<p>Create your very own world for birthdays, anniversaries and all kinds of celebrations.</p>
<a class="bottom" href="events/party_marquees.htm">Find out more</a>
</div>
<div class="homebox right">
<a href="/corporate-marquees.htm"><img src='/images/home/racing_marquee.jpg'  loading="lazy" width='188' height='100' alt="Corporate marquee at racecourse" /></a>
<h3><a href="/corporate-marquees.htm">Corporate marquees</a></h3>
<p>Spectacular tents for corporate events: on time, on budget and without fuss.</p>
<a class="bottom" href="/corporate-marquees.htm">Find out more</a>
</div>
<div id="homeMiddle">
<h2>Look inside real marquees</h2>
<div class="homebox venues upper">
<a href="/real.htm"><img src='/images/home/real_marquees.jpg' width='188' height='100' alt="Marquee ideas"  loading="lazy" /></a>
<p>See how other people style their marquees,  pretty, festive,  themed, brightly coloured...</p>
<a class="bottom" href="/real.htm">See marquee case studies</a>
</div>
<h2>Online marquee quote</h2>
<div class="homebox venues upper">
<a href="/pricing/quote.htm"><img src='/images/home/online_quote.png' width='188' height='100' alt="Online marquee quotation"  loading="lazy" /></a>
<p>Find out <em>immediately</em> how much the marquee you want will cost and then print out or email all online quotes.</p>
<a class="bottom" href="/pricing/quote.htm">Get an immediate marquee hire quote</a>
</div>
<h2>Work for County Marquees</h2>
<div class="social-dist">
<div class="homebox venues distance upper">
<a href="/company/jobs.htm"><img src='/images/home/work_for_us.jpg' width='188' height='100' alt="work for us" /></a>
<p>Hardworking, enthusiastic and enjoy working as part of a team? Keep fit by working for County Marquees</p>
<a class="bottom" href="/company/jobs.htm">Work for us</a>
</div>
</div>
<div class="nomob">
<h2>Marquee venue finder</h2>
<div class="homebox venues upper">
<a href="/venues.htm"><img src='/images/home/venues.jpg' width='188' height='100' alt="Marquee wedding venues"  loading="lazy" /></a>
<p>Find your perfect marquee venue in South East England with our <a href="/venues.htm">marquee venue finder</a></p>
<p>Search by region, price and features.</p>
<a class="bottom" href="/venues.htm">Find marquee venues</a>
</div>
</div></div></div>
<div id="right">
<div id="socialhead">
<a href="https://www.facebook.com/countymarquees" target="_blank" ><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" rel="noopener noreferrer"  loading="lazy" /></a>
<a href="https://twitter.com/County_Marquees" target="_blank"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social" rel="noopener noreferrer"  loading="lazy" /></a>
<a href="http://pinterest.com/county_marquees/" target="_blank" ><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"  loading="lazy" /></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="publisher"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" rel="noopener noreferrer"  loading="lazy" /></a>
</div>
<div class="homebox" id="start" >
<a href="/marquee_essentials.html"><img src='/images/home/where_start.jpg' width='191' height='100' alt="Marquee in garden" /></a>
<h3><a href="/marquee_essentials.html">Where do I start?</a></h3>
<p style="margin-bottom:2.3em">How much does a marquee cost? How big a marquee will you need?</p>
<a class="bottom" href="/marquee_essentials.html">Find answers</a>
</div>
<a href="http://www.muta.org.uk/" target="_blank"><img src="/images/muta.png" alt="Muta membership 2020" width="191" height="221" style="width:191px;height:auto;border-top:1px dotted #bdbeb3;margin-top: 20px;padding-top: 20px;padding-right:0"/></a></div>
EOF;

		

		

		$this -> summary = <<<EOQ
<div id="summaryDiv">
<h3>County Marquees Locations</h3>

<div id="hirecounties"><p><strong>County Marquees London </strong><br/>
Covering <a href="/areas/northlondon.htm">North London, <a href="/areas/southlondon.htm">South London</a>, <a href="/areas/bucks.htm">Buckinghamshire, <a href="/areas/herts.htm">Hertfordshire</a>, <a href="/areas/oxfordshire.htm">Oxfordshire</a>, Essex and Bedfordshire<br/>
<a href="tel:+442072674271">Tel: 020 7267 4271</a></p>
<p><strong><a href="/areas/surrey.htm">County Marquees Surrey</a></strong> <br/>
Covering Berkshire and Surrey<br/>
Guildford<br/>
<a href="tel:+441483538617">Tel:01483 538617</a></p>
<p><strong><a href="/areas/sussex.htm">County Marquees Sussex</a></strong> <br/>
Covering Berkshire, Hampshire and East and West Sussex<br/>
Chichester<br/>
<a href="tel:+441243790290">Tel: 01243 790290</a></p>
<p><strong><a href="/areas/kent.htm">County Marquees Kent</a></strong><br/>
Tunbridge Wells<br/>
<a href="tel:+441892506870">Tel: 01892 506870</a></p>
<p style="clear:both"><strong>Email County Marquees:
<a href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a></strong></p></div>
<h4>Local areas covered include:</h4>
<ul id="local"><li><a href="/areas/abingdon.htm">Abingdon</a></li><li>Aldershot</li><li><a href="/areas/amersham.htm">Amersham</li><li>Andover</li><li>Arkley</li><li>Arundel</li><li>Ashford</li><li>Ascot</li><li>Aylesbury</li><li><a href="/areas/baldock.htm">Baldock</a></li><li>Barnet</li><li>Barnes</li><li><li>Basingstoke</li><li><a href="/areas/beckenham.htm">Beckenham</a></li><li><a href="/areas/berkhamsted.htm">Berkhamsted</a></li><li><a href="/areas/bishops-stortford.htm">Bishops Stortford</a></li><li>Borehamwood</li><li><a href="/areas/brighton.htm">Brighton</a></li><li><a href="/areas/bromley.htm">Bromley</a></li><li>Brookmans Park</li><li>Broxbourne</li><li>Bucks Hill</li><li>Bushey</li><li>Chelsea</li><li>Cheshunt</li><li><a href="/areas/chichester.htm">Chichister</a></li><li>Chigwell</li><li>Chingford</li><li>Chipperfield</li><li>Chiswick</li><li>Chorleywood</li><li><a href="/areas/cranleigh.htm">Cranleigh</a></li><li><a href="/areas/crawley.htm">Crawley</a></li><li><a href="/areas/croydon.htm">Croydon</a></li><li><a href="/areas/didcot.htm">Didcot</a></li><li><a href="/areas/dorking.htm">Dorking</a></li><li><a href="/areas/eastbourne.htm">Eastbourne</a></li><li>Ealing</li><li>Eastcote</li><li>Eastwick</li><li>Edgware</li><li><a href="/areas/enfield.htm">Enfield</a></li><li><a href="/areas/epsom.htm">Epsom</a></li><li>Edmondton</li><li>Elstree</li><li>Epping</li><li>Essendon</li><li>Finchley</li><li><a href="/areas/farnham.htm">Farnham</a></li><li>Friern Barnet</li><li>Fulham</li><li>Golders Green</li><li>Godalming</li><li><a href="/areas/guildford.htm">Guildford</a></li><li>Hadley Wood</li><li><a href="/areas/ham_high.htm">Hampstead</a></li><li>Harefield</li><li>Harlow</li><li>Harpenden</li><li>Harrow on the Hill</li><li>Harrow Weald</li><li><a href="/areas/harrow.htm">Harrow</a></li><li><a href="/areas/hastings.htm">Hastings</a></li><li>Hatfield</li><li>Hayes</li><li><a href="/areas/hertford.htm">Hertford</a></li><li>Hemel Hempstead</li><li><a href="/areas/henley.htm">Henley-on-Thames</a></li><li><a href="/areas/high-wycombe.htm">High Wycombe</a></li><li><a href="/areas/ham_high.htm">Highgate</a></li><li>Hitchin</li><li>Hoddesdon</li><li><a href="/areas/horsham.htm">Horsham</a></li><li>Hounslow</li><li>Hyde Park</li><li>Ickenham</li><li>Isleworth</li><li><a href="/areas/romford.htm">Ilford</a></li><li><a href="/areas/ham_high.htm">Islington</a></li><li>Kensington</li><li>Kew</li><li>Kings Langley</li><li>Kingsbury</li><li>Kingston</li><li>Knebworth</li><li>Letchworth Heath</li><li>Lewes</li><li>Loughton</li><li>Luton</li><li>Maidenhead</li><li><a href="/areas/maidstone.htm">Maidstone</a></li><li>Maple Cross</li><li><a href="/areas/marlow.htm">Marlow</a></li><li>Mill Hill</li><li><a href="/areas/olney.htm">Milton Keynes</a></li><li><a href="/areas/new-malden.htm">New Malden</a></li><li>Notting Hill</li><li><a href="/areas/northwood.htm">Northwood</a></li><li><a href="/areas/olney.htm">Olney</a></li><li><a href="/areas/oxfordshire.htm">Oxford</a></li><li>Pinner</li><li><a href="/areas/princes-risborough.htm">Princes Risborough</a></li><li>Portsmouth</li><li>Radlett</li><li>Redbourn</li><li>Rickmansworth</li><li><a href="/areas/richmond.htm">Richmond</a></li><li><a href="/areas/romford.htm">Romford</a></li><li>Rye</li><li>Shenley</li><li>Slough</li><li><a href="/areas/southgate.htm">Southgate</a></li><li>Southampton</li><li><a href="/areas/st-albans.htm">St Albans</a></li><li>Staines</li><li>Stanmore</li><li>Stansted</li><li>Sunbury on Thames</li><li>Stapleford</li><li>Stevenage</li><li><a href="/areas/stoke-mandeville.htm">Stoke Mandeville</a></li><li><a href="/areas/thame.htm">Thame</a></li><li><a href="/areas/tunbridge-wells.htm">Tunbridge Wells</a></li><li>Twickenham</li><li><a href="/areas/uckfield.htm">Uckfield</a></li><li>Uxbridge</li><li><a href="/areas/walton.htm">Walton-on-Thames</a></li><li>Waltham Cross</li><li>Walthamstow</li><li>Ware</li><li><a href="/areas/watford.htm">Watford</a></li><li><a href="/areas/welwyn.htm">Welwyn Garden City</a></li><li>Wembley</li><li>West Hyde</li><li><a href="/areas/weybridge.htm">Weybridge</a></li><li>Wheathampstead</li><li>Whetstone</li><li><a href="/areas/wimbledon.htm">Wimbledon</a></li><li><a href="/areas/witney.htm">Witney</a></li><li>Woking</li></ul>
<h3 class="hire">Hire a marquee from County Marquees!</h3>
</div><div class="unnec">&nbsp;</div>
EOQ;

		

		require_once("shortBody.php");

		echo $content;

		require_once( "end.php");

		return true;

    }

}





?>