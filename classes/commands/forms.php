<?



class command_forms extends CM_Command {

	public $formResponse;

	public $styleFormResponse;



    function doExecute( CM_Request $request ) {

		

		$contentGetter = new forms_DBController($request -> getProperty("cat"), $request -> getProperty("subtype"));

		$pageDetails = $contentGetter -> find();


		if (!$pageDetails) {

			$request->addFeedback( "No pageDetails, forms.php line 13");

			return 0;

		}

		

		require_once("../classes/formChecker.php");

		$checkerName = $request -> getProperty("subtype") . "Checker";

		$fc = new $checkerName($request);

		if (!$fc -> f_include) {

			//TODO - SOMETHING -  - return status to default command? exception - cos wont be able to display page?

			$request->addFeedback("from forms.php line 22, include file {$fc -> f_include} doesnt exist cat is " . $request -> getProperty("cat") . ", subtype is " . $request -> getProperty("subtype"));

			return 0;

		}

		

		if ($request -> getProperty("submitted")) {

			//$fc -> Check();

			//$request->addFeedback("formName: " . $formName);

			if (!$fc -> Check()) {

			

				$this -> formResponse = $fc -> formResponse;

				$this -> styleFormResponse = $fc -> styleFormResponse;

				//$pageDetails -> bodyExtra .= " onload=\"pageTracker._trackPageview('/goal/fail/" . $request -> getProperty("subtype") . "')\";";

				 $pageDetails -> bodyExtra .= <<<EOQ

					 	onload="_gaq.push(['_trackPageview', '/goal/fail/" . {$request -> getProperty("subtype")} . "']);"

EOQ;

				

			} else {

				require_once("../classes/mailer.php");

				$className = $request -> getProperty("subtype") . "Mailer";

				if (class_exists( $className )) {

					$sender = new $className($request);

					$this -> formResponse = $sender -> formResponse; 

					$this -> styleFormResponse = "style=\"display:block\" ";

					//$pageDetails -> bodyExtra .= " onload=\"pageTracker._trackPageview('/goal/" . $request -> getProperty("subtype") . "')\"";

					//Util::Show($pageDetails -> bodyExtra);

					$pageDetails -> bodyExtra .= " onload=\"_gaq.push(['_trackPageview', '/goal/" . $request -> getProperty("subtype") . "'])\"";

					 /*$pageDetails -> bodyExtra .= <<<EOQ

					 	onload ="_gaq.push(['_trackPageview', '/goal/" . {$request -> getProperty("subtype")} . "']);"

EOQ;*/

				} else {

					//TODO - SOMETHING - return status to default command?

					$request->addFeedback("class $className doesnt exist in forms.php, line 45");

					Util::SendFeedback("class $className doesnt exist in forms.php, line 45");

					$this -> formResponse = "Sorry we have been unable to send your mail. Please try submitting the form again.<br/>If that fails too, please call us on 020 7267 4271.";; 

					$this -> styleFormResponse = "style=\"display:block\" ";

				}

				

			}

			

		}

		

		//echo $pageDetails -> bodyExtra;

		

		switch ($request -> getProperty("subtype")) {

			case "jobs":

				$subMenu = new companyMenu();
				$pageDetails -> bodyExtra .= " class='jobs' ";	

				break;

			case "add_venue":

				$subMenu = new venuesMenu();

				$pageDetails -> bodyExtra .= " class='add_venue' ";			

				break;

			case "questions":

				$subMenu = new helpMenu();

				$pageDetails -> bodyExtra .= " class='questions' ";				

				break;

			case "quote_test":

			case "quote":

				$subMenu = new pricingMenu();

				//echo "y";

				break;

			default:

				//$subMenu = "";

				

		}

		

		require_once("shortBody.php");

		

		$img = $request -> getProperty("subtype") != "quote"?parent::GetImage($pageDetails -> imgName, $pageDetails -> alt):parent::GetImage($pageDetails -> imgName, $pageDetails -> alt,true);

		$vectors = <<<EOQ
<div id='testi'>
{$pageDetails -> quote}{$pageDetails -> testimonial}
</div>
EOQ;

			$right_include = $this -> INCLUDE_DIR . $request -> getProperty("subtype") . ".php";

			

			

		if (isset($subMenu)) {

		

			//HACK the div for the end of the #pic div is added in menus.php for all submenus except venues. Adding it to venues submenu fucks up all other venues pages and I cant see why. But the #pic end div should be in menus.php cos it is for all other submenus and so its easier that way.

			if ($request -> getProperty("subtype") != "add_venue") {

				$img = "<div id='pic'>\n$img\n$vectors\n" . $subMenu -> m_menu . "\n";

			} else {

				$img = "<div id='pic'>\n$img\n$vectors\n" . $subMenu -> m_menu . "\n</div>\n";

			}

		} else {

			$img = "<div id='pic'>\n$img\n$vectors\n</div>\n";

		}

		

		if ($request -> getProperty("subtype") != "questions") {

			echo "$img
<div id='text'><h1>" . $pageDetails -> page_title . "</h1>";			

			require_once($fc -> f_include);

			if ( file_exists( $right_include ) ) {

				include_once( $right_include );

			}

		} else {

			echo $img;

			if ( file_exists( $right_include ) ) {

				include_once( $right_include );

			}

			echo "<div id='text'><h1>" . $pageDetails -> page_title . "</h1>";			

			require_once($fc -> f_include);

			

		}

		

		require_once( "end.php");

		return true;

		

    }

}





?>

