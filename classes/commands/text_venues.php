<?



class command_text_venues extends CM_Command {

	

	private $oxfordSelected = "";

	private $bucksSelected = "";

	private $bedsSelected = "";

	private $hertsSelected = "";

	private $essexSelected = "";

	private $berksSelected = "";

	private $surreySelected = "";

	private $lonSelected = "";

	private $kentSelected = "";

	private $hantsSelected = "";

	private $wSussexSelected = "";

	private $eSussexSelected = "";

	private $allSelected = "";

	private $xmlContent;

	private $endCall =  "<p class='endCall note'><strong>Important note:</strong> County Marquees supplies marquee hire, not marquee venues. The venue map and pages are provided as a courtesy service for clients and website visitors for convenience. But we do not normally provide a marquee hire and venue package.</p><p class='endCall'>For more advice on how to choose a marquee venue or on marquee hire in general, please <a href='/contact_us.htm'>contact us</a>.</p>";



    function doExecute( CM_Request $request ) {

       

		if (!$request -> getProperty("county")) {

			$contentGetter = new text_venues_DBController($request -> getProperty("subtype"),"all");

			$pageDetails = $contentGetter -> find();

			

			if (!$pageDetails) {

				$request->addFeedback( "No pageDetails, text_venues.php line30");

				return 0;

			}

				$vectors = <<<EOQ

				<div id='testi'>

					{$pageDetails -> quote}{$pageDetails -> testimonial}

				</div>

		

EOQ;

		} else {

			$contentGetter = new text_venues_DBController($request -> getProperty("subtype"),$request -> getProperty("county"));

			$pageDetails = $contentGetter -> find();

			

			if ($pageDetails -> page_title == "") {

				return false;

			}

			

			//TODO change quotes to match venue county?

			$vectors = <<<EOQ

				<div id='testi'>

					<div><span id='testimonial'><a href='/company/testimonials.htm' title='Find out what other people say about us'>Everyone was wowed by the marquee</a></span><span>Hurley, Berkshire</span></div>

				</div>

EOQ;

		}

		

		

	

		

		$subMenu = new venuesMenu();		

		$content = "<div id='pic'> 

						<img src=\"/images/top/marquee_venue.jpg\" width=\"918\" height=\"215\" id=\"smallpic\" alt=\"{$pageDetails -> alt}\"/>

						$vectors

						 {$subMenu -> m_menu}

					</div>";

		$subMenu = null;

		if ($request -> getProperty("subtype") == "add_venuefinder") {

		

			$pageDetails -> footerExtra .= "\n<script type='text/javascript' src='/scripts/jquery-1.2.4.min.js'></script><script src='/scripts/form.js' type='text/javascript'></script><script src='/scripts/color.js' type='text/javascript'></script><script src='/scripts/widget.js' type='text/javascript'></script>\n";

			$pageDetails -> bodyExtra = " id='addwidget' ";

			$right_include = $this -> INCLUDE_DIR . "text_venues_add_venuefinder.php";

			if ( !file_exists( $right_include ) ) {

				$right_include = $this -> INCLUDE_DIR . "default_right.php";

			}

			

		} else {

			$pageDetails -> bodyExtra =  " id='sites' ";

			$right_include = "";

			

			require_once("../classes/get_xml.php");		

			$venueGetter = new Get_venues_xml('maps/markers.xml');

			

			//TODO cache this

			if ($venueGetter -> xml) {

				if (!($request -> getProperty("county")) ) {

					$pageDetails -> html_dec = "<html lang='en'>";

		$pageDetails -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com/$_SERVER[REQUEST_URI]\"/>";

		$pageDetails -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees'/>";		

					$this -> xmlContent .= $venueGetter -> GetVenues();
					
					$this -> xmlContent .="</div>";

					$this -> xmlContent .= $this -> endCall;

					$this -> allSelected = " class='selected'";

				} else {

					$xpath = "marker[@area='" . $request -> getProperty("county") . "']";

					$this -> xmlContent .= $venueGetter -> GetVenues($xpath);

					$this -> html_dec = "<html lang='en'>"; //TODO this doesnt work. is in shorbody so not really needed here. But why?

					//HACK cos London not working properly and I cant see why

					$this -> xmlContent .= "</div>";

					$this -> xmlContent .= $this -> endCall;

					$currentClass = $request -> getProperty("county") . "Selected";

					$this -> $currentClass = " class='selected'";

				}

			} else {

				//TODO something

				$request -> addFeedback("xml file not found", $request -> getProperty("cat") . ", " .  $request -> getProperty("subtype"));

				return 0;

			}

			

			

			

			$content .=  "<div id='facets'>

						<p id='switch'>Switch county</p>

						<p id='inspire'>See only venues close to you</p>

						<div id='pick'>

							<p><a href='javascript:void(0)' id='picker'>Choose a county:</a><a href='javascript:void(0)' id='close'><img src='/images/down_arrow.png' width='12' height='6' alt='Show more' /></a></p>

							<div id='facetlists'>

								<ul class='first'>

									<li><a href='/venues/sites.htm' {$this -> allSelected} >All counties</a></li>

								</ul>

								<ul>

								<li><a href='/venues/oxford.htm' id='oxford' title='Marquee venues in Oxfordshire'  {$this -> oxfordSelected} >Oxford</a></li>

								<li><a href='/venues/bucks.htm' id='bucks' title='Marquee venues in Buckinghamshire' {$this -> bucksSelected} >Bucks</a></li>

								<li><a href='/venues/beds.htm' id='beds' title='Marquee venues in Bedfordshire' {$this -> bedsSelected} >Beds</a></li>

								<li><a href='/venues/herts.htm' id='herts' title='Marquee venues in Hertfordshire' {$this -> hertsSelected} >Herts</a></li>

								<li><a href='/venues/essex.htm' id='essex' title='Marquee venues in Essex' {$this -> essexSelected} >Essex</a></li>

								<li><a href='/venues/berks.htm' id='berks' title='Marquee venues in Berkshire' {$this -> berksSelected} >Berks</a></li>

								</ul>

								<ul  class='rightfacet'>

								<li><a href='/venues/surrey.htm' id='surrey' title='Marquee venues in Surrey' {$this -> surreySelected} >Surrey</a></li>

								<li><a href='/venues/lon.htm' id='lon' title='Marquee venues in London' {$this -> lonSelected} >London</a></li>

								<li><a href='/venues/kent.htm' id='kent' title='Marquee venues in Kent' {$this -> kentSelected} >Kent</a></li>

								<li><a href='/venues/hants.htm' id='hants' title='Marquee venues in Hampshire' {$this -> hantsSelected} >Hants</a></li>

								<li><a href='/venues/wSussex.htm' id='wSussex' title='Marquee venues in West Sussex' {$this -> wSussexSelected} >West Sussex</a></li>

								<li><a href='/venues/eSussex.htm' id='eSussex' title='Marquee venues in East Sussex' {$this -> eSussexSelected} >East Sussex</a></li>

								</ul>

							

							</div>

						</div>

						</div>";

		}

		

		

		require_once("shortBody.php");

		

		//SITES

		if ($request -> getProperty("subtype") == "sites") {

			$content .=  "<h1>" . $pageDetails -> page_title . "</h1><div id='text'>";

			$content .= $this -> xmlContent;

			echo $content;

			



		//WIDGET

		} else {

			$content .=  "<h1>" . $pageDetails -> page_title . "</h1><div id='text'>";

			$code = "";

			$templateCode = <<<EOQ

<!-- Begin County Marquees Map Widget --><iframe style="width:#widgetWidth#px; height:450px; border:none;margin:0;padding:0;" scrolling="no" src="https://www.countymarqees.com/widget/#widgetType#.htm"></iframe><br/><p style="font-size:10px;margin:0 0 5px 0;">Widget by <a href='https://www.countymarquees.com/'  style="font-size:10px;">County Marquees</a> - <a href='https://www.countymarquees.com/venues/widget.htm'  style="font-size:10px;">Get your map widget</a></p><!-- End County Marquees Map Widget -->

EOQ;

			

			

			if ($request -> getProperty("submitted")) {

			

				switch($request -> getProperty("rbSize")) {

					case "narrow":

						$code = str_replace("#widgetWidth#", "300", $templateCode);

						$code = str_replace("#widgetType#", "narrow", $code);

						break;

					case "normal":

						$code = str_replace("#widgetWidth#", "400", $templateCode);

						$code = str_replace("#widgetType#", "normal", $code);

						break;

					case "wide":

						$code = str_replace("#widgetWidth#", "500", $templateCode);

						$code = str_replace("#widgetType#", "wide", $code);

						break;

					default:

						$code = "Sorry. There has been a problem. Please try again.";

				

				}

			}		

		

			$f_include = $this -> CONTENT_DIR . $request -> getProperty("cat") . "_" . $request -> getProperty("subtype") . ".php";

			

			if ( file_exists( $f_include ) ) {

				include_once( $f_include );

			} else {

				//Util::Show($f_include);

				echo "</div>";

			}

			

			echo $content;

			require_once($right_include);

		}

		

		

		

        

		require_once( "end.php");

		return true;

    }

}





?>

