<?



//require_once ('classes/db.php');



class command_case_studies extends CM_Command {

	private $covid_test;



    function doExecute( CM_Request $request ) {       

		

		$contentGetter = new case_DBController($request -> getProperty("cat"), $request -> getProperty("subtype"));		

		$pageDetails = $contentGetter -> find();

		

		

		//no db result

		//Util::ShowTrue($pageDetails, "pageDetails");

		if (!$pageDetails) {

			$request->addFeedback( "No pageDetails, case_studies.php line 16 - subtype is " . $request -> getProperty("subtype"));

			return false;

		}

		

		

		$f_include = $this -> CONTENT_DIR . $request -> getProperty("cat") . "_" . $request -> getProperty("subtype") . ".php";
		$covid_test = strpos($request -> getProperty("subtype"),"covid");
		$covid_test = !(($request -> getProperty("subtype") !== "distancing") && $covid_test === false);
		$rogue=false;		

		$no_lightbox = array("barmitzvah","ball","party");

			

			if (!in_array($request -> getProperty("subtype"),$no_lightbox)) {

				$pageDetails -> headerExtra .= <<<EOQ
<link rel="stylesheet" href="/scripts/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" /><script type="text/javascript" src="/scripts/jquery-latest.min.js" defer></script><script type="text/javascript" src="/scripts/fancybox/source/jquery.fancybox.pack.js" defer></script>
EOQ;
			$pageDetails -> headerExtra .= $covid_test?"<meta name='robots' content='NOINDEX' />":"";

			$pageDetails -> bodyExtra .= $covid_test?" class='lightbox case-distancing' ":" class='lightbox' ";



			} else {
				$pageDetails -> bodyExtra .= " class='nolightbox' ";
            	//$pageDetails -> bodyExtra .= " class='nolightbox' ";

            }

            

        $pageDetails -> bodyExtra .= " id='case' ";

            

		//$subMenu = $covid_test?new covidMenu():new helpMenu();
		if ($covid_test) {
			$subMenu = new covidMenu();
		} else {
			$subMenu = new helpMenu();
		}

		require_once("shortBody.php");

		

		$img = parent::GetImage($pageDetails -> imgName, $pageDetails -> alt, $rogue);

		

		$vectors = <<<EOQ
			<div id='testi'>
				{$pageDetails -> quote}{$pageDetails -> testimonial}
			</div>
EOQ;

		

		if (isset($subMenu)) {

			$img = "<div id='pic'>\n$img\n$vectors\n" . $subMenu -> m_menu . "\n";

		} else {

			$img = "<div id='pic'>\n$img\n$vectors\n</div>\n";

		}

		echo $img;




if ( !$covid_test) {			

		echo <<<EOF
<div id="rightwrap">
<div id="right" class="thumbs">
<h3><a href="/real.htm">&laquo; All Real Marquees</a></h3>
<div id="thumbsDiv">{$pageDetails -> thumbs}<div class="unnec"></div> 
</div></div>
<div id="rightextra">
<p class="aboveCall">For assistance or a free site visit:</p>
<a href="/contact_us.htm" class="aboveCall"><img src="/images/contactSM.jpg" alt="Contact us" width="195" height="62" loading="lazy"/></a>
<p class="aboveCall">Or do you already know what you want?</p> 
<a href="/pricing/quotation.htm"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62" loading="lazy"/></a>
<h3 id="socialhead">Get social</h3>
<a href="https://www.facebook.com/countymarquees" target="_blank" onclick="_gaq.push(['_trackEvent', 'Social', 'Facebook']);" rel="noopener noreferrer" ><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" loading="lazy"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank" onclick="_gaq.push(['_trackEvent', 'Social', Twitter']);" rel="noopener noreferrer" ><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/ loading="lazy"></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" onclick="_gaq.push(['_trackEvent', 'Social', 'Pinterest']);" rel="noopener noreferrer" ><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/ loading="lazy"></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" onclick="_gaq.push(['_trackEvent', 'Social', 'Instagram']);"  rel="noopener noreferrer" ><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" loading="lazy"/></a>
</div></div>
EOF;

}		

		echo "<div id='text'><h1>" . $pageDetails -> page_title . "</h1>";

		

		if ( file_exists( $f_include ) ) {

			include_once( $f_include );

            echo $content;

		} else {

			echo "</div>";

		}

		

		

		

		require_once( "end.php");

		//$this -> cacheFileName = $this -> cache -> currFile;

		return true;

    }

	

}





?>

