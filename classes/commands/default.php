<?php





require_once ('../classes/cacher.php');



abstract class CM_Command {



    private static $STATUS_STRINGS = array (

        'CMD_INCORRECT_'=>0,

        'CMD_OK' => 1,

        'CMD_ERROR' => 2,

        'CMD_INSUFFICIENT_DATA' => 3

    );

	private $CACHABLE = array ("events","help","pricing","company","panorama","photos","case_studies", "video", "text_venues","real","areas");

	private $UNCACHABLE_SUBTYPES = array("search","test","alternative_two");

	protected $INCLUDE_DIR = "right_includes/";

	protected $CONTENT_DIR = "includes/";

	protected $DEFAULT_IMG = "no_image.jpg";

	protected $TOP_IMG_DIR = "images/top/";

	protected $summary = "";

	public $cache;



    private $status = 1;



    final function __construct() { }



    function execute( CM_Request $request ) {

	

		//check if cache exists. if yes, require it and return true

		//$request -> setProperty( "command", $this );

		

		if (in_array($request -> getProperty("cat"), $this -> CACHABLE) && !in_array($request -> getProperty("subtype"),$this -> UNCACHABLE_SUBTYPES)) {

			$this -> cache = new cacher($request);

			if ($this -> cache -> CheckCache()) {

				//Util::Show("cache exists for " . $request -> getProperty("subtype"),"default.php");

				$this -> cache -> GetCache();

				$this -> status = 1;

				return true;

			}

		} else if (!$request -> getProperty("cat") && !$request -> getProperty("subtype")) {

			$this -> cache = new cacher($request);

			if ($this -> cache -> CheckCache()) {

				//Util::Show($request -> getProperty("subtype") . "is cached","default.php");

				$this -> cache -> GetCache();

				$this -> status = 1;

				return true;

			}

		}

		

		//Util::Show("shouldnt be here if cache exists","default.php");

		

		

		require_once ('../classes/db_basic.php');

		require_once ('../classes/db.php');

		if ($request -> getProperty("subtype") != "test" && $request -> getProperty("subtype") != "search") {

			require_once( '../classes/menus.php' );

		} else {

			require_once( '../classes/menus_new.php' );

		}

		

        $this -> status = $this -> doExecute( $request );

	    //mysql_close();

        

    }



    function getStatus() {

        return $this -> status;

    }



    /*static function statuses( $str='CMD_DEFAULT' ) {

        if ( empty( $str ) ) { $str = 'CMD_DEFAULT'; }

        print "handling status: $str\n";

        return self::$STATUS_STRINGS[$str];

    }*/

	

	protected function GetImage($dbImg, $alt, $pricing = false) {

		$height = $pricing?'215':'323';

		$img = $this -> TOP_IMG_DIR . "/" . $this -> DEFAULT_IMG;

		if (!empty($dbImg)) {

		 if (file_exists($this -> TOP_IMG_DIR . $dbImg)) {

		 	$img = $this -> TOP_IMG_DIR . $dbImg;

		 }

		}

		$img =  "<img src='/" . $img . "' width='680' height='$height' id='mainPic'  alt='$alt' />";

		return $img;

	}

	

	protected function GetRight($cat, $subtype) {

		$right_include = $this -> INCLUDE_DIR . $cat . "_" . $subtype . ".php";

		if ( !file_exists( $right_include ) ) {

			$right_include = $this -> INCLUDE_DIR . "default_right.php";

		}

		return $right_include;

	}



    abstract function doExecute( CM_Request $request );

}





?>

