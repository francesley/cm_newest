<?

class command_photos extends CM_Command {



	private $exterior;

	private $interior;

	private $lighting;

	private $decoration;

	private $wedding;

	private $party;

	private $corporate;

	private $smaller;

	private $large;

	private $awkward;

	private $all;

	private $categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");

	 

    function doExecute( CM_Request $request ) {

        

		$category = false;

		if ($request -> getProperty("category")) {

			$category = $request -> getProperty("category");

			if (!in_array($category,$this -> categories)) {

				$request->addFeedback( "Non-existent category: $category, from photos.php line 23");

				return false;

			}

			$this -> $category = "class='selected'";

		} else {

			$this -> all = "class='selected'";

		}

		$contentGetter = new photos_DBController($category);		

		$pageDetails = $contentGetter -> find();

		

		if (!$pageDetails) {

			$request->addFeedback( "No pageDetails,photos.php line 29");

			return 0;

		}

		

		

		require_once("shortBody.php");

		

		

		$facets = <<<EOF
<ul class="first">
<li><a href="/photos.htm" {$this -> all} >All</a></li>
</ul>
<ul><li><a href="/photos.htm?category=decoration" {$this -> decoration} >Decoration</a></li>
<li><a href="/photos.htm?category=lighting" {$this -> lighting} >Lighting</a></li>
<li><a href="/photos.htm?category=smaller" {$this -> smaller} >Smaller</a></li>
<li><a href="/photos.htm?category=large" {$this -> large} >Large</a></li>
<li><a href="/photos.htm?category=exterior" {$this -> exterior} >Exterior</a></li>
</ul>
<ul class="rightfacet"><li><a href="/photos.htm?category=wedding" {$this -> wedding} >Weddings</a></li>
<li ><a href="/photos.htm?category=party" {$this -> party} >Party</a></li>
<li><a href="/photos.htm?category=awkward" {$this -> awkward} >Awkward</a></li>
<li><a href="/photos.htm?category=interior" {$this -> interior} >Interior</a></li>
<li><a href="/photos.htm?category=corporate" {$this -> corporate} >Corporate</a></li>
</ul>
EOF;

		echo <<<EOQ
<img src="/images/top/thin_photos.jpg" width="918" height="70" id="smallpic" alt="Marquee nestling behind greenery"/>
<div id="facets">
<p id="switch">Find inspiration for your event</p>
<div id="pick">
<p><a href="#" id="picker">Filter by category</a><a href="javascript:void(0)" id="close"><img src="/images/down_arrow.png" width="12" height="6" alt="Show more" /></a></p>
<div id="facetlists">$facets</div>
</div>
</div>
<h1>{$pageDetails -> page_title}<a href="#video" class="videotitle">Videos &#9660</a></h1>
<div class='thumbs'>
{$pageDetails -> content}</div>
<h2 id='video'>Marquee videos</h2>
<div class="thumbs second">
<div class='thumb' ><a href="/video/drone.htm"><img src="/images/pages/video/droneThumb.jpg" alt="Drone marquee video" width='130' height='90' loading="lazy"/></a><a href="/video/drone.htm" class='thumbLink'>Drone marquee</a></div>
<div class='thumb' ><a href="/video/eighteenth.html"><img src="/images/pages/video/eighteenthThumb.jpg" alt="Video of a marquee for a twenty-first birthday party" width='130' height='90' loading="lazy"/></a><a href="/video/eighteenth.html" class='thumbLink'>Twenty-first birthday</a></div>
<div class='thumb' ><a href="/video/garden.htm"><img src="/images/pages/video/gardenThumb.jpg" alt="Garden wedding video" width='130' height='90' loading="lazy"/></a><a href="/video/garden.htm" class='thumbLink'>Country garden wedding</a></div>
	<div class='thumb' ><a href="/video/video.html"><img src="/images/pages/video/timelapseThumb.jpg" alt="Timelapse marquee video" width='130' height='90' loading="lazy"/></a><a href="/video/video.html" class='thumbLink'>Timelapse marquee</a></div>
</div>
EOQ;



		require_once( "end.php");

		return true;

    }

}



class command_photo extends CM_Command {

	

	private $exterior;

	private $interior;

	private $lighting;

	private $decoration;

	private $wedding;

	private $party;

	private $corporate;

	private $smaller;

	private $large;

	private $awkward;

	private $all;

	private $right_header = "All photos";

	private $right_header_link = "/photos.htm";

	private $categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");

	

    function doExecute( CM_Request $request ) {

		

        

		$selected = $request -> getProperty("category")?$request -> getProperty("category"):false;

		if ($selected == "all") {

			$selected = false;

		}

		

		if ($selected) {

			if (!in_array($selected,$this -> categories)) {

				$request->addFeedback( "Non-existent category: $selected, from photos.php line 113");

				return false;

			}

		}

		

		$contentGetter = new photo_DBController($request -> getProperty("subtype"), $selected);

		$pageDetails = $contentGetter -> find();

		

		if (!$pageDetails) {

			$request->addFeedback( "No pageDetails,photos.php line 103");

			return 0;

		}

		

		global $noIndex;

		if ($request -> getProperty("category")) {			

			$this -> right_header = ucfirst($request -> getProperty("category")) . " photos";

			$this -> right_header_link = "/photos.htm?category=" . $request -> getProperty("category");

		}

		

		$dropdownForm = <<<EOF
<ul class="first">
<li><a href="/photos.htm" {$this -> all} >All</a></li>
</ul>
<ul>
<li><a href="/photos.htm?category=lighting" {$this -> lighting} >Lighting</a></li>
<li><a href="/photos.htm?category=decoration" {$this -> decoration}  >Decoration</a></li>
<li><a href="/photos.htm?category=smaller" {$this -> smaller}  >Smaller</a></li>
<li><a href="/photos.htm?category=large" {$this -> large}  >Large</a></li>
<li><a href="/photos.htm?category=exterior" {$this -> exterior}  >Exterior</a></li>
</ul>
<ul class="rightfacet">
<li><a href="/photos.htm?category=interior" {$this -> interior}  >Interior</a></li>
<li><a href="/photos.htm?category=wedding" {$this -> wedding}  >Weddings</a></li>
<li><a href="/photos.htm?category=corporate" {$this -> corporate}  >Corporate</a></li>
<li ><a href="/photos.htm?category=party" {$this -> party}  >Party</a></li>
<li><a href="/photos.htm?category=awkward" {$this -> awkward}  >Awkward</a></li>
</ul>
EOF;



		require_once("shortBody.php");

		

		echo <<<EOF
<img src="/images/top/thin_photos.jpg" width="918" height="70" id="smallpic" alt="Large marquee nestling behind greenery"/>
<div id="facets">
<p id="switch">Find inspiration for your type of event</p>
<div id="pick">
<p><a href="#" id="picker">Filter by category</a><a href="javascript:void(0)" id="close"><img src="/images/down_arrow.png" width="12" height="6" alt="Show more" /></a></p>
<div id="facetlists">$dropdownForm</div>
</div>
</div>
<div id="right" class="thumbs">
<h3><a href="{$this -> right_header_link}">&laquo; {$this -> right_header}</a></h3>
<div id="thumbsDiv">{$pageDetails -> thumbs}<div class="unnec"></div></div>
</div>
<div id='photo'>
<div id='nextPrev'>{$pageDetails -> nextPrevLinks}</div>
{$pageDetails -> content}
</div>
EOF;

		

		require_once( "end.php");

		return true;

		

    }

}


?>