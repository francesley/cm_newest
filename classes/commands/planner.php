<?

class command_planner extends CM_Command {
    function doExecute( CM_Request $request ) {
	
		//saved plans
       $paramExtra = "";
	   $embedExtra = "";
	   if ($request -> getProperty("plan")) {
			$paramExtra = "<param name='flashvars' value='plan=" . $request -> getProperty("plan") . "' />";
			$embedExtra = " flashvars='plan=" . $request -> getProperty("plan") . "' ";
		} elseif ($request -> getProperty("quote")) {
			$paramExtra = "<param name='flashvars' value='quote=" . $request -> getProperty("quote") . "' />";
			$embedExtra = " flashvars='quote=" . $request -> getProperty("quote") . "' ";
		}
		
	   $contentGetter = new specials_DBController("planner");
		$pageDetails = $contentGetter -> find();
		
		if (!$pageDetails) {
			$request->addFeedback( "No pageDetails, planner.php line 18");
			return 0;
		}
		
		$content = "<img src=\"/images/top/venue_thin.jpg\" width=\"918\" height=\"70\" id=\"smallpic\" alt=\"Large marquee at marquee venue in Surrey\"/>";
					
					
		
		require_once("shortBody.php");
		echo $content;
		
        
		include_once( $this -> CONTENT_DIR . "planner.php" );
		
		
		require_once( "end.php");
		return true;
    }
}


?>
