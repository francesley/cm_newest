<?


class command_widget extends CM_Command {

    function doExecute( CM_Request $request ) {
	
		switch ($request -> getProperty("subtype")) {
			case "special":
				$width = "390px";
				$smallerWidth = "388px";
				break;
			case "narrow":
				$width = "300px";
				$smallerWidth = "298px";
				break;
			case "normal":
				$width = "400px";
				$smallerWidth = "398px";
				break;
			case "wide":
				$width = "500px";
				$smallerWidth = "498px";
				break;
			default:
				$width = "400px";
				$smallerWidth = "398px";
				break;
		}
		
		require_once("widget/widget_body.php");
		
		echo "</div></div></body></html>";
		return true;
    }
	
}


?>
