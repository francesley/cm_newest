<?



class command_areas extends CM_Command {

    private $longer = array("wedding","corporate","party_marquees","marquee_questions","essentials","testimonials","clients");



    function doExecute( CM_Request $request ) {       


		$this -> subtype = $request -> getProperty("subtype");

		

		$contentGetter = new areas_DBController($this -> subtype);		

		$pageDetails = $contentGetter -> find();

	

		

		//no db result

		//Util::ShowTrue($pageDetails, "pageDetails");

		if (!$pageDetails) {

			$request->addFeedback( "No pageDetails, text.php line 17");

			return false;

		}

		

		

		$f_include = $this -> CONTENT_DIR . "areas/" . $this -> subtype . ".inc";
		
		$rogue=false;

		$className = $request -> getProperty("cat") . "Menu";

		//Util::ShowDev($request -> getProperty("subtype"));


		
		//$pageDetails -> headerExtra .= "<meta name=\"robots\" content=\"noindex, nofollow\" />";
		$pageDetails -> bodyExtra .= " class='areas' ";
		$this -> GetLongMobPages($pageDetails);

		$subMenu = new $className();

		require_once("shortBody.php");

		

		$img = parent::GetImage($pageDetails -> imgName, $pageDetails -> alt, $rogue);

		

		$vectors = <<<EOQ
<div id='testi'>
{$pageDetails -> quote}{$pageDetails -> testimonial}
</div>
EOQ;

		

		if (isset($subMenu)) {

			$img = "<div id='pic'>\n$img\n$vectors\n" . $subMenu -> m_menu;

		} else {

			$img = "<div id='pic'>\n$img\n$vectors\n</div>";

		}

		echo $img;

		

		

		$right_include = parent::GetRight($request -> getProperty("cat"), $this -> GetRightCounty());		

		include_once( $right_include );

		

		$mobiClass = " class='" . $request -> getProperty("cat") . "' ";

		

		echo "<div id='text' $mobiClass><h1>" . $pageDetails -> page_title . "</h1>";

		

		//Util::Show($f_include);

		if ( file_exists( $f_include ) ) {

			include_once( $f_include );

		} else {

			//Util::Show($f_include);

			echo "</div>";

		}

		

		

		

		require_once( "end.php");

		//$this -> cacheFileName = $this -> cache -> currFile;

		return true;

    }

	

	function GetLongMobPages ($pageDetails) {

		if (in_array($this -> subtype,$this -> longer)) {

			$pageDetails -> bodyExtra .= " class='longmob' ";

		}

	}
	
	
	
	function GetRightCounty () {
		
		 $counties = array(
			 "guildford" => "surrey","cranleigh" => "surrey","dorking" => "surrey","epsom" => "surrey","farnham" => "surrey","walton" => "surrey",
			 "ham_high" => "northlondon","enfield" => "northlondon","harrow" => "northlondon","northwood" => "northlondon","romford" => "northlondon","southgate" => "northlondon",
			 "crawley" => "sussex","chichester" => "sussex","eastbourne" => "sussex","hastings" => "sussex","horsham" => "sussex","uckfield" => "sussex","brighton" => "sussex",
			 "croydon" => "southlondon","new-maldon" => "southlondon","richmond" => "southlondon","wimbledon" => "southlondon","richmond" => "southlondon","beckenham" => "southlondon","new-malden" => "southlondon",
			 "high-wycombe" => "bucks", "amersham" => "bucks", "princes-risborough" => "bucks", "stoke-mandeville" => "bucks", "waddesdon" => "bucks", "marlow" => "bucks","slough" => "bucks",
			 "oxfordshire" => "oxford","abingdon" => "oxford", "didcot" => "oxford","thame" => "oxford","henley" => "oxford","witney" => "oxford",
			 "berkhamsted" => "herts","bishops-stortford" => "herts","st-albans" => "herts","hertford" => "herts","st-albans" => "herts","welwyn" => "herts","watford" => "herts",
			 "luton" => "beds","baldock" => "herts","olney" => "bucks","milton-keynes" => "bucks","bedford" => "beds",
			 "aldershot" => "hampshire","alton" => "hampshire","basingstoke" => "hampshire","farnborough" => "hampshire","odiham" => "hampshire","southampton" => "hampshire",
			 "maidstone" => "kent","bromley" => "kent","sevenoaks" => "kent","tunbridge-wells" => "kent","tonbridge" => "kent","westerham" => "kent","weybridge" => "surrey"
		 );
		
		
		if (!array_key_exists($this -> subtype, $counties)) {

			return $this -> subtype; 

		 } else {

			return $counties[$this -> subtype];
			
		}
	}

}





?>

