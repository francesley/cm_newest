<?


class command_video extends CM_Command {
	
	//private video_name;
	
	 function doExecute( CM_Request $request ) {
		//Util::Show($request -> getProperty("subtype"));
		
		$contentGetter = new video_DBController($request -> getProperty("subtype"));
		$pageDetails = $contentGetter -> find();
		
		if (!$pageDetails) {
			$request->addFeedback( "No pageDetails, video.php line 14");
			return 0;
		}
		
		$f_include = $this -> CONTENT_DIR . $request -> getProperty("cat") . "_" . $request -> getProperty("subtype") . ".inc";	
		
		require_once("shortBody.php");
		
		echo <<<EOF
			<img src="/images/top/thin_photos.jpg" width="918" height="70" id="smallpic" alt="Large marquee nestling behind greenery"/>
			<div id="right" class="thumbs">
				<h3><a href="/photos.htm#video">&laquo; All videos</a></h3>
				{$pageDetails -> thumbs}
			</div>
				<div id='photo'>
			
EOF;

		if ( file_exists( $f_include ) ) {
			include_once( $f_include );
		} else {
			//Util::Show($f_include);
			echo "</div></div>";
		}
		 echo "</div>";
		
		/*echo "<div id='text'><h1>" .  . "</h1>";		
		require_once( $this -> CONTENT_DIR . "panorama.php" );*/		
		require_once( "end.php");
		return true;
    }
	
}


?>
