<?php


class DB {

	private $conn;
	private static $instance;
	
	private function __construct() {}

	//this isnt implemented right. should be able to get conn from instance without connecting again
	public static function getInstance() {
		if (empty(self::$instance)) {
			self::$instance = new DB();
			self::$instance -> DBH = self::$instance -> GetDBH ();
			//self::$instance -> conn = self::$instance -> GetConn ();
			return self::$instance -> DBH;
                        //TODO make dbarray a private prop so its available everywhere here
		} else {
			return self::$instance -> DBH;
		}
	}
	
	private function GetDBH () {
		
		global $dbArray;
		
			
		try {
			$DBH = new PDO("mysql:host=$dbArray[server];dbname=$dbArray[name];charset=utf8", $dbArray["user"], $dbArray["pw"]);
			//$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );
			//$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
			$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );			
			$DBH->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		catch(PDOException $e) {
			//echo "here";
			
			  /*echo $e->getMessage();
			  exit();*/
			$this -> Irrecoverable($e->getMessage(), true);
		}
		
		
		
		return $DBH;
	}
	
	private function GetConn () {
		
		$dbArray = Registry::Instance() -> GetDB();

		$conn = @mysql_connect($dbArray["server"],$dbArray["user"],$dbArray["pw"]);
		if (!is_resource($conn)) {
			$this -> Irrecoverable ("cant connect, db_basic.php 28 " . $dbArray["server"] . " " . $dbArray["user"] . " " . $dbArray["pw"], true);
		}
		
		
		if (!$db = @mysql_select_db($dbArray["name"],$conn)){
			$this -> Irrecoverable ("cant select db " . $dbArray["name"] . ", mysqlerror is " . mysql_error() . ", db_basic.php line33", true);
			
		} else {
			//$self::$instance -> conn = $conn;
			mysql_set_charset('utf8',$conn);
			return $conn;
		}
		
	}
	
	private function Irrecoverable($error_msg, $report = false) {
			
			$reg = Registry::Instance();
			$request = $reg -> GetRequest();
			//Util::Show("no rows in result: $sql","from DBController line 71");
			$request->addFeedback( $error_msg);
			if ($report) {
				Util::SendFeedback($request -> getFeedbackString());
				Util::Set500();				
			}
	}

}

abstract class DBControllerNew {
 	protected $DBH;
        protected $subtype;
        protected $cat;
	
	
	function __construct() {
		if (empty($this -> DBH)) {
				$this -> DBH = DB::getInstance();
		}
		
		global $includesfilepath;
		require_once ($includesfilepath . "classes/pages.php");
	}
	
	
	protected function doStatement($sql) {
		//Util::Show($sql);
            try {
                $stmt = $this -> DBH -> prepare ($sql );
				if ($this -> subtype) {
                	$stmt->execute(array($this -> cat, $this -> subtype));
				} else {
					$stmt->execute(array($this -> cat));
				}
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
				/*if (count($row) > 0) {
					foreach ($row as $dataMaterial) {
						echo "There's a result";
					}
				} else {
					echo "NO RESULT FOUND";
				}
				exit();*/
            }
            catch(PDOException $e) {
                    $this -> Irrecoverable ($e->getMessage());
                    exit();
                    //TODO need to think about 404s
                    //$this -> Irrecoverable($e->getMessage(), true)
            }
            return $row;
	}
	
	protected function doNoParamsStatement($sql) {
            
            try {
                $stmt = $this -> DBH -> prepare ($sql );
				$stmt->execute();
                $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            catch(PDOException $e) {
                     $this -> Irrecoverable ($e->getMessage());
                    exit();
                    //TODO need to think about 404s
                    //$this -> Irrecoverable($e->getMessage(), true)
            }
            return $row;
	}
	
	protected function doMultipleRowStatement($sql) {
		//Util::Show( $sql );
            try {
                $stmt = $this -> DBH -> prepare ($sql );
				if (isset($this -> subtype)) {
                	$stmt->execute(array($this -> subtype));
				} else {
					$unfetched_row = $stmt->execute();
				}
					$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//var_dump($row);
                
            } catch (PDOException $ex) {
                    $this -> Irrecoverable ( "error: " . $ex->getMessage());
                    exit();
                    //TODO need to think about 404s
                    //$this -> Irrecoverable($e->getMessage(), true)
            }
			return $row;
	}
	
	
	
	protected function doSimpleStatement($sql) {
		//for brochure downloads
		
		 try {
				//$stmt = $this -> DBH -> prepare ( );
				$stmt = $this -> DBH -> query($sql);
				$stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
			 	
			 	
		  } catch (PDOException $ex) {
			 $this -> Irrecoverable ( "error: " . $ex->getMessage());
                    exit();
			  
		  }
		
		return $row;
		
	}
	
	function find () {
		return $this -> doFind();
	}
	
	function load ( $result, $result2 = false, $result3 = false, $result4 = false  ) {
          
		if ( !$result2 && !$result3) {
			$obj = $this -> doLoad( $result );
			//Util::Show("here");
		} elseif (!$result3) {
			$obj = $this -> doLoad( $result, $result2 );
			//Util::Show("here2");
		} elseif (!$result4) {
			$obj = $this -> doLoad( $result, $result2, $result3);
		} else {
                    $obj = $this -> doLoad( $result, $result2, $result3, $result4);
                }
		return $obj;
	}
	
	
        
        protected function Irrecoverable($error_msg, $report = true) {
			/*echo $error_msg;
			exit;*/
			
			$reg = Registry::Instance();
			$request = $reg -> GetRequest();
			//Util::Show("no rows in result: $sql","from DBController line 213");
			$request->addFeedback( $error_msg);
			if ($report) {
				Util::SendFeedback($request -> getFeedbackString());
				Util::Set500();				
			}
	}
}


abstract class DBController {
 	protected $conn;
	
	
	function __construct() {
		//echo empty($this -> conn)?"empty":"not";
		if (empty($this -> conn)) {
				$this -> conn = DB::getInstance();
		}
		//return $this -> conn == false?false:true;
	}
	
	
	protected function doStatement($sql) {
	
		if ($result = mysql_query($sql, $this -> conn)) {
		
			$row = mysql_fetch_assoc($result);
			
			if (mysql_num_rows($result) == 0) {
				$this -> Irrecoverable ("no rows in result: $sql from DBController line 78");
				return false;
			}
			if (! is_array($row)) {
				$this -> Irrecoverable ("result not an array sql: $sql from DBController line 82");
				//Util::Show("result not an array: $sql","from DBController line 74");
				return false;
			}
			return $row;
						
		} else {
		//TODO need to think about 404s
			$this -> Irrecoverable ("no result: $sql from DBController line 88");
			//Util::Show(mysql_error(),"mysql_error from DBController\n$sql");
			return false;
		}
	}
	
	protected function doMultipleRowStatement($sql) {
		//Util::Show( $sql );
		$result = mysql_query($sql, $this -> conn);
		if ($result) {

                    if (mysql_num_rows($result) == 0) {
                            $this -> Irrecoverable ("no rows in result<br>$sql","from multiplerow in DBController line 101");
                            return false;
                    }
                    //Util::Show($result);
                    return $result;

		} else {
		//TODO need to think about 404s
			$this -> Irrecoverable (mysql_error(),"mysql_error from DBController line 110\n$sql");
			return false;
		}
	}
	
	protected function doActionStatement($sql) {
		//for brochure downloads
		$result = mysql_query($sql, $this -> conn);
		if (!$result) {
			 //$this -> Irrecoverable ("sql: $sql error: " . mysql_error($this -> conn), "mysql_error from actiontatement in DBController");
             return "error: " . mysql_error($this -> conn);
		}

		//cos result should be false if error
		return $result;
	}
	
	function find () {
		return $this -> doFind();
	}
	
	function load ( $result, $result2 = false, $result3 = false, $result4 = false  ) {
            //TODO decide if line below will be needed
		
		if ( !$result2 && !$result3) {
			$obj = $this -> doLoad( $result );
		} elseif (!$result3) {
			$obj = $this -> doLoad( $result, $result2 );
		} elseif (!$result4) {
			$obj = $this -> doLoad( $result, $result2, $result3);
		} else {
                    $obj = $this -> doLoad( $result, $result2, $result3, $result4);
                }
		return $obj;
	}
	
	protected function doCheckingStatement($sql) {
	
		if ($result = mysql_query($sql, $this -> conn)) {
		
			$row = mysql_fetch_assoc($result);
			
			if (mysql_num_rows($result) == 0) {
				//$this -> Irrecoverable ("no rows in result: $sql from DBController line 78");
				return false;
			}
			if (! is_array($row)) {
				$this -> Irrecoverable ("result not an array sql: $sql from DBController line 138");
				//Util::Show("result not an array: $sql","from DBController line 74");
				return false;
			}
			return $row;
						
		} else {
		//TODO need to think about 404s
			$this -> Irrecoverable ("no result: $sql from DBController line 146");
			//Util::Show(mysql_error(),"mysql_error from DBController\n$sql");
			return false;
		}
	}
	
	protected function Irrecoverable($error_msg, $report = false) {
                        //Util::Show("irrecoverable");
                        $req = Registry::Instance() -> GetRequest();
                        $req -> addFeedback( $error_msg);
                        Util::SendFeedback($error_msg);
						Util::Set404();
						//echo $error_msg;
			/*
			if ($report) {
				Util::SendFeedback($request -> getFeedbackString());
				Util::Set500();				
			}*/
			//var_dump($e);
			/*if ($sqlerror) {
				Util::Show($sqlerror,"sqlerror");
			} else {
				Util::Show($sqlerror,"no sql error");
			}
			Util::Show($e -> getMessage(),"message");                 // Exception message 
			Util::Show($e -> getCode(),"code");                    // User-defined Exception code
			Util::Show($e -> getFile(),"file");                    // Source filename
			Util::Show($e -> getLine(),"line");                    // Source line
		    Util::Show($e -> getTrace(),"trace array");                   // An array of the backtrace()
			Util::Show($e -> getTraceAsString(),"trace as string");*/
	}
	
}




?>
