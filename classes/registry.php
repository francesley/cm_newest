<?php

class Registry {
	private static $instance;
	private $request;
	
	static function Instance() {
		if (!self::$instance) {
			self::$instance = new self();			
		}
		return self::$instance;
	}
	
	function GetRequest() {
		return $this -> request;
	}
	
	function SetRequest(CM_Request $request) {
		$this -> request = $request;
	}


}

?>
