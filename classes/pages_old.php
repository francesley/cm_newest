<?php





class details {



	public $page_title;

	public $meta_title;

	public $metadesc;

	public $metakey;

	public $f_include;

	public $main_class;

	public $testimonial;

	public $bodyExtra; //hack when main_class not enough; set in whichever command

	public $headerExtra;

	public $html_dec;

	public $footerExtra;

	public $alt;

	public $quote = "";





	function __construct ($title, $m_title, $m_desc, $m_key) {

		//Util::ShowDev ("a");

		//echo $title;

		$this -> page_title = str_replace("&AMP;","&amp;",$title);

		$this -> meta_title = $m_title;

		$this -> metadesc = $m_desc;

		$this -> metakey = $m_key;

		$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\">";

		$this -> footerExtra .= "<script src='/scripts/search_min.js'></script>";

	}

	

	function GetTestimonial ($testimonial, $testifier) {

		//Below = hack cos old site had date as well for testifier. If I ever get rid of all dates, can get rid of conditional return

		if (!empty($testifier)) {

			$testifierArray = explode(", ", $testifier);

			return count($testifierArray) == 3?"<div><span id='testimonial'><a href='/company/testimonials.htm' title='Find out what other people say about us'>" . $testimonial . "</a></span><span>" . $testifierArray[1] . ", " . $testifierArray[2] . "</span></div>":"<div><span id='testimonial'><a href='/company/testimonials.htm' title='Find out what other people say about us'>" . $testimonial . "</a></span><span>" . $testifierArray[0] . ", " . $testifierArray[1] . "</span></div>";

		} else {

			$this -> quote = "";

			return "<div><span>Marquee hire in London and<br/>the South East<br/>for 25 years</span></div>";

		}

	

	}

	

	protected function CheckAlt ($alt) {

		$this -> alt = !empty($alt)?$alt:"Marquee hire from County Marquees";

	}



}





class textDetails extends details{



	private $jquery;



	function __construct ($pageArray, $widePage) {

		//echo "got into headerDetails";

		//echo $title;

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);		

			$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);		

			$this -> imgName = $pageArray["photo"];

			$this -> CheckAlt ($pageArray["alt"]);

			if ($widePage) {

				$this -> bodyExtra = " id='wide'";

				if ($pageArray["page_title"] == "Marquee testimonials") {

					$this -> main_class = " class='testimonials' ";

				}

			}				

		}		

	}

	

class formsDetails extends details{

	

	function __construct ($pageArray) {		

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		$this -> main_class = "class='forms' ";

		$this -> footerExtra .= "<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\" ></script>\n";

		

		$reg = Registry::Instance();

		$request = $reg -> GetRequest();

		

		switch ($request -> getProperty("subtype")) {

			case "questions":

				$this -> footerExtra .= "<script  src=\"/scripts/validate_questions.js\" async></script>\n";

				break;

			case "quotation":

				$this -> footerExtra .= "<script src=\"/scripts/validate_quote.js\" async></script>\n<script src='/scripts/color.js' type='text/javascript'></script>";

				$this -> headerExtra .= "<style type='text/css'>h1{margin-bottom:1.5em;}</style>";

				break;

			case "quote":

				$this -> bodyExtra = " id='quote'";

				$this -> footerExtra .= "<script src=\"/scripts/quote.js\" async></script>\n<script src='/scripts/color.js' type='text/javascript'></script>";

				break;

			case "quote_test":

				//$this -> headerExtra = "<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>\n<META HTTP-EQUIV='PRAGMA' CONTENT='NO-CACHE'>\n";

				if ($_COOKIE["auth"] = "staffmember") {

					$this -> headerExtra .= "<meta name='robots' content='NOINDEX' />";

				}

				$this -> bodyExtra = " id='quote'";

				$this -> footerExtra .= "<script src=\"/scripts/quote.js\"></script>\n<script src='/scripts/color.js' type='text/javascript'></script>";

				break;				

			default:

				$this -> footerExtra .= "<script src=\"/scripts/validate.js\"></script>";

		}

				

		$this -> imgName = $pageArray["photo"];

		$this -> CheckAlt ($pageArray["alt"]);

		

	}

		

}

	

	

	

	

class pricingDetails extends details{



	function __construct ($pageArray) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		$this -> main_class = "class='pricing' ";

		$this -> imgName = $pageArray["photo"];

		$this -> CheckAlt ($pageArray["alt"]);

		

			

	}

	

}





class text_venues_Details extends details{



	public $headerExtra;

	private $countiesArray = array("lon" => "London","beds" => "Bedfordshire", "berks" => "Berkshire", "bucks" => "Buckinghamshire", "eSussex" => "East Sussex", "wSussex" => "West Sussex", "essex" => "Essex", "surrey" => "Surrey", "herts" => "Hertfordshire", "kent" => "Kent", "oxford" => "Oxfordshire", "hants" => "Hampshire");



	function __construct ($pageArray,$county = false) {

		

		if (!$county || $county == "all" ) {

			parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

			$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]); //TODO alter this for all sites			

			$this -> CheckAlt ($pageArray["alt"]);

		} else {

			if (!isset($this -> countiesArray[$county])) {

				$this -> page_title = "";

				return;

			}

			

			$county = $this -> countiesArray[$county];

			$this -> page_title = "Marquee Venues in " . $county;

			$this -> meta_title = "Marquee Venues " . $county . " | Marquee Hire " . $county;

			$this -> metadesc = "Event and wedding marquee venues in " . $county . ". Land for marquees, showing price, size and description. Marquee hire ". $county . " from County Marquees";

			$this -> metakey = "marquee, venues,  " . $county . ", wedding, venue, marquees, site, sites, UK, land, marquee hire";

			$this -> alt = "Marquee Hire " . $county;

			$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://opengraphprotocol.org/schema/\" xmlns:fb=\"http://www.facebook.com/2008/fbml\" lang=\"en\">";

		$this -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com$_SERVER[REQUEST_URI]\"/>";

		$this -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees'/>";

			//$this -> quote = " style='background-image:none' ";

			//$this -> testimonial =  "<div><span id='testimonial'>Event and wedding marquee venues in " . $county . "</span></div>";

		}

		//$this -> main_class = "class='pricing' ";

		$this -> imgName = $pageArray["photo"];

		$this -> footerExtra .= "\n<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></script>\n<script language=\"javascript\" src=\"/scripts/facets.js\"></script><script src=\"/scripts/jquery.listen-min.js\"></script><script src='/scripts/search_min.js'></script>";

	}

	

}







class realDetails extends details{



	public $content;



	function __construct ($pageArray, $photoResult) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		//$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		$this -> main_class = "class='photos real' ";

		

		$this -> GetPhotos ($photoResult);

	}

	

	private function GetPhotos ($photoResult) {


			foreach($photoResult as $data){				

				$imgSrc = file_exists("images/case/home/$data[subtype].jpg")?"images/case/home/$data[subtype].jpg":"/images/photos/no_imageThumb.png";

				/*if ($this -> photoCounter%6 == 0) {

					$this -> style = " style='margin-right:0' ";

				}*/

				//if ($data["subtype"] != "ball" || $data["subtype"] != "barmitzvah") {

					$this -> content .= "<div class='thumb' ><a href=\"case_studies/$data[subtype].htm\"><img src=\"$imgSrc\" alt=\"$data[page_title]\" width='230' height='154'/></a><a href=\"case_studies/$data[subtype].htm\" class='thumbLink'>$data[page_title]</a></div>\n";

				/*} else {

					$this -> content .= "<div class='thumb' ><a href=\"case_studies/$data[subtype].html\"><img src=\"$imgSrc\" alt=\"$data[page_title]\" width='230' height='154'/></a><a href=\"case_studies/$data[subtype].html\" class='thumbLink'>$data[page_title]</a></div>\n";

				}*/

				

			}

		}

	

}







class plannerDetails extends details{



	function __construct ($pageArray) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		//$this -> footerExtra .= "<script src=\"/scripts/planner.js\" type=\"text/javascript\"></script>";

		$this -> bodyExtra = " id='planner'";

		if (isset($_REQUEST["plan"])) {

			$this -> headerExtra = "<meta name='robots' content='NOINDEX' />";

		}

		$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://opengraphprotocol.org/schema/\" xmlns:fb=\"http://www.facebook.com/2008/fbml\" lang=\"en\">";

		$this -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com$_SERVER[REQUEST_URI]\"/>";

		$this -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees'/>";

		$this -> headerExtra .= "\n<script src=\"https://connect.facebook.net/en_US/all.js\"  async></script>\n";

		$this -> headerExtra .= "\n<script>

function flashExists() {

var hasFlash = false;

try {

var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');

if (fo) {

hasFlash = true;

}

} catch (e) {

if (navigator.mimeTypes && navigator.mimeTypes['application/x-shockwave-flash'] != undefined &&  navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {hasFlash = true;}

}

if (!hasFlash) {window.location.href='https://www.countymarquees.com/equipment/noflash.htm';}

}

flashExists();	

</script>\n";

		$this -> footerExtra .= <<<EOQ

<script src='https://connect.facebook.net/en_US/all.js'></script>

<script  type="text/javascript">

  FB.init({status: true, cookie: true, xfbml: true});				

</script>

<script type="text/javascript" src="https://apis.google.com/js/plusone.js" async></script>

EOQ;

			

	}

	

}

	

	

class venuesDetails extends details{

	

	private $map_scripts = "\n<script src=\"https://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAtW7VFb3GTqUuX1Tudp54RhQP6tYg3satRrG4mGhRim9YEQGGWBT1rM8k1fauC_krUektXVyyI7MSCg\" type=\"text/javascript\"></script>\n<script src=\"/maps/scripts/map_functions_packed.js\" type=\"text/javascript\"></script>\n

<!--[if lt IE 7]>

		<script src=\"maps/scripts/pngfix.js\" type=\"text/javascript\"></script>

		<![endif]-->

<script type=\"text/javascript\">

    //<![CDATA[

    function load() {

		if (GBrowserIsCompatible()) {	  

			init();					

		} else {

			alert(\"Sorry, Google Maps are not compatible with your browser\");

		}

    }

    //]]>	

    </script>

<link href=\"/maps/maps_min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />";



	function __construct ($pageArray) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		//$this -> content = $pageArray["content"];

		$this -> main_class = "class='venues' ";

		

		$this -> headerExtra = $this -> map_scripts;

		

		$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://opengraphprotocol.org/schema/\" xmlns:fb=\"http://www.facebook.com/2008/fbml\" lang=\"en\">";

		$this -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com$_SERVER[REQUEST_URI]\"/>";

		$this -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees'/>";

		$this -> footerExtra .= "\n<script  type=\"text/javascript\" src=\"https://connect.facebook.net/en_US/all.js\"></script>\n";

		$this -> footerExtra .= <<<EOQ

		<script  type="text/javascript">

		  FB.init({status: true, cookie: true, xfbml: true});

		</script>

        <script type="text/javascript" src="https://apis.google.com/js/plusone.js" async></script>

EOQ;

	if (isset($_REQUEST["mode"])) {

			$this -> headerExtra = "\n<meta name='robots' content='NOINDEX' />\n" . $this -> map_scripts;

		}

	$this -> bodyExtra = "onload=\"load()\" onunload=\"GUnload()\"";



		}

		

	}	

	

class newvenuesDetails extends details{

	

	private $map_scripts = "\n<script src=\"https://maps.googleapis.com/maps/api/js\" type=\"text/javascript\"></script>\n	
	<script type=\"text/javascript\" src=\"/maps/scripts/markerclusterer.js\"></script>
	<script src=\"/maps/scripts/mapscript.js\"></script>\n
	
	

<!--[if lt IE 7]>

		<script src=\"maps/scripts/pngfix.js\" type=\"text/javascript\"></script>

		<![endif]-->

<script type=\"text/javascript\">

    //<![CDATA[

    function load() {

		if (GBrowserIsCompatible()) {	  

			init();					

		} else {

			alert(\"Sorry, Google Maps are not compatible with your browser\");

		}

    }

    //]]>	

    </script>

<link href=\"/maps/maps_min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />";



	function __construct ($pageArray) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		//$this -> content = $pageArray["content"];

		$this -> main_class = "class='venues' ";

		

		$this -> headerExtra = $this -> map_scripts;

		

		$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://opengraphprotocol.org/schema/\" xmlns:fb=\"http://www.facebook.com/2008/fbml\" lang=\"en\">";

		$this -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com$_SERVER[REQUEST_URI]\"/>";

		$this -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees'/>";

		$this -> footerExtra .= "<script>
		google.maps.event.addDomListener(window, 'load', speedTest.init);
    </script>";

		$this -> footerExtra .= <<<EOQ

		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> 
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js" async></script>

EOQ;

	if (isset($_REQUEST["mode"])) {

			$this -> headerExtra = "\n<meta name='robots' content='NOINDEX' />\n" . $this -> map_scripts;

		}

	$this -> bodyExtra = "";



		}

		

	}	

	

class homeDetails extends details{



	public $content;



	function __construct ($pageArray) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		//$this -> content = $pageArray["content"];

		$this -> main_class = "class='home' ";		

		$this -> headerExtra = "<meta name='robots' content='NOODP NOYDIR'/>";

		$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://opengraphprotocol.org/schema/\" xmlns:fb=\"http://www.facebook.com/2008/fbml\" lang=\"en\">";

		$this -> headerExtra .= "\n<meta property='og:url' content='https://www.countymarquees.com/' />";

		$this -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees' />\n<meta property='fb:admins' content='766316702' />";

		$this -> footerExtra .= <<<EOQ

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>

			<script src="/scripts/jquery.cycle.pack.js" type="text/javascript"></script>

			<script src="/scripts/home.js"></script>

			<!--[if lte IE 6]>

				<script  defer>

				  DD_belatedPNG.fix('#testi');

				</script>

			<![endif]-->

EOQ;

		$this -> footerExtra .= "\n\n";

		}

		

	}

	

	

	

class sitemapDetails extends details{



	public $content;

	private $sitemap_categories = array("case_studies" => "Case studies", "help" => "Marquee information");

	private $sitemap_formpages = array("Add your venue" => "venues","Contact County Marquees" => "company", "Get a free no obligation marquee quote" => "pricing", "Marquee hire questions" => "help", "Marquee work for County Marquees" => "company", "Large event marquee" => "case_studies");

	private $help ;

	private $pricing;

	private $company;

	private $venues;

	private $events;

	private $case_studies;

	private $photos;



	function __construct ($pageArray, $allPagesResult, $photosResult) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);		

			$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);		

			$this -> imgName = $pageArray["photo"];

			$this -> GetPages ($allPagesResult);

			$this -> GetPhotos ($photosResult);

		

		}

		

		private function GetPages ($allPagesResult) {

        

        	//09-01-26    71041790  mr p.ruane

			/*var_dump($allPagesResult);
				exit();*/
			
			foreach($allPagesResult as $data){
				
				if (array_key_exists($data["page_title"], $this -> sitemap_formpages)) {

					$data["cat"] = $this -> sitemap_formpages[$data["page_title"]];

				}

				$data["page_title"] = ucfirst($data["page_title"]);

				
				$temp = $data["cat"];
				
				if (property_exists("sitemapDetails", $data["cat"])) {
					$this -> $temp .= "\n\t<li><a href=\"/$data[url]\">$data[page_title]</a></li>\n";
				}

				//$this -> content .= "\n\t\t<li><a href=\"/$data[url]\">$data[page_title]</a></li>\n";

				/*$this -> photoCounter++;

				$this -> style = "";*/

			}

			

			$this -> content = <<<EOQ

				<h3>Real marquees</h3>

					<ul class="long">

                    <li><a href="/real.htm">All real marquees</a></li>

                    {$this -> case_studies}</ul>

				<h3>Company</h3>

					<ul class="long">{$this -> company}</ul>

				<h3>About marquee events</h3>

					<ul class="long">{$this -> events}</ul>

				<h3>Marquee information</h3>

					<ul class="long">{$this -> help}</ul>

				<h3>Pricing</h3>

					<ul class="long"><li><a href="/pricing/quote.htm">Online quick quote</a></li>{$this -> pricing}</ul>

				<h3>Marquee planner and videos</h3>

					<ul class='long'>

						<li><a href='/movies/planner.html'>Marquee planner</a></li>

                        <li><a href='/video/drone.htm'>Drone's eye vie of Kate and Ali's wedding marquee</a></li>

						<li><a href='/video/eighteenth.htm'>Eighteenth birthday party marquee</a></li>

                        <li><a href='/video/wedding_marquee.html'>Wedding marquee video</a></li>

                        <li><a href='/video/garden.htm'>Garden wedding video</a></li>

						<li><a href='/video/video.html'>Time-lapse video (requires Flash)</a></li>

					</ul>			

				

				<h3>Marquee venues</h3>

				<ul class="long bottom">

					<li><a href="/venues.htm">Marquee venues finder</a></li>

                    <li><a href="/venues/sites.htm">Marquee sites list</a></li>

					<li><a href="/venues/lon.htm">Marquee venues London</a></li>

					<li><a href="/venues/surrey.htm">Marquee venues Surrey</a></li>

					<li><a href="/venues/wSussex.htm">Marquee venues West Sussex</a></li>

					<li><a href="/venues/eSussex.htm">Marquee venues East Sussex</a></li>

					<li><a href="/venues/hants.htm">Marquee venues Hampshire</a></li>

					<li><a href="/venues/kent.htm">Marquee venues Kent</a></li>

					<li><a href="/venues/bucks.htm">Marquee venues Buckinghamshire</a></li>

					<li><a href="/venues/herts.htm">Marquee venues Hertfordshire</a></li>

					<li><a href="/venues/oxford.htm">Marquee venues Oxfordshire</a></li>

					<li><a href="/venues/berks.htm">Marquee venues Berkshire</a></li>

					<li><a href="/venues/beds.htm">Marquee venues Bedfordshire</a></li>

					<li><a href="/venues/essex.htm">Marquee venues Essex</a></li>					

					<li><a href="/venues/add_venue.htm">Add your marquee venue</a></li>				

				</ul>

				

				</div>\n

EOQ;



		/*<li><a href="/venues/widget.htm">Put a marquee venue widget on your website</a></li>*/

					

		}

		

		private function GetPhotos ($photosResult) {

			

			$this -> content .= "<div class='links rightlinks'>\n<h3>Marquee photo galleries</h3>\n";

			$this -> content .= "<ul class='long'>{$this -> photos}</ul><h3>Marquee photos</h3>\n<ul class='long bottom'>";

			
			foreach($photosResult as $data){
			//while ($data = mysql_fetch_assoc($photosResult)) {					

				$this -> content .= "\n\t\t<li><a href=\"/photos/$data[url].htm\">$data[page_title]</a></li>\n";

			}

			

			$this -> content .= "</ul>";

			

		}

		

		

	}

    

	

class case_studiesDetails extends details{

	

	public $thumbs;

	public $nextPrevLinks;

    protected $nextPhoto = "";	

	protected $selectedLink = "";



	function __construct ($pageArray, $thumbsResult) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);		

			$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);		

			$this -> imgName = $pageArray["photo"];

			$this -> CheckAlt ($pageArray["alt"]);

			$this -> GetPhotos ($thumbsResult);	//TODO SHOULD CHECK HERE FOR NO RESULT

		}

        

    private function GetPhotos ($thumbsResult) {

	

		$selected = "";

		$nextPrevLinks = "";

		$needNext = false;

		$lastURL = "";

		$current = "";

		$counter = 0;

		$first;

		$selectedLink;

		

		
		foreach($thumbsResult as $data){

			if (! is_array($data)) {

				Util::Show("result not an array<br>$sql","from case_studiesDetails GetPhotos()");

				return false;

			}

			 

			$current = "/case_studies/" . $data["subtype"] . ".htm";

			if ($counter == 0) {

				$first = $current;

				$counter++;

			}

			

			if ($current ==  $_SERVER["REQUEST_URI"]) {				

				$selected = " selected";

				$this -> selectedLink = $current;

				$nextPrevLinks = $lastURL != ""?"<a href='$lastURL' class='previous'>Previous</a> | ":"";

				$needNext = true;

			} else {

				$selected = "";

				if ($needNext) {

					$nextPrevLinks = $nextPrevLinks . "<a href='$current'>Next</a>";

					$needNext = false;

					$this -> nextPhoto = $current;

				}

			}

			

			$lastURL = $current;					

			$imgSrc = "/images/case/home/$data[subtype]Sm.jpg";//?"/images/case/home/$data[subtype]Sm.jpg":"/images/photos/no_imageThumb.png";			

			$this -> thumbs .= "<div class=\"smThumb$selected\"><a href=\"$current\"><img src=\"$imgSrc\" alt=\"$data[page_title]\" title=\"$data[page_title]\" width='72' height='50' /></a></div>";

		}

		

		if ($this -> nextPhoto == "") {

			$this -> nextPhoto = $first;

			$nextPrevLinks = $nextPrevLinks . "<a href='$first'>Next</a>";

		}

		if ($this -> selectedLink == $first) {

			$nextPrevLinks = "<a href='$current' class='previous'>Previous</a> | " .  $nextPrevLinks;

		}	

		$this -> nextPrevLinks = "<a href='/real.htm'>All</a> | " . $nextPrevLinks;

		

	  }

	}	



class photosDetails extends details{



	public $content;



	function __construct ($pageArray, $photoResult, $category) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> testimonial = $this -> GetTestimonial ($pageArray["testimonial"], $pageArray["testifier"]);

		$this -> main_class = "class='photos' ";

		$this -> footerExtra .= "<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\" language=\"javascript\"></script>\n<script language=\"javascript\" src=\"/scripts/facets.js\" type=\"text/javascript\"></script>";

		

		if (!$category) {

			$this -> GetPhotos ($photoResult);

		} else {

			$this -> GetCategoryPhotos ($photoResult, $category);

		}

	}

	

	private function GetPhotos ($photoResult) {

			
			foreach ($photoResult as $data) {
								

				$imgSrc = file_exists("images/photos/$data[image]Thumb.jpg")?"/images/photos/$data[image]Thumb.jpg":"/images/photos/no_imageThumb.png";

				/*if ($this -> photoCounter%6 == 0) {

					$this -> style = " style='margin-right:0' ";

				}*/

				$this -> content .= "<div class='thumb' ><a href=\"photos/$data[url].htm\"><img src=\"$imgSrc\" alt=\"$data[page_title]\" width='130' height='90'/></a><a href=\"photos/$data[url].htm\" class='thumbLink'>$data[short_desc]</a></div>\n";

				/*$this -> photoCounter++;

				$this -> style = "";*/

			}

		}

		

		private function GetCategoryPhotos ($photoResult, $category) {

			
			foreach ($photoResult as $data) {
								

				$imgSrc = file_exists("images/photos/$data[image]Thumb.jpg")?"/images/photos/$data[image]Thumb.jpg":"/images/photos/no_imageThumb.png";

				/*if ($this -> photoCounter%6 == 0) {

					$this -> style = " style='margin-right:0' ";

				}*/

				$this -> content .= "<div class='thumb' ><a href=\"photos/$data[url].htm?category=$category\" ><img src=\"$imgSrc\" alt=\"$data[page_title]\" width='130' height='90'/></a><a href=\"photos/$data[url].htm?category=$category\" class='thumbLink' >$data[short_desc]</a></div>\n";

				/*$this -> photoCounter++;

				$this -> style = "";*/

			}

		}

	

}

	



class videoDetails extends details {



	public $headerExtra;

	public $content;

	public $thumbs;

	protected $selectedLink = "";



	function __construct ($pageArray) {

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		$this -> GetRightVideos ();

		$this -> main_class = "class='video' ";	

		

		

		$this -> content .= "<p class=\"bottom\"><a href=\"/contact_us.htm\">Contact us</a> to find out more about a marquee like this.</p>";		

		//$this -> content .= "<script type=\"text/javascript\" src=\"http://w.sharethis.com/button/sharethis.js#publisher=a004578c-213c-49f3-8b00-13c73ef03d75&amp;type=website&amp;post_services=email%2Csms%2Cfacebook%2Cstumbleupon%2Cdelicious%2Ctwitter%2Cwordpress%2Cblogger\"></script>";

		

	}

	

	

	private function GetRightVideos () {

	

		$flashSelected = "";

		$weddingSelected = "";

        $droneSelected = "";        

        $gardenSelected = "";

		$eighteenthSelected = "";

		if ($_SERVER["REQUEST_URI"] == "/video/video.html") {

			$flashSelected = " selected";

		} else if ($_SERVER["REQUEST_URI"] == "/video/wedding_marquee.html") {

			$weddingSelected = " selected";

		} else if ($_SERVER["REQUEST_URI"] == "/video/eighteenth.htm") {

			$eighteenthSelected = " selected";

		} else if ($_SERVER["REQUEST_URI"] == "/video/drone.htm") {

			$droneSelected = " selected";

		} else if ($_SERVER["REQUEST_URI"] == "/video/garden.htm") {

			$gardenSelected = " selected";

		}

			

		$this -> thumbs =  <<<EOQ

			<div id="thumbsDiv">

			<div class="smThumb$droneSelected"><a href="/video/drone.htm"><img src="/images/pages/video/droneSm.jpg" alt="Drone marquee video" width='72' height='50' /></a></div>

			<div class="smThumb$eighteenthSelected"><a href="/video/eighteenth.htm"><img src="/images/pages/video/eighteenthSm.jpg" alt="Video of a marquee for an 18th birthday party" width='72' height='50' /></a></div>

			<div class="smThumb$weddingSelected"><a href="/video/wedding_marquee.html"><img src="/images/pages/video/wedding_marqueeSm.jpg" alt="Wedding marquee video" width='72' height='50' /></a></div>

			<div class="smThumb$gardenSelected"><a href="/video/garden.htm"><img src="/images/pages/video/gardenSm.jpg" alt="Garden wedding video" width='72' height='50' /></a></div>

			<div class="smThumb$flashSelected"><a href="/video/video.html"><img src="/images/pages/video/timelapseSm.jpg" alt="Marquee timelapse video" width='72' height='50' /></a></div>

			</div>

EOQ;

		

	}

		

}







	

class photoDetails extends details{



	//public $headerExtra; COMMENTED THIS OUT COS THINK ITS NOT NEEDED. IF THERES A PROBLEM THIS MAY BE WHY. 12/05/13

	public $content;

	public $thumbs;

	public $nextPrevLinks;	

	protected $nextPhoto = "";	

	protected $selectedLink = "";



	function __construct ($pageArray, $thumbsResult, $category) {

		

		//facebook needs headers and also link text somewhere

		

		$fbcode = "";

		$pincode = "";

		$thispage = "https://www.countymarquees.com" .  $_SERVER["REQUEST_URI"];

		$origpage = "https://www.countymarquees.com" .  $_SERVER["REDIRECT_URL"];

		$file_img = "images/photos/$pageArray[image].jpg";

		

		parent::__construct($pageArray["page_title"], $pageArray["meta_title"], $pageArray["meta_desc"], $pageArray["meta_key"]);

		if (!$category) {

			$this -> GetPhotos ($thumbsResult);

			$this -> main_class = "class='photo' ";

			$this -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com$_SERVER[REQUEST_URI]\"/>";

		} else {

			$this -> GetCategoryPhotos ($thumbsResult, $category);

			$this -> main_class = "class='photo category' ";			

			$this -> headerExtra = "<link rel=\"canonical\" href=\"https://www.countymarquees.com" . $_SERVER["REDIRECT_URL"] . "\" />";

			$this -> headerExtra .= "<meta name=\"robots\" content=\"noindex, follow\" />";

			$this -> headerExtra .= "\n<meta property='og:url' content=\"https://www.countymarquees.com" . $_SERVER["REDIRECT_URL"] . "\" />";

			

		}

		$this -> headerExtra .= "\n<meta property='og:site_name' content='County Marquees'/>";

		$this -> headerExtra .= "\n<meta property='og:title' content='{$pageArray["page_title"]}'/>";

		$this -> headerExtra .= "\n<meta property='og:type' content='website'/>";

		$this -> headerExtra .= "\n<meta property='og:image' content=\"https://www.countymarquees.com/images/photos/$pageArray[image].jpg\" />";

		$this -> headerExtra .= "\n<meta property=\"fb:admins\" content='766316702' />";

		$this -> headerExtra .= "\n<meta property=\"fb:app_id\" content='133183086698108' />";

		

		

		$this -> footerExtra .= "\n<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\" language=\"javascript\"></script>\n<script language=\"javascript\" src=\"/scripts/facets.js\" type=\"text/javascript\"></script>";

		$this -> footerExtra .= "</script>\n";

		$this -> html_dec = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://ogp.me/ns#\" xmlns:fb=\"http://www.facebook.com/2008/fbml\" lang=\"en\">";



		

		list($width, $height) = getimagesize( $file_img);

		$this -> content .= "<a href=\"{$this -> nextPhoto}\"><img src=\"/$file_img\" alt=\"$pageArray[alt]\" title=\"$pageArray[alt]\" width=\"$width\"  height=\"$height\" /><span class='photoarrow'>&#9654;</span></a><h1>" . $pageArray["page_title"] . "</h1>" . $pageArray["full_desc"];

		$this -> content .= "<p class=\"bottom\"><a href=\"/contact_us.htm\">Contact us</a> to find out more about a marquee like this.</p>";

       

		

	}

	

	

	private function GetPhotos ($thumbsResult) {

	

		$selected = "";

		$nextPrevLinks = "";

		$needNext = false;

		$lastURL = "";

		$current = "";

		$counter = 0;

		$first;

		$selectedLink;

		

		//while ($data = mysql_fetch_assoc($thumbsResult)) {
		foreach ($thumbsResult as $data) {

			if (! is_array($data)) {

				Util::Show("result not an array<br>$sql","from photoDetails GetPhotos()");

				return false;

			}

			 

			$current = "/photos/" . $data["url"] . ".htm";

			if ($counter == 0) {

				$first = $current;

				$counter++;

			}

			

			if ($current ==  $_SERVER["REQUEST_URI"]) {				

				$selected = " selected";

				$this -> selectedLink = $current;

				$nextPrevLinks = $lastURL != ""?"<a href='$lastURL' class='previous'>Previous</a> | ":"";

				$needNext = true;

			} else {

				$selected = "";

				if ($needNext) {

					$nextPrevLinks = $nextPrevLinks . "<a href='$current'>Next</a>";

					$needNext = false;

					$this -> nextPhoto = $current;

				}

			}

			

			$lastURL = $current;					

			$imgSrc = file_exists("images/photos/$data[image]Sm.jpg")?"../images/photos/$data[image]Sm.jpg":"../images/photos/no_imageSm.png";			

			$this -> thumbs .= "<div class=\"smThumb$selected\"><a href=\"$current\"><img src=\"$imgSrc\" alt=\"$data[page_title]\" title=\"$data[page_title]\" width='72' height='50' /></a></div>";

		}

		

		if ($this -> nextPhoto == "") {

			$this -> nextPhoto = $first;

			$nextPrevLinks = $nextPrevLinks . "<a href='$first'>Next</a>";

		}

		if ($this -> selectedLink == $first) {

			$nextPrevLinks = "<a href='$current' class='previous'>Previous</a> | " .  $nextPrevLinks;

		}	

		$this -> nextPrevLinks = $nextPrevLinks;

		

	}

	

	private function GetCategoryPhotos ($thumbsResult, $category) {

	

		$selected = "";

		$nextPrevLinks = "";

		$needNext = false;

		$lastURL = "";

		$current = "";

		$counter = 0;

		$first;

		
		foreach ($thumbsResult as $data) {

			if (! is_array($data)) {

				Util::Show("result not an array<br>$sql","from photoDetails");

				return false;

			}

	

			$current =  "/photos/" . $data["url"] . ".htm?category=$category";

			if ($counter == 0) {

				$first = $current;

				$counter++;

			}

				

			if ($current ==  $_SERVER["REQUEST_URI"]) {				

				$selected = " selected";

				$this -> selectedLink = $current;

				$nextPrevLinks = $lastURL != ""?"<a href='$lastURL' class='previous'>Previous</a> | ":"";				

				$needNext = true;

			} else {

				$selected = "";

				if ($needNext) {

					$nextPrevLinks = $nextPrevLinks . "<a href='$current'>Next</a>";

					$needNext = false;

					$this -> nextPhoto = $current;

				}

			}

			

			$lastURL = $current;			

			$imgSrc = file_exists("images/photos/$data[image]Sm.jpg")?"../images/photos/$data[image]Sm.jpg":"../images/photos/no_imageSm.png";			

			$this -> thumbs .= "<div class=\"smThumb$selected\"><a href=\"$data[url].htm?category=$category\" ><img src=\"$imgSrc\" alt=\"$data[page_title]\" title=\"$data[page_title]\" width='72' height='50' /></a></div>";

			

		}

		

		if ($this -> nextPhoto == "") {

			$this -> nextPhoto = $first;

			$nextPrevLinks = $nextPrevLinks . "<a href='$first'>Next</a>";

		}

		if ($this -> selectedLink == "") {

			$this -> selectedLink == $first;

		}

		if ($this -> selectedLink == $first) {

			$nextPrevLinks = "<a href='$current' class='previous'>Previous</a> | " .  $nextPrevLinks;

		}		

		$this -> nextPrevLinks = $nextPrevLinks;

	

	}

	

	

	

		

}







?>