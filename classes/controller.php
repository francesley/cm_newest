<?php



//how it works - controller - calls command resolver - controller calls command deduced in command resolvers - command calls db or whatever - db (usually) calls pages.php which sets up metas and deals with repeating db results - command calls include file (in includes dir) or echos what is needed and finishes off

require_once( '../classes/utilities.php' );

require_once( '../cm_config.php' );

require_once( '../classes/request.php' );

require_once( '../classes/registry.php' );

require_once( '../classes/commandResolver.php' );

//require_once( 'woo/controller/AppController.php' );



class CM_Controller {

   // private $applicationHelper;

	private static $instance;



    private function __construct() {}



    /*static function run() {

		

		

        //$instance = new CM_controller();

        //$instance->init();

        $this -> instance -> handleRequest();

    }*/

	

	static function getInstance() {

		//Util::Show("running");
		//echo ("a");

		

		if (empty(self::$instance)) {

			self::$instance = new CM_Controller();

		}

		self::$instance  -> handleRequest();

	}




    function handleRequest() {

        //$request = new CM_Request();

		



		$reg = Registry::Instance();

		$reg -> SetRequest(new CM_Request( new CM_Request()));

		

        $cmd_r = new CM_CommandResolver();

		$cmd = $cmd_r -> getCommand($reg -> GetRequest());

		if (! $cmd) {

			$request = $reg -> GetRequest();

			$request->addFeedback( "No command - Setting 404 - feedbackstring from controller line 52");

			

		//Util::Show("Should be sending feedback");

			Util::SendFeedback($request -> getFeedbackString());

			Util::Set404();

		}

		//Util::ShowDev ("a");

       $cmd -> execute( $reg -> GetRequest() );
		///NOT WORKING HERE

		//Util::ShowDev ("a");
		

	   switch ($cmd -> getStatus()) {

			case 0:

				$request = $reg -> GetRequest();

				$request->addFeedback( "Command status 0 - Setting 404 - feedbackstring from controller line 64");

				Util::SendFeedback($request -> getFeedbackString());			

				Util::Set404();				

				break;

			case 1:

				//Util::Show("commands status is 1");

				

				//$output = ob_get_contents();

				if (isset($cmd -> cache)) {

					if (!$cmd -> cache -> cacheExists) {

						/*Util::Show ("no cache", "controller.php");

						$markCache = "************************************************";

						$breaks = "<br/><br/>";*/

						$output = ob_get_contents();

						//$cmd -> cache -> SetCache($breaks .$markCache . $output . $markCache . $breaks);

						$cmd -> cache -> SetCache($output);

						//echo $output;

					} /*else {				

						Util::Show ("cache exists", "controller.php");

					}*/

				}	/*else {

					Util::Show ("cache not set", "controller.php");

				}	*/

				

				

				//ob_end_flush();

				break;

			case 2:

				$request = $reg -> GetRequest();

				$request->addFeedback( "Command status 2 - Setting 500");

				Util::SendFeedback($request -> getFeedbackString(), "feedbackstring from controller line 90");

				Util::Set500();

				break;

			default:

				break;

				

		}

		

		

        //Util::Show("end");

    }



    /*function forward( $target ) {

        include( "woo/view/$target.php" );

        exit;

    }*/

}

?>

