<?php

require_once( "../classes/commands/default.php" );



class CM_CommandResolver {

    /*private static $base_cmd;

    private static $default_cmd;

*/



	private $cats = array("photos","events","help","planner","company","sitemap","text_venues","widget","forms","case_studies","pricing","venues","siteMap", "panorama", "real", "video", "newvenues","areas");

	//private $stopwords = array("proc","self","environ");

	

    function __construct() {

        /*if ( ! self::$base_cmd ) {

            self::$base_cmd = new ReflectionClass( "woo_command_Command" );

            self::$default_cmd = new woo_command_DefaultCommand();

        }*/

    }



    function getCommand( CM_Request $request ) {

        $subtype = $request->getProperty( 'subtype' );

		$cat = $request->getProperty( 'cat' );

		$filepath; $classname;

		//Util::Show($cat);

		

		

		

		if ($cat) {

			if (!in_array($cat,$this -> cats)) {

				$request->addFeedback( "Non-existent cat: $cat, from commandResolver.php line 26");

				return false;

			}

		}

		

		//TODO may not need to check for special pages

		$specialPages = array ("pricing","planner", "photos", "forms", "venues", "text_venues", "siteMap",  "video","real","case_studies","newvenues","areas");

		if ( ! $subtype ) {

			if (! $cat) {

				$filepath = "../classes/commands/home.php";

				$classname = "command_default";

				

			} else {							

				if (in_array($cat, $specialPages)) {

					$filepath = "../classes/commands/$cat.php";

					$classname = "command_$cat";

					/*Util::Show($filepath,"filepath");

					Util::Show($classname,"classname");*/

				} 

			}

			

		} else {

			//TODO sort this out

			/*if (preg_match("!.|\/!", $subtype) !== 0) {

				$request->addFeedback( "Suspicious chars in subtype: $subtype, commandResolver 49");

				return false;

			}*/

			

			if (in_array($cat, $specialPages)) {

					$filepath = "../classes/commands/$cat.php";

					$classname = $cat == "photos"?"command_photo":"command_$cat";

					/*Util::Show($filepath,"filepath");

					Util::Show($classname,"classname");*/

			} else {

				  $filepath = "../classes/commands/text.php";

				  $classname = "command_text";

			}

			

			

		}

		

		

		if ( file_exists( $filepath ) ) {

				//Util::Show($filepath, "filepath exits from commandresolver");

				

            require_once( "$filepath" );

			

            if ( class_exists( $classname ) ) {

					/*Util::ShowDev( "classname exists from commandresolver");
					exit();*/
                 return new $classname();

            } else {

                 $request->addFeedback( "classname $classname does not exist" );

				 return false;

            }

        } else {

				$request->addFeedback( "filepath $filepath does not exist");

				 return false;

		}

		

        $request->addFeedback( "command '$cmd' not found" );

		return false;

    }

}



?>

