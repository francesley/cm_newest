<?php

abstract class menu {

	protected $links;
	protected $linkNames;
	protected $menuStart;
	public $m_menu;
	protected $menuCat;
	
	function __construct () {
		
		$this -> m_menu = $this -> menuStart;
		$a_extra = "";
		$searchlink_extra = "";
		$lastLinkName = "";
		$start = "";
		$catTest = false;
		
		for ($i = 0; $i < (count($this -> links)); $i++) {
			/*Util::ShowDev($i,"current count");
				Util::ShowDev($this -> links[$i],"current link");
				Util::ShowDev($lastLinkName,"last");
				Util::ShowDev(" ");*/
			if ($i == 0) {
				$a_extra = "class='menu_left";
			} elseif ($i == (count($this -> links) - 1)) {
				$a_extra = "class='menu_right";
				$start = ">" . $lastLinkName . "</a></li>";
				if($this -> links[$i] == "/search.htm") {
					$searchlink_extra = " id='searchlink' title='Search'";
					$a_extra = "class='menu_right' id='search'";
				}
			} else {
				$searchlink_extra = "";
				//if ($i != count($this -> links)) {
					$start = ">" . $lastLinkName . "</a></li>";
				/*} else {
					if (get_class($this) == "topMenu") {
						$start = ">" . $lastLinkName . "</a><div id='searchbox'><gcse:searchbox-only resultsUrl='http://www.countymarquees.com/search.htm'></div></li>";
					} else {
						$start = ">" . $lastLinkName . "</a></li>";
					}
				}*/
				$a_extra = "";
				
			}
			
			//TO GET CATEGORY SELECTED
			if (get_class($this) == "topMenu") {
				$catTest = $this -> menuCat == strtolower($this -> cats[$i]);
				
			} else {
				//HACKS TO GET CASE STUDIES AND MARQUEES (PRICING) SELECTED IN SUBMENU
				$pos = strpos($_SERVER["REQUEST_URI"], "case");
				if ($pos !== false){
					$catTest = $this -> links[$i] == "/real.htm";
				} elseif (strpos($_SERVER["REQUEST_URI"], "erection")){
					$catTest = $this -> links[$i] == "/real.htm";
				}
				if (get_class($this) == "pricingMenu") {
					if (!in_array($_SERVER["REQUEST_URI"],$this -> links)) {
						$catTest = $this -> links[$i] == "/pricing/marquees.htm";
					}
				}
			}
			//TODDO remove test
			if ($this -> links[$i] == $_SERVER["REQUEST_URI"] || $catTest) {
				if ($a_extra != "") {
					$this -> m_menu .= $start . "<li $a_extra selected'><a class='selected' $searchlink_extra href=\"" . $this -> links[$i] . "\" ";
				} else {
					$this -> m_menu .= $start . "<li class='selected' ><a class=\"selected\" href=\"" . $this -> links[$i] . "\" ";
				}
			/*} elseif ($i == count($this -> links)) {
				$this -> m_menu .= $start;		*/	
			} else {
				if ($a_extra != "") {
					$this -> m_menu .=  $start . "<li $a_extra' ><a $searchlink_extra href=\"" . $this -> links[$i] . "\" ";
				} else {
					$this -> m_menu .=  $start . "<li><a href=\"" . $this -> links[$i] . "\"";
				}
				//echo $this -> m_menu . "<br/>";
			}			
			
			$lastLinkName = $this -> linkNames[$i];
			
		}
		$this -> m_menu .= ">$lastLinkName</a></a><div id='searchbox'>		<div class='search_container'>			<form action='/search.htm' method='get'>			<input type='text' name='q'>						<button type='submit'> <svg width='13' height='13' viewBox='0 0 13 13'><title>search</title><path d='m4.8495 7.8226c0.82666 0 1.5262-0.29146 2.0985-0.87438 0.57232-0.58292 0.86378-1.2877 0.87438-2.1144 0.010599-0.82666-0.28086-1.5262-0.87438-2.0985-0.59352-0.57232-1.293-0.86378-2.0985-0.87438-0.8055-0.010599-1.5103 0.28086-2.1144 0.87438-0.60414 0.59352-0.8956 1.293-0.87438 2.0985 0.021197 0.8055 0.31266 1.5103 0.87438 2.1144 0.56172 0.60414 1.2665 0.8956 2.1144 0.87438zm4.4695 0.2115 3.681 3.6819-1.259 1.284-3.6817-3.7 0.0019784-0.69479-0.090043-0.098846c-0.87973 0.76087-1.92 1.1413-3.1207 1.1413-1.3553 0-2.5025-0.46363-3.4417-1.3909s-1.4088-2.0686-1.4088-3.4239c0-1.3553 0.4696-2.4966 1.4088-3.4239 0.9392-0.92727 2.0864-1.3969 3.4417-1.4088 1.3553-0.011889 2.4906 0.45771 3.406 1.4088 0.9154 0.95107 1.379 2.0924 1.3909 3.4239 0 1.2126-0.38043 2.2588-1.1413 3.1385l0.098834 0.090049z'></path></svg> </button></form></div></div></li></ul>\n";
	}
	
	public function __string() {
		return $this -> m_menu;
	}
	

}

class topMenu extends menu {

	function __construct ($cat, $subType) {
		if (!$cat) {
			$this -> menuCat = "home";
		} else {
			switch ($cat) {
				case "forms":
					$this -> menuCat = ($subType == "contact")?"forms":(($subType == "quotation" || $subType == "quote")?"pricing":(($subType == "jobs")?"company":(($subType == "questions")?"help":"venues")));
					break;
				case "case_studies":
					$this -> menuCat = "help";
					break;
				default:
					$this -> menuCat = $cat;
			}
		}
	
		$searchname = "<img src='/images/search_plain.png' alt='search' width='23' height='25'/>";		
		$this -> links = array("/photos.htm", "/pricing/marquees.htm","/equipment/marquees.htm","/wedding-marquees.htm","/company/about.htm","/movies/planner.html","/venues.htm","/contact_us.htm","/search.htm");
		$this -> cats = array("Photos", "Pricing","Help","Events","Company", "Planner","Venues","Forms","Special");
		$this -> linkNames = array("PHOTOS", "PRICING","MARQUEE INFO","EVENTS","COMPANY", "PLANNER", "VENUES","CONTACT",$searchname);
		//$this -> linkNames = array("Photos", "Pricing","Marquee info","Events","Company", "Planner");
		if ($_SERVER["REQUEST_URI"] != "/") {
			$this -> menuStart = "\n<a id=\"skip\" href=\"#content\">Skip navigation</a>\n<ul id=\"mainMenu\">";
		} else {
			$this -> menuStart = "\n<a id=\"skip\" href=\"#contentHome\">Skip navigation</a>\n<ul id=\"mainMenu\">";
		}
		
		parent::__construct();
	}

}

class pricingMenu extends menu {
	
	function __construct () {
		$this -> links = array("/pricing/marquees.htm", "/pricing/furniture.htm","/pricing/equipment.htm","/pricing/sample.htm","/pricing/quote.htm","/pricing/quotation.htm");
		$this -> linkNames = array("Marquees", "Furniture","Equipment","Examples","Online quote", "Request a quote");
		$this -> menuStart = "<ul id='subMenu'>";
		parent::__construct();
		$this -> m_menu .= "</div>\n";
	}

}

class companyMenu extends menu {
		
	function __construct () {
		$this -> links = array("/company/about.htm","/company/testimonials.htm","/company/clients.html","/company/jobs.htm","/Terms_and_conditions.htm","/brochure.htm","/contact_us.htm");
		$this -> linkNames = array("About us","Testimonials","Clients","Marquee work","Terms","Brochure","Contact us");
		$this -> menuStart = "<ul id='subMenu'>";
		parent::__construct();
		$this -> m_menu .= "</div>\n";
	}
}

class eventsMenu extends menu {
	
	function __construct () {
		$this -> links = array("/wedding-marquees.htm","/corporate-marquees.htm","/events/party_marquees.htm","/events/barmitzvah-marquees.htm");
		$this -> linkNames = array("Wedding marquees","Corporate marquees","Party marquees","Barmitzvah marquees");
		$this -> menuStart = "<ul id='subMenu'>";
		parent::__construct();
		$this -> m_menu .= "</div>\n";
	}
}

class covidMenu extends menu {
	
	function __construct () {
		$this -> links = array("/case_studies/covid_schools.htm","/case_studies/covid_hospitality.htm","/case_studies/covid_medical.htm");
		$this -> linkNames = array("Covid &amp; schools","Covid &amp; hospitality","Medical","Covid Christmas");
		$this -> menuStart = "<ul id='subMenu'>";
		parent::__construct();
		$this -> m_menu .= "</div>\n";
	}
}

class venuesMenu extends menu {
	
	function __construct ($noLeft = false) {
		/*$this -> links = array("/venues.htm","/venues/sites.htm","/venues/widget.htm","/venues/add_venue.htm");
		$this -> linkNames = array("Marquee venue finder","View marquee venues list","Add venue finder to YOUR site","Add a venue");*/
		$this -> links = array("/venues.htm","/venues/sites.htm","/venues/add_venue.htm");
		$this -> linkNames = array("Marquee venue map","Marquee venues lists", "Add a venue");
		$this -> menuStart = $noLeft?"<ul id='subMenu'>":"<ul id='subMenu' style='left:230px'>";
		parent::__construct();
		//$this -> m_menu .= "</div>\n";
	}
}

class helpMenu extends menu {
	
	function __construct () {
		$this -> links = array("/equipment/marquees.htm","/equipment/furniture.htm","/equipment/lighting.htm","/equipment/decoration.htm","/real.htm","/marquee_questions.html","/marquee_essentials.html");
		$this -> linkNames = array("Marquees","Furniture","Lighting","Decoration","Real marquees","FAQs","Where to start?");
		$this -> menuStart = "<ul id='subMenu'>";
		parent::__construct();
		$this -> m_menu .= "</div>\n";
	}
}

?>
