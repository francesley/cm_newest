<?php

class cacher {

	private $includePrefix;
	private $request;
	private $currFile;
	public $cacheExists = false;
	
	function __construct($request) {
		global $filepath;
		$this -> request = $request;
		$this -> includePrefix = $filepath;		
	}
	
	function CheckCache() {
		/*if (!$this -> request -> getProperty("category")) {
			$this -> GetFile();
		} else {
			$this -> GetCategoryFile();
		}*/
		if ($this -> request -> getProperty("category")) {
			$this -> GetCategoryFile();
		} else if ($this -> request -> getProperty("county")) {
			$this -> GetVenuesFile();
		}  else {
			$this -> GetFile();
		}
		
		//Util::Show($this -> currFile);
		
		if (file_exists($this -> currFile)) {
			$this -> cacheExists = true;
		}
		
		return $this -> cacheExists;
	}
	
	//think should get rid of these - will be a pain to edit - 
	private function GetCategoryFile() {
		if ($this -> request -> getProperty("subtype")) {
			$this -> currFile = $this -> includePrefix . $this -> request -> getProperty("cat") . "/category/" . $this -> request -> getProperty("category") . "/" . $this -> request -> getProperty("subtype") . ".inc";
		} else {
			$this -> currFile = $this -> includePrefix . $this -> request -> getProperty("cat") . "/category/" . $this -> request -> getProperty("category") . ".inc";
		}
	}
	
	private function GetVenuesFile() {
			$this -> currFile = $this -> includePrefix . $this -> request -> getProperty("cat") . "/" . $this -> request -> getProperty("county") . ".inc";
	}
	
	private function GetFile() {
		
		if ($this -> request -> getProperty("cat")) {			
			if ($this -> request -> getProperty("subtype")) {
				$this -> currFile = $this -> includePrefix . $this -> request -> getProperty("cat") . "/" . $this -> request -> getProperty("subtype") . ".inc";
			} else {				
				$this -> currFile = $this -> includePrefix . $this -> request -> getProperty("cat") . ".inc";
			}
		} else {
			//home page
			$this -> currFile = $this -> includePrefix . "home.inc";
		}
	
	}
	
	//now need to work out how im gonna get content in. prob is that all cache checking takes place in commands but need to save it in controller cos thats where the unzipping occurs
	function SetCache($content) {
		$fp = fopen($this -> currFile, 'w');
		$result = fwrite($fp, $content);
		fclose($fp);
		//Util::Show($result,"file saved?");
	}
	
	function GetCache() {
	//Util::Show("getting cache","cacher.php");
		require_once($this -> currFile);
	}
	
}



?>