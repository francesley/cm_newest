<?php



class text_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ? and subtype = ?";

	private $widePages = array("testimonials","clients", "brochure","terms");

	private $widePage = false;

	protected $subtype;

	

	

	function __construct($cat, $subtype) {

		$this -> subtype = $subtype;
		$this -> cat = $cat;
		//Util::ShowTrue(is_resource(parent::conn), "conn is resource");
		parent::__construct();
		
	}

	

	function doFind() {

		

		$result = parent::doStatement($this -> sql);

		//Util::ShowTrue($result, "result");

		if ($result) {			

			return $this -> load ( $result );

		} else {

			return false;

			/*Util::Show("no result from parent","from text_DBController");

			Util:: Set404();*/

		}

	

	}

	

	function doLoad( $pageDetails ) {

	//Util::Show($this -> subtype);

		if (in_array($this -> subtype, $this -> widePages)) {

			$this -> widePage = true;

		}

		$page = new textDetails($pageDetails, $this -> widePage);

		return $page;

	}



}

class areas_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ? and subtype = ?";

	private $widePages = array("testimonials","clients", "brochure","terms");

	private $widePage = false;

	protected $subtype;

	

	

	function __construct($subtype) {

		$this -> subtype = $subtype;
		$this -> cat = "areas";
		//Util::ShowTrue(is_resource(parent::conn), "conn is resource");
		parent::__construct();
		
	}

	

	function doFind() {

		

		$result = parent::doStatement($this -> sql);

		//Util::ShowTrue($result, "result");

		if ($result) {			

			return $this -> load ( $result );

		} else {

			return false;

			/*Util::Show("no result from parent","from text_DBController");

			Util:: Set404();*/

		}

	

	}

	

	function doLoad( $pageDetails ) {

	//Util::Show($this -> subtype);

		if (in_array($this -> subtype, $this -> widePages)) {

			$this -> widePage = true;

		}

		$page = new areasDetails($pageDetails, $this -> widePage);

		return $page;

	}



}





class forms_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ? AND subtype = ?";

	private $widePage = false;

	protected $subtype;

	

	

	function __construct($cat, $subtype) {

	

		$this -> subtype = $subtype;
		$this -> cat = $cat;

		parent::__construct();

		//$this -> sql .=  " AND subtype = '" .  mysql_real_escape_string($subtype) . "'";

		

	}

	

	function doFind() {

		

		$result = parent::doStatement($this -> sql);

		if ($result) {			

			return $this -> load ( $result );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails ) {

		//Util::ShowDev ("a");

		$page = new formsDetails($pageDetails);

		return $page;

	}



}





class pricing_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ?  AND subtype = ?";

	

	

	function __construct($subtype) {

		$this -> subtype = $subtype;
		$this -> cat = "pricing";

		parent::__construct();		

	}

	

	function doFind() {

		$result = parent::doStatement($this -> sql);

		if ($result) {

		//print_r($result);		

			return $this -> load ( $result );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails ) {

		$page = new pricingDetails($pageDetails);

		return $page;

	}



}



//for home page

class noSubType_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier FROM pages WHERE cat = ?";

	

	

	function __construct($cat) {

		$this -> cat = $cat;

		parent::__construct();
		

	}

	

	function doFind() {

		$result = parent::doStatement($this -> sql);

		if ($result) {			

			return $this -> load ( $result );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails ) {

		$page = new homeDetails($pageDetails);

		return $page;

	}



}





//planner, panorama and think main venues too

class specials_DBController extends DBControllerNew {

	

	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier FROM pages WHERE cat = ?";

	protected $cat;

	

	

	function __construct($cat) {

		

		parent::__construct();

		$this -> cat = $cat;

		//$this -> sql .=  mysql_real_escape_string($cat) . "'";

		//Util::Show ($sql);

		

	}

	

	function doFind() {

		$result = parent::doStatement($this -> sql);

		if ($result) {			

			return $this -> load ( $result );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails ) {

		$className = $this -> cat . "Details";

		$page = new $className($pageDetails);

		return $page;

	}



}



class text_venues_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ? and subtype = ?";

	private $sqlCounties = "SELECT photo, alt FROM pages WHERE cat = 'text_venues'";

	private $counties;

	

	

	function __construct($subtype,$county) {

		$this -> subtype = $subtype;
		$this -> cat = 'text_venues';

		parent::__construct();

		$this -> counties = $county == "all"?false:$county;

		
	}

	

	function doFind() {

			$result = parent::doStatement($this -> sql);

		if ($result) {			

			return $this -> load ( $result );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails ) {

		$page = new text_venues_Details($pageDetails,$this -> counties);

		//Util::Show($this -> counties,"counties");

		return $page;

	}



}



class sitemap_DBController extends DBControllerNEW {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ?";

	private $sqlPages = "SELECT page_title, url, cat FROM pages WHERE url IS NOT NULL ORDER BY cat, page_title";

	private $sqlPhotos = "SELECT page_title, url FROM photos ORDER BY page_title";

	

	

	function __construct($cat) {

		$this -> cat = 'siteMap';

		parent::__construct();

		//Util::Show ($this -> sql);

		

	}

	

	function doFind() {

		$result = parent::doStatement($this -> sql);

		$result2 = parent::doMultipleRowStatement($this -> sqlPages);

		$result3 = parent::doMultipleRowStatement($this -> sqlPhotos);

		if ($result && $result2 && $result3) {

			return $this -> load ( $result, $result2, $result3);

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails, $allPages, $photos) {

		$page = new sitemapDetails($pageDetails, $allPages, $photos);

		return $page;

	}



}



class case_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier, photo, alt FROM pages WHERE cat = ? AND subtype = ?";

	//private $sql_thumbs = "SELECT page_title, subtype FROM pages WHERE cat = 'case_studies' AND live <> 0 ORDER BY  id DESC";

	private $sql_thumbs = "SELECT page_title, subtype FROM pages WHERE cat = 'case_studies' AND live <> 0 ORDER BY (CASE WHEN textorder IS NULL THEN 1 ELSE 0 END) ASC, textorder ASC, id DESC";

	

	protected $subtype;

	

	

	function __construct($cat, $subtype) {



		$this -> subtype = $subtype;
		$this -> cat = $cat;

		//Util::ShowTrue(is_resource(parent::conn), "conn is resource");

		parent::__construct();

		//Util::ShowTrue(is_resource(parent -> conn), "conn is resource");

		//$this -> sql .=  mysql_real_escape_string($cat) . "' AND subtype = '" .  mysql_real_escape_string($subtype) . "' ORDER BY (CASE WHEN textorder IS NULL THEN 1 ELSE 0 END) ASC, textorder ASC, id DESC";

		//Util::Show ($this -> sql);

		

	}

	

	function doFind() {

		

		$result = parent::doStatement($this -> sql);
		
		unset($this -> subtype);
		$result2 = parent::doMultipleRowStatement($this -> sql_thumbs);

		if ($result && $result2) {

			return $this -> load ( $result, $result2 );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails,$thumbsResult ) {

	//Util::Show($this -> subtype);

		

		$page = new case_studiesDetails($pageDetails, $thumbsResult);

		return $page;

	}



}





class real_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key FROM pages WHERE cat = ? ";

	private $sql_gallery = "SELECT page_title, subtype FROM pages WHERE cat = 'case_studies' AND live <> 0 ORDER BY (CASE WHEN textorder IS NULL THEN 1 ELSE 0 END) ASC, textorder ASC, id DESC";

	//private $category;

	

	

	function __construct() {

			$this -> cat = 'real';

			parent::__construct();

							

	}

	

	function doFind() {

				

		$result = parent::doStatement($this -> sql);
		
		$result2 = parent::doMultipleRowStatement($this -> sql_gallery);

		

		if ($result && $result2) {			

			return $this -> load ( $result, $result2 );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails, $photoDetails ) {

		

		$page = new realDetails($pageDetails, $photoDetails);

		return $page;

	}



}







class photos_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, testimonial, testifier FROM pages WHERE cat = 'photos'";

	private $sql_gallery = "SELECT short_desc, image, page_title, url from photos";

	private $category;

	

	

	function __construct($category) {

			

			parent::__construct();

			if ($category) {

				$this -> category = $category;

				$this -> sql .= " AND subtype = '" . $category . "'";

				$this -> sql_gallery .=  " WHERE " . $category . " = '1' AND live = '1' ORDER BY text_order";

			} else {

				$this -> sql_gallery .=  " WHERE live = '1' ORDER BY text_order";

			}

				

				

	}

	

	function doFind() {

		
	//TODO somewhere want to cache this whole page - or all pages? or most pop?

		$result = parent::doSimpleStatement($this -> sql);
		
		$result2 = parent::doMultipleRowStatement($this -> sql_gallery);

		if ($result && $result2) {			

			return $this -> load ( $result, $result2 );

		} else {

			return false;

		}

	

	}

	

	function doLoad( $pageDetails, $photoDetails ) {

		$page = new photosDetails($pageDetails, $photoDetails, $this -> category);

		return $page;

	}



}



class photo_DBController extends DBControllerNew {



	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, image, full_desc, alt FROM photos WHERE url = ?";

	private $sql_thumbs = "SELECT image, page_title, url FROM photos ";

	private $category;
	
	private $url;

	

	function __construct($url, $category) {	

			parent::__construct();

		$this -> url =  $url;

		if ($category) {

			$this -> category = $category;

			$this -> sql_thumbs .=  " WHERE $category = '1'  AND live = '1' ORDER BY text_order";

		} else {

			$this -> sql_thumbs .=  "  WHERE live = '1' ORDER BY text_order";

		}	

	}

	

	function doFind() {

		

		

		

		//TODO somewhere want to cache thumbs

		$result2 = parent::doMultipleRowStatement($this -> sql_thumbs);
		
		$this -> cat = $this -> url;
		
		$result = parent::doStatement($this -> sql);

		if ($result && $result2) {			

			return $this -> load ( $result, $result2 );

		} else {

			//Util::Show("arnt two results<br>" . $this -> sql,"from photo_DBController");

			return false;

		}

	

	}

	

	function doLoad( $pageDetails, $thumbsResult ) {	

		$page = new photoDetails($pageDetails, $thumbsResult, $this -> category);

		return $page;

	}



}



class video_DBController extends DBControllerNew {

	

	//photo column in db holds video name

	private $sql = "SELECT page_title, meta_title, meta_desc, meta_key, photo FROM pages WHERE cat = ? and subtype = ?";
	protected $cat = "video";

	//private $sql_thumbs = "SELECT image, page_title, url FROM pages WHERE cat = 'video'";

	

	function __construct($subtype) {
		
		$this -> subtype = $subtype;

		parent::__construct();


	}

	

	function doFind() {

				

		$result = parent::doStatement($this -> sql);

		//TODO somewhere want to cache thumbs

		//$result2 = parent::doMultipleRowStatement($this -> sql_thumbs); CAN REINSTATE IF WE EVER DO MORE VIDS

		if ($result) {			

			return $this -> load ( $result );

		} else {

			//Util::Show("arnt two results<br>" . $this -> sql,"from photo_DBController");

			return false;

		}

	

	}

	

	function doLoad( $pageDetails ) {	

		$page = new videoDetails($pageDetails);

		return $page;

	}



}







?>

