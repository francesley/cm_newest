<?php


class Util {

	
	
	private function __construct() {}

	static function GetFiles($origDir) {
		
		$files = array();
		if (is_dir($origDir) && is_readable($origDir)) {
			$d = dir($origDir);
			while (false !== ($f = $d->read())) {
				if (('.' == $f) || ('..' == $f)) {
					continue;
				}
				if (!is_dir("$origDir/$f")) {
					array_push($files, "$origDir/$f");
				}				
			}
			$d->close();
		}
		sort($files);
		return $files;
	}
	

	
	static function Set404() {
		 
		ob_end_clean();
		ob_start("ob_gzhandler");
		/*$header = "Content-Type: text/html; charset=iso-8859-1";
		$header = "HTTP/1.0 404 Not Found\n" . $header;*/
		header("Content-Type: text/html; charset=iso-8859-1");
		header("HTTP/1.1 404 Not Found");
		include("404.htm");
		//exit();
	}
	
	static function Set500() {
		ob_end_clean();
		ob_start("ob_gzhandler");
		/*$header = "Content-Type: text/html; charset=iso-8859-1";
		$header = "HTTP/1.0 404 Not Found\n" . $header;*/
		header("Content-Type: text/html; charset=iso-8859-1");
		header("HTTP/1.1 500  Internal Server Error");
		include("500.htm");
		//exit();
	}
	static function SendFeedback($msg) {
		$script = $_SERVER['SCRIPT_NAME'];
		$uri = $_SERVER['REQUEST_URI'];	
		$referrer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"";
		$msg .= "\n\nScript: " . $script . "\nuri: " .  $uri . "\nreferrer: " . $referrer;
		$from = "From:errors@countymarquees.com";
		mail("fdl4712@aol.com","CM errors", $msg, $from);
		//Util::Show($msg, "Email should have gone");
	}
	
	static function ReportError($feedback, $action = "") {
		//error_log($feedback);
		if ($action) {
			$feedback = $feedback . "\n Action taken: " . $action;
		}
		mail("fdl4712@aol.com","CM error report",$feedback);
	}

	static function ReportErrors($error_message, $error_page, $error_line) {

	/*global $level;*/
	$level = "live";
	//$level = "development";
	$script = $_SERVER['SCRIPT_NAME'];
	$uri = $_SERVER['REQUEST_URI'];	
	$referrer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"";
	
	if ($level == "development") {
		echo "<span style='color:#000'>Error Reported:<br>
		Error number: $error_number<br>
		Error message: $error_message<br>
		Error page: $error_page<br>
		Error line: $error_line<br>
		Error script: $script<br>
		Error uri: $uri <br>
		Referrer: $referrer </span>
		<br><br>\n";
	} else {
	
	
		$message = "ERROR:\n Error number: $error_number\n Error message: $error_message \n Error page: $error_page \n Error line: $error_line \n Error script: $script \n Error uri: $uri\n";
		
		
		$from = "From:errors@countymarquees.com";
		mail ("fdl4712@aol.com","CM website errors",$message, $from );
		error_log($message, 0);
		ob_end_clean();
		ob_start("ob_gzhandler");
		include("error.htm");
		exit();
	}
		
	
}

	static function createSelect($optionsArray, $selectName, $selected = false) {
	
		if ($selected == "Error") {
			$ret = "<select name='$selectName' class='error'><option selected='selected' value='0' >Please select one</option>";
		} else {
			$ret = "<select name='$selectName'>";
			$ret .= $selected == false?"<option selected='selected' value='0'>Please select one</option>":"";
		}		
		
		foreach ($optionsArray as $key => $option) {
		
			if ($selected == $key) {
				$ret .= "<option selected='selected' value=\"$key\">" . $option . "</option>\n";
			} else {
				$ret .= "<option value=\"$key\">" . $option . "</option>\n";
			}
		}
		$ret .= "</select>";
		
		return $ret;
	}
	
	static function createSelectSimple ($optionsArray, $selectName, $selected = false) {
			
		$ret = "<select name='$selectName'>";
		$ret .= $selected == false?"<option selected='selected' value='0'>Please select one</option>":"";		
		
		foreach ($optionsArray as $value) {
			
			$option = str_replace('../images/logos/', "", $value);
			
			if ($selected == $option) {
				$ret .= "<option selected='selected'>" . $option . "</option>\n";
			} else {
				$ret .= "<option>" . $option . "</option>\n";
			}
		}
		$ret .= "</select>";
		
		return $ret;
	}
	
	static function createSelectDeadSimple ($optionsArray, $selectName, $selected) {
			
		$ret = "<select name='$selectName'>";		
		
		foreach ($optionsArray as $key => $option) {
			/*Util::Show($option);
			Util::Show($key, "key");*/
			//$exploded = explode(" ",$option);
			
			if ($selected == $key) {
				$ret .= "<option value=\"$key\" selected=\"selected\">" . $option . "</option>\n";
			} else {
				$ret .= "<option value=\"$key\">" . $option . "</option>\n";
			}
		}
		$ret .= "</select>";
		
		return $ret;
	}
	
	//returns true if all integers
	static function Check_Int ($input) {
		return ctype_digit(strval($input));
	}
	
	static function Show ($thing = "", $title = "DEBUG") {
			//echo "<br/>" . strtoupper($title) . ": " . $thing . "<br/>";
		
			mail("fdl4712@aol.com", 
			"ShowSomething from CM website",
			"\n$title, \n$thing\nPage: http://$_SERVER[REQUEST_URI]");
	}
	
	static function ShowDev ($thing = "", $title = "DEBUG") {
			echo "<br/>" . strtoupper($title) . ": " . $thing . "<br/>";
	}
	
	static function LogErrors ($thing = "", $title = "DEBUG") {
		mail("fdl4712@aol.com", 
			"ShowSomething from CM website",
			"\n$title, \n$thing\nPage: http://$_SERVER[REQUEST_URI]");
	}
	
	static function ShowTrue ($thing, $name = "TRUE OR FALSE") {
			$result = $thing?"true":"false";
			echo "<br/>" . strtoupper($name) . ": " . $result . "<br/>";
	}
	
	static function ShowValid ($TB) {
		$test = $this -> valid?"currently valid":"currently invalid";
		Show ($test, $TB);
	}
	
	
	static function CheckEmail($email) {
		  if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		} else {
			return false;
		}	
	}
	
	static function remove_headers($string) { 
	  $headers = array(
		"/to\:/i",
		"/from\:/i",
		"/bcc\:/i",
		"/cc\:/i",
		"/Content\-Transfer\-Encoding\:/i",
		"/Content\-Type\:/i",
		"/Mime\-Version\:/i" 
	  );
	  $string = strtolower($string);
	  if (preg_replace($headers, '', $string) == $string) {
	  	if (eregi("\r", $string) || eregi("\n", $string)) { 
			return false;
		}
		return true;
	  } else {
		return false;
	  }
	}
	
	

	
}

?>