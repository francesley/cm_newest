<?php



abstract class formChecker {



	protected $TBs;

	protected $allFields;

	protected $send;

	protected $request;

	public $styleValidEmail;

	public $styleFormResponse;

	public $formResponse;

	public $eventsDD;

	public $valid;

	public $f_include;

	private $spam_message = "There has been a problem with your details. Please try again";

	private $toolong_message = "Sorry. The text you have entered in the message field is too long. We only allow 1500 characters. Please try again";

	private $error_message = "Please correct the errors highlighted below and submit the form again";

	protected $arrEventType = array("Wedding","Private party","Corporate/hospitality","Show","Product launch","Other");	

	protected $arrHowheard = array("Recommendation","Search engine","Other");

	protected $myErrors = "FORM ERRORS\n";

	protected $spam = false;

	

	function __construct () {

		

		if ($this -> request -> getProperty("subtype") !== "special") { //this excludes login for cm staff

		  $filepath = "form_includes/" . $this -> request -> getProperty("subtype") . ".php";		

		  $this -> f_include = file_exists( $filepath ) ? $filepath : false;

		}

		

		if ($this ->request -> getProperty("subtype") == "quote_test") {

			//Util::ShowDev("here");

		} else if (!$this ->request -> getProperty("submitted")) {

			foreach ($this -> allFields as $field) {

				$boxBase =  substr($field, 2);

				//Util::Show($boxBase);

				$currFormBox = "form" . $boxBase;

				$currStyle = "style" . $boxBase;

				$this -> $currFormBox = "";

				$this -> $currStyle = "";

			}

		}

		

	}

	

	

	public function Check() {

		$counter = 0;

		//TODO: change it so that valid false initially and only set true if no errors

		

		

		//SPAM TEST - TODO change domain
		if (!strpos($_SERVER['HTTP_REFERER'],"countymarquees.com")) {

			$this -> formResponse = $this -> spam_message;

			$this -> styleFormResponse = "style=\"display:block\" ";

			$this -> SetSpam ("cm not the referrer");

			foreach ($this -> allFields as $field) {

				$boxBase =  substr($field, 2);

				//Util::Show($boxBase);

				$currFormBox = "form" . $boxBase;

				$currStyle = "style" . $boxBase;

				$this -> $currFormBox = "";

				$this -> $currStyle = "";

			}

			return false;

		}

		

		if ($this -> request -> getProperty("tbName") == $this -> request -> getProperty("tbTel")) {

			$this -> formResponse = $this -> spam_message;

			$this -> styleFormResponse = "style=\"display:block\" ";

			 $this -> SetSpam ("Name and tel the same: " . $this -> request -> getProperty("tbName") . " and " . $this -> request -> getProperty("tbTel"));

			 foreach ($this -> allFields as $field) {

				$boxBase =  substr($field, 2);

				//Util::Show($boxBase);

				$currFormBox = "form" . $boxBase;

				$currStyle = "style" . $boxBase;

				$this -> $currFormBox = "";

				$this -> $currStyle = "";

			}

			return false;

		}

		if ($this -> request -> getProperty("tbName") == "Barnypok") {

			$this -> formResponse = $this -> spam_message;

			$this -> styleFormResponse = "style=\"display:block\" ";

			 $this -> SetSpam ("Barnypok");

			 foreach ($this -> allFields as $field) {

				$boxBase =  substr($field, 2);

				//Util::Show($boxBase);

				$currFormBox = "form" . $boxBase;

				$currStyle = "style" . $boxBase;

				$this -> $currFormBox = "";

				$this -> $currStyle = "";

			}

			return false;

		}

		

		$this -> valid = true;

		

		foreach ($this -> allFields as $field) {

			

			//TODO Should set up textfields to turn return chars into space on all sites

			

			

			

			

			if (!is_array($this -> request -> getProperty($field))) {

			

				$lcFieldValue = strtolower($this -> request -> getProperty($field));

				//Util::ShowDev($this -> request -> getProperty($field),$field);

				if ($field != "tbVWebsite") {

					if ( stripos($lcFieldValue, 'http') !== FALSE || stripos($lcFieldValue, 'https') !== FALSE )  {
						exit("Sorry no links");
						$this -> SetSpam ("http in $field");

						$this -> request -> setProperty($field, "");

					}

				}	

			

				if ($field != "tbQuestion" && $field != "tbMessage" && $field != "tbVDesc") {

					if ($this -> remove_headers($this -> request -> getProperty($field)) == false) {

						$this -> SetSpam ("extra headers in $field");

						$this -> request -> setProperty($field, "");

					}				

						

				} else {

					if ($this -> remove_textarea_headers($this -> request -> getProperty($field)) == false) {

						$this -> SetSpam ("extra headers in  textarea $field");

						$this -> request -> setProperty($field, "");

					}

				}

				

									

				if (!(stristr($lcFieldValue, '[link') == FALSE)) {

					$this -> SetSpam ("[link in $field");	

					$this -> request -> setProperty($field, "");

				}

				if (!(stristr($lcFieldValue, '[url') == FALSE)) {

					$this -> SetSpam ("[url in $field");	

					$this -> request -> setProperty($field, "");

				}

												

				//check type

				if (!$this -> CheckRightType ($field)) {

					$this -> SetSpam ("$field not string or boolean");

					$this -> request -> setProperty($field, "");

				}

				

				//check length

				//Util::Show(strlen($this -> request -> getProperty($field)),"length of $field");

				if ($field != "tbMessage" && $field != "tbVDesc" && $field != "tbQuestion" ) {

				  if (strlen($this -> request -> getProperty($field)) > 100) {

					  $this -> SetSpam ("$field got more than 100 characters");

					  $this -> request -> setProperty($field, "");

				  }

				} elseif (strlen($this -> request -> getProperty($field)) > 1500) {

					//$this -> SetSpam ("$field got more than 1500 characters");

					$this -> SetSpamlessMsg($this -> toolong_message);

					//$this -> request -> setProperty($field, "");

				}

				

			} else {

				foreach($this -> request -> getProperty($field) as $fieldItem) {

				

					$lcFieldItem = strtolower($fieldItem);

				

					if ($this -> remove_headers($fieldItem) == false) {

						$this -> SetSpam ("extra headers in arrayitem $fieldItem");

						//$this -> request -> setProperty($field, "");

					}

					

					if (!$this -> CheckRightType ($fieldItem)) {

						$this -> SetSpam ("arrayitem $fieldItem not string or boolean");

					}

					

					if (!(stristr($lcFieldItem, 'http') == FALSE && stristr($lcFieldItem, '[link') == FALSE)) {

						$this -> SetSpam ("links in arrayitem $fieldItem");

					}

					

					//check length

					if (strlen($fieldItem) > 100) {

						$this -> SetSpam ("$fieldItem got more than 100 characters");

					}

					

					

				}

			

			}

			

						

					$boxBase =  substr($field, 2);

					$currFormBox = "form" . $boxBase;

					$currStyle = "style" . $boxBase;

					$this -> $currFormBox = $this -> request -> getProperty($field);

					

					//Util::Show($this -> $currFormBox,$currFormBox);

					

					if (in_array($field, $this -> TBs)) {

						if (!($this -> request -> getProperty($field))) {

							if ($field != "tbQuestion") {

								$this -> $currStyle = "style=\"display:block\" ";

								$this -> styleFormResponse = "style=\"display:block\" ";

							}	else {

								$this -> $currStyle = "style=\"display:inline\" ";

								$this -> styleFormResponse = "style=\"display:inline\" ";

							}

							$this -> valid = false;

							$this -> SetSpamlessMsg($this -> error_message);

							//$this -> styleFormResponse = "style=\"display:block\" ";

						//not required fields

						} else {

							$this -> $currStyle = "";

							if ($field == "tbEmail") {

								if (!$this -> CheckEmail($this -> request -> getProperty($field))) {

									$this -> valid = false;

									$this -> SetSpamlessMsg($this -> error_message);

									//Util::Show(get_class($this));

									$this -> styleFormResponse = get_class($this) != "questionsChecker"?"style=\"display:block\" ":"style=\"display:inline\" ";

									$this -> styleValidEmail = " style=\"display:block\" ";

								}

							} 

						}

						

					} else {

						//Util::ShowTrue(!($this -> request -> getProperty($field)),$field);

						if (!($this -> request -> getProperty($field))) {

							$this -> request -> setProperty($field, "Nothing entered");

						}

					}

			

			

		}

		

		

		

		if ($this -> valid && $this -> request -> getProperty( "subhead" )== "quotation" && ($this -> styleLength == "style=\"display:block\" ")) {

			$this -> valid = false;

			$this -> formResponse = "All that's needed now is the length of your marquee...";

			$this -> styleFormResponse = "style=\"display:block\" ";

		}

		

			

		

		//Util::ShowTrue($this -> spam,"spam");

		//Util::ShowTrue($this -> valid,"valid");

		if ($this -> spam) {

			$this -> request -> addFeedback ($this -> myErrors);

			Util::SendFeedback($this -> request -> getFeedbackString());

		}

	

		return $this -> valid; //this should be sent to subclasses and they return it with any additional checking. So each subclass should have a check() function

	

	}

	

	

	

	

	

//***********************************************************************************************************

//UTILITY FUNCTIONS

	

	private function SetSpamlessMsg ($errorMsg) {

		if (!$this -> spam) {

			$this -> formResponse = $errorMsg;

		}

	}

	protected function SetSpam ($errorMessage) {

		//Util::Show($errorMessage,"setting spam");

		$ip = "";		

		  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {

			$ip=$_SERVER['HTTP_CLIENT_IP'];

		  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];

		  } else {

			$ip=$_SERVER['REMOTE_ADDR'];

		  }

		 $deny = array("96.47.225","173.44.37","96.47.224","112.5.234","5.188.211.43");

		foreach ($deny as $value) {

			if (strpos($ip, $value) !== false) {

				exit();

			}

			//$for_me .= "value: " . $value . " ip: " . $ip . "\n" . strpos($value, $ip) . "\n\n";

		}

		$this -> formResponse = $this -> spam_message;

		$this -> styleFormResponse = "style=\"display:block\" ";

		$this -> spam = true;

		$this -> valid = false;

		$this -> myErrors .= "\n" . $errorMessage . "\n and IP is " . $ip;

		//Util::ShowDev($this -> myErrors,"myErrors from SetSpam");// done on line 195

	}

	

	private function ShowValid ($TB) {

		$test = $this -> valid?"currently valid":"currently invalid";

		Util::Show ($test, $TB);

	}

	

	private function CheckRightType ($field) {

		if (!is_string($this -> request -> getProperty($field)) && !is_bool($this -> request -> getProperty($field))) {

			return false;			

				$this -> spam = true;					

				$this -> valid = false;		

				$this -> myErrors .= "$field not a string";

		} else {

			return true;

		}

	}

	

	protected function createSelect ($optionsArray, $selected) {

		$ret = $selected == false?"<option selected='selected' value='0'>Please select one</option>":"";

		//ShowSomething($selected,"selected");

		foreach ($optionsArray as $option) {

			if ($selected == $option) {

				$ret .= "<option selected='selected'>" . $option . "</option>\n";

			} else {

				$ret .= "<option>" . $option . "</option>\n";

			}

		}

		

		//ShowSomething("<pre>$ret</pre>");

		return $ret;

	}

	

	protected function CheckCheckBox ($cb) {

		if($cb) {

			$cbChecked = str_replace(" ", "", $cb);

			$this -> $cbChecked = "checked=\"checked\"";

			return true;		

		} else {

			return false;

		}

	}

	

	protected function CheckCheckboxes($cbArray) {

	

		foreach ($cbArray as $actualcb) {

			if (!is_string($actualcb)) {

				return false;

			}

		}

		

		foreach ($this -> checkBoxes as $cb) {

			//Util::Show( $cb,"cb");

			if (in_array($cb, $cbArray)) {

				$cbChecked = str_replace(" ","",$cb);

				//Util::Show( $cbChecked);

				$this -> $cbChecked = "checked";

			}		

		}

		return true;

	}

	

	private function CheckEmail($email) {

		 if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

			return true;

		} else {

			return false;

		}

	}

	

	private function remove_headers($string) { 

	  $headers = array(

		"/to\:/i",

		"/from\:/i",

		"/bcc\:/i",

		"/cc\:/i",

		"/Content\-Transfer\-Encoding\:/i",

		"/Content\-Type\:/i",

		"/Mime\-Version\:/i" 

	  );

	  $string = strtolower($string);

	  if (preg_replace($headers, '', $string) == $string) {

	  	if(strstr($string, PHP_EOL)) { 

			return false;

		}

		return true;

	  } else {

		return false;

	  }

	}

	

	private function remove_textarea_headers($string) { 

		$headers = array(

		"/to\:/i",

		"/from\:/i",

		"/bcc\:/i",

		"/cc\:/i",

		"/Content\-Transfer\-Encoding\:/i",

		"/Content\-Type\:/i",

		"/Mime\-Version\:/i" 

	  ); 

	  if (preg_replace($headers, '', $string) == $string) {

		return true;

	  } else {

		return false;

	  }	

	

	}

	

	private function replace_returns ( $string ) {

		$remove = array("\n", "\r\n", "\r");

   		 $string = empty($string)?"No address entered":str_replace($remove, ' | ',  $string);

		 return $string;

	}

}	

	







//***********************************************************************************************************

//INDIVIDUAL CHECKERS



class contactChecker extends formChecker {

		

	public $howheardDD;

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array("tbName","tbEmail", "tbTel","tbMessage","tbFunction");

		$this -> allFields = array("tbName","tbEmail", "tbTel","tbMessage","tbFunction","cbBrochure","cbQuote","cbAdvice","ddType_of_function","ddHowheard","tbDate","tbNumbers");

		$this -> errorStyles = array("styleName","styleEmail","styleTel","stylePostcode","styleAdress","styleFunction","styleMessage");

		$this -> request = $request;

		

		//selects

		$selected = $this -> request -> getProperty("ddType_of_function");		

		$this -> eventsDD = $this -> createSelect($this -> arrEventType, $selected);		

		$howheard_selected = $this -> request -> getProperty("ddHowheard");		

		$this -> howheardDD = $this -> createSelect($this -> arrHowheard, $howheard_selected);

		

		//checkboxes

		$this -> Wantsbrochure = "";

		$this -> Wantsadvice = "";

		$this -> Wantsquote = "";

		$this -> Wouldlikephonecall = "";

		$this -> checkBoxes = array("Wants brochure", "Wants advice","Wants quote","Would like phone call");

		

		if ($this -> request -> getProperty("cb")) {

			$test = $this -> CheckCheckboxes($request ->getProperty("cb"));

			if ( !($test) ) {

				$this -> SetSpam("Not string in checkbox field");

				Util::SendFeedback($this -> myErrors);

				return false;

			}

		}

		

		parent::__construct();

	}	



}



class add_venueChecker extends formChecker {



	public $arrPriceBands = array("Less than &pound;1000","Between &pound;1000 &amp; &pound;4000","More than &pound;4,000");

	public $Civilceremonies = "";

	public $CMlink = "<a href=&quot;https://www.countymarquees.com&quot;>County Marquees</a>";

	public $VenueLink = "<a href=&quot;https://www.countymarquees.com/venues.htm&quot;>Marquee venue finder</a>";

	public $stylePrice = "";	

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array("tbName","tbEmail","tbVName","tbVDesc","tbVTel","tbVWebsite", "tbVPostcode");

		$this -> allFields = array("tbName","tbEmail","tbMessage","tbVName","tbVDesc","cbCivil","tbCCnums","tbVTel","tbVWebsite", "tbVPostcode","tbCMlink","tbVenueLink");

		$this -> errorStyles = array("styleName","styleEmail","styleVName","styleVDesc","styleVTel","styleVWebsite", "styleVPostcode");

		$this -> request = $request;

		$selected = $this -> request -> getProperty("ddPrice");		

		$this -> formPrice = $this -> createSelect($this -> arrPriceBands, $selected);

		

		if ($request -> getProperty("contactSubmit")) {

			$this -> CheckCheckBox($this -> request -> getProperty("cbCivil"));	

		} else {

			

		}		 

			

		parent::__construct();

		

	}



}



class jobsChecker extends formChecker {



	

	public $gotdrivinglicense = "";

	public $nodrivinglicense = "";

	public $gotowntransport = "";

	public $notransport = "";

	public $Needsworkpermit = "";

	public $Dontneedworkpermit = "";

	

	function __construct ( CM_Request $request) {

	//Util::Show("in jobschecker");

		$this -> TBs = array("tbName","tbEmail","tbAdress","tbPostcode", "tbTel","tbMessage","rbDL", "rbOT","rbWP");

		$this -> allFields = array("tbName","tbEmail","tbAdress","tbPostcode", "tbTel","tbCounty","tbMessage","rbDL", "rbOT","rbWP");

		$this -> errorStyles = array("styleName","styleEmail","styleTel","stylePostcode","styleAdress","styleMessage");

		$this -> request = $request;

		

		if ($request -> getProperty("contactSubmit")) {

			$this -> CheckRadioboxes();

		}

		

		parent::__construct();

	}

	

	private function CheckRadioboxes() {

	

		$radioBoxes = array("rbDL", "rbOT","rbWP");

		

		foreach ($radioBoxes as $rb) {

			if ($this -> request -> getProperty($rb)) {

				$arrayName = $this -> request -> getProperty($rb);

				$rbChecked = str_replace(" ", "", $arrayName[0]);

				$this -> $rbChecked = "checked=\"checked\"";

			}		

		}

	

	}



}



class loginChecker extends formChecker {

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array("tbName","tbPassword");

		$this -> allFields = array("tbName","tbPassword");

		$this -> errorStyles = array("styleName","stylePassword");

		$this -> request = $request;

		parent::__construct();

	}

	

	function Check() {

			//Util::ShowDev("in subcheck");

			parent::Check();

			

			if ($this -> valid) {

			

				require_once( '../cm_config.php' );

				

				if ( ! (md5($this -> request -> getProperty("tbName")) == $loginuser && md5($this -> request -> getProperty("tbPassword")) == $loginpw) ) {

				 	$this -> valid = false;

					//Util::ShowDev("oops");

				} else {

					/*session_set_cookie_params ( 1 );

					session_start();

					if (!isset($_SESSION['auth'])) {

						$_SESSION["auth"] = "staffmember";

					}*/

					//setcookie("staff", "auth", time()+(60*60*24*30)); //30 days

					setcookie("staff", "auth", time()+(60*60*24));//1 day

					//setcookie("staff");//to unset cookie

				}

			}

			return $this -> valid;

	}

}





class questionsChecker extends formChecker {

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array("tbEmail","tbQuestion");

		$this -> allFields = array("tbName","tbEmail","tbQuestion");

		$this -> errorStyles = array("styleEmail","styleQuestion");

		$this -> request = $request;

		parent::__construct();

	}

}



class widgetChecker extends formChecker {	

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array();

		$this -> allFields = array("rbSize");

		$this -> errorStyles = array("styleQuestion");

		$this -> request = $request;

		parent::__construct();

	}

}



class quoteChecker extends formChecker {

	

	private $arrWidth = array("4 metres","6 metres","9 metres","12 metres","15 metres");

	private $arrLengthStart = array("Choose width first");

	public $widthDD;

	public $lengthDD;

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array();

		$this -> allFields = array();

		$this -> errorStyles = array();

		$this -> request = $request;

		

		//$widthSelected = $this -> request -> getProperty("ddWidth") != "0"?$this -> request -> getProperty("ddWidth"):false; //checks for 		

		$this -> widthDD = $this -> createSelect($this -> arrWidth, false);

		$this -> lengthDD = $this -> createSelect($this -> arrLengthStart, $this -> arrLengthStart[0]);

		

		parent::__construct();

	}

}



class quote_testChecker extends formChecker {

	

	private $arrWidth = array("4 metres","6 metres","9 metres","12 metres","15 metres");

	private $arrLengthStart = array("Choose width first");

	public $widthDD;

	public $lengthDD;

	public $staff = false;

	

	function __construct ( CM_Request $request) {

		$this -> TBs = array();

		$this -> allFields = array();

		$this -> errorStyles = array();

		$this -> request = $request;

		

		/*if (isset($_COOKIE["staff"])) {

			Util::ShowDev($_COOKIE["staff"],"cookie val from formchecker");

		 }*/

		 

		if (isset($_COOKIE["staff"]) && $_COOKIE["staff"]  === "auth") {

			$this -> staff = true;		

		}

		

		//$widthSelected = $this -> request -> getProperty("ddWidth") != "0"?$this -> request -> getProperty("ddWidth"):false; //checks for 		

		$this -> widthDD = $this -> createSelect($this -> arrWidth, false);

		$this -> lengthDD = $this -> createSelect($this -> arrLengthStart, $this -> arrLengthStart[0]);

		

		parent::__construct();

		

		

	}

}







class quotationChecker extends formChecker {

	

	private $arrWidth = array("4 metres","6 metres","9 metres","12 metres","15 metres");

	private $arrLengthStart = array("Choose width first");

	private $arr4Length = array("6 metres","9 metres","12 metres","15 metres");

	private $arr6Length = array("6 metres","9 metres","12 metres","15 metres","18 metres","21 metres","24 metres");

	private $arr9Length = array("9 metres","12 metres","15 metres","18 metres","21 metres","24 metres","27 metres","30 metres");

	private $arr12Length = array("12 metres","15 metres","18 metres","21 metres","24 metres","27 metres","30 metres","33 metres","36 metres");

	private $arr15Length = array("15 metres","18 metres","21 metres","24 metres","27 metres","30 metres","33 metres","36 metres","39 metres","42 metres");

	public $widthCheck;

	public $widthDD;

	public $lengthDD;

	public $howheardDD;

	public $Seatedevent = "";

	public $Wantsdancefloor = "";

	public $Wantsstage = "";

	public $Wantscateringtent = "";

	public $Needbuffetarea = "";

	public $Needbararea = "";

	public $styleLength = "";

	

	function __construct ( CM_Request $request) {

	

		$this -> TBs = array("tbName","tbEmail","tbAdress","tbPostcode", "tbTel","tbNumbers");

		$this -> allFields = array("tbName","tbEmail","tbAdress","tbPostcode", "tbTel","tbMessage","tbFunction","ddType_of_function","ddHowheard","tbDate","tbNumbers","ddWidth","ddLength","tbWidthCheck");

		$this -> errorStyles = array("styleName","styleEmail","styleTel","stylePostcode","styleAdress","styleNumbers");

		

		$this -> request = $request;

		

		$widthSelected = $this -> request -> getProperty("ddWidth") != "0"?$this -> request -> getProperty("ddWidth"):false; //checks for 		

		$this -> widthDD = $this -> createSelect($this -> arrWidth, $widthSelected);

		$lengthSelected = false;

		$this -> checkBoxes = array("Seated event", "Wants dance floor","Wants stage","Wants catering tent","Need buffet area","Need bar area");

		

		//create length drop down

		if ($widthSelected) {

		

			$this -> widthCheck = $widthSelected;

			$intWidth = substr($widthSelected, 0 , strpos($widthSelected, " "));

			$lengthArray = "arr" . $intWidth . "Length";

			//ShowSomething($this -> request -> getProperty("ddLength"),"ddLength");

			

			//if width has changed, check length still ok

			if ($this -> request -> getProperty("tbWidthCheck") != $widthSelected) {

				if (in_array( $this -> request -> getProperty("ddLength"), $this -> $lengthArray)) {

					$lengthSelected = $this -> request -> getProperty("ddLength");

				}

			} else {

				$lengthSelected = $this -> request -> getProperty("ddLength");

			}

						

			$this -> lengthDD = $this -> createSelect($this -> $lengthArray, $lengthSelected);

			

			if (!$lengthSelected) {

				$this -> styleLength =  "style=\"display:block\" ";

			} else {

				$this -> styleLength =  "";

			}		

			

		} else {

			$this -> lengthDD = $this -> createSelect($this -> arrLengthStart, $this -> arrLengthStart[0]);

		}

		

		//checkboxes

		if ($this -> request -> getProperty("cb")) {

			$test = $this -> CheckCheckboxes($request ->getProperty("cb"));

			if ( !($test) ) {

				$this -> SetSpam("Not string in checkbox field");

				Util::SendFeedback($this -> myErrors);

				return false;

			}

		}

		

		//selects

		$selected = $this -> request -> getProperty("ddType_of_function");		

		$this -> eventsDD = $this -> createSelect($this -> arrEventType, $selected);

		$howheard_selected = $this -> request -> getProperty("ddHowheard");		

		$this -> howheardDD = $this -> createSelect($this -> arrHowheard, $howheard_selected);

			

		

		parent::__construct();

	}



}





?>

