<?php

//IN USE 11/14

//TODOTODOTODOTODOTODO allow sending of up to 3? of the same quote - find out where this is set

error_reporting( E_ALL );

ini_set( "display_errors", 1 );

require_once( 'errors.php' );



class ajaxFormChecker {

	

	public $retval;

	private $retvalmarquees;

	private $retvalfurn = "";

	

	//these two used to determine which types of text to return

	private $dims;

	private $mail;

	private $staffMail;

	private $source_for_analytics;

	private $htmlMail;

	private $htmlMail_content = "";

	private $mail_topimg;

	

	private $TBs = array("tbNumbers","rbLining","rbSeated");

	private $allFields = array("tbNumbers" => "string", "rbSeated" => "array","ddWidth" => "string", "ddLength" => "string","cb" => "array","mailName" => "string","mail" => "string","staff" => "string","mailExtra" => "multiline");

	private $extras = array("Hard", "Stage", "Buffet", "Bar", "Dance floor", "Parquet", "Starlight", "Starlight dancefloor", "Catering tent", "Toilets", "Generator","Heater");	

	private $allValues = array("tbNumbers" => "","rbLining" => "","rbSeated" => "","ddWidth" => "", "ddLength" => "", "mailExtra" => "", "mail" => "", "staff" => "", "mailName" => "");

	private $curr_extras = array();

	

	private $valid;

	private $spam = false;

	private $myErrors;

	

	private $spam_message = "<h3>Error</h3><a class='close' title='Close this'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>\n<p>Sorry. There has been a problem with your details. Please try again.</p>";

	

	private $notworking_message = "<h3>Sorry</h3><a class='close' title='Close this'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>\n<p class='topp'>Something has gone wrong. The problem may be nothing more than a blip. So please request your quote again. If that doesn't work, please <a href='/contact_us.htm'>contact</a> or call us. We will be happy to help.</p>";

	

	private $errorstart_message = "<h3>Problem...</h3><a class='close' title='Close this'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>\n";

	

	private $quote_start = "<h3>Your quote</h3><a class='close' title='Remove this quote'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>

	<a class='printbut' title='Printer friendly quote' href='/pricing/quote.htm#printing'>\n<img width='50' height='12' alt='Printer friendly' src='/images/print_text.png'></a>

	<a class='mailbut' title='Email quote'>\n<img width='50' height='12' alt='Email quote' src='/images/mail_text.png'></a>

	<p class='topp'>The marquee/s recommended below will give you a <em>rough idea</em> of the price for the sort of marquee you would like. You can <a class='printtextbut' href='/pricing/quote.htm#printing' style='cursor: pointer;'>print out</a> or <a class='mailtextbut' style='cursor: pointer;'>email</a> this quote.</p>\n<p class='topp'>For more help, please <a href='/contact_us.htm'>contact us</a>. Or if you have have a clear idea of what you want, request an accurate, personalised <a href='/pricing/quotation.htm'>quote</a>.</p><h3 style='margin-top:7px'>Suggested marquee/s:</h3>";

	

	private $empty_quote_start = "<h3>Your quote</h3><a class='close' title='Remove this quote'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>\n";

	

	private $quote_end = "<p class='topp'>* VAT not included</p><p><a class='printtextbut' href='/pricing/quote.htm#printing' style='cursor: pointer;'>Print out</a> or <a class='mailtextbut' style='cursor: pointer;'>email</a> this quote.</p><p><strong>Or for further help, please <a href='/contact_us.htm'>contact us</a></strong>.</p>

	<a class='close' title='Close this'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>

	<a class='printbut' title='Printer friendly quote' href='/pricing/quote.htm#printing'>\n<img width='50' height='12' alt='Printer friendly' src='/images/print_text.png'></a>

	<a class='mailbut' title='Email quote'>\n<img width='50' height='12' alt='Email quote' src='/images/mail_text.png'></a>\n<div class='unnec'></div>";

	private $no_quote_start = "<h3>Your quote</h3><a class='close' title='Remove this quote'>\n<img width='12' height='12' alt='Close' src='/images/close.png'></a>\n";

	

//**************************MAIL	

	private $mail_start = "Here is the approximate marquee quote you requested from County Marquees' online quote page.\n\nIf you have any questions or would like to take things further, please call us on 020 7267 4271 or contact us online at https://www.countymarquees.com/contact_us.htm?utm_source=sent&utm_medium=email&utm_campaign=oq.\n\n";

	private $html_mail_start = "<p>Here is the approximate marquee quote you requested from County Marquees' online quote page.</p><p>If you have any questions or would like to take things further, please call us on 020 7267 4271 or <a href='https://www.countymarquees.com/contact_us.htm?utm_source=sent&utm_medium=email&utm_campaign=oq'>contact us online</a>.</p>";

	

	private $mail_title = "MARQUEE QUOTATION";

	private $html_mail_title = "<p style='font-size:14pt;color:#175179;'>Marquee quotation";

	

	private $mail_end = "\n\n* VAT not included\n\nThese numbers should give you a rough idea of the price for the sort of marquee you would like. But they are based on average requirements. For more accurate advice, please call on <strong>020 7267 4271</strong> or contact us online at \nhttps://www.countymarquees.com/contact_us.htm?utm_source=#whichsource#&utm_medium=email&utm_campaign=oq \nWe are always happy to help\n\n";

	private $html_mail_end = "<p>* VAT not included</p><p>These numbers should give you a rough idea of the price for the sort of marquee you would like. But they are based on average requirements. For more accurate advice, please call on <strong>020 7267 4271</strong> or <a href='https://www.countymarquees.com/contact_us.htm?utm_source=#whichsource#&utm_medium=email&utm_campaign=oq'>contact us online</a>. We are always happy to help</p>";

	

	private $send_mail_end = "Or, if you have a clear idea of what you want, you can request an accurate, personalised quote at \nhttps://www.countymarquees.com/pricing/quotation.htm?utm_source=sent&utm_medium=email&utm_campaign=oq";

	private $html_send_mail_end = "<p>Or, if you have a clear idea of what you want, you can request an <a href='https://www.countymarquees.com/pricing/quotation.htm?utm_source=sent&utm_medium=email&utm_campaign=oq'>accurate, personalised quote</a>";

	

	private $staff_mail_extra = "If you have any questions or would like to take things further, please call me on 020 7267 4271 or contact me online at https://www.countymarquees.com/contact_us.htm?utm_source=staff&utm_medium=email&utm_campaign=oq \n\n";

private $staff_htmlmail_extra = "<p>If you have any questions or would like to take things further, please call me on 020 7267 4271 or <a href='https://www.countymarquees.com/contact_us.htm?utm_source=staff&utm_medium=email&utm_campaign=oq'>contact me online</a>.</p>";

	

	private $mail_sig = "\n\nYours\n\nDavid Higgs\nT: 020 7267 4271\nW: https://www.countymarquees.com";

	private $html_mail_sig = "<p>Yours</p><p>David Higgs<br/>T: 020 7267 4271<br/>W: <a href='https://www.countymarquees.com?utm_source=sent_sig&utm_medium=email&utm_campaign=oq'>www.countymarquees.com</a></p>";

	

	private $mail_from = "david@countymarquees.com";

	private $mail_from_name = "David Higgs";

	private $copy_mail = "";

	



	function __construct () {

		

		 //$this -> SetUpJsonArray("Constructing" . $this -> notworking_message, "problem");

		 

		if (!strpos($_SERVER['HTTP_REFERER'],"countymarquees.com")) {

			$this -> SetSpam("CM not referrer");

		} else {

			$this -> SetUp();

		}

		

		//to email, think would be best to set up a mini controller after setup which would do rest of this construct and get marquees (dims or no dims), get furn (web or mail version), then either return results or email and return maiul success message)

		if (!$this -> spam) { //though shouldnt be cos should have exited in setspam

			require_once(  dirname(__FILE__) . '/get_xml.php' );

			

			$this -> mail = isset($_GET["mail"]);

			$this -> staffMail = isset($_COOKIE["staff"])?($_COOKIE["staff"] === "auth"?true:false):false;

		

			if ($this -> allValues["ddWidth"] == "0") {

				$this -> dims = false;

				$this -> GetXMLnoDims();

			} else {

				 $this -> dims = true;

				 $this -> GetXMLdims();

			}

		}

		

		//echo  $this -> retval;

		/*print_r($this -> allValues) . "<br/>";

		print_r ($this -> curr_extras);

		exit();*/

		

	}

	

	private function GetXMLnoDims() {

		

		$intNumbers = (int) $this -> allValues['tbNumbers'];

		

		if ( count($this -> curr_extras) > 0 ) {

			

			if (in_array("Dance floor",$this -> curr_extras)) {

				

				if ($intNumbers <= 100) {

					$intNumbers += 20;

				} elseif ($intNumbers <= 200) {

					$intNumbers += 40;

				} elseif ($intNumbers <= 300) {

					$intNumbers += 60;

				} else {

					$intNumbers += 80;

				}

				

			} else {

				

				if (in_array("Stage",$this -> curr_extras)) {

					if ($intNumbers <= 100) {

						$intNumbers += 10;

					} else {

						$intNumbers += 20;

					}

				}				

				if (in_array("Bar",$this -> curr_extras)) {

					if ($intNumbers <= 100) {

						$intNumbers += 10;

					} else {

						$intNumbers += 20;

					}

				}

				

			}

			

			if (in_array("Buffet",$this -> curr_extras)) {

				if ($intNumbers <= 100) {

						$intNumbers += 10;

				} else {

					$intNumbers += 20;

				}

			}

			

			if ( ($_GET["rbSeated"][0] == "1" && $intNumbers > 675) || ($_GET["rbSeated"][0] == "0" && $intNumbers > 800)) {

							$this -> SetSpam( "For very large events, we find that an online quote is too vague. Please <a href='/contact_us.htm'>contact us</a> for a more realistic estimate. Or, if you have a clear idea of what you would like, <a href='/pricing/quotation.htm'>request a no-obligation quotation</a>.", true );

			}

		}

		

		$numbersUpperLimit = $intNumbers + 25;		

		

		$xpath = $this -> allValues["rbSeated"] == "0"?"row[@buffetCap >= $intNumbers and @buffetCap <= $numbersUpperLimit]":"row[@seatedCap >= $intNumbers and @seatedCap <= $numbersUpperLimit ]";

		

		$marqueesGetter = new Get_onlinequote_xml("../xml/marquee_prices.xml");

		

		if ($marqueesGetter -> xml) {

			

			$this -> retvalmarquees = $marqueesGetter ->  GetMarquees($xpath);

			

			

			if (!$this -> mail) {

				$this -> GetMarqueesText();

			} else {

				$this -> GetMail();

			}

			 

		} else {

			 $this -> SetUpJsonArray($this -> no_quote_start . $this -> notworking_message, "problem");

		}

		

	}

	

	

	

	

	private function GetXMLdims() {

		//$intNumbers = (int) $this -> allValues['tbNumbers'];

		$intWidth = (int)$this -> allValues['ddWidth'];

		$intLength = (int)$this -> allValues['ddLength'];

		

		

		$marqueesDimsGetter = new Get_onlinequote_xml("../xml/marquee_prices.xml");

		if ($marqueesDimsGetter -> xml) {

			

			$this -> retvalmarquees = $marqueesDimsGetter ->  GetDimsMarquees($intWidth, $intLength);

			

			/*$this -> retval = $this -> quote_start;

			

			if (count($this -> curr_extras) > 0 || $this -> allValues["rbSeated"] == "1") {

				

				//return text set up in getfurniture

				$this -> GetFurniture(true);

			  

			} else {

				foreach ($this -> retvalmarquees as $marquee) {

					  $this -> retval .= "\n<p><strong>A {$marquee -> mWidth}m x {$marquee -> mLength}m ({$marquee -> fWidth}' x {$marquee -> fLength}') {$marquee -> type} marquee</strong><br/>Fully lined, matted and lit (candelabras or uplighters)<br/>\n";					  

					  $this -> retval .= $this -> GetTotals((int)$marquee -> total);

					  

				}				

			}

			

			$this -> retval .= $this -> quote_end;*/

			

			//$this -> GetMarqueesText();

			

			if (!$this -> mail) {

				$this -> GetMarqueesText();

			} else {

				$this -> GetMail();

			}

		

		//no marquee xml

		} else {

			 $this -> SetUpJsonArray($this -> no_quote_start . $this -> notworking_message, "problem");

		}

		

	}

	

	private function GetMail() {

		

		//no marquees found

		if (is_string($this -> retvalmarquees)) {

			$this -> SetUpJsonArray($this -> no_quote_start . $this -> retvalmarquees, "success");

		}

		

		//$this -> retval = !$this -> staffMail?$this -> mail_start:"Form text";

		

		

		if ($this -> dims) {

			$this -> mail_title .= ":\n\n";

			$this -> html_mail_title .= ":</p>";

		} else {

			$this -> mail_title .= " - marquees suitable for " . $this -> allValues["tbNumbers"] . " people:\n\n";

			$this -> html_mail_title .= " - marquees suitable for " . $this -> allValues["tbNumbers"] . " people:</p>";

		}

		$this -> retval = "FAO " . ucwords($this -> allValues["mailName"]) . ":\n\n";

		$this -> htmlMail_content = "<p style='margin-bottom:5px'><strong>FAO " . ucwords($this -> allValues["mailName"]) . ":</strong></p>";

		

		if (!$this -> staffMail) {

			$this -> retval .= $this -> mail_start . $this -> mail_title;

			$this -> htmlMail_content .= $this -> html_mail_start . $this -> html_mail_title;

		} else {

			$this -> GetPhone ();

			$this -> retval .= $this -> allValues["mailExtra"] . $this -> staff_mail_extra . $this -> mail_title;

			$this -> htmlMail_content .= "<p>" . $this -> allValues["mailExtra"] . "</p>". $this -> staff_htmlmail_extra . $this -> html_mail_title;

		}

		

		

			

			if (count($this -> curr_extras) > 0 || $this -> allValues["rbSeated"] == "1") {

				

				//return text set up in getfurniture; dims parameter, set in __construct, determines how tables and chairs calculated;

				$this -> GetMailFurniture($this -> dims);

				$this -> retvalfurn = ""; //clear values set up in pre line ready for next line

				$this -> GetFurniture($this -> dims); //for html mail

			  

			} else {

				

				foreach ($this -> retvalmarquees as $marquee) {

					  $this -> retval .= "\n\nA {$marquee -> mWidth}m x {$marquee -> mLength}m ({$marquee -> fWidth}' x {$marquee -> fLength}') {$marquee -> type} marquee\nFully lined, matted and lit (candelabras or uplighters)";

					  $this -> htmlMail_content .= "<p style='margin-bottom:0;padding-bottom:0'><strong>A {$marquee -> mWidth}m x {$marquee -> mLength}m ({$marquee -> fWidth}' x {$marquee -> fLength}') {$marquee -> type} marquee</strong><br/>";

					  

					  $this -> retval .= $this -> GetMailTotals((int)$marquee -> total);

					   $this -> htmlMail_content .= $this -> GetHtmlMailTotals((int)$marquee -> total);

					  

				}				

			}

			

			$this -> GetMailEndWithAnalytics ();

			$this -> retval .= $this -> mail_end;

			 $this -> htmlMail_content .=  $this -> html_mail_end;

			 

			

			$this -> mail_topimg = "/home/countyma/public_html/images/email/top/email_image_template.jpg";

			

			if (!$this -> staffMail) {

				$this -> retval .= $this -> mail_sig;

				 $this -> htmlMail_content .=  $this -> html_mail_sig;

			} else {

				//TODO this

				switch ($this -> allValues["staff"]) {

					case "Matthew Higgs":

						$this -> retval .= "\n\nYours\n\nMatthew Higgs\n\nT: 01243 790290\nW: https://www.countymarquees.com";

						$this -> htmlMail_content .=  "<p>Yours</p><p>Matthew Higgs<br/>T: 01243 790290<br/>W: <a href='https://www.countymarquees.com?utm_source=sent_sig&utm_medium=email&utm_campaign=oq'>www.countymarquees.com</a></p>";

						$this -> mail_topimg = "/home/countyma/public_html/images/email/top/image_template_mat.jpg";

						$this -> mail_from_name = "Matthew Higgs";

						$this -> mail_from = "matthew@countymarquees.com";

						$this -> copy_mail = "countymarquees@gmail.com";

						break;

					case "Matthew Higgs Surrey":

						$this -> retval .= "\n\nYours\n\nMatthew Higgs\n\nT: 01483 538617\nW: https://www.countymarquees.com";

						$this -> htmlMail_content .=  "<p>Yours</p><p>Matthew Higgs<br/>T: 01483 538617<br/>W: <a href='https://www.countymarquees.com?utm_source=sent_sig&utm_medium=email&utm_campaign=oq'>www.countymarquees.com</a></p>";

						$this -> mail_topimg = "/home/countyma/public_html/images/email/top/email_mat_surrey.jpg";						

						$this -> mail_from_name = "Matthew Higgs";

						$this -> mail_from = "matthew@countymarquees.com";

						$this -> copy_mail = "countymarquees@gmail.com";

						break;

					case "Shaun Haworth":

						$this -> retval .= "\n\nYours\n\nShaun Haworth\n\nT: 01892 5068701\nW: https://www.countymarquees.com";						

						$this -> htmlMail_content .=  "<p>Yours</p><p>Shaun Haworth<br/>T: 01892 5068701<br/>W: <a href='https://www.countymarquees.com?utm_source=sent_sig&utm_medium=email&utm_campaign=oq'>www.countymarquees.com</a></p>";

						$this -> mail_topimg = "/home/countyma/public_html/images/email/top/image_template_shawn.jpg";						

						$this -> mail_from_name = "Shaun Haworth";

						$this -> mail_from = "shaun@countymarquees.com";

						$this -> copy_mail = "countymarqueesevents@gmail.com";

						break;


					default:

						$this -> retval .= $this -> mail_sig;

						$this -> htmlMail_content .=  $this -> html_mail_sig;

						$this -> copy_mail = "countymarquees@hotmail.com";

				}

			}

			

			

			

			

			$this -> SendMail();

			

			

		

	}

	

	private function SendMail () {

	

	  date_default_timezone_set('Europe/London');

	  require_once( "/home/countyma/classes/swift/Swift406/lib/swift_required.php");

	  Swift_Preferences::getInstance()->setCharset('iso-8859-2');

	  

	  $transport = Swift_MailTransport::newInstance();

	  //$transport = Swift_SmtpTransport::newInstance('79.135.125.10');

	  $mailer = Swift_Mailer::newInstance($transport);

	  

	  $message = Swift_Message::newInstance();

	  $msg_susbject =  "Online marquee quote for " . ucwords($this -> allValues["mailName"]) . " from County Marquees";

	   $copy_msg_susbject =  "Copy of online marquee quote for " . ucwords($this -> allValues["mailName"]);

	  $message->setSubject($msg_susbject);

	  

	  //set up html mail

	  $topimg = $message->embed(Swift_Image::fromPath($this -> mail_topimg));

	  $campaign = "oqtest";

	  include( "/home/countyma/public_html/includes/email/template_main.php");

			  

	  $this -> htmlMail = str_replace(array("#img#","#mail_content#"), array($topimg, $this -> htmlMail_content), $htmlmail);

	  

	  $message->setBody($this -> htmlMail, 'text/html');

	  $message->addPart($this -> retval, 'text/plain');

	  $message->setTo(array($this -> allValues["mail"] => ucfirst($this -> allValues["mailName"])));

	  $message->setFrom(array($this -> mail_from  =>  $this -> mail_from_name));

	  $message->setSender($this -> mail_from);

	//$message->setReturnPath('bounces@countymarquees.com');

	



		if ($mailer->send($message)) {

			if ($this -> copy_mail != "") {

			 $message->setTo($this -> copy_mail);

			  $message->setSubject($copy_msg_susbject);

			 $mailer->send($message);

			}

			 $this -> SetUpJsonArray("<p class='error topp success' id='mailResponse'>Your approximate online quote has been sent. We hope it proves helpful.</p>", "mail");

			  

		} else {

			$this -> SetUpJsonArray("<p class='error topp' id='mailResponse'>Sorry. We are unable to send mail at this time. It may be a temporary blip, in which case trying again should solve the problem. If not, please accept our apologies. If it would help, you can always print the quote from the print button above instead.</p>", "mail");

		}

		//Need alternate email addresses?

			/*if (mail($this -> allValues["mail"], 

			  "Online quote from County Marquees",

			  $this -> retval,

			  "From:\"David Higgs\" <david@countymarquees.com>",

			  "-f david@countymarquees.com" )) {

					

				  /*mail("fdl4712@aol.com", 

					"Online quote from County Marquees",

					$this -> retval,

					"From:\"David Higgs\" <david@countymarquees.com>",

					"-f david@countymarquees.com" );

				  $this -> SetUpJsonArray("<p class='error topp success' id='mailResponse'>Your approximate online quote has been sent. We hope it proves helpful.</p>", "mail");

			  

		  } else {

			  $this -> SetUpJsonArray("<p class='error topp id='mailResponse'>Sorry. We are unable to send mail at this time. It may be a temporary blip, in which case trying again should solve the problem. If not, please accept our apologies. If it would help, you can always print the quote from the print button above instead.</p>", "mail");

		  }*/

	}

	

	private function GetMarqueesText() {

		

		//no marquees found

		if (is_string($this -> retvalmarquees)) {

			$this -> SetUpJsonArray($this -> no_quote_start . $this -> retvalmarquees, "success");

		}

		

		$this -> retval = $this -> quote_start;

			

			if (count($this -> curr_extras) > 0 || $this -> allValues["rbSeated"] == "1") {

				

				//return text set up in getfurniture; dims parameter, set in __construct, determines how tables and chairs calculated;

				$this -> GetFurniture($this -> dims);

			  

			} else {

				

				foreach ($this -> retvalmarquees as $marquee) {

					  $this -> retval .= "\n<p><strong>A {$marquee -> mWidth}m x {$marquee -> mLength}m ({$marquee -> fWidth}' x {$marquee -> fLength}') {$marquee -> type} marquee</strong><br/>Fully lined, matted and lit (candelabras or uplighters)<br/>\n";					  

					  $this -> retval .= $this -> GetTotals((int)$marquee -> total);

					  

				}				

			}

			

			$this -> retval .= $this -> quote_end;			

			$this -> SetUpJsonArray($this -> retval, "success");

	}

	

	

	

	private function GetTotals($total_so_far) {

		$damage = $total_so_far * 0.05;

		$final_damaged = $total_so_far + $damage;

		$ret = "</p>\n<p>TOTAL COST: &pound;" .  sprintf("%01.2f", $total_so_far) . "<br/>\n";

		$ret .= "\nDamage waiver @ 5%: &pound;" .  sprintf("%01.2f", $damage) . "</p>\n";

		$ret .= "\n<p class='final'>FINAL COST: &pound;" .  sprintf("%01.2f", $final_damaged) . "*</p>\n\n";

		return $ret;

	}

	

	private function GetHtmlMailTotals($total_so_far) {

		$damage = $total_so_far * 0.05;

		$final_damaged = $total_so_far + $damage;

		$ret = "</p>\n<p style='margin-top:5px;margin-bottom:5px'>TOTAL COST: &pound;" .  sprintf("%01.2f", $total_so_far) . "<br/>\n";

		$ret .= "\nDamage waiver @ 5%: &pound;" .  sprintf("%01.2f", $damage) . "</p>\n";

		$ret .= "\n<p class='final'>FINAL COST: &pound;" .  sprintf("%01.2f", $final_damaged) . "*</p>\n\n";

		return $ret;

	}

	

	private function GetPhone () {

		

		if ($this -> allValues["staff"] != "David Higgs") {

			

			switch ($this -> allValues["staff"]) {

					case "Matthew Higgs":

						$this -> staff_mail_extra = str_replace("020 7267 4271", "01243 790290",$this -> staff_mail_extra);

						$this -> staff_htmlmail_extra  = str_replace("020 7267 4271", "01243 790290",$this ->staff_htmlmail_extra);

						$this -> mail_end = str_replace("020 7267 4271", "01243 790290",$this -> mail_end);

						$this -> html_mail_end = str_replace("020 7267 4271", "01243 790290",$this -> html_mail_end);

						break;

					case "Matthew Higgs Surrey":

						$this -> staff_mail_extra = str_replace("020 7267 4271", "01483 538617",$this -> staff_mail_extra);

						$this -> staff_htmlmail_extra  = str_replace("020 7267 4271", "01483 538617",$this ->staff_htmlmail_extra);

						$this -> mail_end = str_replace("020 7267 4271", "01483 538617",$this -> mail_end);

						$this -> html_mail_end = str_replace("020 7267 4271", "01483 538617",$this -> html_mail_end);

						break;

					case "Shaun Haworth":

						$this -> staff_mail_extra = str_replace("020 7267 4271", "01892 506870",$this -> staff_mail_extra);

						$this -> staff_htmlmail_extra  = str_replace("020 7267 4271", "01892 506870",$this ->staff_htmlmail_extra);

						$this -> mail_end = str_replace("020 7267 4271", "01892 506870",$this -> mail_end);

						$this -> html_mail_end = str_replace("020 7267 4271", "01892 506870",$this -> html_mail_end);

						break;

					case "Chris Haworth":

						$this -> staff_mail_extra = str_replace("020 7267 4271", "01993 201802",$this -> staff_mail_extra);

						$this -> staff_htmlmail_extra  = str_replace("020 7267 4271", "01993 201802",$this ->staff_htmlmail_extra);						

						$this -> mail_end = str_replace("020 7267 4271", "01993 20180",$this -> mail_end);

						$this -> html_mail_end = str_replace("020 7267 4271", "01993 20180",$this -> html_mail_end);

						break;

					default:

						return;

				}

		}

	}

	

	private function GetMailTotals($total_so_far) {

		$damage = $total_so_far * 0.05;

		$final_damaged = $total_so_far + $damage;

		$ret = "\nTOTAL COST: �" .  sprintf("%01.2f", $total_so_far);

		$ret .= "\nDamage waiver @ 5%: �" .  sprintf("%01.2f", $damage) . "\n";

		$ret .= "- - - - - - - - - - - - - \nFINAL COST: �" .  sprintf("%01.2f", $final_damaged) . "*";

		return $ret;

	}

	

	private function GetMailEndWithAnalytics () {

		$source = !$this -> staffMail?"sent":"staff";

		$this -> mail_end = str_replace ( "#whichsource#" , $source ,$this -> mail_end);

		$this -> html_mail_end = str_replace ( "#whichsource#" , $source ,$this -> html_mail_end);

		/*if (!$this -> staffMail) {

			$this -> mail_end .= $this ->send_mail_end;

		}*/

	}

	

	private function GetFurniture ($dims = false) {

		

		$furnGetter = new Get_onlinefurnquote_xml("../xml/quote_furn_equip.xml");

		

		if ($furnGetter -> xml) {

			foreach ($this -> retvalmarquees as $marquee) {

				//$this -> SetUpJsonArray($marquee -> type, "success");

				//$furnRet = "";

				

				  $this -> retvalfurn .= "<p><strong>A {$marquee -> mWidth}m x {$marquee -> mLength}m ({$marquee -> fWidth}' x {$marquee -> fLength}') {$marquee -> type} marquee</strong><br/>";

				  

				  //dims marquees have no guest numbers. Numbers calculated from capacity numbers in getsmal -> getfurn

				  if (!$dims) {

				 	 $this -> retvalfurn .= $furnGetter ->  GetFurn($this -> curr_extras, $marquee,  $this -> allValues["rbSeated"], $this -> allValues["tbNumbers"]);

				  } else {

					  $this -> retvalfurn .= $furnGetter ->  GetFurn($this -> curr_extras, $marquee,  $this -> allValues["rbSeated"] );

				  }

				  

				  $this -> retvalfurn .= (!$this -> mail)?$this -> GetTotals((int)$marquee -> total + $furnGetter -> furnVal):$this -> GetHtmlMailTotals((int)$marquee -> total + $furnGetter -> furnVal);

					

			}

			

			if (!$this -> mail) {

				$this -> retval .= $this -> retvalfurn;

			} else {

				$this -> htmlMail_content .=  $this -> retvalfurn;

			}

			

		} else {

			 $this -> SetUpJsonArray($this -> no_quote_start . $this -> notworking_message, "problem");

		}

		

	}

	

	private function GetMailFurniture ($dims = false) {

		

		$furnGetter = new Get_onlinefurnquote_xml("../xml/quote_furn_equip.xml");

		

		if ($furnGetter -> xml) {

			foreach ($this -> retvalmarquees as $marquee) {

				//$this -> SetUpJsonArray($marquee -> type, "success");

				//$furnRet = "";

				

				  $this -> retvalfurn .= "\n\nA {$marquee -> mWidth}m x {$marquee -> mLength}m ({$marquee -> fWidth}' x {$marquee -> fLength}') {$marquee -> type} marquee\n";

				  

				  //dims marquees have no guest numbers. Numbers calculated from capacity numbers in getsmal -> getfurn

				  if (!$dims) {

				 	 $this -> retvalfurn .= $furnGetter ->  GetFurn($this -> curr_extras, $marquee,  $this -> allValues["rbSeated"], $this -> allValues["tbNumbers"], true);

				  } else {

					  $this -> retvalfurn .= $furnGetter ->  GetFurn($this -> curr_extras, $marquee,  $this -> allValues["rbSeated"], false, true );

				  }

				  

				  $this -> retvalfurn .= $this -> GetMailTotals((int)$marquee -> total + $furnGetter -> furnVal);

					

			}

			

			$this -> retval .= $this -> retvalfurn;

			

		} else {

			 $this -> SetUpJsonArray($this -> no_quote_start . $this -> notworking_message, "problem");

		}

		

	}

	

	private function SetUp () {

		

		//$this -> SetUpJsonArray("GOT into setup" . $this -> notworking_message, "problem");

		//TODO sort out this - it causes the ddwidth error. Looks like some browsers dont send empty or unchanged fields.

		//WHY DONT THE ISSETS BELOW WORK?

		if (isset($_GET["ddWidth"]) && isset($_GET["ddLength"])) {

			if ($_GET["ddWidth"] == "0" && $_GET["ddLength"] != "Choose width first") {

			  $this -> SetSpam( "Width not set and length not choose width first" );

		  	}

		}

		

		foreach ($this -> allFields as $field => $type) {

						

			$multiline =  $type != "multiline" ? false : true;

			$allowedLength = !$multiline ? 40 : 1000;

			

			if (isset($_GET[$field])) {

				

				if ($type == "string" || $type == "multiline") {

					

					if (!is_string($_GET[$field])) {

						$this -> SetSpam( "$field not a string" );

					}

					

					if (strlen($_GET[$field]) > $allowedLength) {

						$this -> SetSpam( "$field more than $allowedLength chars" );

					}

					

					if ($this -> remove_headers($_GET[$field], $multiline) == false) {

						$this -> SetSpam ("extra headers in $field");

					}

					if ($field == "tbNumbers") {

						if (!is_numeric($_GET[$field]) ) {

							$this -> SetSpam( "Please enter only numbers for <em>Guest numbers</em>.", true );

						}

						

						if ( ($_GET["rbSeated"][0] == "1" && (int)($_GET[$field]) > 675) || ($_GET["rbSeated"][0] == "0" && (int)($_GET[$field]) > 800)) {

							$this -> SetSpam( "Sorry. For very large events, the online quote does not work well. Please <a href='/contact_us.htm'>contact us</a> for a more realistic estimate. Or <a href='/pricing/quotation.htm'>request a no-obligation quotation</a>.", true );

						}

					}

					if ($field == "tbMail") {

						if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {

							$this -> SetSpam( "You have entered an invalid email. Please try again.", true );

						}

					}

										

					$this -> allValues[$field] = $_GET[$field];

					

				}

				

				if ($type == "array" && $this -> spam == false) {

					

					if (!is_array($_GET[$field])) {

						$this -> SetSpam( "$field not a array" );

					} else {

						//radio buttoms

						 $checkArrType = stripos($field,"rb");

						 if ( $checkArrType !== false) {

							 $this -> allValues[$field] = $_GET[$field][0];

							//$this -> retval .= $field . ":" . $_GET[$field][0] . "<br/>";

						//extras - checkboxes

						 } else {

							 //echo "Extras: " . count($_GET["cb"]) . "<br/>";

							 foreach ($_GET["cb"] as $extra) {

								 

							   	if (!in_array($extra, $this -> extras)) {

								   $this -> SetSpam( "$extra not an extra" );

							  	}

							   	if (!is_string($extra)) {

								  $this -> SetSpam( "$field not a string" );

							  	}

								if ($this -> remove_headers($extra) == false) {

									$this -> SetSpam ("extra headers in $field");

								}

							   	if (strlen($extra) > 20) {

								   $this -> SetSpam( "$field more than 20 chars" );

							  	}

								$this -> curr_extras[] = $extra;

								 

							 }

						 }

					}

				}

				

			}

			

		}

		//$this -> retval .= "<br/><br/>";

	}

	

	private function remove_headers($string, $multiline = false) {

		//echo "header check | ";

	  $headers = array(

		"/to\:/i",

		"/from\:/i",

		"/bcc\:/i",

		"/cc\:/i",

		"/Content\-Transfer\-Encoding\:/i",

		"/Content\-Type\:/i",

		"/Mime\-Version\:/i" 

	  );

	  $string = strtolower($string);

	  if (preg_replace($headers, '', $string) == $string) {

		if (!$multiline) {

			if(strstr($string, PHP_EOL)) { 

				return false;

			}

		}

		return true;

	  } else {

		return false;

	  }

	}

	

	private function SetSpam($msg, $errors = false) {

		//$this -> retval = $this -> spam_message; TODO restore this

		if (!$errors) {

			$this -> retval = $this -> spam_message;

		} else {

			//no int in tbnumbers is the only prob dealt with here

			$this -> retval = $this -> errorstart_message . "<p>$msg</p>";

		}

		$this -> valid = false;

		$this -> myErrors .= "\n" . $msg;

		$this -> spam = true;

		$this -> SetUpJsonArray($this -> retval, "spam");

		exit();

	}

	

	private function SetUpJsonArray($msg, $result) {

	  $jsonArr["success"] = $result;

	  $jsonArr["msg"] = $msg;

	  echo json_encode($jsonArr);

	  exit();

	}





	

}

?>