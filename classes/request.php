<?php


class CM_Request {
    //private $appreg;
    private $properties;	
    private $feedback = array();
    /*private $objects = array();
    private $lastCommand;*/

    function __construct() {
        $this->init();
    }

    function init() {
		//ShowSomething("", "hello from request init");
        if ( $_SERVER['REQUEST_METHOD'] ) {
            $this->properties = $_REQUEST;
			//print_r($this->properties);
            return;
        }
        foreach( $_SERVER['argv'] as $serverarg ) {
            if ( strpos( $arg, '=' ) ) {
                list( $key, $val )=explode( "=", $serverarg );
                $this->setProperty( $key, $val );
            }
        }
    }

    function getProperty( $key ) {
        return isset($this->properties[$key])?$this->properties[$key]:false;
    }

    function setProperty( $key, $val ) {
        $this->properties[$key] = $val;
    }
    
    /*function __clone() {
        $this->properties = array();
    }*/
    
    function addFeedback( $msg ) {
        array_push( $this->feedback, $msg );
    }
 
    function getFeedback( ) {
        return $this->feedback;
    }
	

    function getFeedbackString( $separator="\n" ) {
		if (count($this->feedback)) {
			return implode( $separator, $this->feedback );
		} else {
			return "No feedback";
		}
	}

    /*function setObject( $name, $object ) {
        $this->objects[$name] = $object;
    }

    function getObject( $name ) {
        return $this->objects[$name];
    }

    function setCommand( woo_command_Command $command ) {
        $this->lastCommand = $command;
    }

    function getLastCommand() {
        return $this->lastCommand;
    }*/
}
?>
