const Site = {
    bp: {
        xxs: 375,
        xs: 600,
        sm: 768,
        md: 1024,
        lg: 1260,
        xl: 1420
    },
    themeUrl: "",
    ajaxUrl: "",
    urlQuery: "",
    resizeTimeout: !1,
    windowWidth: 0,
    headerHeight: 0,
    elems: {
        body: document.body,
        container: document.getElementById("container"),
        header: document.getElementById("header")
    },
    buildParams: e => Object.keys(e).map((t => t + "=" + e[t])).join("&"),
    parseQuerystring(e) {
        const t = {};
        if (e) {
            const s = e.split("?")[1].split("#")[0].split("&");
            for (let e = s.length - 1; e >= 0; e--) {
                const i = s[e].split("=");
                t[i[0]] = i[1]
            }
        }
        return t
    },
    updateActiveClass(e, t=!1) {
        e.forEach((e => {
            t && t === e || !e.classList.contains("active") || e.classList.remove("active"),
            t === e && e.classList.add("active")
        }
        ))
    },
    startListeners() {
        window.addEventListener("resize", ( () => {
            clearTimeout(Site.resizeTimeout),
            Site.resizeTimeout = setTimeout(( () => {
                if (window.innerWidth != Site.windowWidth) {
                    Site.resize();
                    for (const e in Site)
                        "object" == typeof Site[e] && null !== Site[e] && "resize"in Site[e] && Site[e].resize()
                }
            }
            ), 250)
        }
        ))
    },
    resize() {
        Site.windowWidth = window.innerWidth,
        Site.elems.body.style.setProperty("--vh-100", window.innerHeight + "px")
    },
    init() {
        Site.themeUrl = Site.elems.container.dataset.themeUrl,
        Site.ajaxUrl = Site.elems.container.dataset.ajaxUrl,
        Site.urlQuery = Site.parseQuerystring(location.search),
        Site.startListeners(),
        Site.resize()
    }
};
Site.init(),
Site.Cookies = {
    setCookie(e, t, s, i=!1) {
        const o = new FormData;
        o.append("action", "set_single_cookie"),
        o.append("name", e),
        o.append("value", t),
        s && o.append("expiry", s),
        fetch(Site.ajaxUrl, {
            method: "post",
            body: o
        }).then((e => e.json())).then((e => {
            i && i(e)
        }
        )).catch((e => {
            console.log(e)
        }
        ))
    }
},
Site.Form = {
    emailRegex: /^(([^<>()[\]\\.,:\s@"]+(\.[^<>()[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    numberExtraRegex: /^[0-9., \-()]*$/,
    addDefaultListeners(e) {
        e.forEach((e => {
            e.addEventListener("submit", (t => {
                t.preventDefault(),
                Site.Form.validate(e, Site.Form.handleSubmit)
            }
            ))
        }
        ))
    },
    updateRelatedField(e, t, s) {
        t.checked ? s && (s.value = t.value) : "checkbox" === e && s && (s.value = s.dataset.defaultValue)
    },
    showHideRelatedSection(e, t, s, i) {
        let o = !1;
        if (t.dataset.target && (o = document.getElementById(t.dataset.target),
        !o && i && (o = i.querySelector(t.dataset.target))),
        o) {
            const e = +t.dataset.show;
            t.checked && 1 === e || "checkbox" === t.type && !t.checked && 0 === e ? Site.Form.showExtraFields(t, o) : (t.checked && 0 === e || "checkbox" === t.type && !t.checked && 1 === e) && Site.Form.hideExtraFields(t, o)
        }
    },
    showHideViaSelect(e) {
        e.querySelectorAll("option").forEach((e => {
            if (e.dataset.target) {
                const t = document.getElementById(e.dataset.target);
                t && (e.selected ? Site.Form.showExtraFields(e, t) : Site.Form.hideExtraFields(e, t))
            }
        }
        ))
    },
    showExtraFields(e, t) {
        t.style.display = "block";
        t.querySelectorAll('[data-required="1"]').forEach((t => {
            t.dataset.parent && e.dataset.target !== t.dataset.parent || t.classList.add("required")
        }
        ))
    },
    hideExtraFields(e, t) {
        t.style.display = "none";
        t.querySelectorAll("input.text, select, textarea").forEach((e => {
            e.value = "",
            e.classList.remove("invalid", "valid"),
            delete e.dataset.validating;
            const t = e.closest(".field-wrap");
            t && t.classList.remove("focused", "filled")
        }
        ));
        t.querySelectorAll('[data-required="1"]').forEach((e => {
            e.classList.remove("required")
        }
        )),
        t.querySelectorAll('input[type="radio"]:checked, input[type="checkbox"]:checked').forEach((e => {
            e.checked = !1,
            e.dispatchEvent(new Event("change",{
                bubbles: !0
            }))
        }
        )),
        t.querySelectorAll('input[type="file"]').forEach((e => {
            e.value = "",
            e.dispatchEvent(new Event("change",{
                bubbles: !0
            }))
        }
        ))
    },
    addAggressiveValidation(e) {
        e.dataset.validating = 1,
        e.matches("select") || "hidden" == e.type || "file" == e.type || "checkbox" == e.type || "radio" == e.type ? e.addEventListener("change", Site.Form.handleFieldChange) : e.addEventListener("keyup", Site.Form.handleFieldChange)
    },
    removeAggressiveValidation(e) {
        e.matches("select") || "hidden" == e.type || "file" == e.type || "checkbox" == e.type || "radio" == e.type ? e.removeEventListener("change", Site.Form.handleFieldChange) : e.removeEventListener("keyup", Site.Form.handleFieldChange),
        delete e.dataset.validating
    },
    handleFieldChange(e) {
        const t = e.target
          , s = t.closest("form")
          , i = Site.Form.validateField(t, s);
        Site.Form.processField(t, i, s)
    },
    validateField(e, t) {
        let s = !0;
        if (s && e.classList.contains("required") && (("checkbox" != e.type || e.checked) && ("radio" != e.type || t.querySelector('[name="' + e.name + '"]:checked')) && e.value.length || (s = !1)),
        s && "radio" === e.type && e.classList.contains("required-hidden") && !e.closest(".radio-group").querySelector('[name="' + e.dataset.update + '"]').value && (s = !1),
        s && e.value && e.classList.contains("email") && !Site.Form.emailRegex.test(e.value) && (s = !1),
        s && e.value && e.classList.contains("url"))
            try {
                new URL(e.value)
            } catch (e) {
                s = !1
            }
        if (s && e.dataset.equalTo) {
            const t = document.getElementById(e.dataset.equalTo);
            e.value != t.value && (s = !1)
        }
        return s && e.classList.contains("number-extra") && e.value && !Site.Form.numberExtraRegex.test(e.value) && (s = !1),
        s && e.dataset.min && e.value && (isNaN(e.value) || +e.value < +e.dataset.min) && (s = !1),
        s && e.dataset.max && e.value && (isNaN(e.value) || +e.value > +e.dataset.max) && (s = !1),
        s && e.classList.contains("one-checkbox") && !e.closest(".checkbox-group").querySelector('input[type="checkbox"]:checked') && (s = !1),
        s && e.classList.contains("one-yes-radio") && !e.closest(".one-yes-wrap").querySelector('input[type="radio"].yes:checked') && (s = !1),
        s
    },
    addRemoveInvalid(e, t, s) {
        "add" === s ? (t.classList.remove("valid"),
        t.classList.add("invalid")) : (t.classList.remove("invalid"),
        e.dataset.validating && t.classList.add("valid"))
    },
    updateValidState(e, t) {
        if ("file" == e.type || "checkbox" == e.type || "radio" == e.type)
            if ("radio" == e.type || "checkbox" === e.type && e.classList.contains("one-checkbox")) {
                e.closest(".field-group").querySelectorAll(".field-wrap").forEach((s => {
                    Site.Form.addRemoveInvalid(e, s, t)
                }
                ))
            } else
                Site.Form.addRemoveInvalid(e, e.closest(".field-wrap"), t);
        else
            Site.Form.addRemoveInvalid(e, e, t)
    },
    processField(e, t, s) {
        let i = !1
          , o = document.getElementById(e.dataset.error) || !1;
        o || (o = s.querySelector(e.dataset.error) || !1);
        let l = document.getElementById(e.dataset.errorTarget) || !1;
        if (!l) {
            l = (e.dataset.errorTargetParent ? e.closest(e.dataset.errorTargetParent) : s).querySelector(e.dataset.errorTarget) || !1
        }
        return t ? (o && (o.style.display = "none"),
        l && l.classList.remove("invalid"),
        o || l || Site.Form.updateValidState(e, "remove")) : (e.dataset.validating || Site.Form.addAggressiveValidation(e),
        o && (o.style.display = "block",
        "hidden" == e.type && (i = o)),
        l && (l.classList.add("invalid"),
        "hidden" == e.type && (i = l)),
        o || l || Site.Form.updateValidState(e, "add"),
        "hidden" != e.type && (i = e)),
        i
    },
    validate(e, t, s=!1) {
        const i = [];
        Array.from(e.elements).forEach((t => {
            const s = Site.Form.validateField(t, e);
            t.matches("button") || i.push({
                field: t,
                valid: s
            })
        }
        ));
        const o = [];
        if (i.forEach((t => {
            const s = Site.Form.processField(t.field, t.valid, e);
            s && o.push(s)
        }
        )),
        o.length) {
            const e = Site.ScrollLock && Site.ScrollLock.locks.length ? Site.ScrollLock.locks[Site.ScrollLock.locks.length - 1] : document.body;
            Site.Scroll.scrollTo(o[0], e, .5)
        } else
            t(e, s)
    },
    handleError(e, t, s) {
        if (t.error) {
            let e = t.error.dataset.default || !1;
            s.errors && s.errors.length && (e = s.errors.join("<br>")),
            e && (t.error.innerHTML = e)
        }
        t.sending && t.sending.classList.add("invis"),
        t.error && t.error.classList.remove("invis"),
        e.classList.remove("status-sending"),
        e.classList.add("status-error"),
        setTimeout(( () => {
            t.error && t.error.classList.add("invis"),
            t.submit && t.submit.classList.remove("invis"),
            t.button && (t.button.disabled = !1),
            e.classList.remove("status-error"),
            delete e.dataset.submitting
        }
        ), 5e3)
    },
    handleSubmit(e, t) {
        if (e.dataset.noAjax)
            e.submit();
        else if (!e.dataset.submitting) {
            e.dataset.submitting = 1,
            e.classList.add("status-sending");
            const s = {
                button: e.querySelector('button[type="submit"]'),
                submit: e.querySelector(".submit"),
                sending: e.querySelector(".sending"),
                success: e.querySelector(".success"),
                error: e.querySelector(".error")
            };
            s.submit && s.submit.classList.add("invis"),
            s.button && (s.button.disabled = !0),
            s.sending && s.sending.classList.remove("invis");
            const i = new FormData(e);
            fetch(e.getAttribute("action"), {
                method: "post",
                body: i
            }).then((e => e.json())).then((i => {
                if (200 == i.status)
                    if (t)
                        t(e, s, i);
                    else if (i.redirect)
                        location.href = i.redirect;
                    else {
                        e.dataset.reset ? Site.Form.reset(e, "all") : e.dataset.resetPassword && Site.Form.reset(e, "password");
                        const t = e.querySelector(".success-hide");
                        t && t.classList.add("invis"),
                        s.sending && s.sending.classList.add("invis"),
                        s.success && s.success.classList.remove("invis"),
                        e.classList.remove("status-sending"),
                        e.classList.add("status-success"),
                        e.dataset.noRepeat || setTimeout(( () => {
                            !t && s.success && s.success.classList.add("invis"),
                            s.submit && s.submit.classList.remove("invis"),
                            s.button && (s.button.disabled = !1),
                            e.classList.remove("status-success"),
                            delete e.dataset.submitting
                        }
                        ), 5e3)
                    }
                else
                    Site.Form.handleError(e, s, i)
            }
            )).catch((t => {
                Site.Form.handleError(e, s, t)
            }
            ))
        }
    },
    reset(e, t) {
        "all" == t ? (e.reset(),
        Array.from(e.elements).forEach((e => {
            Site.Form.resetField(e)
        }
        ))) : "password" == t && e.querySelectorAll('input[type="password"]').forEach((e => {
            e.value = "",
            Site.Form.resetField(e)
        }
        ))
    },
    resetField(e) {
        e.classList.remove("invalid", "valid");
        const t = e.closest(".field-wrap");
        if (t) {
            t.classList.remove("focused", "filled", "valid", "invalid"),
            t.classList.contains("has-file") && (t.classList.remove("has-file"),
            t.querySelector(".file").value = "",
            t.querySelector(".file-name").innerText = "");
            const s = e.closest(".field-group");
            s && s.classList.remove("valid", "invalid")
        }
        Site.Form.removeAggressiveValidation(e)
    },
    init() {
        Site.Form.addDefaultListeners(document.querySelectorAll(".validate-form")),
        document.querySelectorAll(".field-wrap input.text, .field-wrap textarea, .field-wrap select").forEach((e => {
            const t = e.closest(".field-wrap");
            e.addEventListener("focus", ( () => {
                t.classList.add("focused")
            }
            )),
            e.addEventListener("blur", ( () => {
                t.classList.remove("focused")
            }
            )),
            e.addEventListener("change", ( () => {
                e.value ? t.classList.add("filled") : t.classList.remove("filled")
            }
            ))
        }
        )),
        document.addEventListener("click", (e => {
            if (e.target.matches(".remove-file")) {
                const t = e.target.closest(".file-wrap").querySelector(".file");
                t.value = "",
                t.dispatchEvent(new Event("change",{
                    bubbles: !0
                }))
            }
        }
        )),
        document.addEventListener("change", (e => {
            if (e.target.matches('.validate-form input[type="checkbox"]') || e.target.matches('.validate-form input[type="radio"]')) {
                const t = e.target
                  , s = t.closest(".field-wrap")
                  , i = t.closest(".field-group")
                  , o = t.closest("form")
                  , l = !!t.dataset.update && o.querySelector('input[name="' + t.dataset.update + '"]');
                l && Site.Form.updateRelatedField(t.type, t, l),
                Site.Form.showHideRelatedSection(t.type, t, s, i),
                t.dataset.validating && Site.Form.handleFieldChange(e)
            } else if (e.target.matches(".validate-form select"))
                Site.Form.showHideViaSelect(e.target);
            else if (e.target.matches('.validate-form input[type="file"]')) {
                const t = e.target
                  , s = t.closest(".field-wrap")
                  , i = t.value.split("\\").pop()
                  , o = s.querySelector(".file-name");
                i ? (o.innerText = i,
                s.classList.add("has-file")) : (o.innerText = "",
                s.classList.remove("has-file"))
            }
        }
        ))
    }
},
Site.Form.init(),
Site.LazyLoad = {
    observedCssClass: "observed-lazy-load",
    rootMargin: "100% 25% 100% 25%",
    observer: !1,
    update(e=document) {
        e.querySelectorAll(".lazy:not(." + Site.LazyLoad.observedCssClass + "):not(.loaded)").forEach((e => {
            Site.LazyLoad.observer.observe(e),
            e.classList.add(Site.LazyLoad.observedCssClass)
        }
        ))
    },
    init() {
        Site.LazyLoad.observer = new IntersectionObserver((e => {
            e.forEach((e => {
                const t = e.target;
                e.isIntersecting && (t.setAttribute("src", t.dataset.src),
                delete t.dataset.src,
                t.dataset.srcset && (t.setAttribute("srcset", t.dataset.srcset),
                delete t.dataset.srcset),
                t.dataset.sizes && (t.setAttribute("sizes", t.dataset.sizes),
                delete t.dataset.sizes),
                t.classList.add("loaded"),
                Site.LazyLoad.observer.unobserve(t),
                t.classList.remove(Site.LazyLoad.observedCssClass))
            }
            ))
        }
        ),{
            rootMargin: Site.LazyLoad.rootMargin
        }),
        Site.LazyLoad.update()
    }
},
Site.LazyLoad.init(),
Site.ScrollLock = {
    isIos: "undefined" != typeof window && window.navigator && window.navigator.platform && (/iP(ad|hone|od)/.test(window.navigator.platform) || "MacIntel" === window.navigator.platform && window.navigator.maxTouchPoints > 1),
    startY: -1,
    locks: [],
    handleTouchStart(e) {
        1 === e.targetTouches.length && (Site.ScrollLock.startY = e.targetTouches[0].clientY)
    },
    handleTouchMove(e) {
        const t = e.currentTarget;
        if (1 === e.targetTouches.length) {
            const s = e.targetTouches[0].clientY - Site.ScrollLock.startY;
            (0 === t.scrollTop && s > 0 || t.scrollHeight - t.scrollTop <= t.clientHeight && s < 0) && e.preventDefault(),
            e.stopPropagation()
        }
    },
    handleNoScroll(e) {
        1 === e.targetTouches.length && e.preventDefault()
    },
    lock(e=!1) {
        e && Site.ScrollLock.locks.push(e),
        Site.ScrollLock.isIos ? (window.addEventListener("touchmove", Site.ScrollLock.handleNoScroll, {
            passive: !1
        }),
        e && (e.addEventListener("touchstart", Site.ScrollLock.handleTouchStart, {
            passive: !0
        }),
        e.addEventListener("touchmove", Site.ScrollLock.handleTouchMove, {
            passive: !1
        }))) : Site.elems.body.style.overflow = "hidden"
    },
    unlock(e=!1) {
        if (e) {
            const t = Site.ScrollLock.locks.indexOf(e);
            -1 !== t && Site.ScrollLock.locks.splice(t, 1)
        }
        Site.ScrollLock.isIos ? (e && (e.removeEventListener("touchstart", Site.ScrollLock.handleTouchStart, {
            passive: !0
        }),
        e.removeEventListener("touchmove", Site.ScrollLock.handleTouchMove, {
            passive: !1
        })),
        0 === Site.ScrollLock.locks.length && window.removeEventListener("touchmove", Site.ScrollLock.handleNoScroll, {
            passive: !1
        })) : 0 === Site.ScrollLock.locks.length && (Site.elems.body.style.overflow = "")
    }
},
Site.Scroll = {
    pxPerFrame: 16.666,
    minFrames: 20,
    maxFrames: 60,
    animID: null,
    easing: {
        linear: e => e,
        "ease-out": (e, t=1.75) => 1 - Math.pow(1 - e, t)
    },
    animate(e, t, s, i, o=0) {
        const l = t + (s - t) * Site.Scroll.easing["ease-out"](o / i);
        if (e.scrollTo(0, l),
        o == i)
            return cancelAnimationFrame(Site.Scroll.animID),
            void (Site.Scroll.animID = null);
        Site.Scroll.animID = requestAnimationFrame(( () => {
            Site.Scroll.animate(e, t, s, i, ++o)
        }
        ))
    },
    scrollTo(e, t=document.body, s=0) {
        const i = t === document.body ? window : t
          , o = t === document.body ? window.scrollY : t.scrollTop;
        let l;
        if (isNaN(e)) {
            const i = t === document.body ? window.innerHeight : t.offsetHeight
              , a = t === document.body && 0 === s ? Site.headerHeight : 0;
            l = Math.max(0, e.getBoundingClientRect().y - a + o - window.innerHeight * s),
            l = Math.min(l, t.scrollHeight - i)
        } else
            l = e;
        const a = Math.abs(l - o);
        if (a) {
            const e = Math.floor(Math.max(Math.min(a / Site.Scroll.pxPerFrame, Site.Scroll.maxFrames), Site.Scroll.minFrames));
            Site.Scroll.animate(i, o, l, e)
        }
    },
    init() {
        document.querySelectorAll(".scroll-link").forEach((e => {
            e.addEventListener("click", ( () => {
                const t = e.dataset.id ? document.getElementById(e.dataset.id) : +e.dataset.y;
                Site.Scroll.scrollTo(t)
            }
            ))
        }
        ))
    }
},
Site.Scroll.init(),
Site.Accordions = {
    elems: {
        accordionItems: document.querySelectorAll(".accordion-item")
    },
    open(e) {
        const t = e.querySelector(".accordion-content");
        e.classList.add("active"),
        t.style.height = t.scrollHeight + "px",
        setTimeout(( () => {
            e.classList.add("open"),
            t.style.height = ""
        }
        ), 300)
    },
    close(e) {
        const t = e.querySelector(".accordion-content");
        requestAnimationFrame(( () => {
            t.style.height = t.scrollHeight + "px",
            e.classList.remove("active", "open"),
            requestAnimationFrame(( () => {
                t.style.height = ""
            }
            ))
        }
        ))
    },
    init() {
        Site.Accordions.elems.accordionItems.forEach((e => {
            const t = e.closest(".accordion-group");
            e.querySelector(".accordion-btn").addEventListener("click", ( () => {
                if (e.classList.contains("active"))
                    Site.Accordions.close(e);
                else {
                    if (t) {
                        const e = t.querySelector(".accordion-item.active");
                        e && Site.Accordions.close(e)
                    }
                    Site.Accordions.open(e)
                }
            }
            ))
        }
        ))
    }
},
Site.Accordions.init();
class Carousel {
    constructor(e, t, s) {
        this.options = t,
        void 0 === this.options.interactive && (this.options.interactive = !0),
        void 0 === this.options.moveAction && (this.options.moveAction = "drag"),
        void 0 === this.options.axis && (this.options.axis = "x"),
        void 0 === this.options.transition && (this.options.transition = "slide"),
        "infinite" === this.options.autoplay ? (void 0 === this.options.easing && (this.options.easing = "linear"),
        void 0 === this.options.speed && (this.options.speed = 50)) : (void 0 === this.options.duration && (this.options.duration = "slide" === this.options.transition ? 1e3 : 600),
        void 0 === this.options.easing && (this.options.easing = "slide" === this.options.transition ? "ease-out" : "linear")),
        void 0 === this.options.startCell && (this.options.startCell = 1),
        this.changeCallback = s,
        this.carouselWrap = e,
        this.carouselClip = this.carouselWrap.querySelector(".crsl-clip"),
        this.carousel = this.carouselClip.querySelector(".crsl"),
        this.cells = this.allCells = null,
        this.animating = this.pointing = this.duplicateXorY = !1,
        this.pointerStartX = this.pointerStartY = this.curXorY = this.dragStartXorY = this.lastWheelDelta = 0,
        this.autoplayInterval = this.slideTimeout = null,
        this.prevNext = 1,
        this.offsetDimension = "y" === this.options.axis ? "offsetHeight" : "offsetWidth",
        this.offsetEdge = "y" === this.options.axis ? "offsetTop" : "offsetLeft",
        this.prevArrows = this.carouselWrap.querySelectorAll(".crsl-arw.prev"),
        0 === this.prevArrows.length && (this.prevArrows = !1),
        this.nextArrows = this.carouselWrap.querySelectorAll(".crsl-arw.next"),
        0 === this.nextArrows.length && (this.nextArrows = !1),
        this.dots = this.carouselWrap.querySelector(".crsl-dots") || !1,
        this.progressBar = this.carouselWrap.querySelector(".crsl-progress-bar") || !1,
        this.counter = this.carouselWrap.querySelector(".crsl-counter") || !1,
        this.maybePreventLinkClicks = this.maybePreventLinkClicks.bind(this),
        this.pointerStart = this.pointerStart.bind(this),
        this.pointerMove = this.pointerMove.bind(this),
        this.pointerEnd = this.pointerEnd.bind(this),
        this.wheel = this.wheel.bind(this),
        this.goToCell = this.goToCell.bind(this),
        this.changeCellAfterVideo = this.changeCellAfterVideo.bind(this),
        this.mutationObserver = new MutationObserver(( () => {
            Site.LazyLoad && Site.LazyLoad.update(this.carousel),
            Site.FxFadeIn && Site.FxFadeIn.update(this.carousel)
        }
        )),
        this.mutationObserver.observe(this.carousel, {
            childList: !0,
            subtree: !0
        }),
        this.init()
    }
    unifyEvents(e) {
        return e.changedTouches ? e.changedTouches[0] : e
    }
    maybePreventLinkClicks(e) {
        (this.carousel.classList.contains("no-click") || "slide" === this.options.transition && !isNaN(this.perView) && !e.currentTarget.closest(".crsl-cell").classList.contains("clickable")) && e.preventDefault()
    }
    pointerStart(e) {
        this.carousel.classList.contains("interactive") && (this.pause(),
        this.pointing || this.animating || (this.pointerStartX = this.unifyEvents(e).clientX,
        this.pointerStartY = this.unifyEvents(e).clientY,
        this.dragStartXorY = this.curXorY,
        this.pointing = !0,
        this.carousel.classList.add("pointer-down")))
    }
    pointerMove(e) {
        const t = this.pointerStartX - this.unifyEvents(e).clientX
          , s = this.pointerStartY - this.unifyEvents(e).clientY;
        let i = "y" === this.options.axis ? s : t
          , o = "y" === this.options.axis ? t : s;
        if ((Math.abs(i) > 10 || Math.abs(o) < 5) && (e.preventDefault(),
        this.pointing)) {
            if (Math.abs(i) > 5 || Math.abs(o) > 5) {
                this.carousel.classList.add("no-click");
                const e = this.cells[this.curCellID - 1].querySelector("video");
                e && 1 == +e.dataset.controls && (e.controls = !1)
            }
            "slide" === this.options.transition && this.setXorY(this.dragStartXorY + -1 * i)
        }
    }
    pointerEnd(e) {
        if (this.carousel.classList.contains("interactive") && this.pointing) {
            const t = "y" === this.options.axis ? this.pointerStartY - this.unifyEvents(e).clientY : this.pointerStartX - this.unifyEvents(e).clientX;
            if (t) {
                this.prevNext = Math.sign(t);
                const e = this.options.loop || (!this.start || 1 === this.prevNext) && (!this.end || -1 === this.prevNext);
                if ("dissolve" === this.options.transition)
                    Math.abs(t) > 50 && e && (this.curCellID = this.calcNewCellID(),
                    this.dissolve());
                else {
                    Math.abs(t) / this.carouselClip[this.offsetDimension] > .2 && e && (this.curCellID = this.calcNewCellID(t)),
                    this.slide()
                }
            }
            this.pointerStartX = this.pointerStartY = this.dragStartXorY = 0,
            this.carousel.classList.remove("pointer-down"),
            setTimeout(( () => {
                this.carousel.classList.remove("no-click");
                const e = this.cells[this.curCellID - 1].querySelector("video");
                e && 1 == +e.dataset.controls && (e.controls = !0)
            }
            ), 50),
            this.pointing = !1
        }
    }
    wheel(e) {
        const t = "y" === this.options.axis ? e.deltaY : e.deltaX;
        this.prevNext = Math.sign(t),
        !this.options.loop && (this.start && 1 !== this.prevNext || this.end && -1 !== this.prevNext) || (Math.abs(t) > this.lastWheelDelta && !this.animating && (this.animating = !0,
        this.curCellID = this.calcNewCellID(),
        "dissolve" === this.options.transition ? this.dissolve() : this.slide()),
        this.lastWheelDelta = Math.abs(t))
    }
    goToCell(e) {
        const t = e.currentTarget;
        this.pause(),
        this.animating || (t.classList.contains("dot") || t.classList.contains("crsl-cell") && !t.classList.contains("clickable") ? this.curCellID = +t.dataset.id : t.classList.contains("crsl-arw") && (this.prevNext = t.classList.contains("prev") ? -1 : 1,
        this.curCellID = this.calcNewCellID()),
        "dissolve" === this.options.transition ? this.dissolve() : this.slide())
    }
    autoChangeCell() {
        this.animating || (this.curCellID = this.calcNewCellID(1),
        "dissolve" === this.options.transition ? this.dissolve() : this.slide())
    }
    changeCellAfterVideo(e) {
        e.target.removeEventListener("ended", this.changeCellAfterVideo),
        this.autoChangeCell()
    }
    calcNewCellID(e=0) {
        let t, s = 1;
        if ("slide" === this.options.transition)
            if ("auto" === this.perView) {
                if (0 !== e) {
                    const s = this.options.centreActive || this.options.centreActiveXs && window.innerWidth >= Site.bp.xs ? this.carouselClip[this.offsetDimension] / 2 : 0;
                    for (let i = 0; i < this.allCells.length; i++) {
                        const o = this.allCells[i]
                          , l = +o.dataset.id
                          , a = this.curXorY < 0 ? o[this.offsetEdge] - Math.abs(this.curXorY) : o[this.offsetEdge] + Math.abs(this.curXorY)
                          , r = a + o[this.offsetDimension];
                        if (this.curCellID !== l && a < s && r > s) {
                            t = l,
                            o.classList.contains("duplicate") && this.setDuplicateXorY(e);
                            break
                        }
                    }
                }
            } else
                s = this.perView;
        return t || (t = this.curCellID + this.prevNext * s,
        this.options.loop ? "dissolve" === this.options.transition ? t < 1 ? t = this.cells.length : t > this.cells.length && (t = 1) : (t < 1 || t > this.cells.length) && (t -= this.prevNext * this.cells.length,
        this.setDuplicateXorY(e)) : t < 1 ? t = 1 : t + (s - 1) > this.cells.length && (t = this.cells.length - (s - 1))),
        t
    }
    calcXorY(e) {
        let t = -1 * e[this.offsetEdge];
        if (this.options.centreActive || this.options.centreActiveXs && window.innerWidth >= Site.bp.xs)
            t += (this.carouselClip[this.offsetDimension] - e[this.offsetDimension]) / 2;
        else if (!this.options.loop) {
            const e = this.cells[this.cells.length - 1]
              , s = Math.min(0, -1 * (e[this.offsetEdge] + e[this.offsetDimension] - this.carouselClip[this.offsetDimension]));
            t = Math.max(t, s)
        }
        return t
    }
    slide() {
        !1 !== this.duplicateXorY && (this.setXorY(this.duplicateXorY),
        this.duplicateXorY = !1);
        const e = this.calcXorY(this.cells[this.curCellID - 1]);
        let t;
        if ("infinite" === this.options.autoplay) {
            const s = Math.abs(e - this.curXorY);
            t = s * this.options.speed
        } else
            t = Math.min(this.options.duration, Math.abs(e - this.curXorY) / this.carouselClip[this.offsetDimension] * this.options.duration);
        requestAnimationFrame(( () => {
            this.animating = !0,
            this.carousel.style.transitionProperty = "",
            this.carousel.style.transitionDuration = t + "ms",
            requestAnimationFrame(( () => {
                this.setXorY(e),
                this.handleChangeExtras(),
                this.slideTimeout = setTimeout(( () => {
                    this.carousel.style.transitionProperty = "none",
                    this.carousel.style.transitionDuration = "",
                    this.animating = !1,
                    "infinite" === this.options.autoplay && this.autoChangeCell()
                }
                ), t)
            }
            ))
        }
        ))
    }
    dissolve() {
        this.handleChangeExtras()
    }
    handleChangeExtras() {
        if (this.cells.forEach((e => {
            const t = e.querySelector("video");
            if (t ? this.curCellID === +e.dataset.id && 1 == +t.dataset.autoplay ? (t.play(),
            this.options.autoplayByVideo && t.addEventListener("ended", this.changeCellAfterVideo)) : this.curCellID === +e.dataset.id || t.paused || t.pause() : this.options.autoplayByVideo && this.curCellID === +e.dataset.id && setTimeout(( () => {
                this.autoChangeCell()
            }
            ), this.options.autoplayByVideo),
            Site.VimeoControls && this.cells.forEach((e => {
                const t = e.querySelector(".vimeo-wrap");
                if (t) {
                    const e = Site.VimeoControls.players[t.dataset.index];
                    e && e.getPaused().then((s => {
                        s || (e.pause(),
                        Site.VimeoControls.handlePause(e, t))
                    }
                    ))
                }
            }
            )),
            "slide" === this.options.transition && !isNaN(this.perView)) {
                const t = +e.dataset.id;
                t >= this.curCellID && t < this.curCellID + this.perView ? e.classList.add("clickable") : e.classList.remove("clickable")
            }
        }
        )),
        !this.options.loop) {
            if ("slide" === this.options.transition) {
                this.start = this.curXorY >= 0;
                const e = this.cells[this.cells.length - 1];
                this.end = e[this.offsetEdge] + e[this.offsetDimension] + this.curXorY <= this.carouselClip[this.offsetDimension]
            } else
                "dissolve" === this.options.transition && (this.start = 1 === this.curCellID,
                this.end = this.curCellID === this.cells.length);
            this.prevArrows && (this.start ? this.prevArrows.forEach((e => {
                e.disabled = !0
            }
            )) : this.prevArrows.forEach((e => {
                e.disabled = !1
            }
            ))),
            this.nextArrows && (this.end ? this.nextArrows.forEach((e => {
                e.disabled = !0
            }
            )) : this.nextArrows.forEach((e => {
                e.disabled = !1
            }
            )))
        }
        if (this.dots) {
            const e = this.dots.querySelector(".dot.active");
            e && e.classList.remove("active");
            const t = this.dots.querySelector('.dot[data-id="' + this.curCellID + '"]');
            t && t.classList.add("active")
        }
        this.counter && (this.counter.querySelector(".current").innerText = this.curCellID),
        this.carouselWrap.style.setProperty("--current", this.curCellID),
        this.carouselWrap.querySelectorAll(".crsl-cell-extra").forEach((e => {
            +e.dataset.id === this.curCellID ? e.classList.add("active") : e.classList.remove("active")
        }
        )),
        this.carousel.querySelectorAll(".crsl-cell.active").forEach((e => {
            e.classList.remove("active")
        }
        )),
        this.carousel.querySelectorAll('.crsl-cell[data-id="' + this.curCellID + '"]').forEach((e => {
            e.classList.add("active")
        }
        )),
        this.changeCallback && this.changeCallback(this)
    }
    setXorY(e) {
        this.curXorY = e,
        this.carousel.style.transform = "y" === this.options.axis ? "translateY(" + this.curXorY + "px)" : "translateX(" + this.curXorY + "px)"
    }
    setDuplicateXorY(e) {
        const t = 1 === this.prevNext ? "prev" : "next";
        this.duplicateXorY = this.calcXorY(this.carousel.querySelector("." + t + '[data-id="' + this.curCellID + '"]')) - e
    }
    pause() {
        this.carouselWrap.classList.remove("playing"),
        this.autoplayInterval && clearInterval(this.autoplayInterval),
        this.slideTimeout && (clearTimeout(this.slideTimeout),
        this.carousel.style.transitionProperty = "none",
        this.carousel.style.transitionDuration = "",
        this.animating = !1)
    }
    addInteractions() {
        this.canMove && (this.carouselWrap.classList.add("can-move"),
        this.options.interactive && (this.carousel.classList.add("interactive"),
        "drag" === this.options.moveAction ? (this.carouselClip.addEventListener("mousedown", this.pointerStart, {
            passive: !0
        }),
        this.carouselClip.addEventListener("touchstart", this.pointerStart, {
            passive: !0
        }),
        this.carouselClip.addEventListener("mousemove", this.pointerMove, {
            passive: !1
        }),
        this.carouselClip.addEventListener("touchmove", this.pointerMove, {
            passive: !1
        }),
        this.carouselClip.addEventListener("mouseup", this.pointerEnd, {
            passive: !0
        }),
        this.carouselClip.addEventListener("touchend", this.pointerEnd, {
            passive: !0
        }),
        this.carouselClip.addEventListener("mouseleave", this.pointerEnd, {
            passive: !0
        })) : "scroll" === this.options.moveAction && (this.carouselClip.addEventListener("wheel", this.wheel, {
            passive: !0
        }),
        this.carouselClip.addEventListener("touchstart", this.pointerStart, {
            passive: !0
        }),
        this.carouselClip.addEventListener("touchend", this.pointerEnd, {
            passive: !0
        }))),
        this.prevArrows && this.prevArrows.forEach((e => {
            e.addEventListener("click", this.goToCell)
        }
        )),
        this.nextArrows && this.nextArrows.forEach((e => {
            e.addEventListener("click", this.goToCell)
        }
        )),
        "slide" !== this.options.transition || isNaN(this.perView) || this.cells.forEach((e => {
            e.addEventListener("click", this.goToCell)
        }
        )),
        this.dots && this.dots.querySelectorAll(".dot").forEach((e => {
            e.addEventListener("click", this.goToCell)
        }
        )),
        this.carousel.querySelectorAll("a").forEach((e => {
            e.addEventListener("click", this.maybePreventLinkClicks, {
                passive: !1
            })
        }
        )))
    }
    removeInteractions() {
        this.carouselWrap.classList.remove("can-move"),
        this.options.interactive && (this.carousel.classList.remove("interactive"),
        "drag" === this.options.moveAction ? (this.carouselClip.removeEventListener("mousedown", this.pointerStart, {
            passive: !0
        }),
        this.carouselClip.removeEventListener("touchstart", this.pointerStart, {
            passive: !0
        }),
        this.carouselClip.removeEventListener("mousemove", this.pointerMove, {
            passive: !1
        }),
        this.carouselClip.removeEventListener("touchmove", this.pointerMove, {
            passive: !1
        }),
        this.carouselClip.removeEventListener("mouseup", this.pointerEnd, {
            passive: !0
        }),
        this.carouselClip.removeEventListener("touchend", this.pointerEnd, {
            passive: !0
        }),
        this.carouselClip.removeEventListener("mouseleave", this.pointerEnd, {
            passive: !0
        })) : "scroll" === this.options.moveAction && (this.carouselClip.removeEventListener("wheel", this.wheel, {
            passive: !0
        }),
        this.carouselClip.removeEventListener("touchstart", this.pointerStart, {
            passive: !0
        }),
        this.carouselClip.removeEventListener("touchend", this.pointerEnd, {
            passive: !0
        }))),
        this.prevArrows && this.prevArrows.forEach((e => {
            e.removeEventListener("click", this.goToCell)
        }
        )),
        this.nextArrows && this.nextArrows.forEach((e => {
            e.removeEventListener("click", this.goToCell)
        }
        )),
        "slide" !== this.options.transition || isNaN(this.perView) || this.cells.forEach((e => {
            e.removeEventListener("click", this.goToCell)
        }
        )),
        this.dots && this.dots.querySelectorAll(".dot").forEach((e => {
            e.removeEventListener("click", this.goToCell)
        }
        )),
        this.carousel.querySelectorAll("a").forEach((e => {
            e.removeEventListener("click", this.maybePreventLinkClicks, {
                passive: !1
            })
        }
        ))
    }
    cloneCells() {
        const e = !0 === this.options.loop ? this.cells.length : this.options.loop;
        for (let t = 0; t < e; t++) {
            const e = this.cells[this.cells.length - t - 1].cloneNode(!0)
              , s = this.cells[t].cloneNode(!0);
            e.classList.add("duplicate", "prev"),
            s.classList.add("duplicate", "next"),
            Site.LazyLoad && (e.classList.remove(Site.LazyLoad.observedCssClass),
            e.querySelectorAll("." + Site.LazyLoad.observedCssClass).forEach((e => {
                e.classList.remove(Site.LazyLoad.observedCssClass)
            }
            )),
            s.classList.remove(Site.LazyLoad.observedCssClass),
            s.querySelectorAll("." + Site.LazyLoad.observedCssClass).forEach((e => {
                e.classList.remove(Site.LazyLoad.observedCssClass)
            }
            ))),
            Site.FxFadeIn && (e.classList.remove(Site.FxFadeIn.observedCssClass),
            e.querySelectorAll("." + Site.FxFadeIn.observedCssClass).forEach((e => {
                e.classList.remove(Site.FxFadeIn.observedCssClass)
            }
            )),
            s.classList.remove(Site.FxFadeIn.observedCssClass),
            s.querySelectorAll("." + Site.FxFadeIn.observedCssClass).forEach((e => {
                e.classList.remove(Site.FxFadeIn.observedCssClass)
            }
            ))),
            this.carousel.prepend(e),
            this.carousel.append(s)
        }
        this.allCells = this.carousel.querySelectorAll(".crsl-cell")
    }
    setSizeMovementParams() {
        const e = getComputedStyle(this.carouselWrap).getPropertyValue("--per-view");
        this.perView = isNaN(e) ? e.trim() : +e,
        "auto" === this.perView ? this.canMove = this.cells[this.cells.length - 1][this.offsetEdge] + this.cells[this.cells.length - 1][this.offsetDimension] > this.carouselClip[this.offsetDimension] : this.canMove = this.perView < this.cells.length,
        !this.canMove && this.options.centre ? (this.setXorY(0),
        this.carouselWrap.classList.add("centred"),
        "x" === this.options.axis ? this.carousel.style.justifyContent = "center" : this.carousel.style.alignItems = "center") : ("x" === this.options.axis ? this.carousel.style.justifyContent = "" : this.carousel.style.alignItems = "",
        this.carouselWrap.classList.remove("centred")),
        this.canMove && (this.options.autoplay && (this.pause(),
        requestAnimationFrame(( () => {
            this.carouselWrap.classList.add("playing"),
            "infinite" === this.options.autoplay && "slide" === this.options.transition ? this.autoChangeCell() : this.autoplayInterval = setInterval(( () => {
                this.autoChangeCell()
            }
            ), this.options.autoplay)
        }
        ))),
        this.options.autoplayByVideo && this.carouselWrap.classList.add("playing"),
        "slide" === this.options.transition && this.setXorY(this.calcXorY(this.cells[this.curCellID - 1])))
    }
    destroy() {
        this.pause(),
        this.carousel.querySelectorAll(".crsl-cell.duplicate").forEach((e => {
            e.remove()
        }
        )),
        this.removeInteractions(),
        this.carouselWrap.removeAttribute("style"),
        this.carousel.removeAttribute("style"),
        this.initialised = !1
    }
    create() {
        (this.options.dissolveAbove || this.options.slideAbove) && (this.options.dissolveAbove && window.innerWidth >= Site.bp[this.options.dissolveAbove] || this.options.slideAbove && window.innerWidth < Site.bp[this.options.slideAbove] ? (this.options.transition = "dissolve",
        this.options.duration = 600,
        this.carouselWrap.classList.remove("slide"),
        this.carouselWrap.classList.add("dissolve")) : (this.options.transition = "slide",
        this.options.duration = 1e3,
        this.carouselWrap.classList.remove("dissolve"),
        this.carouselWrap.classList.add("slide"))),
        this.carousel.style.transitionTimingFunction = this.options.easing,
        this.cells = this.allCells = this.carousel.querySelectorAll(".crsl-cell"),
        this.curCellID = this.cells[this.options.startCell - 1] ? this.options.startCell : 1,
        "slide" === this.options.transition && this.options.loop && this.cloneCells(),
        this.setSizeMovementParams(),
        this.handleChangeExtras(),
        this.addInteractions(),
        this.initialised = !0
    }
    init() {
        this.initialised && this.destroy(),
        (!this.options.disableAbove || window.innerWidth < Site.bp[this.options.disableAbove]) && (!this.options.disableBelow || window.innerWidth >= Site.bp[this.options.disableBelow]) && this.create()
    }
}
Site.Carousel = {
    carousels: [],
    setup(e, t=!1) {
        const s = e.dataset.options ? JSON.parse(e.dataset.options) : {};
        e.dataset.carouselIndex = Site.Carousel.carousels.length;
        const i = new Carousel(e,s,t);
        Site.Carousel.carousels.push(i)
    },
    resize() {
        Site.Carousel.carousels.forEach((e => {
            e.init()
        }
        ))
    },
    brochureChangeCallback() {
        const e = this.cells[this.curCellID - 1].dataset.type;
        this.carouselWrap.querySelectorAll(".brochure-details").forEach((t => {
            t.dataset.type === e ? t.classList.remove("invis") : t.classList.add("invis")
        }
        ))
    },
    init() {
        document.querySelectorAll(".crsl-wrap:not(.custom)").forEach((e => {
            e.querySelector(".crsl-cell") && Site.Carousel.setup(e)
        }
        )),
        document.querySelectorAll(".crsl-wrap.brochures").forEach((e => {
            e.querySelector(".crsl-cell") && Site.Carousel.setup(e, Site.Carousel.brochureChangeCallback)
        }
        ))
    }
},
Site.Carousel.init(),
Site.Modals = {
    elems: {
        modals: document.querySelectorAll(".modal-wrap"),
        openBtns: document.querySelectorAll(".open-modal"),
        closeBtns: document.querySelectorAll(".close-modal"),
        cmsModalBtns: document.querySelectorAll('a[href^="#"][href$="-modal"]'),
        brochureSelects: document.querySelectorAll("#brochure-modal .brochure-type")
    },
    openModal(e) {
        const t = document.querySelector(".modal-wrap:not(.invis)");
        if (t && Site.Modals.closeModal(t),
        e.classList.remove("invis"),
        e.classList.contains("over-auto"))
            Site.ScrollLock.lock(e);
        else {
            const t = e.querySelector(".modal");
            t && t.classList.contains("over-auto") ? Site.ScrollLock.lock(t) : Site.ScrollLock.lock()
        }
    },
    closeModal(e) {
        if (e.classList.add("invis"),
        e.classList.contains("over-auto"))
            Site.ScrollLock.unlock(e);
        else {
            const t = e.querySelector(".modal");
            t && t.classList.contains("over-auto") ? Site.ScrollLock.unlock(t) : Site.ScrollLock.unlock()
        }
    },
    init() {
        if (Site.Modals.elems.openBtns.forEach((e => {
            e.addEventListener("click", ( () => {
                const t = document.getElementById(e.dataset.id);
                t && (e.dataset.brochureType && Site.Modals.elems.brochureSelects.forEach((t => {
                    t.value = e.dataset.brochureType
                }
                )),
                Site.Modals.openModal(t))
            }
            ))
        }
        )),
        Site.urlQuery.modal) {
            const e = document.getElementById(Site.urlQuery.modal + "-modal");
            e && Site.Modals.openModal(e)
        }
        Site.Modals.elems.cmsModalBtns.forEach((e => {
            e.addEventListener("click", (t => {
                t.preventDefault();
                const s = document.querySelector(e.getAttribute("href"));
                s && Site.Modals.openModal(s)
            }
            ))
        }
        )),
        Site.Modals.elems.closeBtns.forEach((e => {
            e.addEventListener("click", ( () => {
                const t = e.closest(".modal-wrap");
                Site.Modals.closeModal(t)
            }
            ))
        }
        )),
        document.addEventListener("keyup", (e => {
            if (27 == e.keyCode) {
                const e = document.querySelector(".modal-wrap:not(.invis)");
                e && e.querySelector(".close-modal").click()
            }
        }
        )),
        Site.Modals.elems.modals.forEach((e => {
            e.addEventListener("click", (e => {
                e.target.matches(".modal-wrap") && e.target.querySelector(".close-modal").click()
            }
            ))
        }
        ))
    }
},
Site.Modals.init(),
Site.Nav = {
    elems: {
        navWrap: document.getElementById("nav-wrap"),
        nav: document.getElementById("nav"),
        navBtn: document.getElementById("nav-btn")
    },
    openDropdownTimeout: !1,
    closeDropdownTimeout: !1,
    handleDropdownBtnClick(e) {
        e.preventDefault();
        const t = e.currentTarget
          , s = e.currentTarget.closest("li");
        if (window.innerWidth >= Site.bp.lg)
            s.classList.contains("active") ? location.href = t.getAttribute("href") : (Site.Nav.elems.nav.querySelectorAll("ul.menu > li.active").forEach((e => {
                e.classList.remove("active")
            }
            )),
            s.classList.add("active"));
        else {
            const e = s.lastElementChild;
            s.classList.contains("active") ? (s.classList.remove("active"),
            e.style.height = "") : (Site.Nav.elems.nav.querySelectorAll("ul.menu > li.active").forEach((e => {
                e.classList.remove("active"),
                e.lastElementChild.style.height = ""
            }
            )),
            s.classList.add("active"),
            e.style.height = e.scrollHeight + "px")
        }
    },
    resize() {
        window.innerWidth >= Site.bp.lg && (Site.elems.body.classList.remove("nav-open"),
        Site.Nav.elems.nav.querySelectorAll("li.active").forEach((e => {
            e.classList.remove("active"),
            e.querySelector(".sub-menu").style.height = ""
        }
        )),
        Site.ScrollLock.unlock(Site.Nav.elems.nav))
    },
    init() {
        document.addEventListener("click", (e => {
            if (window.innerWidth >= Site.bp.lg && !e.target.matches("#nav") && !e.target.closest("#nav")) {
                const e = Site.Nav.elems.nav.querySelector("ul.menu > li.active");
                e && e.classList.remove("active")
            }
        }
        )),
        Site.Nav.elems.nav.querySelectorAll("li.menu-item-has-children > a").forEach((e => {
            e.addEventListener("touchend", Site.Nav.handleDropdownBtnClick, {
                passive: !1
            }),
            e.addEventListener("click", Site.Nav.handleDropdownBtnClick)
        }
        )),
        Site.Nav.elems.nav.querySelectorAll("ul.menu > li.menu-item-has-children").forEach((e => {
            e.addEventListener("mouseenter", ( () => {
                if (window.innerWidth >= Site.bp.lg) {
                    const t = Site.elems.body.classList.contains("nav-open") ? 0 : 300;
                    clearTimeout(Site.Nav.closeDropdownTimeout),
                    Site.Nav.openDropdownTimeout = setTimeout(( () => {
                        console.log("actually open dropdown"),
                        Site.elems.body.classList.add("nav-open"),
                        Site.elems.body.classList.remove("search-open"),
                        Site.Nav.elems.nav.querySelectorAll("ul.menu > li.menu-item-has-children").forEach((t => {
                            t !== e && t.classList.contains("active") && t.classList.remove("active")
                        }
                        )),
                        e.classList.add("active")
                    }
                    ), t)
                }
            }
            )),
            e.addEventListener("mouseleave", ( () => {
                window.innerWidth >= Site.bp.lg && (clearTimeout(Site.Nav.openDropdownTimeout),
                e.classList.remove("active"),
                Site.Nav.closeDropdownTimeout = setTimeout(( () => {
                    console.log("actually close dropdown"),
                    Site.elems.body.classList.remove("nav-open")
                }
                ), 300))
            }
            ))
        }
        )),
        Site.Nav.elems.navBtn.addEventListener("click", ( () => {
            Site.elems.body.classList.contains("nav-open") ? (Site.Nav.elems.nav.querySelectorAll("li.active").forEach((e => {
                e.classList.remove("active"),
                e.querySelector(".sub-menu").style.height = ""
            }
            )),
            Site.elems.body.classList.remove("nav-open"),
            Site.ScrollLock.unlock(Site.Nav.elems.nav)) : (Site.ScrollLock.lock(Site.Nav.elems.nav),
            Site.Nav.elems.nav.scrollTop = 0,
            Site.elems.body.classList.add("nav-open"))
        }
        )),
        Site.Nav.resize()
    }
},
Site.Nav.init(),
Site.ScrollHeader = {
    scrollY: window.scrollY,
    direction: "down",
    updateHeader() {
        const e = window.scrollY;
        Site.ScrollHeader.direction = e > Site.ScrollHeader.scrollY ? "down" : "up",
        Site.ScrollHeader.scrollY = e,
        "down" === Site.ScrollHeader.direction && Site.ScrollHeader.scrollY > 100 && !Site.elems.body.classList.contains("header-hide") ? Site.elems.body.classList.add("header-hide") : "up" === Site.ScrollHeader.direction && Site.elems.body.classList.contains("header-hide") && Site.elems.body.classList.remove("header-hide"),
        Site.ScrollHeader.scrollY > 100 && !Site.elems.body.classList.contains("scroll") ? Site.elems.body.classList.add("scroll") : Site.ScrollHeader.scrollY <= 100 && Site.elems.body.classList.contains("scroll") && Site.elems.body.classList.remove("scroll")
    },
    resize() {
        Site.ScrollHeader.updateHeader()
    },
    init() {
        window.addEventListener("scroll", ( () => {
            Site.ScrollHeader.updateHeader()
        }
        ), {
            passive: !0
        }),
        Site.ScrollHeader.resize()
    }
},
Site.ScrollHeader.init(),
Site.ScrollableList = {
    elems: {
        lists: document.querySelectorAll(".scrollable-list")
    },
    update(e) {
        const t = e.querySelector(".scroller");
        t.scrollLeft > 0 ? e.classList.add("can-scroll-left") : e.classList.remove("can-scroll-left"),
        e.offsetWidth + t.scrollLeft < t.scrollWidth ? e.classList.add("can-scroll-right") : e.classList.remove("can-scroll-right")
    },
    resize() {
        Site.ScrollableList.elems.lists.forEach((e => {
            Site.ScrollableList.update(e)
        }
        ))
    },
    init() {
        Site.ScrollableList.elems.lists.forEach((e => {
            const t = e.querySelector(".scroller");
            t.addEventListener("scroll", ( () => {
                Site.ScrollableList.update(e)
            }
            ));
            const s = t.querySelector(".active");
            s && (t.scrollLeft = s.offsetLeft - 40)
        }
        )),
        Site.ScrollableList.resize()
    }
},
Site.ScrollableList.init(),
Site.Search = {
    elems: {
        headerSearchBtn: document.getElementById("header-search-btn")
    },
    init() {
        document.addEventListener("click", (e => {
            !Site.elems.body.classList.contains("search-open") || e.target.matches("#header-search-wrap") || e.target.closest("#header-search-wrap") || Site.elems.body.classList.remove("search-open")
        }
        )),
        Site.Search.elems.headerSearchBtn.addEventListener("click", ( () => {
            Site.elems.body.classList.contains("search-open") ? Site.elems.body.classList.remove("search-open") : Site.elems.body.classList.add("search-open")
        }
        ))
    }
},
Site.Search.init(),
Site.VideosControls = {
    elems: {
        videos: document.querySelectorAll(".video-wrap")
    },
    handlePlay(e) {
        const t = e.currentTarget;
        t.controls = !0,
        t.parentElement.classList.add("playing")
    },
    handlePause(e) {
        const t = e.currentTarget;
        t.seeking || (t.pause(),
        t.controls = !1,
        t.parentElement.classList.remove("playing"))
    },
    init() {
        Site.VideosControls.elems.videos.forEach((e => {
            const t = e.querySelector("video")
              , s = e.querySelector(".play-btn");
            t.addEventListener("play", Site.VideosControls.handlePlay),
            t.addEventListener("pause", Site.VideosControls.handlePause),
            t.addEventListener("ended", Site.VideosControls.handlePause),
            s.addEventListener("click", ( () => {
                t.play()
            }
            ))
        }
        ))
    }
},
Site.VideosControls.init();
