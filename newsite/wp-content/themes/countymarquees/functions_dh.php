<?php

/******
Head/Footer Assets
******/

function enqueue_assets() {
    $version = wp_get_theme()->get('Version');

    // Remove styles/scripts
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
    wp_dequeue_style('global-styles');
    wp_deregister_script('wp-embed');

    // CSS
    wp_register_style('theme', get_template_directory_uri() .'/style.css', false, $version);
    wp_enqueue_style('theme');

    // JS
    wp_register_script('theme', get_template_directory_uri() .'/js/site.js', false, $version, true);
    wp_enqueue_script('theme');

    // Home
    if (is_page_template('page-home.php')) {
        wp_register_script('home', get_template_directory_uri() .'/js/pages/home.js', false, $version, true);
        wp_enqueue_script('home');
    }

    // Recaptcha
    wp_register_script('recaptcha', 'https://www.google.com/recaptcha/api.js', false, null, true);
    wp_enqueue_script('recaptcha');
}
add_action('wp_enqueue_scripts', 'enqueue_assets');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


function wp_head_additions() { ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/fonts/SangBleuKingdom-Light-WebS.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/fonts/SangBleuKingdom-Regular-WebS.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/fonts/untitled-sans-web-regular.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/fonts/untitled-sans-web-medium.woff2" type="font/woff2" crossorigin="anonymous">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5VQ7LT77');</script>
    <!-- End Google Tag Manager -->
<?php }
add_action('wp_head', 'wp_head_additions', 0);


function remove_tag_type_attribute($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}
add_filter('style_loader_tag', 'remove_tag_type_attribute', 10, 2);
add_filter('script_loader_tag', 'remove_tag_type_attribute', 10, 2);


add_theme_support('title-tag'); 


/******
Admin
******/

add_filter('use_block_editor_for_post', '__return_false', 5);


function register_menus() {
    register_nav_menus([
        'main-menu-left' => 'Main Menu Left',
        'main-menu-right' => 'Main Menu Right',
        'services-menu' => 'Services Menu',
        'about-menu' => 'About Menu',
        'footer-legal-menu' => 'Footer Legal Menu',
    ]);
}
add_action('init', 'register_menus');


function remove_menus() {
    remove_menu_page('edit-comments.php');

    if (get_current_user_id() != 1) {
        remove_menu_page('edit.php?post_type=acf-field-group');
    }
}
add_action('admin_menu', 'remove_menus');


/******
Custom post types and taxonomies
******/

function register_custom_post_types() {
    register_taxonomy('product-category', 'product', [
        'hierarchical' => true,
        'labels' => [
            'name' => 'Product Categories',
            'singular_name' => 'Product Category',
            'search_items' =>  'Search Product Categories',
            'all_items' => 'All Product Categories',
            'parent_item' => 'Parent Product Category',
            'parent_item_colon' => 'Parent Product Category:',
            'edit_item' => 'Edit Product Category',
            'view_item' => 'View Product Category',
            'update_item' => 'Update Product Category',
            'add_new_item' => 'Add New Product Category',
            'new_item_name' => 'New Product Category Name',
            'not_found' => 'No product categories found',
            'no_terms' => 'No product categories',
            'filter_by_item' => 'Filter by product category',
        ],
        'rewrite' => [
            'hierarchical' => true
        ]
    ]);

    register_taxonomy('product-service', 'product', [
        'hierarchical' => true,
        'labels' => [
            'name' => 'Product Services',
            'singular_name' => 'Product Service',
            'search_items' =>  'Search Product Services',
            'all_items' => 'All Product Services',
            'parent_item' => 'Parent Product Service',
            'parent_item_colon' => 'Parent Product Service:',
            'edit_item' => 'Edit Product Service',
            'view_item' => 'View Product Service',
            'update_item' => 'Update Product Service',
            'add_new_item' => 'Add New Product Service',
            'new_item_name' => 'New Product Service Name',
            'not_found' => 'No product services found',
            'no_terms' => 'No product services',
            'filter_by_item' => 'Filter by product service',
        ]
    ]);

    register_post_type('product',
        [
            'supports' => ['title', 'page-attributes'],
            'labels' => [
                'name' => 'Products',
                'singular_name' => 'Product'
            ],
            'public' => true
        ]
    );
}
add_action('init', 'register_custom_post_types');


/******
ACF
******/

acf_add_options_page([
    'page_title'    => 'CMS Options',
    'menu_title'    => 'CMS Options',
    'menu_slug'     => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Contact Details',
    'menu_title'    => 'Contact Details',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Footer',
    'menu_title'    => 'Footer',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Global Modules',
    'menu_title'    => 'Global Modules',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Journal',
    'menu_title'    => 'Journal',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Miscellaneous',
    'menu_title'    => 'Miscellaneous',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Products',
    'menu_title'    => 'Products',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Search',
    'menu_title'    => 'Search',
    'parent_slug'   => 'cms-options'
]);

acf_add_options_sub_page([
    'page_title'    => 'Subscribe Form',
    'menu_title'    => 'Subscribe Form',
    'parent_slug'   => 'cms-options'
]);


add_filter('acf/field_group/disable_field_settings_tabs', '__return_true');


function customise_wysiwyg_fields($field) {
    $field['media_upload'] = false;

    return $field;
}
add_filter('acf/prepare_field/type=wysiwyg', 'customise_wysiwyg_fields');


/******
Images
******/

function set_jpeg_compression() {
    return 90;
}
add_filter('jpeg_quality', 'set_jpeg_compression');


function allow_svgs($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'allow_svgs');


function limit_large_images() {
    return 2048;
}
add_filter('big_image_size_threshold', 'limit_large_images');


function disable_wp_image_sizes($sizes) {
    unset($sizes['thumbnail']);
    unset($sizes['medium_large']);
    unset($sizes['large']);
    unset($sizes['1536x1536']);
    unset($sizes['2048x2048']);
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'disable_wp_image_sizes');


add_image_size('sm', 720, 0);
add_image_size('md', 960, 0);
add_image_size('lg', 1536, 0);
add_image_size('xl', 2048, 0);


function render_image($image, $size, $css, $lazy) {
    if (!$image) {
        return;
    }
    
    $responsive = false;
    $height_attr = $width_attr = $src = $srcset = $sizes = '';

    // This must come first to prevent responsive stuff for SVGs/GIFs
    if ($size == 'full' || $image['subtype'] == 'svg' || $image['subtype'] == 'gif') {
        $default_size = $image['url'];
        $height = $image['height'];
        $width = $image['width'];
    }
    else if (is_array($size)) {
        $responsive = true;
        $default_size = $image['sizes']['xl'];
        $height = $image['height'];
        $width = $image['width'];

        $srcset = $image['sizes']['xl'] .' 2048w, ';
        $srcset .= $image['sizes']['lg'] .' 1536w, ';
        $srcset .= $image['sizes']['md'] .' 960w, ';
        $srcset .= $image['sizes']['sm'] .' 720w';

        $sizes = '(min-width: 1260px) '. $size[3] .'vw, ';
        $sizes .= '(min-width: 1024px) '. $size[2] .'vw, ';
        $sizes .= '(min-width: 768px) '. $size[1] .'vw, ';
        $sizes .= '(min-width: 600px) '. $size[0] .'vw, ';
        $sizes .= '100vw';
    }
    else {
        $default_size = $image['sizes'][$size];
        $height = $image['sizes'][$size .'-height'];
        $width = $image['sizes'][$size .'-width'];
    }

    if ($lazy && $image['subtype'] != 'svg') {
        $css .= ' lazy';
        $src = ' src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 '. $width .' '. $height .'\'%3E%3C/svg%3E" data-src="'. $default_size .'"';
        if ($responsive) {
            $srcset = ' data-srcset="'. $srcset .'"';
            $sizes = ' data-sizes="'. $sizes .'"';
        }
    }
    else {
        $src = ' src="'. $default_size .'"';
        if ($responsive) {
            $srcset = ' srcset="'. $srcset .'"';
            $sizes = ' sizes="'. $sizes .'"';
        }
    }

    if ($image['subtype'] != 'svg') {
        $height_attr = ' height="'. $height .'"';
        $width_attr = ' width="'. $width .'"';
    } ?>

    <img class="<?php echo $css; ?>"<?php echo $src . $srcset . $sizes . $height_attr . $width_attr; ?> alt="<?php echo $image['alt']; ?>">
<?php }


/******
UI - Global
******/

class Custom_Menu extends Walker_Nav_Menu {
    private $images;
    private $image_ids;
    private $top_level_item_id;


    public function __construct() {
        $this->images = get_field('dropdown_images', 'option');
        $this->image_ids = array_column($this->images, 'id');
    }


    function start_lvl(&$output, $depth = 0, $args = []) {
        $output .= '<div class="sub-menu-wrap">';
        $key = array_search($this->top_level_item_id, $this->image_ids);
        
        if ($key !== false) {
            $image = $this->images[$key]['image'];
            $output .= '<img src="'. $image['sizes']['lg'] .'" class="menu-image hidden block-lg" alt="Background image">';
        }
        
        $output .= '<ul class="sub-menu">';
    }


    function end_lvl( &$output, $depth = 0, $args = []) {
        $output .= "</ul></div>";
    }


    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0) {
        if ($depth == 0) {
            $this->top_level_item_id = sanitize_title($item->title);
        }
            
        $output .= '<li class="'. implode(' ', $item->classes) .'">';

        if ($item->url == '#' && $depth == 1) {
            $output .= '<div class="small-caps sans white opc-70">'. $item->title .'</div>';
        }
        else {
            $output .= '<a href="'. $item->url .'">'. $item->title .'</a>';
        }
    }
}


function add_custom_body_classes($classes) {
    if (is_singular('product') || is_page_template(['page-home.php'])) {
        $classes[] = 'white-header';
    }

    return $classes;
}
add_filter('body_class', 'add_custom_body_classes');


function render_telephone_link($number, $css = '') {
    // Remove spaces and bracketed zeroes
    $safe_number = str_replace(' ', '', $number);
    $safe_number = str_replace('(0)', '', $safe_number);
    
    // If first digit is still zero, replace that with UK code
    if (strpos($safe_number, '0') === 0) {
        $safe_number = substr($safe_number, 1);
        $safe_number = '+44'. $safe_number;
    } ?>
    <a href="tel:<?php echo $safe_number; ?>" class="<?php echo $css; ?>"><?php echo esc_html($number); ?></a>
<?php }


function render_accordion_block($items) { ?>
    <div class="accordion-group">
        <?php foreach ($items as $item) : ?>
            <div class="accordion-item pad-t-20 pad-b-20">
                <h3><button type="button" class="accordion-btn reg flex align-c jstfy-s"><?php echo esc_html($item['title']); ?></button></h3>
                <div class="accordion-content anim-height">
                    <div class="pad-t-20 cms">
                        <?php echo $item['text']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php }


/******
UI - Modules
******/

function render_contact_cta_panel() {
    $block = get_field('contact_cta', 'option'); ?>
    <div class="bg-dark-teal pad-t-100 pad-b-100 text-c white">
        <div class="wrapper">
            <h2 class="small-title text-c reg less-mar"><?php echo esc_html($block['title']); ?></h2>
            <div class="mw mar-auto large cms mar-b-40">
                <?php echo $block['text']; ?>
            </div>
            <button type="button" class="btn wide bdr-white open-modal" data-id="enquiry-modal"><?php echo esc_html($block['button_text']); ?></button>
        </div>
    </div>
<?php }


function render_brochures_panel($first = 'classic') {
    $brochures = [
        'classic' => get_field('classic_brochure', 'option'),
        'commercial' => get_field('commercial_brochure', 'option'),
        'combined_images' => []
    ];

    foreach ($brochures['classic']['images'] as $key => $image) {
        $brochures['classic']['images'][$key]['brochure_type'] = 'classic';
    }

    foreach ($brochures['commercial']['images'] as $key => $image) {
        $brochures['commercial']['images'][$key]['brochure_type'] = 'commercial';
    }

    if ($first == 'classic') {
        $brochures['combined_images'] = array_merge($brochures['classic']['images'], $brochures['commercial']['images']);
    }
    else if ($first == 'commercial') {
        $brochures['combined_images'] = array_merge($brochures['commercial']['images'], $brochures['classic']['images']);
    } ?>
    <div class="over-hide pad-t-100 pad-b-100">
        <div class="wrapper">
            <?php $carousel_opts = [
                'loop' => count($brochures),
                'centreActive' => 1,
            ]; ?>
            <div class="crsl-wrap slide custom brochures" data-options="<?php echo htmlspecialchars(json_encode($carousel_opts), ENT_QUOTES, 'UTF-8'); ?>">
                <div class="crsl-header mar-b-40 flex align-c spc-btwn">
                    <h2 class="smaller-title reg mar-b-0"><?php echo esc_html(get_field('brochure_panel_title', 'option')); ?></h2>
                    <div class="crsl-nav flex align-c">
                        <button type="button" class="crsl-arw prev" aria-label="Previous"></button>
                        <button type="button" class="crsl-arw next" aria-label="Next"></button>
                    </div>
                </div>

                <div class="crsl-clip mar-b-40">
                    <div class="crsl">
                        <?php $i = 0;
                        foreach ($brochures['combined_images'] as $image) :
                            $i++;  ?>
                            <div class="crsl-cell" data-type="<?php echo $image['brochure_type']; ?>" data-id="<?php echo $i; ?>">
                                <?php render_image($image, 'md', 'h-100', true); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="grid no-gap text-c">
                    <?php foreach (['classic', 'commercial'] as $type) : ?>
                        <div class="brochure-details col-12 overlap fade invis" data-type="<?php echo $type; ?>">
                            <div class="small-caps medium sans mar-b-15"><?php echo esc_html($brochures[$type]['pretitle']); ?></div>
                            <div class="small-title mar-b-0"><?php echo esc_html($brochures[$type]['title']); ?></div>
                            <div class="small-caps medium sans mar-t-15 mar-b-30"><?php echo esc_html($brochures[$type]['page_numbers']); ?></div>
                            <button type="button" class="btn wide bdr-off-black open-modal" data-id="brochure-modal" data-brochure-type="<?php echo $type; ?>"><?php echo esc_html($brochures[$type]['download_button_text']); ?></button>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php }


/******
UI - Archives/Taxonomies
******/

function customise_post_queries($query) {    
    // For top-level product category main queries, exclude featured posts and set per page
    if ($query->is_main_query() && (is_tax('product-category') || is_tax('product-service'))) {
        $term = get_queried_object();

        if ($term->parent === 0) {
            $excluded_ids = get_featured_products($term, $term->taxonomy);

            $query->set('posts_per_page', 11);
            $query->set('post__not_in', $excluded_ids);
        }
    }

    return $query;
}
if (!is_admin()) {
    add_filter('pre_get_posts', 'customise_post_queries');
}


function render_product_taxonomy($taxonomy) {
    global $wp_query;

    // Get term objects
    $page_term = get_queried_object();
    $top_level_term = $page_term->parent ? get_term($page_term->parent) : $page_term;

    // Get CMS fields
    $h1_override = get_field('h1_override', $page_term);
    $seo_text = get_field('seo_text', $page_term);
    $show_info = get_field('show_information', $page_term);

    // Get featured products (for first page of top-level categories only)
    if ($page_term->parent === 0 && $wp_query->get('paged') < 2) {
        $featured_ids = get_featured_products($top_level_term, $taxonomy);
    }

    if ($h1_override) {
        $title = $h1_override;
    }
    else {
        if ($top_level_term == $page_term) {
            $title = __('All ', 'theme') .' '. $top_level_term->name;
        }
        else {
            $title = $page_term->name .' '. $top_level_term->name;
        }
    } ?>

    <div class="wrapper mar-t-60">
        <h1 class="small-title bigger-mobile text-c reg more-mar"><?php echo $title; ?></h1>
    </div>

    <?php if ($taxonomy == 'product-category') {
        $sub_cats = get_terms('taxonomy='. $taxonomy .'&parent='. $top_level_term->term_id);
        render_terms_sub_nav($sub_cats, $page_term, get_term_link($top_level_term), $taxonomy);
    } ?>

    <div class="wrapper">
        <div class="text-c mw mar-auto large cms mar-b-100">
            <p><?php if ($page_term->description) echo $page_term->description; else echo $top_level_term->description; ?></p>
        </div>

        <?php // Only the first page of the "type" taxonomy has featured products
        if ($featured_ids) : ?>
            <div id="leading-products" class="grid c-gap-30 mar-b-100">
                <?php $i = 0;
                foreach ($featured_ids as $post_id) :
                    $i++;

                    $shape = $i == 1 ? 'landscape' : 'portrait';
                    $image = get_field('featured_image_'. $shape, $post_id); ?>

                    <a href="<?php echo get_permalink($post_id); ?>" class="block product-link col-12 col-6-sm">
                        <div class="<?php echo $shape; ?>-box rel mar-b-40">
                            <?php render_image($image, [100, 50, 50, 50], 'abs-box obj-cvr', true); ?>
                        </div>
                        <h2 class="med-title caps less-mar"><?php echo get_the_title($post_id); ?></h2>
                        <div class="cms mw-lesser">
                            <?php the_field('featured_text', $post_id); ?>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <div class="grid product-listings c-gap-30 mar-b-100">
            <?php $i = 0;
            while (have_posts()) : the_post();
                $i++;

                if ($i == 6) {
                    get_template_part('loop-product-cta');
                }
                else {
                    get_template_part('loop-product', '', [ 'image' => 'either' ]);
                }
            endwhile; ?>
        </div>

        <?php render_pagination();

        // SEO text
        if ($seo_text) : ?>
            <div class="mw mar-auto large text-c cms mar-b-100">
                <?php echo $seo_text; ?>
            </div>
        <?php endif; ?>
    </div>

    <?php if ($show_info) : ?>
        <div class="bg-cream pad-t-100 pad-b-100">
            <div class="wrapper">
                <h2 class="smaller-title reg more-mar"><?php echo esc_html(get_field('information_title', $page_term)); ?></h2>
                <div class="mw mar-auto">
                    <?php render_accordion_block(get_field('information', $page_term)); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php render_contact_cta_panel(); ?>

    <?php render_taxonomy_related_articles($page_term); ?>

    <?php render_brochures_panel();
}


function get_first_term($post_id, $taxonomy) {
    $terms = get_terms([
        'object_ids' => $post_id,
        'taxonomy' => $taxonomy,
        'parent' => 0
    ]);

    if ($terms) {
        return $terms[0];
    }

    return false;
}


function get_featured_products($term, $taxonomy) {
    // Build query for featured products
    $args = [
        'post_type' => 'product',
        'posts_per_page' => 3,
        'meta_key' => 'featured',
        'meta_value' => 1,
        'tax_query' => [
            [
                'taxonomy' => $taxonomy,
                'terms' => $term->term_id
            ]
        ],
        'fields' => 'ids'
    ];

    $featured_products = new WP_Query($args);

    return $featured_products->posts;
}


function render_terms_sub_nav($terms, $page_term, $all_link, $taxonomy) { ?>
    <div class="wrapper mar-b-40 flex jstfy-c wrap">
        <a class="sub-nav-link no-shrink sans tiny-caps<?php if (($taxonomy == 'category' && !$page_term) || ($taxonomy != 'category' && $page_term->parent === 0)) echo ' active'; ?>" href="<?php echo $all_link; ?>"><?php echo __('All', 'theme'); ?></a>
        <?php foreach ($terms as $term) : ?>
            <a class="sub-nav-link no-shrink sans tiny-caps<?php if ($page_term->term_id == $term->term_id) echo ' active'; ?>" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
        <?php endforeach; ?>
    </div>
<?php }


function render_pagination() {
    global $wp_query;

    if ($wp_query->max_num_pages > 1) :
        $page_links = paginate_links([
            'prev_next' => false,
            'mid_size' => 1
        ]);

        $page_num = $wp_query->get('paged') === 0 ? 1 : $wp_query->get('paged'); ?>
        <div class="pagination mar-b-100">
            <div class="flex align-c jstfy-c reg">
                <?php if ($wp_query->get('paged') > 0) :
                    $prev_link = previous_posts(false); ?>
                    <a href="<?php echo esc_url($prev_link); ?>" class="nav-link mar-r-10"><?php echo __('Prev', 'theme'); ?></a>
                <?php endif; ?>

                <div class="page-links flex">
                    <?php echo $page_links; ?>
                </div>

                <?php if ($wp_query->get('paged') < $wp_query->max_num_pages) :
                    $next_link = next_posts(0, false); ?>
                    <a href="<?php echo esc_url($next_link); ?>" class="nav-link mar-l-10"><?php echo __('Next', 'theme'); ?></a>
                <?php endif; ?>
            </div>
        </div>
    <?php endif;
}


/******
UI - Carousels
******/

function render_products_carousel($products) {
    global $post;

    $carousel_opts = [
        'disableAbove' => 'sm'
    ]; ?>
    <div class="crsl-wrap slide mobile" data-options="<?php echo htmlspecialchars(json_encode($carousel_opts), ENT_QUOTES, 'UTF-8'); ?>">
        <div class="crsl-clip">
            <div class="crsl">
                <?php $i = 0;
                while ($products->have_posts()) : $products->the_post();
                    $i++;
                    $term = get_first_term($post->ID, 'product-category'); ?>
                    <div class="crsl-cell" data-id="<?php echo $i; ?>">
                        <a href="<?php the_permalink(); ?>" class="block">
                            <div class="portrait-box rel mar-b-20">
                                <?php render_image(get_field('featured_image_portrait'), [50, 50, 33.333, 33.333], 'abs-box obj-cvr', true); ?>
                            </div>
                            <div class="sans small-caps medium mar-b-15"><?php echo $term->name; ?></div>
                            <h3 class="small-title reg mar-b-0"><?php the_title(); ?></h3>
                        </a>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
<?php }


function render_journal_carousel($articles) {
     global $post;

     $carousel_opts = [
        'disableAbove' => 'sm'
    ]; ?>
    <div class="crsl-wrap slide mobile" data-options="<?php echo htmlspecialchars(json_encode($carousel_opts), ENT_QUOTES, 'UTF-8'); ?>">
        <div class="crsl-clip">
            <div class="crsl">
                <?php $i = 0;
                while ($articles->have_posts()) : $articles->the_post();
                    $i++;
                    $term = get_first_term($post->ID, 'category'); ?>
                    <div class="crsl-cell" data-id="<?php echo $i; ?>">
                        <a href="<?php the_permalink(); ?>" class="block">
                            <div class="landscape-box rel mar-b-20">
                                <?php render_image(get_field('featured_image'), [50, 50, 33.333, 33.333], 'abs-box obj-cvr', true); ?>
                            </div>
                            <div class="sans small-caps medium mar-b-15"><?php echo $term->name; ?></div>
                            <h3 class="small-title reg mar-b-0"><?php the_title(); ?></h3>
                        </a>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
<?php }


function render_taxonomy_related_articles($top_level_term) {
    $args = [
        'posts_per_page' => 3,
        'orderby' => 'RAND',
        'tax_query' => [
            [
                'taxonomy' => 'post_tag',
                'field' => 'slug',
                'terms' => $top_level_term->term_id
            ]
        ],
    ];

    $related_articles = new WP_Query($args);
    if ($related_articles->have_posts()) : ?>
        <div class="over-hide bg-cream pad-t-100 pad-b-100">
            <div class="wrapper">
                <h2 class="smaller-title reg more-mar"><?php echo esc_html(get_field('related_articles_title', 'option')); ?></h2>
                <?php render_journal_carousel($related_articles); ?>
            </div>
        </div>
    <?php endif;
}


/******
UI - Misc.
******/

function render_page_sub_nav_intro() {
    global $post; ?>

    <div class="wrapper mar-t-60 mar-b-100">
        <h1 class="small-title text-c reg more-mar"><?php the_title(); ?></h1>

        <div id="page-sub-nav" class="mar-b-60 sans tiny-caps">
            <?php if ($post->post_parent) {
                wp_nav_menu([ 'theme_location' => 'about-menu' ]);
            }
            else {
                wp_nav_menu([ 'theme_location' => 'services-menu' ]);                
            } ?>
        </div>

        <div class="text-c mw mar-auto large cms mar-b-60">
            <?php the_field('intro_text'); ?>
        </div>
    </div>
<?php }


/******
UI - Forms
******/

function render_text_input($field_name, $form_type, $form_fields, $validation_classes) { ?>
    <div class="col-12 col-6-xs field-wrap">
        <label for="<?php echo $form_type; ?>-<?php echo $field_name; ?>" class="tiny-caps sans mar-b-5"><?php echo esc_html($form_fields[$field_name .'_label']); ?></label>
        <input id="<?php echo $form_type; ?>-<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="text <?php echo $validation_classes; ?>">
    </div>
<?php }


function render_how_heard($form_type, $form_fields) { ?>
    <div class="col-12 col-6-xs field-wrap">
        <label for="<?php echo $form_type; ?>-how_heard" class="tiny-caps sans mar-b-5"><?php echo esc_html($form_fields['how_heard_label']); ?></label>
        <select id="<?php echo $form_type; ?>-how_heard" name="how_heard" class="required">
            <option value=""><?php echo __('Please select:', 'theme'); ?></option>
            <option value="Advertising">Advertising</option>
            <option value="Exhibitions/Shows">Exhibitions/Shows</option>
            <option value="Designer Referral">Designer Referral</option>
            <option value="Editorial">Editorial</option>
            <option value="Search Engine">Search Engine</option>
            <option value="Social Media">Social Media</option>
            <option value="Word of Mouth">Word of Mouth</option>
        </select>
    </div>
<?php }


function render_country($form_type, $form_fields) { ?>
    <div class="col-12 col-6-xs field-wrap">
        <label for="<?php echo $form_type; ?>-country" class="tiny-caps sans mar-b-5"><?php echo esc_html($form_fields['country_label']); ?></label>
        <select id="<?php echo $form_type; ?>-country" name="country" class="required">
            <option value=""><?php echo __('Please select:', 'theme'); ?></option>
            <?php foreach ($GLOBALS['countries'] as $code => $name) : ?>
                <option value="<?php echo $name; ?>"><?php echo esc_html($name); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
<?php }


function render_form_bottom($form_type, $form_fields) {
    $field_suffix = $form_type == 'enquiry' ? '' : '_'. str_replace('brochure-', '', $form_type); ?>
    <div class="field-group checkbox-group mar-b-30">
        <label class="field-wrap checkbox-wrap flex">
            <input type="checkbox" name="newsletter" value="1">
            <span class="block">
                <span class="block reg mar-b-5"><?php echo esc_html($form_fields['newsletter_title']); ?></span>
                <span class="block tiny sans"><?php echo esc_html($form_fields['newsletter_label']); ?></span>
            </span>
        </label>
    </div>

    <div class="g-recaptcha mar-b-30" data-sitekey="6Ld6hvgpAAAAADeUXHWwXUfMq3YlurJFSQAGHXb6"></div>
        
    <div class="grid sans small align-c">
        <div class="col-12 overlap fade submit">
            <button type="submit" class="btn wide bg-off-black"><?php echo esc_html($form_fields['submit_button_text'. $field_suffix]); ?></button>
        </div>
        <div class="col-12 overlap fade sending invis">
            <?php echo esc_html($form_fields['please_wait_message']); ?>
        </div>
        <div class="col-12 overlap fade success invis">
            <?php echo esc_html($form_fields['success_message'. $field_suffix]); ?>
        </div>
        <div class="col-12 overlap fade error error-red invis">
            <?php echo esc_html($form_fields['error_message']); ?>
        </div>
    </div>
<?php }


function render_enquiry_form($type) {
    global $post;

    $form_fields = get_field('enquiry_form', 'option'); ?>
    <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="validate-form" data-reset="1">
        <input type="hidden" name="action" value="process_sf_form">
        <input type="hidden" name="form_type" value="enquiry">
        <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
        
        <?php if ($type == 'contact') : ?>
            <div class="smaller-title reg more-mar"><?php echo esc_html($form_fields['title']); ?></div>
        <?php else : ?>
            <div class="med-title reg"><?php echo esc_html($form_fields['title']); ?></div>
        <?php endif; ?>

        <div class="grid gap-30 mar-b-30">
            <?php render_text_input('first_name', 'enquiry', $form_fields, 'required'); ?>
            <?php render_text_input('last_name', 'enquiry', $form_fields, 'required'); ?>
            <?php render_text_input('email', 'enquiry', $form_fields, 'required email'); ?>
            <?php render_text_input('phone', 'enquiry', $form_fields, 'required'); ?>
            <?php render_text_input('company', 'enquiry', $form_fields, ''); ?>

            <div class="col-12 col-6-xs field-wrap">
                <label for="enquiry-type" class="tiny-caps sans mar-b-5"><?php echo esc_html($form_fields['enquiry_type_label']); ?></label>
                <select id="enquiry-type" name="enquiry_type">
                    <option value=""><?php echo __('Please select:', 'theme'); ?></option>
                    <?php foreach ($form_fields['enquiry_type_options'] as $row) : ?>
                        <option value="<?php echo esc_attr($row['option']); ?>"><?php echo esc_html($row['option']); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <?php render_country('enquiry', $form_fields); ?>
            
            <?php render_how_heard('enquiry', $form_fields); ?>
         
            <div class="col-12 field-wrap">
                <label for="enquiry-message" class="tiny-caps sans mar-b-5"><?php echo esc_html($form_fields['message_label']); ?></label>
                <textarea id="enquiry-message" name="message" class="required"></textarea>
            </div>
        </div>

        <?php render_form_bottom('enquiry', $form_fields); ?>
        
    </form>
<?php }


function render_brochure_form($type, $form_fields, $css = '') {
    global $post; ?>
    <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="validate-form <?php echo $css; ?>" data-reset="1">
        <input type="hidden" name="action" value="process_sf_form">
        <input type="hidden" name="form_type" value="brochure_<?php echo $type; ?>">
        <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">

        <?php if ($type == 'digital') : ?>
            <input type="hidden" name="pdf_download" value="1">
            <input type="hidden" name="paperless" value="1">
        <?php elseif ($type == 'print') : ?>
            <input type="hidden" name="hard_copy" value="1">
        <?php endif; ?>
        
        <div class="med-title reg"><?php echo esc_html($form_fields['title_'. $type]); ?></div>

        <div class="grid gap-30 mar-b-30">
            <?php render_text_input('first_name', 'brochure-'. $type, $form_fields, 'required'); ?>
            <?php render_text_input('last_name', 'brochure-'. $type, $form_fields, 'required'); ?>
            <?php render_text_input('email', 'brochure-'. $type, $form_fields, 'required email'); ?>
            <?php render_text_input('phone', 'brochure-'. $type, $form_fields, 'required'); ?>
            <?php render_text_input('company', 'brochure-'. $type, $form_fields, ''); ?>

            <?php if ($type == 'print') {
                render_text_input('street_address', 'brochure-'. $type, $form_fields, 'required');
                render_text_input('city', 'brochure-'. $type, $form_fields, 'required');
                render_text_input('postal_code', 'brochure-'. $type, $form_fields, 'required');
            }

            render_country('brochure-'. $type, $form_fields); ?>

            <div class="col-12 col-6-xs field-wrap">
                <label for="brochure-<?php echo $type; ?>-type" class="tiny-caps sans mar-b-5"><?php echo esc_html($form_fields['brochure_type_label']); ?></label>
                <select id="brochure-<?php echo $type; ?>-type" name="brochure_type" class="brochure-type required">
                    <option value=""><?php echo __('Please select:', 'theme'); ?></option>
                    <option value="classic"><?php echo __('Classic', 'theme'); ?></option>
                    <option value="commercial"><?php echo __('Commercial', 'theme'); ?></option>
                </select>
            </div>

            <?php render_how_heard('brochure-'. $type, $form_fields); ?>
        </div>

        <?php render_form_bottom('brochure-'. $type, $form_fields); ?>
        
    </form>
<?php }


function render_brochure_modal() {
    global $post;

    $form_fields = get_field('brochure_form', 'option'); ?>
    <div id="brochure-modal" class="modal-wrap bg-off-black-50 fxd-box fade invis">
        <div class="modal bg-cream rel over-auto">
            <button type="button" class="close-modal cross" aria-label="Close"></button>
            
            <?php render_brochure_form('digital', $form_fields, 'mar-b-60');
            render_brochure_form('print', $form_fields); ?>
        </div>
    </div>
<?php }


function render_enquiry_modal() { ?>
    <div id="enquiry-modal" class="modal-wrap bg-off-black-50 fxd-box fade invis">
        <div class="modal bg-cream rel over-auto">
            <button type="button" class="close-modal cross" aria-label="Close"></button>
            <?php render_enquiry_form('modal'); ?>
        </div>
    </div>
<?php }


/******
Forms
******/

function verify_recaptcha() {
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $secret = '6Ld6hvgpAAAAAGsVMrZQg4oh1snzyma4WjjkOAKR';
    $token = $_POST['g-recaptcha-response'];

    $result = json_decode(file_get_contents($url . '?secret=' . $secret . '&response=' . $token));

    return $result;
}


function process_sf_form() {
    // Prepare JSON response
    $response = [
        'status' => 200,
        'errors' => []
    ];

    // Get form type
    $form_type = $_POST['form_type'];

    // Verify Grecaptcha
    $result = $form_type == 'newsletter' ? false : verify_recaptcha();

    // Handle Grecaptcha error
    if ($result && !$result->success) {
        $response['status'] = 500;
    }
    else {
        // Get form type
        $form_type = $_POST['form_type'];

        // Maybe redirect to PDF
        if ($form_type == 'brochure_digital') {
            $response['redirect'] = get_field($_POST['brochure_type'] .'_brochure_brochure', 'option');
        }

        // Send to SalesForce
        require_once(get_template_directory() .'/apis/salesforce.php');
        $salesforce->process_lead($_POST);
    }

    // Send JSON response
    wp_send_json($response);
}
add_action('wp_ajax_process_sf_form', 'process_sf_form');
add_action('wp_ajax_nopriv_process_sf_form', 'process_sf_form');


/******
Cookies
******/

function set_single_cookie() {
    // Prepare JSON response
    $response = [
        'status' => 200
    ];

    // Get $_POST data
    $name = sanitize_text_field($_POST['name']);
    $value = sanitize_text_field($_POST['value']);

    // Prepare options
    $options = [
        'path' => '/',
        'secure' => true,
        'samesite' => 'Lax'
    ];

    // Optionally, add expiry
    if (isset($_POST['expiry'])) {
        $options['expires'] = time() + intval($_POST['expiry']);
    }

    // Set cookie
    setcookie($name, $value, $options);

    // Send JSON response
    wp_send_json($response);
}
add_action('wp_ajax_set_single_cookie', 'set_single_cookie');
add_action('wp_ajax_nopriv_set_single_cookie', 'set_single_cookie');


/******
Globals
******/

$countries = [
    'AF' => "Afghanistan",
    'AX' => "Åland Islands",
    'AL' => "Albania",
    'DZ' => "Algeria",
    'AS' => "American Samoa",
    'AD' => "Andorra",
    'AO' => "Angola",
    'AI' => "Anguilla",
    'AQ' => "Antarctica",
    'AG' => "Antigua and Barbuda",
    'AR' => "Argentina",
    'AM' => "Armenia",
    'AW' => "Aruba",
    'AU' => "Australia",
    'AT' => "Austria",
    'AZ' => "Azerbaijan",
    'BS' => "Bahamas",
    'BH' => "Bahrain",
    'BD' => "Bangladesh",
    'BB' => "Barbados",
    'BE' => "Belgium",
    'BZ' => "Belize",
    'BJ' => "Benin",
    'BY' => "Belarus",
    'BM' => "Bermuda",
    'BT' => "Bhutan",
    'BO' => "Bolivia",
    'BQ' => "Bonaire, Sint Eustatius and Saba",
    'BA' => "Bosnia and Herzegovina",
    'BW' => "Botswana",
    'BV' => "Bouvet Island",
    'BR' => "Brazil",
    'IO' => "British Indian Ocean Territory",
    'BN' => "Brunei Darussalam",
    'BG' => "Bulgaria",
    'BF' => "Burkina Faso",
    'BI' => "Burundi",
    'KH' => "Cambodia",
    'CM' => "Cameroon",
    'CA' => "Canada",
    'CV' => "Cape Verde",
    'CF' => "Central African Republic",
    'TD' => "Chad",
    'CL' => "Chile",
    'CN' => "China",
    'CX' => "Christmas Island",
    'CC' => "Cocos (Keeling) Islands",
    'CO' => "Colombia",
    'KM' => "Comoros",
    'CG' => "Congo",
    'CD' => "The Democratic Republic of The Congo",
    'CK' => "Cook Islands",
    'CR' => "Costa Rica",
    'CI' => "Cote D'ivoire",
    'CW' => "Curaçao",
    'HR' => "Croatia",
    'CY' => "Cyprus",
    'CZ' => "Czech Republic",
    'DK' => "Denmark",
    'DJ' => "Djibouti",
    'DM' => "Dominica",
    'DO' => "Dominican Republic",
    'EC' => "Ecuador",
    'EG' => "Egypt",
    'SV' => "El Salvador",
    'GQ' => "Equatorial Guinea",
    'ER' => "Eritrea",
    'EE' => "Estonia",
    'ET' => "Ethiopia",
    'FK' => "Falkland Islands (Malvinas)",
    'FO' => "Faroe Islands",
    'FJ' => "Fiji",
    'FI' => "Finland",
    'FR' => "France",
    'GF' => "French Guiana",
    'PF' => "French Polynesia",
    'TF' => "French Southern Territories",
    'GA' => "Gabon",
    'GM' => "Gambia",
    'GE' => "Georgia",
    'DE' => "Germany",
    'GH' => "Ghana",
    'GI' => "Gibraltar",
    'GR' => "Greece",
    'GL' => "Greenland",
    'GD' => "Grenada",
    'GP' => "Guadeloupe",
    'GU' => "Guam",
    'GT' => "Guatemala",
    'GG' => "Guernsey",
    'GN' => "Guinea",
    'GW' => "Guinea-bissau",
    'GY' => "Guyana",
    'HT' => "Haiti",
    'HM' => "Heard Island and Mcdonald Islands",
    'VA' => "Holy See (Vatican City State)",
    'HN' => "Honduras",
    'HK' => "Hong Kong",
    'HU' => "Hungary",
    'IS' => "Iceland",
    'IN' => "India",
    'ID' => "Indonesia",
    'IQ' => "Iraq",
    'IE' => "Ireland",
    'IM' => "Isle of Man",
    'IL' => "Israel",
    'IT' => "Italy",
    'JM' => "Jamaica",
    'JP' => "Japan",
    'JE' => "Jersey",
    'JO' => "Jordan",
    'KZ' => "Kazakhstan",
    'KE' => "Kenya",
    'KI' => "Kiribati",
    'KR' => "Republic of Korea",
    'KW' => "Kuwait",
    'KG' => "Kyrgyzstan",
    'LA' => "Lao People's Democratic Republic",
    'LV' => "Latvia",
    'LB' => "Lebanon",
    'LS' => "Lesotho",
    'LR' => "Liberia",
    'LY' => "Libya",
    'LI' => "Liechtenstein",
    'LT' => "Lithuania",
    'LU' => "Luxembourg",
    'MO' => "Macao",
    'MK' => "Macedonia",
    'MG' => "Madagascar",
    'MW' => "Malawi",
    'MY' => "Malaysia",
    'MV' => "Maldives",
    'ML' => "Mali",
    'MT' => "Malta",
    'MH' => "Marshall Islands",
    'MQ' => "Martinique",
    'MR' => "Mauritania",
    'MU' => "Mauritius",
    'YT' => "Mayotte",
    'MX' => "Mexico",
    'FM' => "Micronesia",
    'MD' => "Moldova",
    'MC' => "Monaco",
    'MN' => "Mongolia",
    'ME' => "Montenegro",
    'MS' => "Montserrat",
    'MA' => "Morocco",
    'MZ' => "Mozambique",
    'MM' => "Myanmar",
    'NA' => "Namibia",
    'NR' => "Nauru",
    'NP' => "Nepal",
    'NL' => "Netherlands",
    'NC' => "New Caledonia",
    'NZ' => "New Zealand",
    'NI' => "Nicaragua",
    'NE' => "Niger",
    'NG' => "Nigeria",
    'NU' => "Niue",
    'NF' => "Norfolk Island",
    'MP' => "Northern Mariana Islands",
    'NO' => "Norway",
    'OM' => "Oman",
    'PK' => "Pakistan",
    'PW' => "Palau",
    'PS' => "Palestinia",
    'PA' => "Panama",
    'PG' => "Papua New Guinea",
    'PY' => "Paraguay",
    'PE' => "Peru",
    'PH' => "Philippines",
    'PN' => "Pitcairn",
    'PL' => "Poland",
    'PT' => "Portugal",
    'PR' => "Puerto Rico",
    'QA' => "Qatar",
    'RE' => "Reunion",
    'RO' => "Romania",
    'RU' => "The Russian Federation",
    'RW' => "Rwanda",
    'BL' => "Saint Barthélemy",
    'SH' => "Saint Helena",
    'KN' => "Saint Kitts and Nevis",
    'LC' => "Saint Lucia",
    'MF' => "Saint Martin",
    'PM' => "Saint Pierre and Miquelon",
    'VC' => "Saint Vincent and The Grenadines",
    'WS' => "Samoa",
    'SM' => "San Marino",
    'ST' => "Sao Tome and Principe",
    'SA' => "Saudi Arabia",
    'SN' => "Senegal",
    'RS' => "Serbia",
    'SC' => "Seychelles",
    'SL' => "Sierra Leone",
    'SG' => "Singapore",
    'SX' => "Sint Maarten",
    'SK' => "Slovakia",
    'SI' => "Slovenia",
    'SB' => "Solomon Islands",
    'SO' => "Somalia",
    'ZA' => "South Africa",
    'SS' => "South Sudan",
    'GS' => "South Georgia and The South Sandwich Islands",
    'ES' => "Spain",
    'LK' => "Sri Lanka",
    'SD' => "Sudan",
    'SR' => "Suriname",
    'SJ' => "Svalbard and Jan Mayen",
    'SZ' => "Swaziland",
    'SE' => "Sweden",
    'CH' => "Switzerland",
    'TW' => "Taiwan, Province of China",
    'TJ' => "Tajikistan",
    'TZ' => "Tanzania, United Republic of",
    'TH' => "Thailand",
    'TL' => "Timor-leste",
    'TG' => "Togo",
    'TK' => "Tokelau",
    'TO' => "Tonga",
    'TT' => "Trinidad and Tobago",
    'TN' => "Tunisia",
    'TR' => "Turkey",
    'TM' => "Turkmenistan",
    'TC' => "Turks and Caicos Islands",
    'TV' => "Tuvalu",
    'UG' => "Uganda",
    'AE' => "United Arab Emirates",
    'GB' => "United Kingdom",
    'US' => "United States",
    'UM' => "United States Minor Outlying Islands",
    'UY' => "Uruguay",
    'UZ' => "Uzbekistan",
    'VU' => "Vanuatu",
    'VE' => "Venezuela",
    'VN' => "Vietnam",
    'VG' => "Virgin Islands, British",
    'VI' => "Virgin Islands, U.S.",
    'WF' => "Wallis and Futuna",
    'EH' => "Western Sahara",
    'YE' => "Yemen",
    'ZM' => "Zambia",
    'ZW' => "Zimbabwe"
];

