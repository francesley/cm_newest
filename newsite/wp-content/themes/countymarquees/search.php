<?php
get_header();
?>

<div id="content" class="bd inpage">
    <section class="search-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="filter-search">
                        <div class="row">
                            <div class="offset-md-1 col-md-8">
                                <form method="get" id="search-form" enctype="multipart/form-data">
                                    <input name="s" type="text" id="s" class="search-text"
                                        placeholder="I’m looking for..."
                                        value="<?php if (isset($_GET['s'])) {
                                            echo $_GET['s'];
                                        } ?>">
                                    <button type="submit" class="sbtn-search "><svg xmlns="http://www.w3.org/2000/svg"
                                            width="16" height="16" fill="currentColor" class="bi bi-search"
                                            viewBox="0 0 16 16">
                                            <path
                                                d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0">
                                            </path>
                                        </svg></button>
                                    <div id="searchboxclose" class=""></div>
                                </form>
                                <h2> Search Results for
                                    <?php if (isset($_GET['s'])) {
                                        echo $_GET['s'];
                                    } ?>
                                </h2>
                            </div>
                        </div>

                        <?php
                        $searchTerm = $_GET['s'];
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        global $wpdb;
                        $querystr = "SELECT wp_posts.ID
    FROM wp_posts
    LEFT JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id)
    WHERE 1=1
        AND (((wp_posts.post_title LIKE '%$searchTerm%')
            OR (wp_posts.post_content LIKE '%$searchTerm%')
            OR (wp_postmeta.meta_value LIKE '%$searchTerm%')))
        AND wp_posts.post_type IN ('page', 'post', 'news')
        AND ((wp_posts.post_status = 'publish'))
    GROUP BY wp_posts.ID
    ORDER BY wp_posts.post_date";


                        $query3 = $wpdb->get_results($querystr, OBJECT);
                        $post_ids3 = wp_list_pluck($query3, 'ID');
                        $post_array = array();
                        if ($query3) {
                            foreach ($query3 as $post) {
                                $post = get_post($post->ID);
                                $post_array[] = $post;
                            }
                        }

                        $final = new WP_Query();
                        $final->posts = $post_array;
                        $final->post_count = count($final->posts);

                        ?>
                        <div class="search-list">
                            <?php
                            $count = 1;


                            if ($final->have_posts()) {
                                foreach ($final->posts as $post) {
                                    ?>
                                    <div class="row search-item">
                                        <?php
                                        $location = "";
                                        $target = "";
                                        $permalink = get_the_permalink($post->ID); ?>
                                        <div class="offset-md-1 col-md-8">
                                            <div class="post-content">
                                                <a href="<?php echo $permalink; ?>">
                                                    <h4><?php echo get_the_title($post->ID); ?></h4>
                                                </a>
                                                <div>
                                                    <?php
                                                    // Excerpt (short description fo the pages in the page admin ) needs to be added for the pages which will appear here
                                                    echo get_the_excerpt($post->ID); ?>
                                                </div>
                                                <a href="<?php echo $permalink; ?>" class="read-more"><?php echo $permalink; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                    <?php
                                    $count++;
                                }
                            } else {
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p> No posts matching your search </p>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php wp_reset_query(); ?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
</div>
</section>
<?php get_footer(); ?>