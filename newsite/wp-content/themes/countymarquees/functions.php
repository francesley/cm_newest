<?php show_admin_bar( false );

function wpse_manual_redirect( ){
 
  $not_founds = ["equipment","events","company"];
  //$str = 'article.html';


  $url = parse_url( add_query_arg( array() ), PHP_URL_PATH );
  $last_segment = "";
  foreach ($not_founds as $not_found) {
    //ShowDev($not_found,"<br/><br/><br/><br/><br/>");
    $last_segment = strstr($url , $not_found . ".htm");
    
    //ShowTrue($last_segment);
    if ($last_segment) {
      header("HTTP/1.0 404 Not Found"); //TODO check if 404
      $load = locate_template( '404.php', true );
      if ( $load ){
          exit();
      }
    }
  }
}

add_action( 'init', 'wpse_manual_redirect' );


/*
  =============================================================
  Index
  =============================================================

  1.0 - Theme Support

  2.0 - Enqueue Scripts and Styles

  3.0 - Register Menus

  4.0 - Register Widgets Areas


  -------------------------------------------------------------
  Index Ends
  -------------------------------------------------------------
 */
/*
  =============================================================
  1.0 - Theme Support
  =============================================================
 */
//ini_set( "display_errors", 1 );
add_action('after_setup_theme', 'theme_setup');

function theme_setup() {
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
    add_theme_support('custom-header');
    add_image_size('thumb-300x200', 300, 200, true);
    add_image_size( 'gallery-size-large', 1200, 800, true);
    add_image_size( 'gallery-size-medium',  600, 400 ,true);
    add_image_size( 'gallery-size-small', 440, 440 ,true);    
    add_image_size( 'gallery-category',  450, 300);

  
    // add_image_size( 'gallery-size-large', 620, 415 ,true);
    // add_image_size( 'gallery-size-medium', 450 , 300 ,true);

    add_image_size('sm', 720, 0);
add_image_size('md', 960, 0);
add_image_size('lg', 1536, 0);
add_image_size('xl', 2048, 0);

}

/******
Images
******/

function set_jpeg_compression() {
  return 70;
}
add_filter('jpeg_quality', 'set_jpeg_compression');


function allow_svgs($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'allow_svgs');


function limit_large_images() {
  return 2048;
}
add_filter('big_image_size_threshold', 'limit_large_images');


function disable_wp_image_sizes($sizes) {
  unset($sizes['thumbnail']);
  unset($sizes['medium_large']);
  unset($sizes['large']);
  unset($sizes['1536x1536']);
  unset($sizes['2048x2048']);
  return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'disable_wp_image_sizes');




/*
  -------------------------------------------------------------
  Theme Support Ends
  -------------------------------------------------------------
 */

/*
 * *****************************************************************************
 *  Custom Logo
 * *****************************************************************************
 *  @ Custom logo for theme.
 *      - new_theme_the_custom_logo: uses the_custom_logo
 * *****************************************************************************
 */

function new_theme_the_custom_logo() {

    if (function_exists('the_custom_logo')) {
        the_custom_logo();
    }
}

/*
  =============================================================
  2.0 - Enqueue Scripts and Styles
  =============================================================
 */

function theme_enqueue_scripts() {

    global $wp_query;

    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css','', '1.0.0');
    wp_enqueue_style('bootstrap_overwrites-css', get_template_directory_uri() . '/css/bootstrap_overwrites.css','', '1.0.0');

    wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css', '', '1.0.0' );

    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css', '', '1.0.0' );
	
	  wp_enqueue_style('cm-style', get_template_directory_uri() . '/css/cm.css', '', '1.0.0');     
	  wp_enqueue_style('bslayout-css', get_template_directory_uri() . '/css/bootstrap_layout.css', '', '1.0.0');     
	  wp_enqueue_style('table-css', get_template_directory_uri() . '/css/table.css', '', '1.0.0');     
	  wp_enqueue_style('headers-css', get_template_directory_uri() . '/css/headers.css', '', '1.0.0');     
	  wp_enqueue_style('sites-css', get_template_directory_uri() . '/css/sites.css', '', '1.0.0');
    wp_enqueue_style('venues-css', get_template_directory_uri() . '/css/venues.css', '', '1.0.0');    
    wp_enqueue_style('text-css', get_template_directory_uri() . '/css/text.css', '', '1.0.0');    
    wp_enqueue_style('areas-css', get_template_directory_uri() . '/css/areas.css', '', '1.0.0');    
    wp_enqueue_style('text-css', get_template_directory_uri() . '/css/text.css', '', '1.0.0');    
    wp_enqueue_style('sections-css', get_template_directory_uri() . '/css/sections.css', '', '1.0.0');    
    wp_enqueue_style('nav-css', get_template_directory_uri() . '/css/nav.css', '', '1.0.0');    

    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js', array(), '1.0.0', true); //TODO get rid of this!!!

    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), '1.0.0', true );
	
    wp_register_script("theme-script", get_template_directory_uri() . '/js/theme-script.js', array('jquery'), '1.0.0', true );

    // Localize script with AJAX URL and other parameters

    // wp_localize_script('theme-script', 'loadmore_params', array(
    //   'ajaxurl' => admin_url( 'admin-ajax.php' ), 
    //   'posts' => json_encode( $wp_query->query_vars ),
    //   'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
    //   'max_page' => $wp_query->max_num_pages
    //   ) );

    wp_enqueue_script('theme-script');
}
    
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');


function remove_jquery_migrate( $scripts ) {
  if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
       $script = $scripts->registered['jquery'];
    if ( $script->deps ) { 
// Check whether the script has any dependencies

       $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
    }
  }
}
add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

/*
  -------------------------------------------------------------
  Enqueue Scripts and Styles Ends
  -------------------------------------------------------------
 */

/*
  =============================================================
  3.0 - Register Menus
  =============================================================
 */

function register_menus() {
    register_nav_menus(
		array(
			'main-menu' => __('Main Menu'),
			'footer-area' => __('Footer Areas'),
			'useful-links' => __('Footer Useful Links'),
		)
    );
}

add_action('init', 'register_menus');

//if ( !is_admin() )add_action( "wp_enqueue_scripts", "my_jquery_enqueue", 11 );

function my_jquery_enqueue() {
  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.4.1.min.js", false, null );
  wp_enqueue_script( 'jquery' );
}

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page( array(
      'page_title' => 'Theme General Settings',
      'menu_title' => 'Theme Settings',
      'menu_slug' => 'theme-general-settings',
      'capability' => 'edit_posts',
      'redirect' => false
    ) );
  
    acf_add_options_sub_page( array(
      'page_title' => 'Theme Header Settings',
      'menu_title' => 'Header',
      'parent_slug' => 'theme-general-settings',
    ) );
  
    acf_add_options_sub_page( array(
      'page_title' => 'Theme Footer Settings',
      'menu_title' => 'Footer',
      'parent_slug' => 'theme-general-settings',
    ) );
  
  }


  class Bootstrap_NavWalker extends Walker_Nav_Menu {
    public function start_lvl( & $output, $depth = 0, $args = array() ) {
      $output .= '<ul class="dropdown-menu">';
    }
  
    public function start_el( & $output, $item, $depth = 0, $args = array(), $id = 0 ) {
      $output .= '<li class="nav-item';
    
      // Check for custom classes and add them
      if ( !empty($item->classes) ) {
        $output .= ' ' . implode(' ', $item->classes);
      }
    
      if ( in_array( 'menu-item-has-children', $item->classes ) ) {
        $output .= ' dropdown';
      }
      if ( $item->current || in_array( 'current_page_parent', $item->classes ) || in_array( 'current-post-ancestor', $item->classes ) || $item->current_item_ancestor ) {
        $output .= ' active';
      }
    
      $output .= '">';

      if ( in_array( 'menu-item-has-children', $item->classes ) ) {
        $output .= '<a class="nav-link dropdown-toggle" href="' . $item->url . '" id="navbarDropdown' . $item->ID . '" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
      } else {
        if ( $depth > 0 ) {
          $output .= '<a class="nav-link-sub" href="' . $item->url . '">';
        } else {
          $output .= '<a class="nav-link" href="' . $item->url . '">';
        }
      }
     
      $output .= $item->title . '</a>';
      
      //print_r($item);
    }
  
    
  }
 

/*
  -------------------------------------------------------------
  Register Menus Ends
  -------------------------------------------------------------
 */

/*
  =============================================================
  4.0 - Register Widget Areas
  =============================================================
 */

function custom_widgets() {
    register_sidebar(array(
        'id' => 'footer-widget-area',
        'name' => 'Footer Widget Area',
        'description' => 'The widget area in the footer',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    
}

add_action('widgets_init', 'custom_widgets');


/*
  -------------------------------------------------------------
  Register Widget Areas Ends
  -------------------------------------------------------------
 */
 

 add_filter('use_block_editor_for_post', '__return_false', 5);


remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
remove_action('wp_head', 'wlwmanifest_link'); //removes wlwmanifest (Windows Live Writer) link.
remove_action('wp_head', 'wp_generator'); //removes meta name generator.
remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.
remove_action( 'wp_head', 'feed_links', 2 ); //removes feed links.
remove_action('wp_head', 'feed_links_extra', 3 );  //removes comments feed. 
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
add_filter( 'emoji_svg_url', '__return_false' );// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function remove_wp_block_library_css() {
  wp_dequeue_style('wp-block-library');
  wp_dequeue_style('wp-block-library-theme');
}
add_action('wp_enqueue_scripts', 'remove_wp_block_library_css');

add_action( 'wp_enqueue_scripts', 'remove_global_styles' );
function remove_global_styles(){
    wp_dequeue_style( 'global-styles' );
}

function wpplugins_remove_recentcomments() {
  global $wp_widget_factory;
  remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}

add_action('widgets_init', 'wpplugins_remove_recentcomments');

// Disable REST API link tag
remove_action('wp_head', 'rest_output_link_wp_head', 10);

// Disable oEmbed Discovery Links
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

// Disable REST API link in HTTP headers
remove_action('template_redirect', 'rest_output_link_header', 11, 0);




/*  -------------------------------------------------------------  
	WP_Customize Custom Functions  
	------------------------------------------------------------- */ 

/* Maps */	
//require_once( __DIR__ . '/includes/maps/GetVenues.php');

/* Price List */	

//require_once( __DIR__ . '/includes/pricing/GetPricing.php');


// Register Taxonomy
function register_custom_taxonomy() {
      $labels = array(
          'name' => 'Primary Area',
          'singular_name' => 'Primary Area',
          'menu_name' => 'Primary Area',
      );

      $args = array(
          'hierarchical' => true,
          'labels' => $labels,
          'public' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'show_admin_column' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'primary_area'),
      );

      register_taxonomy('primary_area', 'page', $args);
  }
  add_action('init', 'register_custom_taxonomy');



  function hide_taxonomy_metabox_for_specific_pages() {
      // Check if we are editing a page
      if (isset($_GET['post'])) {
          $current_page_id = $_GET['post'];
          // Check if the current page is a child of the "Areas" page
          $parent_page_slug = 'areas'; // Replace with the slug of the "Areas" page
          $parent_page = get_page_by_path($parent_page_slug);

          if ($parent_page && $parent_page->ID === wp_get_post_parent_id($current_page_id)) {
              // Replace 'your_taxonomy_name' with the actual taxonomy name
              
          }else{
              remove_meta_box('primary_areadiv', 'page', 'side');
          }
      }

  }
  add_action('admin_menu', 'hide_taxonomy_metabox_for_specific_pages');



 // Ajax functions for the gallery load more

  //function cw_loadmoregallery_handler() {
//     $page = $_POST[ 'page' ] + 1;  
//     $posttype = $_POST[ 'posttype' ];

//     if($posttype == 'photos'){
//         $postcount = 8;
//         $order = 'ASC';
//         $orderby = 'menu_order';
//     }else{
//       $order = 'ASC';
//       $postcount = 9;
//       $orderby = 'menu_order';
//     }
    
//     $args = array(
//       'post_type' => $posttype,
//       'post_status' => 'publish',
//       'posts_per_page' => $postcount,
//       'order' => $order,
//       'orderby' => $orderby, // Use 'menu_order' for custom post types
//       'paged' => $page,
//     );
//     $res = new WP_Query( $args );
//     $output = '';
  
//     $the_query = new WP_Query( $args );
//     if ( $the_query->have_posts() ) {
//       while ( $the_query->have_posts() ): $the_query->the_post();
//       $image = get_the_post_thumbnail_url(get_the_ID(),'gallery-size-large');
//       if ( !$image ) {
//         $image = "https://via.placeholder.com/290x193.png";
//       }
//       $title = get_the_title();
//       $link = get_the_permalink();
//       $excerpt = get_the_excerpt();
//       if($posttype == 'photos'){
//       $output .= '<div class="col-lg-3 col-md-4 col-sm-6 col-6"><div class="post-item"><a href="' . $link . '"><div class="post-item-img"><img class="image" src="' . $image . '" /> </div>
//         </a><a href="' . $link . '"><div class="post-text">' . $excerpt . '</div></a></div></div>';
//       }else{
//         $output .= '<div class="col-lg-4 col-md-4 col-sm-6 col-6"><div class="post-item"><a href="' . $link . '"><div class="post-item-img"><img class="image" src="' . $image . '" /> </div>
//         </a><a href="' . $link . '"><div class="post-text">' . $title . '</div></a></div></div>';
//       }
//       endwhile;
//   } else {
//     $output .= 'noposts';
//   }  
  
//   $op = array();
//   $op[ 'code' ] = $output;
//   $op[ 'maxpages' ] = $res->max_num_pages;  
//   echo json_encode( $op );
//   die;
// }


//add_action( 'wp_ajax_loadmoregallery', 'cw_loadmoregallery_handler' ); 
//add_action( 'wp_ajax_nopriv_loadmoregallery', 'cw_loadmoregallery_handler' );
// adding .htm suffix to gallery post url

// function set_photos_permalink_structure($post_link, $post) {
//   if ($post->post_type == 'photos'|| $post->post_type == 'case_studies') {
//       $post_link = trailingslashit(get_bloginfo('url')) . $post->post_type . '/' . $post->post_name . '.htm';
//   }
//   return $post_link;
// }

// add_filter('post_type_link', 'set_photos_permalink_structure', 10, 2);

function flush_photos_rewrite_rules() {
  flush_rewrite_rules();
}
add_action('init', 'flush_photos_rewrite_rules');



// Adds rewrite rules for the photo page links to work and not go to 404 
function register_customposts_rewrite_rule() {
  add_rewrite_rule(
      '^photos/([^/]*).htm',
      'index.php?photos=$matches[1]',
      'top'
  );

  add_rewrite_rule(
    '^case_studies/([^/]*).htm',
    'index.php?case_studies=$matches[1]',
    'top'
);
}

add_action('init', 'register_customposts_rewrite_rule', 10, 0);





function add_canonical_tag() {
  if (is_singular('photos')) { // Check if it's a single "photos" post
      $canonical_url = get_permalink(); // Get the permalink of the current post
      echo '<link rel="canonical" href="' . esc_url($canonical_url) . '" />' . "\n";
  }
}

add_action('wp_head', 'add_canonical_tag');



//for case studies
function getPageByTitle( $page_title, $output = OBJECT, $post_type = 'page' ){

  global $wpdb;

  if ( is_array( $post_type ) ) {
      $post_type           = esc_sql( $post_type );
      $post_type_in_string = "'" . implode( "','", $post_type ) . "'";
      $sql                 = $wpdb->prepare(
          "
          SELECT ID
          FROM $wpdb->posts
          WHERE post_title = %s
          AND post_type IN ($post_type_in_string)
      ",
          $page_title
      );
  } else {
      $sql = $wpdb->prepare(
          "
          SELECT ID
          FROM $wpdb->posts
          WHERE post_title = %s
          AND post_type = %s
      ",
          $page_title,
          $post_type
      );
  }
}



/*  -------------------------------------------------------------  
	Old Utility Functions  
	------------------------------------------------------------- */ 

function ShowDev ($thing = "", $title = "DEBUG") {

  echo "<br/>" . strtoupper($title) . ": " . $thing . "<br/>";

}

function ShowTrue ($thing, $name = "TRUE OR FALSE") {

  $result = $thing?"true":"false";

  echo "<br/>" . strtoupper($name) . ": " . $result . "<br/>";

}





/* $categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");
		$category = 0;
    $term=0;

        if (isset($_GET['category'])) {
            $category = in_array($_GET['category'],$categories)?$_GET['category']:0;
        }

// This function gets the Category Title for the filter category pages
if ($category) {
ShowDev($category,"l.478");
    add_filter('wpseo_title', function($title){
    //$category = ($_GET['category']) ? $_GET['category'] : 1;
    //if ($category != 1) {
        $term_taxonomy = 'gallery-category';
        // Check if the term exists in the specified taxonomy
        //ShowDev($category,"category this is the one");
        $term = get_term_by('slug', $category, $term_taxonomy);
        ShowDev($term,"term");
        if ($term) {
            // Get the Yoast SEO data from the options table
            $wpseo_taxonomy_meta = get_option('wpseo_taxonomy_meta', array());          
            // Check if the term has Yoast SEO data
            if (isset($wpseo_taxonomy_meta[$term_taxonomy][$term->term_id])) {
                $term_data = $wpseo_taxonomy_meta[$term_taxonomy][$term->term_id];              
                // Output the Yoast SEO title 
                $seo_title =  $term_data['wpseo_title'];
                // echo '<meta name="description" content="' . esc_attr($term_data['wpseo_desc']) . '" />';
            } else {
              $seo_title =  get_the_title(); 				
            }
        } else {
          $seo_title =  get_the_title(); 			
        }
        $title = $seo_title;		
   // }  
    
  });

  // This function gets the Category Description for the filter category pages 
    add_filter('wpseo_metadesc', function($description){

      //$category = ($_GET['category']) ? $_GET['category'] : 1;
      //if ($category != 1) {
          $term_taxonomy = 'gallery-category';
          // Check if the term exists in the specified taxonomy
          $term = get_term_by('slug', $category, $term_taxonomy);
          if ($term) {
              // Get the Yoast SEO data from the options table
              $wpseo_taxonomy_meta = get_option('wpseo_taxonomy_meta', array());          
              // Check if the term has Yoast SEO data
              if (isset($wpseo_taxonomy_meta[$term_taxonomy][$term->term_id])) {
                  $term_data = $wpseo_taxonomy_meta[$term_taxonomy][$term->term_id];              
                  // Output the Yoast description             
                  $description = $term_data['wpseo_desc'];
              } else {
                $description =  ""; 			
              }
          } else {
            $description =  ""; 		
          }      
     // }
      

      
    });



}  else {
  $title =  get_the_title();
  $description =  "";
}
return $title;
return $description; */

// This function gets the Category Description for the filter category pages 
add_filter('wpseo_metadesc', function($description){
//ShowDev("filtering");
  $categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");
		$category = 1;
    $term=0;

    if (isset($_GET['category'])) {
      $category = in_array($_GET['category'],$categories)?$_GET['category']:1;
  }


  //$category = ($_GET['category']) ? $_GET['category'] : 1;
  if ($category != 1) {
      $term_taxonomy = 'gallery-category';
      // Check if the term exists in the specified taxonomy
      $term = get_term_by('slug', $category, $term_taxonomy);
      if ($term) {
          // Get the Yoast SEO data from the options table
          $wpseo_taxonomy_meta = get_option('wpseo_taxonomy_meta', array());          
          // Check if the term has Yoast SEO data
          if (isset($wpseo_taxonomy_meta[$term_taxonomy][$term->term_id])) {
              $term_data = $wpseo_taxonomy_meta[$term_taxonomy][$term->term_id];              
              // Output the Yoast description             
              $description = $term_data['wpseo_desc'];
          } else {
            $description =  ""; 			
          }
      } else {
        $description =  ""; 		
      }      
  }
  else{
    $description =  ""; 								
  }

  return $description;
});


// This function gets the Category Title for the filter category pages 
add_filter('wpseo_title', function($title){
 
  $categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");
  $category = 1;
  $term=0;

  if (isset($_GET['category'])) {
    $category = in_array($_GET['category'],$categories)?$_GET['category']:1;
}

  if ($category != 1) {
      $term_taxonomy = 'gallery-category';
      // Check if the term exists in the specified taxonomy
      $term = get_term_by('slug', $category, $term_taxonomy);
      if ($term) {
          // Get the Yoast SEO data from the options table
          $wpseo_taxonomy_meta = get_option('wpseo_taxonomy_meta', array());          
          // Check if the term has Yoast SEO data
          if (isset($wpseo_taxonomy_meta[$term_taxonomy][$term->term_id])) {
              $term_data = $wpseo_taxonomy_meta[$term_taxonomy][$term->term_id];              
              // Output the Yoast SEO title 
              $seo_title =  $term_data['wpseo_title'];
             // echo '<meta name="description" content="' . esc_attr($term_data['wpseo_desc']) . '" />';
          } else {
            $seo_title =  get_the_title(); 				
          }
      } else {
        $seo_title =  get_the_title(); 			
      }
      $title = $seo_title;		
  }
  else{
    $title =  get_the_title(); 								
  }
  return $title;
});


// Add Excerpt field in the page admin
function force_page_excerpt() {
  add_post_type_support('page', 'excerpt');
}
add_action('init', 'force_page_excerpt');


/*
 * Snippet: How to Regenerate Thumbnails Without a Plugin in WordPress – 2025
 * Author: John Cook 
 * URL: https://wcsuccessacademy.com/?p=1363
 * Tested with WooCommerce 9.3.3
 * "Function to regenerate thumbnails"
 */
function wcsuccess_regenerate_thumbnails() {
  // Get all attachments
  $attachments = get_posts(array(
      'post_type' => 'attachment',
      'numberposts' => -1,
  ));

  // Process each attachment
  foreach ($attachments as $attachment) {
      $image_path = get_attached_file($attachment->ID);

      // Check if the file exists
      if (file_exists($image_path)) {
          // Regenerate thumbnails
          wp_update_attachment_metadata($attachment->ID, wp_generate_attachment_metadata($attachment->ID, $image_path));
      }
  }
}

/*
 * Snippet: How to Regenerate Thumbnails Without a Plugin in WordPress – 2025
 * Author: John Cook 
 * URL: https://wcsuccessacademy.com/?p=1363
 * Tested with WooCommerce 9.3.3
 * "Create custom admin page for thumbnail regeneration"
 */

function wcsuccess_register_custom_admin_page() {
  add_menu_page('Regenerate Thumbnails', 'Regenerate Thumbnails', 'manage_options', 'wcsuccess-regenerate-thumbnails', 'wcsuccess_regenerate_thumbnails_page');
}
add_action('admin_menu', 'wcsuccess_register_custom_admin_page');

function wcsuccess_regenerate_thumbnails_page() {
  if (isset($_POST['wcsuccess_regenerate'])) {
      wcsuccess_regenerate_thumbnails();
      echo '<div class="updated"><p>Thumbnails regenerated successfully!</p></div>';
  }
  ?>

  <div class="wrap">
      <h1>Regenerate Thumbnails</h1>
      <form method="post">
          <input type="hidden" name="wcsuccess_regenerate" value="1" />
          <p>
              <input type="submit" class="button-primary" value="Regenerate Thumbnails" />
          </p>
      </form>
  </div>
  <?php
}
