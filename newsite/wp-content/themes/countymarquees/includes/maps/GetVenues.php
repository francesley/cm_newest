<?php



function GetVenue($venue,$counter) {

	$arr_areas = array("wSussex" => "WEST SUSSEX", "eSussex" => "EAST SUSSEX", "beds" => "BEDFORDSHIRE", "berks" => "BERKSHIRE", "lon" => "LONDON", "herts" => "HERTFORDSHIRE", "bucks" => "BUCKINGHAMSHIRE", "kent" => "KENT", "surrey" => "SURREY", "oxford" => "OXFORDSHIRE", "essex" => "ESSEX", "hants" => "HAMPSHIRE",);

	$area = $arr_areas[(string) $venue["area"]];
	//$area = $venue["area"];
	$pricingBit = "";
	$details = $venue -> details;
	$details = str_replace ("</h3>" , "<br/>$area</h3>", $details);
	$details = str_replace ("/maps/images/" , get_template_directory_uri()."/maps/images/", $details);
	switch ($venue["price"]) {	
		case 0:		
		$pricingBit = "<p class='price cheap'>Venue cost:</p></div>";
		break;	
		case 1:
		$pricingBit = "<p class='price'>Venue cost:</p></div>";
		break;
		case 2:	
		$pricingBit = "<p class='price expensive'>Venue cost:</p></div>";
		break;
		default:
		$pricingBit = "</div>";
	}
	$extras = "<div class='extras'>";
	$extras .= $venue["civil"] == 1?"<p class='civil'>Civil license:</p>":"<p class='civil no'>Civil license:</p>";
	$extras .= $venue["large"] == 1?"<p class='civil'>Large marquees:</p>":"<p class='civil no'>Large marquees:</p>";
	$extras .= $pricingBit;
	$counter = $counter == 0?1:0;
	//echo $counter;
	
	if ($counter!=0) {
		echo "<div class='list-item " . $counter . "'>  ". $details . $extras . "</div>";
	 }
	 else {	
		echo "<div class='list-item " . $counter . "' style='margin-top:0;'>" . $details . $extras . "</div>";
	 }
 }