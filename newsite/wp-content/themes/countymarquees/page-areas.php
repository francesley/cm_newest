<?php
/**
 * Template Name: Areas Page 
 */
get_header();
?>


<section class="menu-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="menu-wrapper">
					<?php
					// Get the current page's ID
					$current_page_id = get_the_ID();
					// Get the taxonomy terms (replace 'your_taxonomy_name' with the actual taxonomy name)
					$taxonomy = 'primary_area';
					$terms = wp_get_post_terms($current_page_id, $taxonomy);
//var_dump($terms);
					if(!empty($terms)) {
						// Get the first term (assuming a page can have only one term)
						$term = reset($terms);
						// Get other pages attached to the same term
						$args = array(
							'post_type' => 'page',
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomy,
									'field' => 'id',
									'terms' => $term->term_id,
								),
							),
						);

						$query = new WP_Query($args);

                        ?>
                        <li class="large-item"><a href="<?php echo get_bloginfo('url'); ?>/areas/<?php echo $term->slug.'.htm';?>"><?php echo $term->name;?></a></li>
                        <?php

						if($query->have_posts()) {


							while($query->have_posts()) {

								get_permalink();
								$query->the_post();

								$posttitle = get_the_title();								
								?>
								<li class="<?php if($current_page_id == get_the_ID()) {
									echo "active";
								} ?>">
									<?php
									echo '<a href="'.get_permalink().'">'.$posttitle.'</a></li>';								
							}?>
						</ul>
						<?php
							wp_reset_postdata();
						}
					} else {
                            // This is the Top level county page (Bucks, North london, etc) . Generate menu for these pages                            
                            $taxonomy = 'primary_area';

                            // Get all terms for the specified taxonomy
                            $terms = get_terms(array(
                                'taxonomy' => $taxonomy,
                                'hide_empty' => false, 
                            ));

                            // Check if there are any terms
                            if (!empty($terms) && !is_wp_error($terms)) {
                               
                                foreach ($terms as $term) { 
                                    // Check if the term slug matches the current page's slug, add class to that li for underlineing
                                    $current_slug = get_query_var('name'); // Get the current page slug
                                    $li_class = $current_slug === $term->slug ? 'active' : '';
                                    echo '<li class="' . esc_attr($li_class) . '"><a href="' . esc_url(get_bloginfo('url') . '/areas/' . $term->slug . '.htm') . '">' . esc_html($term->name) . '</a></li>';
                                 }
                                
                            } else {
                                echo 'No terms found.';
                            }
                    }
					?>
			</div>
		</div>
	</div>
</section>

<?php
if (have_rows('page_section')):
    // Loop through sections
    while (have_rows('page_section')):
        the_row();

// This section is for the Top Banner slider section
        if (get_row_layout() == 'hero_slider') {            
            ?>
            <section id="heroIndicators" class="carousel slide carousel-fade hero-slider" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <?php
                    if (have_rows('slider_items')):
                        $galCount = 0;
                        while (have_rows('slider_items')):
                            the_row();
                            $slide_image = get_sub_field('slide_image');
                            $slide_heading = get_sub_field('slide_heading');
                            $slide_text = get_sub_field('slide_text'); ?>
                            <button type="button" data-bs-target="#heroIndicators" data-bs-slide-to="<?php echo $galCount; ?>"
                                class="<?php if ($galCount == 0) {
                                    echo 'active';
                                } ?>" aria-current="true"
                                aria-label="Slide <?php echo $galCount; ?>"></button>
                            <?php
                            $galCount++;
                        endwhile;
                    endif;
                    ?>
                </div>

                <div class="carousel-inner">
                    <?php
                    if (have_rows('slider_items')):
                        $galCount = 0;
                        while (have_rows('slider_items')):
                            the_row();
                            $slider_image = get_sub_field('slider_image');
                            $slider_content = get_sub_field('slider_content'); ?>
                            <div class="carousel-item <?php if ($galCount == 0) {
                                echo 'active';
                            } ?>">
                                <img src="<?php echo $slider_image['url']; ?>" alt="<?php echo $slider_image['alt']; ?>" />
                                <div class="slider-content container">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php echo $slider_content; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $galCount++;
                        endwhile;
                    endif;
                    ?>
                </div>
                </section>
        <?php } 
//end top banner section


//3 columns with title content
        if (get_row_layout() == '3_columns_with_title_content') {
        // This section is for the 3 Column blocks
                $stitle = get_sub_field( 'title' );
                $ssubtitle = get_sub_field( 'subtitle' );
            ?>
            <section class="three-col-sec">
                    <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php if($stitle){?><h2><?php echo $stitle; ?></h2><?php } ?>
                                            <?php if($ssubtitle){?><p><?php echo $ssubtitle; ?></p><?php } ?>
                                        </div>
                                    </div>                        
                                    <div class="row">
                                    <?php
                                        if (have_rows('content_blocks')):                                                    
                                            while (have_rows('content_blocks')):
                                                the_row();
                                                $block_link = get_sub_field('block_link');
                                                $image = get_sub_field('image');
                                                //var_dump($image);
                                                //exit;
                                                $block_title = get_sub_field('block_title'); ?>
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="block-wrapper">
                                                                <a href="<?php echo $block_link; ?>">
                                                                                                                                             
                                                                    <img src="<?php echo $image['sizes']['gallery-size-medium']; ?>" alt="<?php echo $image['alt']; ?>" />                                                                             
                                                                </a>

                                                                <a href="<?php echo $block_link; ?>">
                                                                <?php echo $block_title; ?>
                                                                </a>

                                                        </div>
                                                    </div>
                                                <?php                                                       
                                            endwhile;
                                        endif;
                                    ?>
                                        </div>
                        </div>
             </section>
        <? } 

    // This section is for the Top menu links 
        if (get_row_layout() == 'menu_section') {
                ?>
                <section class="menu-section">  
                <div class="container">           
                    <div class="row">
                        <div class="col-12">
                                <ul class="menu-wrapper">
                                        <?php
                                        if (have_rows('menu_list')):                                                    
                                            while (have_rows('menu_list')):
                                                the_row();
                                                $menu_title = get_sub_field('menu_title');
                                                $menu_link = get_sub_field('menu_link');
                                                ?>                                                                
                                                        <li class="<?php if(get_sub_field('currently_active')){echo "active";} ?>">
                                                            <a href="<?php echo $menu_link; ?>">
                                                                <?php echo $menu_title; ?>                                                                       
                                                            </a>
                                                        </li>                                                               
                                                <?php                                                       
                                            endwhile;
                                        endif;
                                        ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } 

        // Top Content section
        if (get_row_layout() == 'top_content_section') {

                 $section_title = get_sub_field( 'section_title' );
                 $section_content = get_sub_field( 'section_content' );
                 $h1_class = empty($section_content)?" class='nomargin' ":"";
                 //ShowDev( $h1_class);
                 
            ?>
            <section class="content-section"> 
                <div class="container">                 
                    <div class="row">
                        <div class="col-12">
                            <div class="content-wrapper">
                                <h1 <?php echo $h1_class ?>><?php echo $section_title; ?></h1>
                                <?php echo $section_content; ?>
                            </div>
                        </div>
                    </div>
                    <?php 
                    if(!empty($terms)) {                        
                        
                        // Get locations attached to the county
						$args = array(
							'post_type' => 'page',
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomy,
									'field' => 'slug',
									'terms' => get_query_var('name'),
								),
							),
						);

						$query = new WP_Query($args);

                        if($query->have_posts()) {

                            ?>
                            <div class="row">
                              <div class="col-12">                                
                             <ul class="menu-wrapper">
                                <?php 
							while($query->have_posts()) {

								get_permalink();
								$query->the_post();

								$postslug = get_post_field('post_name') == "bucks" ? $term->name : ucwords(str_replace("-", " ", get_post_field('post_name')));								
								?>
								<li class="<?php if($current_page_id == get_the_ID()) {
									echo "active";
								} ?>">
									<?php
									echo '<a href="'.get_permalink().'">'.$postslug.'</a></li>';								
							}?>
						</ul>
                        </div>
                        </div>
						<?php
							wp_reset_postdata();
						}
                     } ?>
                </div>
            </section>
        <?php } 

            // Multi Column photo section

            if (get_row_layout() == 'multi_column_image_section') {
                $columns = get_sub_field( 'columns' );
            ?>
            <section class="multi-column-section"> 
            <div class="container">                 
                <div class="row">
                    <div class="col-12">
                                <div class="column-wrapper-<?php echo $columns; ?>">
                                        <?php
                                        if (have_rows('columns_content')):                                                    
                                        while (have_rows('columns_content')):
                                            the_row();
                                            $column_image = get_sub_field('column_image');
                                            $column_content = get_sub_field('column_content');
                                            $column_link = get_sub_field('column_link');
                                        ?>                                                                
                                            <div class="list-item">
                                                <a href="<?php echo $column_link; ?>">
                                                <img src="<?php echo $column_image['url']; ?>" alt="<?php echo $column_image['alt']; ?>" />                                                                             
                                                </a>                                                
                                                <?php echo $column_content; ?> 
                                            </div>                                                               
                                        <?php                                                       
                                        endwhile;
                                        endif;
                                        ?>
                                </div>
                    </div>
                </div>
            </div>
            </section>
            <?php } 

            // Full Width Testimonial section with image background
                if (get_row_layout() == 'full_width_single_testimonial') {
                    $full_width_image = get_sub_field( 'full_width_image' );
                    $testimonial_text = get_sub_field( 'testimonial_text' );
                    $testimonial_image = get_sub_field( 'testimonial_image' );
                    $readmore_link = get_sub_field( 'readmore_link' );
                    $readmore_button_text = get_sub_field( 'readmore_button_text' );
                    ?>
                    <section class="fullwidth-testimonial-section">    
                            <picture>
                                <!-- Small Screen -->
                                <source srcset="<?php echo home_url().'/images/testimonials/small/'.$testimonial_image; ?>" media="(max-width: 480px)">
                                <!-- Medium Screen -->
                                <source srcset="<?php echo home_url().'/images/testimonials/'.$testimonial_image; ?>" media="(max-width: 768px)">
                                <!-- Large Screen and Default -->
                                <source srcset="<?php echo home_url().'/images/testimonials/large/'.$testimonial_image; ?>" media="(min-width: 769px)">
                                <img
                                src="<?php echo home_url().'/images/testimonials/large/'.$testimonial_image; ?>"
                                alt="<?php echo esc_attr(get_the_title()); ?>"/>
                            </picture>
                            <div class="testimonial-wrapper">
                            <?php echo $testimonial_text; ?>
                            <?php if($readmore_link){?><a class="btn btn-class dark" href="<?php echo $readmore_link;?>"><?php if($readmore_button_text!=""){ echo $readmore_button_text; }else{ echo 'Read More'; }?></a><?php }?>
                            </div>
    
                            </div>
                    </section>
            <?php } 

                    //Two Column Image / Text Section
                    
                    if ( get_row_layout() == 'two_column_image_text_section' ) {  
                        //Get column 1 details
                    $column_1 = get_sub_field( 'column_1' );
                    $column_1_width = $column_1[ 'column_1_width' ];
                    $column_1_offset = $column_1[ 'offset' ];
                    $column_1_type = $column_1[ 'column_1_type' ];
                    $column_1_image_items = $column_1[ 'image_items' ];
                    $column_1_content = $column_1[ 'column_content' ];

                    //var_dump( $column_1_image_items);

                    //Get column 2 details
                    $column_2 = get_sub_field( 'column_2' );
                    $column_2_width = $column_2[ 'column_2_width' ];
                    $column_2_offset = $column_2[ 'offset_2' ];
                    $column_2_type = $column_2[ 'column_2_type' ];
                    $column_2_image_items = $column_2[ 'image_items2' ];
                    $column_2_content = $column_2[ 'column_content' ];
                    ?>
                    <section class="two-col-section">
<div class="container">
<div class="row">
<?php if( $column_1_type  == "text"){ ?>
<div class="left-col <?php echo $column_1_width ; ?> <?php echo 'offset-md-'.$column_1_offset ; ?>">
<div class="col-content"><?php echo $column_1_content; ?></div>
</div>
<?php } if( $column_1_type  == "image"){ ?>
<div class="left-col <?php echo $column_1_width ; ?> <?php echo 'offset-md-'.$column_1_offset ; ?>">
<div class="sl-gallery">
                            <?php  //  loop through the image items and display them

                            foreach ( $column_1_image_items as $value ) {
                                $image_id = $value['image_item'];// gets the image id

                                $large_image = wp_get_attachment_image_src( $image_id, 'gallery-size-large');
                                //$large_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full' );
                                $medium_image = wp_get_attachment_image_src( $image_id, 'gallery-size-medium');
                                $small_image = wp_get_attachment_image_src( $image_id, 'gallery-size-small');

                                $large_image_url = $large_image[0];
                                $medium_image_url = $medium_image[0];
                                $small_image_url = $small_image[0];

                               // $image_url = wp_get_attachment_url($image_id);
                                // Get the alt text
                                $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
                                $smaller = $value['smaller'];
                                $image_overlay_text = $value['image_overlay_text'];
                                $image_overlay_link = $value['image_overlay_link'];
                                $image_overlay_link_text = $value['overlay_link_text'];
                                ?>

<div class="image-wrapper <?php if($smaller){ echo "smaller";}?>"> 
<picture>
<!-- Small Screen -->
<source srcset="<?php echo esc_url($small_image_url); ?>" media="(max-width: 480px)">

<!-- Medium Screen -->
<source srcset="<?php echo esc_url($medium_image_url); ?>" media="(max-width: 1024px)">

<!-- Large Screen and Default -->
<source srcset="<?php echo esc_url($large_image_url); ?>" media="(min-width: 1025px)">

<!-- Fallback for browsers that do not support the <picture> element -->
<img
src="<?php echo esc_url($large_image_url); ?>"
alt="<?php echo esc_attr($image_alt); ?>"
height="415"
width="620"
/>
</picture>
                    <?php if($image_overlay_text) {?>
<div class="overlay-text-wrapper">
<div><?php echo $image_overlay_text; ?></div>
<?php if($image_overlay_link){?> <a href="<?php echo $image_overlay_link;?>"><?php echo $image_overlay_link_text;?></a> <?php } ?>
</div>
<?php } ?>
</div>

                           <!--<img src="<?php echo $medium_image_url ?>" alt="<?php echo $image_alt; ?>" class="image"> -->
                           
                            <?php } ?>
</div>
</div>
                        <?php } ?>
                        <?php if( $column_2_type  == "text"){ ?>
<div class="right-col <?php echo $column_2_width ; ?> <?php echo 'offset-md-'.$column_2_offset ; ?>">
    <div class="col-content"><?php echo $column_2_content; ?></div>
</div>
                        <?php } ?>
                        <?php if( $column_2_type  == "image"){ ?>
<div class="right-col <?php echo  $column_2_width ; ?> <?php echo 'offset-md-'.$column_2_offset ; ?>">
<div class="sl-gallery">
<?php  //  loop through the image items and display them

                        foreach ( $column_2_image_items as $value ) {
//VAR_DUMP $value to see whats there
                                $image = $value['image_item2'];// gets the image id
                                $image_id = $image['id'];
                                $large_image = wp_get_attachment_image_src( $image_id, 'gallery-size-large');
                                //$large_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full' );
                                $medium_image = wp_get_attachment_image_src( $image_id, 'gallery-size-medium');
                                $small_image = wp_get_attachment_image_src( $image_id, 'gallery-size-small');

                                $large_image_url = $large_image[0];
                                $medium_image_url = $medium_image[0];
                                $small_image_url = $small_image[0];

                               // $image_url = wp_get_attachment_url($image_id);
                                // Get the alt text
                                $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
                                $smaller = $value['smaller2'];
                                $image_overlay_text = $value['image_overlay_text'];
                                $image_overlay_link = $value['image_overlay_link'];
                                $image_overlay_link_text = $value['image_overlay_link_text'];
                                ?>

<div class="image-wrapper <?php if($smaller){ echo "smaller";}?>"> 
<picture>
<!-- Small Screen -->
<source srcset="<?php echo esc_url($small_image_url); ?>" media="(max-width: 480px)">

<!-- Medium Screen -->
<source srcset="<?php echo esc_url($medium_image_url); ?>" media="(max-width: 1024px)">

<!-- Large Screen and Default -->
<source srcset="<?php echo esc_url($large_image_url); ?>" media="(min-width: 1025px)">

<!-- Fallback for browsers that do not support the <picture> element -->
<img
src="<?php echo esc_url($large_image_url); ?>"
alt="<?php echo esc_attr($image_alt); ?>"
height="415"
width="620"
/>
</picture>
                           
                                    <?php if($image_overlay_text){?>
                                            <div class="overlay-text-wrapper">
                                                <div><?php echo $image_overlay_text; ?></div>
                                                <?php if($image_overlay_link){?> <a href="<?php echo $image_overlay_link;?>"><?php echo $image_overlay_link_text;?></a><?php } ?>
                                            </div>
                                    <?php } ?>                        
                            </div>
                            <?php
                            }
                            ?>
                            </div>
                        </div>
                        <?php } ?>
                        </div>
                    </div>
                    </section>
                    <?php }


                    //Two Column Slider / Text Section
                    
                    if ( get_row_layout() == 'two_column_slider_text_section' ) {  
                        //Get column 1 details
                    $column_1 = get_sub_field( 'column_1' );
                    $column_1_width = $column_1[ 'column_1_width' ];
                    $column_1_offset = $column_1[ 'offset' ];
                    $column_1_image_items = $column_1[ 'image_items' ];

                    

                    //Get column 2 details
                    $column_2 = get_sub_field( 'column_2' );
                    $column_2_width = $column_2[ 'column_2_width' ];
                    $column_2_offset = $column_2[ 'offset_2' ];
                    $column_2_content = $column_2[ 'column_content' ];
                    ?>
                    <section class="two-col-section">
                    <div class="container">
                        <div class="row">
                            <div class="left-col <?php echo $column_1_width ; ?> <?php echo 'offset-md-'.$column_1_offset ; ?>">
                            <div class="slider">

                            <?php  //  loop through the image items and display them
                            foreach ( $column_1_image_items as $value ) {
                                $image_id = $value['image_item']['ID'];// gets the image id
                                $image_url = wp_get_attachment_url($image_id);
                               // var_dump($image_url);
                                // Get the alt text
                                $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
                              
                                ?>
                            <div class="image-wrapper"> <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="image"> </div>
                            <?php } ?>

                            </div>

                            <!-- Thumbnail navigation -->
                            <div class="thumbnail-slider">   
                            <?php  //  loop through the image items and display them
                            foreach ( $column_1_image_items as $value ) {
                            $image_id = $value['image_item']['ID'];// gets the image id
                            $image_url = wp_get_attachment_url($image_id);
                            // Get the alt text
                            $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);

                            ?>
                            <div class="image-thumbnail-wrapper"> <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="image"> </div>
                            <?php } ?>
                             <!-- Add more thumbnails as needed -->
                                </div>
                            </div>
            
                        <div class="right-col <?php echo $column_2_width ; ?> <?php echo 'offset-md-'.$column_2_offset ; ?>">
                            <div class="col-content"><?php echo $column_2_content; ?></div>
                        </div>

                        </div>
                    </div>
                    </section>
                    <?php }
        
        if (get_row_layout() == 'full_width_image_with_overlay_content') {
                $full_width_image = get_sub_field( 'full_width_image' );
                $overlay_position = get_sub_field( 'overlay_position' );
                $overlay_content = get_sub_field( 'overlay_content' );
            ?>
            <section class="fullwidth-overlay" style="background-image: url('<?php echo $full_width_image['url']; ?>')">
                    <div class="container-full">                        
                                    <div class="row">
                                        <div class="col-md-5 col-sm-6 flex-wrapper">
                                        <div class="overlay-wrapper <?php if($overlay_position=='right'){ echo "float-end";}?>">
                                            <?php echo $overlay_content;?>
                                        </div>
                                   </div>
                                 </div>
                        </div>

             </section>
        <?php }  
        
        //Full width Content section
    
        if ( get_row_layout() == 'full_width_content_section' ) {  
            $full_width_content = get_sub_field( 'full_width_content' );

            ?>
            <section class="fullwidth-content">
                    <div class="container-full">                        
                                    <div class="row">
                                        <div class="col-md-8 offset-md-2">
                                        <div class="wrapper-content">
                                            <?php echo $full_width_content;?>
                                        </div>
                                   </div>
                                 </div>
                        </div>

             </section>
             <?php
        }


        // Geid Width Image/ Video section
        // Shows vimeo video or Image based on the option selected in the admin
        if (get_row_layout() == 'grid_width_image_section') {
            $section_type = get_sub_field( 'section_type' );          
           
        ?>
        <section class="grid-width-section">
                <div class="container">                        
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if($section_type == 'image'){
                                             $full_width_image = get_sub_field( 'full_width_image' );?>                                           
                                            <img class="image" src="<?php echo $full_width_image; ?>" />
                                        <?php }else{         // Enqueue your lazysizes JavaScript file if the ACF field exists
                                                                    wp_enqueue_script('lazysizes-js', get_template_directory_uri() . '/js/lazysizes.min.js', array('jquery'), '1.0', true);        
                                                                    $vimeo_video_id = get_sub_field( 'vimeo_video_id' );
                                                    ?>
                                                    <div class='videosection'>
                                                    <div class='video-sec'>
                                                            <div class='ls_video_embed aspect-16-9 lazyload' data-vimeo='<?php echo $vimeo_video_id; ?>'> 
                                                            <span class='play-btn' aria-hidden='true'></span></div>
                                                    </div>
                                                </div>
                                        <?php } ?>

                            </div>
                            </div>
                    </div>

        </section>
        <?php }  




        



        // Map Sectionvar_dump($post);exit();
        if (get_row_layout() == 'map_block') {
            $map_url = get_sub_field('map_content');?>	
			<section class="map-section">
						<div class="container">                        
										<div class="row">
											<div class="col-md-12">
                                                <h2>We cover all of <?php echo $post -> post_excerpt; ?></h2>
													<?php ?>
													<iframe src="<?php echo $map_url; ?>" width="600" height="450" frameborder="0" style="border:0"
														allowfullscreen></iframe>
											</div>
									</div>
							</div>

				</section>
        <?php }  



        // Map Section
        if (get_row_layout() == 'local_resources_testimonial_block') {

            $testi_counter = 0;

            $london = <<<EOD
             <a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
            <div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London </span><span class="region hide">London</span><span class="postal-code">NW5 1SL</span>
EOD;
            $surrey = <<<EOD
            <a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span>
EOD;
            $sussex = <<<EOD
           <a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span>
EOD;
            $kent = <<<EOD
            <a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
            <div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span>
EOD;
			?>	
<section class="resource-section">
    <div class="container">                        
        <div class="row">
            <div class="col-md-12">
            <h2>Local praise</h2>
        </div>
    </div>                        
<?php

if(have_rows('testimonials')) {?>
    <div class="row">
    <?php while(have_rows('testimonials')) {
        $row = the_row();
        $testi_counter ++;
        $pretty_date = "";
        //print_r($row);
       // ShowTrue(empty(get_sub_field('testi_date')));

        $testi_text = get_sub_field('testi_text');
        $testi_location = explode(',',get_sub_field('testi_name'));
        $testi_region = get_sub_field('cm_region');
        $region_var = $$testi_region;
        $testi_header = get_sub_field('testi_header');
        $testi_date = get_sub_field('testi_date');
        if (!empty($testi_date)) {
            $date = DateTime::createFromFormat('m/Y', $testi_date);
            $pretty_date = $date->format('F, Y');
        }
        
        $column_2_offset = $testi_counter == 1?"offset-md-0":"offset-md-1";
//ShowDev($testi_location,"location");
														
//ShowDev($testi_region,"region");

//echo $region_var;

                                           
?>


<div class="col-md-5 <?php echo  $column_2_offset  ?>">
                           
<div class="hreview">
<span class="summary hide"><?php echo $testi_header ?></span>
<abbr title="<?php echo $testi_date  ?>" class="dtreviewed hide"><?php echo $pretty_date  ?></abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="adr"><span class="locality"><h3><?php echo $testi_location[0] ?></h3></span> <span class="region hide"><?php echo $testi_location[1] ?></span></span></span><span class="rating hide">5</span><span class="type hide">business</span><div class="item vcard hide">
<?php echo $region_var  ?><div class="country-name  hide">UK</div></div></div>
<blockquote class="description"><?php echo $testi_text ?></blockquote>
</div></div>


													<?php }
												}?>
</div>
											<h2>Local resources</h2><div class="row"><div class="col-md-5 offset-md-0>">
									<?php if(have_rows('resources')) {
										while(have_rows('resources')) {
											the_row();
                                            
											echo get_sub_field('resource_content');
                                            $area_region = get_sub_field('area_cm_region');

                                            $area_region = $$area_region;?>
</div>
<div class="col-md-5 offset-md-1">
<div class="hreview">
<div class="item vcard">
                                            <?php  echo $area_region; } }?>
											

<div class="country-name  hide">UK</div></div></div>
</div></div>
</div>
</section>
        <?php }  
    
    
    endwhile; 
    endif;     
	?>


<?php get_footer(); ?>