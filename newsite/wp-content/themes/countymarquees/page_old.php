<?php
get_header();
if (have_rows('page_section')):
    // Loop through sections
    while (have_rows('page_section')):
        the_row();
        if (get_row_layout() == 'hero_slider') {
            // This section is for the Top Banner slider section
            ?>
            <section id="heroIndicators" class="carousel slide carousel-fade hero-slider" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <?php
                    if (have_rows('slider_items')):
                        $galCount = 0;
                        while (have_rows('slider_items')):
                            the_row();
                            $slide_image = get_sub_field('slide_image');
                            $slide_heading = get_sub_field('slide_heading');
                            $slide_text = get_sub_field('slide_text'); ?>
                            <button type="button" data-bs-target="#heroIndicators" data-bs-slide-to="<?php echo $galCount; ?>"
                                class="<?php if ($galCount == 0) {
                                    echo 'active';
                                } ?>" aria-current="true"
                                aria-label="Slide <?php echo $galCount; ?>"></button>
                            <?php
                            $galCount++;
                        endwhile;
                    endif;
                    ?>
                </div>

                <div class="carousel-inner">
                    <?php
                    if (have_rows('slider_items')):
                        $galCount = 0;
                        while (have_rows('slider_items')):
                            the_row();
                            $slider_image = get_sub_field('slider_image');
                            $slider_content = get_sub_field('slider_content'); ?>
                            <div class="carousel-item <?php if ($galCount == 0) {
                                echo 'active';
                            } ?>">
                                <img src="<?php echo $slider_image['url']; ?>" alt="<?php echo $slider_image['alt']; ?>" />
                                <div class="slider-content container">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php echo $slider_content; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $galCount++;
                        endwhile;
                    endif;
                    ?>
                </div>
                </section>
        <?php } 
        if (get_row_layout() == '3_columns_with_title_content') {
        // This section is for the 3 Column blocks
                $stitle = get_sub_field( 'title' );
                $ssubtitle = get_sub_field( 'subtitle' );
            ?>
            <section class="three-col-sec">
                    <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php if($stitle){?><h2><?php echo $stitle; ?></h2><?php } ?>
                                            <?php if($ssubtitle){?><p><?php echo $ssubtitle; ?></p><?php } ?>
                                        </div>
                                    </div>                        
                                    <div class="row">
                                    <?php
                                                if (have_rows('content_blocks')):                                                    
                                                    while (have_rows('content_blocks')):
                                                        the_row();
                                                        $block_link = get_sub_field('block_link');
                                                        $image = get_sub_field('image');
                                                        $block_title = get_sub_field('block_title'); ?>
                                                            <div class="col-md-4 col-sm-6">
                                                                <div class="block-wrapper">
                                                                      <a href="<?php echo $block_link; ?>">
                                                                          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />                                                                             
                                                                      </a>

                                                                      <a href="<?php echo $block_link; ?>">
                                                                      <?php echo $block_title; ?>
                                                                     </a>

                                                                </div>
                                                            </div>
                                                        <?php                                                       
                                                    endwhile;
                                                endif;
                                                ?>
                                        </div>
                        </div>
             </section>
        <? } 

    // This section is for the Top menu links 
        if (get_row_layout() == 'menu_section') {
                ?>
                <section class="menu-section">  
                <div class="container">           
                    <div class="row">
                        <div class="col-12">
                                <ul class="menu-wrapper">
                                        <?php
                                        if (have_rows('menu_list')):                                                    
                                            while (have_rows('menu_list')):
                                                the_row();
                                                $menu_title = get_sub_field('menu_title');
                                                $menu_link = get_sub_field('menu_link');
                                                ?>                                                                
                                                        <li class="<?php if(get_sub_field('currently_active')){echo "active";} ?>">
                                                            <a href="<?php echo $menu_link; ?>">
                                                                <?php echo $menu_title; ?>                                                                       
                                                            </a>
                                                        </li>                                                               
                                                <?php                                                       
                                            endwhile;
                                        endif;
                                        ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            <? } 
        // Content section
        if (get_row_layout() == 'top_content_section') {
                 $section_title = get_sub_field( 'section_title' );
                 $section_content = get_sub_field( 'section_content' );
            ?>
            <section class="content-section"> 
                <div class="container">                 
                    <div class="row">
                        <div class="col-12">
                            <div class="content-wrapper">
                                <h1><?php echo $section_title; ?></h1>
                                <?php echo $section_content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <? } 
        if (get_row_layout() == 'subhead') {
                 $section_subtitle = get_sub_field( 'section_subtitle' );
         ?>
            
                <div class="container">                 
                    <div class="row">
                        <div class="col-12">
                            <div class="content-wrapper">
                                <?php echo $section_subtitle; ?>
                            </div>
                        </div>
                    </div>
                </div>
        <? } 

            // Multi Column photo section

            if (get_row_layout() == 'multi_column_image_section') {
                $columns = get_sub_field( 'columns' );
            ?>
            <section class="multi-column-section"> 
            <div class="container">                 
                <div class="row">
                    <div class="col-12">
                                <div class="column-wrapper-<?php echo $columns; ?>">
                                        <?php
                                        if (have_rows('columns_content')):                                                    
                                        while (have_rows('columns_content')):
                                            the_row();
                                            $column_image = get_sub_field('column_image');
                                            $column_content = get_sub_field('column_content');
                                            $column_link = get_sub_field('column_link');
                                        ?>                                                                
                                            <div class="list-item">
                                                <a href="<?php echo $column_link; ?>">
                                                <img src="<?php echo $column_image['url']; ?>" alt="<?php echo $column_image['alt']; ?>" />                                                                             
                                                </a>                                                
                                                <?php echo $column_content; ?> 
                                            </div>                                                               
                                        <?php                                                       
                                        endwhile;
                                        endif;
                                        ?>
                                </div>
                    </div>
                </div>
            </div>
            </section>
            <? } 
            // Full Width Testimonial section with image background

                if (get_row_layout() == 'full_width_single_testimonial') {
                $full_width_image = get_sub_field( 'full_width_image' );
                $testimonial_text = get_sub_field( 'testimonial_text' );
                $testimonial_image = get_sub_field( 'testimonial_image' );
                $readmore_link = get_sub_field( 'readmore_link' );
                $readmore_button_text = get_sub_field( 'readmore_button_text' );
                ?>
                <section class="fullwidth-testimonial-section">

                        <picture>
                            <!-- Small Screen -->
                            <source srcset="<?php echo home_url().'/images/testimonials/small/'.$testimonial_image; ?>" media="(max-width: 480px)">
                            <!-- Medium Screen -->
                            <source srcset="<?php echo home_url().'/images/testimonials/'.$testimonial_image; ?>" media="(max-width: 768px)">
                            <!-- Large Screen and Default -->
                            <source srcset="<?php echo home_url().'/images/testimonials/large/'.$testimonial_image; ?>" media="(min-width: 769px)">
                            <img
                            src="<?php echo home_url().'/images/testimonials/large/'.$testimonial_image; ?>"
                            alt="<?php echo esc_attr(get_the_title()); ?>"/>
                        </picture>
                        <div class="testimonial-wrapper">
                        <?php echo $testimonial_text; ?>
                        <?php if($readmore_link){?><a class="btn btn-class dark" href="<?php echo $readmore_link;?>"><?php if($readmore_button_text!=""){ echo $readmore_button_text; }else{ echo 'Read More'; }?></a><?php }?>
                        </div>

                        </div>
                </section>
            <? } 

                // Video banner Block
                if ( get_row_layout() == 'banner_block' ) {
                    $banner_image = get_sub_field( 'banner_image' );
                    $video_id = get_sub_field( 'video_id' );
                    $banner_content = get_sub_field( 'banner_content' );
                    
                    ?>
                <section class="full-width-banner">
                <div class="hd">
                    <div class="videoholder h-100">
                    <div class="vcontent" id="cwfscreen" role="presentation" style="background-image: url(<?php echo esc_url($banner_image['url']);   ?>)">
                        <div class="vimeo-wrapper" id="vimeoembd">
                        <iframe src="https://player.vimeo.com/video/<?php echo $video_id; ?>?background=1&autoplay=1&loop=1&autopause=0&byline=0&title=0&controls=0"
                            frameborder="0" mozallowfullscreen webkitallowfullscreen allowfullscreen aria-label="Intro Video"></iframe>
                        </div>
                    </div>

                    <?php if ($banner_content){?>
                        <div class="banner-overlay">
                            <div class="container">                 
                                <div class="row">
                                    <div class="col-12">
                                    <?php echo $banner_content; ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                        </div>
                    <?php }?>

                    </div>
                </div>
               </div>
                <?php
                }


                // FAQs block
                if ( get_row_layout() == 'faq_section' ) {
                    $banner_image = get_sub_field( 'banner_image' );
                    $video_id = get_sub_field( 'video_id' );
                    ?>
                <section class="faq-section">
                    <div class="container">                 
                        <div class="row">
                                <div class="col-12">
                                <div class="accordion" id="accordionFaq">
                                                <?php

                                                 $outercount = 1;
                                                if (have_rows('faqs')):                                                    
                                                while (have_rows('faqs')):
                                                the_row();
                                                $title = get_sub_field('title');
                                                ?>      
                                                
                                                <h2><?php echo $title; ?></h2>
                                                            <?php
                                                            $faqcount = 1;
                                                            if (have_rows('faq_list')):                                                    
                                                            while (have_rows('faq_list')):
                                                            the_row();
                                                            $question = get_sub_field('question');
                                                            
                                                            $column_1 = get_sub_field( 'answer_column_1' );
                                                            $column_1_width = $column_1[ 'column_width' ];
                                                            $column_1_offset = $column_1[ 'offset' ];
                                                            $column_1_type= $column_1[ 'column_type' ];
                                                            $column_1_image_item = $column_1[ 'image_item' ];
                                                            $column_1_content = $column_1[ 'column_content' ];


                                                            $column_2 = get_sub_field( 'answer_column_2' );
                                                            $column_2_width = $column_2[ 'column_width2' ];
                                                            $column_2_offset = $column_2[ 'offset2' ];
                                                            $column_2_type = $column_2[ 'column_type2' ];
                                                            $column_2_image_item = $column_2[ 'image_item2' ];
                                                            $column_2_content = $column_2[ 'column_content2' ];
                                                            ?> 

                                                            <div class="accordion-item question-item">
                                                                <h3 class="accordion-header" id="heading<?php echo $outercount.$faqcount; ?>">
                                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $outercount.$faqcount; ?>" aria-expanded="true" aria-controls="collapse<?php echo $outercount.$faqcount; ?>">
                                                                <?php echo $question; ?>
                                                                </button>
                                                                </h3>
                                                                <div id="collapse<?php echo $outercount.$faqcount; ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo $outercount.$faqcount; ?>">
                                                                <div class="accordion-body">
                                                               


                                                                <div class="row">
                                                                              <?php if ($column_1_type == "text") { ?>
                                                                                                    <div class="left-col <?php echo $column_1_width; ?> <?php echo 'offset-md-' . $column_1_offset; ?>">
                                                                                                        <div class="col-content">
                                                                                                            <?php echo $column_1_content; ?>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php }
                                                                                                if ($column_1_type == "image") { ?>
                                                                                                    <div class="left-col <?php echo $column_1_width; ?> <?php echo 'offset-md-' . $column_1_offset; ?>">
                                                                                                        <div class="sl-gallery">
                                                                                                           <img src="<?php echo $column_1_image_item['url']; ?>" alt="<?php echo $column_1_image_item['alt']; ?>" class="image">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php } ?>
                                                                                                <?php if ($column_2_type == "text") { ?>
                                                                                                    <div class="right-col <?php echo $column_2_width; ?> <?php echo 'offset-md-' . $column_2_offset; ?>">
                                                                                                        <div class="col-content">
                                                                                                            <?php echo $column_2_content; ?>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php } ?>
                                                                                                <?php if ($column_2_type == "image") { ?>
                                                                                                    <div class="right-col <?php echo $column_2_width; ?> <?php echo 'offset-md-' . $column_2_offset; ?>">
                                                                                                        <div class="sl-gallery">
                                                                                                        <img src="<?php echo $column_2_image_item['url']; ?>" alt="<?php echo $column_2_image_item['alt']; ?>" class="image">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php } ?>
                                                                </div>

                                                                </div>
                                                                </div>
                                                            </div>                                                             
                                                            <?php   
                                                            $faqcount++;                                                    
                                                            endwhile;
                                                            endif;
                                                            ?>                                                             
                                                <?php      
                                                 $outercount++;                                                 
                                                endwhile;
                                                endif;
                                                ?>
                                                </div>
                                </div>
                        </div>
                    </div>
                </section>
                <?php
                }

                    //Two Column Image / Text Section
                    
                    if ( get_row_layout() == 'two_column_image_text_section' ) {  
                        //Get column 1 details
                    $column_1 = get_sub_field( 'column_1' );
                    $column_1_width = $column_1[ 'column_1_width' ];
                    $column_1_offset = $column_1[ 'offset' ];
                    $column_1_type = $column_1[ 'column_1_type' ];
                    $column_1_image_items = $column_1[ 'image_items' ];
                    $column_1_content = $column_1[ 'column_content' ];

                    //Get column 2 details
                    $column_2 = get_sub_field( 'column_2' );
                    $column_2_width = $column_2[ 'column_2_width' ];
                    $column_2_offset = $column_2[ 'offset_2' ];
                    $column_2_type = $column_2[ 'column_2_type' ];
                    $column_2_image_items = $column_2[ 'image_items2' ];
                    $column_2_content = $column_2[ 'column_content' ];
                    ?>
                    <section class="two-col-section">
                    <div class="container">
                        <div class="row">
                        <?php if( $column_1_type  == "text"){ ?>
                        <div class="left-col <?php echo $column_1_width ; ?> <?php echo 'offset-md-'.$column_1_offset ; ?>">
                            <div class="col-content"><?php echo $column_1_content; ?></div>
                        </div>
                        <?php } if( $column_1_type  == "image"){ ?>
                            <div class="left-col <?php echo $column_1_width ; ?> <?php echo 'offset-md-'.$column_1_offset ; ?>">
                            <div class="sl-gallery">
                            <?php  //  loop through the image items and display them
                            foreach ( $column_1_image_items as $value ) {
                                $image_id = $value['image_item'];// gets the image id
                                $image_url = wp_get_attachment_url($image_id);
                                // Get the alt text
                                $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
                                $smaller = $value['smaller'];
                                $image_overlay_text = $value['image_overlay_text'];
                                $image_overlay_link = $value['image_overlay_link'];
                                $image_overlay_link_text = $value['overlay_link_text'];
                                ?>
                            <div class="image-wrapper <?php if($smaller){ echo "smaller";}?>"> <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="image"> 
                                    <?php if($image_overlay_text) {?>
                                            <div class="overlay-text-wrapper">
                                                <div><?php echo $image_overlay_text; ?></div>
                                                <?php if($image_overlay_link){?> <a href="<?php echo $image_overlay_link;?>"><?php echo $image_overlay_link_text;?></a> <?php } ?>
                                            </div>
                                    <?php } ?>
                            </div>
                            <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if( $column_2_type  == "text"){ ?>
                        <div class="right-col <?php echo $column_2_width ; ?> <?php echo 'offset-md-'.$column_2_offset ; ?>">
                            <div class="col-content"><?php echo $column_2_content; ?></div>
                        </div>
                        <?php } ?>
                        <?php if( $column_2_type  == "image"){ ?>
                        <div class="right-col <?php echo  $column_2_width ; ?> <?php echo 'offset-md-'.$column_2_offset ; ?>">
                            <div class="sl-gallery">
                            <?php //var_dump($column_2_image_items);
                            foreach ( $column_2_image_items as $value ) {
                                $image = $value['image_item2'];// gets the image id
                            // var_dump($image_id);
                                $image_url = wp_get_attachment_url($image_id);
                                // Get the alt text
                                $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
                                $smaller = $value['smaller2'];
                                $image_overlay_text = $value['image_overlay_text'];
                                $image_overlay_link = $value['image_overlay_link'];
                                $image_overlay_link_text = $value['image_overlay_link_text'];
                                ?>
                            <div class="image-wrapper <?php if($smaller){ echo "smaller";}?>"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="image">
                        
                            <?php if($image_overlay_text){?>
                                            <div class="overlay-text-wrapper">
                                                <div><?php echo $image_overlay_text; ?></div>
                                                <?php if($image_overlay_link){?> <a href="<?php echo $image_overlay_link;?>"><?php echo $image_overlay_link_text;?></a><?php } ?>
                                            </div>
                                    <?php } ?>
                        </div>
                            <?php
                            }
                            ?>
                            </div>
                        </div>
                        <?php } ?>
                        </div>
                    </div>
                    </section>
                    <?php }


                    //Two Column Slider / Text Section
                    
                    if ( get_row_layout() == 'two_column_slider_text_section' ) {  
                        //Get column 1 details
                    $column_1 = get_sub_field( 'column_1' );
                    $column_1_width = $column_1[ 'column_1_width' ];
                    $column_1_offset = $column_1[ 'offset' ];
                    $column_1_image_items = $column_1[ 'image_items' ];

                    

                    //Get column 2 details
                    $column_2 = get_sub_field( 'column_2' );
                    $column_2_width = $column_2[ 'column_2_width' ];
                    $column_2_offset = $column_2[ 'offset_2' ];
                    $column_2_content = $column_2[ 'column_content' ];
                    ?>
                    <section class="two-col-section">
                    <div class="container">
                        <div class="row">
                            <div class="left-col <?php echo $column_1_width ; ?> <?php echo 'offset-md-'.$column_1_offset ; ?>">
                            <div class="slider">

                            <?php  //  loop through the image items and display them
                            foreach ( $column_1_image_items as $value ) {
                                $image_id = $value['image_item']['ID'];// gets the image id
                                $image_url = wp_get_attachment_url($image_id);
                               // var_dump($image_url);
                                // Get the alt text
                                $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
                              
                                ?>
                            <div class="image-wrapper"> <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="image"> </div>
                            <?php } ?>

                            </div>
                            <!-- Thumbnail navigation -->
                            <div class="thumbnail-slider">   
                            <?php  //  loop through the image items and display them
                            foreach ( $column_1_image_items as $value ) {
                            $image_id = $value['image_item']['ID'];// gets the image id
                            $image_url = wp_get_attachment_url($image_id);
                            // Get the alt text
                            $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);

                            ?>
                            <div class="image-thumbnail-wrapper"> <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="image"> </div>
                            <?php } ?>
                             <!-- Add more thumbnails as needed -->
                                </div>
                            </div>
            
                        <div class="right-col <?php echo $column_2_width ; ?> <?php echo 'offset-md-'.$column_2_offset ; ?>">
                            <div class="col-content"><?php echo $column_2_content; ?></div>
                        </div>

                        </div>
                    </div>
                    </section>
                    <?php }
        
        if (get_row_layout() == 'full_width_image_with_overlay_content') {
                $full_width_image = get_sub_field( 'full_width_image' );
                $overlay_position = get_sub_field( 'overlay_position' );
                $overlay_content = get_sub_field( 'overlay_content' );
            ?>
            <section class="fullwidth-overlay" style="background-image: url('<?php echo $full_width_image['url']; ?>')">
                    <div class="container-full">                        
                                    <div class="row">
                                        <div class="col-md-5 col-sm-6 flex-wrapper">
                                        <div class="overlay-wrapper <?php if($overlay_position=='right'){ echo "float-end";}?>">
                                            <?php echo $overlay_content;?>
                                        </div>
                                   </div>
                                 </div>
                        </div>

             </section>
        <? }  
        
        //Full width Content section
    
        if ( get_row_layout() == 'full_width_content_section' ) {  
            $full_width_content = get_sub_field( 'full_width_content' );

            ?>
            <section class="fullwidth-content">
                    <div class="container-full">                        
                                    <div class="row">
                                        <div class="col-md-8 offset-md-2">
                                        <div class="wrapper-content">
                                            <?php echo $full_width_content;?>
                                        </div>
                                   </div>
                                 </div>
                        </div>

             </section>
             <?php
        }


        // Geid Width Image/ Video section
        // Shows vimeo video or Image based on the option selected in the admin
        if (get_row_layout() == 'grid_width_image_section') {
            $section_type = get_sub_field( 'section_type' );          
           
        ?>
        <section class="grid-width-section">
                <div class="container">                        
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if($section_type == 'image'){
                                             $full_width_image = get_sub_field( 'full_width_image' );?>                                           
                                            <img class="image" src="<?php echo $full_width_image; ?>" />
                                        <?php }else{         // Enqueue your lazysizes JavaScript file if the ACF field exists
                                                                    wp_enqueue_script('lazysizes-js', get_template_directory_uri() . '/js/lazysizes.min.js', array('jquery'), '1.0', true);        
                                                                    $vimeo_video_id = get_sub_field( 'vimeo_video_id' );
                                                    ?>
                                                    <div class='videosection'>
                                                    <div class='video-sec'>
                                                            <div class='ls_video_embed aspect-16-9 lazyload' data-vimeo='<?php echo $vimeo_video_id; ?>'> 
                                                            <span class='play-btn' aria-hidden='true'></span></div>
                                                    </div>
                                                </div>
                                        <?php } ?>

                            </div>
                            </div>
                    </div>

        </section>
        <? }  




        // Casestudies listing 
        if (get_row_layout() == 'case_studies') {
                ?>
                  <section class="list-wrapper">
                  <div class="container">    
                  <div class="row gallery-list">
                        <?php $args = array(
						'post_type' => 'case_studies',
						'post_status' => 'publish',
						'posts_per_page' => 9,
						'orderby' => 'menu_order', // Use 'menu_order' for custom post types
						'order' => 'ASC',
					);

                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()):
                        while ($the_query->have_posts()):
                            $the_query->the_post();
        
                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="post-item">
                                    <a href="<?php echo get_the_permalink(); ?>">
                                        <div class="post-item-img">
                                            <img class="image" src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'gallery-size-large'); ?>" />
                                        </div>
                                    </a>
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="post-text">
                                            <?php the_title(); ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata();?>
        
                    <?php else: ?>
                        <!-- Your no post found code here -->
                    <?php endif; ?>
                    
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <div class="text-sm-end">
                            <?php if ( $the_query->max_num_pages > 1 ) { ?>
                            <a class="load-more btn btn-class dark"  data-page="1" data-post="case_studies" href="#seemore"> Load more </a>
                            <?php } ?>
                        </div>
                        </div>
                    </div>
                    </div>
                   </section>
            <? } 
    
    
    endwhile; 
    endif;     
    get_footer(); ?>