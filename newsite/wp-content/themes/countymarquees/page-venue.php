<?php
/**
 * Template Name: Venue Page
 */

 get_header();
?>

<section class="menu-section">  
                <div class="container">           
                    <div class="row">
                        <div class="col-12">
                                <ul class="menu-wrapper">
                                                                                                        
						<li class="active">
						<a class='selected'  href="<?php echo get_bloginfo('url');?>/venues.htm" >Marquee venue map</a>
						</li>                                                               
																				
						<li>
						<a href="<?php echo get_bloginfo('url');?>/venues/sites.htm" >Marquee venues lists</a></li>
																				
						<li>
						<a href="<?php echo get_bloginfo('url');?>/add_venue.htm" >Add a venue</a></li></ul>
                            </div>
                        </div>
                    </div>
                </section>


	<div id='outerBox' class="container">
		<div id='content'>	 
			
			<h1>Marquee Venue Finder</h1>
			<?php
				$url = home_url();	
			?>
				
			<table id="outerMap" cellspacing="0">
			<tr>
			<td class="row">
			<div id="side_bar" class="col-md-2"></div>
			<div class="map_wrapper col-md-10">
			<div id="map" style="width: 100%; height: 650px">
			</div>
			<div class="map-filter-wrapper"> 
			<h3>Filters:</h3>
			<select id="area" class="map_filters" onchange="speedTest.SelectType(this)">
			<option value="">All counties</option>
			<option value="beds">Bedfordshire</option>
			<option value="berks">Berkshire</option>
			<option value="bucks">Buckinghamshire</option>
			<option value="eSussex">East Sussex</option>
			<option value="essex">Essex</option>	
			<option value="hants">Hampshire</option>	
			<option value="herts">Hertfordshire</option>	
			<option value="kent">Kent</option>
			<option value="lon">London</option>
			<option value="oxford">Oxfordshire</option>	 
			<option value="surrey">Surrey</option>
			<option value="wSussex">West Sussex</option>
			</select>
			<select name="types" class="map_filters" id="types" onChange="speedTest.SelectType(this)">
			<option value="">All venues</option>
			<option value="civil">Civil ceremonies</option>
			<option value="large">Large (150+ seated)</option>
			<option value="huge">Huge events</option>
			<option value="not">Not expensive</option>
			<option value="medium">Medium price</option>
			<option value="expensive">Expensive</option>
			<option value="management">Event Management</option>
			</select>
			</div></div>
			</td></tr>
			
			</table>
			<div id="key">
			<p class="wide">All capacity figures are for maximum number of seated guests</p>
			</div>
		</div>

		
</div>


<?php get_footer(); ?>
	

