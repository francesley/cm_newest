<?php
/**
 * Template Name: Gallery List Page 
 * TODO - $category not sorted on remote
 */
get_header();
$filtering = "..."
?>
<section class="page-content">
	<div class="container">
		<div class="row header-wrapper">
			<div class="col-lg-6 col-md-8">
					<div class="post-title">
						<h1><?php 
						// Display Gallery page title or the Category title 
						$categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");
						$category = 0;

						if (isset($_GET['category'])) {
							
							$category = in_array($_GET['category'],$categories)?$_GET['category']:0;
						}
							if ($category != 0) {								
								$term_taxonomy = 'gallery-category';
								// Check if the term exists in the specified taxonomy
								$term = get_term_by('slug', $category, $term_taxonomy);
								$arr = explode(' ',trim($term -> name));
								$filtering = $arr[0];
								echo $term->name . " Marquee Photos ";	
															
							} else {
								echo get_the_title(); 								
							}?>
						</h1>
						<?php the_content(); ?>
					</div>			
			</div>
<div class="col-lg-3 col-md-4 col-sm-12 col-12">	
<div class="dropdown">
<a class="btn btn-class btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" >Filter by <span><?php echo $filtering; ?></span></a>
<ul class="dropdown-menu" >
<li><a class="dropdown-item" href="<?php echo get_permalink();?>">All</a></li>
					<?php
					// Get the terms for the "gallery-category" taxonomy
					//TODO highlight selected category
					$terms = get_terms('gallery-category');
					if ($terms && !is_wp_error($terms)) {
					foreach ($terms as $term) {
						$term_link = get_term_link($term); // Get the link for the term
						echo '<li><a class="dropdown-item" href="' .get_permalink().'?category='. strtolower($term->slug) .'">' . ucwords($term->slug) . '</a></li>';
					}
					}
?>
</ul>
</div>					
</div>
</div>
<div class="row gallery-list">
			<?php
			
			//$category = ($_GET['category']) ? $_GET['category'] : 1;
			if($category){
				//ShowDev( $category);
				$term_taxonomy = 'gallery-category';
				// Check if the term exists in the specified taxonomy
				$term = get_term_by('slug', $category, $term_taxonomy);
				if ($term !== false && !is_wp_error($term)) {
					// Term exists
					//echo 'Term found: ' . $term->term_id;

					$args = array(
						'post_type' => 'photos',
						'post_status' => 'publish',
						'posts_per_page' => 100,
						'orderby' => 'menu_order', // Use 'menu_order' for custom post types
						'order' => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => $term_taxonomy, // Use the taxonomy name
								'field' => 'term_id',
								'terms' => $term->term_id, // Use the term ID
							),
						),
					);


				} else {
					// Term does not exist
					echo 'Term not found';
				}
			} else {
				$args = array(
					'post_type' => 'photos',
					'post_status' => 'publish',
					'orderby' => 'menu_order',
					// Order by the publish date
					'order' => 'ASC',
					//'paged' => $paged
					'posts_per_page' => 100
					//'paged' => 1
				);

			}




			$the_query = new WP_Query($args);
			if ($the_query->have_posts()):
				while ($the_query->have_posts()):
					$the_query->the_post();

					?>
<div class="col-lg-3 col-md-4 col-sm-6 col-12">
<div class="post-item">
<a href="<?php if($category != 0){ echo get_the_permalink().'?category='. strtolower($term->slug);} else {echo get_the_permalink();} ?>">
<div class="post-item-img">
<img class="image" loading="lazy" src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'gallery-size-medium'); ?>" />
</div>
</a>
<a href="<?php the_permalink(); ?>">
<div class="post-text"><p>
<?php echo get_the_excerpt(); ?>
</p></div>
</a>
</div>
</div>
				<?php endwhile; ?>

			<?php else: ?>
				<!-- Your no post found code here -->
			<?php endif; ?>
		</div>
		<!--<div class="row"> TODO - TEST THIS OUT WHEN EVERYTHING IS READY. SEE WHAT OTHERS THINK. THERES CODE IN THEME-SCRIPT AND FUNCTIONS
        <div class="col-md-3">
          <div class="text-sm-end">
            php if ( $the_query->max_num_pages > 1 ) { ?>
            <a class="load-more btn btn-class dark"  data-post="photos" data-page="php if($_GET[ 'pg' ]){ echo $_GET[ 'pg' ];}else{ echo '1';}?>" href="#seemore"> Load more </a>
            php } ?>
          </div>
        </div>
      </div>-->
	</div>
</section>

<?php get_footer(); ?>