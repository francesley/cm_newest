<?php
/**
 * Template Name: Sites Page
 */
require_once( __DIR__ . '/includes/maps/GetVenues.php');
 get_header();
 $filtering = $post->post_name !== "sites"?$post->post_name:"...";
?>

<div id="outerBox" class="container">
<section class="banner-section venues">
	<div class="row">
		<div class="col-sm-12">
			<div class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>');">				
			</div>
			<ul id="subMenu"  class="menu-wrapper">
			<li class="menu_left"><a href="/venues.htm">Marquee venue map</a></li>
			<li class="menu_left selected"><a class="selected" href="/venues/sites.htm">Marquee venues lists</a></li>
			<li class="menu_right"><a href="/venues/add_venue.htm">Add a venue</a></li></ul>			
		</div>
	</div>
</section>

<section class="venue-section" id="sites">

		<div class="row">
		<div class="col-lg-8">

<h1 style="margin-bottom:4rem"><?php the_title(); ?></h1>


</div>
<div class="col-lg-4">

<div class="dropdown">
					<a class="btn btn-class btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Filter by  <span><?php echo $filtering; ?></span></a>
					<ul class="dropdown-menu">
					<li><a class="dropdown-item" href="/venues/sites.htm" class="selected">All counties</a></li>
					<li><a class="dropdown-item" href="/venues/oxford.htm" id="oxford" title="Marquee venues in Oxfordshire">Oxford</a></li>

					<li><a class="dropdown-item" href="/venues/bucks.htm" id="bucks" title="Marquee venues in Buckinghamshire">Bucks</a></li>

					<li><a class="dropdown-item" href="/venues/beds.htm" id="beds" title="Marquee venues in Bedfordshire">Beds</a></li>

					<li><a class="dropdown-item" href="/venues/herts.htm" id="herts" title="Marquee venues in Hertfordshire">Herts</a></li>

					<li><a class="dropdown-item" href="/venues/essex.htm" id="essex" title="Marquee venues in Essex">Essex</a></li>

					<li><a class="dropdown-item" href="/venues/berks.htm" id="berks" title="Marquee venues in Berkshire">Berks</a></li>
					<li><a class="dropdown-item" href="/venues/surrey.htm" id="surrey" title="Marquee venues in Surrey">Surrey</a></li>

					<li><a class="dropdown-item" href="/venues/lon.htm" id="lon" title="Marquee venues in London">London</a></li>

					<li><a class="dropdown-item" href="/venues/kent.htm" id="kent" title="Marquee venues in Kent">Kent</a></li>

					<li><a class="dropdown-item" href="/venues/hants.htm" id="hants" title="Marquee venues in Hampshire">Hants</a></li>

					<li><a class="dropdown-item" href="/venues/wSussex.htm" id="wSussex" title="Marquee venues in West Sussex">West Sussex</a></li>

					<li><a class="dropdown-item" href="/venues/eSussex.htm" id="eSussex" title="Marquee venues in East Sussex">East Sussex</a></li>
					</ul>
					</div>					
			</div>


</div>
			
			<section class="multi-column-section"> 
                           
                <div class="row">
                    <div class="col-12">
                                <div class="column-wrapper-3">


			
			
				
				<?php 
				 global $post;
			     $area = $post->post_name;			   			
				 $xml = simplexml_load_file(get_template_directory_uri().'/maps/markers.xml');
					  // var_dump($xml);
					  $counter = 0;
					  foreach($xml as $venue){
						
						if (is_page('sites')) { 
							GetVenue($venue,$counter);
							if($counter == 1){
								$counter = 0;	
							}else{
								$counter = $counter + 1; 
							}	
						} else { 
							if(strtolower($venue["area"]) == $area){
								GetVenue($venue,$counter);
								if($counter == 1){
									$counter = 0;	
								}else{
									$counter = $counter + 1; 
								}	
							}else{
								//skip the venue
							}
							
						}					  
						  						
					  
					  }


				 ?>
				</div>

				</section></div></div></div>


			</div>
		</div>

</section>
</div>

<?php get_footer(); ?>