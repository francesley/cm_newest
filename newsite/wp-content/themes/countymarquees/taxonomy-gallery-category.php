<?php
get_header();
?>
<section class="page-content">
	<div class="container">
		<div class="row header-wrapper">
			<div class="col-lg-6">
					<div class="post-title">
            <?php 
            $page_slug = 'photos'; 
            // Get the post object for the specific page using the slug
            $page = get_page_by_path($page_slug);

            if ($page) {
                $page_title = $page->post_title; // Get the page title
                $page_content = $page->post_content; // Get the page content
                $page_url = get_permalink($page->ID);
                // You can now use $page_title and $page_content as needed
            }
            ?>
						<h1><?php echo $page_title; ?></h1>
						<?php echo $page_content; ?>
					</div>			
			</div>
			<div class="col-lg-6">	
				<div class="dropdown">
        <?php 
        $term_taxonomy = 'gallery-category';
        $current_term = get_queried_object();
            if ($current_term) {
                $term_name = $current_term->name; // Get the term name
                $term_slug = $current_term->slug; // Get the term slug
                $term_id = $current_term->term_id; // Get the term ID
                $term_description = $current_term->description; // Get the term description
                $term_url = get_term_link($current_term); // Get the term URL
                // You can use these variables, including $term_url, as needed
            }
            ?>
					<a class="btn btn-class btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <?php echo $term_name;?>
					</a>
					<ul class="dropdown-menu">            
						<?php
						// Get the terms for the "gallery-category" taxonomy
						$terms = get_terms('gallery-category');
						if ($terms && !is_wp_error($terms)) {
						foreach ($terms as $term) {
							$term_link = get_term_link($term); // Get the link for the term
							echo '<li><a class="dropdown-item" href="' . esc_url($term_link) . '">' . $term->name . '</a></li>';
						}
						}
						?>
					</ul>
					</div>					
			</div>
		</div>
		<div class="row gallery-list">
			<?php
		
        $args = array(
          'post_type' => 'photos',
          'post_status' => 'publish',
          'posts_per_page' => 15,
          'orderby' => 'date',
          'order' => 'DESC',
          'tax_query' => array(
              array(
                  'taxonomy' => $term_taxonomy, // Use the taxonomy name
                  'field' => 'term_id',
                  'terms' => $term_id, // Use the term ID
              ),
          ),
      );

			$the_query = new WP_Query($args);
			if ($the_query->have_posts()):
				while ($the_query->have_posts()):
					$the_query->the_post();

					?>
					<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="post-item">
							<a href="<?php the_permalink(); ?>">
								<div class="post-item-img"
									style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
									<img class="image" src="<?php echo get_the_post_thumbnail_url(); ?>" />
								</div>
							</a>
							<a href="<?php the_permalink(); ?>">
								<div class="post-text">
									<?php the_excerpt(); ?>
								</div>
							</a>
						</div>
					</div>
				<?php endwhile; ?>

			<?php else: ?>
				<!-- Your no post found code here -->
			<?php endif; ?>
		</div>
	</div>
</section>
<?php 
get_footer();
?>
