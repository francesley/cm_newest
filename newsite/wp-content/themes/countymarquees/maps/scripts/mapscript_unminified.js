// function $(element) {
  // return document.getElementById(element);
// }

var speedTest = {};

speedTest.pics = null;
speedTest.map = null;
speedTest.markerClusterer = null;
speedTest.markers = [];
speedTest.infoWindow = null;
speedTest.sortmarker = [];
speedTest.pans = [];

//PANS These are the latlang coordinates for each of the english counties in the dropdown created above in function LabelControl. The map panned to the county when it was selected (before the gmaps code went to v2 and this stopped working)

speedTest.pans["surrey"] = new google.maps.LatLng("51.279958","-0.466919");

speedTest.pans["wSussex"] = new google.maps.LatLng("50.946315","-0.465546");

speedTest.pans["eSussex"] = new google.maps.LatLng("50.950641","0.269165");

speedTest.pans["beds"] = new google.maps.LatLng("52.146973","-0.477905");

speedTest.pans["lon"] = new google.maps.LatLng("51.521562","-0.127716");

speedTest.pans["hants"] = new google.maps.LatLng("51.12766","-1.224976");

speedTest.pans["essex"] = new google.maps.LatLng("51.806917","0.681152");

speedTest.pans["bucks"] = new google.maps.LatLng("51.839172","-0.810242");	  

speedTest.pans["herts"] = new google.maps.LatLng("51.825593","-0.240326");

speedTest.pans["oxford"] = new google.maps.LatLng("51.80352","-1.260681");

speedTest.pans["kent"] = new google.maps.LatLng("51.297993","0.854187");

speedTest.pans["berks"] = new google.maps.LatLng("51.510452","-1.19751");


speedTest.init = function() {
	if ($(window).width() < 813) {
	   var latlng = new google.maps.LatLng(51.23323, -0.124842);
	   var zoom = 8;
	}
	else {
	  var latlng = new google.maps.LatLng(51.53323, -0.124842);
	  var zoom = 8;
	}
 
  var options = {
    'zoom': zoom,
    'center': latlng,
    'mapTypeId': google.maps.MapTypeId.ROADMAP
  };

 var map = document.getElementById('map');
  speedTest.map = new google.maps.Map( map, options);

  speedTest.infoWindow = new google.maps.InfoWindow();

  speedTest.showMarkers();
};

speedTest.showMarkers = function() {
  speedTest.markers = [];

  var type = 1;

  if (speedTest.markerClusterer) {
    speedTest.markerClusterer.clearMarkers();
  }

  var panel = document.getElementById('side_bar');
  
  panel.innerHTML = '';

  //var numMarkers = $('nummarkers').value; 
  
// Read the data

$.ajax({
  type: 'GET',
  async: true,
  url: 'https://newsite.countymarquees.com/wp-content/themes/countymarquees/maps/markers.xml', //TODO fix url, ideally without identifying all folders. And other similar urls
  dataType: "xml",
  success:function(xml){
	    
		var markerstg = xml.documentElement.getElementsByTagName("marker");
		speedTest.sortmarker = markerstg;
		for (var i = 0; i < markerstg.length; i++) {
	  
	  		var lat = parseFloat(markerstg[i].getAttribute("lat"));

			var lng = parseFloat(markerstg[i].getAttribute("lng"));
			
			var title = parseFloat(markerstg[i].getAttribute("label"));
			
			var siteIcon = markerstg[i].getAttribute("icon");
	
						
			var titleText = markerstg[i].getAttribute("label");
			if (titleText === '') {
			  titleText = 'No title';
			}

			var item = document.createElement('DIV');
			var title = document.createElement('A');
			title.href = '#';
			title.className = 'title';
			title.innerHTML = titleText;

			item.appendChild(title);
			panel.appendChild(item);
			
			
			var MlatLng = new google.maps.LatLng(lat,lng);
				
			var imageUrl = 'https://www.countymarquees.com/maps/icons/';
			var imageHoverUrl = 'https://www.countymarquees.com/maps/icons/';
			
			switch (siteIcon) { 
				case 'house': 
					imageUrl = imageUrl + 'houseTiny.png';
					imageHoverUrl = imageHoverUrl + 'houseTinyOver.png';
					break;
				case 'hotel': 
					imageUrl = imageUrl + 'hotelTiny.png';
					imageHoverUrl = imageHoverUrl + 'hotelTinyOver.png';
					break;
				case 'field': 
					imageUrl = imageUrl + 'fieldSm.png';
					imageHoverUrl = imageHoverUrl + 'fieldOver.png';
					break;
				case 'school': 
					imageUrl = imageUrl + 'schoolSm.png';
					imageHoverUrl = imageHoverUrl + 'schoolOver.png';
					break;					
				case 'barn': 
					imageUrl = imageUrl + 'barnTiny.png';
					imageHoverUrl = imageHoverUrl + 'barnTinyOver.png';
					break;
				case 'sports': 
					imageUrl = imageUrl + 'sports.png';
					imageHoverUrl = imageHoverUrl + 'sportsOver.png';
					break;
				case 'castle': 
					imageUrl = imageUrl + 'castle.png';
					imageHoverUrl = imageHoverUrl + 'castleOver.png';
					break;
			    case 'restaurant': 
					imageUrl = imageUrl + 'restaurant.png';
					imageHoverUrl = imageHoverUrl + 'restaurantOver.png';
					break;
				default:
					imageUrl = imageUrl + 'houseTiny.png';
					imageHoverUrl = imageHoverUrl + 'houseTinyOver.png';
			}
			

			
			var markerImage = {
				url: imageUrl, // url
				scaledSize: new google.maps.Size(15, 14), // scaled size
				origin: new google.maps.Point(0,0), // origin
				anchor: new google.maps.Point(0, 0) // anchor
			};
			
			var markerHoverImage = {
				url: imageHoverUrl, // url
				scaledSize: new google.maps.Size(15, 14), // scaled size
				origin: new google.maps.Point(0,0), // origin
				anchor: new google.maps.Point(0, 0) // anchor
			};

			var marker = new google.maps.Marker({
			'position': MlatLng,
			'icon': markerImage
			});			
			
			
			var fn = speedTest.markerClickFunction(markerstg[i], MlatLng);			
			
			marker.setMap(speedTest.map);
			
			speedTest.markers.push(marker);
			
			
			google.maps.event.addDomListener(title, 'mouseover', function(marker) {
					var index = $(this).parent().index();
					//console.log(index);
					speedTest.markers[index].setIcon(markerHoverImage);
			});
			google.maps.event.addDomListener(title, 'mouseout', function(marker) {
					var index = $(this).parent().index();
					//console.log(index);
					speedTest.markers[index].setIcon(markerImage);		
			});
			
			google.maps.event.addListener(marker, 'click', fn);
			
			google.maps.event.addDomListener(title, 'click', fn);
			
			
		}

	  
  }
});

  
  window.setTimeout(speedTest.time, 0);
};

speedTest.markerClickFunction = function(pic, latlng) {
  //	var icon = pic.getAttribute("icon")

  return function(e) {
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) {
      e.stopPropagation();
      e.preventDefault();
    }
	
	
	
	
	
	
	        var details = pic.getElementsByTagName("details")[0].firstChild.nodeValue;

			var civil = pic.getAttribute("civil");

			var huge = pic.getAttribute("huge");

			var price = pic.getAttribute("price");

			var large = pic.getAttribute("large");

			var full = pic.getAttribute("full");

			var label = pic.getAttribute("label");
			
			var mposition = pic.getAttribute("position");

			var area = pic.getAttribute("area");
			

			var extras = "<div class='extras' style='width:300px;height:30px'>";

			extras += civil == "1"?"<p class='civil'>Civil ceremonies:</p>":"<p class='civil no'>Civil ceremonies:</p>";

			extras += large == "1"?"<p class='civil left'>Large marquee:</p>":"<p class='civil no left'>Large marquee:</p>";

			extras += price == "1"?"<p class='price'>Marquee price:</p>":(price == "2"?"<p class='price expensive'>Marquee price:</p>":"<p class='cheap'>Marquee price:</p>");

			extras += full == "1"?"<p class='civil left'>Event management:</p>":"<p class='civil no left'>Event management:</p>";

			extras += "</div>";

			//details = '<div class="map-info-wrapper" style="width:300px;height:210px;overflow:auto;">' + details + extras;//HERE HERE HERE
			details = details + extras;

			//details += "</div>";
	
	
	
	
	
	
	
	
	var infoHtml = details; //info window content
	 
	  
    speedTest.infoWindow.setContent(infoHtml);
	//speedTest.infoWindow.setOptions({ pixelOffset: new google.maps.Size(100, 100) });
    speedTest.infoWindow.setPosition(latlng);
	
		if(mposition=="top"){
		   speedTest.map.setCenter(latlng);		
		}
		if ($(window).width() < 813) {
			//speedTest.map.panBy(0, -75);
		}

	
	
    speedTest.infoWindow.open(speedTest.map);
  };
};

speedTest.clear = function() {
  //$('timetaken').innerHTML = 'cleaning...';
  for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
    marker.setMap(null);
  }
};

speedTest.change = function() {
  speedTest.clear();
  speedTest.showMarkers();
};

speedTest.time = function() { 
    for (var i = 0, marker; marker = speedTest.markers[i]; i++) { 
      //marker.setMap(speedTest.map);
	  marker.setMap(speedTest.map);
    }

};

speedTest.filterMarkers = function(catval,category) { //console.log(speedTest.sortmarker.length); return false;
 //alert('triggered');
  var newmarkers = [];
    var panel = document.getElementById('side_bar');
	panel.innerHTML = '';

  for (i = 0; i < speedTest.sortmarker.length; i++) {
	var price = speedTest.sortmarker[i].getAttribute("price");
	var areas = speedTest.sortmarker[i].getAttribute("area");
	var large = speedTest.sortmarker[i].getAttribute("large");
	var eventm = speedTest.sortmarker[i].getAttribute("full");
	var civil = speedTest.sortmarker[i].getAttribute("civil");
	
	switch(category){
		case 'price': 
			cat = price;
			break;
		case 'area': 
			cat = areas;
			break;
		case 'large': 
			cat = large;
			break;
		case 'eventm': 
			cat = eventm;
			break;		
		case 'civil': 
			cat = civil;
			break;					
		default:
			cat = price;
			break;
	}
	
	marker = speedTest.markers[i];
	var siteIcon = speedTest.sortmarker[i].getAttribute("icon");			
	var imageUrl = 'https://www.countymarquees.com/maps/icons/';
	var imageHoverUrl = 'https://www.countymarquees.com/maps/icons/';
	
	switch (siteIcon) { 
		case 'house': 
			imageUrl = imageUrl + 'houseTiny.png';
			imageHoverUrl = imageHoverUrl + 'houseTinyOver.png';
			break;
		case 'hotel': 
			imageUrl = imageUrl + 'hotelTiny.png';
			imageHoverUrl = imageHoverUrl + 'hotelTinyOver.png';
			break;
		case 'field': 
			imageUrl = imageUrl + 'fieldSm.png';
			imageHoverUrl = imageHoverUrl + 'fieldOver.png';
			break;
		case 'school': 
			imageUrl = imageUrl + 'schoolSm.png';
			imageHoverUrl = imageHoverUrl + 'schoolOver.png';
			break;					
		case 'barn': 
			imageUrl = imageUrl + 'barnTiny.png';
			imageHoverUrl = imageHoverUrl + 'barnTinyOver.png';
			break;
		case 'sports': 
			imageUrl = imageUrl + 'sports.png';
			imageHoverUrl = imageHoverUrl + 'sportsOver.png';
			break;
		case 'castle': 
			imageUrl = imageUrl + 'castle.png';
			imageHoverUrl = imageHoverUrl + 'castleOver.png';
			break;
		case 'restaurant': 
			imageUrl = imageUrl + 'restaurant.png';
			imageHoverUrl = imageHoverUrl + 'restaurantOver.png';
			break;
		default:
			imageUrl = imageUrl + 'houseTiny.png';
			imageHoverUrl = imageHoverUrl + 'houseTinyOver.png';
	}
	
	var markerImage = {
				url: imageUrl, // url
				scaledSize: new google.maps.Size(15, 14), // scaled size
				origin: new google.maps.Point(0,0), // origin
				anchor: new google.maps.Point(0, 0) // anchor
			};
			
	var markerHoverImage = {
				url: imageHoverUrl, // url
				scaledSize: new google.maps.Size(15, 14), // scaled size
				origin: new google.maps.Point(0,0), // origin
				anchor: new google.maps.Point(0, 0) // anchor
			};
	
    // If is same category or category not picked
    if (cat == catval || catval.length === 0) {
		
		var titleText = speedTest.sortmarker[i].getAttribute("label");
			if (titleText === '') {
			  titleText = 'No title';
			}

			var item = document.createElement('DIV');
			var title = document.createElement('A');
			title.href = '#';
			title.className = 'title';
			title.innerHTML = titleText;

			item.appendChild(title);
			panel.appendChild(item);
			
			var lat = parseFloat(speedTest.sortmarker[i].getAttribute("lat"));

			var lng = parseFloat(speedTest.sortmarker[i].getAttribute("lng"));
			
			var MlatLng = new google.maps.LatLng(lat,lng);
			
			var fn = speedTest.markerClickFunction(speedTest.sortmarker[i], MlatLng);
			
            marker.setVisible(true);
            newmarkers.push(marker);
	  
			google.maps.event.addDomListener(title, 'mouseover', function(marker) {
			var index = $(this).parent().index();
			speedTest.markers[index].setIcon(markerHoverImage);
			});
			google.maps.event.addDomListener(title, 'mouseout', function(marker) {
			var index = $(this).parent().index();
			speedTest.markers[index].setIcon(markerImage);		
			});

			google.maps.event.addListener(marker, 'click', fn);

			google.maps.event.addDomListener(title, 'click', fn);
			
			

    }
   // Categories don't match 
    else {
       marker.setVisible(false);
    }
  }
  //speedTest.markerCluster.clearMarkers();
  //speedTest.markerClusterer.clearMarkers();
  //speedTest.markerClusterer.addMarkers(newmarkers);
}


speedTest.SelectType = function(event) { 

    var newmarkers = [];
    var panel = document.getElementById('side_bar');
	panel.innerHTML = '';	
	var type = document.getElementById("types");	
    var selectedType = type.options[type.options.selectedIndex].value;
    var regionSelect = document.getElementById("area");
	var selectedRegion = regionSelect.options[regionSelect.options.selectedIndex].value;
	var cat="";
	var catval="";
	
	if(selectedRegion!==""){
		console.log('area changed');
		speedTest.map.panTo(speedTest.pans[selectedRegion]);
		speedTest.map.setZoom(9);
	}else{		
		speedTest.map.panTo(new google.maps.LatLng(51.53323, -0.124842));
		speedTest.map.setZoom(8);
	}
			
			
	 for (i = 0; i < speedTest.sortmarker.length; i++) {
		 
		var areas = speedTest.sortmarker[i].getAttribute("area");
		var siteIcon = speedTest.sortmarker[i].getAttribute("icon");	
		
		marker = speedTest.markers[i];			
		var imageUrl = 'https://www.countymarquees.com/maps/icons/';
		var imageHoverUrl = 'https://www.countymarquees.com/maps/icons/';
	
	switch (siteIcon) { 
		case 'house': 
			imageUrl = imageUrl + 'houseTiny.png';
			imageHoverUrl = imageHoverUrl + 'houseTinyOver.png';
			break;
		case 'hotel': 
			imageUrl = imageUrl + 'hotelTiny.png';
			imageHoverUrl = imageHoverUrl + 'hotelTinyOver.png';
			break;
		case 'field': 
			imageUrl = imageUrl + 'fieldSm.png';
			imageHoverUrl = imageHoverUrl + 'fieldOver.png';
			break;
		case 'school': 
			imageUrl = imageUrl + 'schoolSm.png';
			imageHoverUrl = imageHoverUrl + 'schoolOver.png';
			break;					
		case 'barn': 
			imageUrl = imageUrl + 'barnTiny.png';
			imageHoverUrl = imageHoverUrl + 'barnTinyOver.png';
			break;
		case 'sports': 
			imageUrl = imageUrl + 'sports.png';
			imageHoverUrl = imageHoverUrl + 'sportsOver.png';
			break;
		case 'castle': 
			imageUrl = imageUrl + 'castle.png';
			imageHoverUrl = imageHoverUrl + 'castleOver.png';
			break;
		case 'restaurant': 
			imageUrl = imageUrl + 'restaurant.png';
			imageHoverUrl = imageHoverUrl + 'restaurantOver.png';
			break;
		default:
			imageUrl = imageUrl + 'houseTiny.png';
			imageHoverUrl = imageHoverUrl + 'houseTinyOver.png';
	}
	
	var markerImage = {
				url: imageUrl, // url
				scaledSize: new google.maps.Size(15, 14), // scaled size
				origin: new google.maps.Point(0,0), // origin
				anchor: new google.maps.Point(0, 0) // anchor
			};
			
	var markerHoverImage = {
				url: imageHoverUrl, // url
				scaledSize: new google.maps.Size(15, 14), // scaled size
				origin: new google.maps.Point(0,0), // origin
				anchor: new google.maps.Point(0, 0) // anchor
			};
			
	
	switch (selectedType) {

		case "civil":
			cat = "civil";
			catval = 1;
			break;

		case "large":
			cat = "large";
			catval = 1;
			break;

		case "huge":
			cat = "huge";
			catval = 1;
			break;

		case "not":
			cat = "price";
			catval = 0;
			break;

		case "medium":
			cat = "price";
			catval = 1;
			break;

		case "expensive":
			cat = "price";
			catval = 2;
			break;
			
		case "management":
			cat = "full";
			catval = 1;
			break;

		default:
			cat = "civil";
			catval ="";
			break;

		}
		
		//cat = speedTest.sortmarker[i].getAttribute(cat);
    // If is same category or category not picked
    if ((selectedRegion == areas || selectedRegion == "") && (speedTest.sortmarker[i].getAttribute(cat) == catval || catval.length === 0) ) {
		
		var titleText = speedTest.sortmarker[i].getAttribute("label");
			if (titleText === '') {
			  titleText = 'No title';
			}

			var item = document.createElement('DIV');
			var title = document.createElement('A');
			title.href = '#';
			title.className = 'title';
			title.innerHTML = titleText;

			item.appendChild(title);
			panel.appendChild(item);
			
			var lat = parseFloat(speedTest.sortmarker[i].getAttribute("lat"));

			var lng = parseFloat(speedTest.sortmarker[i].getAttribute("lng"));
			
			var MlatLng = new google.maps.LatLng(lat,lng);
			
			var fn = speedTest.markerClickFunction(speedTest.sortmarker[i], MlatLng);
			
            marker.setVisible(true);
            newmarkers.push(marker);
	  
			google.maps.event.addDomListener(title, 'mouseover', function(marker) {
			var index = $(this).parent().index();
			speedTest.markers[index].setIcon(markerHoverImage);
			});
			google.maps.event.addDomListener(title, 'mouseout', function(marker) {
			var index = $(this).parent().index();
			speedTest.markers[index].setIcon(markerImage);		
			});

			google.maps.event.addListener(marker, 'click', fn);

			google.maps.event.addDomListener(title, 'click', fn);
    }
   // Categories don't match 
    else {
       marker.setVisible(false);
    }		
		 
	 }
	

}



getInfowindowOffset = function (map, marker) { console.log('triggered..!');
        var center = this.getPixelFromLatLng(map.getCenter()),
            point = this.getPixelFromLatLng(marker.getPosition()),
            quadrant = "",
            offset;
        quadrant += (point.y > center.y) ? "b" : "t";
        quadrant += (point.x < center.x) ? "l" : "r";
        if (quadrant == "tr") {
            offset = new google.maps.Size(-160, 250);
        } else if (quadrant === "tl") {
            offset = new google.maps.Size(160, 260);
        } else if (quadrant === "br") {
            offset = new google.maps.Size(-140, 30);
        } else if (quadrant === "bl") {
            offset = new google.maps.Size(160, 30);
        }
        return offset;
    };