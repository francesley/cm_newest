<?php
/**
 * Template Name: Pricing Page 
 */
require_once( __DIR__ . '/includes/pricing/GetPricing.php');
 get_header();
 //TODO FILTER DROPDOWN ONLY NEEDED FOR MARQUEE PRICES PAGES, DOWNLOAD BUTTON NEEDED FOR ALL PRICING PAGES

 /*$marquee_intro_para = "<div class='row'><div class='col-lg-6'>
	<p>Marquee prices are determined by size</p>
<p><strong>How big</strong> a marquee you will need depends on whether guests are <strong>seated or not</strong>, and <strong><em>extras</em></strong>. Our price lists include seated and buffet prices, and prices with and without simple flooring, lighting and lining.</p>
<p>Determine the size you will need from the <strong>Capacity</strong> columns in the tables below. Then look at the <strong>Total hire price</strong> columns on the right. This will be the rough cost</p>
<p>Another <strong>quicker way</strong> to get an idea of costs is to use our <a href='/marquee_essentials.html'>online marquee quote</a>. It is accurate, immediate and you can print or mail all quotes.</p>
<p>For other ways to work out the cost, see <a href='/marquee_essentials.html'>How much does it cost to hire a marquee</a>.</p>
<p> Or look at some <a href='sample.htm'>sample marquee prices</a> for examples of all-in prices</p>
<p>If you know what you would like, <a href='/pricing/quotation.htm'>request a no-obligation quotation</a> online.</p>
<p>For more help, please <a href='/contact_us.htm'>contact us</a>.</p></div></div>";*/
$marquee_intro_para = "<div class='col-lg-6'>
<p>Marquee prices are determined by size</p>
<p><strong>How big</strong> a marquee you will need depends on whether guests are <strong>seated or not</strong>, and <strong><em>extras</em></strong>. Our price lists include seated and buffet prices, and prices with and without simple flooring, lighting and lining.</p>
<p>Determine the size you will need from the <strong>Capacity</strong> columns in the tables below. Then look at the <strong>Total hire price</strong> columns on the right. This will be the rough cost</p></div>";
$dropdowns = "<div class='col-lg-4'>	
<div class='dropdown'>
<a class='btn btn-class btn-secondary dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Filter</a>
<ul class='dropdown-menu'>
<li><a class='dropdown-item' href='/pricing/marquees.htm'>All</a></li>
	<li>By size</li>						
	<li><a class='dropdown-item' href='/pricing/smaller_marquees.htm''>Small</a></li><li><a class='dropdown-item' href='/pricing/medium_marquees.htm'>Medium</a></li><li><a class='dropdown-item' href='/pricing/event_marquees.htm'>Large</a></li>
	<li>By type</li>
	<li><a class='dropdown-item' href='/pricing/frame_marquees.htm'>Frame</a></li><li><a class='dropdown-item' href='/pricing/traditional_marquees.htm'>Traditional</a></li><li><a class='dropdown-item' href='/pricing/chinese.htm'>Chinese hat</a></li>
	</ul>
</div>					
</div>
<div class='col-lg-2'>	
<div class='dropdown'>
<a class='btn btn-class btn-secondary dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Download</a>
<ul class='dropdown-menu'>
<li>Just prices</li>
<li><a class='dropdown-item' href='/pricing/marquees.htm'><img src='https://www.countymarquees.com/images/right/equipment_prices.jpg' /></a></li>
	<li>Full brochure</li>						
	<a class='dropdown-item' href='/pricing/marquees.htm'><img src='https://www.countymarquees.com/images/right/equipment_prices.jpg' /></a></li>
</ul>
</div>					
</div>
</div>";
$single_dropdown = "<div class='dropdown'>
<a class='btn btn-class btn-secondary dropdown-toggle' href='#' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Download</a>
<ul class='dropdown-menu'>
<li>Just prices</li>
<li><a class='dropdown-item' href='/pricing/marquees.htm'><img src='https://www.countymarquees.com/images/right/equipment_prices.jpg' /></a></li>
	<li>Full brochure</li>						
	<a class='dropdown-item' href='/pricing/marquees.htm'><img src='https://www.countymarquees.com/images/right/equipment_prices.jpg' /></a></li>
</ul>
</div>					
</div>";
$prices_done = 0;
?>

<?php
if (have_rows('page_section')):
	$i = 0;
    // Loop through sections
    while (have_rows('page_section')):
        the_row();
// This section is for the Top menu links 
		if (get_row_layout() == 'menu_section') {
			?>
			<section class="menu-section">  
			<div class="container">           
				<div class="row">
					<div class="col-12">
							<ul class="menu-wrapper">
									<?php
									if (have_rows('menu_list')):                                                    
										while (have_rows('menu_list')):
											the_row();
											$menu_title = get_sub_field('menu_title');
											$menu_link = get_sub_field('menu_link');
											?>                                                                
													<li class="<?php if(get_sub_field('currently_active')){echo "active";} ?>">
														<a href="<?php echo $menu_link; ?>">
															<?php echo $menu_title; ?>                                                                       
														</a>
													</li>                                                               
											<?php                                                       
										endwhile;
									endif;
									?>
							</ul>
						</div>
					</div>
				</div>
			</section>
<?php } 

if ($prices_done == 0) {

?>



<div id="outerBox" class="container">

<section class="pricing-section" id="pricing">

		<div class="row">
			
						
				<div class="col-lg-12">
			<h1><?php the_title(); ?></h1>
</div>
			</div>


			<div class="row">
				<?php 
				 global $post;
			     $price = $post->post_name;			   			
				 $xml = simplexml_load_file(get_template_directory_uri().'/xml/marquee_prices.xml');
				 
				$xpath = false; 	

				$class = "GetMarqueePriceList";

				
				
				
				
			switch ($price) {

					case "event_marquees":

						$class = "GetMarqueeSizePriceList";

						$xpath = "row[@size='large']";						

						$opener = "<div class='col-lg-6'><p>All sizes and styles of marquee for 300+ guests. Total price includes standard lighting and matting.</p></div>$dropdowns";

						break;

					case "smaller_marquees":

						$class = "GetMarqueeSizePriceList";

						$xpath = "row[@size='small']";						

						$opener = "<div class='col-lg-6'><p>All sizes and styles of marquee for less than 100 guests. Total price includes standard lighting and matting.</p></div>$dropdowns";

						break;

					case "medium_marquees":

						$xpath = "marquee[@size='medium']";						

						$opener = "<div class='col-lg-6'><p>All sizes and styles of marquee for 75&ndash;600 guests. Total price includes standard lighting and matting.</p></div>$dropdowns";

						break;

					case "traditional_marquees":

						$xpath = "marquee[@type='traditional']";		

						$opener = "<div class='col-lg-6'><p>Prices for traditional style marquees. Total price includes standard lighting and matting.</p></div>$dropdowns";

						break;

					case "frame_marquees":

						$xpath = "marquee[@type='frame']";						

						$opener = "<div class='col-lg-6'><p>Prices for clearspan frame marquees. Total price includes standard lighting and matting.</p></div>$dropdowns";

						break;

					case "equipment":

						$class = "GetEquipmentPriceList";

						$opener = "<div class='col-lg-10'><p>Prices for generators, windows, stages and other marquee extras.</p></div><div class='col-lg-2'>$single_dropdown</div>";												

						break;

					case "furniture":

						$class = "GetFurniturePriceList";

						$opener = "<div class='col-lg-10'><p>Prices for tables, chairs, dancefloors and other marquee furniture.</p></div><div class='col-lg-2'>$single_dropdown</div>";

						break;

					case "sample":

						$class = "GetSamples";

						$opener = "<div class='col-lg-8'><p>The following example quotes give likely costs for three typical marquee events. For individual advice, please <a href='/contact_us.htm'>contact us</a>. Or request your own free <a href='/pricing/quotation.htm'>personalised quote</a>.</p></div><div class='col-lg-4'>$single_dropdown</div>";

						//$right_include = $this -> INCLUDE_DIR . "pricing_samples.php";

						break;

					case "chinese":

						$xpath = "marquee[@type='chinese hat']";

						$opener = "<p>Prices for <em>chinese hat</em> or <em>pagoda</em> style marquees.</p>";						

						break;

					default:

						$class = "GetAllMarqueesPriceList";					

						$opener = $marquee_intro_para . $dropdowns;

						$xpath = "";

						break;

						

				}

				

					

				echo $content = "\n $opener \n<div id='pricetables'>\n" . $class($xpath);
					  
				$prices_done = 1;

						 }			 ?>
				</div>
			</div>
		

</section>

<?php

// Full Width Testimonial section with image background

if (get_row_layout() == 'full_width_single_testimonial') {
	$full_width_image = get_sub_field( 'full_width_image' );
	$testimonial_text = get_sub_field( 'testimonial_text' );
	$readmore_link = get_sub_field( 'readmore_link' );
	$readmore_button_text = get_sub_field( 'readmore_button_text' );
	?>
	<section class="fullwidth-testimonial-section" style="background-image:url('<?php echo $full_width_image['url'];?>');">
		 <!-- <img src="<?php echo $full_width_image['url']; ?>" alt="<?php echo $full_width_image['alt']; ?>" />                                 -->
			<div class="testimonial-wrapper">
			<?php echo $testimonial_text; ?>
			<?php if($readmore_link){?><a class="btn btn-class dark" href="<?php echo $readmore_link;?>"><?php if($readmore_button_text!=""){ echo $readmore_button_text; }else{ echo 'Read More'; }?></a><?php }?>
			</div>

			</div>
	</section>
<?php } 
endwhile;
endif; ?>

</div>

<?php get_footer(); ?>