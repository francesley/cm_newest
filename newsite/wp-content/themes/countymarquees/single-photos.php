<?php
/**
 * The template for displaying single photos *  
 * TODO - $category not sorted on remote
 */
get_header();
// Start the loop.
if (have_posts()):
   
    while (have_posts()):
        the_post();
        $categories = array("wedding","party","corporate","awkward","lighting","decoration","smaller","large","exterior","interior");
		$category = 0;

        if (isset($_GET['category'])) {
            $category = in_array($_GET['category'],$categories)?$_GET['category']:0;
        }
        ?>
        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="navigation">
                            <a href="javascript: history.go(-1)" role="link"> Back to <?php 
                            //$category = ($_GET['category']) ? $_GET['category'] : 1;
                            if($category != 0){								
                                $term_taxonomy = 'gallery-category';
                                // Check if the term exists in the specified taxonomy
                                $term = get_term_by('slug', $category, $term_taxonomy);
                                echo $term->name . " Gallery";								
                            } else {
                                $post_id = 404; //Gets Gallery page Title
                                $page_title = get_the_title($post_id);
                                echo $page_title;
                            } ?>
                             </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <div class="post-media-wrapper">
                            <div class="post-item-img">
                                <?php
                                // gets the 3 image sizes for the image uploaded , we can use crop plugin in the admin if we want to manually crop the image sizes in the admin
                                $image_id = get_post_thumbnail_id();
                               $large_image = wp_get_attachment_image_src( $image_id, 'gallery-size-large');
                                //$large_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full' );
                                $medium_image = wp_get_attachment_image_src( $image_id, 'gallery-size-medium');
                                $small_image = wp_get_attachment_image_src( $image_id, 'gallery-size-small');

                                $large_image_url = $large_image[0];
                                $medium_image_url = $medium_image[0];
                                $small_image_url = $small_image[0];
                                
                                ?>
                                    <!-- <img
                                    src="<?php echo esc_url($large_image_url); ?>"
                                    srcset="<?php echo esc_url($small_image_url); ?> 300w,
                                            <?php echo esc_url($medium_image_url); ?> 450w,
                                            <?php echo esc_url($large_image_url); ?> 620w"
                                    sizes="(max-width: 480px) 100vw, (max-width: 768px) 80vw, 620px"
                                    alt="<?php echo esc_attr(get_the_title()); ?>"
                                    height="415"
                                    width="620"
                                    /> -->

                                <picture>
                                <!-- Small Screen -->
                                <source srcset="<?php echo esc_url($small_image_url); ?>" media="(max-width: 480px)">

                                <!-- Medium Screen -->
                                <source srcset="<?php echo esc_url($medium_image_url); ?>" media="(max-width: 768px)">

                                <!-- Large Screen and Default -->
                                <source srcset="<?php echo esc_url($large_image_url); ?>" media="(min-width: 769px)">

                                <!-- Fallback for browsers that do not support the <picture> element -->
                                <img
                                src="<?php echo esc_url($large_image_url); ?>"
                                alt="<?php echo esc_attr(get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true )); ?>"
                                height="800"
                                width="1200"
                                />
                                </picture>

                                <?php // Code gets the next and previous post link
                                        $custom_post_type = 'photos';
                                        // Next link
                                        $next_post = get_adjacent_post(false, '', false);

                                        if ($next_post) {
                                            $next_permalink = get_permalink($next_post);
                                        } else {
                                            // If there's no next post, set the link to the first post
                                            $first_posts = get_posts(
                                                array(
                                                    'post_type' => $custom_post_type,
                                                    'posts_per_page' => 1,
                                                    'orderby' => 'date',
                                                    'order' => 'ASC',
                                                )
                                            );
                                            $next_permalink = get_permalink($first_posts[0]);
                                        }

                                        // previous link
                                        $previous_post = get_adjacent_post(false, '', true);
                                        if ($previous_post) {
                                            $previous_permalink = get_permalink($previous_post);
                                        } else {
                                            // If there's no previous post, set the link to the last post
                                            $last_posts = get_posts(
                                                array(
                                                    'post_type' => $custom_post_type,
                                                    'posts_per_page' => 1,
                                                    'orderby' => 'date',
                                                    'order' => 'DESC',
                                                )
                                            );
                                            $previous_permalink = get_permalink($last_posts[0]);
                                        }
                                        ?>

                                <a class="carousel-control-prev" href="<?php echo $next_permalink; ?>">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="<?php echo $previous_permalink; ?>">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="post-content">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="related-gallery-list">
                <?php // gets the other photo galleries
                $args = array(
                    'post_type' => 'photos',
                    'post_status' => 'publish',
                    'orderby' => 'date', //NO! TODO custom order
                    'order' => 'DESC',
                    'posts_per_page' => -1,
                    'post__not_in' => array(get_the_ID()),
                );

                $the_query = new WP_Query($args);
                if ($the_query->have_posts()):
                    while ($the_query->have_posts()):
                        $the_query->the_post();

                        

                        ?>
                        <div class="rel-post-item">
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-item-img">
                                    <?php
                                    // here we are only getting the thumb version of the image
                                    $thumb_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb-300x200');
                                    $thumb_image_url = $thumb_image[0];
                                    ?>
                                    <img class="image" loading="lazy" src="<?php echo $thumb_image_url; ?>" alt="<?php echo the_excerpt(); ?>" title="<?php echo the_excerpt(); ?>" />
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php else: ?>
                    <!-- Your no post found code here -->
                <?php endif; ?>
            </div>

        </section>
        <?php
        // End the loop.
    endwhile;
endif;
get_footer(); ?>