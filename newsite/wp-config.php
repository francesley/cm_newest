<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'countyma_wp');

/** MySQL database username */
define('DB_USER', 'root');

/*if ("DEVELOPMENT" === getenv("APPLICATION_ENV")) {

	$this -> DBARRAY = array('server'=>'localhost','user' => 'pma', "pw" => "5277ede5a7830", "name" => "harber_new");			

} 
elseif("DEV2" === getenv("APPLICATION_ENV")){
	
	$this -> DBARRAY = array('server'=>'localhost','user' => 'root', "pw" => "", "name" => "harber_new");	
	
}		
else {

	$this -> DBARRAY = array('server'=>'localhost','user' => 'dharber_admin', "pw" => "X%,H)tp$}KQXGmP;pv", "name" => "dharber_newsite");
	*/

/** MySQL database password NOT NEEDED ON LOCAL */
//define('DB_PASSWORD', 'sgbh1719');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^/I(kcFl>$b}=rnbgkdgA7/mE$LgLmA8nOPoc/i/uR}xIa>kz{>jh^d2aHIS=ioO');
define('SECURE_AUTH_KEY',  'nnjyS&^5Tt:r&XB06zVlWKr< <Q3C)]AL U7@r<gw!.0`-HoajA?)8F0%(Q</*=n');
define('LOGGED_IN_KEY',    'nj+l|@pAoOXx`A|Jd?n+VWzI.Xqyn(2mB> jLmR#RqSyXjA!GcR_ghGHsiE=VmQw');
define('NONCE_KEY',        '-o1`lMQ*W#1P{O03edB%(?v<u$/kEJ.b& 8*n{1%L:`05&,MbM%mA`P DT;U1hnB');
define('AUTH_SALT',        'V+F8j]Gw=<uoTYrQL(:SDOJo)!`x44zD;adR/$|T=zYN3-maR@I1erAqV_KOf~- ');
define('SECURE_AUTH_SALT', 'w&bFf=nh34}%V}Q`xo0;L]h?HVesI(8=)Vv&]-YNjQwx9*Ao-6gR;u`kd|)}5u0j');
define('LOGGED_IN_SALT',   ';]gV`O5QL=:JiOW^=kaW,2PH> OC?z86/[9w(gFt6p)#Y.!+~4U1|k?p/-`pe./;');
define('NONCE_SALT',       'lSZgGx<N{tJFe;LwSYny9v=~btMe%*c3+~!X*J@q+a[C+!;&iof[3j8||BUYai):');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

define( 'WP_POST_REVISIONS', 5 );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

// define( 'WP_DEBUG', true );

// define( 'WP_DEBUG_DISPLAY', true );

// define( 'WP_DEBUG_LOG', false );
