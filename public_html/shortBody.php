<?php
require_once( '../classes/errors.php' );
$mainMenu = new topMenu($request -> getProperty("cat"), $request -> getProperty("subtype"));
$mainMenu -> m_menu .= "</div>\n<div id='box'><div id='outerBox'>\n<div id='content'>";

//clarity
$clarity = <<<EOF
<script type="text/javascript">
    (function(c,l,a,r,i,t,y){
        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
    })(window, document, "clarity", "script", "jtp195fyzr");
</script>
EOF;

echo <<<EOF
<!doctype html>
$pageDetails->html_dec
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K2NFWFQ');</script>
<!-- End Google Tag Manager -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>$pageDetails->meta_title</title>
<meta name="description" content="$pageDetails->metadesc" />
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1.0">
<link type="text/css" rel="stylesheet" href="/css/cm_min.css" />
<link type="text/css" rel="stylesheet" href="/css/mobi.css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400&display=swap" />
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">
{$pageDetails -> headerExtra}
<link rel="SHORTCUT ICON" href="https://www.countymarquees.com/favicon.ico" />
<link href="/apple-touch-icon.png" sizes="57x57" rel="apple-touch-icon" />
<link href="/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72" />
<link href="/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114" />
<meta name="application-name" content="County Marquees"/>
<meta name="msapplication-TileColor" content="#E0E0CF"/>
<meta name="msapplication-TileImage" content="/cm_logo_tile.png"/>
$clarity
</head>
<body {$pageDetails -> bodyExtra}><div id="outerMain">
<div id="main" {$pageDetails -> main_class}>
<div id="top">
<a href="https://www.countymarquees.com/" id="logo"><img src="/images/logo.png" alt="Marquee hire from County Marquees" width="186" height="93"/></a>			
{$mainMenu -> m_menu}	 
EOF;
/*if (isset($subMenu) && $request -> getProperty("cat") !== "venues") { //SOME FORMS, PHOTOS DONT HAVE SUBMENU SO NEEDS TO BE LEFT IN	
	echo $mainMenu -> m_menu . $subMenu -> m_menu . "</div>\n<div id='box'><div id='outerBox'>\n<div id='content'>";
}*/
		
		
?>