<?php
ob_start("ob_gzhandler"); //TODO - think if i want this. makes it seem slower
error_reporting( E_ALL );
ini_set( "display_errors", 1 );
ini_set('date.timezone', 'Europe/London');
require_once( '../classes/functions.php');
require_once( '../classes/errors.php' );
require_once( '../classes/controller.php' );

CM_Controller::getInstance();
?>