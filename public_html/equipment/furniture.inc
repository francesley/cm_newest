
<!doctype html>
<html lang='en'>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K2NFWFQ');</script>
<!-- End Google Tag Manager -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marquee Flooring, Matting, Tables - Information about Marquee Furniture, County Marquees</title>
<meta name="description" content="Details of marquee furniture from County Marquees: marquee flooring, matting, tables, chairs..." />
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1.0">
<link type="text/css" rel="stylesheet" href="/css/cm_min.css" />
<link type="text/css" rel="stylesheet" href="/css/mobi.css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400&display=swap" />
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">
<script src='/scripts/search_min.js' defer></script>
<link rel="SHORTCUT ICON" href="https://www.countymarquees.com/favicon.ico" />
<link href="/apple-touch-icon.png" sizes="57x57" rel="apple-touch-icon" />
<link href="/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72" />
<link href="/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114" />
<meta name="application-name" content="County Marquees"/>
<meta name="msapplication-TileColor" content="#E0E0CF"/>
<meta name="msapplication-TileImage" content="/cm_logo_tile.png"/>

</head>
<body >
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K2NFWFQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="outerMain">
<div id="main" class="furniture2 real photos">
<div id="top">
<a href="https://www.countymarquees.com/" id="logo"><img src="/images/logo.png" alt="Marquee hire from County Marquees" width="186" height="93"/></a>			
<a id="skip" href="#content">Skip navigation</a>
<ul id="mainMenu"><li class='menu_left' ><a  href="/photos.htm" >GALLERY</a></li><li class='selected' ><a class="selected" href="/equipment/marquees.htm" >MARQUEES</a></li><li><a href="/wedding-marquees.htm">OCCASIONS</a></li><li><a href="/pricing/marquees.htm">PRICING</a></li><li><a href="/company/about.htm">ABOUT</a></li><li><a href="/company/jobs.htm">WORK FOR US</a></li><li><a href="/venues.htm">VENUES</a></li><li><a href="/contact_us.htm">CONTACT</a></li><li id='call' ><a  id='calllink' title='Call County Marquees' onclick="javascript:_gaq.push(['_trackEvent', 'Phone', 'top'])" href="tel:+442072674271" ><img src='/images/call.png' alt='Call County Marquees' width='24' height='24'></a></li><li class='menu_right' id='search' ><a  id='searchlink' title='Search' href="/search.htm" ><img src='/images/search_plain.png' alt='search' width='23' height='25'/></a></a><div id='searchbox'>
<div class='search_container'>
<form action='/search.htm' method='get'>
<input type='text' name='q'>			
<button type='submit'> <svg width='13' height='13' viewBox='0 0 13 13'><title>search</title><path d='m4.8495 7.8226c0.82666 0 1.5262-0.29146 2.0985-0.87438 0.57232-0.58292 0.86378-1.2877 0.87438-2.1144 0.010599-0.82666-0.28086-1.5262-0.87438-2.0985-0.59352-0.57232-1.293-0.86378-2.0985-0.87438-0.8055-0.010599-1.5103 0.28086-2.1144 0.87438-0.60414 0.59352-0.8956 1.293-0.87438 2.0985 0.021197 0.8055 0.31266 1.5103 0.87438 2.1144 0.56172 0.60414 1.2665 0.8956 2.1144 0.87438zm4.4695 0.2115 3.681 3.6819-1.259 1.284-3.6817-3.7 0.0019784-0.69479-0.090043-0.098846c-0.87973 0.76087-1.92 1.1413-3.1207 1.1413-1.3553 0-2.5025-0.46363-3.4417-1.3909s-1.4088-2.0686-1.4088-3.4239c0-1.3553 0.4696-2.4966 1.4088-3.4239 0.9392-0.92727 2.0864-1.3969 3.4417-1.4088 1.3553-0.011889 2.4906 0.45771 3.406 1.4088 0.9154 0.95107 1.379 2.0924 1.3909 3.4239 0 1.2126-0.38043 2.2588-1.1413 3.1385l0.098834 0.090049z'></path></svg> </button>
</form>
</div>
</div></li></ul>
</div>
<div id='box'><div id='outerBox'>
<div id='content'>	 <div id='pic'>
<img src='/images/top/furniture.jpg' width='680' height='323' id='mainPic'  alt='Trestle tables and gilt chairs' />
<div id='testi'>
<div><span id='testimonial'><a href='/company/testimonials.htm' title='Find out what other people say about us'>Many thanks to you and your team for a fantastic customer experience, start to finish. The marquee looked absolutely brilliant.</a></span><span>Redhill, Surrey</span></div>
</div>
<ul id='subMenu'><li class='menu_left' ><a  href="/equipment/marquees.htm" >Marquees</a></li><li class='selected' ><a class="selected" href="/equipment/furniture.htm" >Furniture</a></li><li><a href="/equipment/lighting.htm">Lighting</a></li><li><a href="/equipment/decoration.htm">Decoration</a></li><li><a href="/real.htm">Real marquees</a></li><li><a href="/marquee_questions.html">FAQs</a></li><li class='menu_right' ><a  href="/marquee_essentials.html" >Where to start?</a></a><div id='searchbox'>
<div class='search_container'>
<form action='/search.htm' method='get'>
<input type='text' name='q'>			
<button type='submit'> <svg width='13' height='13' viewBox='0 0 13 13'><title>search</title><path d='m4.8495 7.8226c0.82666 0 1.5262-0.29146 2.0985-0.87438 0.57232-0.58292 0.86378-1.2877 0.87438-2.1144 0.010599-0.82666-0.28086-1.5262-0.87438-2.0985-0.59352-0.57232-1.293-0.86378-2.0985-0.87438-0.8055-0.010599-1.5103 0.28086-2.1144 0.87438-0.60414 0.59352-0.8956 1.293-0.87438 2.0985 0.021197 0.8055 0.31266 1.5103 0.87438 2.1144 0.56172 0.60414 1.2665 0.8956 2.1144 0.87438zm4.4695 0.2115 3.681 3.6819-1.259 1.284-3.6817-3.7 0.0019784-0.69479-0.090043-0.098846c-0.87973 0.76087-1.92 1.1413-3.1207 1.1413-1.3553 0-2.5025-0.46363-3.4417-1.3909s-1.4088-2.0686-1.4088-3.4239c0-1.3553 0.4696-2.4966 1.4088-3.4239 0.9392-0.92727 2.0864-1.3969 3.4417-1.4088 1.3553-0.011889 2.4906 0.45771 3.406 1.4088 0.9154 0.95107 1.379 2.0924 1.3909 3.4239 0 1.2126-0.38043 2.2588-1.1413 3.1385l0.098834 0.090049z'></path></svg> </button>
</form>
</div>
</div></li></ul>
</div>


<h1>Marquee Furniture</h1>

<div class='thumbs'>
    
<div class='thumb' ><img src="/images/pages/furniture/round_with_chivaris.jpg" alt="Round tables with chivari chairs"  width='260' height='260'/><p>Round tables with chivari chairs</p></div>
<div class='thumb' ><img src="/images/pages/furniture/rustic_trestles.jpg" alt="Rustic trestle tables"  width='260' height='260' /><p>Rustic trestle tables</p></div>
<div class='thumb' ><img src="/images/pages/furniture/circular_bar.jpg" alt="Circular bar"  width='260' height='260' /><p>Circular bar</p></div>
<div class='thumb' ><img src="/images/pages/furniture/wood_dance_floor.jpg" alt="Simple wooden dance floor" width='260' height='260' /><p>Simple wooden dance floor</p></div>
<div class='thumb' ><img src="/images/pages/furniture/outdoor_heaters.jpg" alt="Outdoor heaters" width='260' height='260' /><p>Outdoor heaters</p></div>
<div class='thumb' ><img src="/images/pages/furniture/cross_backed_chairs.jpg" alt="Cross backed chairs with trestle tables"    width='260' height='260' /><p>Cross backed chairs with trestle tables</p></div>
</div>

<div id='text'  class='help' ><h2>General Furniture</h2><p>Marquee furniture and equipment available from County Marquees includes:</p>
    <div class="textPhotos byP">
    <img src="/images/pages/furniture/clear_roof_windows.jpg" alt="Clear roof and window panels"/>
    <p>Clear roof and window panels</p>
    <img src="/images/pages/furniture/poseur_tables.jpg" alt="Poseur tables and matching chairs" width="400" height="400"/>
    <p><em>Poseur</em> tables and matching chairs</p>
    <img src="/images/pages/furniture/white_bar.jpg" alt="Modern white bar" width="400" height="400" />
    <p>Modern white bar</p>
    </div>
    <ul class="long">
    <li>Round tables - 3', 4', 5', 5'6&quot;, 6'</li>
    <li>Trestle table - 6' x 2'3&quot;, 6' x 3'</li>
    <li>Vintage style trestle in rich, dark wood - 6' x 3'</li>
    <li>Low tables</li>
    <li>Wooden folding chairs </li>
    <li>Limewashed chiavari chairs in a range of colours</li>	
    <li>Poseur tables, caf&eacute; tables</li>
    <li>Banqueting chairs in gilt or other colours </li>
    <li>Mix and match boho chairs</li>
    <li>Plastic <em>bistro</em> chairs</li>    
    <li>Dance floors - wooden, parquet, black &amp; white and white starlight</li>    
    <li>Soft cube seating</li>
    <li>Sofas</li>
    <li>Bar units in different styles</li>
    <li>Stage units</li>
    <li>Coat rails</li>
    <li>Picket fencing</li>
    <li>Clear panoramic window panels</li>
    <li>Clear roof panels</li>    
    <li><em>Georgian</em> windows</li>
    <li>Hard doors</li>
    <li>Heaters</li>
    <li>Generators</li>
    <li>Portable toilets</li>
    </ul>
    <p><strong>See our price lists for <a href="/pricing/furniture.htm">furniture</a> and <a href="/pricing/equipment.htm">equipment</a></strong>.</p>
    <p >Other furniture and equipment can  easily be sourced if required.</p>
    <p >For more photo examples of County Marquees' furniture, see <a href="https://www.pinterest.com/county_marquees/marquee-furniture-equipment/" target="_blank" rel="noreferrer noopener">this marquee furniture album</a> on Pinterest.</p>
    <div class="unnec">&nbsp;</div>
    <h2>Dance floors:</h2>
    <div class="textPhotos">  
    <img src="/images/pages/furniture/starlight_dance_floor.jpg" alt="White starlight dance floor" width="400" height="400" class="second" loading="lazy" /><p>Starlight white dance floor</p>
    </div>
    <ul>
      <li><strong>Wooden dance floors</strong> &ndash; interlocking oak plywood. Standard dance floors, cover large areas easily. Shown above. Or see <a href="/photos/marquee-garden2.htm">this photo</a> or <a href="/photos/dance_lighting.htm">this photo</a> for examples.</li>
      <li><strong>Parquet dance floors</strong> &ndash; the classic look. See <a href="/photos/prewedding.htm">this photo</a> for a slightly covered up example.</li> 
      <li><strong>Black and white dance floors</strong> &ndash; dramatic, ideal when an event calls for extra <em>oomph</em>, particularly with lighting effects. These dance floors also work well in retro themed marquees. See <a href=/photos/disco.htm">this photo</a> for an example.</li>
      <li><strong>White starlight dance floors</strong> &ndash; shown here. They look both pretty and exciting so are an ideal dance floor for a wedding reception.</li>
    </ul>
    <div class="unnec"></div>
    <h2>Flooring:</h2>
    <div class="textPhotos">
    <img src="/images/pages/furniture/contract_carpet.jpg" alt="Marquee with champagne coloured contract carpet" width="400" height="400"  loading="lazy" /><p>Contract carpet in champagne colourway</p>
    </div>
    <ul>
    <li><strong>Coconut matting</strong>: Simple, stylish floor covering suitable 
    for informal summer events.</li>
    <li><strong>Hard floor</strong>:&nbsp;Made out of interlocking plywood panels, hard flooring
    is often covered by carpet. It is perfect for formal events.  Advisable for winter parties if
    protection against waterlogged ground is necessary.</li>
    <li><strong>Carpet: </strong>contract carpet in a wide range of colours is typically used to cover hard floors, but can overlay matting as well for a more formal style. Shown here in the popular champagne colourway. See <a href="/photos/asian.htm">this photo</a> of an Asian wedding for a larger example.</li>
    </ul>  
    <div class="unnec"></div>
    <h2>Furniture planning:</h2>
    <div class="textPhotos">
 <img src="/images/pages/furniture/immy.jpg" alt="Clear marquee with fabric overlays" width="400" height="400"  loading="lazy" /></a>
    <p class="view">Clear marquee with fabric overlays</p>
    </div>
    <p>County Marquees is happy to assist with planning the style and the layout of furniture.</p>
    <p>For many events, our standard furniture range is sufficient to create the right atmosphere &#8212; banqueting chairs with round tables for  formal functions, limewashed chiavari or bistro chairs with long trestle tables for more casual parties and so on.</p>
    <p >But County Marquees is also happy to source other furniture when necessary. Please <a href="../contact_us.htm">contact us</a> with any special requirements.</p>
    <p><strong>And please don't hesitate to <a href="/contact_us.htm">contact us</a> for more advice.</strong></p>
    
    </div>


</div>
<div class="lowertext">
<ul id="left_ul">
<li id="backtop"><a href="#outerMain">Back to top</a></li>
<li class="header">Useful</li>
<li><a href="/usefullinks.htm">Resources</a></li>
<li><a href="/company/link.htm">Link to us</a></li>
<li><a href="/siteMap.htm">Site map</a></li>
<li><a href="/company/privacy.htm">Privacy policy</a></li>
</ul>
<ul id="mid_ul">
<li class="header">Areas covered</li>
<li><a href="/areas/northlondon.htm">North London</a></li>
<li><a href="/areas/southlondon.htm">South London</a></li>
<li><a href="/areas/sussex.htm">Sussex</a></li>
<li><a href="/areas/surrey.htm">Surrey</a></li>
<li><a href="/areas/oxfordshire.htm">Oxfordshire</a></li>
<li><a href="/areas/kent.htm">Kent</a></li>
<li>Hampshire</li>
<li>Essex</li>
<li><a href="/areas/herts.htm">Hertfordshire</a></li>
<li>Berkshire</li>
<li>Bedfordshire</li>
<li><a href="/areas/bucks.htm">Buckinghamshire</a></li></ul>
<ul id="right_ul">
<li class="hometel">Telephone: <a href="tel:+442072674271"  onclick="javascript:_gaq.push(['_trackEvent', 'Phone', 'bottom'])"><strong>020 7267 4271</strong></a></li>
<li>&copy; <a href="https://www.countymarquees.com/">County Marquees</a> 2009 &#8211; 2024<br/>All rights reserved</li>
<li><div id="socialhead">
<a href="https://www.facebook.com/countymarquees" target="_blank" rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" loading="lazy" ></a>
<a href="https://twitter.com/County_Marquees" target="_blank" rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter" class="social" loading="lazy" ></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social" loading="lazy" ></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" loading="lazy" /></a>
</div></li>
</ul>
</div>
<div class="unnec"></div>
</div></div>
<div class="unnec">&nbsp;</div>
</div></div>

</body>
</html>