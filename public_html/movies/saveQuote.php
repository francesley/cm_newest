<?php
//HERE HERE HERE need to fix dupes; then sort out server not saving multiple marquees properly
require_once( '../../classes/errors.php' );
	$raw_xml = file_get_contents('php://input');
	$not_working_msg = "<Sorry><![CDATA[We are unable to request a quotation at this time. Please try again as this may be a temporary glitch. Otherwise, we apologise. You can still request a quote manually <a href='https://www.countymarquees.com//pricing/quote.htm'><u>here</u></a>]]</Sorry>";
	$spam_msg = "<Problem>We have been unable to save your details</Problem>";
	$details = "";
	$email = "";
	$theirName = "";
	$functionLoc = "";
	$functionDate = "";
	$comment = "";
	$savedQuotes = 0;
	$oldXmlFile = "";
	
	if (!$raw_xml) {
		echo $not_working_msg;
		exit;
		
	} else {
		//*******************************************
		//TODO: change url and need to change urls in flash too and line 218 below
		//*******************************************
		/*$test = $_SERVER['HTTP_REFERER'] == "http://www.countymarq.gradwell.net/movies/planner2marqueesNewer.swf,http://www.countymarq.gradwell.net/movies/planner2marqueesNewer.swf"
		|| $_SERVER['HTTP_REFERER'] == "http://www.countymarq.gradwell.net/movies/planner2marqueesNewer.swf"?true:false;*/
		$test = $_SERVER['HTTP_REFERER'] == "https://www.countymarquees.com/movies/planner2marqueesNewer.swf,https://www.countymarquees.com/movies/planner2marqueesNewer.swf"
		|| $_SERVER['HTTP_REFERER'] == "https://www.countymarquees.com/movies/planner2marqueesNewer.swf"?true:false;
		if (!$test) {
			echo $spam_msg;
			//echo "<Problem>$_SERVER[HTTP_REFERER]</Problem>";
			exit;
		}
		
		$xml = simplexml_load_string($raw_xml);
		
		if (!$xml) {
			echo $not_working_msg;
			//echo "<Problem>no xml</Problem>";
			exit;
		} else {	
			$result = CheckXML($xml );
			//return "<Got>this far</Got>";
			//something wrong with details - cos array returned if all ok, xml otherwise
			if (!is_array($result)) {
				echo $result;
			//save
			} else {
				$saveResult = SaveXML ($result["fileName"], $result["resave"], $xml);
				switch ($saveResult) {
					case "duplicate":
						echo "<Duplicate>You have already requested a quote.\n\nYou may request another one if you have not already requested two.\n\n</Duplicate>";
						break;
					case false:
						echo $not_working_msg;
						break;
					default:
						$encodedFileName = rawurlencode(base64_encode($result["fileName"]));
						//$decodedFileName = base64_decode(rawurldecode($encodedFileName));
						//$planURL = "http://www.countymarq.gradwell.net/movies/planner.html?plan=$encodedFileName";
						$planURL = GetPlanUrl ($encodedFileName);
						//SendMail($planURL, $result);
						if (SendMail($planURL, $result)) {
							//echo "<success>Friend:  $result[email]\nemail:  $result[friendsEmail]\nEncoded: $encodedFileName\nDecoded: $decodedFileName</success>";
							echo "<success><![CDATA[Your quotation request has been sent to County Marquees.\n\nWe will get back to you within two days.]]></success>";
						} else {
							echo $not_working_msg;
						}
				}	
				//echo "<success>Friend:  $result[email]\nemail:  $result[friendsEmail]\nfileName: $result[fileName]</success>";	
			}			
		}
	}
//lisa@designforprofit.co.uk

	function SaveXML ($fileName, $resave, $xml) {
	
		//--- load in xml file to edit.  if doesnt exist, create ---
			$xmlFile = "quoteXML/$fileName.xml";
			
			if (file_exists("/home/countymarque/webs/www.countymarq.com/htdocs/movies/$xmlFile")) {
				if ($resave == "0") {
					return "duplicate";
				}
			}
			
			$xmlHandle = fopen($xmlFile, "w");
			
			//--- set xml as string to be written ---
			$xmlString = $xml -> asXML();
			//echo($xmlString);
			
			//--- write xml to file ---
			$result = fwrite($xmlHandle, $xmlString);
			
			//--- close out xml file ---
			fclose($xmlHandle);

			return $result;
	}




	function CheckXML(SimpleXMLElement $xml ) {
		$resave = $xml -> resave[0];
		$details = $xml->personal[0];
		$theirName = $details -> theirName[0];		
		//$fileName = strtolower(str_replace(" ", "", $theirName));
		$fileName = "quote_" . generate_rand_filename(10);
		$functionLoc = $details -> functionLoc[0];
		$functionDate = $details -> functionDate[0];
		$tel = $details -> tel[0];
		$comment = $details -> comment[0];		
		$email = $details -> email[0];
		
		//TODO this doesnt work anymore - needs to stop more than 2 saves which presumably could do with resave variable
		if ($resave != "0") {
					
			$oldXmlFile = "quoteXML/" . $fileName . ".xml";
			$newXmlFile = "quoteXML/" . $fileName . "_2.xml";
			if (!file_exists("/home/countymarque/webs/www.countymarq.com/htdocs/movies/$oldXmlFile")) {
				$newXmlFile = $oldXmlFile;				
			} elseif (!file_exists("/home/countymarque/webs/www.countymarq.com/htdocs/movies/$newXmlFile")) {
				$savedQuotes = 1;
				$fileName = $fileName . "_2";
			} else {
				return "<TooMany>Sorry. You have already sent two quotation requests, which is the maximum allowed.\n\nCounty Marquees will contact you soon.</TooMany>";
			}				
			
		}
		
		
		//normal saves		
		if ( empty($email) || empty($theirName) || empty($tel) || empty($functionLoc) || empty($functionDate)){
			return "<Problem>Please fill in all required fields.</Problem>";
		} else if ( !CheckLength($email) || !CheckLength($theirName) || !CheckLength($tel) || !CheckLength($functionDate) || !CheckLength($comment, 300)) {  //TODO do this in flash
			return "<TextTooLong>The text you have entered is too long. Please try again.</TextTooLong>";
		} else if ( !remove_headers($email) || !remove_headers($theirName) || !remove_headers($tel) || !remove_headers($functionDate) || !remove_headers($functionLoc)) {
			return $spam_msg;
		//email checks
		} else if (!CheckEmail($email)) {
			return "<InvalidEmail>You have entered an invalid email. Please try again.</InvalidEmail>";
		/*} else if (!is_string($email)  || !is_string($theirName) || !is_string($tel) || !is_string($functionLoc) || !is_string($functionDate) {
			return "<InvalidCharacters>Something odd in your submission. Please try again using only normal characters.</InvalidCharacters>";*/
		} else if (strlen($theirName) < 10) { //TODO do this in flash
			return "<NameTooShort>Please ensure your name is at least 10 characters long</NameTooShort>";
		} else {		
			$result = array("email" => $email, "theirName" => $theirName, "tel" => $tel, "comment" => $comment, "functionLoc" => $functionLoc, "functionDate" => $functionDate, "fileName" => $fileName, "resave" => $resave);
			return $result;
		}
	}
	
	
	//utility functions
	function SendMail($planURL, $result) {
	//return true;
		$mailText = "";
		$result["theirName"] = ucfirst($result["theirName"]);
		$result["comment"] = empty($result["comment"])?"":"\nThey said: $result[comment]\n\n";
		if ($result["resave"] == "0") {
			$mailText = GetEmailText($planURL, $result);
		} else {
			$mailText = GetResavedEmailText($planURL, $result);
		}
		
		$email = $result["email"];
		//$davidEmail = "countymarquees@hotmail.com";
		$davidEmail = "countymarquees@hotmail.com";
		//$from = "From:\"" . $result["theirName"] . "\" <" . $email . ">";
		$from = "From:\"County Marquees\" <david@countymarquees.com>";
		
		//if ( mail($davidEmail, "Quote request from marquee plan", $mailText, $from) ) {
		if ( mail($davidEmail, "Quote request from marquee plan", $mailText, $from, "-f david@countymarquees.com") ) {
			mail("fdl4712@aol.com", "Quote request from marquee plan", $mailText, $from, "-f david@countymarquees.com");
			return true;
			
		} else {
			//echo "<MailNotSent>Friend email: $friendEmail</MailNotSent>";
			return false;
		}
			
	}
	
	
	function GetEmailText($url, $result) {
		return "Hello David\n\nSomebody wants a quote from a marquee plan.\n\nYou can access their plan here:\n$url\n\nTheir details are:\nName: $result[theirName]\nEmail: $result[email]\nTel: $result[tel]\nFunction location: $result[functionLoc]\nFunction Date: $result[functionDate]$result[comment]\n\nYours\n\nThe website\nhttps://www.countymarquees.com";	
	}
	function GetResavedEmailText($url,$result) {
			return "Hello David\n\nSomebody has requested an second quote from a marquee plan.\n\nYou can access their latest plan here:\n$url\n\nHopefully, you have the details of the old one because its too complicated to work it out.\n\nAnyway, their details are:\nName: $result[theirName]\nEmail: $result[email]\nTel: $result[tel]\nFunction location: $result[functionLoc]\nFunction Date: $result[functionDate]$result[comment]\n\nYours\n\nThe website\nhttps://www.countymarquees.com";
	}
	
	function CheckLength($str, $allowed = 40) {
		return strlen($str) > $allowed?false:true;
	}

	function CheckEmail($email) {
		if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
			return false;
		} else {
			return true;
		}	
	}
	
	function remove_headers($string) { 
	  $headers = array(
		"/to\:/i",
		"/from\:/i",
		"/bcc\:/i",
		"/cc\:/i",
		"/Content\-Transfer\-Encoding\:/i",
		"/Content\-Type\:/i",
		"/Mime\-Version\:/i" 
	  );
	  $string = strtolower($string);
	  if (preg_replace($headers, '', $string) == $string) {
	  	if (eregi("\r", $string) || eregi("\n", $string)) { 
			return false;
		}
		return true;
	  } else {
		return false;
	  }
	}
	
		/** Function: generate_rand_filename
	* Param1: $1 - Generates a random string thats as long as param $l given.
	**/
	function generate_rand_filename($length)	{
		//Allowed characters//
		$c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$rand = "";
		//Seed the random number generator//
		srand((double)microtime()*1000000);
		//Loop upto $l times and add a character to the string each time//
		for ($i=0; $i<$length; $i++) {
			$rand .= $c[rand()%strlen($c)];
		}
		
		//Return your new string//
		return $rand;
	}
	
	function GetPlanUrl ($encodedPlan) {
		return "https://www.countymarquees.com/movies/planner.html?quote=$encodedPlan";
	}

?>
