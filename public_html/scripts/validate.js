function Validate()	{
var formErrors = true;
var nextMove  = false;
var curr;
var currLength;
var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
$('.reqfield',document.forms[0]).each(function(){
//curr = $(this).attr("name");
//currLength = $(this).val().length;
//console.log("%d length: %d", curr, currLength);
if (!$(this).val().length) {
var errorDiv = $(this).parent().next();
$(this).focus(function() {
errorDiv.css("display","none");
});
errorDiv.css("display","block");
//alert($(this).next());
formErrors = false;
}
});	
if(!pattern.test($("#tbEmail").val())) {
var tbEmail = $("#tbEmail");
var emailError = $("#emailError");
tbEmail.focus(function() {
emailError.css("display","none");
tbEmail.unbind('focus');
})
emailError.css("display","block");
formErrors = false;
}
//these could be combined
var rbs = $(".rbgroup").length;
if ($(".rbgroup").length) {
var rbgroup = $(".rbgroup");
if ( ! VAlrb (rbgroup) ) {
SetUpRBsErrorDiv (rbgroup);
formErrors = false;
}
}
if ($(".rb1").length) {
var rbgroup1 = $(".rb1");
if ( ! VAlrb (rbgroup1) ) {
SetUpRBsErrorDiv (rbgroup1);
formErrors = false;
}
}
if ($(".rb3").length) {
var rbgroup3 = $(".rb3");
if ( ! VAlrb (rbgroup3) ) {
SetUpRBsErrorDiv (rbgroup3);
formErrors = false;
}
}
/*if ($('#ddPrice').val() == '0') {
$('#ddPrice').change(function() {
$('#ddError').css("display","none");
});
$('#ddError').css("display","block");
//alert($(this).next());
formErrors = false;
}*/
//var test = formErrors == false?"false":"true";
//console.log(test);
return formErrors;
}
function SetUpRBsErrorDiv (rbgroup) {
//console.log("in setuprb...");
var errorDiv = $(rbgroup.get(1)).parent().next();
//console.log(errorDiv);
rbgroup.change(function(){
errorDiv.css("display","none");			
});
errorDiv.css("display","block");
}
function VAlrb (rbgroup) {
var checked = false;
rbgroup.each ( function(){
if ($(this).attr('checked')) {
//console.log("checked");
checked = true;
}			
});
//console.log("returning checked which is ", checked);
return checked;
}
function PrepareForm() {
$("#contactSubmit").click(function(){
var res;
res = Validate();
return res;
});
}
$(document).ready(PrepareForm);