function addListener(element,type,expression,bubbling){bubbling=bubbling||false;if(window.addEventListener){element.addEventListener(type,expression,bubbling);return true;}else if(window.attachEvent){element.attachEvent('on'+type,expression);return true;}else return false;}
function itHappened(evt){var tg=(window.event)?evt.srcElement:evt.target;if(tg.nodeName=='A'){if(tg.href.indexOf(location.host)==-1&&tg.href.indexOf("javascript")==-1){var str=tg.href.replace(/[^a-z|A-Z]/g,"_");try{_gaq.push(['_trackEvent', 'Links', str])}
catch(err){}}}}
addListener(document,'click',itHappened);