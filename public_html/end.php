<?php
require_once( '../classes/errors.php' );

	$this_year = date("Y");
$google = <<<EOF
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-287928-3']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
EOF;


echo <<<EOF
</div>
<div class="lowertext">
<ul id="left_ul">
<li id="backtop"><a href="#outerMain">Back to top</a></li>
<li class="header">Useful</li>
<li><a href="/usefullinks.htm">Resources</a></li>
<li><a href="/company/link.htm">Link to us</a></li>
<li><a href="/siteMap.htm">Site map</a></li>
<li><a href="/company/privacy.htm">Privacy policy</a></li>
</ul>
<ul id="mid_ul">
<li class="header">Areas covered</li>
<li><a href="/areas/northlondon.htm">North London</a></li>
<li><a href="/areas/southlondon.htm">South London</a></li>
<li><a href="/areas/sussex.htm">Sussex</a></li>
<li><a href="/areas/surrey.htm">Surrey</a></li>
<li><a href="/areas/oxfordshire.htm">Oxfordshire</a></li>
<li><a href="/areas/kent.htm">Kent</a></li>
<li>Hampshire</li>
<li>Essex</li>
<li><a href="/areas/herts.htm">Hertfordshire</a></li>
<li>Berkshire</li>
<li>Bedfordshire</li>
<li><a href="/areas/bucks.htm">Buckinghamshire</a></li></ul>
<ul id="right_ul">
<li class="hometel">Telephone: <a href="tel:+442072674271" ><strong>020 7267 4271</strong></a></li>
<li>&copy; <a href="https://www.countymarquees.com/">County Marquees</a> 2009 &#8211; $this_year<br/>All rights reserved</li>
<li><div id="socialhead">
<a href="https://www.facebook.com/countymarquees" target="_blank" rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" loading="lazy" ></a>
<a href="https://twitter.com/County_Marquees" target="_blank" rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter" class="social" loading="lazy" ></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social" loading="lazy" ></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" loading="lazy" /></a>
</div></li>
</ul>
</div>
<div class="unnec"></div>
</div></div>
<div class="unnec">&nbsp;</div>
{$this -> summary}</div></div>
{$pageDetails -> footerExtra}
</body>
</html>
EOF;
?>