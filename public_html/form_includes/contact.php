<?php
echo <<<EOQ
<p>For information, assistance, a free site visit or brochure, please fill out the short <a href="#formStart">form</a> below. Or telephone or email us using the contact details on the right</p>
<p>Please be aware that County Marquees normally only supplies marquees to the South East UK</p>
<form method="post" name="contactForm" action="contact_us.htm" id="contactUsForm">
<div class="error summary" {$this -> styleFormResponse}>{$this -> formResponse}</div>
<fieldset>
<legend>1. Your details</legend>
<label for="tbName" class='first'><span class="required">*</span>Name: <input name="tbName" value="{$fc -> formName}" id="tbName" class="reqfield"/></label>
<div class="error" {$fc -> styleName}>Required</div>
<label for="tbEmail"><span class="required">*</span>Email: <input name="tbEmail" value="{$fc -> formEmail}" id="tbEmail"  class="reqfield"/></label>
<div class="error" {$fc -> styleEmail}>Required</div><div class="error" id="emailError" {$fc -> styleValidEmail}>Invalid email address</div>
<label for="tbTel"><span class="required">*</span>Telephone: <input name="tbTel" value="{$fc -> formTel}" id="tbTel"  class="reqfield"/></label>
<div class="error" {$fc -> styleTel}>Required</div>
<label for="ddHowheard" >Heard about us:
<select name="ddHowheard" id="ddHowheard">
{$fc -> howheardDD}
</select>
</label>
</fieldset>
<fieldset>
<legend>2. Event details</legend>
<label for="tbDate">Event date: <input type="text" name="tbDate" value="{$fc -> formDate}"  id="tbDate" /></label>
<label for="tbNumbers" class="extra" style="margin-left:-65px;">Numbers expected: <input type="text" name="tbNumbers" value="{$fc -> formNumbers}" id="tbNumbers"/><span class="end">(approximate)</span></label>
<label for="tbFunction"  class="extra" style="margin-left:-65px;"><span class="required">*</span>Event location: <input name="tbFunction" value="{$fc -> formFunction}" id="tbFunction" /><span class="end">(approximate)</span></label>
<div class="error" {$fc -> styleFunction}>Required</div>
<label class="last" for="ddType_of_function" >Event type:
<select name="ddType_of_function" id="ddType_of_function">
{$fc -> eventsDD}
</select>
</label>
</fieldset>
<fieldset class="noborder">
<legend>3. Help us to help you</legend>
<label for="cbBrochure" class="cb">Brochure request:<input name="cb[]" type="checkbox" class="cb" {$fc -> Wantsbrochure} value="Wants brochure" id="cbBrochure"/></label>
<label for="cbAdvice" class="cb right">Need advice:<input name="cb[]" type="checkbox" class="cb right" {$fc -> Wantsadvice} value="Wants advice" id="cbAdvice"/></label>
<label for="cbQuote" class="cb">Personalised quote:<input name="cb[]" type="checkbox" class="cb" {$fc -> Wantsquote} value="Wants quote" id="cbQuote"/></label>
<label for="cbPhone" class="cb right">Like phone call:<input name="cb[]" type="checkbox" class="cb right" {$fc -> Wouldlikephonecall} value="Would like phone call" id="cbPhone"/></label>
<label for="msg" class="ta"><span><span class="required">*</span>Message:</span><textarea name="tbMessage" rows="10"  cols="10" id="msg" class="reqfield">{$fc -> formMessage}</textarea></label>
<div class="error" {$fc -> styleMessage}>Required</div>
</fieldset>
<input name="submitted" type="hidden" value="true" />
<label for="contactSubmit" class="submit"><input name="contactSubmit" id="contactSubmit" type="submit" value="Send your enquiry" /></label>
<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a></p>
<div class="unnec"></div>
</form>
</div>
EOQ;



?>

