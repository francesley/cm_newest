<?php

//Util::ShowTrue(isset($_COOKIE["staff"]), "auth cookie");
//Util::ShowTrue($fc -> staff, "formchecker staff var");
//var_dump(session_get_cookie_params());

$opener = <<<EOQ
<p>Would you like an rough idea of how much your marquee will cost?</p>
<p>Enter your details for an immediate online marquee hire quote. You can experiment with marquees and furniture to check out prices. You can also print out or email yourself all quotes.</p>
<!--<p>Online quotes are real prices made from the marquee you specify below. But they sometimes make some assumptions about average requirements. For more information, <a href="/contact_us.htm">contact us</a>. Or, if you have a clear idea of what you would like, <a href="/pricing/quotation.htm">request a personalised quote.</a></p>-->
EOQ;
$mailextra = "";
$mailInstructions = "Enter your details to receive your quote by email:";
$formType = "normal";

if ($fc -> staff) {
	$opener = <<<EOQ
	<p>Just generate your quote as normal and then click the email button. You will get options there.</p>
EOQ;
	$mailextra = <<<EOQ
	<label for="mailddStaff">You *:
	<select name="mailddStaff" id="mailddStaff">
		<option>David Higgs</option> 
<option>Matthew Higgs</option>
<option>Matthew Higgs Surrey</option> 
<option>Shaun Haworth</option> 
<option>Chris Haworth</option> 
	</select>
	</label>
	<label>Opening paragraph *:<em id="staffinstructions"><strong>Instructions</strong>: accept what is here or edit it; whatever text you use is followed by this:<br/>
	&quot;If you have any questions or would like to take things further, please reply to this email or call me on <span style="color:#999">***your tel***</span> &quot;</em><textarea name="tfMailExtra" id="tfMailExtra" rows="15" columns="20">I include the marquee quote you requested from County Marquees.\n\nFor your convenience, I have used our online quote tool which gives you a number of options.\n\n</textarea>
	
	
	</label>
EOQ;
	$mailInstructions = "Enter the details to send a quote:";
	$formType = "staff";
}



echo <<<EOQ

<noscript><p class="error">Sorry. The online marquee quote application only works in browsers that have javascript enabled. You currently do not. You can still request an ordinary  quotation from us <a href="/pricing/quotation.htm">here</a>, but you will need to have a pretty clear idea about your venue and what sort of marquee you would like.</p></noscript>

$opener

<form id="quoteform">
<div class="error summary"></div>

<fieldset>
<legend>1. Marquee size</legend>
<h4>* One of these required</h4>
<h4 class="either">Either:</h4>
<label class="extra" style="width:481px">Guest numbers:<input type="text" name="tbNumbers" id="tbNumbers" value="50" class="reqfield"/><span class="end">(approximately)</span></label>

<h4 class="either">Or:</h4>
<label  class="extra" style="width:481px" for="ddWidth">Width:
	<select name="ddWidth" id="ddWidth">
		{$fc -> widthDD}
	</select>
	<span class="end">(if selected, can&#8217;t <br/>choose numbers)</span>
</label>
<label for="ddLength">Length:
	<select name="ddLength" id="ddLength">
		{$fc -> lengthDD}
	</select>
	
</label>
<div class="error" id="eitheror">Either guest numbers or marquee dimensions required</div>
</fieldset>

<fieldset>
<legend>2. Choose</legend>
<h4><span class="required">*</span>Seated event?</h4>
<label class='rb'>Yes <input name="rbSeated[]" type="radio" value="1" class="rb1" /></label>
<label class='rb rightrb'>No <input name="rbSeated[]" type="radio" value="0" class="rb1" /></label>
<div class="error" id="errseated">Seated events require more space so are more expensive.<br/>Please select one option.</div>
</fieldset>

<fieldset class="noborder">
<legend>3. Simple yes or no</legend>
<h4 style="padding-top:0;margin-bottom:1em;">Check any that apply:</h4>
<label class="cb">Area for a buffet?: <input id="cbBuffet" name="cb[]" type="checkbox" class="cb"  value="Buffet" /></label>
<label class="cb right">Area for a bar?: <input id="cbBar" name="cb[]" type="checkbox" class="cb"  value="Bar" /></label>
<label class="cb">Hard flooring: <input name="cb[]" type="checkbox" id="cbHard" class="cb" value="Hard" /></label>
<label class="cb right">Small stage: <input id="cbStage" name="cb[]" type="checkbox" class="cb" value="Stage"/></label>
<label  class="cb">Wood dancefloor: <input id="cbDance" name="cb[]" type="checkbox" class="cb" value="Dance floor" /></label>
<label  class="cb right">Parquet/black &amp; white dancefloor: <input id="cbParquet" name="cb[]" type="checkbox" class="cb" value="Parquet" /></label>
<label class="cb">Catering tent: <input id="cbCatering" name="cb[]" type="checkbox" class="cb" value="Catering tent" /></label>
<label class="cb right">Starlight ceiling over dance floor: <input id="cbStarlightDance" name="cb[]" type="checkbox" class="cb" value="Starlight dancefloor" /></label>
<label class="cb">Portable toilets: <input id="cbToilet" name="cb[]" type="checkbox" class="cb"  value="Toilets" /></label>
<label  class="cb right">Starlight ceiling throughout: <input id="cbStarlight" name="cb[]" type="checkbox" class="cb" value="Starlight" /></label>
<label class="cb">Generator: <input id="cbGenerator" name="cb[]" type="checkbox" class="cb"  value="Generator" /></label>
</fieldset>

<label class="submit"><input id="contactSubmit" type="submit" value="Get approximate marquee price" disabled="disabled"/></label>
<div class="unnec"></div>
</form>
<div id="hider"><div class="unnec"></div></div>
</div>
<div id="response"></div>
<div id="emaildetails">
	<p>$mailInstructions</p><a class='close' title='Close this'>\n<img width='12' height='12' alt='Close' src='/images/close.png' /></a>
	<!--TODOTODOTOD get rid of values below ************************************************************************-->
  <label>Name *:<input type="text" name="tbMailName" id="tbMailName" value="lisa" /></label>
  <label>Email *:<input type="text" name="tbMail" id="tbMail" value="fdl4712@aol.com"/></label>
  $mailextra
  <label class="submit"><input id="mailSubmit" type="submit" value="Email my quote"/></label>
  <input name="mailType" type="hidden" value="$formType"/>
  <p class="privacy">We respect your <a href="/company/privacy.htm">privacy</a></p>
</div>
EOQ;

?>