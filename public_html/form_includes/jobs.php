<?php


echo <<<EOQ
<p>We currently have vacancies for <strong>marquee riggers in Surrey or London</strong> to erect marquees at outdoor events. You will work as part of a close-knit team travelling all over the South East to build and take down marquees at venues of all kinds.</p>
<p>Apply by filling in the <strong><a href="#JobsForm">form below</a></strong>, or emailing your CV with a covering letter to David Higgs at <strong><a href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a></strong>.</p>

<h2>Job description</h2>
<ul>
<li><strong>Pay:</strong> Competitive hourly rate</li>
<li><strong>Hours:</strong> Flexible hours depending on bookings. In the marquee industry, flexibility is a must. Days are long in the high season. Weekend work is sometimes required.</li>
<li><strong>Reports to:</strong> Event foreman</li>
<li><strong>Responsibilities:</strong> To work as part of an experienced team erecting and taking down marquee structures on site; to pick and load marquees and equipment at our yard and reload on site at the end of a job; to clean equipment.</li>
<li><strong>Experience:</strong> welcome, but not necessary</li>
<li><strong>You will need to be</strong>: hardworking, practical, punctual and enthusiastic. You will also be the sort of person who enjoys working as part of a team. We pride ourself on the quality of our marquees and service and you will need to maintain that by ensuring your work is carried out to the highest standard.</li>
<li><strong>Desirable:</strong> Driving license</li>
</ul>

<div class="tom">
<h2>Working for County Marquees</h2>
<h3>Tom Reeves' Story</h3>
<div class="two-col-d-flex">
<div class="d-col-1">
<p>In 2004 I was a 23 year old, bored of my 9-5 marketing job. I was in need of a change but unsure which route to take, I decided to try something completely different. I enjoyed my sports and keeping fit so I thought I’d look for a physical job.
I picked up a local paper and flicked through the job section. One advert caught my eye – it read <strong>Marquee Erectors Wanted</strong>. It was a fairly local firm so I thought I’d give it a go.</p>
<h3>Tom Reeves at work <span class='hide-mob'>&gt;&gt;</span></h3>
 </div>
 <div class="d-col-2">
    <div style="height: 0;padding-bottom: 350px;position: relative;margin-bottom: 3em;"><iframe src="https://player.vimeo.com/video/682317629?h=166fef4668" width="640" height="1131" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="" style="width: 100%;height: 100%;position: absolute;top: 0;left: 0;"></iframe></div>
</div>
</div>
<p>A couple of days later, I met someone called Matthew at a barn in Godalming. After an hour of him pointing at things asking me to try lifting them, he offered me the job. I wasn’t sure what I was getting myself into but he seemed a nice enough chap and I was keen to give it a go so I accepted.</p>
<p>On my first day of work for County Marquees I arrived and met the rest of the team. There was another new guy called Alex starting also, which took the pressure off a little. We drove up to North London and erected a marquee for a wedding. It was hard work but I was impressed with what we produced in a day. </p>
<p>That summer flew by. There was a good group of lads working and the weather was great. It was the best of both worlds being outside, keeping fit, having a laugh and getting paid for it!</p>
<p>Some of the key factors of me enjoying the job so much was its creativity, and that no job was the same, and the trust you were given to put your own stamp on things.</p>
<h3>…16 Years On</h3>
<p>I am now the Foreman of County Marquees, I have a wife, 2 children and 2 dogs. My life has changed in many ways since my first day but I still love doing what I do.</p>
<p>I have had the privilege to work with many people from all different backgrounds. I have particularly enjoyed nurturing students who come to us with very limited skills.</p>
<p>We are approaching another busy season and I am looking forward to meeting some new faces and passing on some of my knowledge and skills.</p>
</div>

<h2>Application form</h2>
<form method="post" name="contactForm" action="/company/jobs.htm" id="JobsForm">
<div class="error summary" {$this -> styleFormResponse}>{$this -> formResponse}</div>
<fieldset>
<legend>1. Your details</legend>
<label for="tbName"><span class="required">*</span>Name: <input name="tbName" value="{$fc -> formName}" id="tbName" class="reqfield"/></label>
<div class="error" {$fc -> styleName}>Required</div>
<label for="tbEmail"><span class="required">*</span>Email: <input name="tbEmail" value="{$fc -> formEmail}" id="tbEmail"  class="reqfield"/></label>
<div class="error" {$fc -> styleEmail}>Required</div><div class="error" {$fc -> styleValidEmail}>Invalid email address</div>
<label for="tbTel"><span class="required">*</span>Telephone: <input name="tbTel" value="{$fc -> formTel}" id="tbTel"  class="reqfield"/></label>
<div class="error" {$fc -> styleTel}>Required</div>
<label for="tbAdress"><span class="required">*</span>Address: <input name="tbAdress" value="{$fc -> formAdress}" id="tbAdress"  class="reqfield"/></label>
<div class="error" {$fc -> styleAdress}>Required</div>
<label for="tbPostcode"><span class="required">*</span>Postcode: <input name="tbPostcode" value="{$fc -> formPostcode}" id="tbPostcode"  class="reqfield"/></label>
<div class="error" {$fc -> stylePostcode}>Required</div>
<label for="tbCounty">County: <input name="tbCounty" value="{$fc -> formCounty}" id="tbCounty" /></label>
<h4><span class="required">*</span>Current driving license?</h4>
<label class='rb'>Yes <input name="rbDL[]"  type="radio" value="got driving license" class="cb rbgroup" {$fc -> gotdrivinglicense} /></label>
<label class='rb rightrb'>No <input name="rbDL[]" type="radio" value="no driving license" class="cb rbgroup" {$fc -> nodrivinglicense} /></label>
<div class="error" {$fc -> styleDL}>Driving license?</div>
<h4><span class="required">*</span>Own transport?</h4>
<label class='rb'>Yes <input name="rbOT[]" type="radio" value="got own transport" class="rb1"  {$fc -> gotowntransport}/></label>
<label class='rb rightrb'>No <input name="rbOT[]" type="radio" value="no transport" class="rb1" {$fc -> notransport}/></label>
<div class="error" {$fc -> styleOT}>Do you have your own transport?</div>
<h4><span class="required">*</span>Do you need a work permit?</h4>
<label class='rb'>Yes <input name="rbWP[]" type="radio" value="Needs work permit" class="rb3" {$fc -> Needsworkpermit}/></label>
<label class='rb rightrb'>No <input name="rbWP[]" type="radio" value="Dont need work permit" class="rb3" {$fc -> Dontneedworkpermit}/></label>
<div class="error" {$fc -> styleWP}>Need a work permit?</div>
</fieldset>
<fieldset class="noborder">
<legend>2. Your message</legend>
<label for="msg" class="ta"><span><span class="required">*</span>Message:</span><textarea name="tbMessage" rows="7"  id="msg" cols="10"class="reqfield">{$fc -> formMessage}</textarea></label>
<div class="error" {$fc -> styleMessage}>Required</div>
</fieldset>
<input name="submitted" type="hidden" value="true" />
<label for="contactSubmit" class="submit"><input name="contactSubmit" id="contactSubmit" type="submit" value="Please send" /></label>
<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a>.</p>
<div class="unnec"></div>
</form>
</div>
EOQ;









?>

