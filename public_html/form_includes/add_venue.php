<?php

echo <<<EOQ

<p>Add any marquee venues in the South East UK and we will add it promptly to the marquee venue map.</p>
<p>This service is completely free for both customers and venue providers. But if you would like to link to County Marquees or the Marquee Venue Finder, there are codes to cut and paste onto your web page for both at the <a href="#bottomlinks">bottom</a> of this page.</p>

<form method="post" name="venueForm" action="/venues/add_venue.htm" id="AddVenueForm">
<div class="error summary" {$this -> styleFormResponse}>{$this -> formResponse}</div>
<fieldset>
<legend>Personal details</legend>
<label for="tbName"><span class="required">*</span>Name: <input name="tbName" value="{$fc -> formName}" id="tbName" class="reqfield"/></label>
<div class="error" {$fc -> styleName}>Required</div>

<label for="tbEmail"><span class="required">*</span>Your email:<input name="tbEmail" value="{$fc -> formEmail}" id="tbEmail" class="reqfield"/></label>
<div class="error" {$fc -> styleEmail}>Required</div><div class="error" {$fc -> styleValidEmail}>Invalid email address</div>
</fieldset>

<fieldset class="noborder">
<legend>Venue details</legend>
<label for="tbVName"><span class="required">*</span>Venue name: <input name="tbVName" value="{$fc -> formVName}" id="tbVName" class="reqfield" /></label>
<div class="error" {$fc -> styleVName}>Required</div>
<label for="tbVTel"><span class="required">*</span>Venue tel: <input name="tbVTel" value="{$fc -> formVTel}" id="tbVTel" class="reqfield" /></label>
<div class="error" {$fc -> styleVTel}>Required</div>
<label for="tbVWebsite"><span class="required">*</span>Venue website: <input name="tbVWebsite" value="{$fc -> formVWebsite}" id="tbVWebsite" class="reqfield" /></label>
<div class="error" {$fc -> styleVWebsite}>Required</div>
<label for="tbVPostcode" style="width: 390px;margin-left: -15px;"><span class="required">*</span>Venue postcode: <input name="tbVPostcode" value="{$fc -> formVPostcode}" id="tbVPostcode" class="reqfield" /></label>
<div class="error" {$fc -> styleVPostcode}>Required</div>

<label for="tbVDesc" style="width:541px;margin-left:-5px;" class="ta"><span><span class="required">*</span>Description:</span><span class="instructions">Please include:<br/>&bull; Max. numbers of seated guests in marquee<br/>&bull; Brief description of marquee site(s)<br/>&bull; Supplier policy/event planning offered</span><textarea name="tbVDesc" id="tbVDesc" rows="7"  cols="10" class="reqfield" >{$fc -> formVDesc}</textarea></label>
<div class="error errorclear" {$fc -> styleVDesc}>Required</div>

<label for="cbCivil" class="cb" style="clear: none;padding-top:5px;width:132px">Civil licence? <input name="cbCivil" type="checkbox" id="cbCivil" class="cb" value="Civil ceremonies" {$fc -> Civilceremonies}/></label>
<label for="tbCCnums" style="clear: none;width:238px">If so, for how many?: <input name="tbCCnums" value="{$fc -> formCCnums}" id="tbCCnums" style="width:30px"/></label> 

<label for="ddPrice" class="dd"  style="width:54ox;margin-left:-7px;"><span style="padding-left:62px"><span class="required">*</span>Price:</span>
	<span class="end">Approximate. Please specify below whether this is for:<br/>&bull; ground hire<br/>&bull; ground hire &amp; marquee<br/>&bull; all-in package.</span>
	<select name="ddPrice" id="ddPrice">
        {$fc -> formPrice}
      </select>
	  <span class="error" id="ddError" style="margin-top:10px;clear:none;padding-left:3px">Required</span>
</label>

<label for="tbMessage" class="ta extra"><span><span>Other?</span></span><span class="instructions">eg. close to a church? conference facilities? views? anything you would like to highlight</span><textarea name="tbMessage" rows="7"  id="tbMessage" cols="10">{$fc -> formMessage}</textarea></label>
</fieldset>
<input name="submitted" type="hidden" value="true" />
<label for="contactSubmit" class="submit"><input name="contactSubmit" id="contactSubmit" type="submit" value="Add a venue" onsubmit="return false"/></label>
<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a></p>
<div class="unnec"></div>
</form>
<h2 id="bottomlinks">Link codes:</h2>
<p>If you would like to link to County Marquees or the marquee venue finder, please cut and paste the code from the text boxes below.</p>
<p>Link code for County Marquees home page:</p>
<input name="tbCMlink" value="{$fc -> CMlink}" id="tbCMlink" class="links"/>
<p>Link code for marquee venue finder:</p>
<input name="tbVenueLink" value="{$fc -> VenueLink}" id="tbVenueLink"  class="links bottom"/>

</div>
EOQ;


?>
