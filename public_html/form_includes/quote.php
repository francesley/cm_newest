<?php



//Util::ShowTrue(isset($_COOKIE["staff"]), "auth cookie");

//Util::ShowTrue($fc -> staff, "formchecker staff var");

//var_dump(session_get_cookie_params());



$opener = <<<EOQ
<p>Would you like an rough idea of how much your marquee will cost?</p>
<p>Enter your details for an immediate online marquee hire quote. You can also print out or email yourself all quotes.</p>
EOQ;

$mailextra = "";

$mailInstructions = "Enter your details to receive your quote by email:";

$formType = "normal";



echo <<<EOQ
<noscript><p class="error">Sorry. The online marquee quote application only works in browsers that have javascript enabled. You currently do not. You can still request an ordinary  quotation from us <a href="/pricing/quotation.htm">here</a>.</p></noscript>
$opener
<form id="quoteform">
<div class="error summary"></div>
<fieldset>
<legend>1. Marquee size</legend>
<h4>* One of these required</h4>
<h4 class="either">Either:</h4>
<label style="width:481px">Guest numbers:<input type="text" name="tbNumbers" id="tbNumbers" class="reqfield"/><span class="end">(approximately)</span></label>
<h4 class="either">Or:</h4>
<label style="width:490px" for="ddWidth">Width:
	<select name="ddWidth" id="ddWidth">
		{$fc -> widthDD}
	</select>
	<span class="end">(if selected, can&#8217;t <br/>choose numbers)</span>
</label>
<label for="ddLength" style="width:382px">Length:
	<select name="ddLength" id="ddLength">
		{$fc -> lengthDD}
	</select>
</label>
<div class="error" id="eitheror">Either guest numbers or marquee dimensions required</div>
</fieldset>
<fieldset>
<legend>2. Choose</legend>
<h4><span class="required">*</span>Seated event?</h4>
<label class='rb'>Yes <input name="rbSeated[]" type="radio" value="1" class="rb1" /></label>
<label class='rb rightrb'>No <input name="rbSeated[]" type="radio" value="0" class="rb1" /></label>
<div class="error" id="errseated">Seated events require more space so are more expensive.<br/>Please select one option.</div>
</fieldset>
<fieldset class="noborder">
<legend>3. Simple yes or no</legend>
<h4 style="padding-top:0;margin-bottom:1em;">Check any that apply:</h4>
<label class="cb">Area for a buffet?: <input id="cbBuffet" name="cb[]" type="checkbox" class="cb"  value="Buffet" /></label>
<label class="cb right">Area for a bar?: <input id="cbBar" name="cb[]" type="checkbox" class="cb"  value="Bar" /></label>
<label class="cb">Hard flooring: <input name="cb[]" type="checkbox" id="cbHard" class="cb" value="Hard" /></label>
<label class="cb right">Small stage: <input id="cbStage" name="cb[]" type="checkbox" class="cb" value="Stage"/></label>
<label  class="cb">Wood dancefloor: <input id="cbDance" name="cb[]" type="checkbox" class="cb" value="Dance floor" /></label>
<label  class="cb right">Parquet/black &amp; white dancefloor: <input id="cbParquet" name="cb[]" type="checkbox" class="cb" value="Parquet" /></label>
<label class="cb">Catering tent: <input id="cbCatering" name="cb[]" type="checkbox" class="cb" value="Catering tent" /></label>
<label class="cb right">Starlight ceiling over dance floor: <input id="cbStarlightDance" name="cb[]" type="checkbox" class="cb" value="Starlight dancefloor" /></label>
<label class="cb">Portable toilets: <input id="cbToilet" name="cb[]" type="checkbox" class="cb"  value="Toilets" /></label>
<label  class="cb right">Starlight ceiling throughout: <input id="cbStarlight" name="cb[]" type="checkbox" class="cb" value="Starlight" /></label>
<label class="cb">Generator: <input id="cbGenerator" name="cb[]" type="checkbox" class="cb"  value="Generator" /></label>
<label class="cb right">Heater: <input id="cbHeater" name="cb[]" type="checkbox" class="cb"  value="Heater" /></label>
</fieldset>



<label class="submit"><input id="contactSubmit" type="submit" class="mbquote" value="Get approximate marquee price" disabled="disabled"/></label>

<div class="unnec"></div>
</form>
<div id="hider"><div class="unnec"></div></div>
</div>
<div id="response"></div>
<div id="emaildetails">
	<p>$mailInstructions</p><a class='close' title='Close this'>\n<img width='12' height='12' alt='Close' src='/images/close.png' /></a>
  <label>Name *:<input type="text" name="tbMailName" id="tbMailName" value="" /></label>
  <label>Email *:<input type="text" name="tbMail" id="tbMail" value=""/></label>
  $mailextra
  <label class="submit"><input id="mailSubmit" type="submit" value="Email my quote"/></label>
  <input name="mailType" type="hidden" value="$formType"/>
  <p class="privacy">We respect your <a href="/company/privacy.htm">privacy</a></p>
</div>
EOQ;



?>