<?php

echo <<<EOQ

<form method="post" name="contactForm" action="/pricing/quotation.htm" id="QuotationForm">
<div class="error summary" {$this -> styleFormResponse}>{$this -> formResponse}</div>
<fieldset>
<legend>1. Your details</legend>
<label for="tbName"><span class="required">*</span>Full Name: <input name="tbName" value="{$fc -> formName}" id="tbName" class="reqfield"/></label>
<div class="error" {$fc -> styleName}>Required</div>
<label for="tbEmail"><span class="required">*</span>Email: <input name="tbEmail" value="{$fc -> formEmail}" id="tbEmail" class="reqfield"/></label>
<div class="error" {$fc -> styleEmail}>Required</div><div class="error" {$fc -> styleValidEmail}>Invalid email address</div>
<label for="tbTel"><span class="required">*</span>Telephone: <input name="tbTel" value="{$fc -> formTel}" id="tbTel" class="reqfield"/></label>
<div class="error" {$fc -> styleTel}>Required</div>
<label for="tbAdress"><span class="required">*</span>Address: <input name="tbAdress" value="{$fc -> formAdress}" id="tbAdress" class="reqfield"/></label>
<div class="error" {$fc -> styleAdress}>Required</div>
<label for="tbPostcode"><span class="required">*</span>Postcode: <input name="tbPostcode" value="{$fc -> formPostcode}" id="tbPostcode" class="reqfield"/></label>
<div class="error" {$fc -> stylePostcode}>Required</div>
<label for="ddHowheard" >Heard about us: 
	<select name="ddHowheard" id="ddHowheard">
        {$fc -> howheardDD}
      </select>
</label>
</fieldset>
<fieldset>
<legend>2. Event details</legend>
<label>Event date: <input type="text" name="tbDate" value="{$fc -> formDate}"/></label>
<label style="width:465px" class="extra" ><span class="required">*</span>Guest numbers:<input type="text" name="tbNumbers" value="{$fc -> formNumbers}" class="reqfield"/><span class="end">(approximate)</span></label>
<div class="error" {$fc -> styleNumbers}>Required</div>
<label for="tbFunction" >Event location: <input name="tbFunction" value="{$fc -> formFunction}" id="tbFunction" /><span class="end"></span></label>
<label for="ddType_of_function" >Event type: 
	<select name="ddType_of_function" id="ddType_of_function">
        {$fc -> eventsDD}
      </select>
</label>
<h4>Marquee size if known:</h4>
<label for="ddWidth">Width:
	<select name="ddWidth" id="ddWidth">
		{$fc -> widthDD}
	</select>
</label>
<input type="hidden" name="tbWidthCheck" value="{$fc -> widthCheck}"/>
<label for="ddLength">Length:
	<select name="ddLength" id="ddLength">
		{$fc -> lengthDD}
	</select>
</label>
<div class="error length" {$fc -> styleLength}>Please choose a length</div>
</fieldset>
<fieldset>
<legend>3. Simple yes or no</legend>
<h4 style="padding-top:0">Check any that apply:</h4>
<label for="cbSeated" class="cb">All guests seated: <input name="cb[]" type="checkbox" id="cbSeated" class="cb" value="Seated event" {$fc -> Seatedevent}/></label>
<label for="cbDance"  class="cb right">You need a dancefloor: <input id="cbDance" name="cb[]" type="checkbox" class="cb" value="Wants dance floor" {$fc -> Wantsdancefloor}/></label>
<label for="cbStage" class="cb">You need a stage: <input id="cbStage" name="cb[]" type="checkbox" class="cb" value="Wants stage" {$fc -> Wantsstage}/></label>
<label for="cbCatering"  class="cb right">Catering tent: <input id="cbCatering" name="cb[]" type="checkbox" class="cb" value="Wants catering tent" {$fc -> Wantscateringtent}/></label>
<label for="cbBuffet" class="cb">Area for a buffet?: <input id="cbBuffet" name="cb[]" type="checkbox" class="cb"  value="Need buffet area" {$fc -> Needbuffetarea}/></label>
<label for="cbBar"  class="cb right">And/or for a bar?: <input id="cbBar" name="cb[]" type="checkbox" class="cb"  value="Need bar area" {$fc -> Needbararea}/></label>
</fieldset>
<fieldset class="noborder">
<legend>4. Anything else we ought to know?</legend>
<p class="instructions">Like if you want a sit down meal for some guests, and then more guests for a dance party later; or extras like starlit ceilings, hard floors, generators or portable toilets &#8212; anything that might make a difference to the cost of your marquee.</p>
<label for="msg" class="ta"><textarea name="tbMessage" rows="15"  cols="10" id="msg">{$fc -> formMessage}</textarea></label>
</fieldset>
<input name="submitted" type="hidden" value="true" />
<label for="contactSubmit" class="submit"><input name="contactSubmit" id="contactSubmit" type="submit" value="Get no-obligation quote" /></label>
<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a></p>
<div class="unnec"></div>
</form>
</div>
EOQ;



?>