<?php
echo <<<EOQ
<p>County Marquees supplies four types of marquee, each suited to different occasions. We have a large range of <em>frame marquees</em>, which can be put to different uses.</p>
<p>Our marquees range in width from 4 metres (14 foot) to 15 metres (50 foot) and can be pretty much as long as you like, increasing in 10 foot increments. All marquees except for traditional structures can be combined to make the best use of your space.</p>
<ul class="facets">
<li><span>Marquee types</span></li>
<li><span>Frame marquees</span></li>
</ul>
<div id="choices">
<ul class="float">
<li><a href="#size" id="topChoice"><img src="/images/little_marquees/frame.gif" alt="Frame marquee" align="left"/><br/><strong>Frame marquees:</strong><br/>No inner poles, good use of space, flexible</a></li>
<li><a href="#hire" id="midChoice"><img src="/images/little_marquees/trad.gif" alt="Traditional marquee" align="left"/><br/><strong>Traditional tents:</strong><br/>Romantic looking</a></li>
<li class="bottom"><a href="#marq"><strong>Chinese hat marquees:</strong><br/>Pointy topped for chill out areas or entrances</a></li>
</ul>
<ul class="float" style="margin-right:0;">
<li><a href="#accessories" id="topChoice"><strong>Event marquees:</strong><br/>Frame marquees for large events</a></li>
<li><a href="#weather" id="midChoice"><strong>Semi-permanent marquees</strong><br/>Frame marquees for storage or small spaces</a></li>
<li><a href="#hire"><strong>House extension:</strong><br/>Frame marquees attached to a building</a></li>
<li><a href="#marq"><strong>Catering tents</strong></a></li>
<li class="bottom"><a href="#marq"><strong>Walkways</strong></a></li>
</ul>
</div>
  
<div id="frame" class="faq">
<h2>Frame marquees</h2>
 <p>Framed marquees are a modern style of marquee. Instead of centre poles and ropes, these tents are supported by an aluminum skeleton around the edge - illustrated in the photos alongside.</p>
  <h3>Features of framed marquees:</h4>
  <ul >
    <li><strong>Clear span</strong>: no central pole, which means clear space inside the marquee and an unobstructed view for speeches.</li>

    <li><strong>Portable:</strong> can be secured to the ground with weights instead of pegs  (though pegs are used where possible  and  are advisable in severe winds). So can  be erected on hard surfaces.</li>
    <li><strong>Good use of space:</strong> no guy ropes  make frame marquees  suitable for town gardens as they can make the maximum use of space. See <a href="../photos/marquee-garden.htm">this photo</a> for a marquee that spans a patio and flower beds in a town garden. </li>
    <li> <strong>Winter stability:</strong> framed marquees    withstand higher 
	wind speeds and snow loadings so they are a practical option in  winter.</li>

    <li><strong>Safety:</strong> made from flame-retardant PVC manufactured to the latest BSI standards.</li>
    <li><strong>Flexible</strong>: can be joined together to cover  large areas,  fit L-shaped gardens or allow for different areas within an event </li>
    <li>See <a href="/pricing/marquees.htm">prices and sizes  </a></li>
  </ul>  
<hr/>
<a href="/marquee_questions.html#main" class="back">Back to top</a>
</div>


<div id="traditional" class="faq">
<h2>Traditional marquees</h2>
<p>Combining the best of old and new, our traditonal-style marquees have all the classic elegance of old fashioned pole marquees along with the benefits of modern technology.</p>
    <h3>Features of traditional marquees:</h4>
    <ul>
      <li><strong>Romantic</strong>: these have the beautiful  'swooping' lines of the classic English garden party marquee. Ideal for romantic traditional weddings, formal garden parties or any event where  period style or a little extra elegance is called for. </li>
      <li><strong>Poles</strong>:  two supporting poles inside the marquee which can be decorated with ribbons, flowers etc. The poles are illustrated in the lower photo alongside. </li>
      <li><strong>A little extra space:</strong> erected with guy ropes and pegs. But unlike older marquees, they do not need huge amounts of space. Thanks to modern technology, only 5' extra space is required  around the marquee for the ropes and pegs.</li>
      <li>See <a href="/pricing/traditional_marquees.htm">prices and sizes</a></li>
</ul>
<hr/>
<a href="/marquee_questions.html#main" class="back">Back to top</a>
</div>

<div id="weather" class="faq">
<h2>Questions about marquees and weather</h2>
<ol>
<li><a href="#open">Can tents be <strong>opened</strong> in warm weather?</a></li>
<li><a href="#waterproof">Are marquees <strong>waterproof</strong>?</a></li>
<li><a href="#winter">Are marquees suitable for <strong>winter</strong> events?</a></li>
<li><a href="#heaters">Do we need <strong>heaters</strong> and are they warm enough?</a></li>
<li><a href="#storms">Do your marquees stand up to <strong>storms</strong>?</a></li>
</ol>

<h3 id="open">Can tents be opened in warm weather?</h3>
<p><img src="/images/windowsOpen.jpg" alt="Marquee with windows open" width="84" height="195"  />Yes. The walls of our marquees can very easily be opened up and secured. We can demonstrate how to do this so that the decision can be left to the day.</p>
<p>As the photo illustrates, the results can be very attractive. For more examples, see <a href="/photos/summer.htm">this photo</a> or <a href="/photos/water.htm">this one</a>.</p>

<h3 id="waterproof">Are marquees waterproof?</h3>
<p>Yes, marquees are waterproof. But...</p>
<p>In heavy rain, water pouring off the roof of a tent may not soak into the ground fast enough if soil drains poorly. Water may then seep underneath the marquee walls and into the marquee.</p>
<p>To be absolutely certain of a dry floor if heavy rain is expected, hard floor with carpet is a safer though more expensive option than coconut matting. See <a href="/equipment/furniture.htm#flooring">marquee flooring</a> for details.</p>

<h3 id="winter">Are marquees suitable for winter events?</h3> 
<p>Decked out appropriately (balloons and lighting effects rather than lilies), marquees make ideal venues for Christmas and New Year parties. See <a href="photos/balloons.htm">this picture</a>  for an example. Powerful heaters (see above question) ensure warmth, and hard flooring, while more expensive than traditional summer matting, protects winter marquee from wet winter ground. So you can capture the beauty and convenience of a <a href="photos/winter-marquee.htm">marquee event even in winter</a>.</p>

<h3 id="heaters">Do we need heaters and are they warm enough?</h3>
<p>Even in midsummer, a heater may be necessary. So budget for a heater just in case, and make a decision in the week leading up to your party 
when you have seen the weather forecast. Heater technology has advanced substantially in recent years, so a tent even in winter will be surprisingly warm (see next
question).</p>

<h3 id="storms">Do your marquees stand up to storms?</h3>
<p>Our marquees are sturdy, and we always ensure that they are properly anchored. They are made in accordance with EC Safety Standards and manufactured to withstand wind loading of up to 60 mph. This is adequate for all but the fiercest of gales.</p>
<hr/>
<a href="/marquee_questions.html#main" class="back">Back to top</a>
</div>


<div id="marq" class="faq">
<h2>Questions about marquees</h2>
<ol>
<li><a href="#lining">What is the difference between <strong>lined and unlined</strong> marquees?</a></li>
<li><a href="#colour">What <strong>colour</strong> are your marquees?</a></li>
<li><a href="#roll">Can we roll up the <strong>walls</strong>?</a></li>
</ol>
  
<h3 id="lining">What is the difference between  lined and unlined marquees?</h3>
<p ><img src="/images/lining.jpg" alt="Marquee, half lined, half unlined" width="200" height="209" border="0" />The difference is illustrated in the adjacent photo. The photo shows a partially erected marquee with the front half of the roof lined and the back half unlined.</p>
<p>Lined marquees are more luxurious and are an essential element of a 
traditional wedding-style marquee. With unlined marquees, the PVC walls and 
marquee structure are fully visible, making them more suitable for functional
or casual events.</p>
<p>Most of the marquees illustrated in our marquee gallery are lined. See these examples:</p>
<ul>
<li><a href="http://www.countymarquees.com/photos/balloons.htm">Party Marquee</a></li>
<li><a href="http://www.countymarquees.com/photos/summer.htm">Summer marquee</a></li>
<li><a href="http://www.countymarquees.com/photos/wedding-tent.htm">Traditional wedding tent </a></li>
</ul>
  
<h3 id="colour">What colour are your marquees?</h3>
<p>All our marquees are made of lacquered white PVC. We do not provide any striped marquees.</p>

<h3 id="roll">Can we roll up the walls ?</h3>
<p>Marquee walls are hung from a rail and can be drawn back and forth exactly like curtains.</p>
<hr/>
<a href="/marquee_questions.html#main" class="back">Back to top</a>
</div>


<div id="safety" class="faq">
<h2>Questions about marquee safety</h2>
<ol>
<li><a href="#safe">How <strong>safe</strong> are your marquees?</a></li>
<li><a href="#insurance">Does County Marquees have <strong>insurance cover</strong>?</a></li>
<li><a href="#cables">Could a marquee <strong>damage cables</strong> or pipes underneath the site?</a></li>
<li><a href="#damamge">Will your marquees <strong>damage our garden</strong>?</a></li>
</ol>
<h3 id="safe">How safe are your marquees?</h3>
<p>All our marquees are made from materials that conform to British Standard specifications and are fully fire resistant. We also have damage waiver 
(see next question).</p>

<h3 id="insurance">Does County Marquees have insurance cover?</h3>
<p>Damage waiver against damage to persons or equipment caused through negligence by County Marquees is included in all our quotes but we would advise that you insure against cancellations. Please see our <a href="Terms_and_conditions.htm">terms and conditions</a>.</p>

<h3 id="cables">Could a marquee damage cables or pipes running underneath the site?</h3>
<p>If cabling or pipework is near the surface, damage is possible. So please advise us if any water mains or electrical cabling are close to the ground.</p>

<h3 id="damamge">Will your marquees damage our garden?</h3>
<p  ><img border="0" src="/images/dec_ownGarden.jpg" width="243" height="182" alt="Marquee carefully erected around a stunning rock garden" />Temporarily yes. Grass that has been covered for several days will not look its best. But recovery will be swift. And we are always very careful not to damage plants if flower beds are included in a marquee.</p>
<hr/>
<a href="/marquee_questions.html#main" class="back">Back to top</a>
</div>


<div id="accessories" class="faq">
<h2>Questions about marquee accessories</h2>
<ol>
<li><a href="#uplighter">Do <strong>uplighters</strong> provide enough light?</a></li>
<li><a href="#generator">Do we need a <strong>generator</strong>?</a></li>
<li><a href="#disco">How much space does a disco take up?</a></li>
<li><a href="#damamge">Will your marquees <strong>damage our garden</strong>?</a></li></ol>
<h3 id="uplighter">Do uplighters provide enough light?</h3>
<p>Yes if you are seeking an atmospheric effect. No if you want a very bright party. See <a href="equipment/lighting.htm">marquee lighting</a> for more information. </p>

<h3 id="generator">Do we need a generator?</h3>  
<p>Marquee lighting, heating and music don't  require much power and therefore don't normally call for a generator.</p>
<p>But you may need one if the wiring in your house or site is old and unreliable. And if caterers are cooking in a catering tent using electricity rather than gas, then a generator will probably be necessary. </p>

<h3 id="disco">How much room does a disco take up?</h3>
<p>Allow five foot along the width of one side of a dance floor.</p>
<hr/>
<a href="/marquee_questions.html#main" class="back">Back to top</a>
</div>
	

</div>
EOQ;
?>
