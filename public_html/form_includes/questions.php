<?php

echo <<<EOQ
<h2 id="basic">Basic questions</h2>
<dl id="special">
<dt id="large">1. How large a marquee do I need?</dt>
<dd class="show"><p>Please see <a href="/marquee_essentials.html#size">this question</a> on our <a href="/marquee_essentials.html">Where do I start</a> page for answers to this and other basic questions. </p></dd>
<dt id="cost" >2. How much does a marquee cost?</dt>
<dd class="show"><p>Please see <a href="/marquee_essentials.html#howmuch">where do I start</a> for answers to this and other basic questions. </p></dd>
</dl>
<h2>Marquee size and sites</h2>
<dl>
<dt id="big">1. Is my garden/site big enough?</dt>
<dd><p>First, you need to work out what size of marquee you need (see <a href="/marquee_essentials.html#size">this question</a> on our <a href="/marquee_essentials.html">Where do I start</a> page).</p>
<p>Then it's a simple matter of measuring your lawn to find out if the calculated size will fit into the space available.</p>
<p>Frame marquees can utilize small spaces well. See <a href="/photos/small.htm">this photo</a> for an example in a small town garden.</p>
</dd>
<dt id="slope">2. Does it matter if my garden/site has a slope?</dt>
<dd><p>A slight slope is nothing to worry about.</p>
<p>A more pronounced slope is not a problem for the marquee structure but may be uncomfortable for guests, especially at a seated event.</p>
<p>The best solution under these circumstances is to use blocks with adjustable legs. These can be expensive, but they will level out a moderate slope very effectively. For more serious slopes, there are other options but you would then be looking at spending  serious amounts of money.</p></dd>
<dt id="uneven">3. Can marquees be erected on uneven ground?</dt>
<dd><p class="byphoto">Marquees can be put up on sloping or uneven ground. To even out a pronounced slope, marquees can been erected on an adjustable platform. <em>Dwarf</em> scaffolding, illustrated right, is another option.</p>
<div class="textPhotos"><img src="/images/pages/FAQs/dwarf_scaffolding.jpg" alt="Marquee erected on an adjustable platform to even out a slope" width="195" height="150" /><p>Dwarf scaffolding under marquee</p></div>
<p  class="byphoto">See this <a href="case_studies/barmitzvah.html">case study </a> or <a href="case_studies/party.htm">this one</a> for examples of marquees set on awkward terrains.</p>
<p class="byphoto">Although an expensive option, this can transform an unpromising site into the perfect venue.</p></dd>
<dt id="extension">4. Can the marquee be connected to a building?</dt>
<dd class="bottom"><p class="byphoto">Yes. Our marquees can serve as an extension to a building if the space is suitable. Because they need no pegs or guy ropes, frame marquee structures can easily butt up against the side of a house. See <a href="/photos/extension.htm">this photo</a> for an example from outside the marquee.</p>
<div class="textPhotos"><img src="images/pages/FAQs/french_doors.jpg" alt="French doors leading directly to an attached marquee"  width="195" height="150" /><p >House doors leading to attached blacked-out marquee</p></div>
<p class="byphoto">Covered walkways are another option.</p></dd>
</dl>
<h2>Marquee hire</h2>
<dl>
<dt id="just">1. Do you supply just the marquee?</dt>
<dd><p>No. We supply <a href="equipment/furniture.htm">furniture and flooring</a>, <a href="equipment/lighting.htm">lighting</a> and other marquee accessories including generators and portable toilets as well. And we have links with many other event companies, so we can also arrange catering equipment, marquee decoration, theme parties etc, Please <a href="contact_us.htm">ask us</a> for what you need and we will help if we can.</p></dd>
<dt id="period">2. How long is your marquee hire period?</dt>
<dd><p>For weekend events, the marquee is yours for both Saturday and Sunday. During the week, the length of the marquee hire period is by arrangement.</p></dd>
<dt id="lastMin">3. Can I make last minute changes to my tent?</dt>
<dd><p>Within reason, yes. Requests for extra furniture, different flooring etc made early in the week leading up to the marquee hire or before can easily be  accommodated. A longer marquee may be possible, depending on availability. But please don't ask us to make changes five minutes before your party starts. We try to be as helpful as we can, but we're not magicians!</p></dd>
<dt id="upDown">4. When will you put up/take down my marquee?</dt>
<dd><p class="byphoto">Typically, we erect marquees on the Wednesday, Thursday or Friday before an event; and we dismantle them on the following Monday, Tuesday or Wednesday.</p>
<div class="textPhotos"><img src="images/pages/FAQs/putting_up.jpg" alt="Partially erected marquee"  width="195" height="150"  /></div>
<p class="byphoto">If you have a special requirement, please let us know and we will timetable for that.</p>
</dd>
</dl>
<h2>Marquees and weather</h2>
<dl>
<dt id="open">1. Can tents be opened in warm weather?</dt>
<dd>
<p class="byphoto">Yes. The walls of our marquees can very easily be opened up and secured. We can demonstrate how to do this so that the decision can be left to the day.</p>
<div class="textPhotos"><img src="images/pages/FAQs/windowsOpen.jpg" alt="Marquee with windows open"  width="195" height="150"  /></div>
<p class="byphoto">As the photo illustrates, the results can be very attractive. For another larger example, see <a href="/photos/summer.htm">this photo</a>.</p></dd>
<dt id="waterproof">2. Are marquees waterproof?</dt>
<dd><p>Yes, marquees are waterproof. But...</p>
<p>In heavy rain, water pouring off the roof of a tent may not soak into the ground fast enough if soil drains poorly. Water may then seep underneath the marquee walls and into the marquee.</p>
<p>To be absolutely certain of a dry floor if heavy rain is expected, hard floor with carpet is a safer though more expensive option than coconut matting. See <a href="/equipment/furniture.htm#flooring">marquee flooring</a> for details.</p></dd>
<dt id="winter">3. Are marquees suitable for winter events?</dt>
<dd><p class="byphoto">Decked out appropriately, marquees can make ideal venues for Christmas and New Year parties.</p>
<div class="textPhotos"><img src="images/pages/FAQs/winter_marquee.jpg" alt="Christmas marquee"  width="195" height="150"  /></div>
<p class="byphoto">Some extra planning is required however. Powerful heaters (see next question) are needed to ensure warmth. Hard flooring, while more expensive than traditional summer matting, is strongly recommended for winter marquees as it protects against wet winter ground.</p>
<p>With recent weather extremes, it is also sensible to check whether your venue is prone to flooding in heavy rain.</p>
<p>In sum, with a little thought, you can capture the beauty and convenience of a <a href="photos/winter-marquee.htm">marquee event even in winter</a>.</p></dd>
<dt id="heaters">4. Do we need heaters and are they warm enough?</dt>
<dd><p>Even in midsummer, a heater may be necessary. So budget for a heater just in case, and make a decision in the week leading up to your party when you have seen the weather forecast. Heater technology has advanced substantially in recent years, so a tent even in winter will be surprisingly warm.</p></dd>
<dt id="storms">5. Do your marquees stand up to storms?</dt>
<dd><p>Our marquees are sturdy, and we always ensure that they are properly anchored. They are made in accordance with EC Safety Standards and manufactured to withstand wind loading of up to 70 kph. This is adequate for all but the fiercest of gales.</p>
<p>We do take extra care when heavy storms are forecast however. This recent customer was appreciative of our extra checks: &#8220;<em>Coming around to further secure the marquee on Sunday before St Judes hit was very reassuring.</em>&#8221; See the full <a href="/company/testimonials.htm#storm">testimonial</a>.</p></dd>
</dl>
<h2>Marquees</h2>
<dl>
<dt id="lining">1. What is the difference between  lined and unlined marquees?</dt>
<dd><p  class="byphoto">The difference is illustrated in the adjacent photo. The photo shows a partially erected marquee with the front half of the roof lined and the back half unlined.</p>
<div class="textPhotos"><img src="images/pages/FAQs/lining.jpg" alt="Marquee, half lined, half unlined" width="195" height="150"  /></div>
<p class="byphoto">Lined marquees are more luxurious and are an essential element of a traditional wedding-style marquee. With unlined marquees, the PVC walls and marquee structure are fully visible, making them more suitable for functional or casual events.</p></dd>
<dt id="colour">2. What colour are your marquees?</dt>
<dd><p>All our marquees are made of lacquered white PVC. We do not provide any striped marquees.</p></dd>
<dt id="roll">3. Can we roll up the walls ?</dt>
<dd><p class="byphoto">Marquee walls are hung from a rail and can be drawn back and forth exactly like curtains.</p>
<div class="textPhotos"><img src="images/pages/FAQs/opentTent.jpg" alt="Marquee with walls drawn back"  width="195" height="150" /></div>
<p class="byphoto">The photo shows a marquee with all walls drawn back so guests can fully enjoy the surroundings.</p>
<p class="byphoto">See <a href="/photos/summer.htm">this photo</a> for another example.</p></dd>
</dl>
<h2>Marquee safety</h2>
<dl>
<dt id="safe">1. How safe are your marquees?</dt>
<dd><p>All our marquees are made from materials that conform to British Standard specifications and are fully fire resistant. We also have damage waiver (see next question).</p></dd>
<dt id="insurance">2. Does County Marquees have insurance cover?</dt>
<dd><p>Damage waiver against damage to persons or equipment caused through negligence by County Marquees is included in all our quotes but we would advise that you insure against cancellations. Please see our <a href="Terms_and_conditions.htm">terms and conditions</a>.</p></dd>
<dt id="cables">3. Could a marquee damage cables or pipes running underneath?</dt>
<dd><p>If cabling or pipework is near the surface, damage is possible. So please advise us if any water mains or electrical cabling are close to the ground.</p></dd>
<dt id="damamge">4. Will your marquees damage our garden?</dt>
<dd><p class="byphoto">Temporarily yes.</p>
<div class="textPhotos"><img border="0" src="/images/pages/FAQs/roundGarden.jpg"  width="195" height="150" alt="Marquee around a stunning rock garden" /><p>Marquee carefully erected around a stunning rock garden</p></div>
<p class="byphoto">Grass that has been covered for several days will not look its best. But recovery will be swift. And we are always very careful not to damage plants if flower beds are included in a marquee.</p>
<p class="byphoto">See our <a href="/company/testimonials.htm">testimonials page</a> for the praise lavished on our team of trained and careful marquee riggers.</p>
</dd>
</dl>
<h2>Marquee accessories</h2>
<dl>
<dt id="uplighter">1. Do uplighters provide enough light?</dt>
<dd><p>Yes if you are seeking an atmospheric effect. No if you want a very bright party. See <a href="equipment/lighting.htm">marquee lighting</a> for more information. </p></dd>
<dt id="generator">2. Do we need a generator?</dt>  
<dd><p class="byphoto">Marquee lighting, heating and music don't  require much power and therefore don't normally call for a generator.</p>
<div class="textPhotos"><img border="0" src="/images/pages/FAQs/generator.jpg"  width="195" height="150" alt="Generators attached to a marquee" /></div>
<p class="byphoto">But you may need one if the wiring in your house or site is old or unreliable. And if caterers are cooking in a catering tent using electricity rather than gas, then a generator will probably be necessary. </p></dd>
<dt id="disco">3. How much room does a disco take up?</dt>
<dd><p>Allow five foot along the width of one side of a dance floor.</p></dd>
</dl>
<p class="endCall">Or for personal answers to all your questions, please <a href="/contact_us.htm">contact us</a>.</p>
</div>
EOQ;

?>

