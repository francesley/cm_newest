<?php

echo <<<EOQ
<div id='forms'>
<p>We have divided our FAQs into sections in the hope that you will find it easier to find the answer you need. If you have further questions, please ask them below. We relish answering tough questions on our <a href="/blog/">blog</a>. Or <a href="/contact_us.htm">contact us</a> and ask. We are always happy to help.</p>
<form method="post" name="questionsForm" action="/marquee_questions.html">
<label for="tbName"><input name="tbName" value="{$fc -> formName}" id="tbName" />
<label for="tbEmail">Your email<span class="instructions">Not for public display,<br/>but to notify you of an answer</span></label><input name="tbEmail" value="{$fc -> formEmail}" id="tbEmail" />
<label for="tbQuestion"><span class="required">*</span>Your question:</label>
<textarea name="tbQuestion" id="tbQuestion" rows="7">{$fc -> formQuestion}</textarea><div class="error" {$fc -> styleQuestion}>Required</div>
<input name="submitted" type="hidden" value="true" />
<label for="contactSubmit"></label><input name="contactSubmit" id="contactSubmit" type="submit" value="Ask a question" />
<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a>. You won&rsquo;t receive any spam emails as a result of contacting us.</p>
</form>
<ul class="float">
<li><a href="#size">Marquee size and sites</a></li>
<li><a href="#hire">Marquee hire</a></li>
<li><a href="#marq">Marquees</a></li>
<li><a href="#weather">Marquees and bad weather</a></li>
</ul>
<ul class="float" style="margin-right:0;">
<li><a href="#safety">Marquee safety</a></li>
<li><a href="#accessories">Marquee accessories</a></li>
<li><a href="#sunny">Marquees and sunny days</a></li>
</ul>
<hr/>
<div id="size" class="faq">
<h2>Marquee size and sites</h2>
<ol>
<li><a href="#large">How <strong>large</strong> a <strong>marquee</strong> do I need?</a></li>
<li><a href="#large">Is my <strong>garden</strong>/site <strong>big</strong> enough?</a></li>
<li><a href="#slope">Does it matter if my garden/site has a <strong>slope</strong>?</a></li>
<li><a href="#uneven">Can marquees be erected on <strong>uneven ground</strong>?</a></li>
<li><a href="#extension">Can the marquee be <strong>connected to a building</strong>?</a></li>
</ol>
 <h3 id="large">How large a marquee do I need?</h3>
<p>Two good rules of thumb are: </p>
<ul>
<li>For a buffet event: a <strong>minimum</strong> of 8 square feet per person</li>
<li>For a seated event: a <strong>minimum</strong> of 15 square feet per person</li>
</ul>
<p>Or check out our <a href="/pricing/marquees.htm">price lists</a>. They have suggested sizing for seated and buffet events for different numbers of guests for all types of marquees.</p>
<p>You will still need to take account of extras like dance floors, catering tables and music centres. Request a free site vist for a detailed estimate.</p>
<h3 id="big">Is my garden/site big enough?</h3>
<p>First, answer these questions: </p>
<ul><li>How many people do you want to invite?</li>
<li>How many people do you want to seat at tables?</li>
<li>How many extras would you like - dance floor, stage, chill out area, bar area etc?</li>
</ul>
<p>Then check out our <a href="/pricing/marquees.htm">price lists</a> which have suggested sizing for seated and buffet events for all sizes and types of marquees.</p>
<h3 id="slope">Does it matter if my garden/site has a slope?</h3>
<p>A slight slope is nothing to worry about.</p>
<p>A more pronounced slope is not a problem for the marquee structure but may be uncomfortable for guests, especially at a seated event.</p>
<p>The best solution under these circumstances is to use blocks with adjustable legs. These can be expensive, but they will level out a moderate slope very effectively. For more serious slopes, there are other options but you would then be looking at spending  serious amounts of money.</p>
<h3 id="uneven">Can marquees be erected on uneven ground?</h3>
<p ><img src="/images/unevenGround.jpg" alt="Marquee erected on an adjustable platfrom to even out a slope" width="200" height="170" loading="lazy" />Marquees can be put up on sloping or uneven ground. The adjacent picture shows a marquee that has been erected on an adjustable platform to even out a pronounced slope. 'Dwarf' scaffolding is another option.</p>
<p >See this <a href="case_studies/barmitzvah.html">case study </a> or <a href="case_studies/party.htm">this one</a> for examples of marquees set on awkward terrains.</p>
<p >Although an expensive option, this can transform an unpromising site into the perfect venue.</p>
<h3 id="extension">Can the marquee be connected to a building?</h3>
<p>Yes. Our marquees can serve as an 'extension' to a building if the space is suitable. Because they need no pegs or guy ropes, frame marquee structures can easily butt up against the side of a house. See <a href="equipment/marquees.htm#extension">this photo</a> for an example.</p>
<p>Covered walkways are another option.</p>
</div>
<div id="hire" class="faq">
<h2>Marquee hire</h2>
<ol>
<li><a href="#just">Do you <strong>supply just</strong> the <strong>marquee</strong>?</a></li>
<li><a href="#long">How <strong>long</strong> is your marquee hire period?</a></li>
<li><a href="#lastMin">Can I make <strong>last minute</strong> changes to my tent?</a></li>
<li><a href="#upDown">When will you <strong>put up/take down</strong> my marquee?</a></li>
</ol>
<h3 id="just">Do you supply just the marquee?</h3>
<p>No. We supply <a href="equipment/furniture.htm">furniture</a>, <a href="equipment/lighting.htm">lighting</a> and <a href="equipment/furniture.htm">flooring</a> as well. And we have links with many other event companies, so we can also arrange catering equipment, marquee decoration, theme parties etc, Please <a href="contact_us.htm">ask us</a> for what you need and we will help if we can.</p>
<h3 id="long">How long is your marquee hire period?</h3>
<p>For weekend events, the marquee is yours for both Saturday and Sunday. During the week, the length of the marquee hire period is by arrangement.</p>
<h3 id="lastMin">Can I make last minute changes to my tent?</h3>
<p>Within reason, yes. Requests for extra furniture, different flooring etc made early in the week leading up to the marquee hire or before can easily be  accommodated. A longer marquee may be possible, depending on availability. But please don't ask us to make changes five minutes before your party starts. We try to be as helpful as we can, but we're not magicians!</p>
<h3 id="upDown">When will you put up/take down my marquee?</h3>
<p>Typically, we erect marquees on the Wednesday, Thursday or Friday before an event; and we dismantle them on the following Monday, Tuesday or Wednesday. If you have a special requirement, please let us know and we will timetable for that.</p>
</div>
<div id="weather" class="faq">
<h2>Marquees and bad weather</h2>
<ol>
<li><a href="#waterproof">Are marquees <strong>waterproof</strong>?</a></li>
<li><a href="#winter">Are marquees suitable for <strong>winter</strong> events?</a></li>
<li><a href="#heaters">Do we need <strong>heaters</strong> and are they warm enough?</a></li>
<li><a href="#storms">Do your marquees stand up to <strong>storms</strong>?</a></li>
</ol>
<h3 id="waterproof">Are marquees waterproof?</h3>
<p>Yes, marquees are waterproof. But...</p>
<p>In heavy rain, water pouring off the roof of a tent may not soak into the ground fast enough if soil drains poorly. Water may then seep underneath the marquee walls and into the marquee.</p>
<p>To be absolutely certain of a dry floor if heavy rain is expected, hard floor with carpet is a safer though more expensive option than coconut matting. See <a href="/equipment/furniture.htm#flooring">marquee flooring</a> for details.</p>
<h3 id="winter">Are marquees suitable for winter events?</h3> 
<p>Decked out appropriately, marquees make ideal venues for Christmas and New Year parties. See <a href="photos/balloons.htm">this picture</a>  for an example. Powerful heaters (see above question) ensure warmth, and hard flooring, while more expensive than traditional summer matting, protects winter marquees from wet winter ground. So you can capture the beauty and convenience of a <a href="photos/winter-marquee.htm">marquee event even in winter</a>.</p>
<h3 id="heaters">Do we need heaters and are they warm enough?</h3>
<p>Even in midsummer, a heater may be necessary. So budget for a heater just in case, and make a decision in the week leading up to your party when you have seen the weather forecast. Heater technology has advanced substantially in recent years, so a tent even in winter will be surprisingly warm (see next question).</p>
<h3 id="storms">Do your marquees stand up to storms?</h3>
<p>Our marquees are sturdy, and we always ensure that they are properly anchored. They are made in accordance with EC Safety Standards and manufactured to withstand wind loading of up to 60 mph. This is adequate for all but the fiercest of gales.</p>
</div>
<div id="marq" class="faq">
<h2>Marquees</h2>
<ol>
<li><a href="#lining">What is the difference between <strong>lined and unlined</strong> marquees?</a></li>
<li><a href="#colour">What <strong>colour</strong> are your marquees?</a></li>
<li><a href="#roll">Can we roll up the <strong>walls</strong>?</a></li>
</ol>
<h3 id="lining">What is the difference between  lined and unlined marquees?</h3>
<p><img src="/images/lining.jpg" alt="Marquee, half lined, half unlined" width="200" height="209" border="0"  loading="lazy" />The difference is illustrated in the adjacent photo. The photo shows a partially erected marquee with the front half of the roof lined and the back half unlined.</p>
<p>Lined marquees are more luxurious and are an essential element of a traditional wedding-style marquee. With unlined marquees, the PVC walls and marquee structure are fully visible, making them more suitable for functional or casual events.</p>
<h3 id="colour">What colour are your marquees?</h3>
<p>All our marquees are made of lacquered white PVC. We do not provide any striped marquees.</p>
<h3 id="roll">Can we roll up the walls ?</h3>
<p>Marquee walls are hung from a rail and can be drawn back and forth exactly like curtains.</p>
</div>
<div id="safety" class="faq">
<h2>Marquee safety</h2>
<ol>
<li><a href="#safe">How <strong>safe</strong> are your marquees?</a></li>
<li><a href="#insurance">Does County Marquees have <strong>insurance cover</strong>?</a></li>
<li><a href="#cables">Could a marquee <strong>damage cables</strong> or pipes underneath the site?</a></li>
<li><a href="#damamge">Will your marquees <strong>damage our garden</strong>?</a></li>
</ol>
<h3 id="safe">How safe are your marquees?</h3>
<p>All our marquees are made from materials that conform to British Standard specifications and are fully fire resistant. We also have damage waiver (see next question).</p>
<h3 id="insurance">Does County Marquees have insurance cover?</h3>
<p>Damage waiver against damage to persons or equipment caused through negligence by County Marquees is included in all our quotes but we would advise that you insure against cancellations. Please see our <a href="Terms_and_conditions.htm">terms and conditions</a>.
<h3 id="cables">Could a marquee damage cables or pipes running underneath the site?</h3>
<p>If cabling or pipework is near the surface, damage is possible. So please advise us if any water mains or electrical cabling are close to the ground.</p>
<h3 id="damamge">Will your marquees damage our garden?</h3>
<p  ><img border="0" src="/images/dec_ownGarden.jpg" width="243" height="182" alt="Marquee carefully erected around a stunning rock garden" loading="lazy" />Temporarily yes. Grass that has been covered for several days will not look its best. But recovery will be swift. And we are always very careful not to damage plants if flower beds are included in a marquee.</p>
</div>
<div id="accessories" class="faq">
<h2>Marquee accessories</h2>
<ol>
<li><a href="#uplighter">Do <strong>uplighters</strong> provide enough light?</a></li>
<li><a href="#generator">Do we need a <strong>generator</strong>?</a></li>
<li><a href="#disco">How much space does a disco take up?</a></li>
<li><a href="#damamge">Will your marquees <strong>damage our garden</strong>?</a></li></ol>
<h3 id="uplighter">Do uplighters provide enough light?</h3>
<p>Yes if you are seeking an atmospheric effect. No if you want a very bright party. See <a href="equipment/lighting.htm">marquee lighting</a> for more information. </p>
<h3 id="generator">Do we need a generator?</h3>
<p>Marquee lighting, heating and music don't  require much power and therefore don't normally call for a generator.</p>
<p>But you may need one if the wiring in your house or site is old and unreliable. And if caterers are cooking in a catering tent using electricity rather than gas, then a generator will probably be necessary.</p>
<h3 id="disco">How much room does a disco take up?</h3>
<p>Allow five foot along the width of one side of a dance floor.</p>
</div>
<div id="sunny" class="faq">
<h2>Marquees and sunny days</h2>
<h3 id="open">Can tents be opened in warm weather?</h3>
<p><img src="/images/windowsOpen.jpg" alt="Marquee with windows open" width="84" height="195"  loading="lazy"  />Yes. The walls of our marquees can very easily be opened up and secured. We can demonstrate how to do this so that the decision can be left to the day.</p>
<p>As the photo illustrates, the results can be very attractive. For more examples, see <a href="/photos/summer.htm">this photo</a> or <a href="/photos/water.htm">this one</a>.</p>
</div>
</div>
EOQ;
?>

