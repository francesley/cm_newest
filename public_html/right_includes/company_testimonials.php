<div id="right">
<h3>We didn't<br/>make these up!</h3>
<p>These <strong>unsolicited</strong> customer comments show that when you choose marquee hire from County Marquees you get the benefits of:</p>
<ul class="bullet">
<li><strong>Efficient, professional service</strong></li>
<li><strong>Dependable, experienced and courteous staff</strong> who will go out of their way to ensure we more than fulfill our part in your event</li>
<li>Reassurance that our staff will treat your <strong>venue</strong> with the <strong>respect</strong> and the personal attention it deserves</li>
<li>And above all, <strong>beautifully presented marquees</strong></li>
</ul>
<h3>Like what you read?</h3>
<a href="/contact_us.htm" class="call"><img src="../images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
<p class="aboveCall">Or if you know exactly what you want:</p>
<a href="/pricing/quotation.htm" class="call bottom"><img src="../images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer" ><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer" ><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer" ><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="noopener noreferrer" ><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>