<?php

echo <<<EOQ
<div id="right">
<h3>Sussex locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/chichester.htm">Chichester</a></li>
<li><a href="/areas/brighton.htm">Brighton</a></li>
<li><a href="/areas/crawley.htm">Crawley</a></li>
<li><a href="/areas/eastbourne.htm">Eastbourne</a></li>
<li><a href="/areas/hastings.htm">Hastings</a></li>
<li><a href="/areas/horsham.htm">Horsham</a></li>
<li><a href="/areas/uckfield.htm">Uckfield</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>

