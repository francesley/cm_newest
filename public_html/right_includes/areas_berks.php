<?php

echo <<<EOQ
<div id="right">
<h3>Berkshire locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/bracknell.htm">Bracknell</a></li>
<li><a href="/areas/egham.htm">Egham</a></li>
<li><a href="/areas/maidenhead.htm">Maidenhead</a></li>
<li><a href="/areas/reading.htm">Reading</a></li>
<li><a href="/areas/slough.htm">Slough</a></li>
<li><a href="/areas/windsor.htm">Windsor</a></li>
<li><a href="/areas/wokingham.htm">Wokingham</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>

