<?php

echo <<<EOQ
<div id="right">
<h3>North London locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/enfield.htm">Enfield</a></li>
<li><a href="/areas/ham_high.htm">Hampstead &amp; Highgate</a></li>
<li><a href="/areas/harrow.htm">Harrow</a></li>
<li><a href="/areas/northwood.htm">Northwood</a></li>
<li><a href="/areas/romford.htm">Romford and Ilford</a></li>
<li><a href="/areas/southgate.htm">Southgate</a></li>
</ul>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank"   rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>
