<?php


echo <<<EOF
<div id="right">
<h3>What next?</h3>
  <p class="aboveCall">Got questions? For more information, assistance or a free site visit:</p>
 <a href="/contact_us.htm" class="call"><img src="/images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
  
  <p class="aboveCall">Or for your very own quote:</p>
   <a href="/pricing/quotation.htm" class="call bottom"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
 
<h3>Find out more about:</h3>
<ul  class="noBG">
<li><a href="/equipment/marquees.htm">Marquees</a></li>
<li><a href="/wedding-marquees.htm">Wedding marquees</a></li>
<li><a href="/corporate-marquees.htm">Corporate marquees</a></li>
<li><a href="/real.htm">Case studies: real marquees</a></li>
</ul>

<h3>PDF download:</h3>
   <p class="aboveCall">Download a PDF of sample prices:</p>
 <a href="/PDFs/sample_prices.pdf" class="call" onClick="javascript: pageTracker._trackEvent('Downloads','Sample price page','Sample prices' );"><img src="/images/right/sample_prices.jpg" alt="Download sample prices pdf" width="198" height="215"/></a>
 <p class="aboveCall">Or download a PDF of our full brochure and all price lists:</p>
 <a href="/PDFs/countymarquees_brochureandprices.pdf" class="call bottom" onClick="javascript: pageTracker._trackEvent('Downloads','Sample price page','Full brochure' );"><img src="../images/right/brochure.jpg" alt="Download marquee brochure pdf" width="198" height="215"/></a>

  </div>
EOF;

		
?>