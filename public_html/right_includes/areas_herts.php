<?php

echo <<<EOQ
<div id="right">
<h3>Hertfordshire locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/baldock.htm">Baldock</a></li>
<li><a href="/areas/berkhamsted.htm">Berkhamsted</a></li>
<li><a href="/areas/bishops-stortford.htm">Bishops Stortford</a></li>
<li><a href="/areas/hertford.htm">Hertford</a></li>
<li><a href="/areas/st-albans.htm">St Albans</a></li>
<li><a href="/areas/watford.htm">Watford</a></li>
<li><a href="/areas/welwyn.htm">Welwyn Garden City</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank" rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank" rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>

