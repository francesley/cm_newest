<?php

echo <<<EOQ
<div id="right">
<h3>Oxfordshire locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/abingdon.htm">Abingdon</a></li>
<li><a href="/areas/didcot.htm">Didcot</a></li>
<li><a href="/areas/henley.htm">Henley</a></li>
<li><a href="/areas/oxford.htm">Oxford</a></li>
<li><a href="/areas/thame.htm">Thame</a></li>
<li><a href="/areas/witney.htm">Witney</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank" rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank" rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>

