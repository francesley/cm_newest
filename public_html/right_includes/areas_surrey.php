<?php

echo <<<EOQ
<div id="right">
<h3>Surrey locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/guildford.htm">Guildford</a></li>
<li><a href="/areas/cranleigh.htm">Cranleigh</a></li>
<li><a href="/areas/dorking.htm">Dorking</a></li>
<li><a href="/areas/epsom.htm">Epsom</a></li>
<li><a href="/areas/farnham.htm">Farnham</a></li>
<li><a href="/areas/walton.htm">Walton-on-Thames</a></li>
<li><a href="/areas/weybridge.htm">Weybridge</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>

