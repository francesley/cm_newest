<div id="right" class="jobs">
 <h3>A day in the work</h3>
<img src="/images/right/form_extras/work2.jpg" alt="Coffees on the way to a marquee job in Surrey"  width="189" height="150"/>
<img src="/images/right/form_extras/work3.jpg" alt="Erecting a marquee Guildford, Surrey" width="189" height="150"/>
<img src="/images/right/form_extras/work4.jpg" alt="Constructing a  tradtional marquee" width="189" height="150"/>
<img src="../images/right/form_extras/work5.jpg" alt="The finished article"  width="189" height="150"/>
<img src="../images/right/form_extras/work1.jpg" alt="On the way home after a day of marquee work"  width="189" height="150"/>
  <h3 id="socialhead">Get social</h3>
  <a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
   <a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
    <a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
     <a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>