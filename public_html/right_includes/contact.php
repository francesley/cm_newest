<div id="right" class="contact">
<h3>Contact details</h3>
<h2>London</h2>
	<p><span class="areas">Covering <a href="/areas/bucks.htm">Bucks</a>, <a href="/areas/herts.htm">Herts</a>, Essex &amp; Berkshire</span></p>
<div class="vcard" id="london">
<div class="fn org">County Marquees</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2><a href="/areas/surrey.htm">Surrey</a></h2>
<div class="vcard" id="surrey">
<div class="fn org">County Marquees</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2><a href="/areas/sussex.htm">Sussex</a></h2>
<p><span class="areas">Covering East Sussex, West Sussex &amp; Hampshire</span></p>
<div class="vcard" id="sussex">
<div class="fn org">County Marquees</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<div class="region">West Sussex</div>
<div class="postal-code">PO20 7EB</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a  class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2><a href="/areas/oxfordshire.htm">Oxford</a></h2>
<div class="vcard" id="oxford">
<div class="fn org">County Marquees</div>
 <div class="adr">
<div class="street-address">63 Wadards Meadow</div>
<span class="locality">Witney</span>
<div class="region">Oxfordshire</div>
<div class="postal-code">OX28 3YH</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2><a href="/areas/kent.htm">Kent</a></h2>
<div class="vcard" id="kent">
<div class="fn org">County Marquees</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers<br/>
85 High Street</div>
<span class="locality">Tunbridge Wells</span>
<div class="region">Kent</div> <div class="postal-code">TN1 1XP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
</div>