<div id="right">
<h3>See real <br/>corporate events</h3>
<ul>
<li><a href="/photos/launch.htm">Skating under the stars</a></li>
<li><a href="/photos/ark_royal.htm">Decommissioning the Ark Royal</a></li>
<li><a href="/case_studies/balls.htm">University ball</a></li>
<li><a href="/case_studies/christmas.htm">Christmas party</a></li>
</ul>
<p class="aboveCall">For assistance, a brochure or a free site visit:</p>
<a href="/contact_us.htm" class="aboveCall"><img src="../images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
<p class="aboveCall ">Or do you already know exactly what you want?</p>
<a href="/pricing/quotation.htm" class="call bottom"><img src="../images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
<a href="https://www.facebook.com/countymarquees" target="_blank" rel="noopener noreferrer" ><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" loading="lazy" /></a>
<a href="https://twitter.com/County_Marquees" target="_blank" rel="noopener noreferrer" ><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social" loading="lazy" /></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer" ><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social" loading="lazy" /></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" loading="lazy" /></a>
</div>