<div id="right">
	<p class="aboveCall">For more personalised help about the price of your marquee:</p>
 <a href="/contact_us.htm" class="call"><img src="/images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
  
  <p class="aboveCall">Or if you already know exactly what you want?</p>
  <a href="/pricing/quotation.htm" class="call bottom"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
  <h3>Find out more</h3>
  <ul>
	<li>Not sure about the difference between a frame and a traditional marquee?<br/>
	<span class="link">See <a href="/equipment/marquees.htm" class="normal">marquees</a></span></li>
	<li>Want to pore over price lists?<br/>
	<span class="link">See <a href="/pricing/marquees.htm" class="normal">marquee pricing</a></span></li>
</ul>
  </div>
