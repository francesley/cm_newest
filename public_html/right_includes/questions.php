<?php

echo <<<EOQ

<div id="right">



<h3>More helpful pages:</h3>

	<ul class="noBG linklist">

		<li><a href="/marquee_essentials.html">Where do I start?</a></li>

		<li><a href="/marquee_questions.html">All about marquees</a></li>

		<li><a href="/equipment/furniture.htm">Marquee furniture</a></li>

		<li><a href="/equipment/decoration.htm">Marquee decoration</a></li>

		<li><a href="/case_studies/bunting.htm">Case studies: real marquees</a></li>

	</ul>

<h3>More questions?</h3>

<p class="ask">If you still have questions, please ask them here. We will answer all relevent questions.</p>

	<div class="error summary" {$this -> styleFormResponse}>{$this -> formResponse}</div>

<form method="post" name="questionsForm" action="/marquee_questions.html" id="QuestionsForm">

<label for="tbName">Your name:</label><input name="tbName" value="{$fc -> formName}" id="tbName"/>

<label for="tbEmail"><span class="required">*</span>Your email</label><input name="tbEmail" value="{$fc -> formEmail}" id="tbEmail" class="reqfield"/>

<div class="error" {$fc -> styleEmail}>Required</div>

<div class="error" {$fc -> styleValidEmail}>Invalid email</div>

<label for="tbQuestion"><span class="required">*</span>Your question:</label>

<textarea name="tbQuestion" id="tbQuestion" rows="7" cols="10" class="reqfield" >{$fc -> formQuestion}</textarea>

<div class="error" {$fc -> styleQuestion}>Required</div>

<input name="submitted" type="hidden" value="true" style="float:none;display:inline;margin:0"/>

<input name="contactSubmit" id="contactSubmit" type="submit" value="Ask your question" />

<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a></p>

</form>

  <a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>

   <a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>

    <a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>

    <a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>

</div>

EOQ;

?>

