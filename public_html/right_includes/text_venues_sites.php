<?php
//Util::Show($request -> getProperty("subtype"));
echo <<<EOQ

<div id="right" class="sites">
<h3>Switch county:</h3>
<img src="/images/test_venue_map.png" width="233" height="184" border="0" usemap="#Map" id="map"/>
<map name="Map" id="Map">
<area shape="poly" coords="194,150" href="#" /><area shape="poly" coords="199,151,223,124,227,98,196,101,163,92,144,91,136,127" href="/venues/sites.htm?county=kent" alt="Marquee venues in Kent" />
<area shape="poly" coords="147,88,167,88,193,79,194,55,216,51,215,37,183,37,175,25,137,30,138,59,129,70" href="/venues/sites.htm?county=essex" alt="Marquee venues Essex" />
<area shape="poly" coords="98,78,128,67,136,35,118,29,106,40,101,53,88,59" href="/venues/sites.htm?county=herts" alt="Marquee venues hertfordshire" />
</map>
<ul id="counties">
				<li class="top"><a href="/venues/sites.htm" class='selected' >All sites</a></li> 
				<li><a href="/venues/sites.htm?county=lon">London</a></li> 
				<li class="right"><a href="/venues/sites.htm?county=beds">Bedfordshire</a></li> 
				<li><a href="/venues/sites.htm?county=berks" >Berkshire</a></li>
				<li class="right"><a href="/venues/sites.htm?county=bucks" >Bucks</a></li>
				<li><a href="/venues/sites.htm?county=eSussex">East Sussex</a></li> 
				<li  class="right"><a href="/venues/sites.htm?county=essex">Essex</a></li>
				<li><a href="/venues/sites.htm?county=hants">Hampshire</a></li> 
				<li class="right"><a href="/venues/sites.htm?county=herts" >Hertfordhire</a></li>
				<li><a href="/venues/sites.htm?county=kent" >Kent</a></li>
				<li class="right"><a href="/venues/sites.htm?county=oxford" >Oxfordshire</a></li> 
				<li class="bottom"><a href="/venues/sites.htm?county=surrey" >Surrey</a></li>
				<li class="right bottom"><a href="/venues/sites.htm?county=wSussex" >West Sussex</a></li> 
				</ul>
<p class="title">Want a marquee in a venue?</p>
  <p class="aboveCall">For more information on marquee venues:</p>
 <a href="/contact_us.htm" class="call"><img src="/images/right/contact.png" alt="Contact us" width="233" height="55" /></a>
  </div>
EOQ;
?>
		
