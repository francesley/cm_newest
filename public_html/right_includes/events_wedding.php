<div id="right">
<div id="boxout"><h3>What wedding style?</h3>
<p class="aboveCall">A wedding like this?<a href="/case_studies/vintage_wedding.htm"><img src="/images/right/indian_wedding.jpg" alt="Indian style wedding" height="120" width="190" /></a></p>
<p class="aboveCall">Or like this?<a href="/photos/reception.htm"><img src="/images/right/flower_wedding2.jpg" alt="Traditional wedding marquee with lilies"  height="120" width="190"  class="wedding"/></a></p>
<a href="/contact_us.htm"><img src="/images/right/wedding_call_new.jpg" alt="Let us help you create the wedding of your dreams"  width="195" height="62" border="0" /></a></div>
<h3>Already decided?</h3>
<p class="aboveCall belowboxout">Or do you already know exactly what you want?</p>
<a href="/pricing/quotation.htm" class="call bottom"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62" loading="lazy" /></a>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" loading="lazy"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer" ><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social" loading="lazy" /></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer" ><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social" loading="lazy" /></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="noopener noreferrer" ><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" loading="lazy" /></a>
</div>