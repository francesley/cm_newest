<div id="right">
<h3>What kind of party?</h3>
<div id="boxout"><p class="aboveCall">A party like this?<a href="/case_studies/eighteenth.htm"><img src="/images/pages/party/indian.jpg" alt="Glitzy marquee party" height="120" width="190"/></a></p>
<p class="aboveCall">or like this?<a href="/case_studies/babyshower.htm"><img src="/images/pages/party/grey_balloons.jpg" alt="Traditional style teatime party" height="120" width="190" class="wedding"/></a></p>
<a href="/contact_us.htm" class="call"><img src="/images/right/party_call.jpg" alt="Let us help you party in your own style" width="195" height="62" /></a></div>
<h3>Already decided:</h3>
<p class="aboveCall belowboxout">Or do you already know exactly what you want?</p>
<a href="/pricing/quotation.htm" class="call bottom"><img src="../images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a> 
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>