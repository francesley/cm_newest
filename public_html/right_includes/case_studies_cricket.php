<div id="right">

<h3><a href="/real.htm"> &laquo; All real marquees</a></h3>
<ul  class="overheader">
<li><a href="/case_studies/alternative.htm">Funky party tent</a></li>
<li><a href="/case_studies/balls.htm">University ball</a></li>
<li><a href="/case_studies/relaxed.htm">Wedding &amp; exchange of vows</a></li><li><a href="/case_studies/reveals.htm">Marquee divided</a></li>
<li><a href="/case_studies/christmas.htm">Christmas marquee ball</a></li>
<li><a href="/case_studies/venice.htm">Venice themed marquee</a></li>
<li><a href="/case_studies/colourful.htm">Indian wedding marquee</a></li>
<li><a href="/case_studies/bunting.htm">English garden style</a></li>
<li><a href="/case_studies/reception.htm">Classic marquee reception</a></li>
<li><a href="/case_studies/wedding.htm">Large wedding marquee</a></li>
<li><a href="/case_studies/summer.htm">Summer marquee cover</a></li>
<li class="selected"><a href="/case_studies/cricket.htm">Cricket themed marquee</a></li>
<li><a href="/case_studies/ball.html">A marquee ball</a></li>
<li><a href="/case_studies/barmitzvah.htm">Awkward garden marquee</a></li>
<li><a href="/case_studies/party.htm">50th birthday party marquee</a></li>
<li><a href="/equipment/erection.htm">Large corporate party</a></li>
</ul>

<h3>Find out more:</h3>
<ul>
<li><a href="/equipment/decoration.htm">Marquee decoration</a></li>
<li><a href="/events/party_marquees.htm">Marquees for parties</a></li>
<li><a href="/equipment/marquees.htm">All about our marquees</a></li>
</ul>
<h3>Take things further:</h3>
<a href="/contact_us.htm" class="noborder"><img src="../images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>  
  <a href="/pricing/quotation.htm" class=" call bottom"><img src="../images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
  
  
</div>

		
