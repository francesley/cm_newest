<?php

echo <<<EOQ
<div id="right">
<h3>South London locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/beckenham.htm">Beckenham</a></li>
<li><a href="/areas/croydon.htm">Croydon</a></li>
<li><a href="/areas/new-malden.htm">New Malden</a></li>
<li><a href="/areas/richmond.htm">Richmond</a></li>
<li><a href="/areas/wimbledon.htm">Wimbledon</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;

?>

