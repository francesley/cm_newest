<?php

echo <<<EOQ
<div id="right">
<h3>Buckinghamshire locations:</h3>
<ul class="noBG linklist">
<li><a href="/areas/amersham.htm">Amersham</a></li>
<li><a href="/areas/high-wycombe.htm">High Wycombe</a></li>
<li><a href="/areas/marlow.htm">Marlow</a></li>
<li><a href="/areas/milton-keynes.htm">Milton Keynes</a></li>
<li><a href="/areas/olney.htm">Olney</a></li>
<li><a href="/areas/princes-risborough.htm">Princes Risborough</a></li>
<li><a href="/areas/stoke-mandeville.htm">Stoke Mandeville</a></li></ul>
<a href="https://www.facebook.com/countymarquees" target="_blank" rel="noopener noreferrer"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOQ;
?>

