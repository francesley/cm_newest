<?php


echo <<<EOF
<div id="right">
<h3>What next?</h3>
<p  class="aboveCall">Ready to find out how much your marquee will cost?</p>
  <a href="/pricing/quotation.htm" class="call"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
<p class="aboveCall">Confused? Let us help.</p>
 <a href="/contact_us.htm" class="call bottom"><img src="/images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
 
<h3>Find out more about:</h3>
<ul>
<li><a href="/equipment/furniture.htm">Marquee furniture</a></li>
<li><a href="/equipment/lighting.htm">Marquee lighting</a></li>
<li><a href="/equipment/decoration.htm">Marquee decoration</a></li>
<li><a href="/real.htm">Case studies: real marquees</a></li>
</ul>

<h3>PDF download:</h3>
   <p class="aboveCall">Download a PDF of furniture prices:</p>
 <a href="/PDFs/furniture_prices.pdf" class="call" onClick="javascript: pageTracker._trackEvent('Downloads','Furniture price page','Furniture prices' );"><img src="/images/right/furniture_prices.jpg" alt="Download furniture prices pdf" width="198" height="215"/></a>
 <p class="aboveCall">Or download a PDF of our full brochure and all price lists:</p>
 <a href="/PDFs/countymarquees_brochure.pdf" class="call bottom" onClick="javascript: pageTracker._trackEvent('Downloads','Furniture price page','Full brochure' );"><img src="../images/right/brochure.jpg" alt="Download marquee brochure pdf" width="198" height="215"/></a>


  </div>
EOF;

		
?>