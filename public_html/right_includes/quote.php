<div id="right">
<p class="aboveCall">For more personalised help about the price of your marquee:</p>
<a href="/contact_us.htm" class="call"><img src="/images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
<p class="aboveCall">Or if you already know exactly what you want:</p>
<a href="/pricing/quotation.htm" class="call bottom"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
<h3>Find out more</h3>
<ul>
<li>Mystified by marquees?<br/>
<span class="link">See <a href="/marquee_essentials.html" class="normal">where to start</a></span>
</li>
<li>For answers to more detailed marquee questions:<br/>
<span class="link">See <a href="/marquee_questions.html" class="normal">marquee hire questions</a></span>	
</li>
<li>Not sure about the difference between a frame and a traditional marquee?<br/>
<span class="link">See <a href="/equipment/marquees.htm" class="normal">marquees</a></span></li>
<li>Want to pore over price lists?<br/>
<span class="link">See <a href="/pricing/marquees.htm" class="normal">marquee pricing</a></span></li>
</ul>
<h3 id="socialhead">Get social</h3><a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer" ><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social" loading="lazy" /></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer" ><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social" loading="lazy" /></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer" ><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social" loading="lazy" /></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="noopener noreferrer" ><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social" loading="lazy" /></a>
</div>

