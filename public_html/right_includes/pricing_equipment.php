<?php


echo <<<EOF
<div id="right">
<h3>What next?</h3>
<p class="aboveCall">Ready to find out exactly how much your marquee will cost?</p>
<a href="/pricing/quotation.htm" class="call"><img src="/images/quoteSM.jpg" alt="Get a marquee quote"  width="195" height="62"/></a>
<p class="aboveCall">Just want a ball park figure?</p>
<a href="/pricing/quote.htm" class="call"><img src="/images/online_quote.jpg" alt="Get an immediate marquee hire quote"  width="195" height="62"/></a>
<p class="aboveCall">Like some help?</p>
<a href="/contact_us.htm" class="call bottom"><img src="/images/contactSM.jpg" alt="Contact us" width="195" height="62"/></a>
<h3>Find out more:</h3>
<ul>
<li>Not sure about the difference between a frame and a traditional marquee?<br/>
<span class="link">See <a href="/equipment/marquees.htm" class="normal">marquees</a></span></li>
<li>Like to see some real marquees?<br/>
<span class="link">See <a href="/real.htm" class="normal">marquee case studies</a></span></li>
</ul>
<h3>PDF download:</h3>
<p class="aboveCall">Download a PDF of equipment prices:</p>
<a href="/PDFs/equipment_prices.pdf" class="call"  onClick="javascript:_gaq.push(['_trackEvent', 'Downloads', 'Equipment price page','Equipment prices']);"><img src="/images/right/equipment_prices.jpg" alt="Download equipment prices pdf" width="198" height="215"/></a>
<p class="aboveCall">Or download a PDF of our full brochure and all price lists:</p>
<a href="/PDFs/countymarquees_brochureandprices.pdf" class="call bottom" onClick="javascript:_gaq.push(['_trackEvent', 'Downloads', 'Equipment price page','Full brochure']);"><img src="../images/right/brochure.jpg" alt="Download marquee brochure pdf" width="198" height="215"/></a>
<a href="https://www.facebook.com/countymarquees" target="_blank"  rel="noopener noreferrer"><img src="/images/facebook.png" width="30" height="30" alt="County Marquees on Facebook" class="social"/></a>
<a href="https://twitter.com/County_Marquees" target="_blank"  rel="noopener noreferrer"><img src="/images/twitter.png" width="30" height="30" alt="County Marquees on Twitter"  class="social"/></a>
<a href="https://pinterest.com/county_marquees/" target="_blank" rel="noopener noreferrer"><img src="/images/pinterest.png" width="30" height="30" alt="County Marquees on Pinterest" class="social"/></a>
<a href="https://www.instagram.com/countymarquees/" target="_blank" rel="noopener noreferrer"><img src="/images/instagram.png" width="30" height="30" alt="Find us on Instagram" class="social"/></a>
</div>
EOF;



		

?>