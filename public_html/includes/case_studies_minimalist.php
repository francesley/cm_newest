<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/leafy/white_big.jpg" title="Minimalist style marquee" class="fancybox" rel="group" alt="Pond in marquee"><img src="/images/case/leafy/white.jpg" alt="Minimalist style marquee" width="273" height="186" /></a><p>Deliberately simple white marquee with a architectural leafy centrepiece</p></div>
<div class="case rightcase"><a href="/images/case/leafy/exterior_big.jpg" title="Marquee exterior lit up at night" class="fancybox" rel="group"><img src="/images/case/leafy/exterior.jpg" alt="Marquee exterior lit up at night" width="273" height="186" /></a><p>Colourful lighting creates a lively spectacle at night</p></div>
<div class="case"><a href="/images/case/leafy/marquee2_big.jpg" title="Marquee interior" class="fancybox" rel="group"><img src="/images/case/leafy/marquee2.jpg" alt="Marquee interior" width="273" height="186" /></a><p>Elegant white marquee interior</p></div>
<div class="case rightcase"><a href="/images/case/leafy/leafy_centrepiece_big.jpg" title="Detail of the leafy centrepiece" class="fancybox" rel="group"><img src="/images/case/leafy/leafy_centrepiece.jpg" alt="Detail of the leafy centrepiece" width="273" height="186" /></a><p>Detail of the leafy centrepiece showing the wire base and drop festoon lights</p></div>
<div class="case bottomcase"><a href="/images/case/leafy/ring_big.jpg" title="Illuminated floral ring" class="fancybox" rel="group"><img src="/images/case/leafy/ring.jpg" alt="Illuminated floral ring" width="273" height="186" loading="lazy" /></a><p>Beautiful illuminated ring of flowers</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/leafy/dancing_big.jpg" title="Enjoying the dancing" class="fancybox" rel="group"><img src="/images/case/leafy/dancing.jpg" alt="Enjoying the dancing" width="273" height="186" loading="lazy" /></a><p>Enjoying the dancing under coloured lighting and a glitter ball</p></div>
<hr/>
<p class="wide"><strong>The occasion</strong> was Ed and Krystyna's elegant, minimalist white wedding with an unusual rectangular leafy centrepiece suspended from the roof in Hartley Witney, Hampshire.</p>
<p class="wide"><strong>The marquee</strong> was a frame tent on a tennis court with room for dining and dancing areas.</p>
<p class="wide"><strong>Photos</strong> courtesy of <a href="https://nishahaqphotography.com/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/case/nishahaq'])" rel="noopener noreferrer">Nisha Haq</a>.</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a wedding marquee</p>
</div>
EOQ;
?>