<p class="wide">Stretch tents are made from a flexible, strong, waterproof <em>stretchy</em> fabric and can to be rigged in a variety of different shapes. Stretch tents are versatile in design and provide a stylish modern alternative to the traditional marquee.</p>

<h2>Features of stretch tents</h2>
	<div class="textPhotos">
	<img src="/images/pages/marquees/stretch_wedding.jpg"  alt="Stretch tent for a wedding" width="198" height="150"/>
	<p>Stetch tent wedding</p> 
	<img src="/images/pages/marquees/stretch_tent_playground.jpg"  alt="Stretch tent covering a playground" width="198" height="150"/> 
	<p>Garden stretch tent</p>  
	</div>
	<ul>
	<li><strong>Installed on any surface:</strong> including sand, gravel and concrete. Stretch tents can be secured by two different methods &ndash; rigged to buildings or trees, or attached to the ground by anchors, ballasts or pegs. <br><br>This means they can often be installed on more difficult terrain including on slopes and stairs.</li>
	<li><strong>Size:</strong> our stretch tents are 10m (33 feet) x 15m (50 feet).</li>
	<li><strong>Capacity:</strong> for a seated event with tables and dance floor, the maximum capacity is 100 guests. For a standing event, capacity is 150 guests.</li>
	</ul>
	<h2 id="small">Flexible use of space</h2>
	<div class="textPhotos">
	<img src="/images/pages/marquees/stretch_tent_floating.jpg"  alt="Stretch tent open sided canopy" width="198" height="120"/>
	<p>Open sided canopy</p> 
	<img src="/images/pages/marquees/stretch_tent_one_side_closed.jpg"  alt="Stretch tent with one side down" width="198" height="120"/> 
	<p>One side down</p> 
	<img src="/images/pages/marquees/stretch_tent_three_side_closed.jpg"  alt="Stretch tent with three sides down" width="198" height="120"/>
	<p>Three sides down</p> </div> 
	<ul>
	<li><strong>Adaptable:</strong> Because the shape of stretch marquees can alter, they can slip under overhanging shurubs and trees offering a more flexible alternative in town gardens borders. <br><br>The flexible shape also means you can adapt the tent to your party design.</li>
	<li><strong>Suitable for all weather:</strong> in warm weather, stretch tents can be assembled as an open sided canopy. In poor weather, all four sides of the tent come down to create an intimate and protected space. <br><br>Stretch tents can withstand winds of up to 50mph, snow and hail, and are fully waterproof.</li>
	</ul>
	

	<h2 id="small">Additional information</h2>
	<div class="textPhotos">
	<img src="/images/pages/marquees/stretch_tent_field.jpg"  alt="Stretch tent from the back" width="198" height="150"/>
	<p>From behind</p>
	</div> 
	<ul>
		<li><strong>Extras:</strong> tables, chairs, lighting and flooring of various kinds are available. Stretch tents cannot be lined however. Please see our <a href="furniture.htm">furniture</a>, <a href="lighting.htm">lighting</a> and <a href="decoration.htm">decoration</a> pages for more information.</li>
	<li><strong>Events:</strong> Stretch tents are well suited for weddings and parties. They also provide a stylish venue for a concert, festival or corporate event.</li>
	<li><strong>Safety:</strong> our stretch tents are fire resistant and fully comply with fire safety regulations.</li>
	</ul>
	<p class="endCall">Please <a href="/contact_us.htm">contact us</a> for more information.</p>
	</div>