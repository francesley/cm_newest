<p>Planning bar mitzvahs and bat mitzvahs presents special challenges.</p>
<div class="textPhotos byP"><a href="/case_studies/barmitzvah.html"><img src="/images/pages/barmitzvar/arthur.jpg" alt="Busy barmitzvar marquee"  width="198" height="150" /></a><p><a href="/case_studies/barmitzvah.html">View a case study &raquo;</a></p></div>
<p class="byphoto">Your approach will depend on the sort event you are planning &#8212; only for children or for a mixed age-group.</p>
<p class="byphoto">If only children are invited, then marquee aesthetics are less of a worry &#8212; though children do rise to the occasion of a well-dressed event. Bold or glitzy effects work better with youngsters than subtle decoration.</p>
<p>If a mixed age range is invited, very formal decoration is best avoided, as is an overly flowery look for a bar mitzvah.</p>
<h2>The advantages of marquees for bar and bat mitzvahs</h2>
<p>As for all types of event, hiring a marquee offers the advantage of a space that can be exactly tailored to the bar mitzvah or bat mitzvah that you desire.</p>
<div class="textPhotos byP"><img src="/images/pages/barmitzvar/permille.jpg" alt="Fun on the dancefloor" width="198" height="150"  loading="lazy" />
</div>
<p>Our marquees are modular, and can be large or small, L-shaped, square or rectangular, and doors and windows can be positioned anywhere. This means marquees can fit into awkward gardens and allow friends and family to be warmly welcomed into your home for this very personal event.</p>
<p class="wide">Adapting a marquee to an awkward garden is not a cheap option, but it can produce spectacular results. See <a href="../case_studies/barmitzvah.html">this case study </a> for an example at a North London bar mitzvah.</p>
<ul class="wide"><li>See <a href="../equipment/marquees.htm">marquees</a> for information about the different tents provided by County Marquees</li>
<li>Or check out <a href="../pricing/marquees.htm">marquee prices and sizes</a></li>
</ul>
<h2>Decoration and theming </h2>
<p>Our marquees are white and we provide ivory-coloured or black out linings. This is an ideal canvas for any decorative style. In addition, our aluminium framed marquees make it very easy to attach items to the walls or ceilings.</p>
<div class="textPhotos byP"><img src="/images/pages/barmitzvar/red_swag.jpg" alt="Marquee with red stripes"  width="198" height="150" loading="lazy" />
<img src="/images/pages/barmitzvar/ymca.jpg" alt="Dancing to yMca"  width="198" height="150" class="second" loading="lazy" /></div>
<p>Decorative options include:</p>
<ul>
<li>traditional &#8212; swags, flowers, balloons, banners etc</li>
<li>themed backdrops eg sports events,  beaches etc</li>
</ul>
<p>Combined with lighting effects, a vast range of styles are possible. We are happy to implement your chosen look, or to suggest ideas.</p>
<ul>
	<li>Please browse our <a href="/photos.htm?category=decoration">marquee decoration photos</a> for ideas</li>
	<li>Or see <a href="/equipment/decoration.htm">marquee decoration</a></li>
	<li>If you would like help, please <a href="/contact_us.htm">ask us</a>  </li>
</ul>
<h2>Suggestions for extras </h2>
<p>With so many young guests, extras to keep them entertained and keep the party going may be advisable. These are not a bad idea even for a mixed age range &#8212; excited children can help adults let their hair down too!</p>
<div class="textPhotos byP"><img src="/images/pages/barmitzvar/harry.jpg" alt="Bar mitzvah party novelties" width="198" height="150" /><img src="/images/pages/barmitzvar/sarah.jpg" alt="Bar mitzvah party novelties" width="198" height="150"  class="second"/></div>
<p class="byphoto">Here are some ideas: </p>
<ul>
<li>Chocolate Fountains</li>
<li>Candyfloss, popcorn machines </li>
<li><strong>Inexpensive:</strong> Dancing extras: eg selection of party novelties for dancing (as in the photo). </li>
<li><strong>Inexpensive:</strong> Dancing competitions</li>
<li>Entertainers: caricature artist, magicians etc </li>
<li>Personalised T-shirt printing </li>
</ul>
<h2>Lighting effects</h2>
<p>Lighting can produce some of the most exciting and creative marquee effects: subtle washes of colour, night skies, festoon lighting, spotlights for tables, or focal points. Clever use of lighting can really  create a fantastic atmosphere for a party.</p>
<div class="textPhotos byP"><a href="/photos/awkward_sitesReady.htm"><img src="/images/pages/barmitzvar/main_marquee.jpg" alt="Bar mitzvah marquee with night sky and coloured lighting"  width="198" height="150" loading="lazy" /></a><p><a href="/photos/awkward_sitesReady.htm">View larger &raquo;</a></p></div>
<p>County Marquees has a good range of lighting options &#8212; see <a href="../equipment/lighting.htm">marquee lighting</a>. Or for examples of some of the effects that can be achieved, please look at our <a href="/photos.htm?category=lighting">marquee lighting photos</a>.</p>
<h2>More than marquees</h2>
<p class="byphoto">We will happily provide just the marquee hire for an event. But we can organize much more if you wish.</p>
<div class="textPhotos byP"><img src="/images/pages/barmitzvar/giddy.jpg" alt="DJs at a barmitzvar" width="198" height="150" loading="lazy" /></div>
<p class="byphoto">We can manage catering, entertainment, decoration/theming etc - all to your specifications.</p>
<p class="byphoto">This can be a less stressful way to hold a bar or bat mitzvah.</p>
<p class="endCall">Please <a href="../contact_us.htm">contact us</a> if you would like to find out more about our  bat mitzvah and bar mitzvah marquees.</p>
</div>