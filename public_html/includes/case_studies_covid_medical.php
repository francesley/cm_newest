<?php
$content = <<<EOQ
<p class="wide">Temporary marquee structures can provide emergency medical space when triage areas, morgues testing centres or simply extra space is required due to Covid-19 pressures.</p>
<p class="wide">Marquees can slot into awkward spaces on hard standing, and can also be easily divided.</p>
<p class="wide">Below are examples of some of the emergency temporary structures County Marquees has installed to accomodate Covid-19 in medical settings.</p>
<p class="wide"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about a marquee to enable social distancing.</p>
<h2>Medical tent:</h2>
<div class="case-wrap">
<div class="case"> <a href="/images/case/social_distancing/medical_big.jpg" title="Dividing walls create convenient cubicles" class="fancybox" rel="group"><img src="/images/case/social_distancing/medical.jpg" alt="Dividing walls create convenient cubicles for a medical testing tent" width="400" height="300" /></a><p>Dividing walls create convenient cubicles for a medical testing tent</p></div>
<div class="case rightcase"><a href="/images/case/social_distancing/medical_outside_big.jpg" title="Exterior view" class="fancybox" rel="group"><img src="/images/case/social_distancing/medical_outside.jpg" alt="Exterior view" width="400" height="300" /></a><p>Exterior view</p></div>
<h2>Spillover staff area at a hospital</h2>
<div class="case bottomcase"> <a href="/images/case/social_distancing/hospital2_big.jpg" title="Dividing walls create convenient cubicles" class="fancybox" rel="group"><img src="/images/case/social_distancing/hospital2.jpg" alt="Dividing walls create convenient cubicles for a medical testing tent" width="400" height="300" loading="lazy" /></a><p>Extra space for hospital staff</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/social_distancing/hospital1_big.jpg" title="Exterior view" class="fancybox" rel="group"><img src="/images/case/social_distancing/hospital1.jpg" alt="Exterior view" width="400" height="300" loading="lazy" /></a><p>Exterior view</p></div>
</div></div>
EOQ;
?>