<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>

<div class="case"><img src="/images/case/ben_full_marquee.jpg" alt="Inside the marquee" width="273" height="186" /><p>Levelling off the sides of the garden allowed 100 guests to sit in a small area - the final effect was cosy but not cramped.<br/><a href="/photos/awkward_sitesReady.htm">View large photo</a></p></div>
<div class="case rightcase"><img src="/images/case/ben_conga.jpg" alt="Doing the conga" width="273" height="186" /><p>With the children's tables removed, there was plenty of room for a celebratory conga.</p></div>
<div class="case"><img src="/images/case/ben_toasting.jpg" alt="Toasting" width="273" height="186" /><p>Tables were sufficiently spread out to allow guests room to breathe - and toast!</p></div>
<div class="case rightcase"><img src="/images/case/ben_lighting.jpg" alt="Lighting alongside plants" width="273" height="186"/><p>Flowerbeds inside the marquee allowed  lights to be placed amongst plants, attractively illuminating the flowers and softening the effect of the lights themselves.</p></div>
<div class="case bottomcase"><img src="/images/case/ben_plants.jpg" alt="Dancing"  width="273" height="186" loading="lazy" /><p>Dancing in the shadow of greenery.</p></div>
<div class="case rightcase bottomcase"><img src="/images/case/ben_oldies.jpg" alt="In the L-shape of the marquee extension" width="273" height="186" loading="lazy" /><p>An L-shaped extension to the marquee accomodated an extra table</p></div>
<hr>
<p class="wide"><strong>The occasion:</strong> a barmitzvah party for 100 people.</p>
<p class="wide"><strong>The venue:</strong> a small to medium sized London garden with sloping flower beds all around.</p>
<p class="wide"><strong>The challenge:</strong> to accomodate 100 guests all sitting at tables within the space, and to allow for dancing.</p>
<p class="wide"><strong>County Marquees' solution:</strong> level the sloping edges of the garden so a marquee could be built around the flower beds. The marquee could then take advantage of every bit of available space. An L-shaped extension into the garden's patio area allowed another table to be accomodated. Children's tables were placed on top of a dance floor and removed when dancing started.</p>
<p class="wide abovecase">See more real marquees for <a href="/case_studies/small.htm">small</a>, <a href="/case_studies/babyshower.htm">thin</a> gardens.</p>

<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you have an awkward garden and would like to find out about marquee options. <span style="font-weight:normal">But <strong>please note:</strong> altering the surface of the garden in this way is extra work and does incur extra cost.</span></p>
</div>
EOQ;
?>