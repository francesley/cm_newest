<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/maya_gus/interior_big.jpg" title="Pretty, relaxed interior decor with candles" class="fancybox" rel="group" alt="Pond in marquee"><img src="/images/case/maya_gus/interior.jpg" alt="Pretty relaxed marquee decor" width="273" height="186" /></a><p>Pretty, relaxed interior decor with candles</p></div>
<div class="case rightcase"><a href="/images/case/maya_gus/pinapples_big.jpg" title="Unusual hanging decoration: ferns in pots and hoops" class="fancybox" rel="group"><img src="/images/case/maya_gus/pinapples.jpg" alt="Unusual hanging decoration: ferns in pots and hoops" width="273" height="186" /></a><p>Unusual hanging decoration: ferns in pots (don't they look like pinapples?) and different sized hoops hanging from a leafy pole. Very distinctive.</p></div>
<div class="case"><a href="/images/case/maya_gus/setting_big.jpg" title="Traditional tent at a beautiful lakeside site" class="fancybox" rel="group"><img src="/images/case/maya_gus/setting.jpg" alt="Traditional tent at a beautiful lakeside site" width="273" height="186" /></a><p>Traditional tent at a beautiful lakeside site in Surrey</p>
</div>
<div class="case rightcase"><a href="/images/case/maya_gus/chinese_hats_big.jpg" title="Chinese hat tents in the area alongside the main marquee for drinking and chilling" class="fancybox" rel="group"><img src="/images/case/maya_gus/chinese_hats.jpg" alt="Chinese hat tents in the area alongside the main marquee" width="273" height="186" /></a><p>Chinese hat tents in the area alongside the main marquee for drinking and chilling</p></div>
<div class="case bottomcase"><a href="/images/case/maya_gus/ring_big.jpg" title="Beautiful floral ring framing the lake view from the open side of a Chinese hat tent" class="fancybox" rel="group"><img src="/images/case/maya_gus/ring.jpg" alt="Beautiful floral ring framing the lake view from the open side of a Chinese hat tent" width="273" height="186" loading="lazy" /></a><p>A beautiful floral ring framing the lake view from the open side of a Chinese hat tent</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/maya_gus/found_big.jpg" title="Imaginative, pretty table plan using an old wooden step ladder sets the scene" class="fancybox" rel="group"><img src="/images/case/maya_gus/found.jpg" alt="Imaginative, pretty table plan" width="273" height="186" loading="lazy" /></a><p>Imaginative, pretty table plan using an old wooden step ladder sets the scene</p></div>
<hr/>
<p class="wide"><strong>The occasion</strong> was Maya and Gus's very pretty wedding with vintage details and unusual hanging ferns.</p>
<p class="wide"><strong>The tents:</strong> a traditional style tent for the main event and three Chinese hats for an adjoining chilling area.</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a wedding marquee</p>
</div>
EOQ;
?>