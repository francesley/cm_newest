<ul class="clients long">
      <li>Alliance Francais</li>
      <li>Amalgamated Plastics</li>
      <li>Arlington Hotel  </li>
      <li>Bank Indonesia</li>
      <li>Bank Nigara</li>
      <li>Bank of England </li>
      <li>Barnet Borough Council</li>
      <li>BBC</li>
	  <li>Belsize Square Synagogue</li>
      <li>Big Green Door</li>
      <li>Black Spider Technologies  </li>
      <li>Boosey &amp; Hawkes</li>
      <li>Borehamwood Synagogue </li>
      <li>Born Free Foundation</li>
      <li>Borough of Brent</li>
      <li>Brasserie Julius</li>
	  <li>British Museum</li>
      <li>British Rowing</li>
      <li>Brittania Club  </li>
      <li>Chase Farm Hospital</li>
      <li>Childsplay Productions </li>	  
	  <li>Christchurch College, Oxford</li>
      <li>Chrysalis TV Group</li>
      <li>Colliers Conrad Ritblat Erdman</li>
      <li>Consumers Association</li>
      <li>Corporate Rewards</li>
      <li>Cranford Community College </li>
      <li>Cumberland Lawn Tennis Club</li>
      <li>Dominion Mosaic</li>
      <li>DNA Group  </li>
      <li>Ebury Mathiot </li>
      <li>Euro Car Parts</li>
      <li>Finnish Embassy</li>
      <li>Fisher Outdoor Leisure</li>
      <li>Fresh Tracks </li>
      <li>Fulham Golf Club</li>
      <li>Fulham Preparatory School </li>
      <li>GlaxoSmithKline</li>
      <li>Globe Restaurants</li>
      <li>Gough Square Chambers </li>
      <li>Granada TV  </li>
      <li>Grand Design</li>
      <li>Grant Thornton </li>
      <li>Griffin Inn</li>
      <li>Guildhall Brokers </li>
      <li>Guards and Cavalry Club </li>
      <li>Harper Collins</li>
      <li>Harrow School</li>
      <li>Haverstock School </li>
      <li>Haven Bistro and Bar  </li>
      <li>Herbert Smith</li>
      <li>Hercules Unit Trust </li>
    </ul>
    <ul class="clients right long">
      <li>Hertfordshire Golf Club </li>
      <li>High Style Furnishings </li>	  
      <li>Highgate School</li>
      <li>Holstein Friesian Society</li>
      <li>Hustler Hollywood </li>
      <li>Indonesian Embassy</li>
      <li>Jack Morton Worldwide </li>
      <li>Juvenile Diabetes Research Foundation</li>
      <li>King's Troop</li>
      <li>Logica</li>
      <li>London Golf Classic</li>
      <li>London Zoo </li>
      <li>Lovells</li>
      <li>McCormick UK </li>
      <li>Middlesex University</li>
      <li>Miller Fine Arts  </li>
      <li>Newham Hospital</li>
      <li>NSPCC</li>
      <li>Oakington Manor School</li>
      <li>Peabody Trust</li>
      <li>Pinner Hill Golf Course</li>
      <li>PML Consultants</li>
      <li>Price &amp; Myers</li>
      <li>Preston Manor High School   </li>
      <li>Punch Taverns</li>
      <li>Retail Trust  </li>
      <li>Price Waterhouse</li>
      <li>Royal Institute of British Architects (RIBA) </li>
      <li>Royal Institute of Civil Engineers</li>
      <li>Sarasin &amp; Partners LLP</li>
      <li>Sidesaddles</li>
      <li>Snowgold Leisure Group</li>
      <li>Solomon Taylor & Shaw</li>
      <li>South London Gallery </li>
      <li>St George's Cathedral</li>
      <li>St Marylebone Society</li>
      <li>Stag Tool Hire</li>
      <li>Street League</li>
      <li>Tewinbury Farm Hotel  </li>
      <li>Thames &amp; Hudson</li>
	  <li>Trinity College, Oxford</li>
      <li>Totteridge Cricket club </li>
      <li>Tradewise Insurance Sevices</li>
      <li>Venner Shipley</li>
      <li>Venus Asset Management </li>
      <li>Virgin</li>
      <li>Ukrainian Embassy</li>
	  <li>University College School</li>
      <li>University of Middlesex</li>
      <li>University of Westminster</li>
    </ul>
	</div>