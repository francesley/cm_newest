<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/jade/whole_scene_big.jpg" title="Festival style wedding with a traditional marquee, fun fair stalls and an ice cream van" class="fancybox" rel="group"><img src="/images/case/jade/whole_scene.jpg" alt="Festival style wedding marquee" width="273" height="186" /></a><p>Festival style wedding with a traditional marquee, fun fair stalls and an ice cream van</p></div>
<div class="case rightcase"><a href="/images/case/jade/night_big.jpg" title="Marquee exterior at night with festoon lighting and vintage style lampshades" class="fancybox" rel="group"><img src="/images/case/jade/night.jpg" alt="Marquee exterior with outdoorlighting" width="273" height="186" /></a><p>Marquee exterior at night with festoon lighting and vintage style lampshades</p></div>
<div class="case"><a href="/images/case/jade/candlelight_big.jpg" title="The vintage style reception in the marquee looks so so romantic by candlelight" class="fancybox" rel="group"><img src="/images/case/jade/candlelight.jpg" alt="Vintage style wedding reception romantic by candlelight" width="273" height="186" /></a><p>The vintage style reception in the marquee looks so so romantic by candlelight</p></div>
<div class="case rightcase"><a href="/images/case/jade/vintage_big.jpg" title="Vintage style reception by day" class="fancybox" rel="group"><img src="/images/case/jade/vintage.jpg" alt="Vintage style wedding reception" width="273" height="186" /></a><p>Vintage style reception by day</p></div>
<div class="case"><a href="/images/case/jade/ice_cream_van_big.jpg" title="Groom Martin serves bride Jade with an ice cream" class="fancybox" rel="group"><img src="/images/case/jade/ice_cream_van.jpg" alt="Ice cream van" width="273" height="186" loading="lazy" /></a><p>Groom Martin serves bride Jade with an ice cream</p></div>
<div class="case rightcase"><a href="/images/case/jade/shooting_big.jpg" title="Fairground shooting arcade" class="fancybox" rel="group"><img src="/images/case/jade/shooting.jpg" alt="Fairground shooting arcade" width="273" height="186" loading="lazy" /></a><p>Fairground shooting arcade.</p></div>
<div class="case"><a href="/images/case/jade/table_details_big.jpg" title="Vintage table decoration: mismatched crockery, candles in bottles, flowers in jam jars...." class="fancybox" rel="group"><img src="/images/case/jade/table_details.jpg" alt="Vintage table decoration" width="273" height="186" loading="lazy" /></a><p>Vintage table decoration: mismatched crockery, candles in bottles, flowers in jam jars...</p></div>
<div class="case rightcase"><a href="/images/case/jade/cup_big.jpg" title="Cup on a candlestick!" class="fancybox" rel="group"><img src="/images/case/jade/cup.jpg" alt="Cup on a candlestick" width="273" height="186" loading="lazy" /></a><p>Cup on a candlestick!</p></div>
<div class="case"><a href="/images/case/jade/sweeties_big.jpg" title="Sweetie table" class="fancybox" rel="group"><img src="/images/case/jade/sweeties.jpg" alt="Sweetie table" width="273" height="186" loading="lazy" /></a><p>Sweetie table</p></div>
<div class="case rightcase"><a href="/images/case/jade/cake_big.jpg" title="Natural wedding cake" class="fancybox" rel="group"><img src="/images/case/jade/cake.jpg" alt="Natural wedding cake" width="273" height="186" loading="lazy" /></a><p>Natural wedding cake</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a festival style summer wedding reception.</p>
<p class="wide"><strong>The venue</strong> was <a href="https://www.brookfarmcuffley.co.uk/" target="_blank" rel="noreferrer noopener">Brook Farm Cuffley</a>, a nice flat rural venue near Potters Bar, London.</p>
<p class="wide"><strong>The style</strong> was fairground and rustic: funfair games, mini golf, an ice cream van, mismatched teacups, lampshades, and homemade bunting and table runners.</p>
<p class="wide"><strong>The result</strong> was an amazing event, both fun and romantic &ndash; a tribute to bride Jade's inspirational ideas.</p>
<p class="wide abovecase">Photos courtesy of <a href="https://www.rebeccafamilyphotography.co.uk/" target="_blank" rel="noreferrer noopener">Rebecca Family Photography</a>.</p>

<p class="endCall"><a href="../contact_us.htm">Contact us</a> for more about festival style wedding marquees.</p>
</div>
EOQ;
?>