<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/tayla/tables_big.jpg" title="Ready for the reception. The style is simple and elegant, mainly white with classic decor." class="fancybox" rel="group"><img src="/images/case/tayla/tables.jpg" alt="Marquee decorated ready for the reception" width="273" height="186" /></a><p>Ready for the reception. The style is simple and elegant, mainly white with classic decor. </p></div>
<div class="case rightcase"><a href="/images/case/tayla/table_decoration_big.jpg" title="Table decorations: classic flowers and long elegant candlesticks" class="fancybox" rel="group"><img src="/images/case/tayla/table_decoration.jpg" alt="Marquee table decoration" width="273" height="186" /></a><p>Table decorations: classic flowers and long elegant candlesticks.</p></div>
<div class="case"><a href="/images/case/tayla/walking_big.jpg" title="Bride and groom walk to make their entrance as husband and wife" class="fancybox" rel="group"><img src="/images/case/tayla/walking.jpg" alt="Bride and groom walk to their marquee" width="273" height="186" /></a><p>Bride and groom walk to make their entrance as husband and wife.</p></div>
<div class="case rightcase"><a href="/images/case/tayla/toasting_big.jpg" title="Toasting" class="fancybox" rel="group"><img src="/images/case/tayla/toasting.jpg" alt="Toasting" width="273" height="186" /></a><p>Toasting</p></div>
<div class="case"><a href="/images/case/tayla/all_big.jpg" title="All guests wave outside a partially transparent marquee &ndash; some of the benefits of sky views without the risk of overheating" class="fancybox" rel="group"><img src="/images/case/tayla/all.jpg" alt="All guests wave outside the marquee" width="273" height="186" /></a><p>All guests wave outside </p></div>
<div class="case rightcase"><a href="/images/case/tayla/cake_big.jpg" title="The cake continues with the simple and elegant style" class="fancybox" rel="group"><img src="/images/case/tayla/cake.jpg" alt="Lunching inside the marquee" width="273" height="186" /></a><p>The cake continues with the simple and elegant style</p></div>
<div class="case"><a href="/images/case/tayla/moment_big.jpg" title="Enjoying the moment" class="fancybox" rel="group"><img src="/images/case/tayla/moment.jpg" alt="Inside an illuminated marquee" width="273" height="186" /></a><p>Enjoying the moment</p></div>
<div class="case rightcase"><a href="/images/case/tayla/confetti_big.jpg" title="A shower of glittery confetti" class="fancybox" rel="group"><img src="/images/case/tayla/confetti.jpg" alt="Bride and groom dance in a shower of glittery confetti" width="273" height="186" /></a><p>A shower of glittery confetti</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a summer wedding reception.</p>
<p class="wide"><strong>The venue</strong> was the family's garden.</p>
<p class="wide"><strong>The marquee:</strong> a 12m x 21m frame marquee with clear panoramic windows on one long side, one clear gable end, and a partially transparent roof to give some of the benefits of sky views without the risk of overheating. Two Chinese hat tents were used in other parts of the garden.</p>
<p class="wide"><strong>The interior:</strong> round tables, chiavari chairs, a white starlight dance floor and a twinkly fairy light canopy strung across the whole roof.</p>
<p class="wide"><em>All photos courtesy of <a href="https://www.lauramarthaphotography.com/" target="_blank" rel="noopenener noreferrer">Laura Martha Photography</a></em></p>
<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about celebrating a wedding in a marquee.</p>
</div>
EOQ;
?>