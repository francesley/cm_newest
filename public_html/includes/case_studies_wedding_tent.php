<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/nancy/interior_big.jpg" title="Tent interior ready for the reception: at the far end, a dance floor with starlight ceiling above" class="fancybox" rel="group"><img src="/images/case/nancy/interior.jpg" alt="Guests watch the wedding ceremony" width="273" height="186" /></a><p>Tent interior ready for the reception: at the far end, a dance floor with starlight ceiling </p></div>
<div class="case rightcase"><a href="/images/case/nancy/interior2_big.jpg" title="The two sections of the wedding tent: reception area and, on the left, the chillout section. Panoramic windows all round exposed gorgeous views and created a bright space" class="fancybox" rel="group"><img src="/images/case/nancy/interior2.jpg" alt="Two sections of the wedding tent" width="273" height="186" /></a><p>The two sections of the wedding tent: reception area right, and chillout area left. Panoramic windows all round exposed gorgeous views and created a bright space</p></div>

<div class="case"><a href="/images/case/nancy/vintage_big.jpg" title="Vintage items in the entrance porch &#8212; a pointy topped Chinese Hat tent" class="fancybox" rel="group"><img src="/images/case/nancy/vintage.jpg" alt="Vintage items" width="273" height="186" /></a><p>Vintage items in the entrance porch &#8212; a pointy topped Chinese Hat tent</p></div>
<div class="case rightcase"><a href="/images/case/nancy/chillout_big.jpg" title="Chillout section with vintage furniture" class="fancybox" rel="group"><img src="/images/case/nancy/chillout.jpg" alt="Chillout tent section" width="273" height="186" /></a><p>Chillout section with vintage furniture</p></div>

<div class="case"><a href="/images/case/nancy/walkway_big.jpg" title="Guest streaming into the marquee through a long walkway" class="fancybox" rel="group"><img src="/images/case/nancy/walkway.jpg" alt="A long walkway" width="273" height="186" /></a><p>Guest streaming into the marquee through a long walkway</p></div>
<div class="case rightcase"><a href="/images/case/nancy/interior_full2_big.jpg" title="Tent wedding recention in full swing" class="fancybox" rel="group"><img src="/images/case/nancy/interior_full2.jpg" alt="Wedding breakfast in full swing" width="273" height="186" /></a><p>Tent wedding reception in full swing</p></div>

<div class="case"><a href="/images/case/nancy/evening_big.jpg" title="Marquee lighting comes into its own as day turns to night" class="fancybox" rel="group"><img src="/images/case/nancy/evening.jpg" alt="Marquee lighting" width="273" height="186" /></a><p>Marquee lighting comes into its own as day turns to night.</div>
<div class="case rightcase"><a href="/images/case/nancy/dance_big.jpg" title="Bride and groom open the dancing in the dance section, which has a whole different feel thanks to the starlight ceiling" class="fancybox" rel="group"><img src="/images/case/nancy/dance.jpg" alt="Dance section of the wedding tent" width="273" height="186" /></a><p>Bride and groom open the dancing in the dance section, which has a whole different feel thanks to the starlight ceiling</div>

<div class="case"><a href="/images/case/nancy/heart_big.jpg" title="Pretty heart decoration" class="fancybox" rel="group"><img src="/images/case/nancy/heart.jpg" alt="Pretty heart decoration" width="273" height="186" /></a><p>Pretty heart decoration</p></div>
<div class="case rightcase"><a href="/images/case/nancy/centrepiece2_big.jpg" title="Floral centrepiece with James Bond themed table names" class="fancybox" rel="group"><img src="/images/case/nancy/centrepiece2.jpg" alt="Floral centrepiece with James Bond themed table names" width="273" height="186" /></a><p>Floral centrepiece with James Bond themed table names</a></p></div>

<div class="case"><a href="/images/case/nancy/exterior_big.jpg" title="Marquee exterior" class="fancybox" rel="group"><img src="/images/case/nancy/exterior.jpg" alt="Marquee exterior" width="273" height="186" /></a><p>Marquee exterior</p></div>
<div class="case rightcase"><a href="/images/case/nancy/before_big.jpg" title="The before photo, with panoramic windows to show off the view" class="fancybox" rel="group"><img src="/images/case/nancy/before.jpg" alt="Before" width="273" height="186" /></a><p>The before photo, with panoramic windows to show off the view</p></div>

<div class="case"><a href="/images/case/nancy/cateringtent_big.jpg" title="Caterers Cavendish Events working hard in the catering tent" class="fancybox" rel="group"><img src="/images/case/nancy/cateringtent.jpg" alt="Marquee exterior" width="273" height="186" /></a><p>Caterers <a href="https://www.cavendishevents.com/" target="_blank">Cavendish Events</a> working hard in the catering tent</p></div>
<div class="case rightcase"><a href="/images/case/nancy/paella_big.jpg" title="Caterers Cavendish Events preparing a giant Paella" class="fancybox" rel="group"><img src="/images/case/nancy/paella.jpg" alt="Preparing a Paella" width="273" height="186" /></a><p>Cavendish Events preparing a giant paella</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a wedding reception with dancing.</p>
<p class="wide"><strong>The challenge</strong> was to create spaces for a wedding breakfast, for dancing and for a chillout section; and also to take advantage of beautiful views at <a href="https://www.hampdenweddings.com/" target="_blank">Hampden House</a> near High Wycombe, Buckinghamshire. The weather in October was also a concern, as the marquee was at some distance from the house.</p>
<p class="wide"><strong>The solution:</strong> first, two clearspan marquees were joined to create two separate sections &#8212; one for the wedding breakfast, and one for a chillout / reception area.</p>
<p class="wide">Then, the main dining section was split again by a <em>reveal screen</em> into two more sections &#8212; one for dining and one for dancing. The dance section &#8212; lent a different sparkly character by a <em>starlight ceiling</em> &#8212; was hidden behind the <em>reveal screen</em> during the meal for a surprise element.</p>
<p class="wide">Clear panoramic windows on three external sides of the structure took advantage of the views and created a light, airy feel.</p>
<p class="wide">As for the weather, an extra-long walkway with matting to cover the ground was set up at the last minute when the weather forecast looked uncertain to protect guests walking to the wedding tent.</p>
<p class="wide abovecase">The best photos courtesy of <a href="https://www.guyhearn.com/" target="_blank"  onclick="_gaq.push(['_trackPageview', '/outgoing/case/guy_hearn'])">Guy Hearn</a></p>
<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about wedding tent hire.</p>
</div>
EOQ;
?>