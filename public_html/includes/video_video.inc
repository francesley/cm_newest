<iframe width="420" height="315" src="https://www.youtube.com/embed/m9BOsHVttYI" frameborder="0" allowfullscreen id="timelapse"></iframe>


 <h1>Time-lapse marquee video</h1>                      
                      <p>Putting up a marquee in 30 seconds and a quick glimpse of the almost-finished results.</p>
                      <p>Sadly,  plastic wrap still covered the tables when we left and County Marquees' staff and equipment do make the odd unsightly appearance in the video.</p>
                      <p>But scrubbed up and with full lighting effects, you will have to take it from us that the marquee looked magnificent. </p>