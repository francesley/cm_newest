<p>Marquee furniture and equipment available from County Marquees includes:</p>
<div class="textPhotos byP">
<img src="/images/pages/furniture/marquee_furniture.jpg" alt="Assorted marquee furniture" width="198" height="759" />
<p>In the photo from top: <em>trestle tables &amp; limewashed chiavari chairs, clear wall &amp; roof panels, vintage bar, vintage trestle tables, limewashed chiavari chairs, chair covers &amp; bows, sofas &amp; low tables, poseur tables &amp; matching chairs, folding wooden chairs, poseur tables, sofas &amp; soft cube seating</em></p> 
</div>
<ul class="long">
<li>Round tables - 3', 4', 5', 5'6&quot;, 6'</li>
<li>Trestle table - 6' x 2'3&quot;, 6' x 3'</li>
<li>Vintage style trestle in rich, dark wood - 6' x 3'</li>
<li>Low tables</li>
<li>Wooden folding chairs </li>
<li>Limewashed chiavari chairs in a range of colours</li>	
<li>Poseur tables, caf&eacute; tables</li>
<li>Banqueting chairs in gilt or other colours </li>
<li>Mix and match boho chairs</li>
<li>Plastic <em>bistro</em> chairs</li>    
<li>Dance floors - wooden, parquet, black &amp; white and white starlight</li>    
<li>Soft cube seating</li>
<li>Sofas</li>
<li>Bar units in different styles</li>
<li>Stage units</li>
<li>Coat rails</li>
<li>Picket fencing</li>
<li><em>Georgian</em> windows</li>
<li>Clear panoramic window panels</li>
<li>Clear roof panels</li>
<li>Hard doors</li>
<li>Heaters</li>
<li>Generators</li>
<li>Portable toilets</li>
</ul>
<p>For a more extensive list, including pricing, please see our price lists for <a href="/pricing/furniture.htm">furniture</a> and <a href="/pricing/equipment.htm">equipment</a>.</p>
<p>Other furniture and equipment can  easily be sourced if required.</p>
<p>To see more photo examples of County Marquees' furniture, see <a href="http://www.pinterest.com/county_marquees/marquee-furniture-equipment/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/furniture/pinterest']);">this marquee furniture album</a> on Pinterest.</p>
<div class="unnec">&nbsp;</div>
<h2>Dance floors:</h2>
<div class="textPhotos">
<img src="/images/pages/furniture/bw_dance.jpg" alt="Black and white dance floor" width="198" height="150"  loading="lazy" />	
<img src="/images/pages/furniture/starlight_floor.jpg" alt="White starlight dance floor" width="198" height="150" class="second" loading="lazy" />	
</div>
<ul>
  <li><strong>Wooden dance floors</strong> - interlocking oak plywood. <em>Standard</em> dance floors, cover large areas easily. See <a href="/photos/marquee-garden2.htm">this photo</a> or <a href="/photos/dance_lighting.htm">this photo</a> for examples.</li>
  <li><strong>Parquet dance floors</strong> - the classic look. See <a href="/photos/prewedding.htm">this photo</a> for a slightly covered up example.</li> 
  <li><strong>Black and white dance floors</strong> - shown to the right. Fashionable and striking, ideal when an event calls for extra <em>oomph</em>, particularly with lighting effects. See <a href="/photos/lighting-effects.htm">this photo</a> for a stunning example. These dance floors also work well in retro themed marquees.</li>
  <li><strong>White starlight dance floors</strong> - shown in the lower photo. Very popular recently. They look both pretty and exciting so are an ideal dance floor for a wedding reception.</li>
</ul>
<div class="unnec"></div>
<h2>Flooring:</h2>
<div class="textPhotos">
<img src="/images/pages/furniture/matting.jpg" alt="Marquee with coconut matting" width="198" height="150"  loading="lazy" />
<img src="/images/pages/furniture/carpet.jpg" alt="Marquee with blue carpet" width="198" height="150" class="second" loading="lazy" />
</div>
<ul>
<li><strong>Coconut matting</strong>: This simple, stylish floor covering (illustrated alongside) is suitable 
for informal summer events.</li>
<li><strong>Hard floor</strong>:&nbsp;Made out of interlocking plywood panels, hard flooring
is often covered by carpet. It is perfect for formal events, and for winter parties, when
protection against waterlogged ground may be necessary.</li>
<li><strong>Carpet: </strong>contract carpet in a wide range of colours is typically used to cover hard floors, but can overlay matting as well for a more formal style. The lower photo shows popular champagne coloured carpet under tables . See <a href="/photos/asian.htm">this photo</a> of an Asian wedding for a larger example.</li>
</ul>  
<div class="unnec"></div>
<h2>Portable toilets:</h2>
<div class="textPhotos">
<img src="/images/pages/furniture/portable_loo.jpg" alt="Luxury portable toilets" width="198" height="150" loading="lazy" />
</div>
<p>County Marquees supplies luxury portable toilets that are spacious and well equipped, providing elegant facilities for all kinds of events. Interiors are tastefully decorated, with cream walls and floors, inset lighting, and wood and chrome fittings.</p>
<p></p>
<div class="unnec"></div>
<h2>Furniture planning:</h2>
<div class="textPhotos">
<a href="/photos/fresh.htm"><img src="/images/pages/furniture/sofas.jpg" alt="Marquee with sofas, tables and cube seating" width="198" height="150"  loading="lazy" /></a>
<p class="view"><a href="/photos/fresh.htm">View larger</a></p>
<a href="/photos/prewedding.htm"><img src="/images/pages/furniture/indian_furniture.jpg" alt="Indian style furniture" width="198" height="150" loading="lazy" /></a>
<p class="view"><a href="/photos/prewedding.htm">View larger</a></p>
</div>
<p>County Marquees is happy to assist with planning the style and the layout of furniture.</p>
<p>For many events, our standard furniture range is sufficient to create the right atmosphere &#8212; banqueting chairs with round tables for  formal functions, limewashed chiavari or bistro chairs with long trestle tables for more casual parties and so on.</p>
<p >But County Marquees is also happy to source other furniture when necessary. Please <a href="../contact_us.htm">contact us</a> with any special requirements.</p>
<p><strong>And please don't hesitate to <a href="/contact_us.htm">contact us</a> for more advice.</strong></p>

</div>