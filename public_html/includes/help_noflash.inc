         <p class="wide">Sorry. The marquee planner was written for the Flash player and that no longer works on the web.</p>
	      <p class="wide">We are planning to rewrite the planner in more current web technologies but that is unfortunately a long term project.</p>
	      <p class="wide">If you are trying to find out how big a marquee you will need, our <a href="/marquee_essentials.html#size">Where to Start</a> page suggests several methods to answer just that question.</p>
</div>