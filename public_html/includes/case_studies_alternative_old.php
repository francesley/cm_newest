<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><img src="/images/case/festival/flags.jpg" alt="Festival style marquees" width="273" height="186" /><p>Brightly coloured flags led the way to the festival style marquees.</p></div>
<div class="case rightcase"><img src="/images/case/festival/exterior_brighter.jpg" alt="Marquee for an informal party" width="273" height="186" /><p>The main marquee and one of the smaller marquees.  Bollywood movies played from the screen on the far left.</p></div>
<div class="case"><img src="/images/case/festival/cool_marquee.jpg" alt="Dining in unusual marquees" width="273" height="186" /><p>The main marquee with alternative marquee decoration: dangling sari fabric, Indian-style decorative umbrellas and peackock feather table centrepieces.</p></div>
<div class="case rightcase"><img src="/images/case/festival/dining.jpg" alt="Dining in the marquee" width="273" height="186" /><p>Long trestle tables lined three sides of the marquee, leaving the central space open.</p></div>
<div class="case"><img src="/images/case/festival/drinks_marquee.jpg" alt="Inside the drinks tent" width="273" height="186" loading-"lazy" /><p>Inside the drinks tent, decorated with Eastern fabrics.</p></div>
<div class="case rightcase"><img src="/images/case/festival/chillout.jpg" alt="Small chillout tent with alternative decoration" width="273" height="186" loading-"lazy" /><p>Small chillout tent with hay bale seating and lots of tea lights.</p></div>

<div class="case"><img src="/images/case/festival/fire.jpg" alt="Fire outside" width="273" height="186" loading-"lazy" /><p>Keeping warm from the fire outside the marquees.</p></div>
<div class="case rightcase"><img src="/images/case/festival/eating_blue.jpg" alt="Marquee dining" width="273" height="186" /><p>Colourful lighting and table runners improvised from sari fabric.</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a pretty funky night time party at garden art manufacturer <a href="http://www.davidharber.co.uk" target="_blank">David Harber</a> near Didcot, Oxfordshire</p>
<p class="wide"><strong>The venue</strong> was a a fair sized field adjoining David Harber's workshop.</p>
<p class="wide"><strong>The challenge:</strong> the client wanted a festival style marquee &#8212; an informal, Eastern/hippy-themed event with relaxed and alternative marquee decoration, with different locations for eating, dancing, chilling and generally hanging out. And for guests of all ages.</p>
<p class="wide"><strong>County Marquees solution:</strong> a 12m x 9m (40' by 30') frame marquee with several smaller frame marquees to serve for chilling and quieter seating. Eastern fabrics and other items were used for the decoration, with colourful lighting to snaz things up.</p>
<p class="wide abovecase">If you are looking for party tent ideas, it is worth noting that this funky look was acheived <strong>inexpensively</strong>. Sari fabric, bought from Southall in London, dangled from the roof and was used as table runners. Other decorative items &#8211; also from Southall &#8211; included peacock feathers, tinselly wall hangings and lots of tea lights. Colourful lighting completed the look.</p>
<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about a marquee party decorated in alternative style.</p>
</div>
EOQ;
?>