<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><img src="/images/case/kathy_inside.jpg" alt="Eight foot wide steps" width="273" height="186" /><p>The legs of the marquee needed to drop right down on either side of the eight foot wide steps.</p></div>
<div class="case rightcase"><img src="/images/case/kathy_steps.jpg" alt="Pillars on either side of the steps" width="273" height="186" /><p>The large pillars on either side of the steps also had to be accommodated.</p></div>
<div class="case"><img src="/images/case/kathy_marquee.jpg" alt="Flexible framed marquee" width="273" height="186" /><p>The marquee bowed slightly in the middle, where the wide steps called for deep leg drops. Frame marquees can accommodate some bowing.</p></div>
<div class="case rightcase"><img src="/images/case/kathy_to_garden.jpg" alt="Garden showing through open walls of the marquee" width="273" height="186" /><p>Looking out to the garden before the birthday party. Later, for the party, green parcan lighting illuminated the garden's charm against the darkness.</p></div>	
<div class="case bottomcase"><img src="/images/case/kathy_rosyPeople.jpg" alt="Inside the birthday party marquee" width="273" height="186" loading="lazy" /><p>Pink parcan lighting lent a rosy glow. Sadly the photos don't do justice to the finished magical effect of the parcans combined with 100 tea lights twinkling round the steps &amp; through the marquee and greenery.</p></div>
<div class="case rightcase bottomcase"><img src="/images/case/kathy_night.jpg" alt="Happy 50th birthday" width="273" height="186" loading="lazy" /><p>Can this really be a 50th birthday party?   She only looks 40! A faint impression of the final magical effect can be glimpsed behind.</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a 50th birthday party for 100 people.</p>
<p class="wide"><strong>The venue</strong> was a medium sized London garden with a narrow low lying patio with wide steps up to  an irregular terrace and a steeply-sloping lawn.</p>
<p class="wide"><strong>The challenge</strong> was to erect a marquee structure that abutted the house and gave protection from the elements whilst at the same time setting off the charms of the different levels of the terraces and the sloping garden behind. The marquee was to serve as a chill-out and dancing area, with food being served inside the house.</p>
<p class="wide abovecase"><strong>County Marquees' solution</strong> was an L-shaped marquee covering the patio and climbing up the steps to the terrace with the help of different lengths of legs to support the marquee.</p>

<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about celebrating a birthday in a marquee.</p>
</div>
EOQ;
?>