<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/garden_wedding/setting_big.jpg" title="The perfect ingredients for an outdoor marquee wedding" class="fancybox" rel="group"><img src="/images/case/garden_wedding/setting.jpg" alt="The perfect ingredients for an outdoor marquee wedding" width="273" height="186" /></a><p>One large traditional marquee, a couple of small frame marquees, rustic outdoor furniture and a beautiful and spacious garden: the perfect ingredients for an outdoor marquee wedding</p></div>
<div class="case rightcase"><a href="/images/case/garden_wedding/side_view_big.jpg" title="Flowers and candles decorate the open side of the main marquee" class="fancybox" rel="group"><img src="/images/case/garden_wedding/side_view.jpg" alt="Flowers and candles decorate the open side of the main marquee" width="273" height="186" /></a><p>Decorating the long open side of the main marquee were candles, romantic flowers, and a pretty table plan with photos</p></div>
<div class="case"><a href="/images/case/garden_wedding/leafy_big.jpg" title="Reception marquee by day" class="fancybox" rel="group"><img src="/images/case/garden_wedding/leafy.jpg" alt="Reception marquee by day" width="273" height="186" /></a><p>The reception marquee by day</p></div>
<div class="case rightcase"><a href="/images/case/garden_wedding/interior_big.jpg" class="fancybox" rel="group" title="Inside the main marquee"/><img src="/images/case/garden_wedding/interior.jpg" alt="Inside the main marquee" width="273" height="186" /></a><p>Inside the main marquee, a simple treatment</p></div>
<div class="case"><a href="/images/case/garden_wedding/full_scene_big.jpg" class="fancybox" rel="group" title="View of the whole festive garden wedding at night"><img src="/images/case/garden_wedding/full_scene.jpg" alt="View of the whole festive garden wedding at night" width="273" height="186" /></a><p>View of the whole glitzy garden wedding at night</p></div>
<div class="case rightcase"><a href="/images/case/garden_wedding/bar_big.jpg" class="fancybox" rel="group" title="Client's gazebo as a bar"><img src="/images/case/garden_wedding/bar.jpg" alt="Client's gazebo as a bar" width="273" height="186" /></a><p>The client's own gazebo as a pretty bar</p></div>
<div class="case bottomcase"><a href="/images/case/garden_wedding/dance_floor_big.jpg" class="fancybox" rel="group" title="Looking down the main marquee to the busy dance floor"><img src="/images/case/garden_wedding/dance_floor.jpg" alt="Looking down the main marquee to the dance floor" width="273" height="186" /></a><p>Looking down the main marquee to the busy dance floor. The marquee was lit with fairy lights</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/garden_wedding/dancing_big.jpg" class="fancybox" rel="group" title="Tess and Matt enjoying their night"/><img src="/images/case/garden_wedding/dancing.jpg" alt="Tess and Matt enjoying their night" width="273" height="186" /></a><p>Tess and Matt enjoying their night</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a wedding reception near Windsor.</p>
<p class="wide"><strong>The event</strong>: a 12m (40') by 24m (80') traditional marquee with portable toilets, furniture and equipment and the clients' own gazebo in a beautiful garden.</p>
<p class="wide">With such a gorgeous, enclosed space, it came naturally to use open sided tents, pretty lighting and scattered outdoor furniture to create a magical wedding world where guests could wander freely.</p>
<p class="wide">Photos courtesy of <a href="https://richardmurgatroyd.com/" rel="noreferrer noopener" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/case/murgatroyd'])">Richard Murgatroyd</a></p>

<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about wedding marquees.</p></div>
EOQ;
?>