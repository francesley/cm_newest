<?php
$content = <<<EOQ
<p class="wide">When meeting up with friends and family indoors is not allowed, small marquees in the garden can provide a good and pretty alternative. And adaptable frame structures can slot into small and awkward places.</p>
<p class="wide">Marquees are naturally well ventilated. And if a tent can butt up against one or more walls, then the tent can have open sides for excellent air circulation. On cold days, you may definitely prefer a heater. Coats and blankets may also be a good idea.</p>
<p class="wide">Depending on the rules, a small garden marquee can be a good way to enjoy visits from friends and family.</p>
<p class="wide"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about a marquee for safer home meetups.</p>
<h2>Temporary dining marquee</h2>
<div class="case-wrap">
<div class="case"><a href="/images/case/covid_xmas/exterior_big.jpg" title="A small marquee slots into a narrow space in the garden. The end facing the garden has a transparent wall." class="fancybox" rel="group"><img src="/images/case/covid_xmas/exterior.jpg" alt="Outdoor eating area covered by a marquee" width="400" height="300" /></a><p>A small marquee slots into a narrow space in the garden. The end facing the garden has a transparent wall. The roof is also transparent.</p></div>
<div class="case rightcase"><a href="/images/case/covid_xmas/interior_big.jpg" title="Festive and pretty inside the small dining tent." class="fancybox" rel="group"><img src="/images/case/covid_xmas/interior.jpg" alt="Festive and pretty inside the small tent" width="400" height="300" /></a><p>Festive and pretty inside the small tent.</p></div>
<div class="case bottomcase"><a href="/images/case/covid_xmas/closer_big.jpg" title="Looking through a panoramic window into the temporary dining room. The back wall can be left open or pulled shut depending on the temperature." class="fancybox" rel="group"><img src="/images/case/covid_xmas/closer.jpg" alt="Looking through a panoramic window into the temporary dining room tent" width="400" height="300" loading="lazy" /></a><p>Looking through a panoramic window into the temporary dining room. The back wall can be left open of pulled shut depending on the temperature.</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/covid_xmas/day_big.jpg" title="Daytime view. The sides are open against a house wall allowing air to stay fresh." class="fancybox" rel="group"><img src="/images/case/covid_xmas/day.jpg" alt="Daytime view" width="400" height="300" loading="lazy" /></a><p>Daytime view. The sides are open against house walls allowing air to stay fresh.</p></div>
</div>
</div>
EOQ;
?>