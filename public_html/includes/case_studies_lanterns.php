<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/lanterns/paper_lanterns_big.jpg" title="A relaxed traditional marquee decorated with a spray of paper lanterns" class="fancybox" rel="group"><img src="/images/case/lanterns/paper_lanterns.jpg" alt="Marquee decorated with lanterns" width="273" height="186" /></a>
<p>A relaxed traditional marquee decorated with a spray of paper lanterns<br/><a href="/photos/lanterns.htm">View in photo gallery &raquo;</a></p></div>
<div class="case rightcase"><a href="/images/case/lanterns/exterior_big.jpg" title="From the outside, showing the swoops of a traditional style marquee" class="fancybox" rel="group"><img src="/images/case/lanterns/exterior.jpg" alt="Exterior view of a traditional style marquee" width="273" height="186" /></a><p>From the outside, showing the <em>swoops</em> of a traditional style marquee</p></div>
<div class="case"><a href="/images/case/lanterns/tables_big.jpg" title="White table decoration, simple pretty flowers around candlesticks and chiavari chairs for a nice, relaxed feel" class="fancybox" rel="group"><img src="/images/case/lanterns/tables.jpg" alt="Table decoration" width="273" height="186" /></a><p>White table decoration, simple pretty flowers around candlesticks and chiavari chairs for a nice, relaxed feel</p></div>
<div class="case rightcase"><a href="/images/case/lanterns/food_table_big.jpg" title="Pretty flowers and neat layout on a side table" class="fancybox" rel="group"><img src="/images/case/lanterns/food_table.jpg" alt="Side table with flowers" width="273" height="186" /></a><p>Pretty flowers and neat layout on a side table</p></div>

<div class="case"><a href="/images/case/lanterns/entrance_big.jpg" title="Bride and groom make their entrance" class="fancybox" rel="group"><img src="/images/case/lanterns/entrance.jpg" alt="Bride and groom make their entrance" width="273" height="186" loading="lazy" /></a><p>Bride and groom make their entrance</p></div>
<div class="case rightcase"><a href="/images/case/lanterns/cutting_cake_big.jpg" title="Cutting the rustic style wedding cake" class="fancybox" rel="group"><img src="/images/case/lanterns/cutting_cake.jpg" alt="Cutting the wedding cake" width="273" height="186" loading="lazy" /></a><p>Cutting the rustic style wedding cake</p></div>
<div class="case"><a href="/images/case/lanterns/outside_big.jpg" title="Speeches outside the marquee" class="fancybox" rel="group"><img src="/images/case/lanterns/outside.jpg" alt="Speeches outside the marquee" width="273" height="186" loading="lazy" /></a><p>Speeches outside the marquee</p></div>
<div class="case rightcase"><a href="/images/case/lanterns/enjoying_big.jpg" title="Enjoying the party" class="fancybox" rel="group"><img src="/images/case/lanterns/enjoying.jpg" alt="Baby in the marquee" width="273" height="186" loading="lazy" /></a><p>Enjoying the party</p></div>
<div class="case"><a href="/images/case/lanterns/confetti_big.jpg" title="Spectacular confetti on the dancefloor" class="fancybox" rel="group"><img src="/images/case/lanterns/confetti.jpg" alt="Spectacular confetti" width="273" height="186" loading="lazy" /></a><p>Spectacular confetti on the dancefloor</p></div>
<div class="case rightcase"><a href="/images/case/lanterns/dance_big.jpg" title="Fun on the dancefloor" class="fancybox" rel="group"><img src="/images/case/lanterns/dance.jpg" alt="Dancing fun" width="273" height="186" loading="lazy" /></a><p>Fun on the dancefloor</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a summer wedding reception in Faulking, West Sussex.</strong></p>
<p class="wide"><strong>The venue</strong> was a nice flat field, perfect for a erecting a marquee.</p>
<p class="wide"><strong>The marquee:</strong> a 12m (40') x 18m (60') traditional marquee with a chinese hat tent serving as an entrace porch. The remote location meant that luxury loos and a generator were required.</p>
<p class="wide abovecase">All photos courtesy of <a href="http://www.annapumerphotography.com/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/case/anna_pumer'])">Anna Pumer Photography.</a></p>
<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about marquees with lanterns.</p>
</div>
EOQ;
?>