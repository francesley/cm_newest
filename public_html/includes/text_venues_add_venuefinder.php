<?php

$content .= <<<EOQ
		<p>Offer visitors to your own website something else to come back for by displaying a marquee venue finding widget on your website or blog.</p>
		<p>There is no cost. And the method is simple.</p>
		<p>Simply choose a suitable width, click <i>Get the code</i>, then cut and paste the code that appears in the text box onto your web page.</p>
		<p>And there you have it. A no-effort practical attraction for your visitors.</p>
	      <form method="post"  action="/venues/widget.htm" id="widgetForm">
		  <p><strong>Please choose a widget size:</strong></p>
		  <div class="widgetSize"><input name="rbSize" type="radio" value="narrow" class="rb" /> Narrow (300px)</div>
		  <div class="widgetSize"><input name="rbSize" type="radio" value="normal" class="rb" checked="checked" /> Normal (400px)  <em>&#8212; similar to the widget on the right</em></div>
		  <div class="widgetSize"><input name="rbSize" type="radio" value="wide" class="rb" /> Wide (500px)</div>
		  <input name="submitted" type="hidden" value="true"/>
			<input name="widgetSubmit" class="submit" id="widgetSubmit" type="submit" value="Get the code" />
			<label for="code"><textarea rows="12" cols="20" name="code" id="code">$code</textarea></label>
			<div id="unnec"></div>
		  </form>
		  <p class="endCall" style="width:375px">If you would like a marquee venue finder of a custom size or style, please don't hesitate to <a href="/contact_us.htm">contact us</a>.</p>
</div>
EOQ;
?>