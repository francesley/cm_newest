<p>County Marquees has helped countless customers celebrate special occasions of all kinds. Typical party events include: </p> 
<div class="textPhotos">
  <img src="/images/pages/party/glitzy.jpg" alt="Glamorous marquee party" width="273" height="147" />
    <a href="/photos/indian_style.htm"><img src="/images/pages/party/bollywood.jpg" alt="Bollywood themed party" width="273" height="147"  class="second"/></a>
   <img src="/images/pages/party/sugababes.jpg" alt="Sugababes in a marquee" width="273" height="147" class="second"/>
  <p class="view">Sugababes!</p>   
</div>

<ul>
	<li>Birthdays</li>
	<li>Anniversaries</li>
	<li>Christenings</li>
	<li>Children's parties</li>
	<li>Balls</li>
</ul>
<p>Our experienced and helpful staff have deservedly received <a href="../company/testimonials.htm">high praise</a> over the years.</p>
<p>With one of our party marquees, you can be assured of courteous assistance, respect for your venue as well as a stunning backdrop for your party. </p>

<h2>The advantages of a marquee for parties </h2>
<p>As for all sorts of events, a party tent offers the advantage of a space that can be  tailored to the party  you desire.</p>
<div class="textPhotos">
	 <a href="/photos/serene.htm"><img src="/images/pages/party/smart.jpg" alt="Colourful party marquee" width="198" height="150" loading="lazy" /></a>
    <a href="/case_studies/babyshower.htm"><img src="/images/pages/party/ninetieth.jpg" alt="Teatime party tent" width="198" height="150" class="second" loading="lazy" />
   <a href="/case_studies/eighteenth.htm"><img src="/images/pages/evie4.jpg" alt="Eighteenth birthday party marquee" width="198" height="150" class="second" loading="lazy" /><a/>
    <p class="view">Eighteenth birthday marquee<br/><a href="/case_studies/eighteenth.htm" class="inline">View case study</a> | <a href="/photos/eighteenth.htm" class="inline">View in gallery</a></p>
</div>
              <p>Frame marquees are modular, so you can choose a tent that is large, small, L-shaped, square or rectangular. Doors and windows can be positioned anywhere.</p>
			  <p>Frame marquees can also squeeze into awkward gardens and allow friends and family to be  welcomed into your home.</p>
              <p>Traditional style tents are more &ndash; well &ndash; traditional and work well for festival style parties or tea time events. Stretch tents are another option and can add extra wow factor.</p>
              <ul>
                <li>See <a href="/equipment/marquees.htm">marquees</a> for information about the different types of tents we offer </li>
                <li>Or check out <a href="/pricing/marquees.htm">marquee prices and sizes</a> to get an idea of the cost of pary marquee hire.</li>
              </ul>
 
<h2>Party styles</h2>
<p>What sort of party do you want?</p>
<div class="textPhotos">
	<img src="/images/pages/party/funky_party.jpg" alt="Funky party marquee" width="198" height="150" />
	<p class="view">Hippy style party</p>
	 <a href="/photos/party_tent.htm"><img src="/images/pages/party.jpg" alt="Blacked out party marquee" width="198" height="150" class="second" loading="lazy" /></a>
	 <p class="view"><a href="/photos/party_tent.htm" class="inline">View in gallery</a> | <a href="/case_studies/fiftieth_party.htm" class="inline">View case study</a></p>
	 <a href="/case_studies/fiftieth.htm"><img src="/images/pages/party/night_sky.jpg" alt="Blacked out party marquee with starcloth lining" width="198" height="150" class="second" loading="lazy" /></a>
   <p class="view"><a href="/case_studies/fiftieth.htm" class="inline">View case study</a> | <a href="/photos/birthday.htm" class="inline">View in gallery</a></p>
</div>
<p>A marquee is often compared to a blank canvas to paint on. The comparison is accurate.</p>	
	<p >Would you like a classic look for your party, perhaps with a vintage theme? Or funky and colourful? Maybe white and airy? All styles can be easily implemented in a party tent - though some more elaborate looks may hurt your pocket!</p>
  <p>Our marquees are white and we provide ivory and black-out linings. Their aluminium frames make it easy to hang items from the walls or roof &#8212; for example lanterns, draped fabric and flower baskets.</p>
   <p>Combined with lighting effects, almost any look is possible. We are happy to help put into practice your chosen style, or to suggest ideas.</p>
	<ul>
		<li>Take a look at <a href="/photos.htm?category=party">party marquee photos</a> or <a href="/photos.htm?category=decoration">marquee decoration photos</a> for more ideas</li>
		<li>Or have a look at some <a href="/real.htm">real party marquees</a></li>
		<li>Or see <a href="/equipment/decoration.htm">marquee decoration</a></li>
		<li>If you would like help, please <a href="../contact_us.htm">ask us</a>  </li>
  </ul>
 
  <h2>Lighting effects</h2>
<p>Lighting accounts for some of the most exciting and creative marquee effects: popular night skies, large scale or subtle colour washes,  pretty hanging lanterns... Imaginative lighting scehemes create a fantastic atmosphere for a party. </p>
<div class="textPhotos">
	<img src="/images/pages/party/lighting.jpg" alt="Marquee party lighting" width="198" height="150" loading="lazy" />
</div>
              <ul >
                <li>To see some of our wide range of lighting options, view <a href="/equipment/lighting.htm"> marquee lighting</a>. </li>
                <li>Or please look at our <a href="/photos.htm?category=lighting">examples of lighting effects in our photo gallery</a>. </li>
              </ul>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out more about hiring party marquees.</p>
</div>
