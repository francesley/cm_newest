<p class="wide">County Marquees supplies several types of marquee, each suited to different occasions. Our frame marquees can also serve as extensions to a house, catering tents and walkways.</p>
<h2>Choosing the right marquee</h2>
<div  class="marquee" id="leftmarquee">
<h3>Frame marquees</h3>
<a href="#frame"><img src="/images/pages/marquees/smtop_frame2.jpg" alt="Frame marquee" width="75" height="75" loading="lazy" /></a>
<p>Good use of space, no inner poles, flexible.</p>
<p>Because frame marquees need no extra space around them, they work well in awkward sites. They can also serve as catering tents and walkways.</p>
<p>Our frame marquees range in width from 4 metres (14 foot) to 15 metres (50 foot) and can be pretty much as long as you like.</p>
<p><a href="#frame" class="bottom">More about frame marquees</a></p>
<p id="see_uses">See some of the uses of frame marquees:</p>
<ul class="long">
<li><a href="/equipment/frame.htm#small">Small spaces</a></li>
<li><a href="/equipment/frame.htm#walkway">House extensions</a></li>
<li><a href="/equipment/frame.htm#catering">Catering tents and walkways</a></li>
</ul>
</div>
<div class="marquee rightmarquee">
<h3>Traditional marquees</h3>
<a href="#traditional"><img src="/images/pages/marquees/smtop_traditional.jpg" alt="Traditional marquee"  align="left" loading="lazy"  /></a>
<p>Romantic looking with inner poles. Need clear space around them.</p>
<p><a href="#traditional" class="bottom">More about traditional tents</a></p>
</div>
<div class="marquee rightmarquee chinesemarquee">
<h3>Chinese hat marquees</h3>
<a href="#chinese" class="img"><img src="/images/pages/marquees/smtop_chinese2.jpg" alt="Chinese hat marquees" align="left" loading="lazy" /></a>
<p style="margin-bottom:14px">Pointy topped for chill outs, porches and making a splash.</p>
<p><a href="#chinese" class="bottom">More about chinese hats</a></p>
</div>
<div  class="marquee" id="leftmarquee">
<h3>Stretch tents</h3>
<a href="#stretch"><img src="/images/pages/marquees/smtop_stretch_tent.jpg" alt="Stretch tents" width="75" height="75" loading="lazy" /></a>
<p>Contemporary and eyecatching. Can be rigged in different shapes to suit your site / event</p>
<p><a href="#stretch" class="bottom">More about stretch tents</a></p>
</div>
<h2 id="frame" >Frame marquees</h2>
<p class="wide">A modern style of marquee. Instead of centre poles and ropes, these tents are supported by an aluminum skeleton around the edge.</p>
<h3>Features of frame marquees:</h3>
<div class="textPhotos">
<img src="/images/pages/marquees/busbridge.jpg" alt="Frame marquee in very pretty setting" width="198" height="150" loading="lazy" />
<img src="/images/pages/marquees/frame_dusk.jpg" alt="Frame marquee lit up at dusk" width="198" height="150" loading="lazy" />
<img src="/images/pages/marquees/clear_marquee.jpg" alt="Completely transparent marqueee" width="198" height="150" loading="lazy" />
<a href="/photos/reception.htm"><img src="/images/pages/marquees/frame_interior.jpg" alt="Interior of frame tent" width="198" height="150" class="second" loading="lazy" /></a>
<p>Clearspan interior <br/><a href="/photos/reception.htm">View larger &raquo;</a></p>
</div>
<ul >
<li><strong>Clear span</strong>: no central pole, which means clear space inside the marquee and an unobstructed view for speeches.</li>
<li><strong>Installation on any surface:</strong> including hard surfaces like sand, gravel, and concrete. This is because frame marquees can be secured to the ground with weights instead of pegs  (though pegs are used where possible and  are advisable in severe winds).</li>
<li><strong>Installation anywhere:</strong> well... almost anywhere. No guy ropes  means frame marquees can fit in small town gardens, on roof terraces, in courtyards etc as they can make the maximum use of space. See <a href="/photos.htm?category=awkward">photos of marquees in awkward spaces</a> for examples.</li>
<li> <strong>Winter stability:</strong> framed marquees  withstand higher 
wind speeds and snow loadings so they are a practical option in  winter.</li>
<li><strong>Safety:</strong> made from flame-retardant PVC manufactured to the latest BSI standards.</li>
<li><strong>Flexible use of space:</strong> can be joined together to cover  large areas or fit L-shaped gardens</li>
<li><strong>Easily divided up:</strong> see this <a href="/case_studies/reveals.htm">case study</a> or <a href="/case_studies/relaxed.htm">this one</a> for examples of marquees divided into sections for differents parts of an event.</li>
<li> See <a href="/equipment/frame.htm">uses of frame marquees &raquo;</a><br/>Small marquees, catering tents, house extensions..</li>
<li>See <a href="/pricing/marquees.htm">prices and sizes &raquo;</a></li>
</ul>

<h2 id="traditional" >Traditional marquees</h2>
<p class="wide">Combining the best of old and new, our traditonal-style marquees have all the classic elegance of old fashioned pole marquees along with the benefits of modern technology.</p>
<h3>Features of traditional marquees:</h3>
<div class="textPhotos">
<img src="/images/pages/marquees/traditional.jpg" alt="Traditional marquee" width="198" height="150" loading="lazy" />
<a href="/photos/blue_marquee.htm"><img src="/images/pages/marquees/traditional_night.jpg" alt="Traditional marquee lit up at night" width="198" height="150" loading="lazy" /></a>
<a href="/photos/classy.htm"><img src="/images/pages/marquees/wood_poles.jpg" alt="Traditional marquee with wooden poles" width="198" height="150"  loading="lazy" /></a></div>
<ul>
<li><strong>Romantic</strong>: these have the beautiful  'swooping' lines of the classic English garden party marquee. Ideal for romantic traditional weddings, formal garden parties or any event where  period style or a little extra elegance is called for. </li>
<li><strong>Poles</strong>:  supporting poles inside the marquee which can be decorated with ribbons, flowers etc. The poles are illustrated in the lower photo alongside. </li>
<li><strong>A little extra space:</strong> erected with guy ropes and pegs. But unlike older marquees, they do not need huge amounts of space. Thanks to modern technology, only 5' extra space is required  around the marquees.</li>
<li><strong>Safety:</strong> made from flame-retardant PVC manufactured to the latest BSI standards.</li>
<li>See <a href="/pricing/traditional_marquees.htm">prices and sizes &raquo;</a></li>
</ul>

<h2 id="chinese">Chinese hat marquees</h2>
<p class="byphoto"><strong>Oriental-style pointed high roof</strong>: chinese hat &ndash; or pagoda &ndash; tents make a fun centrepiece for a small to medium-sized party.</p>
<div class="textPhotos byP"><img src="/images/pages/marquees/porch.jpg" alt="Porch marquee" width="198" height="150" loading="lazy" />
<img src="/images/pages/marquees/chinese_hat.jpg" alt="Row of chinese hat tents" width="198" height="150" loading="lazy" />
<a href="/photos/pagoda.htm"><img src="/images/pages/marquees/chinese_hat_bar.jpg" alt="Chinese hat marquee" width="198" height="150" loading="lazy" /></a>
<p>Chinese hat as outdoor bar<a href="/photos/pagoda.htm" class="view">View larger &raquo;</a></p></div>

<ul>
<li><strong>Entrance:</strong> Chinese hat tents often serve as an attractive entrance porch to a larger marquee, as in the photo of a prettily lit Chinese hat tent with clear walls.</li>
<li><strong>Buffet or bar...</strong> or as a <em>chill out</em> area away from the main marquee of an event &ndash; a quiet rest space with sofas and comfy chairs, for example. A row of chinese hat tents is a particularily impressive sight.</li>
<li><strong>Details:</strong> Chinese hat marquees can have walls, or transparent walls to allow a view to be enjoyed, or in warm weather have no walls at all. They cannot be lined, but they can be lit for special effects.</li></ul>
<p class="byphoto">See <a href="/pricing/chinese.htm">prices &raquo;</a></p>

<h2 id="stretch">Stretch tents</h2>
<p class="wide">Made from a strong and flexible waterproof fabric, stretch tents are a modern and eye-catching alternative to a traditional marquee.</p>
<div class="textPhotos byP"><a href="/equipment/stretch_tents.htm"><img src="/images/pages/marquees/stretch_tent_white.jpg" alt="Stretch tent evening cocktail party" width="198" height="150" loading="lazy" ></a>
<p>Evening cocktail party</p></div>
<ul>
<li><strong>Adaptable:</strong> stretch tents can be installed on any surface including sand, gravel and concrete. They can be secured by a number of different methods and are well suited for awkward sites</li>
<li><strong>Suitable for all weather:</strong> in warm weather, stretch tents can be assembled as an open sided canopy. In poor weather, all four sides of the tent come down to create an intimate and protected space.</li></ul> 
<p class="wide">See our <a href="/equipment/stretch_tents.htm">stretch tents page</a> for more information.</p>


<p class="endCall">Or <a href="/contact_us.htm">contact us</a> for more information on all tent types.</p>
</div>