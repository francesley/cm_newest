<?php

$htmlmail = <<<EOQ

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; iso-8859-1">
<meta name="viewport" content="width=device-width,initial-scale=1" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	font-family: georgia, times, serif;
}
.addresstext {
	font-family: georgia, times, serif;
	font-size: 9pt;
	line-height: 13pt;
	color: #000000;
	text-decoration: none;
}
a:link {
	color:#175179;
	text-decoration: none;
	font-weight:bold;
	font-family: georgia, times, serif;
}
a:visited {
	color:#175179;
	text-decoration: none;
	font-weight:bold;
	font-family: georgia, times, serif;
}
.style3 {
	color:#175179;
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
.bodytext {
	font-family: georgia, times, serif;
	font-size: 10pt;
	line-height: 14pt;
	color: #000;
	text-decoration: none;
	font-weight:normal;
}
.whitebodytext {
	font-family: georgia, times, serif;
	font-size: 9pt;
	mso-line-height-rule:exactly;
	line-height: 14pt;
	color: #000;
	text-decoration: none;
	font-weight:normal;
	background-color:#fff;
	padding:40px;
}
.biggertext {
	font-family: georgia, times, serif;
	font-size: 9pt;
	color: #000;
	text-decoration: none;
}
p {
margin-bottom:5px;
font-family: georgia, times, serif;
	font-size: 9pt;
}
p.final {
border-top:1px dotted #000000;
font-weight:bold;
margin-bottom:1.5em;
padding-top:5px;
width:170px;
font-family: georgia, times, serif;
font-size: 9pt;
margin-top:5px;
margin-right:350px;
}
@media only screen and (max-device-width: 480px) {
p.final {
margin-right:0;
width:auto;
}
.whitebodytext {
font-size:8pt !important;
padding:20px !important;
}
table{
	width:300px !important;
}
#topimg {
	width:300px !important;
	height:121px !important;
}
.biggertext, .biggertext a, .biggertext a:visited {
font-size:8pt !important;
line-height:12pt !important;
}
.bodytext, .bodytext span, .bodytext a, .bodytext a:visited {
font-size:8pt !important;
}
}

-->
</style>
</head>

<body style="background-color:#E2E3DD">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="#E2E3DD" >
  <tr>
    <td align="center" valign="top" bgcolor="#E2E3DD" style="padding-top:10px;"><table width="650" style="border-width:1px;border-color:#C4C3BF;border-style:solid;" cellpadding="0" cellspacing="0" bgcolor="#fff">
      <tr>
        <td align="center" valign="top" bgcolor=white ><table width="650" border="0" cellspacing="0" cellpadding="0" style="border-width:10px;border-color:#fff;border-style:solid;">
          <tr>
            <td valign="top" bgcolor="#F2F1ED"><table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="94" align="left" bgcolor=white cellspacing="0" cellpadding="0"><a href="https://www.countymarquees.com?utm_source=logo&amp;utm_medium=email&amp;utm_campaign=$campaign" target="_blank"><img src="#img#" width="640" border="0" alt="Beautiful marquees" id="topimg"></a></td>
                </tr>
              <tr>
                <td bgcolor="#FFFFFF" class="whitebodytext" style="padding:30px" align="left">              
#mail_content#

</td>
                </tr>
              <tr>
                 <td align="center" style="background-color:#F2F1ED;padding-top:10px;">
                  <p class="bodytext"> <font face=georgia, times, serif><span style="font-size: 9pt">T: 020 7267 4271<br>
                   E:</span></font>  <a href="mailto:enquiries@countymarquees.com" class="addresstext"><font face=georgia, times, serif><span style="font-size:9pt;text-decoration:none;color:#175179"><strong>enquiries@countymarquees.com</strong></span></font></a><br>
                   W: <a href="https://www.countymarquees.com?utm_source=bottom&amp;utm_medium=email&amp;utm_campaign=$campaign" target="_blank" ><font face=georgia, times, serif><span style="font-size: 9pt;color:#175179;text-decoration:none;"><strong>www.countymarquees.com</strong></span></span></font></a></p>
                  
                  <p class="bodytext" style="font-size: 9.0pt;line-height:24pt;margin-top:0;">Follow us on <a href="https://www.facebook.com/countymarquees"><font face=georgia, times, serif><span style="font-size:9pt;text-decoration:none;color:#175179">Facebook</span></font></a> or <a href="https://twitter.com/county_marquees"><font face=georgia, times, serif><span style="font-size:9pt;text-decoration:none;color:#175179">Twitter</span></font></a>: </p>
				
                  </td>
                </tr>
              <tr>
                <td align="center" style="background-color:#F2F1ED;padding-top:4px;padding-bottom:10px;"> <a href="https://www.facebook.com/countymarquees"><img border="0" width="32" src="https://www.countymarquees.com/images/email/FaceBook_32x32.gif" alt="Facebook" ></a>&nbsp;
				  <a href="http://twitter.com/county_marquees" ><img border="0" width="32" src="https://www.countymarquees.com/images/email/Twitter_32x32.gif" alt="Twitter" ></a></td>
              </tr>
            </table></td>
            </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
EOQ;
?>