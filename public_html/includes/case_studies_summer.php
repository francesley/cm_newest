<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>

<div class="case"><img src="/images/case/summer_first.jpg" alt="Summer marquee" width="273" height="186" /><p>A light summer canopy provided the ideal setting for a lunch time party.</p></div>
<div class="case rightcase"><img src="/images/case/summer_intohouse.jpg" alt="Looking into the house" width="273" height="186" /><p>Looking into the house. Guests could move inside and outside with ease.</p></div>
<div class="case"><img src="/images/case/summer_kitchen.jpg" alt="Toasting" width="273" height="186" /><p>Food could be served straight from the kitchen into the open-sided marquee.</p></div>
<div class="case rightcase"><img src="/images/case/summer_overview.jpg" alt="The whole marquee" width="273" height="186"/><p>The whole marquee from the end of the garden.</p></div>
<div class="case bottomcase"><img src="/images/case/summer_steps.jpg" alt="The marquee spanned wide steps"  width="273" height="186" /><p>The split level marquee spanned the two garden levels and wide steps.</p></div>
<div class="case rightcase bottomcase"><img src="/images/case/summer_poles.jpg" alt="Long and short poles" width="273" height="186" /><p>Longer legs at the back for the lower level, and shorter legs into the flower beds for the upper level, .</p></div>
<hr>
<p class="wide"><strong>The occasion:</strong> a summer party in the garden.</p>
<p class="wide"><strong>The venue:</strong> a medium sized town garden on two levels wide steps edged by flower beds in between the levels.</p>
<p class="wide"><strong>The challenge:</strong> to provide summer-style marquee cover that spanned the two garden levels and the steps and enhanced the contemporary feel of the garden with its fab modern white paving.</p>
<p class="wide abovecase"><strong>County Marquees' solution:</strong> a simple split level marquee without walls that covered the different levels by different lengths of supporting outer poles. The poles were decorated with swathed cloth.</p>

<p class="endCall"><a href="../contact_us.htm">Contact us</a> to find out about summer marquees in the garden</p>
</div>
EOQ;
?>