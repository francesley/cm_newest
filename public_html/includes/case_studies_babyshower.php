<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/baby_shower/main_big.jpg" title="Pretty baby shower marquee" class="fancybox" rel="group" alt="Pond in marquee"><img src="/images/case/baby_shower/main.jpg" alt="Pretty baby shower marquee" width="273" height="186" /></a><p>Pretty baby shower marquee</p></div>
<div class="case rightcase"><a href="/images/case/baby_shower/table_big.jpg" title="All laid up for tea time" class="fancybox" rel="group"><img src="/images/case/baby_shower/table.jpg" alt="Tables laid up for tea time" width="273" height="186" /></a><p>All laid up for tea time</p></div>
<div class="case"><a href="/images/case/baby_shower/tea_big.jpg" title="Gorgeous tea time mismatched crockery" class="fancybox" rel="group"><img src="/images/case/baby_shower/tea.jpg" alt="Gorgeous tea time mismatched crockery" width="273" height="186" /></a><p>Gorgeous tea time mismatched crockery</p>
</div>
<div class="case rightcase"><a href="/images/case/baby_shower/cakes_big.jpg" title="Yummy cakes and biscuits with cute baby themed details" class="fancybox" rel="group"><img src="/images/case/baby_shower/cakes.jpg" alt="Yummy cakes and biscuits with cute themed baby details" width="273" height="186"/></a><p>Yummy cakes and biscuits with cute baby themed details</p></div>
<div class="case"><a href="/images/case/baby_shower/entrance_big.jpg" title="More pretty baby details at the marquee entrance" class="fancybox" rel="group"><img src="/images/case/baby_shower/entrance.jpg" alt="Baby details at the marquee entrance" width="273" height="186" loading="lazy" /></a><p>More pretty baby details at the marquee entrance</p></div>
<div class="case rightcase"><a href="/images/case/baby_shower/girl_or_boy_big.jpg" title="Girl or boy?" class="fancybox" rel="group"><img src="/images/case/baby_shower/girl_or_boy.jpg" alt="Girl or boy sign" width="273" height="186" loading="lazy" /></a><p>Girl or boy? | <a href="/photos/amphitheatre.htm" class="view">View in gallery</a></p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a Hindu <em>Godh bharna</em> ceremony in Kenton, North London at tea time. This is an ancient Hindu ceremony &ndash; a bit like a baby shower &ndash; celebrated in the seventh month of pregnancy to welcome the unborn baby and bless the mother-to-be.</p>
<p class="wide"><strong>The venue</strong> was a typical long narrow London garden</p>
<p class="wide"><strong>The marquee</strong> was a narrow 4m x 15m frame marquee that could squeeze into the long thin garden</p>
<p class="wide abovecase">See a very different <a href="/case_studies/small.htm">real marquee</a> in a small, thin garden</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a baby shower marquee</p>
</div>
EOQ;
?>