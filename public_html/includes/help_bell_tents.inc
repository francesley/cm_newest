<p class="wide"> Supported by a centre pole, bell tents have beautiful <em>swooping</em> lines and an elegant silhouette. Round and roomy, easy to transport and assemble, bell tents lend themselves perfectly to luxury camping.</p>

<h2>How does it work?</h2>
<div class="textPhotos">
	<img src="/images/pages/marquees/bell_tents/bell_tent_basic.jpg"  alt="An unfurnished bell tent" width="198" height="150"/>

	</div>
<p>The aim of our service is to provide a bespoke pop-up bell tent / glamping experience. You choose the location and the accessories you would like included. We pitch the bell tent(s), style it to your specification and once you are done, we pack it all away. </p>

<h2>Features and dimensions</h2>
	<div class="textPhotos">
	<img src="/images/pages/marquees/bell_tents/in_field.jpg"  alt="Bell tent in a field" width="198" height="150"/>
	<img src="/images/pages/marquees/bell_tents/family_tent.jpg"  alt="Room for the family" width="198" height="150"/>
	<p>Up to seven single beds can fit in a bell tent</p>
	<img src="/images/pages/marquees/bell_tents/bell_tent_rugs.jpg"  alt="Bell tent decorated with rugs" width="198" height="150"/>
	</div>
	
	<ul>
	<li><strong>Size:</strong> 5 metres. The height from the floor to the top of the centre pole is 2.5 metres.</li>
	<li><strong>Capacity:</strong> we favour comfort over capacity. Our 5 metre bell tent is great for a family or group of friends and comfortably sleeps four. This ensures that there is plenty of space for personal belongings and furnishings. It sleeps a maximum of six.</li>
	<li><strong>Suitable for all weather:</strong> all of our tents are water resistant and come with fully zipped in ground sheets.
	<br><br>The walls have a 20cm rubber flap that tucks under the groundsheet to create a wind and waterproof seal. These can be unattached to allow the sides of the bell tent to be rolled up to create a refreshing breeze in the warm summer months. </li>
	<li><strong>Personalisation:</strong> personalise your bell tent with furnishings and accessories such as LED fairy lights, a tea light chandelier, luxury single and double airbeds, thick duvets, blankets, rugs etc.  <!--For more information please see our <a href="/pricing/bell_tents.htm">accessories and pricing</a> page.--></li>
	</ul>
	<h2>Hiring a bell tent</h2>
	
	<div class="textPhotos">
	<img src="/images/pages/marquees/bell_tents/four_tents.jpg"  alt="Glamping - four bell tents" width="198" height="150"/>
	<p>Glamping with four tents</p>
	<img src="/images/pages/marquees/bell_tents/bell_tent_field.jpg"  alt="Bell tent in a field setting" width="198" height="150"/>

	</div>
	
	<ul>
	<li><strong>Delivery costs:</strong> delivery is included in the price within Surrey. In the rest of the home counties, a delivery charge will be added. Please <a href="/contact_us.htm">contact us</a> for more information.</li>
<li><strong>Booking a venue / campsite:</strong> if you need to rent a venue, you book your own but we can recommend. Our <a href="/venues.htm">venue map</a> has some suitable sites.</li>
<li><strong>No minimum rental:</strong> a long weekend or a single night, we can help.
<li><strong>Safety:</strong> our tents are European tested to BS5852 standards. This is the <em>cigarette test</em> which shows that the tent will not ignite when burnt by a cigarette.</li></ul>


 
<h2 id="small">A bell tent for every occasion</h2>
<div class="textPhotos">
	<img src="/images/pages/marquees/bell_tents/bell_tent_bunting.jpg"  alt="Bell tent decorated with bunting" width="198" height="150"/>
	<img src="/images/pages/marquees/bell_tents/bell_tent_dog.jpg"  alt="Bell tent with dog" width="198" height="150"/>
	<img src="/images/pages/marquees/bell_tents/yoga_garden.jpg"  alt="Bell tents for yoga at a festival" width="198" height="150"/>
	<p>Bell tents for a yoga garden</p>
	</div>
	<ul>
	<li><strong>Weddings:</strong> provide accommodation to your wedding guests or enjoy an outdoor honeymoon. We offer an external booking service: you  reserve a number of tents with a deposit  and then your guests book and pay the remainder directly with us. Another option is to add us to your wedding invitations as an accommodation option. </li>
	<li><strong>Festivals and events:</strong> bell tents are well suited for large groups. Decorated with beanbags and cushions, they can provide a relaxing chill out area at a festival or a fun addition to a garden party. 5 metre bell tents can hold between 10 to 14 people seated.</li>
	<li><strong>Parties:</strong> add some adventure to a children's birthday party or round up the girls for a glamping hen do.</li>  
	<li><strong>Glamping:</strong> bell tents are ideal for families and friends to enjoy the comforts of glamping together.</li>
	<li><strong>Village:</strong> build a bell tent village</li>
  </ul>

<h2>Contact us</h2>
<p class="wide">Please <a href="/contact_us.htm">contact us</a> for more information, call <a href="tel:+447791 221665">07791 221665</a> or email bohemianbelltents@gmail.com.</p>
</div>

