<div id='text'>
<h1>Marquee planner</h1>
<div id="plannerdiv"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="958" height="590">
<param name="movie" value="/movies/planner2marqueesNewer.swf" />
<param name="quality" value="high" />
<?=$paramExtra?>
<embed src="/movies/planner2marqueesNewer.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="958" height="590" <?=$embedExtra?>></embed>
</object></div>
<h2>Party marquee and wedding tent planning made easy</h2>
<p>Take the pain out of planning how to lay out your wedding tent or party marquee with our free and easy wedding planning tool. Choose a marquee size; then drag and drop tables, dance floors, doors, catering tents into position to your hearts content; then  print and/or save your professional-standard marquee layout. Requires <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&;P5_Language=English" target="_blank">Macromedia Flash 6</a>.</p>
<h2>Link to the marquee planner</h2>
<p>Cut and paste the code below onto your webpage if you would like to link to the marquee planner:</p>
<textarea rows="1"  class="lbExclude"><a href='https://www.countymarquees.com/movies/planner.html'>Marquee planner</a></textarea>

<p id="bookmarks" >
<fb:like layout="standard" show_faces="false" width="450" action="like" colorscheme="light" href="https://www.countymarquees.com/movies/planner.html"></fb:like>          
</p></div>