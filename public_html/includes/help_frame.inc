<p class="wide">Frame marquees - also known as clear span marquees - are very flexible. On this page, we illustrate some of the different ways they can be used.</p>

<h2 id="small">Small marquees</h2>
<div class="textPhotos"><a href="/photos/vintage.htm"><img src="/images/pages/marquees/small_vintage_marquee.jpg" alt="Decorated small marquee" width="198" height="150"/></a>
<p class="view">Small can be stylish<br/><a href="/case_studies/small.htm" class="inline">View case study</a></p>
<a href="/photos/small.htm"><img src="/images/pages/marquees/marquee_semiP.jpg" alt="Small marquee" width="198" height="150"/></a>
<p>Tucked neatly into a small urban garden</p></div>
<p>Small frame structures or clear span marquees with no centre poles or ropes.</p>
<h3 style="clear:left">Features of small marquees:</h3> 
<ul>
<li><strong>Good for small gardens:</strong> narrower than our standard marquees at only 4m (14') wide, our small marquees are also taller creating a spacious feel for a narrower venue.</li>
<li><strong>Safety:</strong> made from flame-retardant PVC manufactured to the latest BSI standards.</li>
<li>See <a href="/pricing/smaller_marquees.htm">prices and sizes &raquo;</a></li>
</ul>
<h2 id="walkway">House extensions, walkways and porches</h2>
<p>Because they need no pegs or guy ropes, framed structures can easily butt up against the side of a building to serve as an extension to a house.</p>
<div class="textPhotos byP"><a href="/photos/extension.htm"><img src="/images/pages/marquees/house_extension.jpg" alt="Marquee house extension" width="198" height="150"/></a>

<a href="/photos/walkway.htm"><img src="/images/pages/marquees/walkway.jpg" alt="Marquee walkway over different levels" width="198" height="150"/></a></p></div>
<p>Similarly, they can serve as an entrance porch to a marquee; or as a walkway between marquees, or between marquees and a building.</p>
<p>The lower photo alongside shows a walkway connecting two large marquees across different levels of a garden.</p>
<h2 id="catering">Catering tents</h2>
<div class="textPhotos"><img src="/images/pages/marquees/catering_tent.jpg" alt="Catering tent" width="198" height="150"/></div>
<p>Various sizes of catering tents can be easily set up next to a main marquee.</p>
<p>In the photo alongside, the tent on the left is a catering tent, adjoining the large main marquee on the right.</p>
<h2 id="large">Large marquees</h2>
<p>Large frame structures with no centre poles or ropes.</p>
<h3>Features of event marquees:</h3>
<div class="textPhotos">
<img src="/images/pages/marquees/event_marquee2.jpg"  alt="Large clear span marquee" width="198" height="150"/>
<img src="/images/pages/marquees/large_marquee.jpg"  alt="Large frame marquee" width="198" height="150"/>
</div>
<ul>
<li><strong>Wide</strong>: wider than our standard frame structures, event marquees accomodate large numbers  in a single clear uninterrupted span.</li>
<li><strong>More than 800 people:</strong> seat more than 600 people or  800+ standing so are  ideal for conferences, presentations, prize givings and the like.</li>
<li><strong>Flexible:</strong> combine these larger marquees with other sizes of marquee for flexible space planning e.g. separate areas for exhibitions, displays and presentations.</li>
<li><strong>Large parties:</strong> event marquees  are also suitable for larger weddings and parties.</li>
<li><strong>Safety:</strong> made from flame-retardant PVC manufactured to the latest BSI standards.</li>
<li>See <a href="/pricing/event_marquees.htm">prices and sizes &raquo;</a></li>
</ul>

</div>