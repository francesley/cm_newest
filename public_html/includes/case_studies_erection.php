<?php

$content = <<<EOQ
<p class="wide"><strong>The occasion:</strong> was  an annual corporate event in Thame, Oxfordshire, soon after Christmas. </p>
<p class="wide"><strong>The challenge:</strong> was to accommodate wining, dining and some serious speechifying with a loud dancing area and large-scale catering requirements. And to withstand the winter weather.</p>
<p class="wide abovecase"><strong>County Marquees' solution:</strong> three large marquees: one for catering, one for dining, and one for serving food and later on, for dancing, each one firmly anchored against strong winter winds. The floor of each event marquee was protected from possible waterlogging with hard floors underneath carpet.</p>
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>

<h2 style="margin-top:0">Before:</h2>
<div class="case"><img src="/images/case/erection_lorriesArrive.jpg" alt="Lorries arrive with equipment" width="273" height="186" /><p>It all began early one bleak February morning  on a  deserted rugby pitch with the rumble of big engines coming closer...</p></div>
<div class="case rightcase"><img src="/images/case/erection_work_beginsSM.jpg" alt="Poles are unloaded from the lorry" width="273" height="186" /><p>Work begins, as marquee parts are unloaded off the lorry.</p></div>
<div class="case bottomcase"><img src="/images/case/erection_laidOut2SM.jpg" alt="Alluminium poles and struts laid out" width="273" height="186" /><p>The aluminum poles and struts are laid out in position on the pitch.</p> </div>
<div class="case rightcase bottomcase"><img src="/images/case/erection_hardfloor.jpg" alt="Laying out hard floor" width="273" height="186" /><p>Laying out hard floor inside one of the three marquees.</p> </div>
	
<h2>After:</h2> 
<div class="case"><img src="/images/case/erection_finished1Sm.jpg" alt="External view of finished marquee" width="273" height="186" /><p>External view of the three finished marquees. One is a catering tent, one for eating, and the third for serving food and dancing.</p></div>
<div class="case rightcase"><img src="/images/case/erection_danceempty.jpg" alt="Inside the dancing marquee" width="273" height="186" /><p>Inside  the dancing marquee, with a night sky and multi-coloured swag. The swag and carpet colours matched the company brand.</p></div>
<div class="case bottomcase"><img src="/images/case/erection_diningfinished.jpg" alt="Inside a dining marquee" width="273" height="186" /><p>Inside the dining marquee, with tables and chairs for 300. Multi-coloured swag matches corporate branding.</p></div>
<div class="case bottomcase rightcase"><img src="/images/case/erection_hogroast.jpg" alt="Serving hog roast" width="273" height="186" /><p>Serving a hog roast in the dancing marquee early on with a <em>night sky</em> in the background.<br/><a href="/photos/winter-marquee.htm">View larger</a></p></div>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about event marquees.</p>
</div>
EOQ;
?>