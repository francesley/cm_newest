<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/wedding_details/better_together_big.jpg" title="Better together" class="fancybox" rel="group" alt="Pond in marquee"><img src="/images/case/wedding_details/better_together.jpg" alt="Better together labels on the back of bride and grooms chairs" width="273" height="186" /></a><p>Chair labels affirm the bride and groom's confidence in their vows</p></div>
<div class="case rightcase"><a href="/images/case/wedding_details/cupcakes_big.jpg" title="Cupcakes and love message" class="fancybox" rel="group"><img src="/images/case/wedding_details/cupcakes.jpg" alt="Cupcakes and love message" width="273" height="186" /></a><p>A message by a display of cupcakes sets the tone</p></div>
<div class="case"><a href="/images/case/wedding_details/interior_big.jpg" title="Classic white wedding marquee" class="fancybox" rel="group"><img src="/images/case/wedding_details/interior.jpg" alt="Classic white wedding marquee" width="273" height="186" /></a><p>Classic white wedding marquee interior</p>
</div>
<div class="case rightcase"><a href="/images/case/wedding_details/marquee_big.jpg" title="Frame marquee" class="fancybox" rel="group"><img src="/images/case/wedding_details/marquee.jpg" alt="Frame marquee" width="273" height="186" /></a><p>Simple frame marquee in beautiful surroundings</p></div>
<div class="case bottomcase"><a href="/images/case/wedding_details/ladder_big.jpg" title="Ladder of flowers and found objects" class="fancybox" rel="group"><img src="/images/case/wedding_details/ladder.jpg" alt="Ladder of flowers and found objects" width="273" height="186" loading="lazy" /></a><p>Ladder crowded with found objects and white flowers looks pretty and interesting</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/wedding_details/adventure_big.jpg" title="The adventure begins" class="fancybox" rel="group"><img src="/images/case/wedding_details/adventure.jpg" alt="The adventure begins" width="273" height="186" loading="lazy" /></a><p>Text on a mirror reflects beautifully and captures the spirit of the wedding</p></div>
<hr/>
<p class="wide"><strong>The occasion</strong> was a classic white themed wedding in a simple frame marquee.</p>
<p class="wide"><strong>Photos</strong> courtesy of <a href="http://hermionemccosh.com/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/case/mccosh'])" rel="noopener noreferrer">Hermione Mccosh</a>.</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a wedding marquee</p>
</div>
EOQ;
?>