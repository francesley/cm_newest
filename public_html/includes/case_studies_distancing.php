<?php
$content = <<<EOQ
<p class="wide"><strong>See <a href="/case_studies/covid_wedding.htm">Covid weddings</a> for the current rules on marquee use for events of all kinds.</strong></p>
<p class="wide">Temporary marquee structures are the ideal solution for creating extra space to help meet Covid-19 safety requirements. With some open sides, they can also be a pleasanter space to enjoy time safely outside &ndash; even, with a heater, in the winter.</p>
<p class="wide">Marquees are extremely adaptable, have good ventilation and can be divided up to suit your needs</p>
<p class="wide">Follow the links below to see examples of temporary structures County Marquees has installed to provide extra space or accomodate Covid-19 in schools, universities, restaurants and the home.</p>
<p class="wide" style="margin-bottom:2rem"><a href="../contact_us.htm">Contact us</a> if you need more space or a marquee to enable social distancing.</p>
<div class="case-wrap">
<div class="case"><a href="/case_studies/covid_wedding.htm"><img src="/images/case/covid_xmas/wedding.jpg" alt="Temporary structures installed at a university" width="400" height="300" /></a><a href="/case_studies/covid_schools.htm" class="distancetop" >What kind of marquee wedding in 2021? &raquo;</a></div>
<div class="case"><a href="/case_studies/covid_schools.htm"><img src="/images/case/social_distancing/trinity.jpg" alt="Temporary structures installed at a university" width="400" height="300" /></a><a href="/case_studies/covid_schools.htm" class="distancetop" >More space in schools and universities &raquo;</a></div>
<div class="case rightcase"><a href="/case_studies/covid_restaurants.htm" ><img src="/images/case/covid_hospitality/distanced_eating.jpg" alt="Socially distanced restaurant marquee" width="400" height="300" loading="lazy" /></a><a href="/case_studies/covid_restaurants.htm" class="distancetop" >Restaurant marquees for outdoor dining &raquo;</a></div>
<div class="case"><a href="/case_studies/covid_pubs.htm" ><img src="/images/case/covid_hospitality/pub.jpg" alt="Three small Chinese hat tents provide outdoor cover for a pub" width="400" height="300" loading="lazy" /></a><a href="/case_studies/covid_pubs.htm" class="distancetop" >Outdoor cover for pubs &raquo;</a></div>
<div class="case rightcase"><a href="/case_studies/covid_xmas.htm" ><img src="/images/case/covid_xmas/exterior.jpg" alt="Small garden marquee for safer meeting up with friends and family" width="400" height="300" loading="lazy" /></a><a href="/case_studies/covid_xmas.htm" class="distancetop" >Small garden marquees for safer meetups &raquo;</a></div>
<div class="case"><a href="/case_studies/covid_medical.htm"><img src="/images/case/social_distancing/hospital2.jpg" alt="Spillover marquee for hospital staff" width="400" height="300" loading="lazy" /></a><a href="" class="distancetop" >Temporary structures for testing and in hospitals &raquo;</a></div>
</div>
</div>
EOQ;
?>