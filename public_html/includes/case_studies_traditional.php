<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/traditional/interior_big.jpg" title="Traditional marquee interior" class="fancybox" rel="group" alt="Traditional marquee interior"><img src="/images/case/traditional/interior.jpg" alt="Minimalist style marquee" width="273" height="186" /></a><p>A popular style: rows of trestle tables, hanging greenery in a traditional style marquee</p></div>
<div class="case rightcase"><a href="/images/case/traditional/interior_night_big.jpg" title="Lighting with fairy light canopy" class="fancybox" rel="group"><img src="/images/case/traditional/interior_night.jpg" alt="Lighting with fairy light canopy" width="273" height="186" /></a><p>Simple and effective lighting with a soft wash from uplighters, and a fairy light canopy echoed in candles on the tables</p></div>
<div class="case"><a href="/images/case/traditional/exterior_front_big.jpg" title="Traditional tent lit up at night" class="fancybox" rel="group"><img src="/images/case/traditional/exterior_front.jpg" alt="Traditional tent lit up at night" width="273" height="186" /></a><p>Nightime exterior of a traditional tent</p></div>
<div class="case rightcase"><a href="/images/case/traditional/exterior_big.jpg" title="Dancing seen through clear window panels" class="fancybox" rel="group"><img src="/images/case/traditional/exterior.jpg" alt="Dancing seen through clear window panels" width="273" height="186" /></a><p>From the side, dancing seen through clear window panels</p></div>
<div class="case bottomcase"><a href="/images/case/traditional/day_big.jpg" title="A traditional tent" class="fancybox" rel="group"><img src="/images/case/traditional/day.jpg" alt="A traditional tent" width="273" height="186" loading="lazy"/></a><p>A traditional tent</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/traditional/table_decoration_big.jpg" title="Table decoration" class="fancybox" rel="group"><img src="/images/case/traditional/table_decoration.jpg" alt="Table decoration" width="273" height="186" loading="lazy"/></a><p>Table decoration</p></div>
<hr/>
<p class="wide"><strong>The marquee</strong> was a traditional style tent in a field for 180 guests with a catering tent alongside.</p>
<p class="wide"><strong>The style</strong> has many of the popular features of marquee weddings nowadays: lined up trestle tables, a romantic, traditional style marquee, and hanging greenery &ndash; in this case elegant plants in baskets hung from wires stretched between the tent poles.</p>
<p class="wide"><strong>The lighting</strong> was simple but effective. About 16 uplighters for a soft overall light and a fairy light canopy throughout the marquee.</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a wedding marquee</p>
</div>
EOQ;
?>