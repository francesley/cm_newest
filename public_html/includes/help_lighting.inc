<p class="wide">Good lighting can create some of the most beautiful and dramatic effects inside marquees.</p>

<h2>Lighting equipment</h2>

<p>Lighting equipment available from County Marquees includes:</p>
<div class="textPhotos byP"><img src="/images/pages/lighting_equipment.jpg" alt="Marquee lighting equipment" width="198" height="502" />
<p><em>In the photo, from top left: night sky with lighting bar, fairy light canopy, uplighter, parcan, hanging paper lanterns, Eastern style lamps, chandelier</em></p>
</div>
<ul class="long">
<li>Festoon lighting</li>
<li>Fairy lighting</li>
<li>Lighting Truss &mdash; for putting schemes into effec</li>
<li>Wash lights, color large areas from a truss </li>
<li>Pinspots, highlight small areas from a truss</li>
<li>Disco Balls</li>
<li>Uplighters &mdash; for general lighting </li>
<li>Candelabra &mdash; for general lighting </li>
<li>LED uplighters &mdash; cast changing coloured light, can respond to sound and music</li>
<li>Night skies &mdash; black starcloth roof covering</li>
<li>Exterior floodlighting, both white and coloured</li>
<li>Hanging lanterns</li>
<li>Moroccan lanterns in different styles</li>
<li>Decorative chandeliers</li>
</ul>
<p class="byphoto">For prices, see our <a href="/pricing/furniture.htm#lighting">lighting price lists</a>.</p>

<h2>Lighting schemes</h2>
<p>Details of our lighting equipment cannot do justice to the magical effect of a well-planned lighting scheme.</p>
<div class="textPhotos">	
	<a href="/photos/party_tent.htm"><img src="/images/pages/party.jpg" alt="Marquee lighting effects" width="198" height="150" loading="lazy" /></a>
	<p class="view"><a href="/photos/party_tent.htm">View larger</a></p>
	<a href="/photos/dance_lighting.htm"><img src="/images/pages/decoration/sabine.jpg" alt="Party marquee lighting" width="198" height="150" loading="lazy" /></a>
	<p class="view"><a href="/photos/dance_lighting.htm">View larger</a></p>
</div>
<p>Like flowers, lighting can set the tone for a whole event event - dramatic, welcoming, surprising...</p>
<p>And don't forget marquee exteriors or their surroundings. In the right space, they can also be illuminated to create a stunning first impression on guests.</p>
<p>Careful control of lighting can also be very functional, focusing attention on stage speakers at more formal events, for example.</p>
<p>See the <a href="/photos.htm?category=lighting">lighting examples</a> in our gallery of marquee photos for more marquee lighting ideas. This <a href="/case_studies/venice.htm">case study</a> also shows off some exciting marquee lighting.</p>
<div class="unnec"></div>
<p class="endCall">And please don't hesitate to <a href="/contact_us.htm">contact us</a> to discuss your ideas.</p>




</div>