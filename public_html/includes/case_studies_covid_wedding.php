<?php
$content = <<<EOQ
<p class="wide">The introduction of Plan B on <strong>December 8th</strong> may mean changes for your wedding or event but the government has not yet clarified the implications.</p>
<p class="wide">The main impacts are likely to come from new rules on:</p>
<ul class="wide"><li>Wearing face coverings in public indoor locations from December 10th &ndash; but not in pubs, restaurants or gyms. The implications of this edict on ceremonies and receptions is unclear.</li>
<li>Proof of vaccination or pre-testing in indoor seated venues for more than 500. Again, the implications for large events are currently unclear.</li></ul>
<p class="wide">The government guidance on wedding and civil partnership ceremonies, receptions and celebrations has been updated to reflect the Plan B changes (for example, transport changes), but does not yet provide clarity on events themselves. You can read the guidance <strong><a href="https://www.gov.uk/guidance/coronavirus-covid-19-wedding-and-civil-partnership-ceremonies-receptions-and-celebrations" target="_blank" rel="noopener noreferrer">here</a></strong>.</p>
<p class="wide">We will update this page when the government lets us know more.</p>
<p class="wide">Our summary of the <em>Freedom Day</em> guidance for weddings and events is below for reference.</p>
<ul class="wide">
    <li>There are no limits on guest numbers</li>
    <li>Face coverings are not required</li>
    <li>Social distancing is not required</li>
    <li>There are no longer restrictions on dancing, singing or how food is consumed</li></ul>
<p class="wide">The only rule left is that people with Covid, or with symptoms of Covid, or those told to isolate by the NHS app, are not allowed to attend weddings.</p>
<p class="wide">There is some guidance from the government. For example:</p>
<ul class="wide">
    <li><strong>On face masks:</strong> &ldquo;<em>people are encouraged to respect other attendees and those working at events who may wish to adopt a more cautious approach. The government expects and recommends that people wear face coverings in crowded areas such as public transport.</em>&rdquo; (Bit odd that the government brings up public transport in its wedding guidance but there you go).</li>
    <li><strong>On ventilation:</strong> &ldquo;<em>when events take place inside or in other enclosed spaces, consider how the space can be continually well ventilated, before, during and after the event.</em>&rdquo;</li>   
    <li><strong>On social distancing:</strong>  &ldquo;<em>it's important to consider that others may wish to take a more cautious approach as we open up. We should all be considerate of this, and provide the opportunity and space for others to reduce close contacts if they wish</em>&rdquo;</li>
    <li><strong>On dancing:</strong> &ldquo;<em>some activities can also increase the risk of catching or passing on COVID-19 ... such as singing, dancing, exercising...The risk is greatest where these activities take place when people are in close contact with others, for example in crowded indoor spaces...In situations where there is a higher risk of catching or passing on COVID-19, you should be particularly careful to follow the guidance on keeping yourself and others safe...</em>&rdquo;</li>
</ul>
<p class="wide">Presumably, the same guidance applies to marquee parties and other types of marquee event since almost all Covid restrictions have now been lifted.</p>
<p class="wide">So it is really up to you now how you wish to hold your wedding or party and we will support you in all your choices. From our limited experience of marquee events booked for after Freedom Day, clients are taking a varied approach. Open sided marquees do remain a very popular option</p>
<p class="wide">For wedding ideas, browse the gorgeous examples in our <strong><a href="/photos.htm?category=wedding">wedding gallery</a></strong>. For parties, see our <strong><a href="/photos.htm?category=party">party gallery</a></strong>.</p>
<p class="wide">For clients who would still like to take a cautious approach, there are a few examples of socially distanced weddings below.</p>

<h2>Socially distanced marquee events:</h2>
<div class="case-wrap">
<div class="case bottomcase"><a href="/images/case/covid_weddings/covid_wedding_day_big.jpg" title="Our first wedding of 2021 &ndash; a pretty transparent tent for 15" class="fancybox" rel="group"><img src="/images/case/covid_weddings/covid_wedding_day.jpg" alt="Socially distanced wedding marquee" width="400" height="300" loading="lazy" /></a><p>Our first wedding of 2021 &ndash; a pretty, legal and safe transparent tent for 15 people in the beautiful grounds of Oakley Court Windsor</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/covid_weddings/covid_wedding_night_big.jpg" title="Looking glamorous at dusk" class="fancybox" rel="group"><img src="/images/case/covid_weddings/covid_wedding_night.jpg" alt="Looking glamorous at night" width="400" height="300" loading="lazy" /></a><p>Looking glamorous at dusk</p></div>
</div>
<div class="case-wrap">
<div class="case bottomcase"><a href="/images/case/covid_xmas/wedding_distanced_big.jpg" title="This wedding from last summer used a very spacious marquee for its 30 guests." class="fancybox" rel="group"><img src="/images/case/covid_xmas/wedding_distanced.jpg" alt="Socially distanced wedding marquee" width="400" height="300" loading="lazy" /></a><p>This wedding from last summer used a very spacious marquee for its 30 guests.</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/covid_xmas/wedding_big.jpg" title="Fresh and pretty from the outside." class="fancybox" rel="group"><img src="/images/case/covid_xmas/wedding.jpg" alt="Fresh and pretty socially distanced wedding marquee" width="400" height="300" loading="lazy" /></a><p>Fresh and pretty from the outside.</p></div>
</div>
</div>
EOQ;
?>