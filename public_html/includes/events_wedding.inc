<p class="wide">Your wedding day needs to be perfect in every way. With experience of more than 1,000 wedding marquees, we do understand that.</p>
<p class="wide">So we aim to manage our part of your event professionally and efficiently so that you can enjoy your day.</p>
<h2>Advantages of a marquee for a wedding</h2>
<p>Every marquee is unique. Set up, temporarily, for a single occasion, they are a space apart from normal life, embodying the specialness of each event.</p>
<div class="textPhotos">
<a href="/case_studies/festival.htm"><img src="/images/pages/wedding/candlelight.jpg" alt="Vintage marquee by candlelight" width="273" height="147"  /></a>
<p class="view"><a href="/case_studies/festival.htm" class="inline">View case study</a></p>
<a href="/photos/fete.htm"><img src="/images/pages/wedding/clear_roof.jpg" alt="Clear roof wedding tent" width="198" height="150"  class="second"/></a>
<p class="view"><a href="/photos/fete.htm">View in gallery &raquo;</a></p>
<div class="textPhotos"><a href="/photos/waiting.htm"><img src="/images/pages/wedding/planning.jpg" alt="Wedding reception in a marquee" width="198" height="150" loading="lazy" /></a>
<p class="view"><a href="/photos/waiting.htm">View in gallery &raquo;</a></p>
</div>
</div>
<p> In addition, marquees are very flexible. In a marquee, anything is possible: long, square, L-shaped; romantic, classical, themed.... Bride and groom can stamp their character on an event in any way they choose.</p>
<p>Hiring a marquee also gives you the opportunity to welcome guests to a wedding reception at home at a time of your choosing. And to party until late if the neighbours are agreeable.</p>
 <p>County Marquees supplies several different sort of marquees: flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pointy toppped Chinese Hat marquees. Which you choose will depend on your space, taste and budget.</p>
 <ul >
<li>See <a href="/equipment/marquees.htm">marquees</a> for more information on the different types of tents available.</li>
<li> Look at <a href="/pricing/marquees.htm">marquee pricing</a> to get an idea of the cost of wedding marquee hire. </li>
<li> <a href="/marquee_essentials.html">Where do I start?</a> and <a href="/marquee_questions.html">marquee hire questions</a> provide answers to questions you may have about hiring a marquee.</li>
</ul> 
<h2>Wedding styles</h2>
<p>There is no <em>right</em> style for decorating wedding marquees.</p>
<div class="textPhotos">
<a href="/photos/classy.htm"><img src="/images/pages/wedding/traditional.jpg" alt="Traditional style wedding marquee" width="273" height="147" loading="lazy" /></a>
<p class="view"><a href="/photos/classy.htm">View in gallery &raquo;</a></p>
<a href="/photos/extension.htm"><img src="/images/pages/wedding/with_sheepskins.jpg" alt="Spring arquee with daffodils and sheepskins" width="198" height="150"  class="second" loading="lazy" /></a>
<p class="view"><a href="/photos/extension.htm">View in gallery &raquo;</a></p>
 <a href="/case_studies/leafy.htm"><img src="/images/pages/wedding/pretty.jpg"" alt="Marquee with leaves hanging from the roof" width="198" height="150"  class="second" loading="lazy" /></a>
<p class="view"><a href="/case_studies/leafy.htm">View case study &raquo;</a></p>
<a href="/photos/wedding-tent.htm"><img src="/images/pages/wedding/stripey.jpg" alt="Traditional style wedding reception" width="198" height="150"  class="second" loading="lazy" /></a>
<p class="view"><a href="/photos/wedding-tent.htm">View in gallery &raquo;</a></p>
</div>
<p >Hanging things, especially greenery, from the roof of a marquee is currently very popular, often alongside strings of small fairy or larger festoon lighting.</p>
<p>The vintage look has evolved and in 2023 is less boho than it was a few years ago. Long, dark wood trestles tables remain popular, as do cross backed chairs. Bunting and paper lanterns are not as fashionable as they were but are still chosen by some brides.</p>
<p>Clear marquees look amazing, especially at night. But they are not always the most practical solution as they can heat up fast on hot days. Combining clear and opaque panels in a single marquee can be a good way to combine wow factor with reality. The <a href="/photos/reception.htm">classic white look</a> also remains in favour.</p>
<p> Examples include this <a href="/photos/wedding.htm">ravishing transparent marquee</a> or this <a href="/photos/bunting.htm">tent with hanging foliage</a>. There are lots more in our <a href="/photos.htm?category=wedding">wedding photo gallery</a>.</p>
<p class="byphoto">Whatever style you select for the reception, it is possible to transform the atmosphere with lighting effects for an evening celebration. See this <a href="/case_studies/relaxed.htm">case study</a> for an example.</p>
  <p>Remember, it's your day. So the style is up to you.</p>
  <ul>
    <li>Please browse the <a href="/photos.htm?category=wedding">wedding section</a> of our photo gallery for ideas. Or have a look at some <a href="/real.htm">real wedding marquees</a>.</li>
    <li>See <a href="/equipment/decoration.htm">marquee decoration</a> for suggestions about and links to decorative ideas specific to tents</li>
    <li>If you would like help,  please <a href="/contact_us.htm">ask us</a></li>
  </ul> 
  <h2>Planning for wedding marquees</h2>
<p>We take weddings seriously. We take care to ensure:</p>
<div class="textPhotos"><a href="/photos/bunting.htm"><img src="/images/pages/wedding/hanging.jpg" alt="Welcoming guests into the wedding marquee" width="198" height="150" loading="lazy" /></a>
<p class="view"><a href="/photos/bunting.htm" class="inline">View in gallery &raquo;</a></p>
</div>
<ul>
<li>that the marquee can accommodate the proposed numbers</li>
<li>that the chosen size will work with the layout</li>
<li>that the layout and position of a marquee will function well with caterering requirements, and with heating and electrical equipment</li>
</ul>
<p  class="wide">Above all, our staff are happy to work with you to discover the marquee that best accomodates your space, budget and dreams.</p>

<div class="textPhotos">
<a href="/case_studies/bunting.htm"><img src="/images/pages/wedding/bunting.jpg" alt="Shabby chic marquee wedding reception" width="198" height="150"/></a>		
<p class="view"><a href="/case_studies/bunting.htm"">View case study &raquo;</a></p>
</div>
<ul>
  <li>See <a href="/pricing/marquees.htm">marquee pricing</a> for prices and sizes</li>
  <li>See <a href="/pricing/sample.htm">sample prices </a> for examples of prices and equipment needs for different types of events</li>
  <li>Or please <a href="/contact_us.htm">contact us</a> to discuss your wedding marquee plans in more detail. </li>
</ul>
<h2>Organising the whole event</h2>
 <p >If you wish, we can take the work off your hands.</p>
 <div class="textPhotos"><img src="/images/pages/band.jpg" alt="Band playing in marquee" width="273" height="147" loading="lazy" /></div>
 <p >We have worked with many caterers, florists, entertainers etc so we know who offers the best service. You tell us what sort of event you require and we will be happy to sort it out for you.</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out more about marquee weddings.</p>
</div>