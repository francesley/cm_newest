<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/abi/exterior_big.jpg" title="Frame marquees need no extra space so the tent fitted neatly into the thin garden" class="fancybox" rel="group"><img src="/images/case/abi/exterior.jpg" alt="Frame marquee in thin London garden" width="273" height="186" /></a>
<p>Frame marquees need no extra space so the tent fitted neatly into the thin garden</p></div>
<div class="case rightcase"><a href="/images/case/abi/sky_big.jpg" title="Inside, the marquee was surprisingly roomy" class="fancybox" rel="group"><img src="/images/case/abi/sky.jpg" alt="Inside, the marquee was surprisingly roomy" width="273" height="186" /></a><p>Inside, the marquee was surprisingly roomy</p></div>
<div class="case"><a href="/images/case/abi/empty_big.jpg" title="We put up the wires, the client found and attached leaves to the roof" class="fancybox" rel="group"><img src="/images/case/abi/empty.jpg" alt="We put up the wires, the client found and attached leaves to the roof" width="273" height="186" /></a><p>We put up the wires, the client found and attached leaves to the roof</p></div>
<div class="case rightcase"><a href="/images/case/abi/chillout_big.jpg" title="There was room for a small chillout area in one corner of the tent" class="fancybox" rel="group"><img src="/images/case/abi/chillout.jpg" alt="Small chillout area in one corner of the tent" width="273" height="186" /></a><p>There was room for a small chillout area in one corner of the tent</p></div>
<div class="case"><a href="/images/case/abi/night_big.jpg" title="With doors and walls drawn shut against the cool evening, the partying could go on late into the night" class="fancybox" rel="group" loading="lazy"><img src="/images/case/abi/night.jpg" alt="Vintage table style" width="273" height="186" loading="lazy" /></a><p>With doors and walls drawn shut against the cool evening and some tolerant neighbours, the partying could go on late into the night.</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a fiftieth birthday party.</p>
<p class="wide"><strong>The venue</strong> was the clients garden, a typically long but quite narrow garden in North London.</p>
<p class="wide"><strong>The solution:</strong> a 12m (40') x 24m (80') frame marquee  Wires strung across the roof created a structure to hang decorative branches and leaves.</p>

<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about garden marquees.</p>
</div>
EOQ;
?>