<p class="wide">Hiring a marquee is expensive and for most people an unfamiliar experience. Below, we attempt to help you answer some basic questions about marquees that we hope will help you decide if a marquee is the right option for you.</p>

<dl>

<dt id="howmuch">1. How much does it cost to hire a marquee?</dt>
<dd>
  <p>How long is a piece of string?</p>
  <p>The prices on our <a href="/pricing/marquees.htm">marquee price list</a> range from &pound;400 to over &pound;5000. These prices include matting, standard lighting, delivery and set up, and, for the higher of the two prices for each size, marquee lining as well. Furniture will need to be factored on top.</p>
  <p>Unfortunately, working out how much <em>your</em> marquee will cost isn't totally straightforward.</p>
<ul>
<li>The <strong>quickest</strong> way to get an <strong>rough idea of prices</strong> is to use our <a href="/pricing/quote.htm">online marquee quote</a>. It is fast and accurate, and includes common extras. You can print or email yourself quotes with no obligation.</li>
<li>Another <strong>simple way</strong> is to use the fact that marquees typically cost between &pound;15 and &pound;50 per person. The lower price is for a basic unlined marquee with matting, simple lighting and furniture. The higher price would be for the marquee with absolutely everything - hard floor, carpet, night sky, chill out areas etc. Most marques fall within the &pound;20 to &pound;30 bracket.
  <br/><br/>So pick a number that seems about right for the sort of marquee you want and then multiply that by the number of guests you expect. That will give you a rough idea of the cost of a marquee.</li>
  <li>A <strong>more accurate</strong> method is to measure your site or work out how big a marquee you need (see next question). You can then look up the price on our <a href="/pricing/marquees.htm">marquee price lists</a>. You will then need to add furniture and accessories on top using our <a href="/pricing/furniture.htm">furniture</a> and <a href="/pricing/equipment.htm">equipment</a> prices.</li>
 <li>For a <strong>rough guide</strong>, we also have a few <a href="/pricing/sample.htm">sample prices of complete marquees</a> of different types on this website.</li></ul>
  <p>For an accurate price, please feel free to <a href="/contact_us.htm">contact us</a>. Or you can <a href="/pricing/quote.htm">request a personalised quotation</a> from this website if you prefer.</p>
</dd>
<dt id="size">2. How big a marquee do I need?</dt>
<dd><p>The answer depends on a number of factors: </p>
<ul><li><strong>How many guests</strong> you are expecting</li>
<li>Any <strong>extras</strong> - typically dance floors or tables to serve food</li>
<li>Whether you would like <strong>extra space</strong> for milling about</li>	
</ul>
<p>Armed with these facts, you can work out your space requirements in a couple of ways:</p>
<ul>
<li><strong>Price list:</strong> check out our <a href="/pricing/marquees.htm">marquee price list</a>. It shows approximate guest capacities for seated and buffet events for all the sizes and types of marquees available from County Marquees.</li>
<li><strong>Work it out:</strong>
	<ul>
	<li>For a buffet event you will need a <em>minimum</em> of 8 square feet per person</li>
	<li>For a seated event you will need a <em>minimum</em> of 15 square feet per person</li>
	<li>Then you will need to add on space for extras like dance floors</li>
	</ul>
</li>	
</ul>
<p>Or please <a href="/contact_us.htm">contact us</a> for an accurate assessment. We are always happy to help. </p>
</dd>
<dt id="process">3. How does the marquee hire process work?</dt>
<dd><p>Hiring a marquee from our company is simple.</p>
 <ul>
   <li>If you would like one, we offer <strong>free no-obligation site visits</strong> where we will inspect and measure your site, and offer suggestions to help achieve  the type of  marquee, size, layout, and look that you want.</li>
   <li>We send you a <strong>quote</strong>.</li>
 <li>Following a confirmed booking, we will contact you a fortnight before the event to <strong>arrange dates</strong> for putting up and taking down. Normally, we put up a marquee a few days before your event, and dismantle it a few days after. But we can fit in with your arrangements.</li>
 <li>We <strong>put up the marquee</strong>. If the marquee is at your home, we will interfere with your house and life as little as access to the site allows. How long put-up takes depends on the size of the marquee. In most cases, it will take less than a day. Larger marquees may take two days or even more.</li>
  <li>After the event, we <strong>take down the marquee</strong> and ensure that the site is left clean and tidy. Typically, this will take less than a day.</li>
 </ul>
</dd>
<dt id="uneven">4. Will you make a mess of my garden?</dt>
<dd><p>We could just say no. But instead we ask you to please check out our <a href="/company/testimonials.htm">testimonials</a> where you can see the praise lavished on our team for the quality of service they offer.</p>
<p>We have a fantastic team of trained marquee erectors who all know how important it is to leave a site clean and tidy. You can be assured that wherever we go, we will not leave a mess.</p></dd>

<dt id="sort">5. What sort of marquee do I need?</dt>
<dd><p>County Marquees supplies four types of tents:</p>
<ul><li><a href="/equipment/marquees.htm#frame">Frame marquees</a>: rectangular shaped tents that need no extra space around them so they are good for smaller spaces and as an <em>extension</em> to a building. They can also be combined for flexible space planning. Frame marquees have no inner poles so sight lines are great.</li>
 <li><a href="/equipment/marquees.htm#traditional">Traditional marquees</a>: pretty tents with curved tops and inner poles. Need more space around them so require slightly larger venues.</li>
 <li><a href="/equipment/marquees.htm#chinese">Chinese hat marquees</a>: Attractive pointy-topped tents, with a maximum size of 6m x 6m (20' x 20'). Good for making a splash at smaller party, or as porches, chill-out areas etc at larger events.</li>
 <li><a href="/equipment/stretch_tents.htm">Stretch tents</a>: Eyecatching shape and good for fixing on all different surfaces, stretch tents are incredibly flexible and can morph into different shapes to suit your space.</li>
 </ul>
<p>Please see <a href="/equipment/marquees.htm">marquees</a> for more about all types.</p></dd>
<dt id="just">6. Can you help me find a marquee venue?</dt>
<dd>
  <p>Finding the perfect venue for a marquee at the right price isn't always an easy task. We have on this site a <a href="/venues.htm">marquee venue finder</a> where you will be able to see marquee venues we know of in your area.</p>

</dd>
</dl>
<p class="wide">Answers to more detailed marquee questions are on our page of <a href="/marquee_questions.html">Frequently asked questions</a>.</p>
<p class="endCall">Or for personal answers to all your questions, please <a href="/contact_us.htm">contact us</a>.</p>
</div>
