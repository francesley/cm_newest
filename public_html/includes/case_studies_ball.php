<?php

$content = <<<EOQ

<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><img src="/images/case/trinity_layingout.jpg" alt="Laying out marquees" width="273" height="186" /><p>Marking out the space for a marquee. The frame of the second large marquee is in the background</p></div>
<div class="case rightcase"><img src="/images/case/trinity_halfup.jpg" alt="View from partially erected marquee" width="273" height="186" /><p>Partially erected marquee with unlined roof and legs</p></div>
<div class="case"><img src="/images/case/trinity_beer.jpg" alt="Sugababes playing in ball marquee" width="273" height="186" /><p>Sugababes playing on the larger stage</p></div>
<div class="case rightcase"><img src="/images/case/trinity_bar.jpg" alt="Chinese hat marquee serving as a bar" width="273" height="186" /><p>Chinese hat marquee serving as a bar</p>
<p><a href="/photos/pagoda.htm" class="view">View larger</a></p></div>
<div class="case"><img src="/images/case/trinity_outsidelights.jpg" alt="Lights strung up outside" width="273" height="186" /><p>Festoon lighting strung up outside one of the marquees</p></div>
<div class="case rightcase"><img src="/images/case/trinity_outside.jpg" alt="Sitting down outside a marquee" width="273" height="186" loading="lazy" /><p>Socializing outside an open sided illuminated marquee</p></div>	
<div class="case bottomcase"><img src="/images/case/trinity_cards.jpg" alt="Playing roulette" width="273" height="186" loading="lazy" /><p>Gambling inside one of the main marquee.</p></div>
<div class="case rightcase bottomcase"><img src="/images/case/trinity_stage.jpg" alt="Marquee stage with lighting" width="273" height="186" loading="lazy" /><p>Performance stage with dance floor inside an unlined marquee</p> </div>
<hr>
<p class="wide"><strong>The occasion</strong> was an university summer ball.</p>
<p class="wide"><strong>The venue</strong> was... perfect! Sweeping flat lawns. The ideal venue.</p>
<p class="wide"><strong>The challenge</strong> was to provide two stages, areas for dancing, eating, relaxation and a bar. Outer seating was also required.</p>
<p class="wide abovecase">You can read another case study of a university ball with marquees <a href="/case_studies/balls.html">here</a>.</p>

<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about creating a ball inside a marquee.</p>
</div>
EOQ;
?>