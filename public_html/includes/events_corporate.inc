
<p>County Marquees are  marquee hire specialists with a full range of <a href="equipment/marquees.htm">marquees</a> and <a href="equipment/furniture.htm">equipment</a>, a skilled workforce, and many years of experience managing corporate marquee solutions.</p>
<div class="textPhotos">
<a href="/photos/butterfly.htm"><img src="/images/pages/corporate/trinity_stage2.jpg" alt="Hospitality marquees" width="198" height="150" class="second"/></a>
<p>Hospitality marquees <a href="/photos/butterfly.htm">View larger &raquo;</a></p>
</div>
<p class="byphoto">Our priority for your event is to create the  environment you require, whether the focus is hospitality, exhibition, product launch or sport. And to do it on time, on budget and without fuss.</p>
<p class="wide"><a href="/photos.htm?category=corporate">View photos</a> of past corporate marquee events.</p>
<h2>Corporate marquee clients</h2>
<p class="wide">County Marquees has considerable experience of event marquees. This is a selection of our clients. You can also view a<a href="company/clients.html"> full client list</a>. </p>
<ul class="float long">
<li>Bank of England </li>
<li>Barnet Borough Council</li>
<li>Boosey &amp; Hawkes</li>
<li>British Rowing</li>
<li>Chrysalis TV Group</li>
<li>Consumers Association</li>
<li>GlaxoSmithKline</li>
<li>Guards and Cavalry Club </li>
<li>Harper Collins</li>
<li>Harrow School </li>
<li>Herbert Smith</li>
</ul>
<ul class="float long" style="margin-right:0"><li>King's Troop</li>
<li>Logica</li>
<li>Newham Hospital</li>
<li>NSPCC</li>
<li>RIBA</li>
<li>Price Waterhouse</li>
<li>Sarasin Chiswell</li>
<li>Thames &amp; Hudson </li>
<li>Virgin</li>
<li>Ukrainian Embassy</li>
<li>University of Middlesex</li>
</ul>
<h2>Event types</h2>
<div class="textPhotos byP">
<a href="/photos/pink-marquee.htm"><img src="/images/pages/corporate/dcam_conference.jpg" alt="Marquee for national political conference" width="273" height="147" border="0" class="second" loading="lazy" /></a>
<p  class="caption">Anti corruption conference <a href="/photos/pink-marquee.htm">View larger &raquo;</a></p>
<a href="/case_studies/christmas.htm"><img src="/images/pages/corporate/christmas.jpg" alt="Christmas party marquee" width="273" height="147" class="second" loading="lazy" /></a><p class="caption">Corporate Christmas party  <a href="/case_studies/christmas.htm">View a case study &raquo;</a></p>
<a href="/photos/ark_royal.htm?category=corporate"><img src="/images/pages/corporate/ark_royal.jpg" alt="Decommissioning the Ark Royal" width="190" height="150" class="second" loading="lazy" /></a><p style="white-space:nowrap">Decommissioning the Ark Royal <a href="/photos/ark_royal.htm?category=corporate">View larger &raquo;</a></p>
</div>
<p class="byphoto">We have experience of the following types of event marquees:</p>
<ul class="long">
<li>Balls</li>
<li>Exhibitions</li>
<li>Shows</li>
<li>Hospitality tents</li>
<li>Promotional events</li>
<li>Product launches </li>
<li>Sports event marquees</li>
<li>Synagogue marquees for festivals</li>
<li>Fun days</li>
<li>Games events </li>
</ul>
<p class="byphoto">We have modular <a href="/corporate-marquees.htm#equip">marquees</a> that can accommodate large or small gatherings of people and a wide range of <a href="/corporate-marquees.htm#equip">accessories and ancillary equipment</a> to tailor a marquee to suit your event. Branding with furnishings or creative lighting is also an option.</p>
<p class="byphoto"><a href="/contact_us.htm">Contact us</a> to find out more about our event marquees.</p>

<h2 id="equip">Equipment</h2>
<div class="textPhotos byP">
<a href="/photos/launch.htm"><img src="/images/pages/corporate/jaguar.jpg" alt="Marquee with transparent roof for skating rink" width="190" height="150" class="second"/></a>
<img src="/images/pages/corporate/large.jpg" alt="Interior of a corporate marquee event" width="190" height="150" class="second" loading="lazy" />
<img src="/images/pages/corporate/wimbledon.jpg" alt="Sports marquee event at Wimbledon" width="198" height="150" loading="lazy" />
</div>
<ul>
<li><strong>Marquees</strong>: 4m (14') &#8212; 15m (50') clearspan frame structures that can be combined in various shapes to make best use of space. Also traditional style tents, stretch tents and <em>chinese hats</em>.
<br/><a href="/pricing/marquees.htm">View prices and sizes &raquo;</a><br/>
<a href="/equipment/marquees.htm">View more information &raquo; </a></li>

<li><strong>Furniture</strong>: Comprehensive range of tables, chairs, dance floors and flooring.<br/>
<a href="/pricing/furniture.htm">View prices &raquo;</a><br/>
<a href="/equipment/furniture.htm">View more information &raquo;</a></li>
<li><strong>Lighting:</strong> Comprehensive range. Our experienced staff can help devise the best lighting scheme for your event.<br/>
<a href="/equipment/lighting.htm">View more information &raquo;</a></li>
<li><strong>Accessories: </strong>Bar units, staging, staged seating, podiums, picket fencing, electrical generators and portable loos.<br />
<a href="/pricing/equipment.htm">View pricing &raquo;</a></li></li>
<li><strong>Interior decor</strong>: Selection of linings including starcloth, swags in all colours. Various corporate branding options.<br/>
<a href="/equipment/decoration.htm">View more infomation &raquo;</a></li>
</ul>
<p class="endCall">To find out more about event marquee solutions from County Marquees, please <a href="contact_us.htm">contact us</a>.</p>
</div>