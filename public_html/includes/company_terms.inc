<h2> Definitions</h2>
        <ul>
         <li >&quot;The company&quot; is COUNTY MARQUEES
             and/or their subcontractors or agents.</li>
         <li> &quot;The hirer&quot; is the person hiring equipment from
             the company.</li>
         <li>&quot;The equipment&quot; is all items provided to or
             hired by the hirer.</li>
         <li> &quot;The period of hire&quot; means the time commencing
             with the arrival of the equipment on site, and terminating when
             the equipment is removed by the company.</li>
         <li> &quot;The hire agreement&quot; is the contract entered
             into by the hirer and the company. </li>
        </ul>
        <h2>General</h2>
        <p>These
        terms and conditions apply to all contracts entered into by the company unless otherwise stated in the company's written quotation.&nbsp; Any
        offer of equipment is subject to stock being available on receipt of an order.</p>
        <h2 >Terms</h2>
        <p>The hirer will pay a non-returnable deposit of a designated sum inclusive of Vat (as detailed in the quotation at the time of the order) and the balance of the total hire charge on commencement of the period of hire.  Should full settlement not be made on the day of hire then the company reserves the right to charge interest at 4% per annum above the base rate of Lloyds Bank.</p>
		<h2 >Cancellation</h2>
		<p>In the event of cancellation the following charges will be invoiced and due for payment on the date of commencement of the originally contracted period of hire:</p>
        <ul>		
         <li> Cancellation more than 28 days before the commencement of the period of hire - 20% of the total hire charge.</li>
         <li> Cancellation between 14 and 8 days before the commencement of the period of hire - 50% of total hire charge.</li>
         <li> Cancellation less than 14 days before the commencement of the period of hire - 90% of the total hire charge.</li>
         <li> The deposit paid is not returnable under any circumstances.</li>
        </ul>
        <h2 >Site conditions</h2>
        <p>The companies quotation for hire charges is dependent on a level firm site being provided with easy access for commercial
        vehicles. The company cannot be held responsible for damage caused to concealed or buried pipes, cables and other services and features
        unless their positions have been clearly marked on the site by the hirer.</p>
        <p>The  quotation for lighting is made on the assumption that a suitable and sufficient power point is available within 5 meters of the marquee.
        The company reserves the right to erect, dismantle and remove the equipment from the site at its convenience. The hire charges do not
        include any repairs or making good that may be required to the site. </p>
		<h2 >Health and safety</h2>
		<p>The hirer shall ensure that all doors and other openings into the marquee(s) are closed and secured at all times during which the marquee(s) are not in use.  The company reserves the right, in its absolute discretion, to require the evacuation of a marquee(s) and/or the cancellation of an event to be held in the marquee(s). Where this occurs due to health and safety considerations the company accepts no liability for any loss whatsoever.  Any alteration or addition to the Equipment by the hirer or his licensee must have the prior written permission of the company.</p>
        <h2 >Hirer's responsibilities</h2>
        <p>The
        hirer shall provide the company with a plan showing where he/she requires the equipment to be erected, or alternatively have a
        representative on the site for that purpose. Otherwise the company will erect the equipment where it thinks fit and a further charge shall be
        incurred if the hirer wishes the equipment to be repositioned. </p>
        <p> The hirer is responsible for obtaining any site permits that may be necessary.  The hirer is responsible and will indemnify the company against any loss or damage whatsoever the cause unless the hirer has paid the damage waiver fee referred to on the quotation/confirmation of order. In no circumstances will the liability of the Company exceed the total hire charge. The hirer will remain responsible and indemnify the company against any damage or loss caused by their negligence.  In the event of a claim for loss or damage being accepted by the company's insurers, the hirer will be liable for the first &pound;500.00 of any such loss or damage.</p>
        <h2 >Third party liability</h2>
        <p>The company will not be responsible for, and the hirer will indemnify the company against, all claims for injury to persons, or loss of, or
        damage to, property, however caused, unless it be proved that such injury or damage resulted from faulty materials, workmanship, or negligence on the part of the company. The Company will not be responsible for mechanical or electronic failure irrespective of the cause of this. </p>
        <h2 >Force Majeure</h2>
        <p> Whilst every effort will be made by the company to complete any orders, the company cannot be held liable for variation or non-completion of orders due to Act of God, Fire, Flood, Storm, Gale, Tempest, War, Pandemic, Terrorism, Strikes, Riots, Lockouts or any other cause whatsoever that is beyond the company's control.</p>
</div>