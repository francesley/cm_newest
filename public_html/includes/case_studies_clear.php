<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/clear/clear_marquee_big.jpg" title="Dramatic illuminated transparent marquee" class="fancybox" rel="group" alt="Dramatic illuminated transparent marquee"><img src="/images/case/clear/clear_marquee.jpg" alt="Dramatic illuminated transparent marquee" width="273" height="186" /></a><p>Dramatic illuminated transparent marquee. Glamorous mirrored bar at the centre</p></div>
<div class="case rightcase"><a href="/images/case/clear/transparent_marquee_big.jpg" title="Candles and potted trees for decoration" class="fancybox" rel="group"><img src="/images/case/clear/transparent_marquee.jpg" alt="Candles and potted trees for deocration" width="273" height="186" /></a><p>Candles and potted trees are all that is needed to create a splash inside the tent</p></div>
<div class="case"><a href="/images/case/clear/profile_big.jpg" title="Exterior side view at dusk" class="fancybox" rel="group"><img src="/images/case/clear/profile.jpg" alt="Exterior side view at dusk" width="273" height="186" /></a><p>Exterior side view at dusk</p></div>
<div class="case rightcase"><a href="/images/case/clear/transparent_exterior_big.jpg" title="Transparent marquee exterior at night" class="fancybox" rel="group"><img src="/images/case/clear/transparent_exterior.jpg" alt="Transparent marquee exterior at night" width="273" height="186" /></a><p>Eyepopping clear marquee exterior when lit up at night</p></div>
<div class="case bottomcase"><a href="/images/case/clear/ready_big.jpg" title="Marquee ready for action" class="fancybox" rel="group"><img src="/images/case/clear/ready.jpg" alt="Marquee ready for action" width="273" height="186" loading="lazy" /></a><p>Daytime view &ndash; ready for action</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/clear/day_big.jpg" title="Positioned on a tennis court" class="fancybox" rel="group" loading="lazy"><img src="/images/case/clear/day.jpg" alt="Positioned on a tennis court" width="273" height="186" /></a><p>Positioned on a tennis court</p></div>
<hr/>
<p class="wide"><strong>The occasion</strong> was Emily and Harry's ravishing wedding in a beautifully illuminated transparent frame marquee on a tennis court with candles on long trestle tables, potted trees and an eye catching mirrored bar. The effect was gorgeous inside and out!</p>
<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a wedding marquee</p>
</div>
EOQ;
?>