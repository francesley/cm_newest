
<p>It is best to think of marquee decoration as but  part of the overall look for an event.</p>
<div class="textPhotos">
<a href="/photos/wedding_effects.htm"><img src="/images/pages/decoration/very_vintage.jpg" alt="Vintage marquee decoration" width="198" height="150" /></a>
<p class="view"><a href="/photos/wedding_effects.htm" class="inline">View in gallery</a> | <a href="/case_studies/festival.htm" class="inline">View case study</a></p>
<a href="/photos/venice.htm"><img src="/images/pages/decoration/venice_tables.jpg" alt="Themed marquee" width="198" height="150"/></a>
<p class="view"><a href="/photos/venice.htm" class="inline">View in gallery</a> | <a href="/case_studies/venice.htm" class="inline">View case study</a></p>
<a href="/photos/wedding.htm"><img src="/images/pages/clear_marquee.jpg" alt="Clear marquee" width="198" height="150"/></a>
<p class="view"><a href="/photos/wedding.htm" class="inline">View in gallery</a></p>
</div>
<p>Marquees are flexible enough to carry classic styles, romantic styles, functional, funky, vintage... as well as many themed looks.</p>
<p>You need to decide on a style or theme and then pick table centrepieces, flowers and other items to suit. Not simple, but there are a loads of resources on the web to help. The <a href="/photos.htm?category=decoration">decoration section of our own photo gallery</a> also illustrates lots of decorative ideas.</p>
<p>Corporate marquee ideas are harder to come by. For inspiration try a  <a href="https://www.google.co.uk/search?q=melbourne+cup+marquees&rlz=1C1GGLS_en-GBGB360GB361&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjL1On3g5PXAhXBLsAKHR79BJUQ_AUICigB&biw=1680&bih=919" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/decoration/google_birdcage_images'])" rel="noopener noreferrer">Google image search</a> for corporate tents inside the exclusive Birdcage at Australia's Melbourne cup. The event is reknown for its opulent corporate marquees.</p>
<p>On this page, we highlight a few  items particular to marquees that, coupled with <a href="lighting.htm">lighting</a> and <a href="furniture.htm">furniture</a>, can help create the atmosphere you want.</p>
<div class="unnec"></div>
<h2>Hanging decorations:</h2>
<p>Hanging things from the roof of marquees &ndash; especially leaves &ndash; is one of the most popular decorative strategies of the moment.</p>
<div class="textPhotos">
<a href="/photos/frame.htm"><img src="/images/pages/decoration/on_the_roof.jpg" alt="Strings of intertwined leaves across marquee roof" width="198" height="150" loading="lazy" /></a>
<p class="view"><a href="/photos/frame.htm">View in gallery</a></p>
<a href="/photos/bunting.htm"><img src="/images/pages/decoration/hanging.jpg" alt="Marquee with hanging foliage baskets" width="198" height="150"  class="second" loading="lazy" /></a>
<p class="view"><a href="/photos/bunting.htm">View in gallery</a></p>
<a href="/photos/informal.htm"><img src="/images/pages/decoration/hanging_flowers.jpg" alt="Restrained and stylish leafy centrepiece" width="198" height="150"  class="second" loading="lazy" /></a>
<p class="view"><a href="/photos/informal.htm">View in gallery</a></p>
</div>
<p>Foliage is often interwined with strings of fairy lights or larger festoon lighting. There are other options:</p>
<ul>
<li>paper lanterns &ndash; combined with flowers for this <a href="/photos/relaxed.htm">wedding</a></li>
<li>ribbons or origami strings (can look surprisingly effective)</li>
<li>balloons &ndash; pirate ships in this <a href="/photos/pirates.htm">pirate themed corporate marquee event</a>)</li>
<li>novelty items &ndash; we have hung bicycles, upturned parasols, wine crates and <a href="/photos/venice.htm">giant Venetian Carnival masks</a></li>
<li>Have a look at our <a href="https://www.pinterest.com/county_marquees/hanging-decorations/" target="_blank" onclick="_gaq.push(['_trackEvent', 'Social', 'pinterest_hanging');" rel="noopener noreferrer">Pinterest board</a> which has lots more hanging decoration ideas</li>
</ul>
<p>You can also hang things down marquee walls as in <a href="/photos/flower_marquee.htm">this photo</a> with sprays of flowers prettily dangling down.</p>
<div class="unnec"></div>
<h2>What happened to vintage:</h2>
<div class="textPhotos">
<a href="/photos/wedding_effects.htm"><img src="/images/pages/decoration/wood_tables.jpg" alt="Wedding tent with dark wood tables and wooden chairs" width="198" height="150" class="second" loading="lazy" /></a>
<p class="view"><a href="/photos/wedding_effects.htm">View in gallery</a></p>
<a href="/photos/lighting-effects.htm"><img src="/images/pages/decoration/hanging_decorations.jpg" alt="Greenery in bowls hanging from marquee roof" width="198" height="150" class="second" loading="lazy" /></a>
<p class="view"><a href="/photos/lighting-effects.htm">View in gallery</a></p>
<img src="/images/pages/lighting/festoon_lighting.jpg" alt="Tent with fairy lights" width="190" height="150" loading="lazy" />
</div>
<p>A few years ago, most of the wedding marquees we set up looked a bit like a village fete with bunting featuring heavily and hanging lanterns almost as popular.</p>
<p>Both are still used but nowhere near as much. And the vintage style seems to have evolved away from boho towards a more rustic look with more variation. Popular components include, amongst other things, wooden chairs or limewashed chiavari chairs, rich dark-coloured trestle tables, simple pretty flowers, table runners...</p>
<p>Very popular currently are strings of festoon (bigger) or fairy lights (smaller). It's understandable. They create a magical atmosphere as night comes on.</p> 
<p>This <a href="/case_studies/vintage_wedding.htm">vintage style wedding marquee</a> has some of the features described above but has a unique overall feel with a hanging leaf canopy with coloured glass bowls dangling through the leaves.</p>
<p>This <a href="/photos/vintage.htm">vintage party marquee</a> takes a historical approach to the vintage look with furniture and bunting designs from the early twentieth century.</p>
<h2>Black-out linings:</h2>
<p >Marquees can also be completely or partially <em>blacked out</em>, typically in combination with a starlight ceiling.</p>
<div class="textPhotos"><a href="/case_studies/fiftieth.htm"><img src="/images/pages/decoration/black3.jpg" alt="Elegant birthday party tent" width="198" height="150"  loading="lazy" /></a>
<p class="view"><a href="/case_studies/fiftieth.htm" class="inline">View case study</a> | <a href="/photos/birthday.htm" class="inline">View in gallery </a></p>
</div>
<p>The effect is dramatic and fun &#8212; and popular for evening parties.</p>
<p>Coloured lighting can enhance the effect by spreading washes of colour across the black.</p>
<div class="unnec"></div>
<h2>Walls, windows and poles:</h2>
 <p >Other marquee specific items to consider include:</p>
  <div class="textPhotos"><a href="/case_studies/small.htm"><img src="/images/pages/marquees/small_vintage_marquee.jpg" alt="Open sided marquee with vintage decoration"  width="198" height="150"  loading="lazy" /></a>
  <a href="/case_studies/wedding_tent.htm"><img src="/images/pages/decoration/panoramic_windows.jpg" alt="Marquee with panoramic windows"  width="198" height="150" class="second"  loading="lazy" /></a>
<img src="/images/pages/decoration/pole_treatment.jpg" alt="Marquee pole decoration" title="Marquee pole decoration" width="198" height="150"  class="second" loading="lazy" /></div>
<ul>
<li><strong>Opening up the walls in summer:</strong> drawing back the walls allows the landscape around to be part of your decor while still enjoying the protection of a tent. <br/><br/>See <a href="/marquee_questions.html#open">this question</a> or <a href="/photos/summer.htm">this photo</a> for more examples.</li>
<li><strong>Windows:</strong> a similar outdoors effect can be achieved on colder days with windows.<br/><br/>
Clear panoramic panels make a whole bay clear so you can enjoy a view unimpeded. Arched <em>Georgian</em> style windows have a more formal, traditional look.</li>
<li><strong>Poles:</strong> in traditional tents or on open sides of frame marquees, provide another opportunity to make a decorative statement. They can be draped with material, or garlanded with flowers, ribbons or what you will.</li>
</ul>
<div class="unnec"></div>
<h2>Roofs:</h2>
<div class="textPhotos"><a href="/photos/fete.htm"><img src="/images/pages/decoration/clear_roof.jpg" alt="Marquee with clear roof" width="198" height="150" loading="lazy" /></a>
<p class="view"><a href="/photos/fete.htm">View in gallery</a></p>
<img src="/images/pages/decoration/roof_swag.jpg" alt="Marquee roof decorated with fabric overlays" width="198" height="150" class="second" loading="lazy" /></div>
<p>Marquee roofs can be lined, blacked out or transparent. All three types can be decorated with greenery of many kinds, or with fabric overlays.</p>
<p>The upper photo alongside shows a clear roof with fabric overlays.</p>
<p>The lower photo shows multi-coloured overlays combined with festoon lighting for a bright, cheerful atmosphere.</p>
<div class="unnec"></div>
<h2>Bunting:</h2>
<div class="textPhotos">
<img src="/images/pages/decoration/bunting_round_edge.jpg" alt="Patterned bunting in a wedding tent" width="190" height="150" loading="lazy"  />
<img src="/images/pages/decoration/bunting2.jpg" alt="Colourful bunting strung across the roof of a marquee" width="190" height="150" loading="lazy"  />
<p class="view"><a href="https://pinterest.com/county_marquees/bunting/" target="_blank" rel="noopener noreferrer">View more bunting ideas on our <em>Pinterest</em> board</a></p>
</div>
<p>Bunting or flags in all sorts of fabrics can be strung across or around the roof of a tent (as in the two photos alongside), often as an element of a vintage style design. Bunting can also radiate from the high centre of a traditional style marquee.</p>
<p>Bunting can be custom-made with your choice of fabric, or bought off the shelf. If you are looking for decoration ideas, we maintain a <a href="https://pinterest.com/county_marquees/bunting/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/decoration/pinterest_bunting'])" rel="noopener noreferrer"><em>Pinterest board</em></a> to show some different bunting styles.</p> 
<p>This <a href="/case_studies/bunting.htm">case study</a> is an elegant example of the <em>shabby chic</em> with custom-made bunting look. This <a href="/photos/vintage.htm">vintage party marquee</a> features unusual bunting hand made from William Morris and Charles Renee Mackintosh designs.</p>
<div class="unnec"></div>
<h2>Linings:</h2>
<p>Marquees can be <em>lined</em>, a natural accompaniment to formal or traditionally-styled events. Please see this <a href="../marquee_questions.html#lining">frequently asked question</a> for a photo showing the difference between lined and unlined marquees.</p>
<div class="textPhotos"><img src="/images/pages/decoration/gothic.jpg" alt="Atmospheric marquee decoration" width="198" height="150" loading="lazy" />
<p>Marquee lining with decor</p></div>
<p>County Marquees' standard linings are pleated and ivory-coloured. They are an ideal backdrop for most styles. They can be set off with swags, roof swags, bunting (see below), as well as with lighting effects, theming accessories and bespoke printed linings. We also stock flat ivory linings.</p>
<div class="unnec"></div>
<h2>Chill out areas:</h2>
<div class="textPhotos"><img src="/images/pages/decoration/larger_corner_sofas.jpg" alt="Colourful sofas, cushions and low tables in a chill out area" width="198" height="150"  loading="lazy" /><img src="/images/pages/decoration/chillout.jpg" alt="Chill-out area with soft cube seating" width="198" height="150" class="second" loading="lazy" /></div>
<p >Events can be exhausting. And the experience of sharing a table with the same small group of people for hours can feel a little intense.</p>
<p >As a way of helping guests relax for a while, chill out areas have become popular features of long weddings and parties.</p>
<p >Chill out areas can be in a separate small marquee (<a href="/equipment/marquees.htm#chinese">chinese hat marquees</a> look fun and are the ideal size); or they can be in a more or less screened-off section of the main marquee.</p>
<p >Sofas, soft cube seating and low tables are common furnishings. And different or striking decor is sometimes applied to chill out areas just for fun.</p>
<div class="unnec"></div>
<h2>Swags:</h2>
<p>Swags are available in a wide range of colours to suit the overall look of your event.</p>
<div class="textPhotos"><img src="/images/pages/decoration/swags.jpg" alt="Decorative swags come in a wide range of colours" width="190" height="225"  loading="lazy" />
</div>
<p >The photo alongside shows a selection. Ivory for a pretty wedding, red to warm up a winter party, grey for an elegant corporate do and so on.</p>
<p >We can match pelmet colours as well if required.</p>
<h2>Flowers, balloons, theming, corporate branding:</h2>
<div class="textPhotos">
<a href="/photos/waiting.htm" target="_blank" rel="noopener noreferrer"><img src="/images/pages/decoration/leafy.jpg" alt="Leafy decoration" width="198" height="150" loading="lazy" /></a>
<p class="view">Light and leafy<br/>
<a href="https://www.countymarquees.com/photos/waiting.htm">View in gallery</a></p>
<a href="/photos/fresh.htm"><img src="/images/pages/decoration/bunting.jpg" alt="Fashionable light marquee with bunting" width="198" height="150" class="second" loading="lazy" /></a>
<p class="view">Light and pretty with bunting<br/>
<a href="/photos/fresh.htm">View in gallery</a></p>
<a href="https://www.facebook.com/pg/countymarquees/photos/?tab=album&album_id=10155950974467239&__xts__%5B0%5D=68.ARDDRgs45brzDuzL3hS9Cv4-CuRgW_-xDrKzBrSj3V5EujBiHhZO8XG_pqeRJkdr7KeAr1btG1msu09PUjoP297rYo1kqvBTUYPGTnuHl2FhLsPaeneCRqiN8sTDR6VghH7MpYa7UIAQVI9m6kM55iTFeBbbiwZt2z2xTDyN1pMz4tu2KkK0pFA&__tn__=-UC-R"  target="_blank" rel="noopener noreferrer"><img src="/images/pages/decoration/fete.jpg" alt="Rustic style marquee" width="198" height="150" class="second" loading="lazy" /></a>
<p class="view">Rustic style<br/>
<a href="https://www.facebook.com/pg/countymarquees/photos/?tab=album&album_id=10155950974467239&__xts__%5B0%5D=68.ARDDRgs45brzDuzL3hS9Cv4-CuRgW_-xDrKzBrSj3V5EujBiHhZO8XG_pqeRJkdr7KeAr1btG1msu09PUjoP297rYo1kqvBTUYPGTnuHl2FhLsPaeneCRqiN8sTDR6VghH7MpYa7UIAQVI9m6kM55iTFeBbbiwZt2z2xTDyN1pMz4tu2KkK0pFA&__tn__=-UC-R" target="_blank" rel="noopener noreferrer">View facebook album</a></p>
<img src="/images/pages/decoration/indian_style.jpg" alt="Colourful Indian style" width="198" height="150" class="second" loading="lazy" /></a>
<p class="view">Colourful Indian style<br/>
<a href="/case_studies/colourful.htm">View case study</a></p>
</div>
<p>Where to start? For further marquee decoration ideas, see:</p>
<ul><li>the <a href="/photos.htm?category=decoration">marquee 
decoration</a> section of our photo gallery</li>
<li>or some real life <a href="/real.htm">marquee case studies</a></li>
<li>we also have more photos to look at on our <a href="https://www.instagram.com/countymarquees/" target="_blank"  rel="noopener noreferrer">Instagram</a>, <a href="https://www.facebook.com/countymarquees/" target="_blank"  rel="noopener noreferrer">Facebook</a>, <a href="https://pinterest.com/county_marquees/" rel="noopener noreferrer" target="_blank" >Pinterest</a> and <a href="https://www.flickr.com/photos/county_marquees/" target="_blank" rel="noopener noreferrer" >Flickr</a> pages.</li></ul>
<p>Here are a few <strong>other guides</strong> from around the web to set your creative juices flowing:</p>
<ul>
<li>Marquee wedding archive from blog <a href="https://www.rockmywedding.co.uk/search?query=marquee" target="_blank" rel="noopener no referrer">Rock My Wedding</a></li>
<li><a href="https://whimsicalwonderlandweddings.com/marquee-wedding-ideas/" target="_blank" >Whimsical Wonderland Weddings</a>'s marquee decoration ideas.</li>
<li><a href="http://www.jayandievents.com/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/decoration/jayandiservices.com']);" >Jay &amp; I Services</a> offer fantastic Indian inspired marquee decor ideas including mandap decoration.</li>
</ul>
<p>Although County Marquees does not stock theme items, flowers etc, we are happy to take on the task of organizing supply, and then integrating  them with all the other elements of marquee design to create the look and feel you require.</p>
<div class="unnec"></div>
<p class="endCall">Or please <a href="../contact_us.htm">contact us</a> for advice on your particular requirements.</p>
</div>