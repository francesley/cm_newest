<?php

$content = <<<EOQ
<div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/olivia_bryn/marquee_big.jpg" title="Fresh, simple and pretty, with open sides and leaves attached to wires  for ceiling decoration" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/marquee.jpg" alt="Fresh pretty wedding tent with marquee ceiling decoration" width="273" height="186" /></a><p>Fresh, simple and pretty, with open sides and leaves attached to wires for ceiling decoration</p></div>
<div class="case rightcase"><a href="/images/case/olivia_bryn/full_big.jpg" title="Tent in action. The space is divided between dining and dancing areas" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/full.jpg" alt="Tent with dining and dancing areas" width="273" height="186" /></a><p>Tent in action. The space is divided between dining and dancing areas</p></div>

<div class="case"><a href="/images/case/olivia_bryn/outside_big.jpg" title="Beautiful by day..." class="fancybox" rel="group"><img src="/images/case/olivia_bryn/outside.jpg" alt="Wedding tent in beautiful setting during the day" width="273" height="186" /></a><p>Beautiful by day...</p></div>
<div class="case rightcase"><a href="/images/case/olivia_bryn/exterior_dusk_big.jpg" title="...and by night" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/exterior_dusk.jpg" alt="Wedding tent in beautiful setting at night" width="273" height="186" /></a><p>...and by night</p></div>
<div class="case"><a href="/images/case/olivia_bryn/dancing_big.jpg" title="Olivia and Bryn opening the dancing, with eyes only for each other!" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/dancing.jpg" alt="Opening the dancing" width="273" height="186" loading-"lazy" /></a><p>Olivia and Bryn opening the dancing, with eyes only for each other!</p></div>
<div class="case rightcase"><a href="/images/case/olivia_bryn/cake_big.jpg" title="Cutting the cake" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/cake.jpg" alt="Cutting the cake" width="273" height="186" loading-"lazy" /></a><p>Cutting the <em>naked</em> wedding cake</p></div>
<div class="case"><a href="/images/case/olivia_bryn/leaping_big.jpg" title="Chinese hat tent and leaping girl" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/leaping.jpg" alt="Chinese hat tent and leaping girl" width="273" height="186" loading-"lazy" /></a><p>A Chinese hat tent in case of rain. And a very athletic girl, caught just before she turned upside down!</p></div>
<div class="case rightcase"><a href="/images/case/olivia_bryn/table_name_big.jpg" title="Hand-painted personalised table names &ndash; Julie Andrews thrown in for free" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/table_name.jpg" alt="Hand-painted table names" width="273" height="186" loading="lazy" /></a><p>Hand-painted personalised table names &ndash; Julie Andrews thrown in for free</p></div>
<div class="case"><a href="/images/case/olivia_bryn/flowers_big.jpg" title="Pretty, informal flowers" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/flowers.jpg" alt="Pretty, informal flowers" width="273" height="186" loading="lazy" /></a><p>Pretty, informal flowers</p></div>
<div class="case rightcase"><a href="/images/case/olivia_bryn/toast_big.jpg" title="Toasting" class="fancybox" rel="group"><img src="/images/case/olivia_bryn/toast.jpg" alt="Toasting" width="273" height="186"  loading="lazy" /></a><p>Toasting</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was Olivia and Bryn&#8217;s gorgeous summer wedding reception.</p>
<p class="wide"><strong>The venue</strong> was Busbridge Lakes, a very beautiful marquee venue, now sadly closed, near Godalming in Surrey.</p>
<p class="wide"><strong>Olivia&#8217;s thinking:</strong> &ldquo;<em>Even though we got married in July, you can never trust the British weather so we decided to get a marquee.</p>
<p class="wide">From the start, County Marquees were very helpful providing us with a site visit and even a car-lift to the station afterwards! We wanted to have as much of the beautiful venue visible from inside the marquee as possible and we were delighted with our decision to have a clear gable at one end. They kindly communicated and worked with our florist to put up wires for our marquee foliage and provided a Chinese hat so our guests could be sheltered from any potential rain if they went outside.</p>
<p class="wide">At no point were we pushed or pressured into buying extras or spending more; in fact County Marquees were refreshingly discouraging of spending more than we needed to (which makes a pleasant change from the world of wedding retailers!).</p>
<p class="wide">The end result was better than we could have even imagined. They gave us an absolutely breathtaking and yet relaxed setting in which to dine, boogie and celebrate all the love!</em>&rdquo;</p>
<p class="wide abovecase">Photos courtesy of <a href="http://www.fionasweddingphotography.co.uk/" target="_blank" onclick="_gaq.push(['_trackPageview', '/outgoing/case/fiona_kelly_leafy'])">Fiona Kelly Photography</a></p>


<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about celebrating a wedding in a marquee.</p>
</div>
EOQ;
?>