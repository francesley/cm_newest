<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>

<div class="case"><a href="/images/case/jewish_wedding/chuppah_big.jpg" title="Small garden marquee laid out for Jewish wedding ceremony and reception" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/chuppah.jpg" alt="Marquee laid out for Jewish wedding ceremony and reception" width="273" height="186" /></a>
<p>Marquee laid out for Jewish wedding ceremony and reception</p></div>
<div class="case rightcase"><a href="/images/case/jewish_wedding/bride_dad_big.jpg" title="Bride and father-of-the-bride walk up the aisle" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/bride_dad.jpg" alt="Bride and father-of-the-bride walk up the aisle" width="273" height="186" /></a><p>Bride and father-of-the-bride walk up the aisle</p></div>

<div class="case"><a href="/images/case/jewish_wedding/bride_entrance_big.jpg" title="The bride led by her father approaches the groom who is waiting under a <em>chupah</em>, the traditional Jewish wedding canopy" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/bride_entrance.jpg" alt="The bride led by her father approaches the groom who is waiting under a the traditional Jewish wedding canopy" width="273" height="186" /></a><p>The bride led by her father approaches the groom who is waiting under a <em>chupah</em>, the traditional Jewish wedding canopy</p></div>
<div class="case rightcase"><a href="/images/case/jewish_wedding/at_chuppag_big.jpg" title="Don't want to be too soppy but &ndash; the look of love" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/at_chuppa.jpg" alt="The look of love" width="273" height="186" /></a><p>Don't want to be too soppy but &ndash; the look of love...</p></div>
<div class="case"><a href="/images/case/jewish_wedding/audience_big.jpg" title="Seated audience watching intently" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/audience.jpg" alt=Audience watching intently" width="273" height="186" loading="lazy" /></a><p>Seated audience watching intently</p></div>
<div class="case rightcase"><a href="/images/case/jewish_wedding/breaking_glass_big.jpg" title="Smashing the glass as per Jewish tradition" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/breaking_glass.jpg" alt="Smashing the glass as per Jewish tradition" width="273" height="186" loading="lazy" /></a><p>Smashing the glass as per Jewish tradition</p></div>

<div class="case"><a href="/images/case/jewish_wedding/married_big.jpg" title="Triumphantly married!" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/married.jpg" alt="Triumphantly married!" width="273" height="186" loading="lazy" /></a><p>Triumphantly married!</p></div>
<div class="case rightcase"><a href="/images/case/jewish_wedding/reception_big.jpg" title="The reception" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/reception.jpg" alt="Pretty wedding reception in a small marquee" width="273" height="186" loading="lazy" /></a><p>The reception</p></div>

<div class="case"><a href="/images/case/jewish_wedding/bride_chair_big.jpg" title="Bride carried on a chair as per Jewish tradition" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/bride_chair.jpg" alt="Bride carried on a chair" width="273" height="186" loading="lazy" /></a><p>Bride carried on a chair as per Jewish tradition</p></div>
<div class="case rightcase"><a href="/images/case/jewish_wedding/groom_chair_big.jpg" title="Groom thrown in the air as per Jewish tradition" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/groom_chair.jpg" alt="Groom thrown in the air" width="273" height="186" loading="lazy" /></a><p>Groom thrown in the air as per Jewish tradition</div>

<div class="case bottomcase"><a href="/images/case/jewish_wedding/dancing_big.jpg" title="First dance together</p></div>
<hr>" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/dancing.jpg" alt="First dance together as husband and wife" width="273" height="186" loading="lazy" /></a><p>First dance</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/jewish_wedding/happy_big.jpg" title="Yes, we really are married" class="fancybox" rel="group"><img src="/images/case/jewish_wedding/happy.jpg" alt="Bride and groom exchange happy looks" width="273" height="186" loading="lazy" /></a><p>Yes, we really are married!.</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a Jewish wedding and a wedding reception.</p>
<p class="wide"><strong>The venue:</strong> was a small and pretty garden. The size did limit the guest list, but there was still enough room in the 6m (20') by 12m (30') frame marquee for a lovely wedding with a beautifully decorated <em>chuppah</em> &ndash; Jewish wedding canopy &ndash; followed by a warm and pretty reception with dancing.</p>
<p class="wide abovecase">Photos courtesy of <a href="https://www.stephanieandnicole.com/" target="_blank"  onclick="_gaq.push(['_trackPageview', '/outgoing/case/steph_nicole'])">Stephanie & Nicole</a>.</p>
<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about wedding marquee hire.</p>
</div>
EOQ;
?>