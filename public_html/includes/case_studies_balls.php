<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/ball/arial_big.jpg" title="Aerial view of the large main marquee" class="fancybox" rel="group"><img src="/images/case/ball/arial.jpg" alt="Aerial view of the large main marquee" width="273" height="186" /></a><p>Aerial view of the main large 15m x 21m frame marquee</p></div>
<div class="case rightcase"><a href="/images/case/ball/dancing_big.jpg" title="Inside the dancing marquee with exciting lighting" class="fancybox" rel="group"><img src="/images/case/ball/dancing.jpg" alt="Inside the dancing marquee" width="273" height="186" /></a><p>Dancing inside the packed main marquee with exciting lighting</p></div>
<div class="case"><a href="/images/case/ball/quiet_marquee_big.jpg" title="Quiet marquee, music and drinks and a starcloth lining for some oomph" class="fancybox" rel="group"><img src="/images/case/ball/quiet_marquee.jpg" alt="Quiet marquee" width="273" height="186" /></a><p>The quiet marquee, music and drinks and a starcloth lining for some oomph</p></div>
<div class="case rightcase"><a href="/images/case/ball/quad_big.jpg" title="Milling around in the quad" class="fancybox" rel="group"><img src="/images/case/ball/quad.jpg" alt="Milling around in the quad" width="273" height="186" /></a><p>Milling around in the quad in front of the main large marquee</p></div>
<div class="case"><a href="/images/case/ball/freddy_brian_big.jpg" title="Freddy Mercury and Queen tribute band" class="fancybox" rel="group"><img src="/images/case/ball/queen_tribute.jpg" alt="Freddy Mercury and Queen tribute band" width="273" height="186" loading="lazy" /></a><p>A charismatic <em>Freddy Mercury</em> and <em>Brian May</em> (behind the pole) from the Queen tribute band</p></div>
<div class="case rightcase"><a href="/images/case/ball/chute_big.jpg" title="An unusual exit" class="fancybox" rel="group"><img src="/images/case/ball/chute.jpg" alt="An unusual exit" width="273" height="186" loading="lazy" /></a><p>An unusual means of escape from the noise of the large marquee</p></div>
<div class="case"><a href="/images/case/ball/entrance_big.jpg" title="A chinese hat marquee bar at an entrance creates a pretty spectacle" class="fancybox" rel="group"><img src="/images/case/ball/entrance.jpg" alt="Chinese hat bar at entrance" width="273" height="186" loading="lazy" /></a><p>A chinese hat marquee bar at an entrance creates a pretty spectacle.</p></div>
<div class="case rightcase"><a href="/images/case/ball/bar_two_big.jpg" title="One of several marquee bars" class="fancybox" rel="group"><img src="/images/case/ball/bar_two.jpg" alt="Marquee bar" width="273" height="186" loading="lazy" /></a><p>Quite a few bars were needed, which explains the large number of small marquees</p></div>
<div class="case bottomcase"><a href="/images/case/ball/computer_games_big.jpg" title="Entertainment marquee with video games" class="fancybox" rel="group"><img src="/images/case/ball/computer_games.jpg" alt="Entertainment marquee with video games" width="273" height="186" loading="lazy" /></a><p>Small marquees were also used for entertainment and later on...</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/ball/chillin_big.jpg" title="Chilling by a small marquee" class="fancybox" rel="group"><img src="/images/case/ball/chillin.jpg" alt="Chilling by a small marquee" width="273" height="186" loading="lazy" /></a><p>... as a focus for relaxing away from all the bustle</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a summer ball at a Cambridge University college.</p>
<p class="wide"><strong>The challenge</strong> was to set up multiple marquees for various functions (dancing, performance, bars...), while at the same time:</p>
<ul class="wide long"><li>taking account of different <em>stakeholders</em>, each with their own plans for using the space</li>
<li>Making a minimal impact on college life</li>
<li>Set up just before, and take down and collect soon after the event to enable a reception to take place in the space</li>
</ul>
<p class="wide"><strong>The solution:</strong> Lots of flexible frame marquees of various sizes: 15m x 21m, 9m x 12m, three 3m x 6m and four 3m x 3m. Also a 3m x 3m Chinese Hat marquee. All were set up speedily and <strong>silently</strong> and to the tight timetable the college needed. Here is some feedback to that effect from the President of the Ball Committee:</p>
<blockquote>
<p class="wide"><em>&#8220;The marquees ... looked good ... and were erected incredibly quickly and efficiently with no disturbance to college life. ... As a committee we also strongly believe that County Marquees must be one of the best value for money companies out there because we had such a tight budget but managed to get everything we wanted in the end!&#8221;</em></p>
</blockquote>
<p class="wide abovecase">You can read <a href="http://www.countymarquees.com/company/testimonials.htm#ball">more of the comittee's feedback</a> on our testimonials page.</p>
<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about ball marquee hire.</p>
</div>
EOQ;
?>