<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/dermalogica/marquee_exterior_big.jpg" title="The flat lawn was ideal for siting a marquee" class="fancybox" rel="group"><img src="/images/case/dermalogica/marquee_exterior.jpg" alt="Outside the marquee" width="273" height="186" loading="lazy"/></a><p>The flat lawn was ideal for siting a marquee</p></div>
<div class="case rightcase"><a href="/images/case/dermalogica/empty_big.jpg" title="Sparkly Christmas atmosphere" class="fancybox" rel="group"><img src="/images/case/dermalogica/empty.jpg" alt="Sparkly Christmas atmosphere" width="273" height="186"  loading="lazy"/></a><p>Just before the ball, very sparkly and festive</p><p><a href="/photos/christmas.htm" class="view">View in gallery</a></p></div>
<div class="case"><a href="/images/case/dermalogica/wrong_way_big.jpg" title="Eating dinner in the Xmas party marquee" class="fancybox" rel="group"><img src="/images/case/dermalogica/wrong_way.jpg" alt="Eating dinner in the Xmas party marquee" width="273" height="186" loading="lazy"/></a><p>Ball attendees sitting and eating early on in the evening</p></div>
<div class="case rightcase"><a href="/images/case/dermalogica/centrepiece_big.jpg" title="Christmas style centrepiece" class="fancybox" rel="group"><img src="/images/case/dermalogica/centrepiece.jpg" alt="Christmas style centrepiece" width="273" height="186" loading="lazy" /></a><p>Unusual twirled golden table centrepieces with hanging baubles were a key feature of the Christmas feel to the marquee</p></div>	
<div class="case bottomcase"><a href="/images/case/dermalogica/screen2_big.jpg" title="The company created an app to put photos taken at the table to on a large screen" class="fancybox" rel="group"><img src="/images/case/dermalogica/screen2.jpg" alt="Screen" width="273" height="186" loading="lazy" /></a><p>The company created an app for the party that allowed photos taken at the table to be immediately shown on a large screen</p>
</div>
<div class="case rightcase bottomcase"><a href="/images/case/dermalogica/before_big.jpg" title="Unlit and undecorated, the marquee looks nice but lacks oomph" class="fancybox" rel="group"><img src="/images/case/dermalogica/before.jpg" alt="Before the ball" width="273" height="186" loading="lazy" /></a><p>Unlit and undecorated, the marquee looks nice but lacks oomph</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a Christmas ball at skin care company <a href="https://www.dermalogica.co.uk/" target="_blank">Dermalogica</a></p>
<p class="wide"><strong>The venue</strong> was... pretty straightforward if we are honest. The nice flat lawn outside Dermalogica's office is an ideal setting for a Christmas party marquee.</p>
<p class="wide abovecase"><strong>The marquee</strong> was a 12m x 33m (40' x 110') frame structure that seated 220 for the ball. The marquee was up for a week, and Dermalogica was able to take advantage of it for other meetings and presentations.</p>

<p class="endCall">Please <a href="/contact_us.htm">contact us</a> if you would like to find out about a Christmas marquee party.</p>
</div>
EOQ;
?>