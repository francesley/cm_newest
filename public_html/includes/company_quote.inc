         
		 <p class="wide">Sorry but our marquee quote tool is under repair. It normally generates an immediate price estimate.</p>
		
	      <p class="wide">We are working hard to get the quote tool back online. In the meantime, you can <a href="/pricing/quotation.htm">request an accurate obligation free quotation</a> personalised for your event. Or if you prefer, you can layout the marquee you want on our <a href="/movies/planner.html">marquee planner</a> and then request a quote from your plan.</p>
</div>