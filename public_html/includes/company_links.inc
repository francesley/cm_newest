<div class="links">
<h3>Catering</h3>
<ul class="long">
<li><a href="https://www.archiesfood.com/" target="_blank" rel="noopener noreferrer">www.archiesfood.com</a></li>
<li><a href="http://www.clareskitchen.co.uk/" target="_blank" rel="noopener noreferrer">www.clareskitchen.co.uk</a></li>
<li><a href="https://www.cookdlondon.com/" target="_blank" rel="noopener noreferrer">www.cookdlondon.com</a></li>
<li><a href="http://www.patacakepatacake.com/" target="_blank" rel="noopener noreferrer">www.patacakepatacake.com</a></li>
<li><a href="https://www.peapodandco.com/" target="_blank" rel="noopener noreferrer">www.peapodandco.com</a></li>
</ul>
<h3 style="line-height:1.5em">Photographers and Videographers</h3>
<ul class="long">
<li><a href="https://www.andymacphotography.com/" target="_blank" rel="noopener noreferrer">www.andymacphotography.com</a></li>
<li><a href="https://fionasweddingphotography.co.uk/" target="_blank"  rel="noopener noreferrer">fionasweddingphotography.co.uk</a></li>
<li><a href="https://www.jenksandco.tv/" target="_blank" rel="noopener noreferrer">www.jenksandco.tv</a></li>
</ul>
<h3>Entertainment</h3>
<ul class="long">
<li><a href="https://www.feedingthefish.com/" target="_blank" rel="noopener noreferrer">www.feedingthefish.com</a></li>
<li><a href="https://www.alivenetwork.com/" target="_blank" rel="noopener noreferrer">www.alivenetwork.com</a></li>
<li><a href="https://hitsquaddjsandentertainment.co.uk/" target="_blank" rel="noopener noreferrer">hitsquaddjsandentertainment.co.uk</a></li>
<li><a href="https://tropical-ents.co.uk/" target="_blank" rel="noopener noreferrer">www.tropical-ents.co.uk</a></li>
</ul>
<h3>Honeymoon Destinations</h3>
<ul class="long">
<li><a href="https://www.amazontupana.com/" target="_blank" rel="noopener noreferrer">amazontupana.com</a></li>
<li><a href="https://www.kwando.co.bw/" target="_blank" rel="noopener noreferrer">www.kwando.com</a></li>
<li><a href="https://mihingo-lodge.com/" target="_blank" rel="noopener noreferrer">mihingo-lodge.com</a></li>
<li><a href="https://safari.co.uk/" target="_blank" rel="noopener noreferrer">www.safari.co.uk</a></li>
<li><a href=https://www.sabisandslodges.com/kruger-park-lodges/thornybush-nkaya-lodge/" target="_blank" rel="noopener noreferrer">nkaya-game-lodge</a></li>
</ul>
</div>
<div class="links rightlinks">
<h3>Event and Wedding Advice</h3>
<ul class="long">
<li><a href="https://english-wedding.com/"  target="_blank"  rel="noopener noreferrer">www.english-wedding.com</a></li>
<li><a href="https://www.boho-weddings.com/" target="_blank"  rel="noopener noreferrer">www.boho-weddings.com</a></li>
<li><a href="https://www.rockmywedding.co.uk/" target="_blank"  rel="noopener noreferrer">www.rockmywedding.co.uk</a></li>
<li><a href="https://www.rocknrollbride.com/" target="_blank"  rel="noopener noreferrer">www.rocknrollbride.com</a></li>
</ul>
<h3>Wedding &amp; Party Accessories </h3>
<ul class="long">
<li><a href="https://www.hanginglanterns.co.uk/" title="Hanging lanterns"  target="_blank" rel="noopener noreferrer">www.hanginglanterns.co.uk</a></li>
<li><a href="https://www.twilight-trees.com/" title="Trees to hire" target="_blank" rel="noopener noreferrer">www.twilight-trees.com</a></li>
<li><a href="https://www.chillerbox.com/" title="Mobile fridge trailers"  target="_blank" rel="noopener noreferrer">www.chillerbox.com</a></li>
</ul>
<h3>Marquee Venues</h3>
<ul class="long">
<li><a href="https://www.blankcanvasweddings.co.uk/" target="_blank" rel="noopener noreferrer">www.blankcanvasweddings.co.uk</a></li>
</ul>
<h3>Wedding Lingerie </h3>
<ul class="long">
<li><a href="https://www.lingerie.co.uk/" target="_blank" rel="noopener noreferrer">www.delicates.co.uk</a></li>
</ul>
<h3>Wedding Car Hire</h3>
<ul class="long">
<li><a href="https://www.wedding-car.co.uk/" target="_blank"  rel="noopener noreferrer">www.wedding-car.co.uk</a></li>
</ul>
</div>
</div>