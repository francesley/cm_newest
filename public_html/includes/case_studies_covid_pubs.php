<?php
$content = <<<EOQ
<p class="wide">For pubs and bars, adaptable frame marquees can provide extra space, well ventilated drinking and dining areas that are protected from the elements. County Marquees has supplied temporary tents that serve as nicely furnished restaurants as well as plainer structures large and small that make outdoor drinking possible.</p>
<p class="wide">Below are examples of some of the temporary structures County Marquees has installed to accomodate safer Covid-19 hospitality in pubs and bars.</p>
<p class="wide"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about a marquee to enable social distancing in pubs.</p>
<h2>Drinking and dining marquee on an outdoor terrace:</h2>
<div class="case-wrap">
<div class="case bottomcase"> <a href="/images/case/covid_hospitality/cambridge_big.jpg" title="A frame tent slots neatly onto a pub's outdoor terrace" class="fancybox" rel="group"><img src="/images/case/covid_hospitality/cambridge.jpg" alt="Frame tent on an outdoor terrace" width="400" height="300" /></a><p>A frame tent slots neatly onto a pub's outdoor terrace</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/covid_hospitality/cambridge_interior_big.jpg" title="Inside, the marquee hosts a pretty, socially distanced restaurant" class="fancybox" rel="group"><img src="/images/case/covid_hospitality/cambridge_interior.jpg" alt="Socially distanced restaurant inside a frame tent" width="400" height="300" /></a><p>Inside, the marquee hosts a pretty, socially distanced restaurant</p> </div>
<h2>Socially distanced bar:</h2>
<div class="case bottomcase"> <a href="/images/case/covid_hospitality/bar_big.jpg" title="An open sided marquee provides cover in a bar garden at a golf course" class="fancybox" rel="group"><img src="/images/case/covid_hospitality/bar.jpg" alt="A marquee provides cover in a bar garden at a golf course" width="400" height="300" loading="lazy" /></a><p>An open sided marquee provides cover in a bar garden at a golf course, enabling guests to drink even in the rain</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/covid_hospitality/bar2_big.jpg" title="The open marquee presents a welcoming spectacle" class="fancybox" rel="group"><img src="/images/case/covid_hospitality/bar2.jpg" alt="Looking back at the bar's tent cover" width="400" height="300" loading="lazy" /></a><p>Open to the elements, the tent allows air to circulate freely</p> </div>
<h2>Simple rain cover:</h2>
<div class="case bottomcase"> <a href="/images/case/covid_hospitality/pub_big.jpg" title="Pointy topped Chinese hat tents provide  well ventilated rain cover" class="fancybox" rel="group"><img src="/images/case/covid_hospitality/pub.jpg" alt="Pointy topped Chinese hat tents outside a pub" width="400" height="300" loading="lazy" /></a><p>Pointy topped Chinese hat tents provide well ventilated rain cover</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/covid_hospitality/white_horse_big.jpg" title="Customers congregate outside" class="fancybox" rel="group"><img src="/images/case/covid_hospitality/white_horse.jpg" alt="Customers congregating outside the White Horse pub" width="400" height="300" loading="lazy" /></a><p>Customers congregate outside</p> </div>
</div>
</div>
EOQ;
?>