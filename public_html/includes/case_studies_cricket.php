<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/cricket/cricket_view_big.jpg" title="Cricket themed marquee with scoreboard dominating" class="fancybox" rel="group"><img src="/images/case/cricket/cricket_view.jpg" alt="Cricket themed marquee with scoreboard dominating" width="273" height="186" /></a><p>The cricket scoreboard clearly sets the theme for the event.<br/><a href="/photos/cricket.htm">View in photo gallery</a></p></div>
<div class="case rightcase"><a href="/images/case/cricket/cricket_cheese2_big.jpg" title="The cheesestand, central element of the marquee theme" class="fancybox" rel="group"><img src="/images/case/cricket/cricket_cheese2.jpg" alt="The cheesestand, central element of the marquee theme" width="273" height="186" /></a><p>The cheesestand, a central decorative feature, created from stumps, bailes and cricket ball</p></div>
<div class="case"><a href="/images/case/cricket/cricket_details_big.jpg" title="Tables were named after cricket positions: here Bowler, Batsman and First slip" class="fancybox" rel="group"><img src="/images/case/cricket/cricket_details.jpg" alt="Marquee theme details" width="273" height="186" loading="lazy" /></a><p>Small details carried through the theme. Tables were named after cricket positions: here Bowler, Batsman and First slip.</p></div>
<div class="case rightcase"><a href="/images/case/cricket/cricket_marquee_big.jpg" title="From the outside, showing the marquee attached to the house and the catering tent alongside" class="fancybox" rel="group"><img src="/images/case/cricket/cricket_marquee.jpg" alt="Sports themed marquee from the outside" width="273" height="186" loading="lazy" /></a><p>From the outside, showing the marquee attached to the house and the catering tent alongside.</p></div>	
<div class="case bottomcase"><a href="/images/case/cricket/cricket_notout_big.jpg" title="Fifty not out. May the innings continue for many more!" class="fancybox" rel="group"><img src="/images/case/cricket/cricket_notout.jpg" alt="Themed table place cards" width="273" height="186" loading="lazy" /></a><p>Fifty not out. May the innings continue for many more!</p></div>
<div class="case rightcase bottomcase"><a href="/images/case/cricket/cricket_view2_big.jpg" title="Viewing the marquee from the house. Guests could proceed straight to the event" class="fancybox" rel="group"><img src="/images/case/cricket/cricket_view2.jpg" alt="Full view of cricket themed marquee" width="273" height="186" loading="lazy" /></a><p>Viewing the marquee from the house. Guests could proceed straight to the event.</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a 50th wedding anniversary.</p>
<p class="wide"><strong>The venue</strong> was a fair sized garden in West Sussex. The 40' by 80' frame marquee was attached to the house. Another 30' by 30' frame marquee was alongside, partitioned to serve both as a catering tent and a food service area.</p>
<p class="wide"><strong>The theme</strong> was cricket. The couple in question were both long time cricket fans and had originally met at a cricket match. Cricket wasn't merely a passion but also the source of romantic memories.</p>
<p class="wide abovecase">What is notable about this marquee is that the look was achieved <em>affordably</em>. Table names, a scoreboard, a cheese stand ingeniously contrived out of cricketing items... showing how, with thought and planning, it is possible to achieve a fun and meaningful theme on a tight budget.</p>

<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about holding an anniversary party in a marquee.</p>
</div>
EOQ;
?>