<p>Since 1989, County Marquees has been providing professional marquees, decorations, and lighting to the Kent area, including its county town of Maidstone.</p>
<div class="textPhotos">
<img src="/images/pages/areas/kent_winter.jpg" alt="A Kentish winter marquee interior width="198" height="150"/>
<p>Winter marquee interior</p>
<img src="/images/pages/areas/kent.jpg" alt="Traditional style tent" width="198" height="150"/>
<p>Traditional style tent</p>
</div>
<p>Maidstone is rich with history, and has evidence of settlement from before the Stone Age. The town is close to Leeds Castle, a remarkable historic property which has served as a Norman Stronghold, as the private property of six of England's medieval queens, as a palace used by Henry VIII and Catherine of Aragon, a Jacobean country house, a Georgian mansion, and an elegant early 20th century retreat.</p>
<p>You can even have a piece of its history for yourself, as this venue is hireable! We're here to help in that situation with well maintained marquees of all kinds, as well as a large selection of furniture, lighting and other equipment suitable for all types of occasions. Our <a href="/pricing/marquees.htm">prices</a> are reasonable and you can obtain an immediate no obligation quote from our <a href="/pricing/quote.htm">online calculator</a>.</p>
<p> We do our very best to ensure that customers are completely satisfied with their choice of our company. Our brilliant team are constantly being <a href="/company/testimonials.htm">complimented</a> on their friendliness, efficiency, and readiness to go the extra mile for  customers, so please do <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous clear marquee</h2>
<img src="/images/areas/clear_marquee.jpg" alt="Clear marquee in Tunbridge Wells, Kent" width="581" height="410"/>
<p class="wide">A ravishing clear marquee with dramatic lighting in Tunbridge Wells, Kent.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">The marquee looked pristine and fabulous</span>
<abbr title="2017-11" class="dtreviewed hide">November 2017</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Maidstone</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I must say that the event went very well and the marquee greatly exceeded our expectations. Quite honestly I don't see how it could have been any better.</p>
<p>Numerous people mentioned how good it looked and that it didn't look like an attached marquee at all but more like an extension of the house.</p>
<p>And the guys who put it up were also excellent. Their focus on making sure we were happy was excellent plus they fitted the floor exactly to the angled step which saved me work! Please pass on my thanks once again to them.</p>
<p>I would have no hesitation at all in using you again or recommending you which is the highest praise that I can give someone.
Many, many thanks.</p>
</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recomendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Maidstone area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39946.03322901837!2d0.4850587194751784!3d51.2627753263946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47ded602f8347067%3A0xa6f562520cb400cd!2sMaidstone!5e0!3m2!1sen!2suk!4v1544373872226" width="579" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>