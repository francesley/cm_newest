<p>County Marquees have been serving the lovely old coastal town of Eastbourne for over 25 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Wedding tent with open sides" width="198" height="150"/>
<p>Wedding tent with open sides</p>
<img src="/images/pages/areas/buckhurst_park.jpg" alt="Traditional marquee at Buckhurst Park" width="198" height="150"/>
<p>Traditional marquee at Buckhurst Park, East Sussex wedding venue</p>
</div>
<p>We stock a <a href="/equipment/marquees.htm">variety of marquees</a> that are suitable for all sort of occasions, formal and informal, large and small. Our tents are also modular which allows us to extend space indefinitely. Traditional style marquees are well suited to larger festival type events, while framed marquees can be adapted to accommodate events in narrow, awkward spaces with intrusive shrubbery. We also stock stretch tents, bell tents, Chinese Hat tents  plus a good range of furniture, lighting and other event equipment.</p>
<p>We offer a free site visit where our friendly Sussex representative will survey your site discuss with you how best to turn your ideas into reality.</p>
<p>Have a look through our <a href="/photos.htm">gallery</a> to give you some examples of the marquees we have installed in the past and to also give you an inkling of what is possible.</p>
<p>Our wonderful team is regularly <a href="/company/testimonials.htm">complimented</a> on their good nature, tidiness and fantastic results.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more or to arrange a site visit.</p>
<h2>Wedding marquee on the Sussex coast</h2>
<img src="/images/areas/saltdean.jpg" alt="Corner of a flowery wedding marquee" width="581" loading = "lazy"/>
<p class="wide">Corner of a traiditionally styled flowery wedding marquee in Saltdean with coconut matting on the floor and Georgian windows.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Exactly what we wanted</span>
<abbr title="2012-08" class="dtreviewed hide">August 2012</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday girl</span><span class="adr"><span class="locality">Heathfield</span><span class="region">East Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Just to thank all at County Marquees for the lovely job you did for us last weekend. Your teams were friendly, obliging and efficient and the marquee couldn't have been a nicer one, it was immaculate and exactly what we wanted. We were obviously right to choose the company that brought their dogs on the job!</p>
</blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Sussex</h3>
<p class="venues">We maintain a list of marquee venues in <a href="/venues/eSussex.htm">East</a> and <a href="/venues/wSussex.htm">West Sussex</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Eastbourne area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d80724.50919021342!2d0.21827695835200117!3d50.78222982979685!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df70ac77af480d%3A0xf767a9a392d8f605!2sEastbourne!5e0!3m2!1sen!2suk!4v1543937395409" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>