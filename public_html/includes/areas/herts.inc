<p>County Marquees has been supplying Marquees across the beautiful County of Hertfordshire for over 25 years.  We pride ourselves on providing all of our customers with a service that is second to none, both practical and personal.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Vintage trestle tables</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style for a reception</p>
</div>
<p>Marquees provide the perfect setting for many different occasions and give you the flexibility to create a venue reflecting your style and taste. Whether you are hosting a wedding on a huge lawn, a significant birthday in the back garden, a smart corporate event or family barbecue, we have it all covered. We offer a wide range of <a href="/equipment/marquees.htm">marquee styles</a>, a choice of flooring options, lots of furniture and a host of different lighting accessories for you to choose from.  Our marquee range includes flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pointy topped Chinese Hat marquees.</p>
<p>Our <a href="/company/testimonials.htm">customer testimonials</a> showcase the good nature and professionalism of our excellent team. View our <a href="/photos.htm">gallery</a> to get an idea of the high quality of our work.</p>
<p>We offer a no-obligation free site visit where our Hertfordshire representative will measure your space and discuss with you how best to turn your ideas into reality. For pricing information, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">rough estimate</a> based on your requirements.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Beautiful Hertfordshire wedding marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style wedding marquee" width="581" loading = "lazy"/>
<p class="wide">A traditional style marquee nestling in trees in a large garden</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic marquee</span>
<abbr title="2015-03" class="dtreviewed hide">March 2015</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Husband and father</span><span class="adr"><span class="locality">Wheathamstead</span><span class="region">Hertfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Many thanks again for a fantastic marquee and a fantastic team who put it up and took it down effortlessly. Both parties were brilliant (Christian's 18th maybe a bit livelier than Caroline's 50th!) and you did us proud once again.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in Hertfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover Hertfordshire</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d315537.8350880169!2d-0.5552883630462817!3d51.84015098696809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d878a7ec6d05d9%3A0x43c45ff02434f1ee!2sHertfordshire!5e0!3m2!1sen!2suk!4v1549910466109" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>