<p>County Marquees has been providing marquees for parties, weddings, baby showers, divorce parties and funerals in the beautiful old medieval town of Berkhamsted for many years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Vintage trestle tables</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style for a reception</p>
</div>
<p>We offer a wide range of marquees, furniture, flooring and lighting to make your event a roaring success. Our bespoke service allows us to adapt our tents to accommodate narrow gardens with uneven surfaces, and incorporate trees and flowerbeds. Whether you are organising a small garden get together or a large wedding in the Chiltern Hills, we have the kit and the expertise to truly transform your outdoor space.</p>
<p>Our professional and experienced team are regularly complimented for their helpfulness and tidiness. Check out our <a href="/company/testimonials.htm">testimonials</a> to see what our customers have said about us. See our <a href="/photos.htm">gallery</a> to see some of our recent work. For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>We offer a free site visit where our Hertfordshire representative will measure your available space, guide you through our portfolio and help you choose marquee, furniture and equipment to suit your square footage, numbers and budget.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Beautiful Berkhamstead wedding marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style wedding marquee" width="581"  loading="lazy" />
<p class="wide">A traditional style marquee nestling in trees in a large Berkhamsted garden</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Knew exactly what they were doing</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Berkhamsted</span><span class="region">Hertfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can highly recommend this marquee company both for the quality of the marquee and the service they provided.</p>
<p class="noborder">We hired a traditional marquee and catering tent for a wedding held in our garden. We saw several companies but County Marquees gave us confidence that they knew exactly what they were doing and suggested several things we hadn't thought of.  Before the day they were helpful sorting out the details so we got exactly what we wanted. The day they put up the marquee they worked like trojans to get everything in place but were still polite, helpful and smiling (especially Tom) after 13 hours work!</p>
<p class="noborder">The wedding was blessed by the good weather but even if it had rained on the day, the marquee would have been up to the job. It was enhanced by wonderful lighting, rustic furniture and  luxury loos to make the perfect venue.</p>
<p>If we ever needed another marquee we would certainly go back to County Marquees with no hesitation.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in Hertfordshire</a> for customers' convenience. These are not recommendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Booking Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Berkhamsted area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19754.949860342847!2d-0.5931187425939359!3d51.762865949368965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48764331de4f38d7%3A0xce00ed56ae5060aa!2sBerkhamsted!5e0!3m2!1sen!2suk!4v1543423777107" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>