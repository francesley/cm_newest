<p>For more than 25 years, County Marquees has supplied tents for events in the historic town of Amersham and the beautiful surrounding South Buckinghamshire countryside.</p>
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/bucks.jpg" alt="Dancing in a wedding marquee" width="198" height="150"/>
<p>Bride and groom in their Buckinghamshire wedding marquee</p>
</div>
<p>Marquees offer the advantage of a useful, temporary space that is practical and can also be very personal. No event is too large or too small. Whether you are organising a garden party in Chesham or a 40th amidst the Chiltern Hills, we have the equipment and experience to make it special.</p>
<p>We offer a free site visit where our Buckinghamshire representative will measure your available space, advise on size and layouts and guide you through all the options for <a href="/equipment/marquees.htm">marquee type</a>, furniture, lighting and equipment.</p>
<p>Our friendly and reliable team are regularly <a href="/company/testimonials.htm">praised</a> for their good nature and professionalism.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">prices page</a>. Alternatively, you can get a rough idea of costs by getting a quick <a href="/pricing/quotation.htm">online quote</a>
<p>From your initial enquiry right through to the big day, we will make it our job to ensure your event is one to remember.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more or to arrange a free site visit.</p>
<h2>Buckinghamshire wedding</h2>
<img src="/images/areas/milton_keynes.jpg" alt="Romantically styled wedding reception" width="581" loading = "lazy"/>
<p class="wide">A romantically styled wedding reception inside a traditional marquee with wooden poles.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Minor miracle in our garden</span>
<abbr title="2011-11" class="dtreviewed hide">Nov 2011</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday boy's wife</span><span class="adr"><span class="locality">Amersham</span> <span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thanks for your excellent service and quick installation and take down. Our guests thought you had arranged a minor miracle in our garden.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/bucks.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Buckinghamshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<div class="region different">Surrey</div> <div class="postal-code">NW5 1SL</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">0207 267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Amersham area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39589.20782156894!2d-0.641139149630673!3d51.6721849434618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48765d5de642892b%3A0x53bb64d290f3fe8a!2sAmersham!5e0!3m2!1sen!2suk!4v1543937009329" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>