<p>With nearly 30 years experience County Marquees have been supplying tents of all types and styles for functions and events in the Buckinghamshire county area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/blacked_out.jpg" alt="Blacked out party tent" width="198" height="150"/>
<p>Blacked out party tent</p>
</div>
<p>We offer a all sorts of <a href="/equipment/marquees.htm">tents</a>, furniture and equipment. So whether you are organising a birthday, a barbecue or a corporate event, we have the very thing to suit your function.</p> 
<p>We carefully finish our marquees to the highest standard. For instance, we have a large range of <a href="/equipment/lighting.htm">lighting</a> including starlight ceilings, decorative chandeliers, LED uplighters, festoon lights, fairy lights, truss lighting with pin spots and par cans to name just a few. Check out our <a href="/photos.htm">gallery</a> to view our recent work or to get inspiration for your forthcoming event.</p>
<p>To read the very nice things that our customers say about us, check out our <a href="/company/testimonials.htm">testimonials</a> page.</p>
<p>We offer free site visits across Buckinghamshire where one of our experienced representatives will discuss and advise you on how to bring your ideas to life, and what options would best suit your space, function and budget.</p>
<p>All of our prices are displayed <a href="/pricing/marquees.htm">here</a> or you can get a <a href="/pricing/quotation.htm">immediate online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Made the party</span>
<abbr title="2004-04" class="dtreviewed hide">April 2004</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Beaconsfield</span> <span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Saturday night went brilliantly, thank you. The marquee, in both incarnations, was fabulous and thank you to you and the team for an extremely efficient service. I will have no hesitation in recommending your company to others.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Special occasion</span>
<abbr title="2003-11" class="dtreviewed hide">November 2003</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">Marlow</span> <span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>It was all perfect, thank you. The marquee was stunning in all its detail... Many thanks for a fantastic service from the very beginning. We will be sure to give glowing references.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Buckingham</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<div class="region different">Surrey</div> <div class="postal-code">NW5 1SL</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">0207 267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>

<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover all of Buckinghamshire</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d631888.0690167883!2d-1.3690871780224334!3d51.782169568156206!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876429c5944d5d7%3A0x534e1b8d064679d!2sBuckinghamshire!5e0!3m2!1sen!2suk!4v1543939472388" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>