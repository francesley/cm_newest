<p>County Marquees is an independent, family run company which supplies quality marquees to the ancient town of Princes Risborough at the foot of the Chilterns.</p>
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/bucks.jpg" alt="Dancing in a wedding marquee" width="198" height="150"/>
<p>Bride and groom in their Buckinghamshire wedding marquee</p>
</div>
<p>We supply a large variety of tents: flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pagoda style Chinese Hat marquees. Our bespoke service enables us to adapt our tents to fit into the narrowest of gardens with intrusive shrubbery and different levels.</p>
<p>Our team is regularly complimented on their cheerfulness and fantastic results. We are incredibly proud of our <a href="/company/testimonials.htm">customer testimonials</a>. Check out our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p>
<p>We offer a free site visit where our Buckinghamshire representative will measure your site and discuss with you how best to turn your ideas into reality taking into account space, numbers of guests and budget. For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hanging Greenery</h2>
<img src="/images/areas/marlow.jpg" alt="Hanging greenery in transparent marquee, Marlow" width="581" loading = "lazy"/>
<p class="wide">Stunning foliage hanging from the roof of a transparent wedding marquee in Marlow</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">We couldn't believe how fab it looked</span>
<abbr title="2010-06" class="dtreviewed hide">October 2006</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">High Wycombe</span><span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Your team were brilliant and the marquee was really fantastic - by far the best thing we spent money on for the wedding. We couldn't believe how fab it looked.  I had rather low expectations as our neighbours had one for a big party a couple of months before and I thought it looked really tatty, so I was preparing myself for the worst. Instead, you created a really beautiful space for our reception. Our guests couldn't believe how it looked and neither could we.  We feel we received great service from you in addition to the fab marquee and will certainly be recommending you to friends.</p>
</blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/bucks.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Buckinghamshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Princes Risborough area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19771.442013345044!2d-0.8475487427049695!3d51.72516539895836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876f30af20173c7%3A0xcb834cd773cd9f10!2sPrinces+Risborough!5e0!3m2!1sen!2suk!4v1543937729202" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>