<p>County Marquees is an independent, family run company offering an efficient, quality marquee hire service to all of South London.</p>
<div class="textPhotos">
<img src="/images/pages/areas/streatham2.jpg" alt="Sitting round a firepit with a lit up traditional tent in the background" width="198" height="150"/>
<p>Round a firepit outside an illuminated wedding tent in a Streatham garden</p>
<img src="/images/pages/areas/barnes.jpg" alt="Wedding marquee with opulent floral ring hanging from the roof, Barnes" width="198" height="150"/>
<p>Barnes wedding marquee with opulent hanging floral ring</p>
</div>
<p>We supply several different sorts of marquees: flexible frame structures, romantic traditional style poled marquees, stretch tents and pointy topped Chinese Hat marquees. We offer free site visits across South London, where our representative will measure up and discuss how to turn your ideas into reality, factoring space, numbers and budget into a practical quotation.</p>
<p>Our brilliant team are regularly <a href="/company/testimonials.htm">praised</a> for their hard work and results. Check out our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p>
<p>See our <a href="/pricing/marquees.htm">price list</a> for more information on pricing, or you can also get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough idea of how much your dream marquee might cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Made the party</span>
<abbr title="2014-04" class="dtreviewed hide">April 2014</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Thames Ditton</span> <span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I must say that we and our friends thought the marquee was great - it really made the party. I would not hesitate to recommend you to anyone that asked.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Sincerely appreciated</span>
<abbr title="2020-06" class="dtreviewed hide">June 2020</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Kingston</span><span class="region">South London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I wanted to drop you a quick note to thank you and to say what a pleasure it was dealing with you. It was the first time we used a marquee for any event, and your prompt and efficient handling of all my various queries and changes over the last month or two was sincerely appreciated. I think everybody who came to the party was genuinely impressed, so thank you for your help and support throughout.</p></blockquote></div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>Marquees over all of South London</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d159302.17512490196!2d-0.19362630282328308!3d51.40128180288056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760164c02f4ed3%3A0x924732ad507ab3af!2sSouth%20London!5e0!3m2!1sen!2suk!4v1677682016615!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>