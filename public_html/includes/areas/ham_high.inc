﻿<p>With nearly thirty years of experience, County Marquees has worked with many clients in the Hampstead and Highgate area supplying marquees for weddings, parties and barmitzvahs.</p>
<div class="textPhotos">
<img src="/images/pages/areas/muswell_hill.jpg" alt="Art Deco style marquee in Muswell Hill" width="198" height="150"/>
<p>Art Deco style marquee in small Muswell Hill garden</p>
<img src="/images/pages/areas/highgate2.jpg" alt="Flexible frame tent slotted in alongside a swimming pool" width="198" height="150"/>
<p>Flexible frame tent slotted in alongside a swimming pool</p>
</div>
<p>We have a variety of tents for you to choose from, which range from flexible frame structures, romantic traditional style poled marquees, through to stretch tents, bell tents and pointy topped Chinese Hat marquees. Our tailored service allows us to adapt and accommodate narrow gardens and the hills of North London.</p>
<p>Our fantastic team is consistently <a href="/company/testimonials.htm">commended</a> for their good nature and professionalism from start to finish. We offer a free site visit where David Higgs, our North London representative, will outline the options which best suit your space and help you arrange all aspects of the marquee including lighting, flooring and heating.</p> 
<p>Check out our <a href="/real.htm">real examples</a> gallery to see our recent work or get some inspiration for your next event.</p>
<p>Please see our <a href="/pricing/marquees.htm">pricing</a> page for a clear list of options or for a rough estimate, get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Highgate Party Tent</h2>
<img src="/images/areas/highgate.jpg" alt="Marquee built over a steeply sloping garden in Highgate" width="581" loading = "lazy"/>
<p class="wide">Marquee built over a steeply sloping garden in Highgate</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">First class operation</span>
<abbr title="2007-03" class="dtreviewed hide">March 2007</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Hampstead</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Everything was terrific. Looked amazing and party excellent. Your men were a delight — helpful, thoughtful, quick, efficient. They put the marquee up and took it down with such expertise. Thank you very much for everything. County Marquees really does seem a first class operation.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Helpful, calm and extremely efficient service</span>
<abbr title="2006-08" class="dtreviewed hide">August 2006</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Highgate</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I would  like to thank you again for the helpful, calm and extremely efficient service you provided. You made our house and garden look beautiful. The young men could not have worked harder or been more pleasant and you were all supportive in the face of our wedding/weather panics.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>Marquee hire in the Hampstead area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d19839.9319800062!2d-0.1909333488400489!3d51.56838939013095!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2suk!4v1539448368309" width="600" height="450" frameborder="0" style="border:0"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>