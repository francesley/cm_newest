<p>County Marquees has over 25 year of experience of supplying marquees for weddings, parties and corporate events in the Guildford area.  It is where it all started for us!</p>
<div class="textPhotos">
<img src="/images/pages/areas/littlefield.jpg" alt="Traditional style tent at Littlefield Manor" width="198" height="150"/>
<p>Traditional style tent at Littlefield Manor, near Guildford</p>
<img src="/images/pages/areas/littlefield_manor.jpg" alt="Wedding reception interior" width="198" height="150"/>
<p>Wedding reception interior</p>
</div>
<p>We have a lot of kit for you to choose from. Frame marquees with their flexible interior space, traditional tents for romantic or rustic style weddings, Chinese hat pagoda structures, stretch tents... And on top of that an extensive stock of furniture, flooring and a wide variety of lighting equipment.</p>
<p>Whether you have a small garden or a large lawn overlooking the South Downs, Shaun Haworth, County Marquees' charming Surrey representative will help you find the combination of marquee, furniture and equipment that best matches your square footage, budget and dreams.</p>
<p>Our wonderful team are consistently <a href="/company/testimonials.htm">praised</a> for their cheerfulness, helpfulness and outstanding results: you can see some <a href="/real.htm">real examples here</a>.</p>
<p>Our <a href="/pricing/marquees.htm">price guide</a> is on our Website and you can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough idea of how much your dream marquee might cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Guildford Wedding</h2>
<img src="/images/areas/littlefield_manor.jpg" alt="Wedding marquee at Littlefield Manor" width="581" loading = "lazy"/>
<p class="wide">Traditional style wedding tent at Littlefield Manor, a pretty marquee venue three miles outside Guildford</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Interior looked spectacular</span>
<abbr title="2003-12" class="dtreviewed hide">December 2003</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Guildford</span> <span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I can't tell you how pleased we were with the marquee in every respect. Your team of erectors were hard working and fun, and the interior looked truly spectacular when completed.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Marquee looked absolutely stunning</span>
<abbr title="2010-05" class="dtreviewed hide">May 2010</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Groom</span><span class="adr"><span class="locality">Guildford</span><span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>The marquee looked absolutely stunning and the dance floor, bar and chill out zone were brilliant. Once again thank you so much for all your help.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>

<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We serve Guildford</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39964.51158253012!2d-0.6004425806943762!3d51.24151012553452!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875c4afb79659df%3A0x946699b2f9eba8a1!2sGuildford!5e0!3m2!1sen!2suk!4v1539227325962" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>