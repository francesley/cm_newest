<p>County Marquees is an independent, family run company which supplies quality marquees to Wimbledon and the surrounding South London area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/roehampton.jpg" alt="Blacked out party marquee" width="198" height="150"/>
<p>Blacked out party marquee in Roehampton</p>
<img src="/images/pages/areas/wimbledon.jpg" alt="Marquee for Wimbledon tennis" width="198" height="150"/>
<p>Marquee for Wimbledon tennis</p>
</div>
<p>We supply a large variety of tents: flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pointy topped Chinese Hat marquees. Our tailored services allows us to adapt our tents to accommodate narrow gardens with uneven surfaces and incorporate natural structures such as trees and flowerbeds.</p>
<p>Our team is regularly complimented on their cheerfulness and fantastic results. We are incredibly proud of our <a href="/company/testimonials.htm">customer testimonials</a>. Alternatively, check out our <a href="/photos.htm">gallery</a> to see examples of our recent work or to get inspiration for your next event.</p>
<p>We offer a free site visit where our South London representative will measure your space and discuss with you how best to turn your ideas into reality taking into account space, numbers and budget.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Fiftieth at Wimbledon</h2>
<img src="/images/areas/wimbledon2.jpg" alt="Fiftieth birthday party in a Wimbledon marquee" width="581"  loading="lazy" />
<p class="wide">A glamorous blacked out tent for a 50th birthday party in Wimbledon</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Helpful, cheerful and worked very hard</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Richmond</span><span class="region">South London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>We had a cracking (as one guest described it) party and the marquee really made all the difference to it – we were so pleased.  Artur and Bartek were really helpful, cheerful and worked very hard. Many thanks, and when we need another marquee or to give a recommendation you’ll be top of the list.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Wimbledon area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39802.84784244764!2d-0.25886935252122095!3d51.427341583095874!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487608b7738a00c5%3A0x260eae2ec8a63d71!2sWimbledon%2C+London!5e0!3m2!1sen!2suk!4v1543332001775" width="600" height="450" frameborder="0" style="border:0"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>