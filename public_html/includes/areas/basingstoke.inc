<p>County Marquees is an independent, family run business which has operated for nearly 30 years supplying quality marquees to Basingstoke.</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small Colindale garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>Marquees are highly versatile, making them perfect for very occasion. We have a variety of different styles in a range of sizes and shapes to be used alone or in conjunction with each other to create a truly unique space.</p>
<p>One of the reasons marquee hire is so popular is because you have far more control over the interior. During your free site visit, our Hampshire representative will guide you through our portfolio to help you create a package that is perfectly tailored to your taste.</p>
<p>We are incredibly proud of the work we do. To read what our customers have to say about us, check out our <a href="/company/testimonials.htm"> testimonials</a>. View our <a href="/real.htm">real examples gallery</a> to see our latest work or to get inspiration for your next event.</p>
<p>Information on pricing can be found on our <a href="/pricing/marquees.htm">pricing page</a> or for a rough estimate, get a <a href="/pricing/quotation.htm">quick online quote</a>.
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hampstead Garden Suburb Wedding</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden on Winnington Road. Gardens in Hampstead Garden Suburb come in interesting shapes and we often put up complicated marquee layouts to accomodate.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Your enthusiastic team sorted everything out beautifully</span>
<abbr title="2001-01" class="dtreviewed hide">January 2001</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Petersfield</span><span class="region">Hampshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for so efficiently providing the marquee and tent arrangements for our daughter's wedding. I am afraid that our tiered terrace and the generally lopsided nature of our garden posed some unique technical problems. But you and your enthusiastic team sorted everything out beautifully.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hampshire</h3>
<p class="venues">We maintain a list of <a href="/venues/hants.htm">marquee venues in Hampshire</a> for customers' convenience.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees South</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<div class="region">West Sussex</div>
<span class="postal-code">PO20 7EB</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Basingstoke area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d79903.82977171344!2d-1.1773516192251388!3d51.25600730363931!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487404c180a05953%3A0x682dc7216eb9702a!2sBasingstoke!5e0!3m2!1sen!2suk!4v1543937037333" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>