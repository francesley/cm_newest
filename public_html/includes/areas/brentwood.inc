<p>County Marquees is a respected marquee hire company with over 25 years experience of supplying beautiful marquees to Brentwood for weddings, parties and corporate events.</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small Colindale garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>Marquees are a practical and impressive venue option which gives you complete control over the size, shape and interior decor. They provide a comfortable environment and create a unique atmosphere. Our aim is to combine your most creative ideas with our experience to make the perfect venue fashioned to your tasteand style.</p>
<p>Our frame marquees are modular in design and as such can be made larger or smaller by adding or removing sections. So whether you have a small garden or huge lawn we can provide what you want.  As well as framed Marquees we have the trusted and tried Traditional style Marquees with their centre poles and guy ropes, modern stretch tents great for informal parties and the pagoda style Chinese hats which can combine with other styles of tent as porches or Chill-out areas. </p>
<p>Our brilliant team is consistently <a href="/company/testimonials.htm">praised</a> for their good nature, efficiency and fantastic results. Check out our <a href="/real.htm">real examples gallery</a> to see beautifully dressed marquees and to get inspiration for your next event.</p>
<p>For information on prices, see our <a href="/pricing/marquees.htm">pricing page</a>. Alternatively, to get a rough idea of costs, get a <a href="/pricing/quotation.htm">quick online quote</a>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hampstead Garden Suburb Wedding</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden on Winnington Road. Gardens in Hampstead Garden Suburb come in interesting shapes and we often put up complicated marquee layouts to accomodate.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Essex</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Essex</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in Essex</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Essex</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Brentwood area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d79249.9374210944!2d0.23578874863758126!3d51.631258766182576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d895f09b17f6c1%3A0x8587606559e92a59!2sBrentwood!5e0!3m2!1sen!2suk!4v1543937269074" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>