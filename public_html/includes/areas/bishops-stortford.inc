<p>County Marquees is a family-run business which for over 25 years has provided marquees for weddings, parties and corporate events to the ancient Roman town of Bishop's Stortford and the surrounding Hertfordshire and Essex countryside.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Vintage trestle tables</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style for a reception</p>
</div>
<p>We provide a service which brings together all aspects of the event. From initial site inspections, through marquee erection and decoration, flooring, lighting, furniture etc., we have it all covered. We have all sorts of <a href="/equipment/marquees.htm">tents</a>: from flexible frame structures, romantic traditional style poled marquees, through to stretch tents, bell tents and pointy topped Chinese Hat marquees.</p>
<p>On your free site visit, our Hertfordshire representative will guide you through our portfolio and discuss all the different layout and furniture options to suit your space and taste.</p>
<p>Our wonderful team is regularly <a href="/company/testimonials.htm">praised</a> for their good nature, helpfulness and tidiness. Check out our <a href="/photos.htm">gallery</a> to get an idea of tje quality of our work or for some inspiration.</p> 
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or for a rough estimate of how much your dream marquee may cost, get an <a href="/pricing/quotation.htm">immediate quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Beautiful Hertfordshire wedding marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style wedding marquee" width="581" loading = "lazy"/>
<p class="wide">A traditional style marquee nestling in trees in a large garden</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">You three have given new meaning to customer care</span>
<abbr title="2007-21" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Father of the bride</span><span class="adr"><span class="locality">Bishop Stortford</span><span class="region">Hertfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder"Thank you for doing such a wonderful, professional job with the marquee.</p>
 <p>Your team was extremely efficient getting everything set up and cleared away.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in London and the Home Counties</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Bishop's Stortford area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19706.06958877791!2d0.13532035773516216!3d51.87449015059798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d88f92175df725%3A0xdac4e1ab995f27de!2sBishop&#39;s+Stortford!5e0!3m2!1sen!2suk!4v1543423809282" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>