<p>County Marquees has extensive experience in providing the perfect marquee for birthdays, weddings and corporate events in Olney and the surrounding Buckinghamshire area. 
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/bucks.jpg" alt="Dancing in a wedding marquee" width="198" height="150"/>
<p>Bride and groom in their Buckinghamshire wedding marquee</p>
</div>
<p>Marquees are a versatile and flexible venue option, giving you control over the size, shape and layout.</p>
<p>Whether you are looking for a traditional style wedding marquee, a small unlined tent for a garden barbeque or a modern stretch tent for a corporate function, the team at County Marquees will be happy to help.</p>
<p>We don't just provide <a href="/equipment/marquees.htm">all sorts of marquees</a>, equipment and accessories. We will work alongside you to design a stunning marquee interior. Check out our <a href="/photos.htm">photo gallery</a> for photos of beautifully dressed marquees or to get ideas for your next event.</p>
<p>Our wonderful team has a <a href="/company/testimonials.htm">reputation</a> for being cheerful, helpful and efficient. As the day of your event approaches, you can relax knowing our friendly and experienced team are overseeing the construction of your venue. Our goal is not to meet expectations &ndash; we want to exceed them. </p>
<p>We have a list of <a href="/pricing/marquees.htm">prices</a> on this website. There are no hidden extras. For a rough estimate based on your requirements, get a <a href="/pricing/quotation.htm">quick online quote</a>.
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more or to arrange a free site visit.</p>
<h2>Buckinhamshire wedding</h2>
<img src="/images/areas/milton_keynes.jpg" alt="Romantically styled wedding reception" width="581" loading = "lazy"/>
<p class="wide">A romantically styled wedding reception inside a traditional marquee with wooden poles.</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Helped us out no end with everything</span>
<abbr title="2017-06" class="dtreviewed hide">June 2017</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday boy</span><span class="adr"><span class="locality">Olney</span> <span class="region">Bucks</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you David. And I want to say how lovely all the guys are that set up and took down the marquee. Both days were punishingly hot but they were cheery, friendly and helped us out no end with everything. You have a great team there! Until next time.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Special occasion</span>
<abbr title="2003-11" class="dtreviewed hide">November 2003</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Olney</span> <span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for all your help and assistance in making my wife's 40th such a special occasion.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/bucks.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Buckinghamshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<div class="region different"></div> <div class="postal-code">NW5 1SL</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">0207 267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>

<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Olney area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39157.13604116304!2d-0.748766341420592!3d52.164856915502156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4877a831601f8713%3A0xf5581f1810955233!2sOlney!5e0!3m2!1sen!2suk!4v1549975604211" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>