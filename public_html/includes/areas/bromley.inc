<p>County Marquees is a family run company with over 25 years experience providing a first class marquee hire service to Bromley and South London.</p>
<div class="textPhotos">
<img src="/images/pages/areas/kent_winter.jpg" alt="Kentish winter marquee interior" width="198" height="150"/>
<p>Winter marquee interior</p>
<img src="/images/pages/areas/stretch.jpg" alt="Stretch tent in a walled garden" width="198" height="150"/>
<p>Stretch tent in a walled garden</p>
</div>
<p>County Marquees will work with you to create a package tailored to your style and requirements whether you have a small garden or a grand event space. During a free site visit, our Kent representative will survey your site and discuss the different options available to you.</p>
<p>Along with a great selection of different types of <a href="/equipment/marquees.htm">marquees</a>, you can choose from our extensive list of furniture, flooring, lighting and much more.</p>
<p>Our team is reliable and dedicated to not only meeting your expectations, but surpassing them. To read what our customers say about us, check out our <a href="/company/testimonials.htm">customer testimonials</a>.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> for a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous clear marquee</h2>
<img src="/images/areas/clear_marquee.jpg" alt="Clear marquee in Kent" width="581" height="410"/>
<p class="wide">A ravishing clear marquee with dramatic lighting in Kent.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Wonderful professional job</span>
<abbr title="2015-06" class="dtreviewed hide">August 2016</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Croydon</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I just wanted to say thank you for doing such a wonderful, professional job with the marquee. Your team was extremely efficient getting everything set up and cleared away, and managed to preserve all of the flowers (which was great). Thank you so much. If you ever need a recommendation, let me know.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recommendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Bromley area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d159422.41340147168!2d-0.09946111480333378!3d51.36675243186501!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8aade0f2c8551%3A0x40eae2da2ec6970!2sLondon+Borough+of+Bromley!5e0!3m2!1sen!2suk!4v1543937322113" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>