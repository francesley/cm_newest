<p>County Marquees is a friendly, family run company which supplies beautiful finished marquees to Walton-on-Thames and the surrounding Surrey area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/farnham.jpg" alt="Blacked out party tent in Farnham" width="198" height="150"/>
<p>Blacked out party tent in Farnham</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>From flexible frame structures, romantic traditional style poled marquees, to stretch tents, bell tents and pointy topped Chinese Hat marquees, we have a lot of kit for you to choose from. Whether you are organising a small garden party or large corporate event, our Surrey representative will help you find the combination of marquee, furniture, equipment and accessories which best suits your sqaure footage, budget and style.</p>
<p>We are incredibly proud of our team and the fantastic results they produce. Check out our <a href="/company/testimonials.htm">testimonials</a> to see what our customers have said about us. View our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p> 
<p>For more information on our prices, please see our <a href="/pricing/marquees.htm">pricing list</a> or to get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough estimate of how much your ideal marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Party in Walton-on-Thames</h2>
<img src="/images/areas/walton_party.jpg" alt="Marquee attached to a glass house in Farnham" width="581" loading = "lazy"/>
<p class="wide">Marquee party with blacked out lining, night sky and lighting in Walton-on-Thames</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Walton-on-Thames</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19917.527415415694!2d-0.42007236596682274!3d51.39035827522746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760ad5522c3c67%3A0x48bd10007520cad9!2sWalton-on-Thames!5e0!3m2!1sen!2suk!4v1540882840654" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>