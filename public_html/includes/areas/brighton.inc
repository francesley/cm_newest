<p>For 25 years and more County Marquees has been supplying marquees to Brighton and the surrounding areas. It is one of our favourite towns on the South Coast.</p> 
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Traditional style wedding tent with open sides" width="198" height="150"/>
<p>Traditional style wedding tent with open sides</p>
<img src="/images/pages/areas/buckhurst_park.jpg" alt="Traditional marquee at Buckhurst Park" width="198" height="150"/>
<p>Traditional marquee at Buckhurst Park, East Sussex wedding venue</p>
</div>
<p>Come rain or shine marquees provide a versatile and impressive answer to your venue dilemmas. Our marquees come in a <a href="/equipment/marquees.htm">variety of styles, shapes and sizes</a> so whatever the occasion you will find the perfect marquee design for your requirements.</p>
<p>During the initial obligation&ndash;free site visit, our Sussex representative will survey your available space and guide you through our portfolio to choose a combination of tent, furniture and equipment that best matches your site, budget and dreams.</p>
<p>In the run up to your event you can relax knowing our friendly and experienced staff are overseeing the construction of your venue. Our brilliant team works hard to not only meet your expectations, but surpass them. For examples of the quality of their work, see our <a href="/photos.htm">gallery</a>. To read what our customers have to say about us, check out our <a href="/company/testimonials.htm">testimonals</a> 
<p>For information on prices, see our <a href="/pricing/marquees.htm">pricing page</a>. Alternatively, to get a rough idea of costs, get a <a href="/pricing/quotation.htm">quick online quote</a>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Saltdean Wedding</h2>
<img src="/images/areas/saltdean.jpg" alt="Corner of a traiditionally styled flowery wedding marquee with coconut matting on the floor and Georgian windows." width="581" loading = "lazy"/>
<p class="wide">Corner of a flowery wedding marquee.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Wonderful marquee</span>
<abbr title="2017-07" class="dtreviewed hide">July 2017</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">Uckfield</span><span class="region">East Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you for the wonderful marquee you supplied us with. It was just perfect. You all did a great job getting it put up so smoothly and then it was all down and gone so quickly on the Monday. It was all top-class and your guys worked really efficiently and all so helpful. So glad we were lucky with the weather too as it made it even more lovely. We had very many compliments and enquiries as it was much admired.</p>
</blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Sussex</h3>
<p class="venues">We maintain a list of marquee venues in <a href="/venues/eSussex.htm">East Sussex</a> and <a href="/venues/wSussex.htm">West Sussex</a> for customers' convenience.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay Road</div>
<div class="locality">Chichester</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Brighton area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d80629.2312602821!2d-0.17622968904455535!3d50.83739698810048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48758509f6294167%3A0x9cc6af7a727d0ef9!2sBrighton!5e0!3m2!1sen!2suk!4v1543937299599" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>