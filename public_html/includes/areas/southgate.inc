<p>County Marquees has over 25 year of experience of supplying tailored marquees suitable for birthdays, weddings and barmitzvahs in the Southgate area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/southgate_dancing.jpg" alt="Dancing in a marquee in Southgate" width="198" height="150"/>
<p>Dancing in Southgate!</p>
<img src="/images/pages/areas/muswell_hill.jpg" alt="Art Deco style marquee in Muswell Hill" width="198" height="150"/>
<p>Art Deco style marquee in small Muswell Hill garden</p>
</div>
<p>We have a beautiful collection of tents, furniture and equipment for you to choose from. So whether you want a marquee for small garden or a grand event, we have tents suitable for every occasion.</p>
<p>During the free site visit, our friendly and helpful London representative will advise you on your different options and take you through our extensive portfolio that best matches your square footage, budget and dreams.</p>
<p>Take a look at our <a href="/real.htm">real examples</a> to see images of our recent work or if you would like to see what our comstomers have said about us, check out our <a href="/company/testimonials.htm">testimonials</a>.</p>
<p>See our <a href="/pricing/marquees.htm">prices</a> page for more information on pricing or you can also get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to give you a rough estimate of how much your your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hampstead Garden Suburb Wedding</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden on Winnington Road. Gardens in Hampstead Garden Suburb come in interesting shapes and we often put up complicated marquee layouts to accomodate.</p>
<h2>Local praise</h2>

<div class="hreview single">
<span class="summary hide">How efficient</span>
<abbr title="2010-05" class="dtreviewed  hide">May 2010</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Southgate</span><span class="region">London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Just wanted to say just how efficient your guys were in putting up and taking down. Very pleasant too. THANK YOU!</p></blockquote></div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Southgate</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19813.403299931364!2d-0.14424931549827527!3d51.629154787740354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487619185785b5fb%3A0x8c9477a21529a64d!2sSouthgate%2C+London!5e0!3m2!1sen!2suk!4v1540239358736" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>