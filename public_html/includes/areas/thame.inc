<p>County Marquees is a friendly, efficient family run business which has served Thame and the surrounding Oxfordshire area since 1989.</p>
<div class="textPhotos">
<img src="/images/pages/areas/attached.jpg" alt="Flowery interior for a winter party" width="198" height="150"/>
<p>Flowery interior for a winter party</p>
<img src="/images/pages/areas/attached2.jpg" alt="Frame marquee attached to a house" width="198" height="150"/>
<p>Frame marquee attached to a house</p>
</div>
<p>Come rain or shine, marquees provide a practical and impressive venue option when organising a special occasion. We have a variety of tents in different styles an sizes: from flexible frame structures, romantic traditional style poled marquees, through to stretch tents, bell tents and pointy topped Chinese Hat marquees.</p>
<p>Whether you are organising a garden birthday party or wedding overlooking the Chiltern Hills, we have a range of furniture and accessories for you decorate and create the ideal space for you, your family and friends to enjoy.</p>
<p>We work hard to not only meet your expectations, but surpass them. This sentiment is reflected in our <a href="/company/testimonials.htm">customer testimonials</a>. Check out our <a href="/real.htm">real examples gallery</a> to see our recent work or to get inspiration for your next event. We offer a free site visit where our Oxfordshire representative will advise you on how to create a bespoke package that suits your space, style and budget.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> to get a rough estimate, get a <a href="/pricing/quotation.htm">quick online quote</a>.
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous traditional marquee at night</h2>
<img src="/images/areas/oxfordshire.jpg" alt="Illuminated traditional style marquee" width="581" loading = "lazy"/>
<p class="wide">Illuminated traditional style marquee in a large Oxfordshire garden.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">you are all fab</span>
<abbr title="2013-06" class="dtreviewed hide">June 2013</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Party giver</span><span class="adr"><span class="locality">Abingdon</span><span class="region">Oxfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Can I just say again you are all fab.  So impressed with everything; your team were charming and helpful and we will definitely use you again.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Thame area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19762.99161146512!2d-0.9917255926480741!3d51.7444851991685!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487692ccccc246a1%3A0xf5b79ef2a4cccee2!2sThame!5e0!3m2!1sen!2suk!4v1543423979489" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>