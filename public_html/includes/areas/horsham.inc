<p>County Marquees is an independent, family run company which has supplied quality marquees to Horsham and the surrounding Sussex countryside for over 25 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Wedding tent with open sides" width="198" height="150"/>
<p>Wedding tent with open sides</p>
<img src="/images/pages/areas/buckhurst_park.jpg" alt="Traditional marquee at Buckhurst Park" width="198" height="150"/>
<p>Traditional marquee at Buckhurst Park, East Sussex wedding venue</p>
</div>
<p>We supply a large variety of tents: flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pagoda style Chinese Hat marquees. Our bespoke service allows you to choose a tent that will accommodate your site &ndahs; fom the narrowest of gardens with different levels and intrusive vegetation to the perfect, spacious lawn.</p>
<p>Our team is regularly complimented on their cheerfulness and fantastic results. We are incredibly proud of our <a href="/company/testimonials.htm">customer testimonials</a>. Check out our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p>
<p>We offer a free site visit where our Sussex representative will assess your site and discuss with you how best to turn your ideas into reality taking into account the venue, guest numbers and your budget. For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which will provide you with a rough estimate of how much your dream marquee might cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Wedding in Horsham</h2>
<img src="/images/areas/horsham.jpg" alt="Traditional style tent for a wedding in Horsham" width="581" loading = "lazy"/>
<p class="wide">Traditional style tent for a wedding in Horsham featuring bunting and paper lanterns. Final decoration not yet completed. The floor is covered with coconut matting and at the bottom left is a glimpse of a wooden dance floor.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">We were so pleased</span>
<abbr title="2012-06" class="dtreviewed hide">June 2012</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Horsham</span><span class="region">West Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichestern</span><span class="region">Sussex</span><span class="postal-code"> PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Just to say thank you for the lovely marquee for Charlotte and Grant's wedding. Everything went really well – even the weather! We were so pleased.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Sussex</h3>
<p class="venues">We maintain a list of <a href="/venues/wSussex.htm">marquee venues in West Sussex</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243 790390"><span class="tel">01243 790390</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Horsham area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39659.3151937733!2d-0.36812227789392377!3d51.59192949007347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761158a02507f9%3A0x24d74f8999e25bd9!2sHarrow!5e0!3m2!1sen!2suk!4v1540209714786" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>