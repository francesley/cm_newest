<p>County Marquees is an independent family-run company which has been supplying Marquees to Hastings and the surrounding areas for alsmost 30 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Wedding tent with open sides" width="198" height="150"/>
<p>Wedding tent with open sides</p>
<img src="/images/pages/areas/buckhurst_park.jpg" alt="Traditional marquee at Buckhurst Park" width="198" height="150"/>
<p>Traditional marquee at Buckhurst Park, East Sussex wedding venue</p>
</div>
<p>There are marquees for every occasion: baby showers, significant birthdays, weddings, divorces and funerals &ndash; we have done them all. We have several different types of tent:</p>
<ul><li>Flexible <a href="/equipment/marquees.htm#frame">frame marquees</a> come in many different widths (3m, 4m, 6m, 9m, 10m, 12m, and 15m) and can be used in narrow complicated gardens with intrusive trees and shrubbery as well as on huge front lawns</li>
<li><a href="/equipment/marquees.htm#traditional">Traditional style marquees</a> with their internal poles and swooping exterior lines are perfect for currently popular rustic / festival style weddings</li>
<li>We also stock stretch tents, pointy topped Chinese hat marquees and bell tents</li></ul>
<p>We offer a free site visit where our Sussex representative will assess your site, advise on size and layouts, and then guide you through our extensive portfolio of furniture, flooring, lighting equipment and diverse accessories to help you create a bespoke package to suit your space, style and budget.</p>
<p>Our team is regularly <a href="/company/testimonials.htm">praised</a> for their good humour, cheerfulness, tidiness and fantastic results. Our <a href="/photos.htm">gallery</a> showcases some of our previous work and offers decorative inspiration for your next event.</p>
<p>For information on prices, see our <a href="/pricing/marquees.htm">pricing page</a>. Alternatively, to get a rough idea of costs, get an <a href="/pricing/quotation.htm">online quote</a>.
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Saltdean Wedding</h2>
<img src="/images/areas/saltdean.jpg" alt="Flowery frame marquee" width="581" loading = "lazy"/>
<p class="wide">A simple lined Marquee with Coconut Matting, Georgian style windows and beautiful flowers.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">I was truly impressed</span>
<abbr title="2013-08" class="dtreviewed hide">August 2014</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Robertsbridge</span><span class="region">Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I was truly impressed by the quality of the marquees, your brilliant fitting of the dinner/dancing marquee in amongst the trees and the lovely nature of your teams working through such extremes of weather.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Sussex</h3>
<p class="venues">We maintain a list of marquee venues in <a href="/venues/eSussex.htm">East</a> and <a href="/venues/wSussex.htm">West Sussex</a> for customers' convenience.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243 790390"><span class="tel">01243 790390</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Hastings area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d40288.24239361389!2d0.5447662038292598!3d50.86791928016032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df107433235113%3A0x6d17a64660baced7!2sHastings!5e0!3m2!1sen!2suk!4v1550159263647" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>