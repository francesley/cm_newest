<p>With nearly 30 years of experience, County Marquees has supplied beautifully finished marquees for weddings, parties and corporate events to Croydon and the South London area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/croydon2.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Huge marquee setup for a very large Croydon wedding</p>
<img src="/images/pages/areas/croydon.jpg" alt="Inside the marquee on top of a garage" width="198" height="150"/>
<p>Inside the marquee on top of a garage (see below)</p>
</div>
<p>We offer a range of marquees suited to variety of occasions. From more traditional style tents through to modern stretch tents, we have an extensive stock of furniture and equipment for you to choose from. We cater for any kind of event, from a small birthday in the garden to a fully serviced and decorated marquee for a sizable wedding.</p>
<p>We offer a free site visit where our helpful Croydon representative will guide you through our portfolio and advise you on a combination of marquee, <a href="/equipment/furniture.htm">furniture</a> and accessories to best match your square footage, budget and dreams.</p>
<p>Our cheerful and fantastic team will help and advise you on all aspects of the marquee including
<a href="/equipment/lighting.htm">lighting</a>, flooring and heating. We pride ourselves in consistently supplying outstanding marquees which are beautifully and carefully finished. You can see some <a href="/real.htm">real examples here</a>.</p>
<p>Our prices are outlined on our <a href="/pricing/marquees.htm">pricing page</a> or you can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>On the roof</h2>
<img src="/images/areas/croydon.jpg" alt="Frame marquee on the roof of a Croydon garage" width="581" loading = "lazy"/>
<p class="wide">Frame marquee on the roof of a Croydon garage. Frame marquees can fit in awkward spaces and narrow gardens. Over to the left is a Chinese hat tent.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Extremely efficient</span>
<abbr title="2016-08" class="dtreviewed hide">August 2016</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Croydon</span><span class="region">South London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I just wanted to say thank you for doing such a wonderful, professional job with the marquee. Your team was extremely efficient getting everything set up and cleared away, and managed to preserve all of the flowers (which was great). Thank you so much. If you ever need a recommendation, let me know.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Croydon area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d79709.37063282958!2d-0.15227946391267247!3d51.36780722189854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875fe2a84421673%3A0xd574f25dc50c4945!2sCroydon!5e0!3m2!1sen!2suk!4v1543331463670" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>