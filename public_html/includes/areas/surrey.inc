<p>County Marquees is a friendly, efficient family run company that has been serving Surrey from our Guildford office since our foundation in 1989.</p>
<div class="textPhotos">
<img src="/images/pages/areas/littlefield.jpg" alt="Traditional style tent at Littlefield Manor" width="198" height="150"/>
<p>Traditional style tent at Littlefield Manor, near Guildford</p>
<img src="/images/pages/areas/littlefield_manor.jpg" alt="Wedding reception interior" width="198" height="150"/>
<p>Wedding reception interior</p>
</div>
<p>We stock marquees of all kinds. We also have good range of furniture and equipment &ndash; from tables and chairs to heaters and hat stands.</p>
<p>Our team includes some of the people most dedicated to errecting perfectly finished marquees in the world. We sometimes have to calm them down! But the results are worth it for our customers: see some <a href="/real.htm">real marquees</a> or <a href="/company/testimonials.htm">testimonials</a>.</p>
<p>We list our <a href="/pricing/marquees.htm">prices</a> on this website and you can also get a <a href="/pricing/quotation.htm">quick online quote</a>. We offer free site visits across Surrey where an experienced member of staff will measure up and discuss how to turn your ideas into the reality of space, numbers and budgets.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Surrey Wedding</h2>
<img src="/images/areas/littlefield_manor.jpg" alt="Wedding marquee at Littlefield Manor" width="581" loading = "lazy"/>
<p class="wide">Traditional style wedding tent at Littlefield Manor, a pretty marquee venue three miles outside Guildford</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Made the party</span>
<abbr title="2004-04" class="dtreviewed hide">April 2004</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday girl</span><span class="adr"><span class="locality">Thames Ditton</span> <span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I must say that we and our friends thought the marquee was great - it really made the party. I would not hesitate to recommend you to anyone that asked.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Special occasion</span>
<abbr title="2003-11" class="dtreviewed hide">November 2003</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday girl's husband</span><span class="adr"><span class="locality">Runfold</span> <span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for all your help and assistance in making my wife's 40th such a special occasion.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>

<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover all of Surrey</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d319509.8260638805!2d-0.6755299754252206!3d51.27118026403769!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df555ad5122581%3A0xd55283445c034277!2sSurrey!5e0!3m2!1sen!2suk!4v1538796754230" width="600" height="450" allowfullscreen></iframe>

</div>