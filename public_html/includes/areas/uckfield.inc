<p>County Marquees is an independent, family run company which has supplied high quality marquees to Uckfield and the surrounding East Sussex area for over 25 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Traditional style wedding tent with open sides" width="198" height="150"/>
<p>Traditional style wedding tent with open sides</p>
<img src="/images/pages/areas/piltdown.jpg" alt="Traditional marquee and catering tent" width="198" height="150"/>
<p>Traditional marquee with catering tent alongside at Piltdown</p>
</div>
<p>We supply marquees in a variety of styles and sizes to suit most occasions. Whether you are organising a small garden party, a significant birthday or a wedding overlooking The Weald we will have the equipment you need. We stock as well a large range of furniture, lighting equipment and diverse accessories which our friendly Sussex representative will advise you on to help you turn your ideas into reality.  Our watchwords are being both practical and personal.</p> 
<p>Our brilliant team is frequently <a href="/company/testimonials.htm">complimented</a> on their good nature and hard work. Check our our <a href="/photos.htm">gallery</a> see some of our previous work, or see our <a href="/real.htm">real examples</a> page to get inspiration for your next event.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or to get a rough estimate of how much your dream marquee may cost, get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Sussex wedding</h2>
<img src="/images/areas/horsham.jpg" alt="Traditional style tent for a wedding in Horsham" width="581" loading = "lazy"/>
<p class="wide">Traditional style tent for a wedding in Horsham featuring bunting and paper lanterns. Final decoration not yet completed. The floor is covered with coconut matting and at the bottom left is a glimpse of a wooden dance floor.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">Uckfield</span><span class="region">Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you for the wonderful marquee you supplied us with. It was just perfect. You all did a great job getting it put up so smoothly and then it was all down and gone so quickly on the Monday. It was all top-class and your guys worked really efficiently and all so helpful. So glad we were lucky with the weather too as it made it even more lovely. We had very many compliments and enquiries as it was much admired.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in East Sussex</h3>
<p class="venues">We maintain a list of <a href="/venues/eSussex.htm">marquee venues in East Sussex</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243 790390"><span class="tel">01243 790390</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Uckfield area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20100.590978195945!2d0.07850890507886725!3d50.96858239119502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df4cb231c06f6d%3A0xbe6a49ee52196faf!2sUckfield!5e0!3m2!1sen!2suk!4v1543939552993" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>