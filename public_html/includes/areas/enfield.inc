<p>With over 25 years of experience, County Marquees has been supplying outstanding marquees for weddings, birthdays and parties in Enfield and the North London area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/harrow.jpg" alt="Baby shower tea time marquee" width="198" height="150"/>
<p>Baby shower marquee in Harrow</p>
<img src="/images/pages/areas/whetstone.jpg" alt="Wedding reception marquee" width="198" height="150"/>
<p>Wedding reception marquee in long Whetstone garden</p>
</div>
<p>We provide a quality, tailored marquee service which caters to every occasion big or small. Whether you are arranging a small family gathering, a wedding, or other large function, we are here to help. With a range of <a href="/equipment/marquees.htm">marquees</a> in different styles and an extensive portfolio of furniture and equipment, our North London representative will help you make sense of all the options and how they could best work with your space.</p>
<p>Our wonderful team will assist you from start to finish help you arrange all aspects of the marquee from the lighting to the flooring and heating. Consistently <a href="/company/testimonials.htm">commended</a> for our good nature, helpfulness and fantastic results, check out our <a href="/real.htm">real examples</a> gallery to see some of our recent work or to get inspiration for your next event.</p>
<p>You can find a list of <a href="/pricing/marquees.htm">prices</a> or for a rough estimate, get a  <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>An Indian wedding in Enfield</h2>
<img src="/images/areas/enfield.jpg" alt="Indian wedding tent" width="581" loading="lazy" />
<p class="wide">Stunningly decorated tent for an Indian wedding </p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Marquee smart and nicely finished</span>
<abbr title="2014-07" class="dtreviewed hide">July 2014</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Enfield</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Many thanks for your marquee and your team's efficiency, speed and courtesy. All went well, thanks. Marquee smart and nicely finished and the extra space really helped us.</p>
</blockquote></div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Enfield area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23548.34256866422!2d-0.09099769932766291!3d51.655907052160245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761878d288422d%3A0x19b8e39ceea4fef9!2sEnfield!5e0!3m2!1sen!2suk!4v1540197086687" width="600" height="450" frameborder="0" style="border:0"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>