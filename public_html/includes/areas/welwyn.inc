<p>For nearly 30 years, County Marquees has provided a high quality and personalised marquee service to Welwyn Garden City and the surrounding Hertfordshire area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Vintage trestle tables</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style for a reception</p>
</div>
<p>Marquees provide a practical and impressive venue option when organising that special occasion. We can individualise our marquees to suit most sites and styles. Whether you are hosting a garden party or something a little more special like a wedding or significant birthday, we have it covered.</p>
<p>We offer a free site visit where our Hertfordshire representative will assess your site and advise on size and layouts. We will then guide you through our extensive portfolio of furniture, flooring, lighting equipment and other accessories helping you to create a bespoke package to suit your space, style and budget.</p>
<p>Our team is regularly <a href="/company/testimonials.htm">praised</a> for their good nature, tidiness and fantastic results. Our <a href="/photos.htm">gallery</a> showcases some of our previous work and offers decorative inspiration for your next event.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">prices page</a>. Alternatively, get a <a href="/pricing/quotation.htm">quick online quote</a> to get a rough idea of costs.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Beautiful Hertfordshire wedding marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style wedding marquee" width="581" loading = "lazy"/>
<p class="wide">A traditional style marquee nestling in trees in a large garden</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2014-07" class="dtreviewed hide">July 2014</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Father of the bride</span><span class="adr"><span class="locality">Welwyn</span><span class="region">Hertfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you for your efficiency and for the polite and courteous staff who put up and took down the marquee. It has been a pleasure doing business with you; you do exactly what you say you will and this is refreshing!</p> </blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in Hertfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Welwyn Garden City area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39475.259546763715!2d-0.22736904808900937!3d51.802438099131386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48763b2cfae26d51%3A0x87c6eedf2bf3263!2sWelwyn+Garden+City!5e0!3m2!1sen!2suk!4v1543424063029" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></iframe>
</div>