<p>County Marquees a independent, family run company which supplies high quality marquees to Witney and the surrounding Oxfordshire area. 
</p>
<div class="textPhotos">
<img src="/images/pages/areas/attached.jpg" alt="Flowery interior for a winter party" width="198" height="150"/>
<p>Flowery interior for a winter party</p>
<img src="/images/pages/areas/attached2.jpg" alt="Frame marquee attached to a house" width="198" height="150"/>
<p>Frame marquee attached to a house</p>
</div>
<p>We supply luxury marquees in a variety of styles and sizes to suit any occasion. Whether you are organising a small garden party or wedding overlooking the rolling Cotswold Hills, we have it covered. With a large variety of tents, furniture, equipment and accessories, our friendly Oxfordshire representative will guide you through our portfolio advising you on how best to turn your dream into reality.</p> 
<p>Our billiant team is frequently <a href="/company/testimonials.htm">complimented</a> on their good nature and hard work. Check our our <a href="/photos.htm">gallery</a> see some of our previous work, or see our <a href="/real.htm">real examples</a> page to get inspiration for your next event.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or to get a rough estimate of how much your dream marquee may cost, get a <a href="/pricing/quote.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous traditional marquee at night</h2>
<img src="/images/areas/oxfordshire.jpg" alt="Illuminated traditional style marquee" width="581" loading = "lazy"/>
<p class="wide">Illuminated traditional style marquee in a large Oxfordshire garden.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">The marquee was perfect</span>
<abbr title="2021-07" class="dtreviewed hide">July 2021</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Henley-on-Thames</span><span class="region">Oxfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Just wanted to say a HUGE thanks to you and the team. The marquee was perfect, and the team top notch. We had THE perfect time. Thank you!</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Oxfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/oxford.htm">marquee venues in Oxfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Oxford</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Witney area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19744.678340298884!2d-1.5127542425247833!3d51.78633644962576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48712d59b4dd94a7%3A0xd8007a2cc120563f!2sWitney!5e0!3m2!1sen!2suk!4v1543424086194" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>