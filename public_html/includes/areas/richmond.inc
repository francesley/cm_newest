<p>County Marquees provide perfectly finished marquees for weddings, parties and events in Richmond and the surrounding South London area. </p>
<div class="textPhotos">
<img src="/images/pages/areas/streatham2.jpg" alt="Sitting round a firepit with a lit up traditional tent in the background" width="198" height="150"/>
<p>Round a firepit outside an illuminated wedding tent in a Streatham garden</p>
<img src="/images/pages/areas/barnes.jpg" alt="Wedding marquee with opulent floral ring hanging from the roof, Barnes" width="198" height="150"/>
<p>Barnes wedding marquee with opulent hanging floral ring</p>
</div>
<p>With a good range of tents and equipment, we can incorporate natural features inside the marquee such as trees and flower beds and slopes. We can tailor our tents to fit narrow gardens or grand event spaces. So whether you are organising a small get-together in the garden or a large wedding, we have the kit to cater to your event.</p>
<p>We offer a free site visit where our South London representative will discuss with you the options for marques, furniture and equipment that best suits your space, budget and vision. Our wonderful team is consistently being praised for their hard work and efficiency. Check out our <a href="/company/testimonials.htm">testimonials</a> to see what our customers said about us. Or view our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p>
<p>See our <a href="/pricing/marquees.htm">price list</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to give you rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Kew Gardens marquee</h2>
<img src="/images/areas/kew.jpg" alt="Marquee in Kew Gardens" width="581" loading = "lazy"/>
<p class="wide">Marquee nestled into a space alongside the ravishing Victorian Temperate House at Kew Gardens.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Helpful, cheerful and worked very hard</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Richmond</span><span class="region">South London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>We had a cracking (as one guest described it) party and the marquee really made all the difference to it – we were so pleased.  Artur and Bartek were really helpful, cheerful and worked very hard. Many thanks, and when we need another marquee or to give a recommendation you’ll be top of the list.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Richmond area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39779.407538565734!2d-0.32635682899585533!3d51.45424613426788!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760b88237c5027%3A0xa9a7de1ab1d56e58!2sRichmond!5e0!3m2!1sen!2suk!4v1540693231412" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>