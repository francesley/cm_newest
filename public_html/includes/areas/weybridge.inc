<p>County Marquees is a friendly, family run company which supplies beautifully finished marquees to Weybridge and around.</p>
<div class="textPhotos">
<img src="/images/pages/areas/blacked_out.jpg" alt="Glitzy blacked out party tent near Weybridge" width="198" height="150"/>
<p>Glitzy, blacked out party/p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground</p>
</div>
<p>From flexible frame structures and romantic traditional style poled marquees to stretch tents and pointy topped Chinese Hat marquees, we have a wide range of equipment for you to choose from. Whether you are organising a small party in the back garden or a large corporate event, our friendly Surrey representative Shaun Haworth will help you find the best combination of tent and equipment for your space, budget and style.</p>
<p>We are so proud of our hardworking, friendly team and what they do. See our <a href="/company/testimonials.htm">testimonials</a> to find out what our customers say about us or view our <a href="/photos.htm">photo gallery</a> to see examples of our recent work.</p> 
<p>To find out about pricing, please see our <a href="/pricing/marquees.htm">price list</a>. Or get an immediate <a href="/pricing/quotation.htm">online quote</a> for a rough estimate of how much your ideal marquee will cost.</p>
<p>Do <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Weybridge wedding</h2>
<img src="/images/areas/walton_party.jpg" alt="Traditional wedding tent in a Weybridge garden" width="581" loading = "lazy"/>
<p class="wide">Milling about in a traditional style wedding tent in a beautiful, big garden in Weybridge</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Superb job</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday boy's daughter</span><span class="adr"><span class="locality">Esher</span><span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I just wanted to say a big thank you for sorting the marquee for us at short notice. And that the guys who fitted it were absolutely amazing. Martin ( I think that was his name) &ndash; was fantastic. A lot of people would have made things hard for us but he worked things out and did a superb job. Very professional and nice to us all the time. He's an asset to your company. My dad ended up having a great birthday.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Weybridge</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39851.360750451175!2d-0.45790402954822224!3d51.37162690615036!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875d8a820781111%3A0x7ef1a4794151ace1!2sWeybridge!5e0!3m2!1sen!2suk!4v1700747319720!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>