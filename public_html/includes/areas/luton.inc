<p>County Marquees is a family run company which has been providing beautiful, professional marquees to Luton and the Bedfordshire area for over 25 years. 
</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground</p>
</div>
<p>We provide a wide variety of marquees in a range of sizes and <a href="/equipment/marquees.htm">styles</a>. Our bespoke service allows us to adapt our marquees to accommodate narrow gardens, different levels and even to incorporate natural features such as trees and flowerbeds, completely transforming an outdoor space. Whether you are organising a small birthday party or a large, lavish wedding, marquees provide a practical and personal venue option.</p>
<p>We take a lot of pride in the results we produce. Take a look at <a href="/real.htm">real examples</a> of our recent work or read what our <a href="/company/testimonials.htm">customers</a> had to say about us.</p>
<p>We offer a no-obligation site visit where our Bedfordshire representative will assess your site and show you our portfolio of photos. We will then advise you on the combination of marquee, <a href="/equipment/furniture.htm">furniture</a> and ancillary equipment which best suits your square footage, style and budget.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> to give you an estimate of how much your dream marquee would cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Fab Wedding Marquee</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden.  Gardens come in all shapes and sizes so we are often putting up complex marquee layouts to accommodate this.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Befordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/beds.htm">marquee venues in Bedfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Bedfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Luton area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d78795.18477486235!2d-0.49797568894132066!3d51.89108526158297!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876360018f62c49%3A0x33fa5303edefd681!2sLuton!5e0!3m2!1sen!2suk!4v1543424238011" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>