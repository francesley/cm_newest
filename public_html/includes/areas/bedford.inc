<p>County Marquees is a family run business that has provided quality marquees to the historical County town of Bedford and the surrounding areas for over 25 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground</p>
</div>
<p>We provide a range of marquees in a variety of <a href="/equipment/marquees.htm">styles</a>, 
sizes and configurations and cater for everything from birthdays, weddings to corporate events. Our bespoke service enables us to adapt our tents to accommodate awkward sites, for example a narrow garden with uneven surfaces and features such as trees and flowerbeds.</p>
<p>On your free site visit, our Bedfordshire representative will measure your site and advise you on a combination of marquee, furniture and equipment which best suits your venue, guest numbers and budget and help you to turn your ideas into reality.</p>
<p>We are incredibly proud of the outstanding results our team produce. Check out our <a href="/company/testimonials.htm">testimonials</a> to see what our customers say about us. Or have a look at the <a href="/real.htm">real examples</a> of our recent work.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Fab Wedding Marquee</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden.  Gardens can come in interesting shapes and we often put up complicated marquee layouts to accommodate.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summar0711" class="dtreviewed hide">July 20168</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Billington</span><span class="region">Bedfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">The marquee was beautiful and the crew who put it up and took it down were wonderful; a really nice group of lads who were very polite, efficient and resourceful - nothing was too much trouble and they did an amazing job.</p>
</blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Bedfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/beds.htm">marquee venues in Bedfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Bedfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Bedford area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39179.8081129153!2d-0.48651769409194984!3d52.13908711428254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4877b6c6c66c36cd%3A0x125df4c90cabaf30!2sBedford!5e0!3m2!1sen!2suk!4v1543424203064" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>