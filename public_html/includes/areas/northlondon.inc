<p>County Marquees is a friendly, professional company that has been supplying marquees for wedding, parties and of course barmitzvahs across North London for more than 25 years. </p>
<div class="textPhotos">
<img src="/images/pages/areas/golders_green.jpg" alt="Thin frame marquee with transparent roof panels, Golders Green" width="198" height="150"/>
<p>Thin marquee with clear roof panels, Golders Green</p>
<img src="/images/pages/areas/highgate.jpg" alt="Steeply sloping marquee" width="198" height="150"/>
<p>Marquee built over a steeply sloping garden, Highgate</p>
</div>
<p>Our marquees range from large, traditional marquees suitable for the large gardens, to frame tents that can be adapted to accommodate narrow gardens and the sloping ground that characterises much of hilly North London. Our beautiful stretch tents are a modern and eye-catching alternative and leave a lasting impression for you and your guests. Marquees provide a practical and impressive venue option perfect for birthdays, festivals and corporate events.</p> 
<p>Through our tailored service, we can provide a complete package to include lighting, flooring, dance floors, heating, and much more. During the free site visit, David Higgs, our North London representative, will help you make sense of all the options and how they could work with your space and ideas.</p> 
<p>Our team is regularily <a href="/company/testimonials.htm">complimented</a> for their good nature, helpfulness and for the quality of their work: take a look at some <a href="/real.htm">real life examples</a>.</p>
<p>Our prices are outlined on our <a href="/pricing/marquees.htm">pricing page</a> or you can alternatively get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Romantic and stylish</span>
<abbr title="2005-05" class="dtreviewed hide">May 2005</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">City of London</span> <span class="region hide">London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Many thanks for the beautiful marquee you put up on Sunday. The event was a resounding success and all the guests absolutely adored the marquee. It created a very romantic and stylish image on our terrace.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Very professional service</span>
<abbr title="2006-12" class="dtreviewed hide">December 2006</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Stratford</span><span class="region">London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for providing a very professional service.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>

<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>Marquees over all of North London</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d79335.55895109774!2d-0.1627234727482042!3d51.58231939209388!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2suk!4v1539359640793" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</div>