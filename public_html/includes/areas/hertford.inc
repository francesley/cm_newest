<p>County Marquees is an independent, family run firm which has supplied marquees to Hertford for over 25 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Vintage trestle tables</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style for a reception</p>
</div>
<p>Marquees provide a practical and impressive venue option for all sorts of events. With a choice of <a href="/equipment/marquees.htm">marquee styles</a> which range from the traditional style poled marquees through to the modern framed marquees and stretch tents, we also have a range of furniture, equipment and accessories for you to create a perfect venue, both practical and personal</p>
<p>During your free site visit our friendly Hertfordshire representative will guide you through the selection process and advise you on a combination which suits your square footage, your budget and taste.</p> 
<p>Our wonderful team are regularly <a href="/company/testimonials.htm">complimented</a> on their good nature, tidiness and results. See some <a href="/real.htm">real dressed marquees</a> to see examples of our recent work or to get inspiration for your next event be it a party, wedding or corporate event.</p> 
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or to get a rough estimate of how much your dream marquee may cost, get a <a href="/pricing/quotation.htm">quick online quote</a>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Beautiful Hertfordshire wedding marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style wedding marquee" width="581" loading = "lazy"/>
<p class="wide">A traditional style marquee nestling in trees in a large garden</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">You three have given new meaning to customer care</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Groom</span><span class="adr"><span class="locality">Hertford</span><span class="region">Hertfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Just to thank you and Martin and Martin for the professionalism with which our marquee was erected, gremlins sorted and marquee taken down. You three have given new meaning to customer care!! Thank you so much.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in Hertfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Hertford area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9869.926425230828!2d-0.09205013599359278!3d51.79736011684847!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876212e8c2f619d%3A0xa6018bb5cf8e48cb!2sHertford!5e0!3m2!1sen!2suk!4v1543423903114" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>