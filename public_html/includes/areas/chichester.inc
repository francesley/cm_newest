<p>Since 1989, County Marquees has been providing high quality, stylish marquees for weddings, parties and corporate events to Chichester and across Sussex.</p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Traditional style wedding tent with open sides" width="198" height="150"/>
<p>Traditional style wedding tent with open sides</p>
<img src="/images/pages/areas/chichester_party.jpg" alt="Eighteenth party at Dell Quay, outside Chichester" width="198" height="150"/>
<p>Eighteenth party at Dell Quay, outside Chichester</p>
</div>
<p>From a backyard bash, a festival style wedding to an elegant corporate event and beyond, we have the equipment, the experience and the expertise to ensure your event goes off without a hitch. We have several different kinds of tents, plus an extensive stock of furniture, lighting equipment and theming styles for you to customise your marquee exactly to your space and taste.</p>
<p>From the initial site visit to the painstaking clear up, our Sussex representative will work alongside you to create an event to remember.</p> 
<p>Our team is regularly <a href="/company/testimonials.htm">complimented</a> for their good nature, tidiness and fantastic results &ndash; you can see them in our <a href="/real.htm">gallery</a> or on our <a href="/real.htm">real examples page</a>.</p>
<p>For pricing information, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> for a rough estimate.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Chichester Wedding</h2>
<img src="/images/areas/chichester.jpg" alt="Traditional style tent for a wedding reception just outside Chichester" width="581" loading = "lazy"/>
<p class="wide">Traditional style tent for a wedding reception just outside Chichester</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Your marquee and equipment created a fabulous venue</span>
<abbr title="2012-11" class="dtreviewed hide">November 2012</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of birthday girl</span><span class="adr"><span class="locality">Chichester</span><span class="region">Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for all your help and advice. Poppy's party was a huge success and your marquee and equipment created a fabulous venue. Your team of Tom, Martin and Marcin were great. They were very helpful, professional, funny and created no mess. Could you pass my thanks on to them for doing such a great job too. We look forward to using your company again.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Sussex</h3>
<p class="venues">We maintain a list of <a href="/venues/wSussex.htm">marquee venues in Sussex</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House Dell Quay Road</div>
<div class="locality">Sussex</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Chichester area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d40315.76097255662!2d-0.8137286594614784!3d50.836071159635836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487448a304768be1%3A0x89743104b8aa4233!2sChichester!5e0!3m2!1sen!2suk!4v1543937368219" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>