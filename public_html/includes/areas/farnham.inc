<p>County Marquees has over 25 year of experience of supplying marquees for weddings, parties and events in Farnham and the surrounding Surrey area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/farnham.jpg" alt="Blacked out party tent in Farnham" width="198" height="150"/>
<p>Blacked out party tent in Farnham</p>
<img src="/images/pages/areas/westmead.jpg" alt="Wedding reception in a traditional marquee" width="198" height="150"/>
<p>Traditional and stretch tent at pretty Surrey wedding venue, Westmead Events</p>
</div>
<p>Come rain or shine, marquees can provide a practical, striking and straightforward venue option. Entertain your guests in a marquee that is bespoke to your  requirements. Whether you are having a small party in the garden or a large corporate event overlooking the Surrey Hills, we have a large selection of equipment for you to choose from. Our helpful Surrey representative will guide you through all of the options, advising you on the best combination to match your space, budget and style.</p>
<p>We offer a free, no obligation site visit where our expert team is available to help you with all the decision making, from the lighting through to the flooring and heating. Check out our <a href="/real.htm">gallery</a> to view some of our recent work or read what our clients have said about us on our <a href="/company/testimonials.htm">testimonials</a> page.</p>
<p>We have a list of <a href="/pricing/marquees.htm">prices</a> on this website for you to look at, or you can also get a <a href="/pricing/quotation.htm">quick online quote</a> to aims to give you a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Party in Farnham</h2>
<img src="/images/areas/farnham.jpg" alt="Marquee attached to a glass house in Farnham" width="581" loading = "lazy"/>
<p class="wide">Marquee attached to a glass house in Farnham &ndash; and its reflection</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Beautiful marquee</span>
<abbr title="2016-07" class="dtreviewed hide">July 2016</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">Busbridge Lakes</span><span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you so very much for our beautiful marquee. The Chinese hat and lighting looked amazing too. We were left speechless when we first saw the marquee. It all looked so pretty lit up in the evening too.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Farnham</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39992.423025610005!2d-0.8230044434924524!3d51.209377145759504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48742c02125a0e93%3A0x875c1ded7715bf26!2sFarnham!5e0!3m2!1sen!2suk!4v1540844700140" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>