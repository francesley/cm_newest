<p>For over thirty years, family-run business County Marquees has been providing event services to the Sevenoaks area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/kent_winter.jpg" alt="Kentish winter marquee interior" width="198" height="150"/>
<p>Winter marquee interior</p>
<img src="/images/pages/areas/sevenoaks.jpg" alt="Traditional style tent in a Sevenoaks garden" width="198" height="150"/>
<p>Traditional style tent in a Sevenoaks garden</p>
<img src="/images/pages/areas/stretch.jpg" alt="Stretch tent in a walled garden" width="198" height="150"/>
<p>Stretch tent in a walled garden</p>
</div>
<p>Sevenoaks and the surrounding area, what with the rippling High Weald hills and the rolling North Downs fields, hold plenty of must see sights. Chartwell, Winston Churchill's much-loved family home, is one; another is Emmetts Garden, with its profusion of roses, shrubs, and bluebells in spring. There's also family-owned Riverhill Himalayan Gardens, home to the Rogers family since 1840 &ndash; a venue that can be hired out and is featured in the photo of a clear marquee at the top of this page.</p>
<p>If you're planning to hold an event in the Sevenoaks region &ndash; whether it's a wedding, a birthday party, or anything in between &ndash; the friendly team at County Marquees can help to plan and execute it. Do you need a bar unit with soft cube seating? No worries. Or how about a transparent marquee with lime washed chiavari chairs and fairy lights? Easily done.</p>
<p>Our <a href="/photos.htm">photo gallery</a> will give you examples of how our marquees, decorations, and lighting options can help bring your dream event to life. If you have a vision in mind, our friendly Kent representative can assess your site completely free of charge, and turn your dream into practical reality.</p>
<p>We're always here to help, which is why you can get a general idea of our <a href="/pricing/marquees.htm">pricing</a> without leaving the site, and receive a <a href="/pricing/quotation.htm">free online quote</a> if you so desire. If you need any more information regarding our services, please do not hesitate to <a href="/contact_us.htm">get in touch</a>.</p>
<h2>Sevenoaks Wedding</h2>
<img src="/images/areas/sevenoaks.jpg" alt="Colonial styled wedding at Ide Hill" width="581" loading = "lazy"/>
<p class="wide">A beautiful colonial styled wedding at Ide Hill</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">The most wonderful day of my life!</span>
<abbr title="2016-06" class="dtreviewed hide">June 2016</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Sevenoaks</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>It was the most wonderful day of my life!  The marquee was so beautiful, thank you.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">We sing your praises everywhere we go</span>
<abbr title="2017-06" class="dtreviewed hide">June 2017</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Ide Hill</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>What a great job you and the team did. We sing your praises everywhere we go, so we hope you’ll get some more business out of it!</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recomendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Sevenoaks area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39936.346996665365!2d0.1504319456724815!3d51.27391987679457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8ad4c5d5bdfc1%3A0x2c9c51426386fe62!2sSevenoaks!5e0!3m2!1sen!2suk!4v1543938305231" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>