<p>County Marquees is an independent, family run company which for over 25 years has been supplying marquees throughout East and West Sussex.</p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Wedding tent with open sides" width="198" height="150"/>
<p>Wedding tent with open sides</p>
<img src="/images/pages/areas/buckhurst_park.jpg" alt="Traditional marquee at Buckhurst Park" width="198" height="150"/>
<p>Traditional marquee at Buckhurst Park, East Sussex wedding venue</p>
</div>
<p>We supply a <a href="/equipment/marquees.htm">several different types of tents</a>: flexible frame structures, romantic traditional style poled marquees for that rustic wedding, stretch tents, bell tents and pagoda style Chinese Hat marquees. Our range of marquees enable us to accommodate narrow gardens with uneven surfaces and intrusive shrubbery as well as the fabled well manicured front lawn.</p>
<p>We also stock a good range of furniture, lighting and larger equipment. Sussex marquees &ndash; with some of the finest views in England &ndash; can really take advantage of more recent introductions like <a href="/photos/wedding.htm">clear marquees</a> and clear panoramic windows.</p>
<p>We offer a free site visit where our Sussex representative will assess your site and discuss with you how best to turn your ideas into reality taking into account available space, numbers and your budget. For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough estimate of how much your dream marquee may cost.</p>
<p>Our team is regularly complimented on their cheerfulness and fantastic results and we are incredibly proud of our <a href="/company/testimonials.htm">customer testimonials</a>. And most important, check out our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Sussex Wedding</h2>
<img src="/images/areas/chichester.jpg" alt="Traditional style tent for a wedding reception just outside Chichester" width="581" loading = "lazy"/>
<p class="wide">Traditional style tent for a wedding reception just outside Chichester</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Just perfect</span>
<abbr title="2017-07" class="dtreviewed hide">July 2017</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday girl</span><span class="adr"><span class="locality">Uckfield</span><span class="region">East Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you for the wonderful marquee you supplied us with. It was just perfect. You all did a great job getting it put up so smoothly and then it was all down and gone so quickly on the Monday. It was all top-class and your guys worked really efficiently and all so helpful. So glad we were lucky with the weather too as it made it even more lovely. We had very many compliments and enquiries as it was much admired.</p>
</blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Sussex</h3>
<p class="venues">We maintain a list of marquee venues in <a href="/venues/eSussex.htm">East Sussex</a> and <a href="/venues/wSussex.htm">West Sussex</a> for customers' convenience.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Sussex</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<div class="region">West Sussex</div>
<span class="postal-code">PO20 7EB</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover West  and East Sussex</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d321777.1806377427!2d-0.7367096082682786!3d50.94435413679902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df58434716977f%3A0xaea9abe6ecb97dc2!2sWest+Sussex!5e0!3m2!1sen!2suk!4v1550024478837" frameborder="0" style="border:0;margin-bottom:0.75em;" allowfullscreen></iframe>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d321806.5526600403!2d0.08582068936230731!3d50.94011042634431!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47dee0253a3551b9%3A0x94da6e8412e06b70!2sEast+Sussex!5e0!3m2!1sen!2suk!4v1550024567310" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>