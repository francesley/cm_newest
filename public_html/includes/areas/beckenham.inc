<p>County Marquees is a family-run company which supplies professional marquees to Beckenham and surrounding South London area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/streatham2.jpg" alt="Sitting round a firepit with a lit up traditional tent in the background" width="198" height="150"/>
<p>Round a firepit outside an illuminated wedding tent in a Streatham garden</p>
<img src="/images/pages/areas/barnes.jpg" alt="Wedding marquee with opulent floral ring hanging from the roof, Barnes" width="198" height="150"/>
<p>Barnes wedding marquee with opulent hanging floral ring</p>
</div>
<p>We have a wide variety of luxury marquees, equipment and furniture along with a lovely team to bring your marquee dreams to life. Regularly <a href="/company/testimonials.htm">complimented</a> for our friendliness and efficiency, we offer free site visits where our Beckenham representative will measure your area and provide advice to help you decide on the style and size of marquee which best suits your ideas, function and budget.</p>
<p>We pride ourselves in providing <a href="/real.htm">beautifully finished</a> marquees</a>. Our tailored service means we can adapt our tents to accomodate narrow gardens with uneven and sloped surfaces and incorporate features such as trees and flowerbeds.</p>
<p>See our <a href="/pricing/marquees.htm">pricing</a> page for a clear list of options with no hidden or extra fees. Or get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough estimate on how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Dulwich Corporate Event</h2>
<img src="/images/areas/dulwich.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A smart corporate marquee in Dulwich.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Perfect set up for our wedding</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Dulwich</span><span class="region">South London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">A huge thank you for organising such a perfect set up for our wedding. And to your team for everything they did to help make our day such a special one – they were fantastic.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Beckenham area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d106281.22153850351!2d-0.10584219256960042!3d51.42921553671026!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876002c98ae8579%3A0x69e9592192db09e5!2sBeckenham!5e0!3m2!1sen!2suk!4v1543331336858" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>