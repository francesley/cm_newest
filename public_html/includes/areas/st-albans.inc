<p>County Marquees is an independent, family run firm which supplies high quality marquees to St Albans and the surrounding Hertfordshire area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Vintage trestle tables</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style for a reception</p>
</div>
<p>Whether you are organising a wedding, birthday party or corporate event, a beautifully finished County Marquees marquee can transform a garden or outdoor space. We supply a range of different <a href="/equipment/marquees.htm">tents</a> in various sizes and styles: from traditional style poled marquees to contemporary, flexible frame structures, through to versatile stretch tents, cool bell tents and pointy topped Chinese Hats. Our bespoke service allows us to adapt our tents to accommodate narrow gardens with encroaching vegetation and even different levels.</p>
<p>We pride ourselves on the exceptionally high standards of our marquees and service. Our wonderful team is frequently <a href="/company/testimonials.htm">praised</a> for their good nature and fantastic results. Check out our <a href="/photos.htm">gallery</a> to see examples of our recent work or to get inspiration for your next event.</p>
<p>We offer a free site visit where our Hertfordshire representative will assess your space and advise you on the right combination of marquee, furniture and accessories to best suit your square footage, style and budget.</p>
<p>For information on pricing, visit our <a href="/pricing/marquees.htm">pricing page</a> get a rough estimate of how much your dream marquee may cost with our <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Beautiful Hertfordshire wedding marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style wedding marquee" width="581" loading = "lazy"/>
<p class="wide">A traditional style marquee nestling in trees in a large garden</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">One thing we were able to tick off and not be anxious about</span>
<abbr title="2015-03" class="dtreviewed hide">March 2015</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday boy</span><span class="adr"><span class="locality">St Albans</span><span class="region">Hertfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you for an excellent service. The marquee was great. You guys were efficient, friendly and careful. One thing we were able to tick off and not be anxious about, which added greatly to our enjoyment of the party. They were back here today (Sunday) before 8am to take the marquee down and away, which they did by 10am, quietly with no hassle.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in Hertfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the St Albans area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39523.59008745394!2d-0.3493187487429099!3d51.747220446714756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487638a0e793c909%3A0x71ec848046a64059!2sSt+Albans!5e0!3m2!1sen!2suk!4v1543423953538" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>