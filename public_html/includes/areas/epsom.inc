<p>For nearly 30 years, County Marquess has been supplying stunning marquees for weddings, parties and corporate events in Epsom and the Surrey area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/redhill.jpg" alt="Wedding reception in a traditional marquee" width="198" height="150"/>
<p>Wedding reception in a traditional marquee, Redhill</p>
<img src="/images/pages/areas/westmead.jpg" alt="Wedding reception in a traditional marquee" width="198" height="150"/>
<p>Traditional and stretch tent at pretty Surrey wedding venue, Westmead Events</p>
</div>
<p>We offer a variety of <a href="/equipment/marquees.htm">marquees</a> in different styles and sizes to suit every occasion. Our bespoke service, practical and personal, allows us to adapt our tents to suit either huge front lawns or narrow back gardens with uneven surfaces. Whether you are hosting a small residential garden party or large wedding overlooking the South Downs, we have you covered.</p>
<p>Our Surrey representative will guide you through a list of options on how best to transform your space. During the free site visit, we will help you find a combination of marquee, furniture and equipment that compliments your square footage, budget and style.</p>    
<p>Our wonderful and professional team produces outstanding results and our marquees are designed to make any outdoor event a success. Click to read our <a href="/company/testimonials.htm">testimonials</a> or to view our <a href="/real.htm">gallery</a> to see examples of our recent work.</p>
<p>Information on pricing can be found on our <a href="/pricing/marquees.htm">prices page</a>. Alternatively, you can get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough idea on how much your ideal marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Fab Wedding Marquee</h2>
<img src="/images/areas/redhill.jpg" alt="Traditional style wedding tent" width="581" loading = "lazy"/>
<p class="wide">Streaming into a traditonal style wedding tent near Epsom</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Really did exceed our expectations</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Headley</span><span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Everything was delivered exactly as planned and looked stunning when fully erected. The finished result really did exceed our expectations. Your team were very thoughtful and courteous at all times and took extra special care with our garden — which was really appreciated.</p>
<p>Luckily we had great weather for both events although you certainly prepared for the worst — so again thanks for coming up to make sure everything was fully prepared for the second weekend... We would recommend you 100% — and your name will be top of the list for our next celebration.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Epsom</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39885.076472039276!2d-0.29790609252004224!3d51.33288055643576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875e158082bfa23%3A0x9136efc96d36d455!2sEpsom!5e0!3m2!1sen!2suk!4v1540844299266" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>