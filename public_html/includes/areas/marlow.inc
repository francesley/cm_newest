<p>For nearly thirty years, County Marquees has supplied quality marquees to the beautiful old Norman town of Marlow and the Buckinghamshire countryside surrounding it.</p>
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/bucks.jpg" alt="Dancing in a wedding marquee" width="198" height="150"/>
<p>Bride and groom in their Buckinghamshire wedding marquee</p>
</div>
<p>On a summer's day with the Thames glinting in the distance there can be no better venue for a country wedding than a marquee.  Have a look here at some of our <a href="/wedding-marquees.htm">wedding marquees</a>.  Marquees are also perfect for baby showers, that significant birthday, family garden parties and of course  corporate events.</p>
<p>We have a range of <a href="/equipment/marquees.htm">styles</a> for you to choose from: flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pagoda style Chinese Hat marquees.</p>
<p>Our brillant team are continuously being <a href="/company/testimonials.htm">praised</a> for their cheerful, friendly nature and outstanding results. Check out our <a href="/photos.htm">gallery</a> for examples of our work.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">rough estimate</a> of how much your dream marquee might cost. We offer a no-obligation, free site visit where our Buckinghamshire representative will measure your space and discuss the options for your site, numbers and budget.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hanging Greenery</h2>
<img src="/images/areas/marlow.jpg" alt="Hanging greenery in transparent marquee, Marlow" width="581" loading = "lazy"/>
<p class="wide">Stunning foliage hanging from the roof of a transparent wedding marquee in Marlow</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Marquee was stunning</span>
<abbr title="2012-09" class="dtreviewed hide">September 2012</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Father of the bride</span><span class="adr"><span class="locality">Marlow</span><span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>It was all perfect, thank you. The marquee was stunning in all its detail... Many thanks for a fantastic service from the very beginning. We will be sure to give glowing references.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/bucks.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Buckinghamshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Marlow area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19838.16301265513!2d-0.794860717224396!3d51.57244289732186!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487661882e969811%3A0xb25284f05eccc5c2!2sMarlow!5e0!3m2!1sen!2suk!4v1550019078776" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>