<p>County Marquees provides beautifully finished marquees for weddings, parties and corporate events to Alton and the surrounding Hampshire area.</p> 
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small Colindale garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>We have been putting up Marquees in the Alton area for nearly 30 years.  We specialise in marquees for all types of events, no matter the size. The modular nature of our framed marquees allow us to provide a perfect venue even in difficult spaces, customising them to your needs. Whether your space is on uneven ground, overlapped by trees or simply very large our expert team will be on hand to help.</p>
<p>We offer a free site visit where our Hampshire representative will measure your available space and give advice on size and layout. We will then help you create your own bespoke package choosing equipment, furniture and accessories from our portfolio tailored to your taste.</p> 
<p>Our team is reliable and regularly <a href="/company/testimonials.htm">complimented</a> on their professionalism, efficiency and good nature. See our <a href="/real.htm">real examples gallery</a> to view some of our recent work.
<p>For information on pricing, please see our <a href="/pricing/marquees.htm">prices page</a> or to get a <a href="/pricing/quotation.htm">quick online quote</a>. 
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more or to arrange a free site visit.</p>
<h2>Hertfordshire wedding</h2>
<img src="/images/areas/milton_keynes.jpg" alt="Romantically styled wedding reception" width="581" loading = "lazy"/>
<p class="wide">A romantically styled wedding reception inside a traditional marquee with wooden poles.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Your enthusiastic team sorted everything out beautifully</span>
<abbr title="2001-01" class="dtreviewed hide">January 2001</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Father of the bride</span><span class="adr"><span class="locality">Petersfield</span><span class="region">Hampshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for so efficiently providing the marquee and tent arrangements for our daughter's wedding. I am afraid that our tiered terrace and the generally lopsided nature of our garden posed some unique technical problems. But you and your enthusiastic team sorted everything out beautifully.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hampshire</h3>
<p class="venues">We maintain a list of <a href="/venues/hants.htm">marquee venues in Hampshire</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees South</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<div class="region">West Sussex</div>
<span class="postal-code">PO20 7EB</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Alton area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20021.82495645953!2d-0.99183299439081!3d51.150364592977205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4874232c17a9a3cb%3A0x84cc6dd2260e40b9!2sAlton!5e0!3m2!1sen!2suk!4v1543936986098" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>