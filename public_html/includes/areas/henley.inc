<p>For over 25 years, County Marquee has supplied professional marquees to Henley-on-Thames and the surrounding Oxfordshire area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/attached.jpg" alt="Flowery interior for a winter party" width="198" height="150"/>
<p>Flowery interior for a winter party</p>
<img src="/images/pages/areas/attached2.jpg" alt="Frame marquee attached to a house" width="198" height="150"/>
<p>Frame marquee attached to a house</p>
</div>
<p>It is a great English tradition to celebrate in the garden.  Only problem is the English weather.  Solution - a beautiful marquee.  We have a wide range of marquee types and styles suited to gardens of all sizes and shapes.   The flexible framed marquees, the traditional style marquees with guy ropes and centre poles, the informal New World stretch tents and the Eastern influenced Chinese Hats.  We also have a range of furniture and diverse ancillary equipment for you to make the venue the way you want it. Our Oxfordshire representative will help you through the selection process and advise you on all aspects of marquee hire and generally ensure that getting the right marquee is as painless as possible.</p>
<p>We take pride in providing a quality, efficient service. Our <a href="/company/testimonials.htm">customer testimonials</a> bear witness to the high standards we always aim to match. Check out our <a href="/photos.htm">photo gallery</a> to see our recent work or get inspiration for your next event.</p>
<p>Information on pricing is displayed on our <a href="/pricing/marquees.htm">pricing page</a>, alternatively you can get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous traditional marquee at night</h2>
<img src="/images/areas/oxfordshire.jpg" alt="Illuminated traditional style marquee" width="581" loading = "lazy"/>
<p class="wide">Illuminated traditional style marquee in a large Oxfordshire garden.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">The marquee looked wonderful!</span>
<abbr title="2012-12" class="dtreviewed hide">December 2012</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Groom</span><span class="adr"><span class="locality">Abingdon</span><span class="region">Oxfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">All went very well thank you. The marquee looked wonderful! Thanks to Martin and his hard working team; they were very professional but also very helpful (even though it was dark and getting late on the evening of set-up) with arranging the marquee and hanging our lanterns just the way we had envisaged.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Oxfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/oxford.htm">marquee venues in Oxfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Oxfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Henley-on-Thames area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9927.581044410914!2d-0.9113041385079435!3d51.533480877494576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48768542ad0cb2ab%3A0x50b007c5953c9c7d!2sHenley-on-Thames!5e0!3m2!1sen!2suk!4v1543423874435" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>