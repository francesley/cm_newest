<p>County Marquees have been supplying perfectly finished marquees for weddings, parties and events to Harrow and the North London area for nearly 30 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/harrow.jpg" alt="Baby shower tea time marquee" width="198" height="150"/>
<p>Baby shower marquee in Harrow</p>
<img src="/images/pages/areas/whetstone.jpg" alt="Wedding reception marquee" width="198" height="150"/>
<p>Wedding reception marquee in long Whetstone garden</p>
</div>
<p>We supply marquees in a variety of styles and sizes to suit any occasion. From flexible frame structures, romantic traditional style poled marquees, through to stretch tents, bell tents and pointy topped Chinese Hat marquees. Whether you are planning a barbecue, a birthday party or corporate event, our Harrow representative will guide you through our portolio of furniture and equipment and help you decide on a combination which best suits your space, taste and budget.</p> 
<p>Our fantastic team are regularly <a href="/company/testimonials.htm">praised</a> for their effiency, good nature and outstanding results: you can see some <a href="/real.htm">real examples here</a>.</p>
<p>Our <a href="/pricing/marquees.htm">prices</a> are available online with no hidden fees. You can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough idea of how much your dream marquee might cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Harrow Wedding Marquee</h2>
<img src="/images/areas/harrow.jpg" alt="Wedding marquee with mandap in Harrow" width="581" loading = "lazy"/>
<p class="wide">A glamorous marquee wedding with a mandap in Harrow</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Harrow area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39659.3151937733!2d-0.36812227789392377!3d51.59192949007347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761158a02507f9%3A0x24d74f8999e25bd9!2sHarrow!5e0!3m2!1sen!2suk!4v1540209714786" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>