<p>County Marquees is a friendly family-run company with over 25 years of experience supplying marquees for weddings, birthdays and events in Dorking and the Surrey area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/redhill.jpg" alt="Wedding reception in a traditional marquee" width="198" height="150"/>
<p>Surrey wedding reception in a traditional marquee</p>
<img src="/images/pages/areas/westmead.jpg" alt="Traditional marquee and stretch tent" width="198" height="150"/>
<p>Traditional and stretch tent for a wedding at pretty wedding venue, Westmead Events</p>
</div>
<p>We stock a variety of styles and sizes, from frame and traditional marquees, pagoda style Chinese hat marquees through to bell and stretch tents. Every booking, be it a small garden party or sizeable wedding, comes with a dedicated representative to oversee your special occasion from start to finish. We offer a free site visit where a member of our team will assess your site and advise you on the best combination of marquee, furniture and equipment to suit your style, square footage and budget.</p>
<p>Our fantastic team is consistently <a href="/company/testimonials.htm">praised</a> for their good nature, efficiency and outstanding results. Check out our <a href="/real.htm">real examples</a> page to see recent marquees dressed for a range of different functions.</p>
<p>For a clear list of prices, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims provide you with a rough estimate of how much your ideal marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Wedding in Dorking</h2>
<img src="/images/areas/dorking.jpg" alt="Traditional style wedding tent in a sheep field" width="581" loading = "lazy"/>
<p class="wide">A traditional style wedding tent in a sheep field near Dorking, before the event with the morning sun shining through.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Absolutely delighted by the final image</span>
<abbr title="2018-08" class="dtreviewed hide">August 2018</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">Dorking</span><span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Jim and I (and my mum!) worked with Shaun to turn a sheep field into the most amazing venue for our wedding. It is literally a sheep field with no power, no water, nothing. But Shaun listened to what we wanted and came up with some brilliant suggestions.</p>
<p class="noborder">We’d chosen a traditional marquee which meant we had the beautiful wooden poles in the middle and were able to remove two whole sides, allowing the breeze in and for our guests to be able to see the view.</p>
<p class="noborder">When the marquee went up, Tom and his team were very professional and humoured my endless questions. 
We wanted rectangular tables that seat 10 and, although they don’t usually stock these, Shaun sourced from elsewhere for us. They also put up all of the bunting my mum had made, as well as sorting out stunning lighting to complete the look.</p>
<p>We were both absolutely delighted by the final image &ndash; it was exactly what we had wanted and looked stunning. Thank you so much Shaun, Tom and the rest of the team.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Dorking area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39978.706192543854!2d-0.3642167308246557!3d51.22517042487565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875c214f77a2a7d%3A0xf3af2964b0e53ced!2sDorking!5e0!3m2!1sen!2suk!4v1540755991966" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>