<p>Since our foundation in 1989, County Marquees have been providing a professional marquee hire service to Berkshire. From parties and weddings to corporate functions and themed events, we aim to provide you with the perfect venue to enjoy with your family, friends and or clients.</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small Colindale garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>If you are looking for a versatile, flexible venue to host an event, hiring a marquee can give you everything you want. With the ability to choose the venue's shape, size and and style of decor, our bespoke service allows you to create a truly unique space.</p> 
<p>From your first contact via the phone or by email, to the following free site visit and then through to the big day our Berkshire representative will work alongside you to ensure your event is one to cherish. We’re passionate about helping you to enjoy your party and we will always go the extra mile to ensure your needs are met.</p>
<p>Our friendly and professional team work tirelessly to ensure that our marquees not only meet your expectations, but exceed them. We have a large number of <a href="/company/testimonials.htm">customer testimonials</a> which detail our hard work.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or to get a rough estimate of how much your dream marquee may cost, get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hampstead Garden Suburb Wedding</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden on Winnington Road. Gardens in Hampstead Garden Suburb come in interesting shapes and we often put up complicated marquee layouts to accomodate.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Berkshire</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in Berkshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Berkshire</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Road</div>
<div class="locality">Guildford</div>
<span class="postal-code">GU1 3QP</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Berkshire area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d318243.47245892475!2d-1.3192507627691024!3d51.45306811684383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48769bd8c5af65a3%3A0xf3d97f73063f8d6d!2sBerkshire!5e0!3m2!1sen!2suk!4v1543937440809" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>