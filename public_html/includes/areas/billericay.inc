<p>County Marquees is an independent, family run company which supplies beautifully finished marquees to the ancient town of Billericay and the surrounding Essex countryside.</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small Colindale garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>From Christmas family meals to christenings and birthdays to barmitzvahs, weddings and anniversaries, marquees provide a versatile, friendly and flexible venue for hosting that special event. The modular nature of our framed marquees allow us to cater for difficult spaces, customising them to your needs perfectly. Whether your space is on uneven ground, a narrow garden or a very large field, our expert and experienced team will be on hand to help.</p>
<p>The interior of the marquee is one of the most important aspects of the structure, helping create a truly spectacular effect. During the initial site visit, our Essex representative will guide you through our portfolio of furniture and accessories, working alongside you to create a beautiful bespoke look reflecting  your style and taste.</p> 
<p>Our friendly and professional team work hard to ensure that our marquees not only meet your expectations, but exceed them. To see what our customers had to say about us, check out our <a href="/company/testimonials.htm">customer testimonials</a> or view our <a href="/photos.htm">gallery</a> for images of our recent work.
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a>.
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more or to arrange a free site visit.</p>
<h2>Hampstead Garden Suburb Wedding</h2>
<img src="/images/areas/hgs.jpg" alt="Marquee " width="581" loading = "lazy"/>
<p class="wide">A wedding marquee winding round the terrace of a large garden on Winnington Road. Gardens in Hampstead Garden Suburb come in interesting shapes and we often put up complicated marquee layouts to accomodate.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London and the surrounding counties</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Essex</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Billericay area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39634.2278302819!2d0.3872008497602171!3d51.620658491248705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8bf562a9a7eb1%3A0xd7a680051cc4551e!2sBillericay!5e0!3m2!1sen!2suk!4v1543937188771" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>