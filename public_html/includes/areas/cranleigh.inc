<p>County Marquees is a long established family run firm that has been serving the Cranleigh area from its Guildford office for more than 25 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/littlefield.jpg" alt="Traditional style tent at Littlefield Manor" width="198" height="150"/>
<p>Traditional style tent at Littlefield Manor, near Guildford</p>
<img src="/images/pages/areas/littlefield_manor.jpg" alt="Wedding reception interior" width="198" height="150"/>
<p>Wedding reception interior</p>
</div>
<p>We stock a a wide variety of luxury <a href="/equipment/marquees.htm">marquees</a>, furniture and ancillary equipment. Our professional and experienced team care passionately about the quality of their work and are regularly <a href="/company/testimonials.htm">complimented</a> for their helpfulness and outstanding results. During your free site visit, our Surrey representative Shaun Haworth will measure and assess your site and offer advice for how best to combine your ideas and budget.</p>
<p>We take a lot of pride in putting up beautiful, carefully finished marquees. Check out our <a href="/photos.htm">gallery</a> to see images of dressed marquees or to get inspiration for your next event.</p>
<p>We have a detailed <a href="/pricing/marquees.htm">price guide</a> on this website and there are no hidden extras. You can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough idea of how much your dream marquee might cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Surrey Wedding</h2>
<img src="/images/areas/littlefield_manor.jpg" alt="Wedding marquee at Littlefield Manor" width="581" loading = "lazy"/>
<p class="wide">Traditional style wedding tent at Littlefield Manor, a pretty marquee venue three miles outside Guildford</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Interior looked spectacular</span>
<abbr title="2003-12" class="dtreviewed hide">December 2003</abbr>  <span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Mother of the bride</span><span class="adr"><span class="locality">Guildford</span> <span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I can't tell you how pleased we were with the marquee in every respect. Your team of erectors were hard working and fun, and the interior looked truly spectacular when completed.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Marquee looked absolutely stunning</span>
<abbr title="2010-05" class="dtreviewed hide">May 2010</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Groom</span><span class="adr"><span class="locality">Guildford</span><span class="region">Surrey</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>The marquee looked absolutely stunning and the dance floor, bar and chill out zone were brilliant. Once again thank you so much for all your help.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Surrey</h3>
<p class="venues">We maintain a list of <a href="/venues/surrey.htm">marquee venues in Surrey</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Surrey</div>
 <div class="adr">
<div class="street-address">Addison House, Addison Rd</div>
<div class="locality">Guildford</div>
<div class="region different">Surrey</div> <div class="postal-code">GU1 3QP</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441483538617"><span class="tel">01483 538617</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>

<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>Cranleigh area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20025.908767000397!2d-0.49910789680633205!3d51.14095110046725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875c48d98597fa5%3A0x99588df8c05a8c18!2sCranleigh!5e0!3m2!1sen!2suk!4v1540045232151" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>