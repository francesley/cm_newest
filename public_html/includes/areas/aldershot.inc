<p>County Marquees have been supplying perfectly finished marquees to Aldershot and around for nearly 30 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/hendon.jpg" alt="Marquee at Kenwood House, Highgate" width="198" height="150"/>
<p>Minimalist, leafy style in a small Colindale garden</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>We are a professional, friendly company with a wide range of stock and long experience of the Aldershot area.  We can provide the venue and the backdrop for all kinds of events &ndash; from romantic, traditional style tents for weddings to large framed structures that can accommodate different internal spaces and a wide variety of features for large corporate events or parties.  We have Bell tents for a festival style event and stretch tents for extra flexibility and wow factor in the summer.</p>
<p>We also have a large range of marquee accessories including furniture, lighting, heating and  different types of flooring so you can personalise your event to suit your style and requirements.</p>
<p>A free site visit comes as standard, where our Hampshire representative will assess your space and talk you through what type of tent and extras will best fit in with your ideas and budget.</p>
<p>Our marquees look <a href="/photos.htm">visually stunning</a> and our amazing team has a <a href="/company/testimonials.htm">reputation</a> for being cheerful and efficient.</p> 
<p>For guidance on pricing &ndash;see our <a href="/pricing/marquees.htm">pricing page</a>. Or for a rough estimate, get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more or to arrange a no-obligation free site visit.</p>
<h2>Party marquee at dusk</h2>
<img src="/images/areas/petersfield.jpg" alt="wedding marquee at dusk" width="581" loading = "lazy"/>
<p class="wide">A A frame tent with transparent windows for a party near Petersfield</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Your enthusiastic team sorted everything out beautifully</span>
<abbr title="2001-01" class="dtreviewed hide">January 2001</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Petersfield</span><span class="region">Hampshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you very much for so efficiently providing the marquee and tent arrangements for our daughter's wedding. I am afraid that our tiered terrace and the generally lopsided nature of our garden posed some unique technical problems. But you and your enthusiastic team sorted everything out beautifully.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hampshire</h3>
<p class="venues">We maintain a list of <a href="/venues/hants.htm">marquee venues in Hampshire</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees South</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<div class="region">West Sussex</div>
<span class="postal-code">PO20 7EB</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Aldershot area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39954.59144374996!2d-0.7936864501249036!3d51.252927175975174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48742b4d2f9cb7cd%3A0x19ae01604385ba3c!2sAldershot!5e0!3m2!1sen!2suk!4v1543936951497" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>