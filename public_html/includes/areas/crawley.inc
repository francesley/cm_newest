<p>County Marquees is a long established family run marquee company that has been serving Crawley and the West Sussex area  for nearly 30 years. </p>
<div class="textPhotos">
<img src="/images/pages/areas/chichester.jpg" alt="Wedding tent with open sides" width="198" height="150"/>
<p>Wedding tent with open sides</p>
<img src="/images/pages/areas/buckhurst_park.jpg" alt="Traditional marquee at Buckhurst Park" width="198" height="150"/>
<p>Traditional marquee at Buckhurst Park, East Sussex wedding venue</p>
</div>
<p>We offer a <a href="/equipment/marquees.htm">range of marquees in a variety of sizes</a>: from frame marquees and traditional style tents to Chinese hats, stretch tents and bell tents. In other words, we can deal with most event spaces from large and grand tents for sizeable gardens to tents that can be adapted to fit narrow gardens with uneven ground and protruding shrubbery.</p>
<p>Whatever the event, we pride ourself on an exceptionally high standard of marquee, furniture and equipment and a thoroughly professional service. Shaun Haworth, our charming and helpful Sussex representative will help you decide on a combination of marquee, furniture and equipment that best matches your venue, budget and vision. And then our brilliant team will pay close attention to detail and work with you to create the perfect setting for your special occasion.</p>
<p>Check out our <a href="/company/testimonials.htm">customer testimonials</a> to see what we mean. Or look at our <a href="/photos.htm">marquee gallery</a> for inspirational examples of our recent work.</p> 
<p>A full list of <a href="/pricing/marquees.htm">prices</a> is available with no hidden extra costs. You can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you an estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Sussex Wedding</h2>
<img src="/images/areas/chichester.jpg" alt="Traditional style tent for a wedding reception just outside Chichester" width="581" loading = "lazy"/>
<p class="wide">Traditional style tent for a wedding reception just outside Chichester</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">My family couldn't speak more highly of your service</span>
<abbr title="2013-07" class="dtreviewed hide">July 2013</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">Crawley</span><span class="region">Sussex</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Sussex</a><div class="tel">01243 790290</div><div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I wanted to email and thank you and your team for the wonderful service we received for our wedding. We couldn't have been happier with everything you provided for our wedding day. After using County Marquees a few times now as you know my family couldn't speak more highly of your service. Tom and the team who came and delivered everything were amazing and just made it all so easy, in what is a normally stressful time. Thank you for everything. Hopefully we have another party in the not too distant future so we can use you again.</p>
</blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in West Sussex</h3>
<p class="venues">We maintain a list of <a href="/venues/wSussex.htm">marquee venues in West Sussex</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees West Sussex</div>
 <div class="adr">
<div class="street-address">Rock House Dell Quay Road</div>
<div class="locality">West Sussex</div>
<span class="postal-code">PO20 7EB</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+441243 790390"><span class="tel">01243 790390</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Crawley area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d80140.45944756958!2d-0.2530767566598859!3d51.11972578206246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4875ed930f13ff35%3A0x12f9939e52364adb!2sCrawley!5e0!3m2!1sen!2suk!4v1543331444919" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>