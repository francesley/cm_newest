<p>County Marquees is a family run business which has been supplying professional marquees to Milton Keynes for nearly 30 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/bucks.jpg" alt="Blacked out party tent" width="198" height="150"/>
<p>Bride and groom in their Buckinghamshire wedding marquee</p>
</div>
<p>We provide a range of <a href="/equipment/marquees.htm">marquees</a>: from flexible frame structures and romantic traditional style poled marquees, through to stretch tents, bell tents and pointy topped Chinese Hat marquees. We also stock a great choice of furniture, flooring and equipment.</p>
<p>During your initial free site visit, our friendly Buckinghamshire representative will discuss with you the combination of tent, flooring, furniture and equipment which best suits your needs, and will provide advice on how to transform your outdoor space.</p>
<p>Our team is regularly <a href="/company/testimonials.htm">complimented</a> on their good nature, helpfulness and tidiness. We are incredibly proud of the work we do and the results we produce. For examples of our work, check out our <a href="/photos.htm">gallery</a>.</p>
<p>For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Milton Keynes wedding</h2>
<img src="/images/areas/milton_keynes.jpg" alt="Romantically styled wedding reception" width="581" loading = "lazy"/>
<p class="wide">A romantically styled wedding reception inside a traditional marquee with wooden poles.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Excellent service</span>
<abbr title="2018-05" class="dtreviewed hide">May 2018</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Father of the bride</span><span class="adr"><span class="locality">Weston Underwood</span><span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">County Marquees gave an excellent service for our daughter’s wedding in Buckinghamshire. From the quote through to putting the marquee up they were so professional, the team were so efficient and the marquee looked amazing. They went the extra mile – many thanks.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/bucks.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Buckinghamshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Milton Keynes area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19634.27345184553!2d-0.7751536990366075!3d52.03813695244175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48764cf882238685%3A0x161a9df74cb97e14!2sMilton+Keynes!5e0!3m2!1sen!2suk!4v1550019266343" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>