<p>County Marquees is an independent, family run business which has supplied a wide range of  marquees to the always so beautiful City of Oxford and the surrounding areas for nearly 30 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/attached.jpg" alt="Flowery interior for a winter party" width="198" height="150"/>
<p>Flowery interior for a winter party</p>
<img src="/images/pages/areas/attached2.jpg" alt="Frame marquee attached to a house" width="198" height="150"/>
<p>Frame marquee attached to a house</p>
</div>
<p>Marquees can be modern looking, or traditional looking, or basic, luxurious, functional or decorative.</p>
<p>At County Marquees we have a range of <a href="/equipment/marquees.htm">tent styles and sizes</a> to suit almost every venue and occasions: large frame marquees on the college lawn for graduation ceremonies, traditional style poled marquees for the rustic festival style wedding,  modern informal stretch tents, bell tents and pagoda style Chinese Hat marquees. In other words, we have them all!</p>
<p>We stock an extensive range of furniture, various types of flooring, many types of lighting equipment and diverse accessories. Whether you are organising a small garden get-together or a College Ball, our Oxfordshire representative will take you through the selection process and advise on a combination which best suits your site, style and budget.</p>
<p>We take pride in providing our customers with good quality marquees and our aim is to exceed your expectations. Our team is frequently <a href="/company/testimonials.htm">complimented</a> on their good nature, tidiness and fantastic results. View our photo gallery to see <a href="/photos.htm">real examples</a> of dressed marquees and to get inspiration for your next event.</p>
<p>Information regarding pricing is available on our <a href="/pricing/marquees.htm">pricing page</a>. Or to get a rough estimate of how much your dream marquee may cost, get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous traditional marquee at night</h2>
<img src="/images/areas/oxfordshire.jpg" alt="Illuminated traditional style marquee" width="581" loading = "lazy"/>
<p class="wide">Illuminated traditional style marquee in a large Oxfordshire garden.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Your employees are great to work with</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">College anonymous</span><span class="adr"><span class="locality">An Oxford College</span><span class="region">Oxfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you to you and your team for the marquee over the weekend – it was assembled very speedily and removed today in a very efficient manner as well. Your employees are great to work with and very helpful.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Oxfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/oxford.htm">marquee venues in Oxfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Oxford</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Oxford area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d79041.6241073927!2d-1.3176277956723954!3d51.750395486787234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48713380adc41faf%3A0xc820dba8cb547402!2sOxford!5e0!3m2!1sen!2suk!4v1543423927522" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>