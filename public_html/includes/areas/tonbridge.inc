<p>County Marquees is a long-established, family-run business servicing towns all over Kent, including Tonbridge.</p>
<div class="textPhotos">
<img src="/images/pages/areas/kent_winter.jpg" alt="A Kentish winter marquee interior width="198" height="150"/>
<p>Winter marquee interior</p>
<img src="/images/pages/areas/kent.jpg" alt="Traditional style tent" width="198" height="150"/>
<p>Traditional style tent</p>
<img src="/images/pages/areas/stretch.jpg" alt="Stretch tent in a walled garden" width="198" height="150"/>
<p>Stretch tent in a walled garden</p>
</div>
<p>Tonbridge, a historic market town, is set on the banks of the River Medway, and boasts several spectacular venues which can  be hired for an event. Penshurst Place is one such &ndash; the historic stately home, complete with houses, gardens, and a maze, can be used as a wedding venue.</p>
<p>We supply a large variety of tents: flexible frame structures, romantic traditional style poled marquees, stretch tents, bell tents and pointy topped Chinese Hat marquees. Our tailored services allows us to adapt our tents to accommodate to the narrowest of gardens with uneven surfaces and incorporate natural structures such as trees and flowerbeds.</p>
<p>Our team is regularly complimented on their cheerfulness and fantastic results. We are incredibly proud of our <a href="/company/testimonials.htm">customer testimonials</a>. Check out our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p>
<p>We offer a free site visit where our South London representative will measure your space and discuss with you how best to turn your ideas into reality taking into account space, numbers and budget. For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous clear marquee</h2>
<img src="/images/areas/clear_marquee.jpg" alt="Clear marquee in Tunbridge Wells, Kent" width="581" height="410"/>
<p class="wide">A ravishing clear marquee with dramatic lighting in Tunbridge Wells, Kent.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2018-08" class="dtreviewed hide">August 2018</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Tonbridge</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description" style="clear:both"><p class="noborder">Thank you for a fantastic marquee and fantastic service.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recomendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Tonbridge area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39999.54925234659!2d0.24141274481727854!3d51.201170773859!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df2536ce10e873%3A0xf625ebb2b8e7ea7e!2sTonbridge!5e0!3m2!1sen!2suk!4v1543939290752" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>