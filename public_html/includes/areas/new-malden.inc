<p>County Marquees is a family run business with over 25 years of experience working with clients in New Malden and the surrounding South London area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/streatham2.jpg" alt="Sitting round a firepit with a lit up traditional tent in the background" width="198" height="150"/>
<p>Round a firepit outside an illuminated wedding tent in a Streatham garden</p>
<img src="/images/pages/areas/barnes.jpg" alt="Wedding marquee with opulent floral ring hanging from the roof, Barnes" width="198" height="150"/>
<p>Barnes wedding marquee with opulent hanging floral ring</p>
</div>
<p>We offer beautifully finished marquees that can transform an outdoor space. With a large variety of styles and sizes of tents, we offer a free site visit where our South London representative will help you make sense of the options and how they could work with your event space, budget and dream. From the furniture to the flooring, we can help you decide on all the different aspects of your marquee and help you transform your outdoor space.</p>
<p>Our professional team are regularly <a href="/company/testimonials.htm">complimented</a> for their cheerfulness, efficiency and outstanding results. Check out our <a href="/real.htm">real examples</a> page to see images of dressed marquees or to get inspiration for your next event.</p>
<p>See our <a href="/pricing/marquees.htm">prices</a> page or get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough estimate of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Streatham Wedding</h2>
<img src="/images/areas/streatham.jpg" alt="Marquee with fairy light canopy in Streatham" width="581" loading = "lazy"/>
<p class="wide">&#8220;Cheers&#8220; under a fairy light canopy in a Streatham wedding marquee. Photo courtesy of <a href="https://eleanorjoyphotography.co.uk/streatham-wedding-photographer/" rel="noopener noreferrer" target="_blank">Eleanor Joy Photography</a>.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Sincerely appreciated</span>
<abbr title="2010-06" class="dtreviewed hide">June 2010</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Kingston</span><span class="region">South London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Surrey</a><div class="tel">01483 538617</div>
<div class="adr"><div class="street-address">Addison House, Addison Road</div><span class="locality">Guildford</span><span class="region">Surrey</span><span class="postal-code">GU1 3QP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>I wanted to drop you a quick note to thank you and to say what a pleasure it was dealing with you. It was the first time we used a marquee for any event, and your prompt and efficient handling of all my various queries and changes over the last month or two was sincerely appreciated. I think everybody who came to the party was genuinely impressed, so thank you for your help and support throughout.</p></blockquote></div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the New Malden area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d159306.80755205813!2d-0.39648667937493903!3d51.3999517955158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487608e2a7b1e37d%3A0x82a1c1d25d05995e!2sNew%20Malden!5e0!3m2!1sen!2suk!4v1677586036064!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>