<p>For nearly 30 years, County Marquees has provided a high quality and personalised marquee service to Welwyn Garden City and the surrounding Hertfordshire.</p>
<div class="textPhotos">
<img src="/images/pages/areas/farnborough.jpg" alt="Wedding tent in Farnborough" width="198" height="150"/>
<p>Farnborough wedding with bunting and burlap table runners</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>Marquees provide a practical and impressive venue option when organising an special occasion. We tailor our marquees to suit any occasion. Whether you are hosting a garden party or something a little more special be it a wedding or birthday, we have it covered. We offer a free site visit where our Hertfordshire representative will measure your available space, advise on size and layouts. We will then guide you through our extensive portfolio of furniture, equipment and accessories helping you create a bespoke package to suit you space, style and budget.</p>
<p>Our team is regularly <a href="/company/testimonials.htm">praised</a> on their good nature, tidiness and fantastic results. Our <a href="/real.htm">real examples gallery</a> showcases some of our previous work and offers decorative inspiration for your next event.</p>
<p>For information on prices, see our <a href="/pricing/marquees.htm">pricing page</a>. Alternatively, to get a rough idea of costs, get a <a href="/pricing/quotation.htm">quick online quote</a>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Corporate event at Minley Manor</h2>
<img src="/images/areas/farnborough.jpg" alt="Tent for a corporate event" width="581" loading = "lazy"/>
<p class="wide">Marquee for a corporate party spanning several levels at event venue Minley Manor in Farnborough. The party was pirate themed, and you can just see a couple of pirate galleon balloons.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2008-11" class="dtreviewed hide">November 2008</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Harrow</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">I can't thank you and your team enough for the work they put in to make our marquee special. They are a fantastic bunch of lads and they are a credit to you and your business.</p>
 <p>I certainly will be using your company again and will recommend anyone who may be thinking of having a marquee to contact you.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Harrow area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39659.3151937733!2d-0.36812227789392377!3d51.59192949007347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761158a02507f9%3A0x24d74f8999e25bd9!2sHarrow!5e0!3m2!1sen!2suk!4v1540209714786" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>