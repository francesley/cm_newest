<p>Since its inception in 1989, County Marquees has been providing the Kent area with &ndash; you guessed it &ndash; marquees, as well as everything needed for a fun, memorable event.</p>
<div class="textPhotos">
<img src="/images/pages/areas/kent_winter.jpg" alt="A Kentish winter marquee interior width="198" height="150"/>
<p>Winter marquee interior</p>
<img src="/images/pages/areas/kent.jpg" alt="Traditional style tent" width="198" height="150"/>
<p>Traditional style tent</p>
</div>
<p>Whether you're crafting the wedding of your dreams, wish to enter your golden years in style, or just want to throw a dang good English-style party, County Marquees has got you covered, both theoretically and literally.</p> 
<p>We have frame marquees, traditional marquees, chinese hat marquees, stretch tents, and bell tents, all of which can be fitted to your specific tastes and themes. Indian weddings, university balls, eighteenth birthdays, and classic English weddings are just some of the events that County Marquees can cater for.</p> 
<p>Our services are not limited to marquees, though; we also have a large range of furniture, equipment and lighting &ndash; with plenty of table and seating types, fairy lights, festoon lights, chandeliers and candelabras along with other essential event accessories. See <a href="/equipment/decoration.htm">marquee decoration</a> for examples of how lighting and accessories can come together to create stunning marquee interiors.</p> 
<p>Our <a href="/pricing/marquees.htm">pricing pages</a> will give you an idea of how much your event might cost. You can also get an immediate no obligation <a href="/pricing/quote.htm">online quote</a> now.</p> 
<p>Our billiant team is frequently <a href="/company/testimonials.htm">complimented</a> on their good nature and hard work. Check our our <a href="/photos.htm">gallery</a> see some of our previous work, or see our <a href="/real.htm">real examples</a> page to get inspiration for your next event.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous clear marquee</h2>
<img src="/images/areas/clear_marquee.jpg" alt="Clear marquee in Tunbridge Wells, Kent" width="581" height="294"/>
<p class="wide">A ravishing clear marquee with dramatic lighting in Tunbridge Wells, Kent.</p>
<h2>Local praise</h2>
<div class="hreview">
<span class="summary hide">Pristine and fabulous</span>
<abbr title="2015-05" class="dtreviewed hide">May 2015</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Hawkhurst</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Hawkhurst</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description">
 <p>We'd like to thank you and the whole team at County Marquees for a fantastic job. The marquee looked pristine and fabulous, everything was delivered and set up as ordered, and your lovely team went out of their way to be helpful. We had a wonderful day, and a big part of its success was the marquee. I wouldn't hesitate to recommend you to anyone else planning to set up a wedding marquee in the garden.</p></blockquote>
</div>
<div class="hreview">
<span class="summary hide">Such a cool space</span>
<abbr title="2018-08" class="dtreviewed hide">August 2018</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Tunbridge Wells</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description">
 <p>What a great event &ndash; we loved every minute of it &ndash; and the marquee was brilliant.  Thanks to you and all the team who created such a cool space.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recomendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover Kent</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d320031.88408698956!2d0.46247048248698264!3d51.19606162004758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a815e2b18297%3A0x9251e76476201559!2sKent!5e0!3m2!1sen!2suk!4v1544397095100" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>