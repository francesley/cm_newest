<p>County Marquees is a friendly, family run company which supplies beautiful finished marquees to Walton-on-Thames and the surrounding Surrey area.</p>
<div class="textPhotos">
<img src="/images/pages/areas/thakeham.jpg" alt="Frame marquee for a party in in Thakeham" width="198" height="150"/>
<p>Frame marquee for a party in in Thakeham</p>
<img src="/images/pages/areas/sloping.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Marquee on levelled ground, Golders Green</p>
</div>
<p>From flexible frame structures, romantic traditional style poled marquees, to stretch tents, bell tents and pointy topped Chinese Hat marquees, we have a lot of kit for you to choose from. Whether you are organising a small garden party or large corporate event, our Surrey representative will help you find the combination of marquee, furniture, equipment and accessories which best suits your sqaure footage, budget and style.</p>
<p>We are incredibly proud of our team and the fantastic results they produce. Check out our <a href="/company/testimonials.htm">testimonials</a> to see what our customers have said about us. View our <a href="/photos.htm">gallery</a> to see examples of our recent work.</p> 
<p>For more information on our prices, please see our <a href="/pricing/marquees.htm">pricing list</a> or to get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough estimate of how much your ideal marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Impressive marquee</span>
<abbr title="2004-06" class="dtreviewed hide">June 2004</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Petersfield</span><span class="region">Hampshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">01243 790290</div>
<div class="adr"><div class="street-address">Rock House, Dell Quay</div><span class="locality">Chichester</span><span class="region">West Sussex</span><span class="postal-code">PO20 7EB</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Many thanks for providing such an impressive marquee. Thank you also for supplying the outside lights at such short notice. The extra night sky made all the difference.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hampshire</h3>
<p class="venues">We maintain a list of <a href="/venues/hants.htm">marquee venues in Hampshire</a> for customers' convenience.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees South</div>
 <div class="adr">
<div class="street-address">Rock House, Dell Quay</div>
<div class="locality">Chichester</div>
<div class="region">West Sussex</div>
<span class="postal-code">PO20 7EB</span>
</div>
<div>Telephone: <a href="tel:+441243790290"><span class="tel">01243 790290</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Walton-on-Thames</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19917.527415415694!2d-0.42007236596682274!3d51.39035827522746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760ad5522c3c67%3A0x48bd10007520cad9!2sWalton-on-Thames!5e0!3m2!1sen!2suk!4v1540882840654" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>