<p>County Marquees specialises in marquee hire for weddings, parties and all types of outdoor occasions, supplying to Baldock, Hitchin, Letchworth and Stotfold and the surrounding Hertfordshire areas.</p>
<div class="textPhotos">
<img src="/images/pages/areas/vintage_tables.jpg" alt="Vintage style tables" width="198" height="150"/>
<p>Antiqued trestle tables are a great look</p>
<img src="/images/pages/areas/bunting.jpg" alt="Frame marquee on levelled off ground" width="198" height="150"/>
<p>Rustic style with bunting</p>
</div>
<p>We have a special affection for the beautiful North Herts countryside and the old market town of Baldock in particular (the Hertfordshire Baghdad as the Knights Templar christened it).</p>
<p>Whether you are planning a small birthday party or the <a href="/wedding-marquees.htm">wedding of your dreams</a>, we believe we have everything you need in order to make your outdoor occasion truly memorable. We offer a large variety of tents in a range of styles and sizes: flexible frame structures, romantic pole tents, stretch tents, bell tents and pointy topped Chinese Hat marquees.</p>
<p>We are dedicated to the satisfaction of our customers who regularly <a href="/company/testimonials.htm">compliment</a> our wonderful team for their cheerfulness and fantastic results. We offer a free site visit where our Hertfordshire representative will discuss and advise you on how to best turn your vision into praactical reality.</p>
<p>Check out our <a href="/photos.htm">gallery</a> to see examples of our recent work. For pricing, see our <a href="/pricing/marquees.htm">pricing page</a>, or get a <a href="/pricing/quotation.htm">quick online quote</a>.
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Fab Hertfordshire Wedding Marquee</h2>
<img src="/images/areas/berkhampstead.jpg" alt="Traditional style pole marquee" width="581" loading = "lazy"/>
<p class="wide">A romantic pole tent for a wedding in a large Hertfordshire garden.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Fantastic bunch of lads</span>
<abbr title="2013-05" class="dtreviewed hide">May 2013</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Birthday girl's husband</span><span class="adr"><span class="locality">Hitchin</span><span class="region">Hertsfordshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Thank you and your team for the marquee you provided for my wife's surprise birthday party last Thursday. The marquee was ideal and looked amazing. It accommodated everyone well and the heater was a godsend. Please say thank you again to your team who erected and dismantled the marquee. They were excellent; very pleasant, very helpful, very efficient. Their work helped everything go so well. Once again, thank you so much for your help in making this such a wonderful evening for us all.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Hertfordshire</h3>
<p class="venues">We maintain a list of <a href="/venues/herts.htm">marquee venues in the Hertfordshire</a> for customers' convenience. These are not recommendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Hertfordshire</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Baldock area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19656.895749553594!2d-0.2026694419337706!3d51.98661280185233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48762c414383b875%3A0xf517a25d9d8ee2ed!2sBaldock!5e0!3m2!1sen!2suk!4v1543424177970" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>