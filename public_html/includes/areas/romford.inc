<p>County Marquees is a professional, family run firm which has been supplying beautiful marquees to the Romford and Ilford area for nearly thirty years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/muswell_hill.jpg" alt="Art Deco style marquee in Muswell Hill" width="198" height="150"/>
<p>Art Deco style marquee in small Muswell Hill garden</p>
<img src="/images/pages/areas/highgate2.jpg" alt="Flexible frame tent slotted in alongside a swimming pool" width="198" height="150"/>
<p>Flexible frame tent slotted in alongside a swimming pool</p>
</div>
<p>Our marquees range from frame tents that can be adapted to fit narrow gardens to grand tents suitable for the biggest gardens. So whether you are organising a small garden get-together or a large wedding, we have it covered.</p>
<p>During your free site visit, our friendly North London representative will measure your available space and advise you on the combination of marquee, furniture and equipment that best matches your square footage, budget and function.</p>
<p>Always eager to help, our team is regularly praised for their cheerfulness, efficiency and outstanding results. View our <a href="/company/testimonials.htm">testimonials</a> to see what other customers thought, or see our gallery to see <a href="/real.htm">real examples</a> of our work.</p>
<p>See the a clear list of our <a href="/pricing/marquees.htm">prices</a> or alternatively you can also get a <a href="/pricing/quotation.htm">quick online quote</a> which aims to provide you with a rough idea of how much your dream marquee may cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Hackney pub garden marquee</h2>
<img src="/images/areas/hackney.jpg" alt="Boho style wedding in the garden of a Hackney pub" width="581" loading-"lazy"/>
<p class="wide">Boho style wedding in the garden of a Hackney pub</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">My Aunt sends her thanks for the efficiency and great attitude of your staff</span>
<abbr title="2011-07" class="dtreviewed hide">June 2011</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Northwood</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">We would just like to say how impressed we were with the marquee and your team.  All the workers were very professional, speedy and polite and were a pleasure to deal with, giving us helpful advice when needed.  My Aunt also sends her thanks for the efficiency and great attitude of your staff.</p>
<p class="noborder">The marquee really was beautiful and we couldn't have asked for a more lovely day...the weather was particular kind to us.</p>
<p>I am more than happy to recommend you to friends and family as the service was fantastic.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Romford and Ilford</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39670.9021031187!2d0.04966192199975521!3d51.57865678950856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a1494a4cce5b%3A0xd4405940c0cf45ab!2sIlford!5e0!3m2!1sen!2suk!4v1540219107064" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>