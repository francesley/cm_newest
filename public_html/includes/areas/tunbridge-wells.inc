<p>For just over thirty years, County Marquees has supplied Kent and Tunbridge Wells with professional marquees and a large range of event decorations and lighting.</p>
<div class="textPhotos">
<img src="/images/pages/areas/kent_winter.jpg" alt="A Kentish winter marquee interior width="198" height="150"/>
<p>Winter marquee interior</p>
<img src="/images/pages/areas/kent.jpg" alt="Traditional style tent" width="198" height="150"/>
<p>Traditional style tent</p>
<img src="/images/pages/areas/stretch.jpg" alt="Stretch tent in a walled garden" width="198" height="150"/>
<p>Stretch tent in a walled garden</p>
</div>
<p>We supply luxury marquees in a variety of styles and sizes to suit any occasion. Whether you are organising a small garden party or a wedding at stunning Wadhurst Castle, we have it covered with range of different type of tents big and small, furniture, equipment and accessories. Our friendly Kent representative will make a free site visit and guide you through our portfolio with advice on how best to turn your dream into reality.</p> 
<p>Our brilliant team is frequently <a href="/company/testimonials.htm">complimented</a> on their good nature and hard work. Check our our <a href="/photos.htm">gallery</a> see some of our previous work, or see our <a href="/real.htm">real examples</a> page to get inspiration for your next event.</p>
<p>If you like what you see, please don’t hesitate to get in contact, have a look at our <a href="/pricing/marquees.htm">prices</a>, or get a quick (and free!) online quote. For information on pricing, see our <a href="/pricing/marquees.htm">pricing page</a> or to get a rough estimate of how much your dream marquee may cost, get a <a href="/pricing/quotation.htm">quick online quote</a>.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Glamorous clear marquee</h2>
<img src="/images/areas/clear_marquee.jpg" alt="Clear marquee in Tunbridge Wells, Kent" width="581" height="410"/>
<p class="wide">A ravishing clear marquee with dramatic lighting in Tunbridge Wells, Kent.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Such a cool space</span>
<abbr title="2018-08" class="dtreviewed hide">August 2018</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Tunbridge Wells</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description">
 <p>What a great event &ndash; we loved every minute of it &ndash; and the marquee was brilliant.  Thanks to you and all the team who created such a cool space.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recomendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Tunbridge Wells area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d40056.964470488245!2d0.23177319404042393!3d51.13501842121889!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df389f0109f8d9%3A0x73507a3bf176a764!2sRoyal+Tunbridge+Wells%2C+Tunbridge+Wells!5e0!3m2!1sen!2suk!4v1543939404684" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>