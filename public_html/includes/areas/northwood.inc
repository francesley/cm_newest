<p>County Marquees is a friendly, efficient firm that has been supplying a large variety of tents to Northwood, Rickmansworth and the surrounding area for nearly 30 years.</p>
<div class="textPhotos">
<img src="/images/pages/areas/muswell_hill.jpg" alt="Art Deco style marquee in Muswell Hill" width="198" height="150"/>
<p>Art Deco style marquee in small Muswell Hill garden</p>
<img src="/images/pages/areas/highgate2.jpg" alt="Flexible frame tent slotted in alongside a swimming pool" width="198" height="150"/>
<p>Flexible frame tent slotted in alongside a swimming pool</p>
</div>
<p>We have a lot of kit for you to choose from. Frame marquees, traditional tents, Chinese hat, and stretch tents plus an extensive stock of furniture and equipment. During our free site visit, our North London representative can measure the available space and advise on you on the size and styles which best suit your square footage. So whether you are having a small event or large wedding, we will find the combination to match your budget and dreams.  
<p>View our <a href="/photos.htm">gallery</a> to see some examples of our recent work. Or check out our <a href="/company/testimonials.htm">testimonials</a> to see what our customers said about our brilliant team.</p>
<p>Our <a href="/pricing/marquees.htm">prices</a> are clear for all to see and you can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough idea of how much your dream marquee might cost.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Rickmansworth Marquee</h2>
<img src="/images/areas/northwood.jpg" alt="Marquee at Moor Park golf club, Rickmansworth" width="581"  loading="lazy" />
<p class="wide">Marquee in the beautiful setting of Moor Park Golf Club, Rickmansworth, from some years ago.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">My Aunt sends her thanks for the efficiency and great attitude of your staff</span>
<abbr title="2011-07" class="dtreviewed hide">June 2011</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Northwood</span><span class="region">North London</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">We would just like to say how impressed we were with the marquee and your team.  All the workers were very professional, speedy and polite and were a pleasure to deal with, giving us helpful advice when needed.  My Aunt also sends her thanks for the efficiency and great attitude of your staff.</p>
<p class="noborder">The marquee really was beautiful and we couldn't have asked for a more lovely day...the weather was particular kind to us.</p>
<p>I am more than happy to recommend you to friends and family as the service was fantastic.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in London</h3>
<p class="venues">We maintain a list of <a href="/venues/lon.htm">marquee venues in London</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees London</div>
 <div class="adr">
<div class="street-address">53 Dartmouth Park Road</div>
<div class="locality">London</div>
<span class="postal-code">NW5 1SL</span>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">020 7267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>County Marquees covers Northwood</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39638.560520087274!2d-0.4644862277035173!3d51.61569769108787!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48766b05141f5df5%3A0x76f62777a7f157bd!2sNorthwood!5e0!3m2!1sen!2suk!4v1540214932997" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>