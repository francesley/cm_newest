<p>County Marquees is an independent, family run company which supplies marquees to High Wycombe and the surrounding Buckinghamshire countryside.</p>
<div class="textPhotos">
<img src="/images/pages/areas/milton_keynes.jpg" alt="Traditional tent" width="198" height="150"/>
<p>Traditional tent at a wedding</p>
<img src="/images/pages/areas/bucks.jpg" alt="Blacked out party tent" width="198" height="150"/>
<p>Bride and groom in their Buckinghamshire wedding marquee</p>
</div>
<p>With frame marquees, traditional style tents, Chinese hats, stretch and bell tents, we offer a variety of beautifully finished <a href="/equipment/marquees.htm">marquees</a> for weddings, parties and corporate events. Whether you are organising a lavish wedding or a small garden party we can help you choose the best combination of marquee, furniture and accessories to complement your venue. We also give advice on how best to combine your ideas and budget.</p>
<p> Whatever the event, we pride ourselves on the exceptionally high standards of our marquees and service. See what our <a href="/company/testimonials.htm">customers</a> have to say about us, or check out our <a href="/photos.htm">gallery</a> for examples of our recent work.</p>
<p>We have a clear list of <a href="/pricing/marquees.htm">prices</a> on this website and there are no hidden extra costs. You can also get a <a href="/pricing/quotation.htm">quick online quote</a> to give you a rough estimate of how much your dream marquee might cost. Or <a href="/contact_us.htm">get in touch</a> to arrange a free site visit.</p>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>High Wycombe area wedding</h2>
<img src="/images/areas/high_wycombe.jpg" alt="Wedding marquee at Hampden House" width="581" loading = "lazy"/>
<p class="wide">A partially transparent and very pretty wedding marquee at Hampden House, Great Missenden. Window walls have wow factor if the view is good and the walls can be opened up on hot summer days to let the air flow.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Really beautiful space</span>
<abbr title="2010-06" class="dtreviewed">June 2010</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">Bride</span><span class="adr"><span class="locality">High Wycombe</span><span class="region">Buckinghamshire</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees London</a><div class="tel">020 7267 4271</div>
<div class="adr"><div class="street-address">53 Dartmouth Park Road</div><span class="locality">London</span><span class="region">London</span><span class="postal-code">NW5 1SL</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p class="noborder">Your team were brilliant and the marquee was really fantastic - by far the best thing we spent money on for the wedding.  We couldn't believe how fab it looked.</p>
<p class="noborder">I had rather low expectations as our neighbours had one for a big party a couple of months before and I thought it looked really tatty, so I was preparing myself for the worst.  Instead, you created a really beautiful space for our reception.  Our guests couldn't believe how it looked and neither could we.</p>
<p>We feel we received great service from you in addition to the fab marquee and will certainly be recommending you to friends.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Buckinghamshire</h3>
<p class="venues">We maintain a list of <a href="/venues/bucks.htm">marquee venues in Buckinghamshire</a> for customers' convenience. These are not recomendations, simply for your information.</p>
</div>
<div class="vcard">
<div class="fn org">County Marquees Buckingham</div>
 <div class="adr">
<div class="street-address">Admin Office: 53 Dartmouth Park Road</div>
<div class="locality">London</div>
<div class="region different">Surrey</div> <div class="postal-code">NW5 1SL</div>
<span class="country-name">UK</span>
</div>
<div>Telephone: <a href="tel:+442072674271"><span class="tel">0207 267 4271</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>High Wycombe area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39628.58227398203!2d-0.7879031401954714!3d51.62712197389117!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876600361b47cc3%3A0xcd47a66f382f4def!2sHigh+Wycombe!5e0!3m2!1sen!2suk!4v1540087294299" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>