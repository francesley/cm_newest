<p>Independent, family-run County Marquees has been supplying professional marquees to Westerham and the beautiful surrounding Kent area since 1989.</p>
<div class="textPhotos">
<img src="/images/pages/areas/tunbridge_wells.jpg" alt="Clear marquee near Tunbridge Wells" width="198" height="150"/>
<p>Glamorous clear marquee near Tunbridge Wells</p>
<img src="/images/pages/areas/sevenoaks.jpg" alt="Traditional style tent in a Sevenoaks garden" width="198" height="150"/>
<p>Traditional style tent in a Sevenoaks garden</p>
</div>
<p>In Westerham, you’ll find inspiring history, stunning views and some great resources for events. Like <a href="https://www.squerryes.co.uk/" target="_blank" rel="noreferrer noopener">Squerryes Court</a>, just outside Westerham. This late 17th century manor house is surrounded by gardens and parkland and has been held by the same family for over 280 years. You can hire this venue for marquee events &ndash; but only six times a year. You can also buy wine from the Squerryes Winery. And Westerham Brewery has an award winning range of craft beers.</p>
<p>County Marquees has been serving the Westerham area for many years. We supply tents of all sizes and kinds, from small pointy topped Chinese Hats</em> to large traditional tents with romantic swooping roofs and everything in between. We also have a wide selection of furniture and equipment and some fantastic staff who are <a href="/company/testimonials.htm">praised so often</a> that it is surprising it doesn't go to their heads. 
<p>Our <a href="/real.htm">real examples gallery</a> showcases some of our previous work and offers decorative inspiration for your next event.</p>
<p>For information on prices, see our <a href="/pricing/marquees.htm">pricing page</a>. Alternatively, to get a rough idea of costs, get a <a href="/pricing/quotation.htm">quick online quote</a>
<p>Please <a href="/contact_us.htm">contact us</a> if you would like to find out more.</p>
<h2>Westerham Wedding</h2>
<img src="/images/areas/westerham.jpg" alt="Spring wedding in Westerham" width="581" loading = "lazy"/>
<p class="wide">A prettily decorated spring wedding marquee in Westerham with daffodills through the windows.</p>
<h2>Local praise</h2>
<div class="hreview single">
<span class="summary hide">Your marquee was absolutely amazing</span>
<abbr title="2015-06" class="dtreviewed hide">June 2015</abbr><span class="hide">by</span> <span class="reviewer vcard"><span class="fn hide">anonymous</span><span class="adr"><span class="locality">Edenbridge</span><span class="region">Kent</span></span></span><span class="rating hide">5</span>
<span class="type hide">business</span>
<div class="item vcard hide"><a class="url fn org" href="https://www.countymarquees.com/">County Marquees Kent</a><div class="tel">01892 506870</div>
<div class="adr"><div class="street-address">Pantiles Chambers, 85 High Street</div><span class="locality">Tunbridge Wells</span><span class="region">Kent</span><span class="postal-code">TN1 1XP</span><div class="country-name">UK</div></div></div>
<blockquote class="description"><p>Thank you for the most amazing evening, your marquee was absolutely amazing and your team were just great fun to have around. I will be coming back to County Marquees for any future parties! It's been great working with you and your company, thank you so much.</p></blockquote>
</div>
<h2>Local resources</h2>
<div class="venues">
<h3>Marquee venues in Kent</h3>
<p class="venues">We maintain a list of <a href="/venues/kent.htm">marquee venues in Kent</a> for customers' convenience. These are not recomendations.</p>
</div>
<div class="vcard"">
<div class="fn org">County Marquees Kent</div>
 <div class="adr">
<div class="street-address">Pantiles Chambers, 85 High Street</div>
<div class="locality">Tunbridge Wells</div>
<span class="region">Kent</span>
<span class="postal-code">TN1 1XP</span>
<span class="country-name hide">UK</span>
</div>
<div>Telephone: <a href="tel:+441892506870"><span class="tel">01892 506870</span></a></div>
<a class="email" href="mailto:enquiries@countymarquees.com">enquiries@countymarquees.com</a>
</div>
<h2>What next?</h2>
<a class="marquee rightmarquee first" href="/photos.htm">
<h3>Have a look</h3>
<img src="/images/pages/areas/photos.jpg" alt="Traditional marquee with external lighting" loading="lazy" />
<p>Marquee gallery</p>
</a>
<a class="marquee rightmarquee" href="/equipment/marquees.htm">
<h3>Marquees</h3>
<img src="/images/pages/areas/gallery.jpg" alt="Marquee interior with lanterns and clear roof" loading="lazy" />
<p>Which type suits you?</p>
</a>
<a class="marquee rightmarquee" href="/contact_us.htm">
<h3>Get in touch</h3>
<img src="/images/pages/areas/contact.jpg" alt="Traditional marquee" loading="lazy" />
<p>Contact us</p>
</a>
<h2>We cover the Westerham area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19970.776696268767!2d0.051743405952928624!3d51.2679298441578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47df5134d28d2cd3%3A0x6ad247250a230d7f!2sWesterham!5e0!3m2!1sen!2suk!4v1543939584946" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>