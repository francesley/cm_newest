<?php



echo <<<EOQ



<p>Do you have a pretty good idea of what sort of marquee and accessories you would like?</p>
<p>Please fill in as much of this form as you can. We do ask for a lot of detail, but without it, we will be unable to supply you with a realistic quote. </p>
<p>And just so you know, all marquee quotes are <strong>free, personalised</strong> and confer absolutely <strong>no obligation.</strong></p>
<form>
<div class="error summary" {$this -> styleFormResponse}>{$this -> formResponse}</div>

<fieldset>
<legend>Event details</legend>

<label class="extra" style="width:481px"><span class="required">*</span>Guest numbers:<input type="text" name="tbNumbers" value="{$fc -> formNumbers}" class="reqfield"/><span class="end">(approximately)</span></label>
<div class="error" {$fc -> styleNumbers}>Required</div>

<h4>Marquee size if known:</h4>
<label for="ddWidth">Width:
	<select name="ddWidth" id="ddWidth">
		{$fc -> widthDD}
	</select>
</label>
<input type="hidden" name="tbWidthCheck" value="{$fc -> widthCheck}"/>
<label for="ddLength">Length:
	<select name="ddLength" id="ddLength">
		{$fc -> lengthDD}
	</select>
</label>
<div class="error length" {$fc -> styleLength}>Please choose a length</div>
</fieldset>


<fieldset>
<legend>3. Simple yes or no</legend>
<h4 style="padding-top:0">Check any that apply:</h4>
<label for="cbSeated" class="cb">All guests seated: <input name="cb[]" type="checkbox" id="cbSeated" class="cb" value="Seated event" {$fc -> Seatedevent}/></label>
<label for="cbDance"  class="cb right">You need a dancefloor: <input id="cbDance" name="cb[]" type="checkbox" class="cb" value="Wants dance floor" {$fc -> Wantsdancefloor}/></label>
<label for="cbStage" class="cb">You need a stage: <input id="cbStage" name="cb[]" type="checkbox" class="cb" value="Wants stage" {$fc -> Wantsstage}/></label>
<label for="cbCatering"  class="cb right">Catering tent: <input id="cbCatering" name="cb[]" type="checkbox" class="cb" value="Wants catering tent" {$fc -> Wantscateringtent}/></label>
<label for="cbBuffet" class="cb">Area for a buffet?: <input id="cbBuffet" name="cb[]" type="checkbox" class="cb"  value="Need buffet area" {$fc -> Needbuffetarea}/></label>
<label for="cbBar"  class="cb right">And/or for a bar?: <input id="cbBar" name="cb[]" type="checkbox" class="cb"  value="Need bar area" {$fc -> Needbararea}/></label>
</fieldset>


<input name="submitted" type="hidden" value="true" />
<label for="contactSubmit" class="submit"><input name="contactSubmit" id="contactSubmit" type="submit" value="Get no-obligation quote" /></label>
<p class="privacy">County Marquees respects your <a href="/company/privacy.htm">privacy</a></p>
<div class="unnec"></div>
</form>
</div>
EOQ;

?>
