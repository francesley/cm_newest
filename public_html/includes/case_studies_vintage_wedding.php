<?php

$content = <<<EOQ
 <div id="nextPrev">{$pageDetails -> nextPrevLinks}</div>
<div class="case"><a href="/images/case/serlin/with_leaves_big.jpg" title="The popular vintage style: chiavari chairs, multicoloured candles in gothic style candlesticks..." class="fancybox" rel="group"><img src="/images/case/serlin/with_leaves.jpg" alt="Vintage marquee style" width="273" height="186" /></a>
<p>The popular vintage style: chiavari chairs, mismatched candles in gothic style candlesticks...<br/><a href="/photos/vintage_wedding.htm">View in photo gallery &raquo;</a></p></div>
<div class="case rightcase"><a href="/images/case/serlin/finished_big.jpg" title="...branches attached to wires and swathed with leaves, colourful natural flowers, lined up trestle tables" class="fancybox" rel="group"><img src="/images/case/serlin/finished.jpg" alt="Branches with leaves cover the marquee roof" width="273" height="186" /></a><p>...branches attached to wires and swathed with leaves, colourful natural flowers, lined up trestle tables</p></div>
<div class="case"><a href="/images/case/serlin/dining_big.jpg" title="Branches before the leaves were attached, also shows burlap table runners" class="fancybox" rel="group"><img src="/images/case/serlin/dining.jpg" alt="Branches before leaves on marquee roof" width="273" height="186" /></a><p>Branches before the leaves were attached, also shows burlap table runners</p></div>
<div class="case rightcase"><a href="/images/case/serlin/full_on_big.jpg" title="The overall effect: bright, friendly and colourful" class="fancybox" rel="group"><img src="/images/case/serlin/full_on.jpg" alt="Front view of vintage wedding marquee" width="273" height="186" /></a><p>The overall effect: bright, friendly and colourful</p></div>
<div class="case"><a href="/images/case/serlin/lights_big.jpg" title="Detail shot, showing fairy lights attached to branches. In the background, the dancefloor on the right, and bar on the left" class="fancybox" rel="group"><img src="/images/case/serlin/lights.jpg" loading="lazy" alt="Fairy lights attached to branches on marquee roof" width="273" height="186" /></a><p>Detail shot, showing fairy lights attached to branches. In the background, the dancefloor on the right, and bar on the left</p></div>
<div class="case rightcase"><a href="/images/case/serlin/table_big.jpg" title="Table close up, showing pretty mismatched flowers and dangling glass baubles" class="fancybox" rel="group"><img src="/images/case/serlin/table.jpg" alt="Vintage table style" width="273" height="186" loading="lazy" /></a><p>Table close up, showing pretty mismatched flowers and dangling glass baubles</p></div>
<div class="case"><a href="/images/case/serlin/exterior_big.jpg" title="The marquee was in a field, portable loos and generator visible to the right" class="fancybox" rel="group"><img src="/images/case/serlin/exterior.jpg" alt="Wedding marquee in a field" width="273" height="186" loading="lazy" /></a><p>The marquee was in a field, portable loos and generator visible to the right</p></div>
<div class="case rightcase"><a href="/images/case/serlin/overview_big.jpg" title="Vintage wedding marquee overview" class="fancybox" rel="group"><img src="/images/case/serlin/overview.jpg" alt="Vintage wedding marquee overview" width="273" height="186" loading="lazy" /></a><p>Vintage wedding marquee overview</p></div>
<hr>
<p class="wide"><strong>The occasion</strong> was a summer wedding reception.</p>
<p class="wide"><strong>The challenge</strong> was to create a workable party space in a remote field with wheelchair access over uneven ground. The client also wanted to decorate the marquee with branches across the marquee roof.</p>
<p class="wide abovecase"><strong>The solution:</strong> a 12m (40') x 24m (80') frame marquee with a 12m (40') x 3m (10') extension that served as a catering tent. The set up included a generator and portable toilets to provide all facilities. We also built a ramp over a ditch to enable wheelchair access. Wires strung over the roof created a structure to hang the decorative branches and leaves.</p>


<p class="endCall"><a href="../contact_us.htm">Contact us</a> if you would like to find out more about vintage wedding marquees.</p>
</div>
EOQ;
?>