<?php
$accessKey = 'd2e40c335e4f49b0a87004c4cf182a9b';
$endpoint = 'https://api.bing.microsoft.com/v7.0/search';
$term = isset($_GET['q']) ? $_GET['q'] : '';

function BingWebSearch($url, $key, $query, $resultsPerPage, $offset)
{
    $siteDomain = 'countymarquees.com';
    // Prepare the HTTP request headers.
    $headers = "Ocp-Apim-Subscription-Key: $key\r\n";
    $options = array(
        'http' => array(
            'header' => $headers,
            'method' => 'GET'
        )
    );

    // Build the URL with query parameters.
    $queryParams = array('q' => 'site:' . $siteDomain . ' ' . $query, 'count' => $resultsPerPage, 'offset' => $offset);
    $url .= '?' . http_build_query($queryParams);

    // Perform the request and get a JSON response.
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    // Extract Bing HTTP headers.
    $headers = array();
    foreach ($http_response_header as $k => $v) {
        $h = explode(":", $v, 2);
        if (isset($h[1]) && (preg_match("/^BingAPIs-/", $h[0]) || preg_match("/^X-MSEdge-/", $h[0]))) {
            $headers[trim($h[0])] = trim($h[1]);
        }
    }

    return array($headers, $result);
}

// Validates the subscription key.
if (strlen($accessKey) == 32) {
    // Check if the form was submitted and process the search query.
    if (isset($_GET['q'])) {
        $term = $_GET['q'];
    }

    // Display the search form.
    echo '<form class="srch-form" action="" method="get">';
    echo '<input type="text" name="q" value="' . htmlspecialchars($term) . '" placeholder="Enter your search term">';
    echo '<button type="submit"><img src="/images/SearchIconBlue.svg" alt="search" width="23" height="25"></button>';
    echo '</form>';

    // Makes the request only if the form was submitted with a search term.
    if (!empty($term)) {
        // Get the current page number from the URL parameter.
        $page = isset($_GET['page']) ? max(1, intval($_GET['page'])) : 1;

        // Set the number of results per page.
        $resultsPerPage = 10;

        // Calculate the offset for the current page.
        $offset = ($page - 1) * $resultsPerPage;

        // Makes the request.
        list($headers, $json) = BingWebSearch($endpoint, $accessKey, $term, $resultsPerPage, $offset);

        // Decode the JSON response.
        $response = json_decode($json, true);

        // Display results in an ordered list.
        if (isset($response['webPages']['value']) && count($response['webPages']['value']) > 0) {
            echo '<ul class="search-results">';
            foreach ($response['webPages']['value'] as $result) {
                $title = $result['name'];
                $url = $result['url'];
                $caption = $result['snippet'];

                echo '<li>';
                echo '<h3><a href="' . $url . '">' . $title . '</a></h3>';
                echo '<p class="page-url"><a href="' . $url . '">' . $url . '</a></p>';
                echo '<p>' . $caption . '</p>';
                echo '</li>';
            }
            echo '</ul>';

            // Display pagination links.
            $totalResults = isset($response['webPages']['totalEstimatedMatches']) ? intval($response['webPages']['totalEstimatedMatches']) : 0;
            $totalPages = ceil($totalResults / $resultsPerPage);
            
            // Restrict the total number of pages to 3
            $totalPages = min($totalPages, 3);

            if ($totalPages > 1) {
                echo '<div class="pagination">';
                if ($page > 1) {
                    echo '<a href="?q=' . urlencode($term) . '&page=' . ($page - 1) . '">Previous</a>';
                }

                for ($i = 1; $i <= $totalPages; $i++) {
                                      if ($i === $page) {
                        echo '<span class="current">' . $i . '</span>';
                    } else {
                        echo '<a href="?q=' . urlencode($term) . '&page=' . $i . '">' . $i . '</a>';
                    }
                }
                if ($page < $totalPages) {
                    echo '<a href="?q=' . urlencode($term) . '&page=' . ($page + 1) . '">Next</a>';
                }
                echo '</div>';
            }
        } else {
            echo '<p>No results found.</p>';
        }
    }
} else {
    print("Invalid Bing Search API subscription key!\n");
}
?>