// l.1 - l.300 set up
// line 300 - line 450 reads xml and creates the map markers and sidebar
// sets up filtering of markers by venue region, size and civil ceremony




//*****************************  SETUP
//GLOBALS

var gmarkers = [];

var side_bar_html = "";

var lastlinkid;

var map;

var pans = [];

var currentMarker;

// This function picks up the click and opens the corresponding info window

function myclick(i) {

GEvent.trigger(gmarkers[i], "click");

}



function init() {





//CUSTOM CONTROL

var labelContainer;


//Below function created a dropdown with English counties and a few other options for filtering markers by selection. The code that does this mainly starts at l.451
function LabelControl() {  }

LabelControl.prototype = new GControl();



LabelControl.prototype.initialize = function(map) {

labelContainer = document.createElement("div");

labelContainer.innerHTML = '<h3>Refine venue view:</h3><select name="regions" id="regions" onChange="SelectRegion(this)"><option value="all">All counties</option><option value="lon">London</option> <option value="beds">Bedfordshire</option><option value="berks">Berkshire</option><option value="bucks">Buckinghamshire</option><option value="eSussex">East Sussex</option><option value="essex">Essex</option><option value="hants">Hampshire</option><option value="herts">Hertfordshire</option><option value="kent">Kent</option><option value="oxford">Oxfordshire</option><option value="surrey">Surrey</option><option value="wSussex">West Sussex</option></select><select name="types" id="types" onChange="SelectType(this)"><option value="all2">All venues</option><option value="civil">Civil ceremonies</option><option value="large">Large (150+ seated)</option><option value="huge">Huge events</option><option value="not">Not expensive</option><option value="medium">Medium price</option><option value="expensive">Expensive</option></select>';

labelContainer.id = "selects";		

map.getContainer().appendChild(labelContainer); //this added the dropdown to the map

return labelContainer;

}



LabelControl.prototype.getDefaultPosition = function() {

	return new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(7, 33));

}



//for ie7

if(!Array.indexOf){

Array.prototype.indexOf = function(obj){

  for(var i=0; i<this.length; i++){

	  if(this[i]==obj){

		  return i;

	  }

  }

  return -1;

}

}





//ICONS - pretty obvious - sets up custom icons, size, shadow, image src etc

var baseIcon = new GIcon();

baseIcon.iconSize=new GSize(15,14);

baseIcon.shadowSize=new GSize(23,14);

baseIcon.iconAnchor=new GPoint(10,15);

baseIcon.infoWindowAnchor=new GPoint(10,5);

baseIcon.transparent = "maps/icons/transparent.png";



var gicons=[];

gicons["house"] = new GIcon(baseIcon, "/maps/icons/houseTiny.png", null, "maps/icons/house_shadow.png");

gicons["house"].imageMap = [3,2,16,4,16,16,1,13];

gicons["house"].over = "houseTinyOver.png";

gicons["hotel"]  = new GIcon(baseIcon, "/maps/icons/hotelTiny.png", null, "maps/icons/hotel_shadow.png");

gicons["hotel"].imageMap = [2,4,10,8,18,6,19,17,9,14,0,17];

gicons["hotel"].over = "hotelTinyOver.png";

gicons["field"] = new GIcon(baseIcon, "/maps/icons/fieldSm.png", null, "maps/icons/field_shadow.png");

gicons["field"].imageMap = [8,17,13,15,17,9,8,1,2,9];

gicons["field"].over = "fieldOver.png";

gicons["school"] = new GIcon(baseIcon, "/maps/icons/schoolSm.png", null, "maps/icons/school_shadow.png");

gicons["school"].imageMap = [0,11,10,5,19,11,17,17,4,16];

gicons["school"].over = "schoolOver.png";

gicons["barn"] = new GIcon(baseIcon, "/maps/icons/barnTiny.png", null, "maps/icons/barnShadow.png");		 

gicons["barn"].imageMap = [6,1,16,2,17,12,9,16,0,8];

gicons["barn"].over = "barnTinyOver.png";

gicons["sports"] = new GIcon(baseIcon, "/maps/icons/sports.png", null, "maps/icons/sports_shadow.png");

gicons["sports"].imageMap = [11,-1,0,9,11,22,24,11];

gicons["sports"].over = "sportsOver.png";

gicons["castle"] = new GIcon(baseIcon, "/maps/icons/castle.png", null, "maps/icons/castle_shadow.png");

gicons["castle"].imageMap = [-1,0,14,0,14,13,0,13];

gicons["castle"].over = "castleOver.png";

gicons["restaurant"] = new GIcon(baseIcon, "/maps/icons/restaurant.png", null, "maps/icons/restaurant_shadow.png");

gicons["restaurant"].imageMap = [0,0,14,0,13,12,0,13];

gicons["restaurant"].over = "restaurantOver.png";



//PANS These are the latlang coordinates for each of the english counties in the dropdown created above in function LabelControl. The map panned to the county when it was selected (before the gmaps code went to v2 and this stopped working)

pans["surrey"] = new GLatLng("51.279958","-0.466919");

pans["wSussex"] = new GLatLng("50.946315","-0.465546");

pans["eSussex"] = new GLatLng("50.950641","0.269165");

pans["beds"] = new GLatLng("52.146973","-0.477905");

pans["lon"] = new GLatLng("51.521562","-0.127716");

pans["hants"] = new GLatLng("51.12766","-1.224976");

pans["essex"] = new GLatLng("51.806917","0.681152");

pans["bucks"] = new GLatLng("51.839172","-0.810242");	  

pans["herts"] = new GLatLng("51.825593","-0.240326");

pans["oxford"] = new GLatLng("51.80352","-1.260681");

pans["kent"] = new GLatLng("51.297993","0.854187");

pans["berks"] = new GLatLng("51.510452","-1.19751");





map = new GMap2(document.getElementById("map"));

map.addControl(new GLargeMapControl());

map.addControl(new LabelControl());

map.setCenter(new GLatLng(51.533232, -0.124842), 13);

map.setZoom(8);

//STREETVIEW OVERLAY***************************************************

/*svOverlay = new GStreetviewOverlay();

map.addOverlay(svOverlay);*/



// setting up the map

var mapControl = new GMapTypeControl();

map.addControl(mapControl);

//map.addControl(new GLargeMapControl());



var mt = map.getMapTypes();

// Overwrite the getMinimumResolution() and getMaximumResolution() methods

for (var i=0; i<mt.length; i++) {

mt[i].getMinimumResolution = function() {return 8;}

mt[i].getMaximumResolution = function() {return 19;}

}

mt[0].getMaximumResolution = function() {return 16;}



GEvent.addListener(map, "move", function() {

checkBounds();

});





// The allowed region which the whole map must be within - approx


var allowedBounds = new GLatLngBounds(new GLatLng(49.5,-2), new GLatLng(53,2));



// If the map position is out of range, move it back

function checkBounds() {

		// Perform the check and return if OK

		if (allowedBounds.contains(map.getCenter())) {

		return;

		}

		// It`s not OK, so find the nearest allowed point and move there

		var C = map.getCenter();

		var X = C.lng();

		var Y = C.lat();



		var AmaxX = allowedBounds.getNorthEast().lng();

		var AmaxY = allowedBounds.getNorthEast().lat();

		var AminX = allowedBounds.getSouthWest().lng();

		var AminY = allowedBounds.getSouthWest().lat();



		if (X < AminX) {X = AminX;}

		if (X > AmaxX) {X = AmaxX;}

		if (Y < AminY) {Y = AminY;}

		if (Y > AmaxY) {Y = AmaxY;}

		//alert ("Restricting "+Y+" "+X);

		map.setCenter(new GLatLng(Y,X));

}


//***************************** READS XML, FORMATS ATTRIBUTES AND ASSIGNS THEM, CREATES ALL THE MAP MARKERS PLUS ADDS THEM TO THE SIDEBAR LIST HTML
	
//MARKERS  

i = 0;



// This function creates all the markers from the data read from xml. It is called multiple times from the code below - once for each venue. The parameters are what is in the xml. Each marker gets all the details added to it ready to work in the map

function createMarker(point, siteTitle, siteIcon, details, area, civil, large, full, price, huge) {



	var marker = new GMarker(point, {icon:siteIcon, title:siteTitle});

	var linkid = "link" + (gmarkers.length);

	var overImg = "maps/icons/" + siteIcon.over;

	GEvent.addListener(marker, "click", function() {


		marker.openInfoWindowHtml("<span class='iws'>" + details + "</span>"); // this line was for opening an overlay on click and filling it with "details" attribute from xml for each marker

		document.getElementById(linkid).style.background="#EDEEE6";

		//marker.setImage(overImg);

		lastlinkid = linkid; 

	});

	marker.area = area;

	marker.civil = civil;

	marker.large = large;

	marker.price = price;

	marker.full = full;

	marker.huge = huge;

	// save the info we need to use later for the side_bar

	gmarkers[i] = marker;

	// add a line to the side_bar html - this is added later (line 431 in createmarker function) to the code that goes to make up the right side list of venues
	side_bar_html += '<a href="javascript:myclick(' + i + ')" id="'+ linkid +'" onmouseover="gmarkers[' + i + '].setImage(\'' + overImg + '\')" onmouseout="gmarkers[' + i + '].setImage(\'' + siteIcon.image + '\')" >' + siteTitle + '</a>';

	i++;

	return marker;

}



//XML - this code reads the xml and puts it into the form that the gmap api v1 used. You should use this code and modify it to suit v3? v4? api

var request = GXmlHttp.create();

request.open("GET", "/maps/markers.xml", true);

request.onreadystatechange = function() {

	if (request.readyState == 4) {

		var xmlDoc = GXml.parse(request.responseText);

		// obtain the array of markers and loop through it

		var markers = xmlDoc.documentElement.getElementsByTagName("marker");


	//This code runs through the markers, gets details for each one and then assigns the details for each to variables that are then used to set up the markers and populate what was then an infowindow and is now an overlay. 
	for (var i = 0; i < markers.length; i++) {

			// obtain the attribues of each marker

			var lat = parseFloat(markers[i].getAttribute("lat"));

			var lng = parseFloat(markers[i].getAttribute("lng"));

			var point = new GLatLng(lat,lng);



			var details = markers[i].getElementsByTagName("details")[0].firstChild.nodeValue;

			var civil = markers[i].getAttribute("civil");

			var huge = markers[i].getAttribute("huge");

			var price = markers[i].getAttribute("price");

			var large = markers[i].getAttribute("large");

			var full = markers[i].getAttribute("full");

			var label = markers[i].getAttribute("label");

			var area = markers[i].getAttribute("area");

			var siteIcon = gicons[markers[i].getAttribute("icon")];

			var extras = "<div class='extras' style='width:300px;height:30px'>";

			extras += civil == "1"?"<p class='civil'>Civil ceremonies:</p>":"<p class='civil no'>Civil ceremonies:</p>";

			extras += large == "1"?"<p class='civil left'>Large marquee:</p>":"<p class='civil no left'>Large marquee:</p>";

			extras += price == "1"?"<p class='price'>Marquee price:</p>":(price == "2"?"<p class='price expensive'>Marquee price:</p>":"<p class='cheap'>Marquee price:</p>");

			extras += full == "1"?"<p class='civil left'>Event management:</p>":"<p class='civil no left'>Event management:</p>";

			extras += "</div>";

			details = '<div style="width:300px;height:210px;overflow:auto;">' + details + extras;//HERE HERE HERE

			details += "</div>";

			// creates the marker with all the details now set up as they should be to create the map marker and the infowindow/overlay that opens up when it is clicked.
			var marker = createMarker(point, label, siteIcon, details, area, civil, large, full, price, huge);

			map.addOverlay(marker);

	}

	//put the assembled side_bar_html contents into the side_bar div

	document.getElementById("side_bar").innerHTML = side_bar_html;

	GEvent.addListener(map,"infowindowclose", function() {

		document.getElementById(lastlinkid).style.background = "";

	});

}

}

request.send(null);



}


//***************************** FILTERS

//All the code from here to end is to filter markers by region, price, size and whether or not you can hold a civil ceremony at the venue (civil ceremony is a non religious marriage service). Not sure how it works. The code on line 465 seems to decide whether only region is being selected, in which case the SelectJustRegion function is called. Otherwise, there's a massive switch statement that filters by whatever has been chosen.

//I cant see how it works but it looks long and straightforward.

function SelectRegion(regionSelect) {

		map.closeInfoWindow();

		var selectedCat = regionSelect.options[regionSelect.options.selectedIndex].value;

		var typeSelect = document.getElementById("types");

		var selectedType = typeSelect.options[typeSelect.options.selectedIndex].value;


		if (selectedType == "all2") {


			SelectJustRegions (selectedCat);

		//if civil or large selected

		} else {

				//var price = gmarkers[i][selectedType] === undefined?true:false;

				var prices = ["not","medium","expensive"];

				if (selectedCat != "all") {

				for (var i=0; i<gmarkers.length; i++) {

				  //var test = prices.indexOf(selectedType);

				  //var test2 = gmarkers[i][selectedType] != undefined;

				  //console.log("in array: ", test, " marker price: ", gmarkers[i].price);

				  //console.log("selected type not undefined: ", test2);


					if (gmarkers[i].area != selectedCat) {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					} else if (gmarkers[i][selectedType] !== undefined) {

						//console.log("selected type undefined");

						if (eval(gmarkers[i][selectedType]) == "0") {

							gmarkers[i].hide();

							document.getElementById("link" + i).style.display = "none";

						} else {

							gmarkers[i].show();

							  document.getElementById("link" + i).style.display = "block";

						}

					} else if (prices.indexOf(selectedType) != gmarkers[i].price ) {

							gmarkers[i].hide();

							document.getElementById("link" + i).style.display = "none";

					} else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					}

				}



				map.panTo(pans[selectedCat]);



				} else {

				for (i=0; i<gmarkers.length; i++) {

				  if (gmarkers[i][selectedType] != undefined) {

					  if (eval(gmarkers[i][selectedType]) == "1") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  }

				  } else if (prices.indexOf(selectedType) == gmarkers[i].price ) {

							gmarkers[i].show();

							document.getElementById("link" + i).style.display = "block";

				  } else {

					  gmarkers[i].hide();

					  document.getElementById("link" + i).style.display = "none";

				  }

				}

				}



				}

}



function SelectType(typeSelect) {

		map.closeInfoWindow();

		var selectedType = typeSelect.options[typeSelect.options.selectedIndex].value;

		var regionSelect = document.getElementById("regions");

		var selectedRegion = regionSelect.options[regionSelect.options.selectedIndex].value;

		/*console.log(selectedType);

		console.log(selectedRegion);*/

		switch (selectedType) {

			case "civil":

				if (selectedRegion == "all") {

				  for (var i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].civil);

					  if (gmarkers[i].civil == "1") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  } else {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					  }

				  }

				} else {

				  //alert (selectedRegion);

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].area);

					  if (gmarkers[i].civil != "1") {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else if (gmarkers[i].area != selectedRegion) {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					  }

				  }

				}

				break;

			case "large":

				if (selectedRegion == "all") {

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].civil);

					  if (gmarkers[i].large == "1") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  } else {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					  }

				  }

				} else {

				  //alert (selectedRegion);

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].area);

					  if (gmarkers[i].large != "1") {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else if (gmarkers[i].area != selectedRegion) {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					  }

				  }

				}

				break;

			case "huge":

				if (selectedRegion == "all") {

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].civil);

					  if (gmarkers[i].huge == "1") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  } else {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					  }

				  }

				} else {

				  //alert (selectedRegion);

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].area);

					  if (gmarkers[i].huge != "1") {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else if (gmarkers[i].area != selectedRegion) {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					  }

				  }

				}

				break;

			case "not":

				if (selectedRegion == "all") {

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].civil);

					  if (gmarkers[i].price == "0") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  } else {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					  }

				  }

				} else {

				  //alert (selectedRegion);

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].area);

					  if (gmarkers[i].price != "0") {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else if (gmarkers[i].area != selectedRegion) {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					  }

				  }

				}

				break;

			case "medium":

				if (selectedRegion == "all") {

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].civil);

					  if (gmarkers[i].price == "1") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  } else {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					  }

				  }

				} else {

				  //alert (selectedRegion);

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].area);

					  if (gmarkers[i].price != "1") {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else if (gmarkers[i].area != selectedRegion) {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					  }

				  }

				}

				break;

			case "expensive":

				if (selectedRegion == "all") {

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].civil);

					  if (gmarkers[i].price == "2") {

						  gmarkers[i].show();

						  document.getElementById("link" + i).style.display = "block";

					  } else {

						gmarkers[i].hide();

						document.getElementById("link" + i).style.display = "none";

					  }

				  }

				} else {

				  //alert (selectedRegion);

				  for (i=0; i<gmarkers.length; i++) {

				  //alert(gmarkers[i].area);

					  if (gmarkers[i].price != "2") {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else if (gmarkers[i].area != selectedRegion) {

						  gmarkers[i].hide();

						  document.getElementById("link" + i).style.display = "none";

					  } else {

						gmarkers[i].show();

						document.getElementById("link" + i).style.display = "block";

					  }

				  }

				}

				break;

			default:

				SelectJustRegions (selectedRegion);

			}

}



function SelectJustRegions (selectedRegion) {

		if (selectedRegion != "all") {

		for (i=0; i<gmarkers.length; i++) {

			if (gmarkers[i].area == selectedRegion) {

			  gmarkers[i].show();

			  document.getElementById("link" + i).style.display = "block";

			} else {

				gmarkers[i].hide();

				document.getElementById("link" + i).style.display = "none";

			}

		}

		map.panTo(pans[selectedRegion]);

		} else {

			for (i=0; i<gmarkers.length; i++) {

			  gmarkers[i].show();

			  document.getElementById("link" + i).style.display = "block";

			}

		}

}
