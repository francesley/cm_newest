<?php
//IN USE 11/14

	class Get_xml {
	
		public $xml;
		public $content = "";
		public $cmLogo;
		public $errorMsgForTest;
		//TODO  when i update pricing, see if all these protected variables cant go in the right class
		protected $pricingCall = "<ul class='endCall'><li>For individual advice, please  <a href='/contact_us.htm'>contact us</a></li> <li>You can also  <a href='/pricing/quotation.htm'>request a personalised quotation online</a></li></ul>";
		
		protected $end_terms = "</div><ul id='pricing_terms'>
		<li >Marquees can be joined to provide widths of 15m to 27m</li>
		<li>Allow extra space for reception areas, dance floors, buffet tables etc</li>
		<li>A non-refundable deposit is required to confirm  marquee hire bookings. The balance must be settled on the day of erection</li>
		<li>All prices are subject to current rates of VAT</li>
		<li>Damage waiver  is 5% + VAT on the final invoice</li>
	</ul>";

protected $furn_end_terms = "<ul id='pricing_terms'>
				<li>A non-refundable deposit is required to confirm  marquee hire bookings. The balance must be settled on the day of erection</li>
				<li>All prices are subject to current rates of VAT</li>
				<li>Damage waiver  is 5% + VAT on the final invoice</li>
			</ul>";
	
		function __construct($xml_file) {
		
			if (!file_exists($xml_file)) {
				$this -> xml = false;
				$this -> errorMsgForTest = "No xml file for $xml_file";
				return;
			}
			$this -> xml = simplexml_load_file($xml_file);
			//$this -> cmLogo = "<a href='http://www.countymarquees.com'><img src='/images/logoGreen.jpg' width='111' height='52' /></a>";
		
		}
		
	}
		
		class Get_html_xml extends Get_xml {
			
			function __construct($xml_file) {
				parent::__construct($xml_file);
			}
			
			
				public function GetMarqueePriceList($xpath_expr) {
				
				$rowCounter = 1;
				//foreach ($xml -> xpath("marquee[@size='medium']") as $marquee) {
				foreach ($this -> xml -> xpath($xpath_expr) as $marquee) {
					
					$this -> content .= "<table  cellspacing='0' cellpadding='0' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";
					$this -> content .= $marquee["type"] != "chinese hat"?"<caption>" . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees":"<caption>3 or 6m/10 or 20' chinese hat marquees";
					$this -> content .= "<span>For " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people </span></caption>";
					$this -> content .= "<thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>
						<th colspan='2' class='odd top'>Capacity </th>
						<th colspan='2' class='top'>Marquee</th>
						<th rowspan='2' valign='bottom' scope='col' class='nomob'>Coconut <br/>Matting</th>
						<th rowspan='2' valign='bottom' scope='col' class='nomob'>Lighting</th>
						<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>
					</tr>
					<tr><th scope='col'> Seated</th>
						<th scope='col'>Buffet</th>
						<th valign='bottom' scope='col'>Lined</th>
						<th valign='bottom' scope='col'>Unlined</th>
						<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>
						<th align='center' scope='col' class='marquee_total'>Unlined</th>
					 </tr></thead><tbody>";
					
					foreach($marquee -> row as $row){
						$rowClass = ($rowCounter % 2 == 0)?" class='odd' ":"";
						$this -> content .= "<tr $rowClass>\n<th class='leftCol' scope='row'>";
						$this -> content.= $marquee["type"] != "chinese hat"?$marquee["width"] . "m x " . $row["length"] . "m<br/>":$row["length"] . "m x " . $row["length"] . "m<br/>";
							$this -> content .= $marquee["type"] != "chinese hat"?"<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n":"<span>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";
						$this -> content .= "<td>" . $row["seatedCap"] . "</td>\n";
						$this -> content .= "<td >" . $row["buffetCap"] . "</td>\n";
						$this -> content .= $row["linedP"] != "N/A"?"<td>&pound;" . $row["linedP"] . "</td>\n":"<td>" . $row["linedP"] . "</td>\n";
							$this -> content .= $row["unlinedP"] != "POA"?"<td>&pound;" . $row["unlinedP"] . "</td>\n":"<td>" . $row["unlinedP"] . "</td>\n";
							$this -> content .= $row["mat"] != "POA"?"<td >&pound;" . $row["mat"] . "</td>\n":"<td>" . $row["mat"] . "</td>\n";
							$this -> content .= $row["light"] != "POA"?"<td>&pound;" . $row["light"] . "</td>\n":"<td>" . $row["light"] . "</td>\n";		
							$this -> content .= $row -> totals["lined"] != "N/A"?"<td  class='marquee_total leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n":"<td  class='marquee_total leftBorder'>" . $row -> totals["lined"] . "</td>\n";
							$this -> content .= $row -> totals["unlined"] != "POA"?"<td  class='marquee_total'>&pound;" . $row -> totals["unlined"] . "</td>\n":"<td  class='marquee_total'>" . $row -> totals["unlined"] . "</td>\n";
						$this -> content .= "</tr>\n";
						$rowCounter++;
					}		
				$this -> content .= "</tbody></table>\n<p class='price_bytable'>Please <a href='/contact_us.htm'>contact us</a> for a personalised quotation.</p>\n";
				
				}
				$this -> content .= $this -> end_terms . $this -> pricingCall;
				return $this -> content;
			
			}
			
			public function GetMarqueeSizePriceList($xpath_expr) {
				
				$rowCounter = 1;
				
				foreach ($this -> xml -> marquee as $marquee) {
				
					$xmlMarquee = simplexml_load_string($marquee->asXML());
					$rows = $xmlMarquee-> xpath($xpath_expr);
					//Util::Show(count($rows), "how many this size");
					
					if (count($rows) > 0) {
						
						$this -> content .= "<table cellspacing='0' cellpadding='0' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";
						$this -> content .= $marquee["type"] != "chinese hat"?"<caption>" . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees":"<caption>3 or 6m/10 or 20' chinese hat marquees";
						//$this -> content .= "<span>For " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people </span></caption>";
						$this -> content .= "<thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>
							<th colspan='2' class='odd top'>Capacity </th>
							<th colspan='2' class='top'>Marquee</th>
							<th rowspan='2' valign='bottom' scope='col'>Coconut <br/>Matting</th>
							<th rowspan='2' valign='bottom' scope='col'>Lighting</th>
							<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>
						</tr>
						<tr><th scope='col'> Seated</th>
							<th scope='col'>Buffet</th>
							<th valign='bottom' scope='col'>Lined</th>
							<th valign='bottom' scope='col'>Unlined</th>
							<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>
							<th align='center' scope='col' class='marquee_total'>Unlined</th>
						 </tr></thead><tbody>";
						 
						foreach ($rows as $row) {	
						
							$rowClass = ($rowCounter % 2 == 0)?" class='odd' ":"";
							$this -> content .= "<tr $rowClass>\n<th class='leftCol' scope='row'>";
							$this -> content.= $marquee["type"] != "chinese hat"?$marquee["width"] . "m x " . $row["length"] . "m<br/>":$row["length"] . "m x " . $row["length"] . "m<br/>";
							$this -> content .= $marquee["type"] != "chinese hat"?"<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n":"<span>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";
							$this -> content .= "<td>" . $row["seatedCap"] . "</td>\n";
							$this -> content .= "<td >" . $row["buffetCap"] . "</td>\n";
							$this -> content .= $row["linedP"] != "N/A"?"<td>&pound;" . $row["linedP"] . "</td>\n":"<td>" . $row["linedP"] . "</td>\n";
							$this -> content .= $row["unlinedP"] != "POA"?"<td>&pound;" . $row["unlinedP"] . "</td>\n":"<td>" . $row["unlinedP"] . "</td>\n";
							$this -> content .= $row["mat"] != "POA"?"<td >&pound;" . $row["mat"] . "</td>\n":"<td>" . $row["mat"] . "</td>\n";
							$this -> content .= $row["light"] != "POA"?"<td>&pound;" . $row["light"] . "</td>\n":"<td>" . $row["light"] . "</td>\n";		
							$this -> content .= $row -> totals["lined"] != "N/A"?"<td  class='marquee_total leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n":"<td  class='marquee_total leftBorder'>" . $row -> totals["lined"] . "</td>\n";
							$this -> content .= $row -> totals["unlined"] != "POA"?"<td  class='marquee_total'>&pound;" . $row -> totals["unlined"] . "</td>\n":"<td  class='marquee_total'>" . $row -> totals["unlined"] . "</td>\n";
							$this -> content .= "</tr>\n";
							$rowCounter++;
						}		
					$this -> content .= "</tbody></table>\n<p class='price_bytable'>Please <a href='/contact_us.htm'>contact us</a> for a personalised quotation.</p>\n";
					
					}
				}
				$this -> content .= $this -> end_terms . $this -> pricingCall;
				return $this -> content;
			
			}
			
			public function GetAllMarqueesPriceList() {
				
				$rowCounter = 1;
				
				foreach ($this -> xml -> marquee as $marquee) {
				
					$this -> content .= "<table cellspacing='0' cellpadding='0' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";
						$this -> content .= $marquee["type"] != "chinese hat"?"<caption>" . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees</caption>":"<caption>3 or 6m/10 or 20' chinese hat marquees</caption>";
						//$this -> content .= "<span> &#8212; for " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people </span></caption>";
						$this -> content .= "<thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>
							<th colspan='2' class='odd top'>Capacity </th>
							<th colspan='2' class='top'>Marquee</th>
							<th rowspan='2' valign='bottom' scope='col'>Coconut <br/>Matting</th>
							<th rowspan='2' valign='bottom' scope='col'>Lighting</th>
							<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>
						</tr>
						<tr><th scope='col'> Seated</th>
							<th scope='col'>Buffet</th>
							<th valign='bottom' scope='col'>Lined</th>
							<th valign='bottom' scope='col'>Unlined</th>
							<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>
							<th align='center' scope='col' class='marquee_total'>Unlined</th>
						 </tr></thead><tbody>";
						 
				
					foreach($marquee -> row as $row){
						
							$rowClass = ($rowCounter % 2 == 0)?" class='odd' ":"";
							$this -> content .= "<tr $rowClass>\n<th class='leftCol' scope='row'>";
							$this -> content.= $marquee["type"] != "chinese hat"?$marquee["width"] . "m x " . $row["length"] . "m<br/>":$row["length"] . "m x " . $row["length"] . "m<br/>";
							$this -> content .= $marquee["type"] != "chinese hat"?"<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n":"<span>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";
							$this -> content .= "<td>" . $row["seatedCap"] . "</td>\n";
							$this -> content .= "<td >" . $row["buffetCap"] . "</td>\n";
							$this -> content .= $row["linedP"] != "N/A"?"<td>&pound;" . $row["linedP"] . "</td>\n":"<td>" . $row["linedP"] . "</td>\n";
							$this -> content .= $row["unlinedP"] != "POA"?"<td>&pound;" . $row["unlinedP"] . "</td>\n":"<td>" . $row["unlinedP"] . "</td>\n";
							$this -> content .= $row["mat"] != "POA"?"<td >&pound;" . $row["mat"] . "</td>\n":"<td>" . $row["mat"] . "</td>\n";
							$this -> content .= $row["light"] != "POA"?"<td>&pound;" . $row["light"] . "</td>\n":"<td>" . $row["light"] . "</td>\n";		
							$this -> content .= $row -> totals["lined"] != "N/A"?"<td  class='marquee_total leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n":"<td  class='marquee_total leftBorder'>" . $row -> totals["lined"] . "</td>\n";
							$this -> content .= $row -> totals["unlined"] != "POA"?"<td  class='marquee_total'>&pound;" . $row -> totals["unlined"] . "</td>\n":"<td  class='marquee_total'>" . $row -> totals["unlined"] . "</td>\n";
							$this -> content .= "</tr>\n";
							$rowCounter++;
						}		
					$this -> content .= "</tbody></table>\n<p class='price_bytable'>Please <a href='/contact_us.htm'>contact us</a> for a personalised quotation.</p>\n";
					
					}
				
				$this -> content .= $this -> end_terms . $this -> pricingCall;
				return $this -> content;
			
			}
			
			
			public function GetFurniturePriceList() {
			
				
				foreach ($this -> xml -> group as $furnitureGroup) {
					
					$this -> content .= "<table border='0' cellspacing='0' cellpadding='0' summary=\"Prices for marquee tables, chairs and dancefloors\" class='furn' >";
					$this -> content .= "<caption>Marquee ". $furnitureGroup["name"] . " price list</caption>";
					
					$classCounter = 0;
					foreach($furnitureGroup -> item as $item){
						//rows with no price bolded
						switch ($item["price"]) {
							case "":
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'><strong>";
								$this -> content.= $item . "</strong></th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' ></td>\n";
								break;
							case "POA":
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";
								$this -> content.= $item . "</th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "</td>\n";
								break;
							case "Standard flooring":
							case "Standard lighting":
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";
								$this -> content.= $item . "</th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "<br/>See <a href='/pricing/marquees.htm'>marquee pricing</a></td>\n";
								break;
							default:
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";
								$this -> content.= $item . "</th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";
						}
						$this -> content .= "</tr>\n";
						$classCounter = $classCounter == 0?1:0;
					}		
				$this -> content .= "</table>\n";
				}
				
				$this -> content .= $this -> furn_end_terms . $this -> pricingCall;
				return $this -> content;
			
			}
			
			public function GetEquipmentPriceList() {
			
				
				foreach ($this -> xml -> group as $furnitureGroup) {
					
					$this -> content .= "<table border='0' cellspacing='0' cellpadding='0' summary=\"Prices for marquee tables, chairs and dancefloors\" class='furn' >";
					$this -> content .= "<caption>". $furnitureGroup["name"] . "</caption>";
					
					$classCounter = 0;
					foreach($furnitureGroup -> item as $item){
						//rows with no price bolded
						switch ($item["price"]) {
							case "":
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'><strong>";
								$this -> content.= $item . "</strong></th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' ></td>\n";
								break;
							case "POA":
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";
								$this -> content.= $item . "</th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "</td>\n";
								break;
							default:
								$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";
								$this -> content.= $item . "</th>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";
						}
						
						$this -> content .= "</tr>\n";
						$classCounter = $classCounter == 0?1:0;
					}		
				$this -> content .= "</table>\n";
				}
				
				$this -> content .= $this -> furn_end_terms . $this -> pricingCall;
				return $this -> content;
			
			}
			
			
			public function GetSamples() {
			
				
				foreach ($this -> xml -> sample as $sample) {
					
					
					$this -> content .= "<table border='0' cellspacing='0' cellpadding='0'  class='furn' summary=\"Example marquee prices for " . strtolower($sample["title"]) . "\" id=\"" . $sample["id"] . "\">";
					$this -> content .= "<caption>" . $sample["title"] . "</caption>";
					
					
					$classCounter = 0;
					foreach($sample -> item as $item){
						$this -> content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";
						$this -> content.= $item . "</th>";
						$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";
						$this -> content .= "</tr>\n";
						$classCounter = $classCounter == 0?1:0;
					}
					
					$this -> content .= "<tr><th class='damage furnLeft" . $classCounter . "' scope='row'>Damage waiver @ 5%</th>";
					$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $sample["damage"] . "</td></tr>\n";
					$this -> content .= "<tr><th class='total' scope='row'>Total</th>";
					$this -> content .= "<td class='total' >&pound;" . $sample["total"] . "</td></tr>\n";
				$this -> content .= "</table>\n";
				}
				
				$this -> content .= $this -> pricingCall;
				
				return $this -> content;
			
			}
			
		}
		
		class Get_pdf_xml extends Get_xml {
		
			protected $coDetails = "<p class='codetails' align='center'>020 7267 4271 ~ www.countymarquees.com ~ 01243 790290</p>";
			
			function __construct($xml_file) {
				parent::__construct($xml_file);
			}
			
			
				public function GetMarquee() {
			
				$this -> content = $this -> coDetails;
				$this -> content .= "<h1>Marquee prices</h1>";
				
				foreach ($this -> xml -> marquee as $marquee) {
					
					if ($marquee["type"] != "chinese hat") {
						$this -> content .= "<br/><table cellspacing='0' cellpadding='3' align='center' class='marquee' style='page-break-after:always'>";
					} else {
						$this -> content .= "<br/><table cellspacing='0' cellpadding='3' align='center' class='marquee'>";
					}
					
					//$this -> content .= "<table cellspacing='0' cellpadding='3' align='center' class='marquee'>";
					
					$tableHeader = $marquee["type"] != "chinese hat"?"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees":"Prices for 3m or 6m/10' or 20' chinese hat marquees";
					$tableHeader .= " - for " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people";
					$this -> content .= "<tr><td colspan='9' class='white' align='left'>$tableHeader</td></tr>";
					
					$this -> content .= "<tr>
						<td  align='center' class='head'>Marquee size<br/><span style='font-size:8pt'>Width x<br/>Length</span></td>
						<td colspan='2' align='center' class='head'>Capacity </td>
						<td colspan='2' align='center' class='head'>Marquee</td>
						<td class='head'></td>
						<td  class='head'></td>
						<td colspan='2' align='center'  class='marqRightHead'>Total hire price</td>
					</tr>";
					$this -> content .= "<tr>
						<td class='head'> </td>
						<td  class='head'> Seated</td>
						<td  class='head'>Buffet</td>
						<td valign='bottom' class='head'>Lined</td>
						<td valign='bottom' class='head'>Unlined</td>
						<td valign='bottom' class='head'>Matting</td>
						<td valign='bottom' class='head'>Lighting</td>
						<td align='center'  class='marqRightHead'>Lined</td>
						<td align='center'  class='marqRightHead'>Unlined</td>
					 </tr>";
					
					$classCounter = 0;
					if ($marquee["type"] != "chinese hat") {
						foreach($marquee -> row as $row){
							$this -> content .= "<tr>\n<td class='headLeft'>";
							$this -> content.= $marquee["width"] . "m x " . $row["length"] . "m<br/>";
							$this -> content .= "<span style='font-size:8pt'>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["seatedCap"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["buffetCap"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>&pound;" . $row["linedP"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>&pound;" . $row["unlinedP"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>&pound;" . $row["mat"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>&pound;" . $row["light"] . "</td>\n";			
							$this -> content .= "<td class='marqRight" . $classCounter . "'>&pound;" . $row -> totals["lined"] . "</td>\n";
							$this -> content .= "<td class='marqRight" . $classCounter . "'>&pound;" . $row -> totals["unlined"] . "</td>\n";
							$this -> content .= "</tr>\n";
							
							$classCounter = $classCounter == 0?1:0;
						}
					} else {
						foreach($marquee -> row as $row){
							$this -> content .= "<tr>\n<td class='headLeft'>";
							$this -> content.= $row["length"] . "m x " . $row["length"] . "m<br/>";
							$this -> content .= "<span style='font-size:8pt'>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["seatedCap"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["buffetCap"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["linedP"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["unlinedP"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["mat"] . "</td>\n";
							$this -> content .= "<td class='marqLeft" . $classCounter . "'>" . $row["light"] . "</td>\n";			
							$this -> content .= "<td class='marqRight" . $classCounter . "'>" . $row -> totals["lined"] . "</td>\n";
							$this -> content .= "<td class='marqRight" . $classCounter . "'>" . $row -> totals["unlined"] . "</td>\n";
							$this -> content .= "</tr>\n";
							
							$classCounter = $classCounter == 0?1:0;
						}
					}		
				$this -> content .= "</table><br/>\n";
				}
				
				$this -> content .= "<br/></br><ul class='marquee'>
				<li>All prices include the erection and dismantlement of marquees</li>
				<li>Lighting is for chandeliers or uplighters. More lighting options are available. See www.countymarquees.com</li>
				<li>Matting is for plain or striped coconut matting. More flooring options are available. See www.countymarquees.com</li>
				<li >Marquees can be joined to provide widths of up to 27m/100'.</li>
				<li>Allow extra space for reception areas, dance floors, buffet tables etc.</li>
				<li>A non-refundable deposit is required to confirm  marquee hire bookings. The balance must be settled on the day of erection.</li>				
				<li>All prices are subject to current rates of VAT.</li>
				<li>Damage waiver  is 5% + VAT on the final invoice</li>
				</ul>";

				
				return $this -> content;
			
			}
			
			
			public function GetFurniture() {
				
				$this -> content = $this -> coDetails;
				$this -> content .= "<h1>Furniture prices</h1>";
				foreach ($this -> xml -> group as $furnitureGroup) {
				
					if ($furnitureGroup["name"] != "tables" && $furnitureGroup["name"] != "lighting"  && $furnitureGroup["name"] != "flooring") {
						$this -> content .= "<br/><table cellspacing='0' cellpadding='5' align='center' style='page-break-after:always;margin-bottom:0;padding-bottom:0'>";
					} else {
						$this -> content .= "<br/><table cellspacing='0' cellpadding='5' align='center'>";
					}
					
					$this -> content .= "<tr><td colspan='2' class='white'><h2>" . ucfirst($furnitureGroup["name"]) . "</h2></td></tr>";
					
					$classCounter = 0;
					foreach($furnitureGroup -> item as $item){
						switch ($item["price"]) {
							case "":
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "' height='50px'><strong>";
								$this -> content.= $item . "</strong></td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' ></td>\n";
								break;
							case "POA":
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "'>";
								$this -> content.= $item . "</td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "</td>\n";
								break;
							case "Standard flooring":
							case "Standard lighting":
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "'>";
								$this -> content.= $item . "</td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "<br/>See marquee prices</td>\n";
								break;
							default:
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "' >";
								$this -> content.= $item . "</td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";
						}
						$this -> content .= "</tr>\n";
						$classCounter = $classCounter == 0?1:0;
					}		
				$this -> content .= "</table><br/>\n";
				}
				
				$this -> content .= "<br/></br><ul>
				<li>All prices are subject to current rate of VAT</li>
				<li>All equipment is hired out subject to our Standard Terms and Conditions of 
				hire </li>
				</ul>";

				
				return $this -> content;
			
			}
			
			public function GetEquipment() {
			
				$this -> content = $this -> coDetails;			
				$this -> content .= "<h1>Equipment prices</h1>";
				
				foreach ($this -> xml -> group as $furnitureGroup) {
					
					if ($furnitureGroup["name"] != "Auxiliary marquees" && $furnitureGroup["name"] != "Generators and power" && $furnitureGroup["name"] != "Ancillary equipment") {
						$this -> content .= "<br/><table cellspacing='0' cellpadding='5' align='center' style='page-break-after:always'>";
					} else {
						$this -> content .= "<br/><table cellspacing='0' cellpadding='5' align='center'>";
					}
					$this -> content .= "<tr><td colspan='2' class='white'><h2>" . $furnitureGroup["name"] . "</h2></td></tr>";
					
					$classCounter = 0;
					foreach($furnitureGroup -> item as $item){
						switch ($item["price"]) {
							case "":
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "' height='30px' valign='middle'><strong>";
								$this -> content.= $item . "</strong></td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' ></td>\n";
								break;
							case "POA":
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "' >";
								$this -> content.= $item . "</td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "</td>\n";
								break;
							default:
								$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "' >";
								$this -> content.= $item . "</td>";
								$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";
						}
						
						$this -> content .= "</tr>\n";
						$classCounter = $classCounter == 0?1:0;
					}		
				$this -> content .= "</table><br/>\n";
				}
				
				$this -> content .= "<br/></br><ul>
				<li>All prices are subject to current rate of VAT</li>
				<li>All equipment is hired out subject to our Standard Terms and Conditions of 
				hire </li>
				</ul>";
				
				return $this -> content;
			
			}
			
			public function GetSamples() {
				
				$this -> content = $this -> coDetails;
				$this -> content .= "<h1>Sample marquee prices</h1>";
				
				foreach ($this -> xml -> sample as $sample) {
					
					//$logo = $this -> cmLogo;
					
				//$this -> content .= "<img src='" . $_SERVER['DOCUMENT_ROOT'] . "'/images/logo350.png'>";
					$this -> content .= "<table border='0' cellspacing='0' cellpadding='5' align='center' class='sample'>";
					$this -> content .= "<tr><td colspan='2' class='white'><h2>" . $sample["title"] . "</h2></td></tr>";
					
					$classCounter = 0;
					foreach($sample -> item as $item){
						
						$this -> content .= "<tr>\n<td class='furnLeft" . $classCounter . "' >";
						$this -> content.= $item . "</td>";
						$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";
						$this -> content .= "</tr>\n";
						$classCounter = $classCounter == 0?1:0;
					}
					
					$this -> content .= "<tr><td class='damage furnLeft" . $classCounter . "'>Damage waiver @ 5%</td>";
					$this -> content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $sample["damage"] . "</td></tr>";
					$this -> content .= "<tr><td class='total' style='border:none;border-top:1px solid #D6D6D3'>Total</td>";
					$this -> content .= "<td class='totalResult'  style='border:none;border-top:1px solid #D6D6D3'>&pound;" . $sample["total"] . " + VAT</td></tr>\n";
				$this -> content .= "</table>\n";
				}
				
				return $this -> content;
			
			}
			
			public function GetInfo() {
				
				$this -> content = $this -> coDetails;
				$this -> content .= "<h1>Information sheet</h1>";
				$this -> content .= "<table border='0' cellspacing='0' cellpadding='5' align='center' class='sample'>";
				
				foreach ($this -> xml -> section as $section) {
						
						$this -> content .= "<tr><td>\n";
						$this -> content.= (string)$section . "</td>";
						$this -> content .= "</tr>\n";
				
				}
				$this -> content .= "</table>\n";
				return $this -> content;
			
			}
			
		}
		
		
		
		
		
		
		class Get_venues_xml extends Get_xml {
		
			private $classCounter = 0;
			private $first = true;
			private $areas = array("wSussex" => "WEST SUSSEX", "eSussex" => "EAST SUSSEX", "beds" => "BEDFORDSHIRE", "berks" => "BERKSHIRE", "lon" => "LONDON", "herts" => "HERTFORDSHIRE", "bucks" => "BUCKINGHAMSHIRE", "kent" => "KENT", "surrey" => "SURREY", "oxford" => "OXFORDSHIRE", "essex" => "ESSEX", "hants" => "HAMPSHIRE",);
			
			function __construct($xml_file) {
				parent::__construct($xml_file);
			} 
			
			public function GetVenues($xpath_expression = false) {				
				
				if (!$xpath_expression) {
					foreach ($this -> xml -> marker as $venue) {					
						$this -> GetVenue($venue);
					}
				} else {
					foreach ($this -> xml -> xpath($xpath_expression) as $venue) {
						$this -> GetVenue($venue, true);
					}
				}
				
				
				//***************ERRORS
				if ($this -> content == "") {
					if ($xpath_expression) {
						$rogueArea = substr($xpath_expression,14);
						$rogueArea = substr($rogueArea, 0, -2);
						if (array_key_exists($rogueArea, $this -> areas)) {
							$this -> content = <<<EOF
							<p>Sorry. There has been a system error. Please refresh the page to try again.</p>
EOF;
						} else {
							$rogueArea = ucfirst($rogueArea);
							$this -> content = <<<EOF
							<p>$rogueArea is not a county!</p>
EOF;
						}
					} else {
						$this -> content = <<<EOF
							<p>Sorry. There has been a system error. Please refresh the page to try again.</p>
EOF;
					}
				}
				
				return $this -> content;
			
			}
			
			
			private function GetVenue($venue,$xpath = false) {
			
				$details = $venue -> details;
						$area = $venue["area"];
						$area = $this -> areas[(string)$area];
						//if (!$xpath) {
							$details = str_replace ("</h3>" , "<span>$area</span></h3>", $details);
						//}
						$pricingBit = "";
						switch ($venue["price"]) {
							case 0:
								$pricingBit = "<p class='price cheap'>Venue cost:</p></div>";
								break;
							case 1:
								$pricingBit = "<p class='price'>Venue cost:</p></div>";
								break;
							case 2:
								$pricingBit = "<p class='price expensive'>Venue cost:</p></div>";
								break;
							default:
								$pricingBit = "</div>";
						}
						$extras = "<div class='extras'>";
						$extras .= $venue["civil"] == 1?"<p class='civil'>Civil license:</p>":"<p class='civil no'>Civil license:</p>";
						$extras .= $venue["large"] == 1?"<p class='civil'>Large marquees:</p>":"<p class='civil no'>Large marquees:</p>";
						$extras .= $pricingBit;
						if (!$this -> first) {
							$this -> content .= "\n<div class='testvenue" . $this -> classCounter . "'>" . $details . $extras . "</div>\n";
						} else {
							$this -> content .= "\n<div class='testvenue" . $this -> classCounter . "' style='margin-top:0;'>" . $details . $extras . "</div>\n";
							$this -> first = false;
						}
						
						$this -> classCounter = $this -> classCounter == 0?1:0;
			
			}
			
		}
		
		class Get_onlinequote_xml extends Get_xml {
			
			private $marquee_table_top = "<tbody><thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>
							<th colspan='2' class='odd top'>Capacity </th>
							<th colspan='2' class='top'>Marquee</th>
							<th rowspan='2' valign='bottom' scope='col'>Coconut <br/>Matting</th>
							<th rowspan='2' valign='bottom' scope='col'>Lighting</th>
							<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>
						</tr>
						<tr><th scope='col'> Seated</th>
							<th scope='col'>Buffet</th>
							<th valign='bottom' scope='col'>Lined</th>
							<th valign='bottom' scope='col'>Unlined</th>
							<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>
							<th align='center' scope='col' class='marquee_total'>Unlined</th>
						 </tr></thead>";
			
			function __construct($xml_file) {
				parent::__construct($xml_file);
			}
					
			
			public function GetMarquees($xpath_expr) {
				
				$rowCounter = 1;
				$anyResults = 0;
				$marquees = array();
				
				foreach ($this -> xml -> marquee as $marquee) {
				
					$xmlMarquee = simplexml_load_string($marquee->asXML());
					
					if ( $marquee["type"] == "chinese hat") {
						continue;
					}
					
					$rows = $xmlMarquee-> xpath($xpath_expr);
					
					
					if (count($rows) > 0) {
						
						$anyResults += 1;
						 
						foreach ($rows as $row) {
							
							$marquees[] = new marquee_mapper(
												$marquee["type"],
												$marquee["width"],
												$row["length"],
												$marquee["footWidth"],
												$row["footLength"],
												$row["mat"],
												$row -> totals["lined"],
												$row["seatedCap"]);
						}			
					
					}
				}
				
				if ($anyResults == 0) {
					return("<h3>No matches!</h3><p>Sorry. No marquees matched your query.</p>");
				}
				return $marquees;
				//return $this -> content;
			
			}
			
			
			public function GetDimsMarquees($intWidth, $intLength) {
				
				$rowCounter = 1;
				$anyResults = 0;
				$testing = "";
				//$xml->xpath('//character') as $character)
				foreach ($this -> xml -> xpath("//marquee[@width = $intWidth]") as $marquee) {
				//foreach ($this -> xml -> marquee as $marquee) {
				
					$xmlMarquee = simplexml_load_string($marquee->asXML());
					
					if ( $marquee["type"] == "chinese hat") {
						continue;
					}
					
					$rows = $xmlMarquee-> xpath("row[@length  = $intLength]");
					if (count($rows) > 0) {
						
						$anyResults += 1;						
						
						 
						foreach ($rows as $row) {
							
							$marquees[] = new marquee_mapper(
												$marquee["type"],
												$marquee["width"],
												$row["length"],
												$marquee["footWidth"],
												$row["footLength"],
												$row["mat"],
												$row -> totals["lined"],
												$row["seatedCap"] );
							}
					}
				}
				
				if ($anyResults == 0) {
					return("<h3>No matches!</h3><p>Sorry. No marquees matched your query.</p>");
				}
				return $marquees;
			}
				
}


	class marquee_mapper {

		public $type;
		public $mWidth;
		public $mLength;
		public $fWidth;
		public $fLength;
		public $mat;
		public $total;		
		public $seatedCap;
		 
			public function __construct ($type = false,
				$mWidth = false,
				$mLength = false,
				$fWidth = false,
				$fLength = false,
				$mat = false,
				$total = false,
				$seatedCap = false) {
			
		
				$this -> type = (string)$type;
				$this -> mWidth = (string)$mWidth;
				$this -> mLength = (string)$mLength;
				$this -> fWidth = (string)$fWidth;
				$this -> fLength = (string)$fLength;
				$this -> mat = (string)$mat;
				$this -> total = (string)$total;
				$this -> seatedCap = (string)$seatedCap;
			}
			
		}


	class Get_onlinefurnquote_xml extends Get_xml {
		
			private $furnretval = "";
			public $furnVal;
			
			function __construct($xml_file) {
				parent::__construct($xml_file);
			}
			
			private function GetDanceFloor ($intWidth) {
				if ($intWidth == "4") {
					  return 100; //10x10
				  } elseif ($intWidth == "6") {
					  return 225; //15x15
				  } elseif ($intWidth  == "9") {
					  return 400; //20x20
				  } elseif ($intWidth  == "12") {
					  return 400; //20x20
				  } else {
					  return 576; //24x24
				}
			}
			
			private function GetDanceFloorL ($intWidth) {
				if ($intWidth == "4") {
					  return 10; //10x10
				  } elseif ($intWidth == "6") {
					  return 15; //20x20
				  } elseif ($intWidth  == "9") {
					  return 20; //20x20
				  } elseif ($intWidth  == "12") {
					  return 20; //20x20
				  } else {
					  return 24; //24x24
				}
			}
			
			public function GetFurn($arrExtras, $marquee, $seated, $numbers = false, $email = false) {
				
				$this -> furnretval = "";
				$total = 0;
				//this here instead of get marquees cos need to adjust for hard floor
				if (!$email) {
					$break = "<br/>";
					$pound = "&pound;";
					$em = "<em>";
					$end_em = "</em>";
				} else {
					$break = "\n";
					$pound = "�";
					$em = "";
					$end_em = "";
				}
				
				$marquees_end = "Fully lined, matted and lit (candelabras or uplighters) $break";
				$dancefloor_chosen = false;
				$estimated_nums = ""; //for tables, chairs and toilets when size specified
				
				//tables, chairs 
				if ($seated == "1") {
					$node = $this -> xml -> tables;
					$chair_node = $this -> xml -> chairs;
					
					//for marquees with specified w x h
					if (!$numbers) {
						$numbers = (int)$marquee -> seatedCap;
						$estimated_nums = "$em(estimated numbers)$end_em";
					}
					
					$table_nums = ceil($numbers / 10);
					$tables_cost = (float)$node * $table_nums;
					$chairs_cost = (float)$chair_node * $numbers;
					
					
					
					$this -> furnretval .= "$table_nums 5' round tables $pound" . sprintf("%01.2f", $tables_cost) . " $estimated_nums $break";					
					$this -> furnretval .= "$numbers banqueting chairs: $pound" . sprintf("%01.2f", $chairs_cost) . " $estimated_nums $break";
					
					$total += $chairs_cost + $tables_cost;
				}
				
							
				if (in_array("Dance floor", $arrExtras )) {
					$node = $this -> xml -> dance;
					$square_feet = $this -> GetDanceFloor ($marquee -> mWidth);
					$df_length = $this -> GetDanceFloorL ($marquee -> mWidth);
					$dancefloor_chosen = true;
					$price = (float)$node * $square_feet;
					$total += $price;
					$this -> furnretval .= "$df_length' x $df_length' wooden dance floor: $pound" . sprintf("%01.2f", $price) . $break;
				}
				if (in_array("Parquet", $arrExtras )) {
					$node = $this -> xml -> parquet;
					$square_feet = $this -> GetDanceFloor ($marquee -> mWidth);
					$df_length = $this -> GetDanceFloorL ($marquee -> mWidth);
					$dancefloor_chosen = true;
					$price = (float)$node * $square_feet;
					$total += $price;
					$this -> furnretval .= "$df_length' x $df_length' parquet/black &amp; white dance floor: $pound" . sprintf("%01.2f", $price) . $break;
				}
				if (in_array("Starlight", $arrExtras )) {
					if ((int)$marquee -> fWidth != 50) {
						$node = $this -> xml -> starlight;
					} else {
						$node = $this -> xml -> starlight50;
					}
					
					$length_num = ((int)($marquee -> fLength))/10;
					$price = (float)$node * $length_num;
					$this -> furnretval .= "Full starlight ceiling: $pound" . sprintf("%01.2f", $price) . $break;
					$total += $price;
				}
				if (in_array("Starlight dancefloor", $arrExtras )) {
					if (!$dancefloor_chosen) {
						 $this -> furnretval .= "$em No starlight ceiling over dancefloor because dance floor not selected$end_em" . $break;
					} elseif (in_array("Starlight", $arrExtras )) {
						//do nothing
					} else {
					  if ((int)$marquee -> fWidth != 50) {
						$node = $this -> xml -> starlight;
					  } else {
						  $node = $this -> xml -> starlight50;
					  }
					  $df_length = $this -> GetDanceFloorL ($marquee -> mWidth);
					  $price = (float)$node * ($df_length/10);
					  $this -> furnretval .= "Starlight ceiling over dancefloor: $pound" . sprintf("%01.2f", $price) . $break;
					  $total += $price;
					}
				}
				if (in_array("Hard", $arrExtras )) {
					$node = $this -> xml -> hard;
					$square_feet = (int)($marquee -> fWidth) * (int)($marquee -> fLength);
					$price = (float)$node * $square_feet;
					$this -> furnretval .= "Hard floor and carpet: $pound" . sprintf("%01.2f", $price) . $break;
					$total += $price - (int)$marquee -> mat;
					$marquees_end = "Fully lined, lit (candelabras or uplighters) with hard floor and carpet throughout $break";
				}
				if (in_array("Bar", $arrExtras )) {					
					$node = $this -> xml -> bar;
					$total += (int)$node;
					$this -> furnretval .= "One mahogany effect bar unit with shelf: $pound" . sprintf("%01.2f", $node) . $break;
				}
				if (in_array("Catering tent", $arrExtras )) {
					$node = $this -> xml -> catering_tent;					
					$total += (int)$node;
					$this -> furnretval .= "One 20' x 10' catering Tent (matted and lit): $pound" . sprintf("%01.2f", $node) . $break;
				}
				if (in_array("Stage", $arrExtras )) {
					$node = $this -> xml -> stage;					
					$this -> furnretval .= "Two stage units: $pound" . sprintf("%01.2f", $node) . $break;
					$total += (int)$node;
				}
				if (in_array("Generator", $arrExtras )) {
					$node = $this -> xml -> generator;					
					$total += (int)$node;
					$this -> furnretval .= "17 KVA + Generator (includes distribution and cabling): $pound" . sprintf("%01.2f", $node) . $break;
				}
				if (in_array("Heater", $arrExtras )) {
					$node = $this -> xml -> heater;
					$total += (int)$node;
					$this -> furnretval .= "Heater (12 hours fuel included): $pound" . sprintf("%01.2f", $node) . $break;
				}	
				//toilets here cos need numbers
					if (in_array("Toilets", $arrExtras )) {						
						
						//for marquees with specified w x h
						if (!$numbers) {
							$numbers = (int)$marquee -> seatedCap;
							$estimated_nums = "$em(based on estimated numbers)$end_em";
						}
					
						switch(true) {
							case ($numbers < 100):
					  			$node = $this -> xml -> loo1x1;
								$this -> furnretval .= "1 1+1 Trailer Loo (1 ladies, 1 gents &amp; urinals): $pound" . sprintf("%01.2f", $node) . " $estimated_nums $break";
					  			$total += (int)$node;
								break;
							case ($numbers < 175):
					  			$node = $this -> xml -> loo2x1;
								$this -> furnretval .= "1 2+1 Trailer Loo (2 ladies, 1 gents &amp; urinals): $pound" . sprintf("%01.2f", $node) . " $estimated_nums $break";
					  			$total += (int)$node;
								break;
							case ($numbers < 250):
					  			$node = $this -> xml -> loo3x1;
								$this -> furnretval .= "1 3+1 Trailer Loo (3 ladies, 1 gents &amp; urinals): $pound" . sprintf("%01.2f", $node) . $break;
					  			$total += (int)$node;
								break;
							case ($numbers < 400):
					  			$node = $this -> xml -> loo2x1;
								$this -> furnretval .= "1 2+1 Trailer Loo (2 ladies, 1 gents &amp; urinals): $pound" . sprintf("%01.2f", $node) . $break;
					  			$total += (int)$node;
					  			$node = $this -> xml -> loo3x1;
								$this -> furnretval .= "1 3+1 Trailer Loo (3 ladies, 1 gents &amp; urinals): $pound" . sprintf("%01.2f", $node) . $break;
					  			$total += (int)$node;
								break;
							default:
					  			$node = $this -> xml -> loo3x1;
								$loos_cost = (float)$node * 2;
								$this -> furnretval .= "2 3+1 Trailer Loos (3 ladies, 1 gents &amp; urinals): $pound" . sprintf("%01.2f", $loos_cost) . $break;
					  			$total += $loos_cost;
								break;
						}
						
						$this -> furnretval .= "$em (Toilet units estimated)$end_em $break";
					  
				  }
				
				
				
			
				$this -> furnVal = $total;
				return $marquees_end . $this -> furnretval;
				
			}
			
	}
		
		

?>
