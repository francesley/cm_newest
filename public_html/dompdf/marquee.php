<?php
require_once 'dompdf/autoload.inc.php';
require_once 'get_xml.php';
use Dompdf\Dompdf;
$priceListGetter = "";
$fileName = "";
$current = "";

if (!isset($_REQUEST["type"])) {
	die("what?");
} else {
	$current = $_REQUEST["type"];
}


switch ($current) {
	case "marquee":
		$priceListGetter = new Get_pdf_xml('xml/marquee_prices.xml');		
		$fileName = "marquee_prices.pdf";
		break;
	default:
		die("what?");
}
//$priceListGetter = new Get_pdf_xml('../xml/sample_prices.xml');

if ($priceListGetter -> xml) {
	$functionName = "Get" . ucfirst($current);
	$content = $priceListGetter -> $functionName();


	$html = <<<EOF
	  <html>
	  <head>
	  <style>
			td, th {
					padding:2pt;
					text-align:center;
					font-size:12pt;
					font-weight:normal;
					font-family: 'helvetica', 'times';
				}
				td.nobreak{
						white-space:nowrap;
				}
				th {
					background-color:#ccc;
				}
				th.odd {
					background-color:#aaa;
				}
				td.odd {
					background-color:#bbb;
				}
				
				td.white {
					background-color:#fff;
					border:none;
				}
				td.total, td.totalResult {					
					font-weight:bold;
					background-color:#e5e5e5;
					border: solid thick white;
					text-align:right;
				}
				
				
				
				table {
					 margin:0 0 25px 0;
					border-collapse:collapse;
					width:100%;
				}
				
				p {
					font-size:12pt;
					line-height:1.4em;
					margin:0 0 25px 0;
				}
				p.codetails {
					font-size:10pt;
					line-height:1.4em;					
					font-family: 'helvetica', 'times';
					margin:0 0 30px 0;
				}
				h1 {
				  text-align: center;
				  color: black;
				  font-style: normal;
				  font-size: 2em;
				  margin-top:0;
				  margin-bottom:10px;
				  font-family: 'helvetica', verdana, sans-serif;
				}
				h2 {
				  text-align: left;
				  color: black;
				  font-style: normal;
				  font-size: 1.5em;
				  margin:0;
				  font-family: 'helvetica', verdana, sans-serif;
				}
				body {
					margin-top:10px;
					padding:40px 0 10px 0;
					background-image: url('https://www.countymarquees.com/genpdf/logoNew.png'); background-repeat: no-repeat; background-position: center 0;
				}
</style>
	
	  </head>
	  <body>
	 

EOF;
$html .= $content;

$html .= <<<EOF
<script type="text/php">

if ( isset(\$pdf) ) {
  \$font = Font_Metrics::get_font("helvetica");
  \$size = 9;
  \$color = array(0,0,0);
  \$text_height = Font_Metrics::get_font_height(\$font, \$size);

  \$foot = \$pdf->open_object();
  
  \$w = \$pdf->get_width();
  \$h = \$pdf->get_height();

  // Draw a line along the bottom
  \$y = \$h - 2 * \$text_height - 24;
  \$pdf->line(16, \$y, \$w - 16, \$y, \$color, 1);

  \$y += \$text_height;

  \$text = "County Marquees 2009";
  \$pdf->text(16, \$y, \$text, \$font, \$size, \$color);
  
  \$text2 = "020 7267 4271 ~ www.countymarquees.com";
  \$text_width = Font_Metrics::get_text_width(\$text2, \$font, \$size);
  \$xPos = \$w - 16 - \$text_width;
  \$pdf->text(\$xPos, \$y, \$text2, \$font, \$size, \$color);

  \$pdf->close_object();
  \$pdf->add_object(\$foot, "all");
 
  // Mark the document as a duplicate
  //\$pdf->text(110, \$h - 240, "DUPLICATE", Font_Metrics::get_font("helvetica", "bold"),110, array(0.85, 0.85, 0.85), 0, -52);

  \$text = "Page {PAGE_NUM} of {PAGE_COUNT}";  

  // Center the text
  \$width = Font_Metrics::get_text_width("Page 1 of 2",\$font, \$size);
  \$pdf->page_text(\$w / 2 - \$width / 2, \$y, \$text, \$font, \$size, \$color);
  
}
</script>
EOF;
$html .= "</body></html>";	
	
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($html);
	
	
	$dompdf->render();
	$dompdf->stream($fileName);
}

?>
