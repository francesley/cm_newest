<?php
// THIS FILE NEEDS PHP 7.1 TO WORK AND WAS CREATED BY VIKAS LOCAL MACHINE
error_reporting(-1);
require_once 'dompdf/autoload.inc.php';
require_once 'get_xml.php';
use Dompdf\Dompdf;
$priceListGetter = "";
$fileName = "";
$current = "";

if (!isset($_GET["type"])) {
	die("what?");
} else {
	$current = $_GET["type"];
	/*print_r (scandir(DOMPDF_FONT_DIR));
	exit();*/
}


switch ($current) {
	case "samples":
		$priceListGetter = new Get_pdf_xml('../xml/sample_prices.xml');
		$fileName = "sample_prices.pdf";
		break;
	case "equipment":
		$priceListGetter = new Get_pdf_xml('../xml/equipment_prices.xml');		
		$fileName = "equipment_prices.pdf";
		break;
	case "furniture":
		$priceListGetter = new Get_pdf_xml('../xml/furniture_prices.xml');		
		$fileName = "furniture_prices.pdf";
		break;
	case "marquee":
		$priceListGetter = new Get_pdf_xml('../xml/marquee_prices.xml');		
		$fileName = "marquee_prices.pdf";
		break;
	// case "info":
		// $priceListGetter = new Get_pdf_xml('xml/infosheet.xml');		
		// $fileName = "info_sheet.pdf";
		// break;
	default:
		die("what?");
}




if ($priceListGetter -> xml) {
	$functionName = "Get" . ucfirst($current);
	$content = $priceListGetter -> $functionName();
	
	/*if ($current == "info") {
		die($content);
	}*/

//width 100% messes up everything; page-break-inside doesnt work;
	$html = <<<EOF
	  <html>
	  <head>
	  <style>
	  		ul {
				font-size:10pt;
				font-family: 'helvetica';
				width:80%;
				margin:25px auto 0 auto;
			}
			ul.marquee {
			width:90%;
			}
			ul.infosheet {
					font-size:12pt;
					line-height:1.4em;
					margin:0 0 25px 0;
			}
			ul.infosheet li{
				margin:0 0 12pt 0;
				padding:0;
			}
			td {
					padding:5pt;
					text-align:left;
					font-size:11pt;
					vertical-align:center;
					font-weight:normal;
					border-bottom-width:1px;
					border-bottom-color:#D6D6D3;
					border-bottom-style:solid;					
					font-family: 'helvetica';
				}
				table.marquee td {
					text-align:center;
					font-size:10pt;
				}
				td.head {
					text-align:center;
					background-color:#FBFAF9;
					border-bottom-style:none;
				}
				td.headLeft {
					text-align:center;
					background-color:#FBFAF9;
				}
				td.nobreak{
						white-space:nowrap;
				}
				th.odd {
					background-color:#fff;
				}
				td.odd {
					background-color:#fff;
				}
				td.furnLeft0, td.furnLeft1{
					width:300px;
					background-color:#fff;
				}
				td.furnLeft1{
					background-color:#FBFAF9;
				}
				td.furnRight0, td.furnRight1 {
					width:150px;
					text-align:right;
					background-color:#fff;
				}
				td.furnRight1 {
					background-color:#FBFAF9;
				}
				td.marqLeft0, td.marqLeft1{
					background-color:#Fff;
				}
				td.marqLeft1{
					background-color:#FBFAF9;
				}
				td.marqRight0, td.marqRight1,td.marqRightHead {
					text-align:right;
					background-color:#fff;
					font-weight:bold;
				}
				td.marqRightHead {
					border-bottom-style:none;
					background-color:#FBFAF9;
				}
				td.marqRight1 {
					background-color:#FBFAF9;
				}
				td.white {
					background-color:#fff;
					border:none;
				}
				table.marquee td.white {
				font-size:13pt;
				color:#17515F;				
				font-style: italic;
				padding-top:10pt;
				}
				td.total, td.totalResult {
					font-weight:bold;
					background-color:#FBFAF9;
					border: solid thick white;
					text-align:right;
				}
				td.damage {
					text-align:right
				}
				
				table {
					 margin:0;
					border-collapse:collapse;
					width:100%;
					font-family: 'helvetica';
				}
				table.marquee {
					 margin:0;
					border-collapse:collapse;
					width:100%;					
					font-family: 'helvetica';
				}
				table.sample {
					margin-bottom:20px;
					font-family: 'helvetica';
				}
				td span.subhead {
					 color: #17515F;
				  	font-style: italic;
				}
				p {
					font-size:12pt;
					line-height:1.4em;
					margin:0 0 25px 0;
				}
				p.codetails {
					font-size:10pt;
					line-height:1.4em;					
					font-family: 'helvetica';
					margin:0 0 30px 0;
				}
				h1 {
				  text-align: center;
				  color: #17515F;
				  font-style: italic;
				  font-size: 2em;
				  margin-top:0;
				  margin-bottom:10px;
				  font-family:'times';
				}
				h2 {
				  text-align: left;
				  color: #17515F;
				  font-style: italic;
				  font-size: 1.3em;
				  margin:0;
				  font-family: 'helvetica';
				}
				body {

					padding:110px 0 10px 0;
					background-image: url('https://www.countymarquees.com/images/logoNew.png'); background-repeat: no-repeat; background-position: center 18px;

				}
</style>
	
	  </head>
	  <body>
	 

EOF;


$html .= $content;

$html .= <<<EOF
<script type="text/php">

if ( isset(\$pdf) ) {
  \$font = Font_Metrics::get_font("helvetica");
  \$size = 9;
  \$color = array(0,0,0);
  \$text_height = Font_Metrics::get_font_height(\$font, \$size);
  
  
  	\$w = \$pdf->get_width();
  	\$h = \$pdf->get_height();
	\$y = \$h - 2 * \$text_height;
  /*\$foot = \$pdf->open_object();
  
  // Draw a line along the bottom
  
  \$pdf->line(16, \$y, \$w - 16, \$y, \$color, 1);

  \$y += \$text_height;

  \$text = "County Marquees 2011";
  \$pdf->text(16, \$y, \$text, \$font, \$size, \$color);
  
  \$text2 = "020 7267 4271 ~ www.countymarquees.com";
  \$text_width = Font_Metrics::get_text_width(\$text2, \$font, \$size);
  \$xPos = \$w - 16 - \$text_width;
  \$pdf->text(\$xPos, \$y, \$text2, \$font, \$size, \$color);

  \$pdf->close_object();
  \$pdf->add_object(\$foot, "all");*/
 
  // Mark the document as a duplicate
  //\$pdf->text(110, \$h - 240, "DUPLICATE", Font_Metrics::get_font("georgia", "bold"),110, array(0.85, 0.85, 0.85), 0, -52);

  \$text = "Page {PAGE_NUM} of {PAGE_COUNT}";  

  // Center the text
  \$width = Font_Metrics::get_text_width("Page 1 of 2",\$font, \$size);
  \$pdf->page_text(\$w / 2 - \$width / 2, \$y, \$text, \$font, \$size, \$color);
  
}
</script>
EOF;
$html .= "</body></html>";	
// echo $html;exit;
	/*$font = Font_Metrics::get_font(null);
	echo $font;*/

	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	$dompdf->set_option('isRemoteEnabled', true);
	$dompdf->render();
	$dompdf->stream($fileName);
} else {
 echo "no xml";
}

?>
