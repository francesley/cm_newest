<?php
/**
 * Template Name: Sites Page
 */

 get_header();
?>

<div id="outerBox" class="container">
<section class="banner-section venues">
	<div class="row">
		<div class="col-sm-12">
			<div class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>');">				
			</div>
			<ul id="subMenu">
			<li class="menu_left"><a href="/wordpress/venues.htm">Marquee venue map</a></li>
			<li class="menu_left selected"><a class="selected" href="/wordpress/venues/sites.htm">Marquee venues lists</a></li>
			<li class="menu_right"><a href="/wordpress/venues/add_venue.htm">Add a venue</a></li></ul>			
		</div>
	</div>
</section>

<section class="venue-section" id="sites">

		<div class="row">
			<div class="col-sm-12">
			<div id="facets">

						<p id="switch">Switch county</p>

						<p id="inspire">See only venues close to you</p>

						<div id="pick">

							<p><a href="javascript:void(0)" id="picker">Choose a county:</a><a href="javascript:void(0)" id="close"><img src="/images/down_arrow.png" width="12" height="6" alt="Show more"></a></p>

							<div id="facetlists" style="display: none;">

								<ul class="first">

									<li><a href="/wordpress/venues/sites.htm" class="selected">All counties</a></li>

								</ul>

								<ul>

								<li><a href="/wordpress/venues/oxford.htm" id="oxford" title="Marquee venues in Oxfordshire">Oxford</a></li>

								<li><a href="/wordpress/venues/bucks.htm" id="bucks" title="Marquee venues in Buckinghamshire">Bucks</a></li>

								<li><a href="/wordpress/venues/beds.htm" id="beds" title="Marquee venues in Bedfordshire">Beds</a></li>

								<li><a href="/wordpress/venues/herts.htm" id="herts" title="Marquee venues in Hertfordshire">Herts</a></li>

								<li><a href="/wordpress/venues/essex.htm" id="essex" title="Marquee venues in Essex">Essex</a></li>

								<li><a href="/wordpress/venues/berks.htm" id="berks" title="Marquee venues in Berkshire">Berks</a></li>

								</ul>

								<ul class="rightfacet">

								<li><a href="/wordpress/venues/surrey.htm" id="surrey" title="Marquee venues in Surrey">Surrey</a></li>

								<li><a href="/wordpress/venues/lon.htm" id="lon" title="Marquee venues in London">London</a></li>

								<li><a href="/wordpress/venues/kent.htm" id="kent" title="Marquee venues in Kent">Kent</a></li>

								<li><a href="/wordpress/venues/hants.htm" id="hants" title="Marquee venues in Hampshire">Hants</a></li>

								<li><a href="/wordpress/venues/wSussex.htm" id="wSussex" title="Marquee venues in West Sussex">West Sussex</a></li>

								<li><a href="/wordpress/venues/eSussex.htm" id="eSussex" title="Marquee venues in East Sussex">East Sussex</a></li>

								</ul>

							

							</div>

						</div>

						</div>
			<h1><?php the_title(); ?></h1>
			
			
			
				<div id="text">
				<?php 
				 global $post;
			     $area = $post->post_name;			   			
				 $xml = simplexml_load_file(get_template_directory_uri().'/maps/markers.xml');
					  // var_dump($xml);
					  $counter = 0;
					  foreach($xml as $venue){
						
						if(is_page('sites')){ 
							GetVenue($venue,$counter);
							if($counter == 1){
								$counter = 0;	
							}else{
								$counter = $counter + 1; 
							}	
						}else{ 
							if(strtolower($venue["area"]) == $area){
								GetVenue($venue,$counter);
								if($counter == 1){
									$counter = 0;	
								}else{
									$counter = $counter + 1; 
								}	
							}else{
								//skip the venue
							}
							
						}					  
						  						
					  
					  }


				 ?>
				</div>
			</div>
		</div>

</section>
</div>

<?php get_footer(); ?>