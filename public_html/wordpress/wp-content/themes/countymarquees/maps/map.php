<?php


echo <<<EOQ
<table id="outerMap" cellspacing="0">
<tr><td><div id="side_bar"></div>
<div class="map_wrapper">
<div id="map" style="width: 735px; height: 650px">
</div>
<div class="map-filter-wrapper"> 
<h3>Filters:</h3>
<select id="area" class="map_filters" onchange="speedTest.SelectType(this)">
<option value="">All counties</option>
<option value="beds">Bedfordshire</option>
<option value="berks">Berkshire</option>
<option value="bucks">Buckinghamshire</option>
<option value="eSussex">East Sussex</option>
<option value="essex">Essex</option>	
<option value="hants">Hampshire</option>	
<option value="herts">Hertfordshire</option>	
<option value="kent">Kent</option>
<option value="lon">London</option>
<option value="oxford">Oxfordshire</option>	
<option value="surrey">Surrey</option>
<option value="wSussex">West Sussex</option>
</select>
<select name="types" class="map_filters" id="types" onChange="speedTest.SelectType(this)">
<option value="">All venues</option>
<option value="civil">Civil ceremonies</option>
<option value="large">Large (150+ seated)</option>
<option value="huge">Huge events</option>
<option value="not">Not expensive</option>
<option value="medium">Medium price</option>
<option value="expensive">Expensive</option>
<option value="management">Event Management</option>
</select>
</div></div></td></tr></table>
<div id="key">
<p class="wide">All capacity figures are for maximum number of seated guests</p>
</div>
<h2>Important note</h2>
<p class="wide">County Marquees supplies marquee hire, not marquee venues. The venue map and pages are provided as a courtesy service for clients and website visitors for convenience. But we do not in the normal course of events provide a marquee and venue package.</p>
<h2>Marquee venue pricing</h2>
<p class="wide">Marquee venues are priced in all sorts of different ways, from a simple ground hire charge covering just the land for a marquee, leaving you to choose and pay for all the other components of an event; to marquee wedding venues that only charge an all in price for everything - ground hire, exclusive use, civil ceremony, catering and all reception and event services. And every sort of variation in between is on offer.</p>
<p class="wide">It is therefore difficult to compare marquee venues on price and you will need to make sure you understand what is and isn't included before making a choice.</p>
<p class="wide">The price bands on the marquee venue finder are purely subjective and designed to make your choice easier. But they should not be regarded as definitive.</p>
<h2>Lists of marquee venues</h2>
<p class="wide">You can also view the marquee venues in list form &#8212; altogether, and also divided up by county:</p>
<ul id="maps">
<li><a href="/venues/sites.htm">Marquee sites list</a></li>
<li><a href="/venues/lon.htm">Marquee venues London</a></li>
<li><a href="/venues/surrey.htm">Marquee venues Surrey</a></li>
<li><a href="/venues/wSussex.htm">Marquee venues West Sussex</a></li>
<li><a href="/venues/eSussex.htm">Marquee venues East Sussex</a></li>
<li><a href="/venues/hants.htm">Marquee venues Hampshire</a></li>
<li><a href="/venues/kent.htm">Marquee venues Kent</a></li>
<li><a href="/venues/bucks.htm">Marquee venues Buckinghamshire</a></li>
<li><a href="/venues/herts.htm">Marquee venues Hertfordshire</a></li>
<li><a href="/venues/oxford.htm">Marquee venues Oxfordshire</a></li>
<li><a href="/venues/berks.htm">Marquee venues Berkshire</a></li>
<li><a href="/venues/beds.htm">Marquee venues Bedfordshire</a></li>
<li><a href="/venues/essex.htm">Marquee venues Essex</a></li>				
</ul>
<h2>Add a marquee venue</h2>
<p class="wide">The marquee venue finder is a work in progress. If you know of or represent a marquee venue that we have not included, please <a href="/venues/add_venue.htm">add it here</a>.</p>
<h2>Spotted a mistake? / remove a marquee venue</h2>
<p class="wide">We try to keep up to date, but it's a huge task. If you have found some incorrect venue details, please <a href="/contact_us.htm">let us know</a>. Similarly, if you own a marquee venue and would like to change details or remove your venue, please <a href="/contact_us.htm">contact us</a>.</p>
<h2>Link to the marquee venue finder</h2>
<p class="wide">If you would like to link to the marquee venue finder, please use the code below.</p>
<textarea cols="10" rows="10" class="lbExclude">&lt;a href="https://www.countymarquees.com/venues.htm"&gt;Marquee venues in the South East UK&lt;/a&gt;</textarea>
<p class="wide"><fb:like layout="standard" show_faces="false" width="450" action="like" colorscheme="light" href="http://www.countymarquees.com/venues.htm"></fb:like></p>
EOQ;


?>