<?php
get_header();
?>

<section id="content" role="main">

	<header class="page_header">
		<div class="container">
			<div class="term_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/images/search_icon.png"/>
			</div>
			<h1 class="page-title"><?php printf(__('Du søkte etter: %s', 'epoxy'), get_search_query()); ?></h1>
		</div>
	</header>
    <div class="container mar-50-100">

        <?php
        $program_search = get_search_query();
        global $wpdb;
        $query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT wp_posts.ID FROM wp_posts INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id) Left Join (wp_term_relationships join wp_terms on term_taxonomy_id = term_id) on wp_posts.ID=wp_term_relationships.object_id WHERE 1=1 AND (((wp_posts.post_title LIKE '%$program_search%') OR wp_terms.name LIKE '%$program_search%' OR (wp_posts.post_content LIKE '%$program_search%') OR (wp_postmeta.meta_key = 'description' AND CAST(wp_postmeta.meta_value AS CHAR) LIKE '%$program_search%'))) AND (wp_posts.post_status = 'publish') ORDER BY wp_posts.menu_order ASC";
		
        $pageposts = $wpdb->get_results($query, OBJECT);
        $x[] = 0;
        foreach ($pageposts as $pagepost) {
            $x[] = $pagepost->ID;
        }
        $args = array(
            'post__in' => $x,
            'post_type' => 'any',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
        $wp_query = new WP_Query($args);
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>

                <article class="row" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<a href="<?php the_permalink(); ?>">
					<div class="col-md-3 col-sm-4 img-sec">
						<?php if(has_post_thumbnail()){ 
							the_post_thumbnail( 'full', array('class' => 'img-responsive')); 
						 } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/not_found.png"/>
						<?php } ?>
					</div>
					<div class="col-md-9 col-sm-8">
						<header class="header">
							<h2 class="entry-title"><?php the_title(); ?></h2>
						</header>
						<section class="entry-content">
							<p><?php the_excerpt(); ?></p>
						</section>
					</div>
					</a>
                </article>

                <?php
            }
        } else {
            ?>
            <article id = "post-0" class = "post no-results not-found">
                <header class = "header">
                    <h2 class = "entry-title"><?php _e('Ingenting funnet', 'epoxy'); ?></h2>
                </header>
                <section class="entry-content">
                    <p><?php _e('Beklager, ingenting som passet ditt søk. Vær så snill, prøv på nytt.', 'epoxy'); ?></p>
                </section>
            </article>
            <?php
        }
        wp_reset_query();
        ?>
    </div>
</section>
<?php get_footer(); ?>
                