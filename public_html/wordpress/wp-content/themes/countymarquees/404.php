<?php get_header(); ?>
<div class="main_container">
	<div class="page_default">
		<div class="container">
            <div class="content_404" >
                <h3><?php _e('Oh - Page Not Found!', 'county_marquees'); ?></h3>
                <p><?php _e('The link you have tried has either moved or no longer exists.', 'county_marquees'); ?></p>
                <h1><?php _e('404', 'county_marquees'); ?></h1>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>