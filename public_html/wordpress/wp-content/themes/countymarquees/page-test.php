<?php
/**
 * Template Name: Test Page
 */

 get_header();
?>

<div id="outerBox" class="container">
<section class="banner-section venues">
	<div class="row">
		<div class="col-sm-12">
			<div class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>');">				
			</div>			
		</div>
	</div>
</section>

<section class="pricing-section" id="pricing">

		<div class="row">	
		<div class="col-sm-12">
				<h1><?php the_title(); ?></h1>	
		</div>				
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/home/real_marquees.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/home/party_marquees2.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
					
				</div>
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/home/racing_marquee.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/home/bell_tent.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/home/venues.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>			
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/pages/wedding/candlelight.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/pages/wedding/planning.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>
				<div class="col-sm-3">
					<a href="/real.htm"><img src="/images/pages/wedding/traditional.jpg" width="188" height="100" alt="Marquee ideas"></a>
					<p>See how other people style their marquees, some pretty, some festive, some themed, some brightly coloured...</p>
				</div>

			<div class="col-sm-12">			
			<div id="text">
				<?php 
				 global $post;
			     $price = $post->post_name;			   			
				 $xml = simplexml_load_file(get_template_directory_uri().'/xml/marquee_prices.xml');
				 
				$xpath = false; 	

				$class = "GetMarqueePriceList";
				
				
				
			switch ($price) {

					case "event_marquees":

						$class = "GetMarqueeSizePriceList";

						$xpath = "row[@size='large']";						

						$opener = "<p>All sizes and styles of marquees accomodating 300+ guests.</p>";

						break;

					case "smaller_marquees":

						$class = "GetMarqueeSizePriceList";

						$xpath = "row[@size='small']";						

						$opener = "<p>All sizes and styles of marquees accomodating less than 100 guests.</p>";

						break;

					case "medium_marquees":

						$xpath = "marquee[@size='medium']";						

						$opener = "<p>All sizes and styles of marquees accomodating between 75 and 600 guests</p>";

						break;

					case "traditional_marquees":

						$xpath = "marquee[@type='traditional']";		

						$opener = "<p>Prices for <em>traditional</em> style marquees.</p>";

						break;

					case "frame_marquees":

						$xpath = "marquee[@type='frame']";						

						$opener = "<p>Prices for clear span <em>frame</em> marquees.</p>";

						break;

					case "equipment":

						$class = "GetEquipmentPriceList";

						$opener = "";						

						break;

					case "furniture":

						$class = "GetFurniturePriceList";

						$opener = "";						

						break;

					case "sample":

						$class = "GetSamples";

						$opener = "<p style='margin-bottom:1.5em'>The following example quotes give likely costs for three typical marquee events. We hope they provide useful guidelines.</p><p>For individual advice, please <a href='/contact_us.htm'>contact us</a>. Or request your own free <a href='/pricing/quotation.htm'>personalised quote</a>.</p>";

						//$right_include = $this -> INCLUDE_DIR . "pricing_samples.php";

						break;

					case "chinese":

						$xpath = "marquee[@type='chinese hat']";

						$opener = "<p>Prices for <em>chinese hat</em> or <em>pagoda</em> style marquees.</p>";						

						break;

					default:

						$class = "GetAllMarqueesPriceList";					

						$opener = "";

						$xpath = "";

						break;

						

				}

				

					

				echo $content = "\n $opener \n<div id='pricetables'>\n<div id='mob'>" .$class($xpath). "</div>";
				 ?>
				</div>
			</div>
		</div>

</section>
</div>


<?php get_footer(); ?>
	

