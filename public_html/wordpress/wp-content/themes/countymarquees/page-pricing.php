<?php
/**
 * Template Name: Pricing Page 
 */
 get_header();
?>

<div id="outerBox" class="container">
<section class="banner-section venues">
	<div class="row">
		<div class="col-sm-12">
			<div class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>');">				
			</div>			
		</div>
	</div>
</section>

<section class="pricing-section" id="pricing">

		<div class="row">
			<div class="col-sm-12">
				<div id="facets">
					<p id="switch">Prices for the marquee you need</p>
					<div id="pick">
						<p><a href="javascript:void(0)" id="picker">Filter by size or type</a><a href="javascript:void(0)" id="close"><img src="/images/down_arrow.png" width="12" height="6" alt="Show more"></a></p>
						<div id="facetlists" style="display: none;">
						<ul class="first">
						<li><a href="/wordpress/pricing/marquees.htm" class="tabSelected">All marquee prices</a></li>
						</ul>
						<ul><li class="select">Select by size</li>

						<li><a href="/wordpress/pricing/smaller_marquees.htm">Small<br>(30 to 110)</a></li>
						<li><a href="/wordpress/pricing/medium_marquees.htm">Medium<br>(75 to 600)</a></li>
						<li><a href="/wordpress/pricing/event_marquees.htm">Large<br>(230 to 880)</a></li>
						</ul>
						<ul class="rightfacet">
						<li class="select">Select by type</li>
						<li><a href="/wordpress/pricing/frame_marquees.htm">Frame<br>marquees</a></li>
						<li><a href="/wordpress/pricing/traditional_marquees.htm">Traditional<br>marquees</a></li>
						<li><a href="/wordpress/pricing/chinese.htm">Chinese hat<br>marquees</a></li>
						</ul>
						</div>
					</div>
				</div>		
			
			<h1><?php the_title(); ?></h1>
			<div id="text">
				<?php 
				 global $post;
			     $price = $post->post_name;			   			
				 $xml = simplexml_load_file(get_template_directory_uri().'/xml/marquee_prices.xml');
				 
				$xpath = false; 	

				$class = "GetMarqueePriceList";
				
				
				
			switch ($price) {

					case "event_marquees":

						$class = "GetMarqueeSizePriceList";

						$xpath = "row[@size='large']";						

						$opener = "<p>All sizes and styles of marquees accomodating 300+ guests.</p>";

						break;

					case "smaller_marquees":

						$class = "GetMarqueeSizePriceList";

						$xpath = "row[@size='small']";						

						$opener = "<p>All sizes and styles of marquees accomodating less than 100 guests.</p>";

						break;

					case "medium_marquees":

						$xpath = "marquee[@size='medium']";						

						$opener = "<p>All sizes and styles of marquees accomodating between 75 and 600 guests</p>";

						break;

					case "traditional_marquees":

						$xpath = "marquee[@type='traditional']";		

						$opener = "<p>Prices for <em>traditional</em> style marquees.</p>";

						break;

					case "frame_marquees":

						$xpath = "marquee[@type='frame']";						

						$opener = "<p>Prices for clear span <em>frame</em> marquees.</p>";

						break;

					case "equipment":

						$class = "GetEquipmentPriceList";

						$opener = "";						

						break;

					case "furniture":

						$class = "GetFurniturePriceList";

						$opener = "";						

						break;

					case "sample":

						$class = "GetSamples";

						$opener = "<p style='margin-bottom:1.5em'>The following example quotes give likely costs for three typical marquee events. We hope they provide useful guidelines.</p><p>For individual advice, please <a href='/contact_us.htm'>contact us</a>. Or request your own free <a href='/pricing/quotation.htm'>personalised quote</a>.</p>";

						//$right_include = $this -> INCLUDE_DIR . "pricing_samples.php";

						break;

					case "chinese":

						$xpath = "marquee[@type='chinese hat']";

						$opener = "<p>Prices for <em>chinese hat</em> or <em>pagoda</em> style marquees.</p>";						

						break;

					default:

						$class = "GetAllMarqueesPriceList";					

						$opener = "";

						$xpath = "";

						break;

						

				}

				

					

				echo $content = "\n $opener \n<div id='pricetables'>\n<div id='mob'>" .$class($xpath). "</div>";
					  
					  // Currently working on this 
					//   foreach($xml as $venue){				
					// 	var_dump($venue); 
					// 	echo "<br><br>";
					//   }
						// exit;

				 ?>
				</div>
			</div>
		</div>

</section>
</div>

<?php get_footer(); ?>