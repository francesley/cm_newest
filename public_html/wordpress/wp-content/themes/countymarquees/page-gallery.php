<?php
/**
 * Template Name: Gallery List Page 
 */
get_header();
?>
<section class="page-content">
	<div class="container">
		<div class="row header-wrapper">
			<div class="col-lg-6">
					<div class="post-title">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>			
			</div>
			<div class="col-lg-6">	
				<div class="dropdown">
					<a class="btn btn-class btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						Filter by Category
					</a>
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="<?php echo get_permalink();?>">Action</a></li>
						<?php
						// Get the terms for the "gallery-category" taxonomy
						$terms = get_terms('gallery-category');
						if ($terms && !is_wp_error($terms)) {
						foreach ($terms as $term) {
							$term_link = get_term_link($term); // Get the link for the term
							echo '<li><a class="dropdown-item" href="' . esc_url($term_link) . '">' . $term->name . '</a></li>';
						}
						}
						?>
					</ul>
					</div>					
			</div>
		</div>
		<div class="row gallery-list">
			<?php
			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'post_type' => 'photos',
				'post_status' => 'publish',
				'orderby' => 'date',
				// Order by the publish date
				'order' => 'DESC',
				//'paged' => $paged
				'posts_per_page' => 8,
				'paged' => 1
			);

			$the_query = new WP_Query($args);
			if ($the_query->have_posts()):
				while ($the_query->have_posts()):
					$the_query->the_post();

					?>
					<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="post-item">
							<a href="<?php the_permalink(); ?>">
								<div class="post-item-img"
									style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
									<img class="image" src="<?php echo get_the_post_thumbnail_url(); ?>" />
								</div>
							</a>
							<a href="<?php the_permalink(); ?>">
								<div class="post-text">
									<?php the_excerpt(); ?>
								</div>
							</a>
						</div>
					</div>
				<?php endwhile; ?>

			<?php else: ?>
				<!-- Your no post found code here -->
			<?php endif; ?>
		</div>
		<div class="row">
        <div class="col-md-12">
          <div class="text-sm-end">
            <?php if ( $the_query->max_num_pages > 1 ) { ?>
            <a class="load-more btn btn-class dark"  data-page="<?php if($_GET[ 'pg' ]){ echo $_GET[ 'pg' ];}else{ echo '1';}?>" href="#seemore"> Load more </a>
            <?php } ?>
          </div>
        </div>
      </div>
	</div>
</section>

<?php get_footer(); ?>