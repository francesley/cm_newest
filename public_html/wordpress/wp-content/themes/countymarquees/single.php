<?php
/**
 * The template for displaying all single posts
 */
get_header();
?>

<?php
// Start the loop.
while (have_posts()) : the_post();
    ?>
   <section class="page-content">
         <div class="container">
                <div class="row">
                    <div class="col-12">
                            <div class="navigation">
                             <a href="javascript: history.go(-1)" role="link"> Back </a>
                            </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                    </div>
                    <div class="col-lg-6">
                            <div class="post-title">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>	
                    </div>
                </div>
        </div>
    </section>
    <?php
// End the loop.
endwhile;
?>
<?php get_footer(); ?>
