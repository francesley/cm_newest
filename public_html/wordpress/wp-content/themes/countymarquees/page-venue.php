<?php
/**
 * Template Name: Venue Page
 */

 get_header();
?>



<div class='venues'>
<div id='box'>
	<div id='outerBox' class="container">
		<div id='content'>	 
			<div class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>');">

			</div>
			<h1>Marquee Venue Finder</h1>
			<?php
				$url = home_url();	
			?>
				<ul id='subMenu'>
					<li class='menu_left selected'><a class='selected'  href="<?php echo $url;?>/venues.htm" >Marquee venue map</a></li>
					<li class='menu_left' ><a href="<?php echo $url;?>/venues/sites.htm" >Marquee venues lists</a></li>
					<li class='menu_right' ><a href="<?php echo $url;?>/add_venue.htm" >Add a venue</a></li>
				</ul>
			<table id="outerMap" cellspacing="0">
			<tr>
			<td class="row">
			<div id="side_bar" class="col-md-2"></div>
			<div class="map_wrapper col-md-10">
			<div id="map" style="width: 100%; height: 650px">
			</div>
			<div class="map-filter-wrapper"> 
			<h3>Filters:</h3>
			<select id="area" class="map_filters" onchange="speedTest.SelectType(this)">
			<option value="">All counties</option>
			<option value="beds">Bedfordshire</option>
			<option value="berks">Berkshire</option>
			<option value="bucks">Buckinghamshire</option>
			<option value="eSussex">East Sussex</option>
			<option value="essex">Essex</option>	
			<option value="hants">Hampshire</option>	
			<option value="herts">Hertfordshire</option>	
			<option value="kent">Kent</option>
			<option value="lon">London</option>
			<option value="oxford">Oxfordshire</option>	 
			<option value="surrey">Surrey</option>
			<option value="wSussex">West Sussex</option>
			</select>
			<select name="types" class="map_filters" id="types" onChange="speedTest.SelectType(this)">
			<option value="">All venues</option>
			<option value="civil">Civil ceremonies</option>
			<option value="large">Large (150+ seated)</option>
			<option value="huge">Huge events</option>
			<option value="not">Not expensive</option>
			<option value="medium">Medium price</option>
			<option value="expensive">Expensive</option>
			<option value="management">Event Management</option>
			</select>
			</div></div>
			</td></tr>
			
			</table>
			<div id="key">
			<p class="wide">All capacity figures are for maximum number of seated guests</p>
			</div>
		</div>

		<div class="unnec"></div>
	</div>
</div>
<div class="unnec">&nbsp;</div>
</div>


<?php get_footer(); ?>
	

