<?php show_admin_bar( false );
/*
  =============================================================
  Index
  =============================================================

  1.0 - Theme Support

  2.0 - Enqueue Scripts and Styles

  3.0 - Register Menus

  4.0 - Register Widgets Areas


  -------------------------------------------------------------
  Index Ends
  -------------------------------------------------------------
 */
/*
  =============================================================
  1.0 - Theme Support
  =============================================================
 */
add_action('after_setup_theme', 'theme_setup');

function theme_setup() {
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
    add_theme_support('custom-header');
    add_image_size('thumb-300x200', 300, 200, true);
}

/*
  -------------------------------------------------------------
  Theme Support Ends
  -------------------------------------------------------------
 */

/*
 * *****************************************************************************
 *  Custom Logo
 * *****************************************************************************
 *  @ Custom logo for theme.
 *      - new_theme_the_custom_logo: uses the_custom_logo
 * *****************************************************************************
 */

function new_theme_the_custom_logo() {

    if (function_exists('the_custom_logo')) {
        the_custom_logo();
    }
}

/*
  =============================================================
  2.0 - Enqueue Scripts and Styles
  =============================================================
 */

function theme_enqueue_scripts() {

    global $wp_query;

    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css','', '1.0.0');

    wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css', '', '1.0.0' );

    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css', '', '1.0.0' );
	
	  wp_enqueue_style('cm-style', get_template_directory_uri() . '/css/cm.css', '', '1.0.0');     

    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js', array('jquery'), '1.0.0', true);

    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), '1.0.0', true );
	
    wp_register_script("theme-script", get_template_directory_uri() . '/js/theme-script.js', array('jquery'), '1.0.0', true );

    // Localize script with AJAX URL and other parameters

    wp_localize_script('theme-script', 'loadmore_params', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ), 
      'posts' => json_encode( $wp_query->query_vars ),
      'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
      'max_page' => $wp_query->max_num_pages
      ) );

    wp_enqueue_script('theme-script');
}
    
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');

/*
  -------------------------------------------------------------
  Enqueue Scripts and Styles Ends
  -------------------------------------------------------------
 */

/*
  =============================================================
  3.0 - Register Menus
  =============================================================
 */

function register_menus() {
    register_nav_menus(
		array(
			'main-menu' => __('Main Menu'),
			'footer-area' => __('Footer Areas'),
			'useful-links' => __('Footer Useful Links'),
		)
    );
}

add_action('init', 'register_menus');

if ( !is_admin() )add_action( "wp_enqueue_scripts", "my_jquery_enqueue", 11 );

function my_jquery_enqueue() {
  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.4.1.min.js", false, null );
  wp_enqueue_script( 'jquery' );
}

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page( array(
      'page_title' => 'Theme General Settings',
      'menu_title' => 'Theme Settings',
      'menu_slug' => 'theme-general-settings',
      'capability' => 'edit_posts',
      'redirect' => false
    ) );
  
    acf_add_options_sub_page( array(
      'page_title' => 'Theme Header Settings',
      'menu_title' => 'Header',
      'parent_slug' => 'theme-general-settings',
    ) );
  
    acf_add_options_sub_page( array(
      'page_title' => 'Theme Footer Settings',
      'menu_title' => 'Footer',
      'parent_slug' => 'theme-general-settings',
    ) );
  
  }


  class Bootstrap_NavWalker extends Walker_Nav_Menu {
    public function start_lvl( & $output, $depth = 0, $args = array() ) {
      $output .= '<ul class="dropdown-menu">';
    }
  
    public function start_el( & $output, $item, $depth = 0, $args = array(), $id = 0 ) {
      $output .= '<li class="nav-item';
  
      if ( in_array( 'menu-item-has-children', $item->classes ) ) {
        $output .= ' dropdown';
      }
      if ( $item->current || in_array( 'current_page_parent', $item->classes ) || in_array( 'current-post-ancestor', $item->classes ) || $item->current_item_ancestor ) {
        $output .= ' active';
      }
  
      $output .= '">';
  
      if ( in_array( 'menu-item-has-children', $item->classes ) ) {
        $output .= '<a class="nav-link dropdown-toggle" href="' . $item->url . '" id="navbarDropdown' . $item->ID . '" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
      } else {
        if ( $depth > 0 ) {
          $output .= '<a class="nav-link-sub" href="' . $item->url . '">';
        } else {
          $output .= '<a class="nav-link" href="' . $item->url . '">';
        }
      }
  
      $output .= $item->title;
      $output .= '</a>';
    }
  }
  

/*
  -------------------------------------------------------------
  Register Menus Ends
  -------------------------------------------------------------
 */

/*
  =============================================================
  4.0 - Register Widget Areas
  =============================================================
 */

function custom_widgets() {
    register_sidebar(array(
        'id' => 'footer-widget-area',
        'name' => 'Footer Widget Area',
        'description' => 'The widget area in the footer',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    
}

add_action('widgets_init', 'custom_widgets');


/*
  -------------------------------------------------------------
  Register Widget Areas Ends
  -------------------------------------------------------------
 */
 


remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
remove_action('wp_head', 'wlwmanifest_link'); //removes wlwmanifest (Windows Live Writer) link.
remove_action('wp_head', 'wp_generator'); //removes meta name generator.
remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.
remove_action( 'wp_head', 'feed_links', 2 ); //removes feed links.
remove_action('wp_head', 'feed_links_extra', 3 );  //removes comments feed. 
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
add_filter( 'emoji_svg_url', '__return_false' );// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function remove_wp_block_library_css() {
  wp_dequeue_style('wp-block-library');
  wp_dequeue_style('wp-block-library-theme');
}
add_action('wp_enqueue_scripts', 'remove_wp_block_library_css');


/*  -------------------------------------------------------------  
	WP_Customize Custom Functions  
	------------------------------------------------------------- */ 

/* Maps */	
require_once( __DIR__ . '/includes/maps/GetVenues.php');

/* Price List */	

require_once( __DIR__ . '/includes/pricing/GetPricing.php');


// Register Taxonomy
function register_custom_taxonomy() {
      $labels = array(
          'name' => 'Primary Area',
          'singular_name' => 'Primary Area',
          'menu_name' => 'Primary Area',
      );

      $args = array(
          'hierarchical' => true,
          'labels' => $labels,
          'public' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'show_admin_column' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'primary_area'),
      );

      register_taxonomy('primary_area', 'page', $args);
  }
  add_action('init', 'register_custom_taxonomy');



  function hide_taxonomy_metabox_for_specific_pages() {
      // Check if we are editing a page
      if (isset($_GET['post'])) {
          $current_page_id = $_GET['post'];
          // Check if the current page is a child of the "Areas" page
          $parent_page_slug = 'areas'; // Replace with the slug of the "Areas" page
          $parent_page = get_page_by_path($parent_page_slug);

          if ($parent_page && $parent_page->ID === wp_get_post_parent_id($current_page_id)) {
              // Replace 'your_taxonomy_name' with the actual taxonomy name
              
          }else{
              remove_meta_box('primary_areadiv', 'page', 'side');
          }
      }

  }
  add_action('admin_menu', 'hide_taxonomy_metabox_for_specific_pages');

add_image_size( 'gallery-size-large', 620, 415 ,true);
add_image_size( 'gallery-size-medium', 450 , 300 ,true);
add_image_size( 'gallery-size-small', 300, 200 ,true);

 // Ajax functions for the gallery load more

  function cw_loadmoregallery_handler() {
    $page = $_POST[ 'page' ] + 1;  
    $args = array(
      'post_type' => 'photos',
      'post_status' => 'publish',
      'posts_per_page' => 8,
      'order' => 'DESC',
      'paged' => $page,
    );
    $res = new WP_Query( $args );
    $output = '';
  
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
      while ( $the_query->have_posts() ): $the_query->the_post();
      $image = get_the_post_thumbnail_url();
      if ( !$image ) {
        $image = "https://via.placeholder.com/290x193.png";
      }
      $title = get_the_title();
      $link = get_the_permalink();
      $excerpt = get_the_excerpt();
      $output .= '<div class="col-lg-3 col-md-4 col-sm-6 col-6"><div class="post-item"><a href="' . $link . '"><div class="post-item-img" style="background-image:url(' . $image . ')"><img class="image" src="' . $image . '" /> </div>
        </a><a href="' . $link . '"><div class="post-text">' . $excerpt . '</div></a></div></div>';
      endwhile;
  } else {
    $output .= 'noposts';
  }  
  
  $op = array();
  $op[ 'code' ] = $output;
  $op[ 'maxpages' ] = $res->max_num_pages;  
  echo json_encode( $op );
  die;
}


add_action( 'wp_ajax_loadmoregallery', 'cw_loadmoregallery_handler' ); 