<footer>
<div class="container">
	<div class="row">
		<div class="col-sm-2 offset-sm-1">	
			<div class="footer-menu">
			   	<h4> Useful</h4>
				<?php wp_nav_menu( array( 'theme_location' => 'useful-links' ,'menu_id' => 'useful-links') ); ?>
			</div>		
		</div>
		<div class="col-sm-4">		
			<div class="footer-menu">
				<h4> Areas Covered </h4>
				<?php wp_nav_menu( array( 'theme_location' => 'footer-area' ,'menu_id' => 'footer-area') ); ?>
			</div>
		</div>
	</div>
</div>
<div class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">	
					<div class="copyrights">
					<?php the_field('copyrights_text', 'option'); ?>
					</div>
			</div>
			<div class="col-sm-6">	
					<ul class="footer-bottom-menu">
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Cookie Settings</a></li>
					</ul>
			</div>
		</div>
	</div>
	</div>
</footer>


<?php wp_footer(); ?>

<?php if(is_page('Venues')){
	// Loading Maps API only for Venues page
	?>
	<!--[if IE 8]>
	<link href="/css/ie8.css" rel="stylesheet" type="text/css" media="screen"/>
	<![endif]-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi4naAaJZfKQaDsahtFHpyIEtnU-hx8PM" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/maps/scripts/markerclusterer.js"></script><script src="/maps/scripts/mapscript.js"></script>
	<!--[if lt IE 7]>
	<script src="maps/scripts/pngfix.js" type="text/javascript"></script>
	<![endif]-->
	<script type="text/javascript">
	//<![CDATA[
	function load() {
	if (GBrowserIsCompatible()) {
	init();
	} else {
	alert("Sorry, Google Maps are not compatible with your browser");
	}
	}
	//]]>	
	</script>	
	<script src='<?php echo get_template_directory_uri(); ?>/js/search_min.js'></script><script>google.maps.event.addDomListener(window, 'load', speedTest.init);
	</script>
<?php }				
?>
<?php if(is_page_template('page-sites.php')|| is_page_template('page-pricing.php')){?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/facets.js"></script>
<?php } ?>
</body>

</html>

