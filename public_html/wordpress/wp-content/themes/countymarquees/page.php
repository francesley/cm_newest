<?php
get_header();
if (have_rows('page_section')):
    // Loop through sections
    while (have_rows('page_section')):
        the_row();
        if (get_row_layout() == 'hero_slider') {
            // This section is for the Top Banner slider section
            ?>
            <section id="heroIndicators" class="carousel slide carousel-fade hero-slider" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <?php
                    if (have_rows('slider_items')):
                        $galCount = 0;
                        while (have_rows('slider_items')):
                            the_row();
                            $slide_image = get_sub_field('slide_image');
                            $slide_heading = get_sub_field('slide_heading');
                            $slide_text = get_sub_field('slide_text'); ?>
                            <button type="button" data-bs-target="#heroIndicators" data-bs-slide-to="<?php echo $galCount; ?>"
                                class="<?php if ($galCount == 0) {
                                    echo 'active';
                                } ?>" aria-current="true"
                                aria-label="Slide <?php echo $galCount; ?>"></button>
                            <?php
                            $galCount++;
                        endwhile;
                    endif;
                    ?>
                </div>

                <div class="carousel-inner">
                    <?php
                    if (have_rows('slider_items')):
                        $galCount = 0;
                        while (have_rows('slider_items')):
                            the_row();
                            $slider_image = get_sub_field('slider_image');
                            $slider_content = get_sub_field('slider_content'); ?>
                            <div class="carousel-item <?php if ($galCount == 0) {
                                echo 'active';
                            } ?>">
                                <img src="<?php echo $slider_image['url']; ?>" alt="<?php echo $slider_image['alt']; ?>" />
                                <div class="slider-content container">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php echo $slider_content; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $galCount++;
                        endwhile;
                    endif;
                    ?>
                </div>
                </section>
        <? } 
        if (get_row_layout() == '3_columns_with_title_content') {
        // This section is for the 3 Column blocks
                $stitle = get_sub_field( 'title' );
                $ssubtitle = get_sub_field( 'subtitle' );
            ?>
            <section class="three-col-sec">
                    <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php if($stitle){?><h2><?php echo $stitle; ?></h2><?php } ?>
                                            <?php if($ssubtitle){?><p><?php echo $ssubtitle; ?></p><?php } ?>
                                        </div>
                                    </div>                        
                                    <div class="row">
                                    <?php
                                                if (have_rows('content_blocks')):                                                    
                                                    while (have_rows('content_blocks')):
                                                        the_row();
                                                        $block_link = get_sub_field('block_link');
                                                        $image = get_sub_field('image');
                                                        $block_title = get_sub_field('block_title'); ?>
                                                            <div class="col-md-4 col-sm-6">
                                                                <div class="block-wrapper">
                                                                      <a href="<?php echo $block_link; ?>">
                                                                          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />                                                                             
                                                                      </a>

                                                                      <a href="<?php echo $block_link; ?>">
                                                                      <?php echo $block_title; ?>
                                                                     </a>

                                                                </div>
                                                            </div>
                                                        <?php                                                       
                                                    endwhile;
                                                endif;
                                                ?>
                                        </div>
                        </div>
             </section>
        <? } 
        
        if (get_row_layout() == 'full_width_image_with_overlay_content') {
                $full_width_image = get_sub_field( 'full_width_image' );
                $overlay_position = get_sub_field( 'overlay_position' );
                $overlay_content = get_sub_field( 'overlay_content' );
            ?>
            <section class="fullwidth-overlay" style="background-image: url('<?php echo $full_width_image['url']; ?>')">
                    <div class="container-full">                        
                                    <div class="row">
                                        <div class="col-md-5 col-sm-6 flex-wrapper">
                                        <div class="overlay-wrapper <?php if($overlay_position=='right'){ echo "float-end";}?>">
                                            <?php echo $overlay_content;?>
                                        </div>
                                   </div>
                                 </div>
                        </div>

             </section>
        <? }  endwhile; 
     endif;     
    get_footer(); ?>