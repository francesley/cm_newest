document.addEventListener("DOMContentLoaded", function () {
    window.addEventListener('scroll', function () {
      if (window.scrollY > 200) {
        document.getElementById('navbar').classList.add('nav-scroll');
        document.getElementById('navbar').classList.add('nav-trn');
      } else {
        document.getElementById('navbar').classList.remove('nav-scroll');
      }
    });
  });


  $(document).ready(function() {    
    // Ajax call for load more galleries
    $('body').on('click', '.load-more', function (e) {
      e.preventDefault();
      $(this).prop('disabled', true);
      page = $(this).data('page');
      nextpage = page + 1;
      var button = $(this),
        data = {
          'action': 'loadmoregallery',
          'page': page
        };
      $.ajax({
        url: loadmore_params.ajaxurl,
        data: data,
        dataType: 'json',
        type: 'POST',
        beforeSend: function (xhr) {},
        success: function (data) {
          if (data.code!= 'noposts') {
            $(data.code).appendTo(".gallery-list").hide().fadeIn('slow');
            $('.load-more').data('page', nextpage);
            if (loadmore_params.current_page == loadmore_params.max_page)
              button.remove();
          } else {
            // hides the load more button and show message
           $('<div class="col-sm-12 text-center"><p> No more galleries to show</p></div>').appendTo(".gallery-list").hide().fadeIn('slow');           
            $(".load-more").hide();
            button.text('No more galleries to show'); // if no data, remove the button as well
          }
          button.prop('disabled', false);
        }
      });
  
      return false;
    });


    $('.related-gallery-list').slick({
      dots: true,
      slidesToShow: 6,
      slidesToScroll: 3,
      variableWidth: false,
      arrows: true,
      autoplay: false,
      swipe: true,
      fade: false,
      autoplaySpeed: 5000,
      adaptiveHeight: true,
      infinite: true, // Set infinite option to false
      initialSlide: 0,    
      cssEase: 'linear',
      prevArrow: '<button type="button" class="slick-prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span></button>', // Custom previous arrow
  nextArrow: '<button type="button" class="slick-next"><span class="carousel-control-next-icon" aria-hidden="true"></span></button>', // Custom next arrow
      responsive: [
        {
          breakpoint: 1024, // Set a breakpoint for iPad
          settings: {
            slidesToShow: 4, // Adjust the number of slides to show for iPad
            slidesToScroll: 2, // Adjust the number of slides to scroll for iPad
          }
        },
        {
          breakpoint: 768, // Set a breakpoint for mobile
          settings: {
            slidesToShow: 2, // Adjust the number of slides to show for mobile
            slidesToScroll: 2, // Adjust the number of slides to scroll for mobile
          }
        }
      ]
    });

});
  