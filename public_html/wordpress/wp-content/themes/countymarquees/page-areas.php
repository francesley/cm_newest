<?php
/**
 * Template Name: Areas Page 
 */
 get_header();
?>

<div id="outerBox" class="container">
<section class="banner-section venues">
	<div class="row">
		<div class="col-sm-12">
			<div class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>');">				
			</div>			
		</div>
	</div>
</section>

<section class="areas-section" id="areas">
		<div class="row">
		<div class="col-sm-4">
			<div class="location-sidebar">
				
					<?php
					// Get the current page's ID
					$current_page_id = get_the_ID();

					// Get the taxonomy terms (replace 'your_taxonomy_name' with the actual taxonomy name)
					$taxonomy = 'primary_area';
					$terms = wp_get_post_terms($current_page_id, $taxonomy);

					if (!empty($terms)) {
						// Get the first term (assuming a page can have only one term)
						$term = reset($terms);

						?>
						<h3><?php echo $term->name; ?> locations: </h3>
						<?php

						// Get other pages attached to the same term
						$args = array(
							'post_type' => 'page',
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomy,
									'field' => 'id',
									'terms' => $term->term_id,
								),
							),
						);

						$query = new WP_Query($args);

						if ($query->have_posts()) {
							echo '<ul>';
							while ($query->have_posts()) {
								$postslug = get_post_field( 'post_name');
								$query->the_post();
								if ($current_page_id !== get_the_ID()) { // Exclude the current page
									echo '<li class="'.$postslug.'"><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
								}
							}
							echo '</ul>';
							wp_reset_postdata();
						}
					}
					?>

				</div>
		</div>
		<div class="col-sm-8">			
			<h1><?php the_title(); ?></h1>
			<div class="area-content">
				<?php the_content(); ?>
				<?php // Testimonials, resources , map sections
				if ( have_rows( 'page_sections' ) ) {
				while ( have_rows( 'page_sections' ) ) {
					the_row();
					$secCount++;
					?>
					<?php
					if ( get_row_layout() == 'testimonials_block' ) {?>					
					<h2>Local praise</h2>
					<?php 
							if ( have_rows( 'testimonials' ) ) {
							while ( have_rows( 'testimonials' ) ) {
								the_row();
									$testimonee_name = get_sub_field( 'testimonee_name' );
									$testimonial_text = get_sub_field( 'testimonial_text' );
									?>
									<div class="tem-item">
									<h6><?php echo $testimonee_name; ?></h6>
									<?php echo $testimonial_text; ?>
									</div>
								
							<?php } } 

					} 
					if ( get_row_layout() == 'local_resources_block' ) {?>					
						<h2>Local resources</h2>
						<?php 							if ( have_rows( 'resources' ) ) {
								while ( have_rows( 'resources' ) ) {
									the_row();
										$resource_content = get_sub_field( 'resource_content' );								
										?>
										<div class="resource-item">										
										<?php echo $resource_content; ?>
										</div>									
								<?php } } 

					} 
					if ( get_row_layout() == 'map_block' ) {
						$map_url = get_sub_field( 'map_content' );
						$slug = get_post_field( 'post_name', get_post() );?>					
						<h2>We cover the <?php echo ucfirst($slug); ?> area</h2>
						<?php ?>
						<iframe src="<?php echo $map_url; ?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						<?php

					} 
					?>
				<?php } } ?>
			</div>
		</div>
</div>
</section>

<?php get_footer(); ?>