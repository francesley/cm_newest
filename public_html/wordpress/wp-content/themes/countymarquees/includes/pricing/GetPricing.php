<?php

$xml = simplexml_load_file(get_template_directory_uri().'/xml/marquee_prices.xml');

$pricingCall = "<ul class='endCall'><li>For individual advice, please  <a href='/contact_us.htm'>contact us</a></li> <li>You can also  <a href='/pricing/quotation.htm'>request a personalised quotation online</a></li></ul>";

$end_terms = "</div><ul id='pricing_terms'>

<li >Marquees can be joined to provide widths of 15m to 27m</li>

<li>Allow extra space for reception areas, dance floors, buffet tables etc</li>

<li>A non-refundable deposit is required to confirm  marquee hire bookings. The balance must be settled on the day of erection</li>

<li>All prices are subject to current rates of VAT</li>

<li>Damage waiver  is 5% + VAT on the final invoice</li>

</ul>";

$furn_end_terms = "<ul id='pricing_terms'>

<li>A non-refundable deposit is required to confirm  marquee hire bookings. The balance must be settled on the day of erection</li>

<li>All prices are subject to current rates of VAT</li>

<li>Damage waiver  is 5% + VAT on the final invoice</li>

</ul>";

function GetMarqueePriceList($xpath_expr) {	

	global $xml,$pricingCall,$end_terms,$furn_end_terms;
	
	$rowCounter = 1;

	foreach ($xml -> xpath($xpath_expr) as $marquee) {

		

		$content .= "<table  cellspacing='0' cellpadding='0' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";

		$content .= $marquee["type"] != "chinese hat"?"<caption>" . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees":"<caption>3 or 6m/10 or 20' chinese hat marquees";

		$content .= "<span>For " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people </span></caption>";

		$content .= "<thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>

			<th colspan='2' class='odd top'>Capacity </th>

			<th colspan='2' class='top'>Marquee</th>

			<th rowspan='2' valign='bottom' scope='col' class='nomob'>Coconut <br/>Matting</th>

			<th rowspan='2' valign='bottom' scope='col' class='nomob'>Lighting</th>

			<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>

		</tr>

		<tr><th scope='col'> Seated</th>

			<th scope='col'>Buffet</th>

			<th valign='bottom' scope='col'>Lined</th>

			<th valign='bottom' scope='col'>Unlined</th>

			<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>

			<th align='center' scope='col' class='marquee_total'>Unlined</th>

		 </tr></thead><tbody>";

		

		foreach($marquee -> row as $row){

			$rowClass = ($rowCounter % 2 == 0)?" class='odd' ":"";

			$content .= "<tr $rowClass>\n<th class='leftCol' scope='row'>";

			$content.= $marquee["type"] != "chinese hat"?$marquee["width"] . "m x " . $row["length"] . "m<br/>":$row["length"] . "m x " . $row["length"] . "m<br/>";

				$content .= $marquee["type"] != "chinese hat"?"<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n":"<span>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";

			$content .= "<td>" . $row["seatedCap"] . "</td>\n";

			$content .= "<td >" . $row["buffetCap"] . "</td>\n";

			$content .= $row["linedP"] != "N/A"?"<td>&pound;" . $row["linedP"] . "</td>\n":"<td>" . $row["linedP"] . "</td>\n";

				$content .= $row["unlinedP"] != "POA"?"<td>&pound;" . $row["unlinedP"] . "</td>\n":"<td>" . $row["unlinedP"] . "</td>\n";

				$content .= $row["mat"] != "POA"?"<td >&pound;" . $row["mat"] . "</td>\n":"<td>" . $row["mat"] . "</td>\n";

				$content .= $row["light"] != "POA"?"<td>&pound;" . $row["light"] . "</td>\n":"<td>" . $row["light"] . "</td>\n";		

				$content .= $row -> totals["lined"] != "N/A"?"<td  class='marquee_total leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n":"<td  class='marquee_total leftBorder'>" . $row -> totals["lined"] . "</td>\n";

				$content .= $row -> totals["unlined"] != "POA"?"<td  class='marquee_total'>&pound;" . $row -> totals["unlined"] . "</td>\n":"<td  class='marquee_total'>" . $row -> totals["unlined"] . "</td>\n";

			$content .= "</tr>\n";

			$rowCounter++;

		}		

	$content .= "</tbody></table>\n<p class='price_bytable'>Please <a href='/contact_us.htm'>contact us</a> for a personalised quotation.</p>\n";



	}

	$content .= $end_terms . $pricingCall;

	return $content;

			

}


function GetAllMarqueesPriceList() {  
	global $xml,$pricingCall,$end_terms,$furn_end_terms;
	$rowCounter = 1;

	

	foreach ($xml -> marquee as $marquee) { 

	

		$content .= "<table cellspacing='0' cellpadding='0' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";

			$content .= $marquee["type"] != "chinese hat"?"<caption>" . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees</caption>":"<caption>3 or 6m/10 or 20' chinese hat marquees</caption>";

			//$content .= "<span> &#8212; for " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people </span></caption>";

			$content .= "<thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>

				<th colspan='2' class='odd top'>Capacity </th>

				<th colspan='2' class='top'>Marquee</th>

				<th rowspan='2' valign='bottom' scope='col'>Coconut <br/>Matting</th>

				<th rowspan='2' valign='bottom' scope='col'>Lighting</th>

				<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>

			</tr>

			<tr><th scope='col'> Seated</th>

				<th scope='col'>Buffet</th>

				<th valign='bottom' scope='col'>Lined</th>

				<th valign='bottom' scope='col'>Unlined</th>

				<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>

				<th align='center' scope='col' class='marquee_total'>Unlined</th>

			 </tr></thead><tbody>";

			 

	

		foreach($marquee -> row as $row){

			

				$rowClass = ($rowCounter % 2 == 0)?" class='odd' ":"";

				$content .= "<tr $rowClass>\n<th class='leftCol' scope='row'>";

				$content.= $marquee["type"] != "chinese hat"?$marquee["width"] . "m x " . $row["length"] . "m<br/>":$row["length"] . "m x " . $row["length"] . "m<br/>";

				$content .= $marquee["type"] != "chinese hat"?"<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n":"<span>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";

				$content .= "<td>" . $row["seatedCap"] . "</td>\n";

				$content .= "<td >" . $row["buffetCap"] . "</td>\n";

				$content .= $row["linedP"] != "N/A"?"<td>&pound;" . $row["linedP"] . "</td>\n":"<td>" . $row["linedP"] . "</td>\n";

				$content .= $row["unlinedP"] != "POA"?"<td>&pound;" . $row["unlinedP"] . "</td>\n":"<td>" . $row["unlinedP"] . "</td>\n";

				$content .= $row["mat"] != "POA"?"<td >&pound;" . $row["mat"] . "</td>\n":"<td>" . $row["mat"] . "</td>\n";

				$content .= $row["light"] != "POA"?"<td>&pound;" . $row["light"] . "</td>\n":"<td>" . $row["light"] . "</td>\n";		

				$content .= $row -> totals["lined"] != "N/A"?"<td  class='marquee_total leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n":"<td  class='marquee_total leftBorder'>" . $row -> totals["lined"] . "</td>\n";

				$content .= $row -> totals["unlined"] != "POA"?"<td  class='marquee_total'>&pound;" . $row -> totals["unlined"] . "</td>\n":"<td  class='marquee_total'>" . $row -> totals["unlined"] . "</td>\n";

				$content .= "</tr>\n";

				$rowCounter++;

			}		

		$content .= "</tbody></table>\n<p class='price_bytable'>Please <a href='/contact_us.htm'>contact us</a> for a personalised quotation.</p>\n";

		

		}

	

	$content .= $end_terms . $pricingCall;

	return $content;



}



function GetFurniturePriceList() {	
	global $pricingCall,$end_terms,$furn_end_terms;
	$xml = simplexml_load_file(get_template_directory_uri().'/xml/furniture_prices.xml');
	//var_dump($xml -> group);exit;
	foreach ($xml -> group as $furnitureGroup) {
		

		$content .= "<table border='0' cellspacing='0' cellpadding='0' summary=\"Prices for marquee tables, chairs and dancefloors\" class='furn' >";

		$content .= "<caption>Marquee ". $furnitureGroup["name"] . " price list</caption>";

		

		$classCounter = 0;

		foreach($furnitureGroup -> item as $item){

			//rows with no price bolded

			switch ($item["price"]) {

				case "":

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'><strong>";

					$content.= $item . "</strong></th>";

					$content .= "<td class='furnRight" . $classCounter . "' ></td>\n";

					break;

				case "POA":

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";

					$content.= $item . "</th>";

					$content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "</td>\n";

					break;

				case "Standard flooring":

				case "Standard lighting":

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";

					$content.= $item . "</th>";

					$content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "<br/>See <a href='/pricing/marquees.htm'>marquee pricing</a></td>\n";

					break;

				default:

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";

					$content.= $item . "</th>";

					$content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";

			}

			$content .= "</tr>\n";

			$classCounter = $classCounter == 0?1:0;

		}		

	$content .= "</table>\n";

	}

	

	$content .= $furn_end_terms . $pricingCall;

	return $content;

}



function GetEquipmentPriceList() {	
	global $pricingCall,$end_terms,$furn_end_terms;
	$xml = simplexml_load_file(get_template_directory_uri().'/xml/equipment_prices.xml');
	foreach ($xml -> group as $furnitureGroup) {

		

		$content .= "<table border='0' cellspacing='0' cellpadding='0' summary=\"Prices for marquee tables, chairs and dancefloors\" class='furn' >";

		$content .= "<caption>". $furnitureGroup["name"] . "</caption>";

		

		$classCounter = 0;

		foreach($furnitureGroup -> item as $item){

			//rows with no price bolded

			switch ($item["price"]) {

				case "":

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'><strong>";

					$content.= $item . "</strong></th>";

					$content .= "<td class='furnRight" . $classCounter . "' ></td>\n";

					break;

				case "POA":

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";

					$content.= $item . "</th>";

					$content .= "<td class='furnRight" . $classCounter . "' >" . $item["price"] . "</td>\n";

					break;

				default:

					$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";

					$content.= $item . "</th>";

					$content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";

			}

			

			$content .= "</tr>\n";

			$classCounter = $classCounter == 0?1:0;

		}		

	$content .= "</table>\n";

	}

	

	$content .= $furn_end_terms . $pricingCall;

	return $content;



}



function GetSamples() {	
	global $pricingCall,$end_terms,$furn_end_terms;
	$xml = simplexml_load_file(get_template_directory_uri().'/xml/sample_prices.xml'); 
	
	foreach ($xml -> sample as $sample) {		

		$content .= "<table border='0' cellspacing='0' cellpadding='0'  class='furn' summary=\"Example marquee prices for " . strtolower($sample["title"]) . "\" id=\"" . $sample["id"] . "\">";

		$content .= "<caption>" . $sample["title"] . "</caption>";

		

		

		$classCounter = 0;

		foreach($sample -> item as $item){

			$content .= "<tr>\n<th class='furnLeft" . $classCounter . "' scope='row'>";

			$content.= $item . "</th>";

			$content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $item["price"] . "</td>\n";

			$content .= "</tr>\n";

			$classCounter = $classCounter == 0?1:0;

		}

		

		$content .= "<tr><th class='damage furnLeft" . $classCounter . "' scope='row'>Damage waiver @ 5%</th>";

		$content .= "<td class='furnRight" . $classCounter . "' >&pound;" . $sample["damage"] . "</td></tr>\n";

		$content .= "<tr><th class='total' scope='row'>Total</th>";

		$content .= "<td class='total' >&pound;" . $sample["total"] . "</td></tr>\n";

	$content .= "</table>\n";

	}

	

	$content .= $pricingCall;

	

	return $content;



}



function GetMarqueeSizePriceList($xpath_expr) {
	global $xml,$pricingCall,$end_terms,$furn_end_terms;

	$rowCounter = 1;

	

	foreach ($xml -> marquee as $marquee) {

	

		$xmlMarquee = simplexml_load_string($marquee->asXML());

		$rows = $xmlMarquee-> xpath($xpath_expr);

		//Util::Show(count($rows), "how many this size");

		

		if (count($rows) > 0) {

			

			$content .= "<table cellspacing='0' cellpadding='0' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";

			$content .= $marquee["type"] != "chinese hat"?"<caption>" . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees":"<caption>3 or 6m/10 or 20' chinese hat marquees";

			//$content .= "<span>For " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people </span></caption>";

			$content .= "<thead><tr><th rowspan='2' scope='col' class='nobreak leftCol'>Marquee size<br/><span>Width x<br/>Length</span></th>

				<th colspan='2' class='odd top'>Capacity </th>

				<th colspan='2' class='top'>Marquee</th>

				<th rowspan='2' valign='bottom' scope='col'>Coconut <br/>Matting</th>

				<th rowspan='2' valign='bottom' scope='col'>Lighting</th>

				<th colspan='2' align='center' class='marquee_total top leftBorder'>Total hire price</th>

			</tr>

			<tr><th scope='col'> Seated</th>

				<th scope='col'>Buffet</th>

				<th valign='bottom' scope='col'>Lined</th>

				<th valign='bottom' scope='col'>Unlined</th>

				<th align='center' class='marquee_total leftBorder' scope='col'>Lined</th>

				<th align='center' scope='col' class='marquee_total'>Unlined</th>

			 </tr></thead><tbody>";

			 

			foreach ($rows as $row) {	

			

				$rowClass = ($rowCounter % 2 == 0)?" class='odd' ":"";

				$content .= "<tr $rowClass>\n<th class='leftCol' scope='row'>";

				$content.= $marquee["type"] != "chinese hat"?$marquee["width"] . "m x " . $row["length"] . "m<br/>":$row["length"] . "m x " . $row["length"] . "m<br/>";

				$content .= $marquee["type"] != "chinese hat"?"<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></th>\n":"<span>(" . $row["footLength"] . "' x " . $row["footLength"] . "')</span></th>\n";

				$content .= "<td>" . $row["seatedCap"] . "</td>\n";

				$content .= "<td >" . $row["buffetCap"] . "</td>\n";

				$content .= $row["linedP"] != "N/A"?"<td>&pound;" . $row["linedP"] . "</td>\n":"<td>" . $row["linedP"] . "</td>\n";

				$content .= $row["unlinedP"] != "POA"?"<td>&pound;" . $row["unlinedP"] . "</td>\n":"<td>" . $row["unlinedP"] . "</td>\n";

				$content .= $row["mat"] != "POA"?"<td >&pound;" . $row["mat"] . "</td>\n":"<td>" . $row["mat"] . "</td>\n";

				$content .= $row["light"] != "POA"?"<td>&pound;" . $row["light"] . "</td>\n":"<td>" . $row["light"] . "</td>\n";		

				$content .= $row -> totals["lined"] != "N/A"?"<td  class='marquee_total leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n":"<td  class='marquee_total leftBorder'>" . $row -> totals["lined"] . "</td>\n";

				$content .= $row -> totals["unlined"] != "POA"?"<td  class='marquee_total'>&pound;" . $row -> totals["unlined"] . "</td>\n":"<td  class='marquee_total'>" . $row -> totals["unlined"] . "</td>\n";

				$content .= "</tr>\n";

				$rowCounter++;

			}		

		$content .= "</tbody></table>\n<p class='price_bytable'>Please <a href='/contact_us.htm'>contact us</a> for a personalised quotation.</p>\n";

		

		}

	}

	$content .= $end_terms . $pricingCall;

	return $content;



}
