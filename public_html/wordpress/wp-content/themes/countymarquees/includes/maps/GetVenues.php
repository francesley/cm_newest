<?php

function GetVenue($venue,$counter) {  
	$area = $venue["area"];
	$pricingBit = "";
	$details = $venue -> details;
	$details = str_replace ("</h3>" , "<span>$area</span></h3>", $details);
	switch ($venue["price"]) {	
		case 0:		
		$pricingBit = "<p class='price cheap'>Venue cost:</p></div>";
		break;	
		case 1:
		$pricingBit = "<p class='price'>Venue cost:</p></div>";
		break;
		case 2:	
		$pricingBit = "<p class='price expensive'>Venue cost:</p></div>";
		break;
		default:
		$pricingBit = "</div>";
	}
	$extras = "<div class='extras'>";
	$extras .= $venue["civil"] == 1?"<p class='civil'>Civil license:</p>":"<p class='civil no'>Civil license:</p>";
	$extras .= $venue["large"] == 1?"<p class='civil'>Large marquees:</p>":"<p class='civil no'>Large marquees:</p>";
	$extras .= $pricingBit;
	$counter = $counter == 0?1:0;
	//echo $counter;
	
	if ($counter!=0) {
		echo "<div class='testvenue" . $counter . "'>  ". $details . $extras . "</div>";
	 }
	 else {	
		echo "<div class='testvenue" . $counter . "' style='margin-top:0;'>" . $details . $extras . "</div>";
	 }
 }