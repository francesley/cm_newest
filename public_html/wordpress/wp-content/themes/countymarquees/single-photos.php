<?php
/**
 * The template for displaying all single posts
 */
get_header();
// Start the loop.
if (have_posts()):
    while (have_posts()):
        the_post();
        ?>
        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="navigation">
                            <a href="javascript: history.go(-1)" role="link"> Back to Gallery </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="post-media-wrapper">
                            <div class="post-item-img">
                                <?php
                                // gets the 3 image sizes for the image uploaded , we can use crop plugin in the admin if we want to manually crop the image sizes in the admin
                                $large_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'gallery-size-large');
                                $medium_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'gallery-size-medium');
                                $small_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'gallery-size-small');

                                $large_image_url = $large_image[0];
                                $medium_image_url = $medium_image[0];
                                $small_image_url = $small_image[0];
                                ?>
                                    <img
                                    src="<?php echo esc_url($large_image_url); ?>"
                                    srcset="<?php echo esc_url($small_image_url); ?> 300w,
                                            <?php echo esc_url($medium_image_url); ?> 450w,
                                            <?php echo esc_url($large_image_url); ?> 620w"
                                    sizes="(max-width: 480px) 100vw, (max-width: 768px) 80vw, 620px"
                                    alt="<?php echo esc_attr(get_the_title()); ?>"
                                    height="415"
                                    width="620"
                                    />
                                <?php // Code gets the next and previous post link
                                        $custom_post_type = 'photos';
                                        // Next link
                                        $next_post = get_adjacent_post(false, '', false);

                                        if ($next_post) {
                                            $next_permalink = get_permalink($next_post);
                                        } else {
                                            // If there's no next post, set the link to the first post
                                            $first_posts = get_posts(
                                                array(
                                                    'post_type' => $custom_post_type,
                                                    'posts_per_page' => 1,
                                                    'orderby' => 'date',
                                                    'order' => 'ASC',
                                                )
                                            );
                                            $next_permalink = get_permalink($first_posts[0]);
                                        }

                                        // previous link
                                        $previous_post = get_adjacent_post(false, '', true);
                                        if ($previous_post) {
                                            $previous_permalink = get_permalink($previous_post);
                                        } else {
                                            // If there's no previous post, set the link to the last post
                                            $last_posts = get_posts(
                                                array(
                                                    'post_type' => $custom_post_type,
                                                    'posts_per_page' => 1,
                                                    'orderby' => 'date',
                                                    'order' => 'DESC',
                                                )
                                            );
                                            $previous_permalink = get_permalink($last_posts[0]);
                                        }
                                        ?>

                                <a class="carousel-control-prev" href="<?php echo $next_permalink; ?>">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="<?php echo $previous_permalink; ?>">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="post-content">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="related-gallery-list">
                <?php // gets the other photo galleries
                $args = array(
                    'post_type' => 'photos',
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => -1,
                    'post__not_in' => array(get_the_ID()),
                );

                $the_query = new WP_Query($args);
                if ($the_query->have_posts()):
                    while ($the_query->have_posts()):
                        $the_query->the_post();

                        ?>
                        <div class="rel-post-item">
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-item-img">
                                    <?php
                                    // here we are only getting the medium version of the image
                                    $medium_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'gallery-size-medium');
                                    $medium_image_url = $medium_image[0];
                                    ?>
                                    <img class="image" src="<?php echo $medium_image_url; ?>" />
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php else: ?>
                    <!-- Your no post found code here -->
                <?php endif; ?>
            </div>

        </section>
        <?php
        // End the loop.
    endwhile;
endif;
get_footer(); ?>