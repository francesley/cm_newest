// function $(element) {
  // return document.getElementById(element);
// }

var speedTest = {};

speedTest.pics = null;
speedTest.map = null;
speedTest.markerClusterer = null;
speedTest.markers = [];
speedTest.infoWindow = null;

speedTest.init = function() {
  var latlng = new google.maps.LatLng(51.53323, -0.124842);
  var options = {
    'zoom': 8,
    'center': latlng,
    'mapTypeId': google.maps.MapTypeId.ROADMAP
  };

 var map = document.getElementById('map');
  speedTest.map = new google.maps.Map( map, options);
  //speedTest.pics = data.photos;
  
  /*var useGmm = document.getElementById('usegmm');
  google.maps.event.addDomListener(useGmm, 'click', speedTest.change);*/
  
 /* var numMarkers = document.getElementById('nummarkers');
  google.maps.event.addDomListener(numMarkers, 'change', speedTest.change);*/

  speedTest.infoWindow = new google.maps.InfoWindow();

  speedTest.showMarkers();
};

speedTest.showMarkers = function() {
  speedTest.markers = [];

  var type = 1;
  /*if ($('usegmm').checked) {
    type = 0;
  }*/

  if (speedTest.markerClusterer) {
    speedTest.markerClusterer.clearMarkers();
  }

  var panel = document.getElementById('markerlist');
  
  panel.innerHTML = '';

  //var numMarkers = $('nummarkers').value; 
  
// Read the data

$.ajax({
  type: 'GET',
  async: true,
  url: 'markers.xml',
  dataType: "xml",
  success:function(xml){
	
		var markerstg = xml.documentElement.getElementsByTagName("marker");
		for (var i = 0; i < markerstg.length; i++) {
	  
	  		var lat = parseFloat(markerstg[i].getAttribute("lat"));

			var lng = parseFloat(markerstg[i].getAttribute("lng"));
			
			var title = parseFloat(markerstg[i].getAttribute("label"));
			
			//var siteIcon = gicons[markerstg[i].getAttribute("icon")];

			//var point = new GLatLng(lat,lng);			
			
			var latLng = new google.maps.LatLng(lat,lng);
			//console.log(lng);

			var imageUrl = 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&chco=' +
			'FFFFFF,008CFF,000000&ext=.png';
			
			
			 var titleText = markerstg[i].getAttribute("label");
			if (titleText === '') {
			  titleText = 'No title';
			}

			var item = document.createElement('DIV');
			var title = document.createElement('A');
			title.href = '#';
			title.className = 'title';
			title.innerHTML = titleText;

			item.appendChild(title);
			panel.appendChild(item);
			
			var markerImage = new google.maps.MarkerImage(imageUrl,new google.maps.Size(24, 32));

			var marker = new google.maps.Marker({
			'position': latLng,
			'icon': markerImage
			});			
			
			//console.log('reached here');
			var fn = speedTest.markerClickFunction(markerstg[i], latLng);
			
			google.maps.event.addDomListener(title, "mouseover", function (mCluster) {    
				// infowindow.content += "<div>Something<\/div>";
				// infowindow.setPosition(mCluster.getCenter());
				// infowindow.open(map);
				console.log('active'); /* Currently working on */
			});
			
			google.maps.event.addListener(marker, 'click', fn);
			
			google.maps.event.addDomListener(title, 'click', fn);
	
			speedTest.markers.push(marker);
			
		}

	  
  }
});

  
/* var numMarkers = 25;

  for (var i = 0; i < numMarkers; i++) {
    var titleText = speedTest.pics[i].photo_title;
    if (titleText === '') {
      titleText = 'No title';
    }

    var item = document.createElement('DIV');
    var title = document.createElement('A');
    title.href = '#';
    title.className = 'title';
    title.innerHTML = titleText;

    item.appendChild(title);
    panel.appendChild(item);


    var latLng = new google.maps.LatLng(speedTest.pics[i].latitude,
        speedTest.pics[i].longitude);

    var imageUrl = 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&chco=' +
        'FFFFFF,008CFF,000000&ext=.png';
    var markerImage = new google.maps.MarkerImage(imageUrl,
        new google.maps.Size(24, 32));

    var marker = new google.maps.Marker({
      'position': latLng,
      'icon': markerImage
    });

    var fn = speedTest.markerClickFunction(speedTest.pics[i], latLng);
    google.maps.event.addListener(marker, 'click', fn);
    google.maps.event.addDomListener(title, 'click', fn);
    speedTest.markers.push(marker);
  }
  
  */


  window.setTimeout(speedTest.time, 0);
};

speedTest.markerClickFunction = function(pic, latlng) { 
	var icon = pic.getAttribute("icon")

  return function(e) {
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) {
      e.stopPropagation();
      e.preventDefault();
    }
	
	
	
    var title = pic.getAttribute("label")
    var url = pic.photo_url;
    var fileurl = pic.photo_file_url;
	var infoHtml = pic.getElementsByTagName("details")[0].firstChild.nodeValue; //info window content
	 
	// console.log(infoHtml); return false;

    // var infoHtml = '<div class="info"><h3>' + title +
      // '</h3><div class="info-body">' +
      // '<a href="' + url + '" target="_blank"><img src="' +
      // fileurl + '" class="info-img"/></a></div>' +
      // '<a href="http://www.panoramio.com/" target="_blank">' +
      // '<img src="http://maps.google.com/intl/en_ALL/mapfiles/' +
      // 'iw_panoramio.png"/></a><br/>' +
      // '<a href="' + pic.owner_url + '" target="_blank">' + pic.owner_name +
      // '</a></div></div>';

    speedTest.infoWindow.setContent(infoHtml);
    speedTest.infoWindow.setPosition(latlng);
    speedTest.infoWindow.open(speedTest.map);
  };
};

speedTest.clear = function() {
  //$('timetaken').innerHTML = 'cleaning...';
  for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
    marker.setMap(null);
  }
};

speedTest.change = function() {
  speedTest.clear();
  speedTest.showMarkers();
};

speedTest.time = function() {
  //$('timetaken').innerHTML = 'timing...';
  var start = new Date();
 /* if ($('usegmm').checked) {
    speedTest.markerClusterer = new MarkerClusterer(speedTest.map, speedTest.markers, {imagePath: '../images/m'});
  } else {*/
    for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
      marker.setMap(speedTest.map);
    }
  /*}*/

  var end = new Date();
 // $('timetaken').innerHTML = end - start;
};
