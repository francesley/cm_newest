<?php
require_once( 'classes/errors.php' );
echo <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>/*$page_headers->meta_title*/</title>
<meta name="description" content="/*$page_headers->metadesc*/" />
<meta name="keywords" content="/*$page_headers->metakey*/" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<link type="text/css" rel="stylesheet" href="/cm.css" />
<!--[if lt IE 6]>
		<style type="text/css" media="screen">
			@import "/bradburysIE.css";
		</style>
		<![endif]-->
<link type="text/css" rel="stylesheet" href="/sifr/sIFR-screen.css" />
<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />
<script src="/sifr/sifr.js" type="text/javascript"></script>
$headerExtra
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.icra.org/ratingsv02.html" comment "ICRAonline EN v2.0" l gen true for "https://www.countymarquees.com" r (nz 1 vz 1 lz 1 oz 1 cz 1) "http://www.rsac.org/ratingsv01.html" l gen true for "https://www.countymarquees.com" r (n 0 s 0 v 0 l 0))'>
<meta name="robots" content="all">
</head>
<body><div id="outerMain">
	<div id="main" $mainClass>
		<div id="top">
		<div id="topLinks"><a href="https://www.countymarquees.com">Home</a> | <a href="https://www.countymarquees.com">Brochure</a> | <a href="/contact_us.htm">Contact</a></div>
			<a href="https://www.countymarquees.com/"><img src="/images/logo.gif" alt="Marquee hire from County Marquees" width="213" height="48" id="logo"/></a>
			<a href="/venues.htm">Marquee venue finder</a>			
	  </div>
EOF;
		$mainMenu = new topMenu();
  		echo $mainMenu -> m_menu;
		if (!empty($page_headers -> page_title)) {
			$pageTitle = strtoupper($page_headers -> page_title);
			$pageTitle = str_replace("&UUML;", "&Uuml;", $pageTitle);
			//$pageTitle = str_replace("&AMP;", "&amp;", $pageTitle);
			echo "<h1>$pageTitle</h1>";
		}

?>