<?php

	if(!$xml = simplexml_load_file('xml/marquee_prices.xml')){
		trigger_error('Error reading XML file',E_USER_ERROR);
	}
	
	$content = "";
	
	//roofing-material[type="shingles"]
	foreach ($xml -> xpath("marquee[@size='medium']") as $marquee) {
	//foreach($xml -> marquee as $marquee){
		$content .= "<h2>For " . $marquee["minCap"] . " to " . $marquee["maxCap"] . " people<br/>";
		$content .= "<span>Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees</span><h2/>";
		
		$content .= "<table class='pricing' border='1' summary=\"Prices for " . $marquee["width"] . "m/" . $marquee["footWidth"] . "' " . $marquee["type"] . " marquees\">";
		$content .= "<tr><th rowSpan='2' scope='col' style='nobreak'>Marquee size<br><span>Width x<br/>Length</span></th>
			<th colSpan='2'>Capacity </th>
			<th colSpan='2'>Marquee</th>
			<th rowspan='2' valign='bottom' scope='col'>Coconut <br/>Matting</th>
			<th rowspan='2' valign='bottom' scope='col'>Lighting</th>
			<th colSpan='2' align='center' class='leftBorder'>Total hire price</th>
		</tr>
		<tr><th scope='col'> Seated</th>
			<th scope='col'>Buffet</th>
			<th valign='bottom' scope='col'>Lined</th>
			<th valign='bottom' scope='col'>Unlined</th>
			<th align='center' class='leftBorder' scope='col'>Lined</th>
			<th align='center' class='subHeaderCell' scope='col'>Unlined</th>
		 </tr>";
		
		foreach($marquee -> row as $row){
			$content .= "<tr>\n<td class='leftCol' scope='row' style='nobreak'>";
			$content.= $marquee["width"] . "m x " . $row["length"] . "m<br/>";
			$content .= "<span>(" . $marquee["footWidth"] . "' x " . $row["footLength"] . "')</span></td>\n";
			$content .= "<td>" . $row["seatedCap"] . "</td>\n";
			$content .= "<td>" . $row["buffetCap"] . "</td>\n";
			$content .= "<td>&pound;" . $row["linedP"] . "</td>\n";
			$content .= "<td>&pound;" . $row["unlinedP"] . "</td>\n";
			$content .= "<td>&pound;" . $row["mat"] . "</td>\n";
			$content .= "<td>&pound;" . $row["light"] . "</td>\n";			
			$content .= "<td class='leftBorder'>&pound;" . $row -> totals["lined"] . "</td>\n";
			$content .= "<td class='leftBorder'>&pound;" . $row -> totals["unlined"] . "</td>\n";
			$content . "</tr>\n";
		}		
		$content .= "</table>\n";
	}
	
	echo $content;
	

?>
