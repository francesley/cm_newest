<?php
require_once( '../classes/errors.php' );
$mainMenu = new topMenu($request -> getProperty("cat"), $request -> getProperty("subtype"));
$mainMenu -> m_menu .= "</div>\n<div id='box'><div id='outerBox'>\n<div id='content'>";
echo <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
$pageDetails->html_dec
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>$pageDetails->meta_title</title>
<meta name="description" content="$pageDetails->metadesc" />
<meta name="keywords" content="$pageDetails->metakey" />
<link type="text/css" rel="stylesheet" href="/css/cm_min.css" />
<!--[if lte IE 6]>
<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="/scripts/DD_belatedPNG_0.0.7a-min.js"></script>
<script  defer>
  DD_belatedPNG.fix('#logo img, #subMenu, #subMenu li');
</script>
<![endif]-->
<!--[if IE 7]>
<link href="/css/ie7.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->
<!--[if IE 8]>
<link href="/css/ie8.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->
{$pageDetails -> headerExtra}
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.icra.org/ratingsv02.html" comment "ICRAonline EN v2.0" l gen true for "http://www.countymarquees.com" r (nz 1 vz 1 lz 1 oz 1 cz 1) "http://www.rsac.org/ratingsv01.html" l gen true for "http://www.countymarquees.com" r (n 0 s 0 v 0 l 0))' />
<link rel="SHORTCUT ICON" href="http://www.countymarquees.com/favicon.ico" />
</head>
<body {$pageDetails -> bodyExtra}><div id="outerMain">
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
	<div id="main" {$pageDetails -> main_class}>
		<div id="top">
			<a href="http://www.countymarquees.com/" id="logo"><img src="/images/logo.png" alt="Marquee hire from County Marquees" width="186" height="93"/></a>			
	  {$mainMenu -> m_menu}
	 
EOF;
		
		/*if (isset($subMenu)) {
			echo $mainMenu -> m_menu . $subMenu -> m_menu . "</div>\n<div id='box'><div id='outerBox'>\n<div id='content'>";
		} else {*/
			
		//}
  		//echo "<p id='value'>beautiful marquees and friendly, professional service</p>";
		
?>